package com.flutura.ImpalaRestApiController.security;

import com.flutura.ImpalaRestApiController.model.JwtUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

@Component
public class JwtGenerator {


    public String generate(JwtUser jwtUser) {


        Claims claims = Jwts.claims();
                //.setSubject(jwtUser.getUsername());
        claims.put("user_id", jwtUser.getUser_id());
        claims.put("exp", jwtUser.getExp());
        claims.put("email",jwtUser.getEmail());
        claims.put("username",jwtUser.getUsername());
        claims.put("orig_iat",jwtUser.getOrig_iat());


        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS256, "dz-e6*6-fo*c4zh^mjihqcw2)#zea@z&_vnrz08#-^2utxxc*b")
                .compact();
    }
}
