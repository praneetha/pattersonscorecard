package com.flutura.ImpalaRestApiController.security;

import com.flutura.ImpalaRestApiController.model.JwtUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class JwtValidator {

	private static final Logger log = LoggerFactory.getLogger(JwtValidator.class);
    private String secret = "dz-e6*6-fo*c4zh^mjihqcw2)#zea@z&_vnrz08#-^2utxxc*b";

    public JwtUser validate(String token) {
        JwtUser jwtUser = null;
		//System.out.println("Token " + token);
	
        try {

            Claims body = Jwts.parser()
                    .setSigningKey(secret.getBytes())
                    .parseClaimsJws(token)
                    .getBody();

            jwtUser = new JwtUser();
            jwtUser.setExp(Long.parseLong(body.get("exp").toString()));
            jwtUser.setEmail((String)body.get("email"));
            jwtUser.setUser_id(Integer.parseInt(body.get("user_id").toString()));
            jwtUser.setOrig_iat(Long.parseLong(body.get("orig_iat").toString()));
            jwtUser.setUsername((String)body.get("username"));
        }
        catch (Exception e) {
            log.info(e.getMessage());
        }

        return jwtUser;
    }
}
