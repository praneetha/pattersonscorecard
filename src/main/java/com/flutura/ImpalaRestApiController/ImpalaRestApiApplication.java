package com.flutura.ImpalaRestApiController;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

//@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
@SpringBootApplication(scanBasePackages = {"com.pten", "com.flutura.ImpalaRestApiController"})
@ServletComponentScan(basePackages = {"com.pten.rs"})
public class ImpalaRestApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ImpalaRestApiApplication.class, args);
    }

    @Bean("stringStringRedisTemplate")
    public RedisTemplate<String,String> test(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<String, String> objectObjectRedisTemplate = new RedisTemplate<>();
        objectObjectRedisTemplate.setConnectionFactory(redisConnectionFactory);
        objectObjectRedisTemplate.setKeySerializer(new StringRedisSerializer());
        objectObjectRedisTemplate.setValueSerializer(new StringRedisSerializer());
        objectObjectRedisTemplate.setHashKeySerializer(new StringRedisSerializer());
        objectObjectRedisTemplate.setHashValueSerializer(new StringRedisSerializer());
        objectObjectRedisTemplate.afterPropertiesSet();
        return objectObjectRedisTemplate;
    }
    // to globally change parsing and serializing dates in restcontroller input parameters // Spring 2.0.3+
//    @Bean
//    public Formatter<LocalDate> localDateFormatter() {
//        return new Formatter<LocalDate>() {
//            @Override
//            public LocalDate parse(String text, Locale locale) throws ParseException {
//                return LocalDate.parse(text, DateTimeFormatter.ofPattern("yyyy/MM").ISO_DATE);
//            }
//
//            @Override
//            public String print(LocalDate object, Locale locale) {
//                return DateTimeFormatter.ofPattern("yyyy/MM").ISO_DATE.format(object);
//            }
//        };
//    }
}
