package com.flutura.ImpalaRestApiController;

import com.pten.Utilities.DbUtils.PtenInternalScorecardDbUtilLib;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

public class CSVExport implements Callable<JSONObject> {

    private static final Logger log = LoggerFactory.getLogger(CSVExport.class);

    private String year;
    private String month;
    private String fileName;
    private int fileTypeID;
    private String uploadUser;

    public CSVExport(String year, String month, String fileName, int fileTypeID, String uploadUser) {
        this.year = year;
        this.month = month;
        this.fileName = fileName;
        this.fileTypeID = fileTypeID;
        this.uploadUser = uploadUser;
    }

    PtenInternalScorecardDbUtilLib utils = BeanUtil.getBean(PtenInternalScorecardDbUtilLib.class);

    @Override
    public JSONObject call() {
        JSONObject jsonObject = new JSONObject();
        Properties properties = new Properties();
        String configFileName = "application.properties";

        try(InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            Thread.sleep(3000);
            System.out.println(Thread.currentThread().getName() + " for fileTypeID " + fileTypeID);
            Long startTime = System.currentTimeMillis();
            jsonObject = connectToPostgres(properties, startTime);

        } catch (InterruptedException e) {
            log.error("InterruptedException in call: " + e.getMessage(),e);
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in call: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in call: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in call: " + e.getMessage(),e);
        }
        return jsonObject;
    }

    private JSONObject connectToPostgres(Properties properties, Long startTime) {

        JSONObject jsonObject = new JSONObject();
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(properties.getProperty("ptenInternal.datasource.scorecard.jdbc-url"),
                    properties.getProperty("ptenInternal.datasource.scorecard.username"),
                    properties.getProperty("ptenInternal.datasource.scorecard.password"));
            System.out.println("Connected to the PostgreSQL server successfully.");
            if (conn != null) {
                jsonObject = getFileTableName(properties, conn, fileTypeID, startTime, uploadUser);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (SQLException e) {
            log.error("SQLException in connectToPostgres: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in connectToPostgres: " + e.getMessage(),e);
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch(SQLException e) {
                log.error("SQLException in connectToPostgres: " + e.getMessage(),e);
            }
        }
        return jsonObject;
    }

    private JSONObject getFileTableName(Properties properties, Connection conn, int fileTypeID, Long startTime,
                                        String uploadUser) {

        String fileTableName = null;
        JSONObject jsonObject = new JSONObject();

        try {
            String query = "SELECT file_table_name" +
                    " FROM filetype " +
                    "WHERE file_type_id = '" + fileTypeID + "'";

            Collection result = utils.executeNamedQuery(query, null);
            JSONArray jsonResult = new JSONArray(result);
            for (int i = 0; i < jsonResult.length(); i++) {
                JSONObject resultObject = jsonResult.getJSONObject(i);
                Iterator keys = resultObject.keys();
                while (keys.hasNext()) {
                    fileTableName = resultObject.get(keys.next().toString()).toString();
                }
            }
            jsonObject = insertToFileUploadTable(properties, conn, year, month, fileName, fileTypeID, fileTableName,
                    startTime, uploadUser);
        } catch (JSONException e) {
            log.error("JSONException in getFileTableName: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getFileTableName: " + e.getMessage(),e);
        }
        return jsonObject;
    }

    private JSONObject insertToFileUploadTable(Properties properties, Connection conn, String year, String month,
                                               String fileName, int fileTypeID, String fileTableName,
                                               Long startTime, String uploadUser) {

        String effectiveDate = year + "-" + month + "-01";
        JSONObject jsonObject = new JSONObject();

        try(Statement statement = conn.createStatement()) {

            String checkQuery = "SELECT * FROM FileUpload " +
                    "WHERE effective_date = '" + effectiveDate + "' AND file_type_id = " + fileTypeID +
                    " AND upload_user = '" + uploadUser + "'";

            ResultSet resultSet = statement.executeQuery(checkQuery);
            if (resultSet == null) {
                String insertQuery = "INSERT INTO FileUpload " +
                        "(effective_date, upload_date, upload_user, filename, file_type_id, active) " +
                        "VALUES ('" + effectiveDate + "',now(),'" + uploadUser + "', '" + fileName + "', " + fileTypeID +
                        ", true)";
                statement.executeUpdate(insertQuery);

                String selectQuery = "SELECT effective_date FROM FileUpload " +
                        "WHERE filename = '" + fileName + "' and active = 't' AND upload_user = '" + uploadUser + "'";
                Collection result = utils.executeNamedQuery(selectQuery, null);
                JSONArray jsonResult = new JSONArray(result);
                for (int i = 0; i < jsonResult.length(); i++) {
                    JSONObject resultObject = jsonResult.getJSONObject(i);
                    String key = resultObject.keys().next().toString();
                    effectiveDate = resultObject.get(key).toString();
                }
            } else {
                String updateQuery = "UPDATE FileUpload SET " +
                        "active = false WHERE effective_date = '" + effectiveDate + "' AND " +
                        "file_type_id = " + fileTypeID +
                        " AND upload_user = '" + uploadUser + "'";
                statement.executeUpdate(updateQuery);
                String insertQuery = "INSERT INTO FileUpload " +
                        "(effective_date, upload_date, upload_user, filename, file_type_id, active) " +
                        "VALUES ('" + effectiveDate + "',now(),'" + uploadUser + "', '" + fileName + "', " +
                        fileTypeID + ", true)";
                statement.executeUpdate(insertQuery);

                String selectQuery = "SELECT effective_date FROM FileUpload " +
                        "WHERE filename = '" + fileName + "' and active = 't' AND upload_user = '" + uploadUser + "'";
                Collection result = utils.executeNamedQuery(selectQuery, null);
                JSONArray jsonResult = new JSONArray(result);
                for (int i = 0; i < jsonResult.length(); i++) {
                    JSONObject resultObject = jsonResult.getJSONObject(i);
                    String key = resultObject.keys().next().toString();
                    effectiveDate = resultObject.get(key).toString();
                }
            }
            jsonObject = getFileUploadID(properties, conn, fileName, effectiveDate, fileTypeID, fileTableName,
                    startTime, uploadUser);
        } catch (SQLException e) {
            log.error("SQLException in insertToFileUploadTable: " + e.getMessage(),e);
        } catch (JSONException e) {
            log.error("JSONException in insertToFileUploadTable: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in insertToFileUploadTable: " + e.getMessage(),e);
        }
        return jsonObject;
    }

    private JSONObject getFileUploadID(Properties properties, Connection conn, String fileName, String effectiveDate,
                                       int fileTypeID, String fileTableName, Long startTime, String uploadUser) {

        int fileUploadID = 0;
        JSONObject jsonObject = new JSONObject();
        Timestamp updatedTime = null;

        try {
            String query = "SELECT file_upload_id " +
                    "FROM FileUpload " +
                    "WHERE filename = '" + fileName + "' AND effective_date = '" + effectiveDate + "' " +
                    "AND upload_user = '" + uploadUser + "'";
            System.out.println(query);

            Collection result = utils.executeNamedQuery(query, null);
            JSONArray jsonResult = new JSONArray(result);
            for (int i = 0; i < jsonResult.length(); i++) {
                JSONObject resultObject = jsonResult.getJSONObject(i);
                String key = resultObject.keys().next().toString();
                fileUploadID = Integer.parseInt(resultObject.get(key).toString());
            }

            int retResult = extractRowsFromCSV(properties, conn, fileUploadID, fileTypeID, fileTableName, fileName, startTime);
            if (retResult == -1) {
                deactivateFileUploadFlag(conn, fileUploadID);
            } else {
                updatedTime = getLastUpdatedDate(conn, fileUploadID);
            }
            jsonObject.put("FileUploadID", retResult);
            jsonObject.put("LastUpdatedDate", updatedTime);
        } catch (JSONException e) {
            log.error("JSONException in getFileUploadID: " + e.getMessage(),e);
        }
        return jsonObject;
    }

    private int extractRowsFromCSV(Properties properties, Connection conn, int fileUploadID, int fileTypeID,
                                     String tableName, String fileName, Long startTime) {

        int lineOrder = 1;
        int flag = 0;
        String columns = null;
        String fileID;
        boolean exemptionFlag = false;
        String serverPath = properties.getProperty("file.intermediateServerPath");
        ArrayList<String> columnMapping = new ArrayList<>();
        File schemaMapping = new File("src/main/resources/schema_mapping.txt");

        try(BufferedReader reader = new BufferedReader(new FileReader(schemaMapping))) {

            String readLine;
            Pattern pattern = Pattern.compile("[a-zA-Z]");

            while ((readLine = reader.readLine()) != null) {
                fileID = readLine.split("\\$")[0];
                if (Integer.parseInt(fileID) == fileTypeID) {
                    columns = readLine.split("\\$")[1];
                }
            }

            String[] columnMapList = new String[0];
            if (columns != null) {
                columnMapList = columns.split("\\|");
            }
            for (String column : columnMapList) {
                String value = column.split(":")[1].replace("'", "");
                columnMapping.add(value);
            }

            File directory = new File(serverPath);
            File[] files = directory.listFiles();
            if (files != null) {
                for (File file : files) {
                    if (file.getName().equals(fileName)) {
                        CSVParser csvParser = new CSVParser(new FileReader(file), CSVFormat.DEFAULT
                                .withFirstRecordAsHeader()
                                .withIgnoreHeaderCase()
                                .withTrim());

                        for (CSVRecord csvRecord : csvParser) {
                            StringBuilder stringBuilder = new StringBuilder();
                            StringBuilder columnBuilder = new StringBuilder();
                            for (int cn = 0; cn < csvRecord.size(); cn++) {
                                columnBuilder.append(columnMapping.get(cn)).append(",");
                                if(csvRecord.get(cn) != null) {
                                    if(csvRecord.get(cn).contains(" ")) {
                                        stringBuilder.append("'").append(csvRecord.get(cn)).append("'").append(",");
                                    }
                                    else {
                                        Matcher matcher = pattern.matcher(csvRecord.get(cn));
                                        boolean match = matcher.find();
                                        if(match) {
                                            stringBuilder.append("'").append(csvRecord.get(cn)).append("'").append(",");
                                        }
                                        else {
                                            stringBuilder.append(csvRecord.get(cn)).append(",");
                                        }
                                    }
                                } else {
                                    stringBuilder.append("NULL").append(",");
                                }
                            }
                            stringBuilder.setLength(stringBuilder.length() - 1);
                            columnBuilder.setLength(columnBuilder.length() - 1);
                            flag = insertRowToTable(conn, fileUploadID, lineOrder, exemptionFlag, stringBuilder, columnBuilder,
                                    tableName, flag);
                            lineOrder = lineOrder + 1;
                        }
                        file.delete();
                        csvParser.close();
                    }
                }
            } else {
                throw new Exception("Directory is empty");
            }
            if (flag > 0) {
                deactivateFileUploadFlag(conn, fileUploadID);
                return -1;
            }

            System.out.println("Data inserted into postgres");
            Long endTime = System.currentTimeMillis();
            System.out.println("Total time: " + ((endTime - startTime) / 1000) % 60 + " seconds");

        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in extractRowsFromCSV: " + e.getMessage(),e);
            return -1;
        } catch (IOException e) {
            log.error("IOException in extractRowsFromCSV: " + e.getMessage(),e);
            return -1;
        } catch (Exception e) {
            log.error("Exception in extractRowsFromCSV: " + e.getMessage(),e);
            return -1;
        }
        return fileUploadID;
    }

    private int insertRowToTable(Connection conn, int fileUploadID, int lineOrder, boolean exemptionFlag,
                                 StringBuilder columns, StringBuilder columnList, String tableName, int flag) {

        try(Statement statement = conn.createStatement()) {

            String query = "INSERT INTO " + tableName + "(file_upload_id,line_order," + columnList + ",exemption_flag)" +
                    " VALUES(" + fileUploadID + "," + lineOrder + "," + columns + "," + exemptionFlag + ")";
            System.out.println(query);
            //statement.executeUpdate(query);

        } catch (SQLException e) {
            log.error("SQLException in insertRowToTable: " + e.getMessage(),e);
            flag = flag + 1;
        }
        return flag;
    }

    private void deactivateFileUploadFlag(Connection conn, int fileUploadID) {

        try(Statement statement = conn.createStatement()) {

            String query = "UPDATE FileUpload set active = false WHERE file_upload_id = " + fileUploadID;

            statement.executeUpdate(query);

        } catch (SQLException e) {
            log.error("SQLException in deactivateFileUploadFlag: " + e.getMessage(),e);
        }
    }

    private Timestamp getLastUpdatedDate(Connection conn, int fileUploadID) {

        Timestamp updateTime = null;

        try(Statement statement = conn.createStatement()) {

            String query = "SELECT upload_date FROM FileUpload where file_upload_id = " + fileUploadID;

            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                updateTime = result.getTimestamp("upload_date");
            }

        } catch (SQLException e) {
            log.error("SQLException in getLastUpdatedDate: " + e.getMessage(),e);
        }
        return updateTime;
    }
}
