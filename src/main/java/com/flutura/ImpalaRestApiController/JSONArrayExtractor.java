package com.flutura.ImpalaRestApiController;

import org.json.JSONObject;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Types;
import java.util.ArrayList;

@Component
public class JSONArrayExtractor implements ResultSetExtractor<ArrayList> {

    @Override
    public ArrayList extractData(ResultSet rs) throws DataAccessException {
        ArrayList jsonArray = new ArrayList();

        try {
            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount();
            int columnType;

            JSONObject jsonObject;
            while (rs.next()) {
                jsonObject = new JSONObject();
                for (int i = 1; i < columnCount + 1; i++) {
                    String column_name = metaData.getColumnName(i);

                    columnType = metaData.getColumnType(i);

                    if (columnType == Types.BIGINT) {
                        jsonObject.put(column_name, rs.getLong(column_name));
                    } else if (columnType == Types.DOUBLE) {
                        jsonObject.put(column_name, rs.getDouble(column_name));
                    } else if (columnType == Types.FLOAT) {
                        jsonObject.put(column_name, rs.getDouble(column_name));
                    } else if (columnType == Types.INTEGER) {
                        jsonObject.put(column_name, rs.getInt(column_name));
                    } else if (columnType == Types.VARCHAR) {
                        jsonObject.put(column_name, rs.getString(column_name));
                    } else if (columnType == Types.TINYINT) {
                        jsonObject.put(column_name, rs.getInt(column_name));
                    } else if (columnType == Types.SMALLINT) {
                        jsonObject.put(column_name, rs.getInt(column_name));
                    } else if (columnType == Types.TIMESTAMP) {
                        jsonObject.put(column_name, rs.getTimestamp(column_name));
                    } else if (columnType == Types.DECIMAL) {
                        jsonObject.put(column_name, rs.getBigDecimal(column_name));
                    } else {
                        jsonObject.put(column_name, rs.getObject(column_name));
                    }
                    if (rs.wasNull()) {
                        jsonObject.put(column_name, JSONObject.NULL);
                    }
                }
                jsonArray.add(jsonObject);
            }
        } catch (Exception ex) {
            jsonArray.add(ex.getMessage());
        }
        return jsonArray;
    }
}
