package com.flutura.ImpalaRestApiController.model;

import org.springframework.stereotype.Component;

@Component
public class JwtUser {
    private String username;
    private String email;
    private long orig_iat;
    private long exp;
    private int user_id;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getOrig_iat() {

        return orig_iat;
    }

    public void setOrig_iat(long orig_iat) {
        this.orig_iat = orig_iat;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getExp() {
        return exp;
    }

    public void setExp(long exp) {
        this.exp = exp;
    }
}
