package com.flutura.ImpalaRestApiController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Date;
import java.util.Optional;


@Service
public class RequestHandler {
    private static final Logger log = LoggerFactory.getLogger(RequestHandler.class);

    @Autowired
    CerebraAPIDataLayer myDataLayer;

    public String inSlipsCasingFTHr(String wellnum, String starttime, String endtime) {
        return myDataLayer.inSlipsCasingFTHr(wellnum, starttime, endtime).toString();
    }

    public String inSlipsCasingMinutes(String wellnum, String starttime, String endtime) {
        return myDataLayer.inSlipsCasingMinutes(wellnum, starttime, endtime).toString();
    }

    public String DepthVsTime(String wellnum, String starttime, String endtime) {
        return myDataLayer.DepthVsTime(wellnum, starttime, endtime).toString();
    }

    public String DrillingTrendROP(String wellnum, String starttime, String endtime) {
        return myDataLayer.DrillingTrendROP(wellnum, starttime, endtime).toString();
    }

    public String DrillingTrendRPM(String wellnum, String starttime, String endtime) {
        return myDataLayer.DrillingTrendRPM(wellnum, starttime, endtime).toString();
    }

    public String DrillingTrendSPP(String wellnum, String starttime, String endtime) {
        return myDataLayer.DrillingTrendSPP(wellnum, starttime, endtime).toString();
    }

    public String DrillingTrendHKL(String wellnum, String starttime, String endtime) {
        return myDataLayer.DrillingTrendHKL(wellnum, starttime, endtime).toString();
    }

    public String DrillingTrend(String wellnum, String starttime, String endtime) {
        return myDataLayer.DrillingTrend(wellnum, starttime, endtime).toString();
    }

    public String totalRigStateBreakDown(String wellnum, String starttime, String endtime) {
        return myDataLayer.totalRigStateBreakDown(wellnum, starttime, endtime).toString();
    }

    public String trippingSpeedFTHr(String wellnum, String starttime, String endtime) {
        return myDataLayer.trippingSpeedFTHr(wellnum, starttime, endtime).toString();
    }

    public String trippingConnectionTime(String wellnum, String starttime, String endtime) {
        return myDataLayer.trippingConnectionTime(wellnum, starttime, endtime).toString();
    }

    public String drillingROPByStand(String wellnum, String starttime, String endtime) {
        return myDataLayer.drillingROPByStand(wellnum, starttime, endtime).toString();
    }

    public String w2WDrillingConnection(String wellnum, String starttime, String endtime) {
        return myDataLayer.w2WDrillingConnection(wellnum, starttime, endtime).toString();
    }

    public String rigStateBreakdownTrend(String wellnum, String starttime, String endtime) {
        return myDataLayer.rigStateBreakdownTrend(wellnum, starttime, endtime).toString();
    }

    public String WellDetails(String wellnum) {
        return myDataLayer.WellDetails(wellnum).toString();
    }

    public String wellParamList(String rigname, String starttime, String endtime) {
        return myDataLayer.wellParamList(rigname, starttime, endtime).toString();
    }

    public String rigParamList() {
        return myDataLayer.rigParamList().toString();
    }

    public String RigCMRPercentByMonth(String rigname, String starttime, String endtime, String subCodeText) {
        return myDataLayer.RigCMRPercentByMonth(rigname, starttime, endtime, subCodeText).toString();
    }

    public String RigCMRHoursByMonth(String rigname, String starttime, String endtime, Optional<String> subCodeText) {
        if (subCodeText.isPresent() && subCodeText.get().trim() != "") {
            return myDataLayer.RigCMRHoursByMonth(rigname, starttime, endtime, subCodeText.get()).toString();
        } else {
            return myDataLayer.RigCMRHoursByMonth(rigname, starttime, endtime, null).toString();
        }
    }

    public String RigCMRHoursBySubCode(String rigname, String starttime, String endtime, Optional<String> subCodeText) {
        if (subCodeText.isPresent() && subCodeText.get().trim() != "") {
            return myDataLayer.RigCMRHoursBySubCode(rigname, starttime, endtime, subCodeText.get()).toString();
        } else {
            return myDataLayer.RigCMRHoursBySubCode(rigname, starttime, endtime, null).toString();
        }
    }

    public String RigCMRDetailsBySubSubCode(String rigname, String starttime, String endtime) {
        return myDataLayer.RigCMRDetailsBySubSubCode(rigname, starttime, endtime).toString();
    }

    public String NPTPercentageByRegion(String starttime, String endtime) {
        return myDataLayer.NPTPercentageByRegion(starttime, endtime).toString();
    }

    public String NPTHoursBySubCode(Date starttime, Date endtime) {
        return myDataLayer.NPTHoursBySubCode(starttime, endtime).toString();
    }

    public String NPTHoursBySubCode(String starttime, String endtime) {
        return myDataLayer.NPTHoursBySubCode(starttime, endtime).toString();
    }

    public String NPTQuarterlyWithAverage(String starttime, String endtime) {
        return myDataLayer.NPTQuarterlyWithAverage(starttime, endtime).toString();
    }

    public String NPTPercentageByRegionMonth(String starttime, String endtime) {
        return myDataLayer.NPTPercentageByRegionMonth(starttime, endtime).toString();
    }

    public String DateTest(String starttime, String endtime) {
        return myDataLayer.DateTest(starttime, endtime).toString();
    }

    public String CMRRigList(String starttime, String endtime) {
        return myDataLayer.CMRRigList(starttime, endtime).toString();
    }

    public String CMRDailyDetails(String wellnum, String starttime, String endtime) {
        return myDataLayer.CMRDailyDetails(wellnum, starttime, endtime).toString();
    }

    public String NPTRegionQuarterlyWithAverage(String region, String starttime, String endtime) {
        return myDataLayer.NPTRegionQuarterlyWithAverage(region, starttime, endtime).toString();
    }

    public String NPTRegionRigMonthAverageSort(String region, String starttime, String endtime) {
        return myDataLayer.NPTRegionRigMonthAverageSort(region, starttime, endtime).toString();
    }

    public String NPTRegionHoursByYQM(String region, String starttime, String endtime) {
        return myDataLayer.NPTRegionHoursByYQM(region, starttime, endtime).toString();
    }

    public String NPTRegionHoursByRYM(String region, String starttime, String endtime) {
        return myDataLayer.NPTRegionHoursByRYM(region, starttime, endtime).toString();
    }

    public String NPTRegionHoursByRig(String region, String starttime, String endtime) {
        return myDataLayer.NPTRegionHoursByRig(region, starttime, endtime).toString();
    }

    public String NPTRegionRigPercent(String region, String starttime, String endtime) {
        return myDataLayer.NPTRegionRigPercent(region, starttime, endtime).toString();
    }

    public String NPTRegionPercentByRYM(String region, String starttime, String endtime) {
        return myDataLayer.NPTRegionPercentByRYM(region, starttime, endtime).toString();
    }

    public String NPTRegionRigTypeNPTPercent(String region, String starttime, String endtime) {
        return myDataLayer.NPTRegionRigTypeNPTPercent(region, starttime, endtime).toString();
    }

    public String wellDrillingParameters(String wellnum, String starttime, String endtime, String targetrows) {
        return myDataLayer.wellDrillingParameters(wellnum, starttime, endtime, targetrows).toString();
    }

    public String rigDrillingParameters(String rigname, String starttime, String endtime, String targetrows) {
        return myDataLayer.rigDrillingParameters(rigname, starttime, endtime, targetrows).toString();
    }

    public String NPTHoursByRegionSubCode(String starttime, String endtime) {
        return myDataLayer.NPTHoursByRegionSubCode(starttime, endtime).toString();
    }

    public String NPTHoursByRegionSubCodeYQM(String starttime, String endtime) {
        return myDataLayer.NPTHoursByRegionSubCodeYQM(starttime, endtime).toString();
    }

    public String NPTHoursByRYQM(String starttime, String endtime) {
        return myDataLayer.NPTHoursByRYQM(starttime, endtime).toString();
    }

    public String NPTHoursBySCRLYM(String riglist, String starttime, String endtime) {
        return myDataLayer.NPTHoursBySCRLYM(starttime, endtime, riglist.split(",")).toString();
    }

    public String NPTHoursBySCRL(String riglist, String starttime, String endtime) {
        return myDataLayer.NPTHoursBySCRL(starttime, endtime, riglist.split(",")).toString();
    }

    public String NPTSCHoursFromRL(String riglist, String starttime, String endtime) {
        return myDataLayer.NPTSCHoursFromRL(starttime, endtime, riglist.split(",")).toString();
    }

    public String NPTPctFromRL(String riglist, String starttime, String endtime) {
        return myDataLayer.NPTPctFromRL(starttime, endtime, riglist.split(",")).toString();
    }

    public String NPTPctByYMRL(String riglist, String starttime, String endtime) {
        return myDataLayer.NPTPctByYMRL(starttime, endtime, riglist.split(",")).toString();
    }

    public String StringTest(String riglist) {
        return myDataLayer.StringTest(riglist.split(","));
    }

    public String NPTPctArbitraryRigsTimegroups(String regionlist, String starttime, String endtime, String grouptype) {
        return myDataLayer.NPTPctArbitraryRegionsTimegroups(starttime, endtime, grouptype, regionlist.split(",")).toString();
    }

    public String NPTHoursArbitraryRigsTimegroups(String regionlist, String starttime, String endtime, String grouptype) {
        return myDataLayer.NPTHoursArbitraryRegionsTimegroups(starttime, endtime, grouptype, regionlist.split(",")).toString();
    }

    public String RegionList() {
        return myDataLayer.RegionList().toString();
    }

    public Object dateTest(Date from, Date to) {
        return myDataLayer.testDates(from, to);
    }

    public String NPTHoursArbitraryRegionsTotal(String regionlist, String starttime, String endtime) {
        return myDataLayer.NPTHoursArbitraryRegionsTotal(starttime, endtime, regionlist.split(",")).toString();
    }

    public String cmrOperator(String starttime, String endtime) {
        return myDataLayer.cmrOperator(starttime, endtime).toString();
    }

    public String cmrRegionList(String operator, String starttime, String endtime) {
        return myDataLayer.cmrRegionList(operator, starttime, endtime).toString();
    }

     public String cmrStateList(String starttime, String endtime) {
        return myDataLayer.cmrStateList( starttime, endtime).toString();
    }

    public String cmrCountyList(String stateProvince, String starttime, String endtime) {
        return myDataLayer.cmrCountyList(stateProvince.split(","),  starttime, endtime).toString();
    }

    public String cmrRigList(String region, String stateProvince, String county, String operator, String starttime, String endtime) {
        return myDataLayer.cmrRigList(region.split(","), stateProvince.split(","), county.split(","), operator.split(","), starttime, endtime).toString();
    }


    public String cmrWellList(String region, String stateProvince, String county, String rig_name, String operator, String starttime, String endtime) {
        return myDataLayer.cmrWellList(region.split(","), stateProvince.split(","), county.split(","), rig_name.split(","), operator.split(","), starttime, endtime).toString();
    }

    public String drillingRigMovement(String starttime, String endtime, String wellnum) {
        return myDataLayer.drillingRigMovement(starttime, endtime, wellnum).toString();
    }

    public String drillingRigDown(String starttime, String endtime, String wellnum) {
        return myDataLayer.drillingRigDown(starttime, endtime, wellnum).toString();
    }

    public String rigDrilling(String wellnum) {
        return myDataLayer.rigDrilling(wellnum).toString();
    }

    public String rigTripping(String wellnum) {
        return myDataLayer.rigTripping(wellnum).toString();
    }

    public String rigsOutOfHole(String wellnum) {
        return myDataLayer.rigsOutOfHole(wellnum).toString();
    }

    public String wellRecentlyReleased(String starttime, String endtime, String wellnum) {
        return myDataLayer.wellRecentlyReleased(starttime, endtime, wellnum).toString();
    }

    public String wellUpcomingRelease(String starttime, String endtime, String wellnum) {
        return myDataLayer.wellUpcomingRelease(starttime, endtime, wellnum).toString();
    }

    public String rigMetaInfo(String starttime, String endtime, String wellnum) {
        return myDataLayer.rigMetaInfo(starttime, endtime, wellnum).toString();
    }

    public String rigCurrentStatusInfo(String starttime, String endtime, String wellnum) {
        return myDataLayer.rigCurrentStatusInfo(starttime, endtime, wellnum).toString();
    }

    public String vSWellList(String rigname, String starttime, String endtime) {
        return myDataLayer.vSWellList(rigname.split(","), starttime, endtime).toString();
    }

    public String patDRPRigList() {
        return myDataLayer.patDPRRigList().toString();
    }

    public String patDRPWellList(String rigname, String endTime) {
        return myDataLayer.patDPRWellList(rigname, endTime).toString();
    }

    public String patCMRRigList() {
        return myDataLayer.patCMRRigList().toString();
    }

    public String totalRigList(String wellnum) {
        return myDataLayer.totalRigList(wellnum).toString();
    }

    public HashMap cmrAllFilterData(String starttime, String endtime) {
        return myDataLayer.cmrAllFilterData(starttime, endtime);
    }

}
