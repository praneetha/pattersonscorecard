package com.flutura.ImpalaRestApiController;

import com.pten.Utilities.DateUtils.DateTimeUtils;
import com.pten.Utilities.DbUtils.ImpalaDbUtilLib;
import com.pten.Utilities.UtilityLibrary;
import com.pten.ui.data.Well;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static java.util.stream.Collectors.toList;

@Component
public class CerebraAPIDataLayer {
    private static final Logger log = LoggerFactory.getLogger(RequestHandler.class);

    private static final int TOP_10 = 10;
    private static final int TOP_6 = 6;

    @Autowired
    UtilityLibrary myUtils;

    @Autowired()
    @Qualifier("stringStringRedisTemplate")
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    ImpalaDbUtilLib impalaDbUtil;

    public int getUser_id() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().
                getAuthentication().getPrincipal();
        return Integer.parseInt(userDetails.getUsername());
    }

    public Collection inSlipsCasingFTHr(String wellnum, String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {
            long wellNum = Long.parseLong(wellnum);
            long endTime = DateTimeUtils.dateToEpoch(endtime + " 06:00");
            long startTime = endTime - 86400;
            long k = System.currentTimeMillis();

            String query = "with standDetails as (\n" +
                    "   select a.well_num\n" +
                    "        ,cast(a.Stand_ID as int) as Stand_ID\n" +
                    "        ,cast(a.Starting_Bit_Depth as int) as Starting_Bit_Depth\n" +
                    "        ,SUM(3600 * (a.stand_length / a.total_sec )) over (Partition by a.stand_id) as total_ft_per_hour\n" +
                    "   from ptendrilling.vw_wellstands_casing a\n" +
                    "   inner join user_Security.user_mapping_details u on  a.well_num=u.well_num AND u.userid =" + getUser_id() + "\n" +
                    "   Where a.well_num = CAST( " + wellNum + " AS bigint) \n" +
                    "        and a.stand_start_time >= " + startTime + "\n" +
                    "        and a.stand_start_time < " + endTime + "\n" +
                    ")\n" +
                    "select well_num\n" +
                    "        ,stand_id\n" +
                    "        ,cast(Starting_Bit_Depth as int) as Starting_Bit_Depth\n" +
                    "        ,cast(total_ft_per_hour as decimal(10,0)) as total_ft_per_hour\n" +
                    "        ,cast(total_ft_per_hour as decimal(10,0)) as in_total_ft_per_hour\n" +
                    "        ,null as out_total_ft_per_hour\n" +
                    "        ,cast(avg(total_ft_per_hour) over (partition by well_num) as decimal(10,0)) as avg_ft_per_hour\n" +
                    "        ,concat('<div style=\"padding:4px;color:white;background:#002060;position:relative;bottom:5px\">'\n" +
                    "                ,cast(cast(avg(total_ft_per_hour) over (partition by well_num) as decimal(10,0)) as string)\n" +
                    "                ,' Avg ft/hr</div>'\n" +
                    "                ) as avg_ft_per_hour_custom_label \n" +
                    "from standDetails\n" +
                    "Order by Stand_ID;";

            log.debug("{}", query);

            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            total = System.currentTimeMillis() - start;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;
    }

    public Collection inSlipsCasingMinutes(String wellnum, String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {
            long wellNum = Long.parseLong(wellnum);
            long endTime = DateTimeUtils.dateToEpoch(endtime + " 06:00");
            long startTime = endTime - 86400;
            long k = System.currentTimeMillis();

            String query = "with standDetails as (\n" +
                    "  select a.stand_start_timepoint\n" +
                    "        ,a.well_num\n" +
                    "        ,cast(a.Stand_ID as int) as Stand_ID\n" +
                    "        ,cast(a.Starting_Bit_Depth as int) as Starting_Bit_Depth\n" +
                    "        ,a.sec_s2s\n" +
                    "        ,SUM(3600 * (a.stand_length / a.total_sec )) over (Partition by a.stand_id) as total_ft_per_hour\n" +
                    "   from ptendrilling.vw_wellstands_casing a\n" +
                    "   inner join user_Security.user_mapping_details u on  a.well_num=u.well_num AND u.userid = " + getUser_id() + "\n" +
                    "   Where a.well_num = CAST( " + wellNum + " AS bigint) \n" +
                    "        and a.stand_start_time >= " + startTime + "\n" +
                    "        and a.stand_start_time < " + endTime + " \n" +
                    ")\n" +
                    "SELECT well_num\n" +
                    "        ,Stand_ID\n" +
                    "        ,cast(Starting_Bit_Depth as int) as Starting_Bit_Depth\n" +
                    "        ,cast(sec_S2S/60 as decimal(10,2)) as sec_S2S\n" +
                    "        ,sec_S2S/60 as IN_Sec_S2S\n" +
                    "        ,null as OUT_Sec_S2S\n" +
                    "        ,cast((AVG(sec_S2S) over (Partition by well_num))/60 as decimal(10,2)) AS avg_in_Slips\n" +
                    "        ,concat('<div style=\"padding:4px;color:white;background:#002060;position:relative;bottom:5px\">',cast(cast((AVG(sec_S2S) over (Partition by well_num))/60 as decimal(10,2)) as string),' Avg In Slips</div>') as avg_in_Slips_custom_label \n" +
                    "FROM standDetails \n" +
                    "Order by stand_start_timepoint ;";

            log.debug("{}", query);

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            total = System.currentTimeMillis() - start;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        log.info("Response Sent " + fetchingtime + " " + total);
        return myJSONArray;
    }

    public Collection DepthVsTime(String wellnum, String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {
            long wellNum = Long.parseLong(wellnum);
            long endTime = DateTimeUtils.dateToEpoch(endtime + " 06:00");
            long startTime = endTime - 86400;
            long k = System.currentTimeMillis();
            String query = "With TimeGenerator as (\n" +
                    "SELECT " + startTime + "+ID as Timepoint  from ptendrilling.ptendrilling_timeseq timeseq\n" +
                    "), TimeDataGen as (\n" +
                    "SELECT " + wellNum + " as WELL_NUM\n" +
                    ",g.timepoint\n" +
                    ",t.timepoint as TimeDataTimepoint\n" +
                    ",t.hole_depth_feet\n" +
                    ",t.bit_depth_feet\n" +
                    ",Case when t.timepoint IS NULL THEN 'No Data' ELSE 'Rig Status' END AS rig_status_text\n" +
                    ",Case when t.timepoint IS NULL THEN  20 ELSE -1 END AS rig_status_id \n" +
                    "FROM TimeGenerator g \n" +
                    "LEFT OUTER JOIN ptendrilling.ptendrilling_timedata t \n" +
                    "on g.timepoint=t.timepoint \n" +
                    "and t.well_num=" + wellNum + " \n" +
                    "and t.timepoint>=" + startTime + " \n" +
                    "and t.timepoint<" + endTime + "\n" +
                    "), \n" +
                    "TimeDataSensorCal as (\n" +
                    "Select TG.Well_Num\n" +
                    ",CAST(TG.TIMEPOINT / 5 AS BIGINT) AS timepoint_group\n" +
                    ",TG.Timepoint\n" +
                    ",TG.hole_depth_feet\n" +
                    ",TG.bit_depth_feet\n" +
                    ", Case When TG.TimeDataTimepoint is not null and sc.timepoint IS NULL Then 'Insuf. DQ' WHEN sc.timepoint IS NOT NULL AND TG.rig_status_text='Rig Status' then sc.rig_status_text ELSE TG.rig_status_text END as rig_status_text\n" +
                    ",Case When sc.timepoint IS NULL Then 21 WHEN sc.timepoint IS NOT NULL AND TG.rig_status_text='Rig Status' then sc.rig_status_id ELSE TG.rig_status_id END as rig_status_id \n" +
                    "FROM TimeDataGen TG \n" +
                    "LEFT OUTER JOIN ptendrilling.ptendrilling_sensordatacalculations sc \n" +
                    "on TG.timepoint=sc.Timepoint \n" +
                    "and sc.Well_Num=" + wellNum + " \n" +
                    "and sc.timepoint>=" + startTime + " \n" +
                    "and sc.timepoint<" + endTime + "\n" +
                    ")\n" +
                    "SELECT " + wellNum + " as WELL_NUM\n" +
                    ",SC.timepoint_group,MIN(SC.TIMEPOINT) as EPOCTIME" +
                    ",FROM_UNIXTIME(MIN(SC.TIMEPOINT),'HH:mm') AS TIMEPOINT" +
                    ",CAST(AVG(SC.bit_depth_feet) AS DECIMAL(10,3)) as bit_depth_feet\n" +
                    ",CAST(AVG(SC.hole_depth_feet) AS DECIMAL(10,3)) as hole_depth_feet\n" +
                    ",'#ff0000' AS ColorProperty\n" +
                    "FROM TimeDataSensorCal SC inner join user_Security.user_mapping_details u on  SC.well_num=u.well_num AND u.userid = " + getUser_id() + " \n" +
                    "GROUP BY SC.timepoint_group \n" +
                    "order by SC.timepoint_group";

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            total = System.currentTimeMillis() - start;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;
    }

    public Collection getWellsCmrDailyDepth(Collection<Long> wells) {
        String query = "WITH WellAliases AS (\n" +
                "SELECT a.well_num\n" +
                "                , a.well_name\n" +
                "                , CONCAT('Well ', CAST(ROW_NUMBER() OVER(ORDER BY a.release_time DESC) AS STRING)) AS well_alias\n" +
                "  FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_Details u on a.well_num=u.well_num and u.userid=" + getUser_id() + " \n" +
                "WHERE a.well_num IN (:wells)\n" +
                "   AND NOT EXISTS (\n" +
                "                SELECT NULL\n" +
                "                  FROM pten_cmr.cmr_data_usalt7 z inner join user_Security.user_mapping_Details u on z.well_num=u.well_num and u.userid=" + getUser_id() + " \n" +
                "                 WHERE a.well_num = z.well_num\n" +
                "                   AND a.report_date < z.report_date)\n" +
                ")SELECT a.well_num\n" +
                "                , a.well_name\n" +
                "                , wa.well_alias" +
                "                , ROW_NUMBER() OVER(PARTITION BY a.well_num ORDER BY a.report_date) AS day_num\n" +
                "                , COALESCE(LAG(a.current_depth, 1) OVER(PARTITION BY a.well_num ORDER BY a.report_date), 0.0) AS day_depth\n" +
                "  FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_Details u on a.well_num=u.well_num and u.userid=" + getUser_id() + " \n" +
                "       INNER JOIN WellAliases wa ON a.well_num = wa.well_num\n" +
                "WHERE a.current_depth > 0.0";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells);

        return impalaDbUtil.executeNamedQuery(query, namedParameters);
    }

    public Collection DrillingTrendROP(String wellnum, String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {
            long wellNum = Long.parseLong(wellnum);
            long endTime = DateTimeUtils.dateToEpoch(endtime + " 06:00");
            long startTime = endTime - 86400;
            long k = System.currentTimeMillis();
            String query = "WITH NOCONN AS (\n" +
                    "SELECT sdc.well_num \n" +
                    " ,sdc.timepoint\n" +
                    " ,sdc.hole_depth_segment_id\n" +
                    " ,sdc.actual_rop\n" +
                    " ,sdc.weight_on_bit\n" +
                    " ,sdc.standpipe_pressure \n" +
                    " ,SUM(CASE WHEN sdc.Connection_Events IS NOT NULL THEN 1 ELSE 0 END) OVER(PARTITION BY sdc.well_num ORDER BY sdc.well_num, sdc.timepoint ROWS BETWEEN 15 PRECEDING AND 15 FOLLOWING) AS connection_window \n" +
                    "FROM ptendrilling.ptendrilling_sensordatacalculations sdc \n" +
                    "inner join user_Security.user_mapping_details u on  sdc.well_num = u.well_num AND u.userid = " + getUser_id() + " \n" +
                    "WHERE sdc.tight_bottom = 1 \n" +
                    "and sdc.WELL_NUM=" + wellNum + " \n" +
                    "AND sdc.TIMEPOINT >=" + startTime + " \n" +
                    "AND sdc.TIMEPOINT<" + endTime + "\n" +
                    ")\n" +
                    "SELECT NC.hole_depth_segment_id as TIMEPOINT,\n" +
                    "Case When NC.hole_depth_segment_id<1000 then cast(NC.hole_depth_segment_id as string) when NC.hole_depth_segment_id>=1000 and NC.hole_depth_segment_id<1000000 then concat(cast(cast(NC.hole_depth_segment_id/1000 as decimal(10,1)) as string),'K') else cast(NC.hole_depth_segment_id as string) END AS hole_depth_segment_id\n" +
                    ",Cast(AVG(NC.actual_rop) as Decimal(10,2)) as ROP\n" +
                    ",Cast(AVG(NC.weight_on_bit) as Decimal(10,2)) as WOB \n" +
                    "FROM NOCONN  NC \n" +
                    "WHERE NC.connection_window=0 \n" +
                    "and NC.weight_on_bit>=3.0 \n" +
                    "and NC.standpipe_pressure>=285.00 \n" +
                    "group by NC.hole_depth_segment_id\n" +
                    ",Case When NC.hole_depth_segment_id<1000 then cast(NC.hole_depth_segment_id as string) when NC.hole_depth_segment_id>=1000 and NC.hole_depth_segment_id<1000000 then concat(cast(cast(NC.hole_depth_segment_id/1000 as decimal(10,1)) as string),'K') else cast(NC.hole_depth_segment_id as string) END \n" +
                    "ORDER BY NC.hole_depth_segment_id ASC;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            total = System.currentTimeMillis() - start;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        log.info("Response Sent " + fetchingtime + " " + total);
        return myJSONArray;
    }

    public Collection DrillingTrendRPM(String wellnum, String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {
            long wellNum = Long.parseLong(wellnum);
            long endTime = DateTimeUtils.dateToEpoch(endtime + " 06:00");
            long startTime = endTime - 86400;
            long k = System.currentTimeMillis();
            String query = "WITH NOCONN AS (\n" +
                    "SELECT sdc.well_num \n" +
                    ",sdc.timepoint\n" +
                    ",sdc.hole_depth_segment_id\n" +
                    ",sdc.weight_on_bit\n" +
                    ",sdc.standpipe_pressure\n" +
                    ",sdc.rotary_rpm\n" +
                    ",sdc.convertible_torque \n" +
                    ",SUM(CASE WHEN sdc.Connection_Events IS NOT NULL THEN 1 ELSE 0 END) OVER(PARTITION BY sdc.well_num ORDER BY sdc.well_num, sdc.timepoint ROWS BETWEEN 15 PRECEDING AND 15 FOLLOWING) AS connection_window \n" +
                    "FROM ptendrilling.ptendrilling_sensordatacalculations sdc \n" +
                    "inner join user_Security.user_mapping_details u on  sdc.well_num = u.well_num AND u.userid = " + getUser_id() + " \n" +
                    "WHERE sdc.tight_bottom = 1 and sdc.WELL_NUM=" + wellNum + " \n" +
                    " AND sdc.TIMEPOINT >=" + startTime + "\n" +
                    " AND sdc.TIMEPOINT<" + endTime + ")\n" +
                    "SELECT NC.hole_depth_segment_id AS TIMEPOINT\n" +
                    ",Case When NC.hole_depth_segment_id<1000 then cast(NC.hole_depth_segment_id as string) when NC.hole_depth_segment_id>=1000 and NC.hole_depth_segment_id<1000000 then concat(cast(cast(NC.hole_depth_segment_id/1000 as decimal(10,1)) as string),'K') else cast(NC.hole_depth_segment_id as string) END AS hole_depth_segment_id\n" +
                    ",Cast(AVG(NC.rotary_rpm) as Decimal(10,2)) as RPM\n" +
                    ",Cast(AVG(NC.convertible_torque) as Decimal(10,2)) as TORQUE\n" +
                    "FROM NOCONN NC \n" +
                    "WHERE NC.connection_window = 0 \n" +
                    "and NC.weight_on_bit >= 3.0 \n" +
                    "and NC.standpipe_pressure >= 285.00 \n" +
                    "group by NC.hole_depth_segment_id\n" +
                    ",Case When NC.hole_depth_segment_id<1000 then cast(NC.hole_depth_segment_id as string) when NC.hole_depth_segment_id>=1000 and NC.hole_depth_segment_id<1000000 then concat(cast(cast(NC.hole_depth_segment_id/1000 as decimal(10,1)) as string),'K') else cast(NC.hole_depth_segment_id as string) END \n" +
                    "ORDER BY NC.hole_depth_segment_id ASC;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        log.info("Response Sent " + fetchingtime + " " + total);
        return myJSONArray;
    }

    public Collection DrillingTrendSPP(String wellnum, String starttime, String endtime) {
        Collection myJSONArray = null;

        try {
            long wellNum = Long.parseLong(wellnum);
            long endTime = DateTimeUtils.dateToEpoch(endtime + " 06:00");
            long startTime = endTime - 86400;
            long k = System.currentTimeMillis();
            String query = "WITH NOCONN AS (\n" +
                    "SELECT sdc.well_num \n" +
                    "   ,sdc.timepoint\n" +
                    "   , sdc.hole_depth_segment_id\n" +
                    "   ,sdc.total_pump_output_gpm\n" +
                    "   ,sdc.standpipe_pressure\n" +
                    "   ,sdc.weight_on_bit \n" +
                    "   ,SUM(CASE WHEN sdc.Connection_Events IS NOT NULL THEN 1 ELSE 0 END) OVER(PARTITION BY sdc.well_num ORDER BY sdc.well_num, sdc.timepoint ROWS BETWEEN 15 PRECEDING AND 15 FOLLOWING) AS connection_window \n" +
                    "FROM ptendrilling.ptendrilling_sensordatacalculations sdc \n" +
                    "inner join user_Security.user_mapping_details u on  sdc.well_num = u.well_num AND u.userid = " + getUser_id() + " \n" +
                    "WHERE sdc.tight_bottom = 1 \n" +
                    "   and sdc.WELL_NUM=" + wellNum + "\n" +
                    "   AND sdc.TIMEPOINT >=" + startTime + "\n" +
                    "   AND sdc.TIMEPOINT<" + endTime + "\n" +
                    ")\n" +
                    "SELECT NC.hole_depth_segment_id AS TIMEPOINT\n" +
                    "   ,Case When NC.hole_depth_segment_id<1000 then cast(NC.hole_depth_segment_id as string) when NC.hole_depth_segment_id>=1000 and NC.hole_depth_segment_id<1000000 then concat(cast(cast(NC.hole_depth_segment_id/1000 as decimal(10,1)) as string),'K') else cast(NC.hole_depth_segment_id as string) END AS hole_depth_segment_id\n" +
                    "   ,Cast(AVG(NC.standpipe_pressure) as Decimal(10,2)) as SPP\n" +
                    "   ,Cast(AVG(NC.total_pump_output_gpm) as Decimal(10,2)) as PTSR\n" +
                    "FROM NOCONN NC \n" +
                    "WHERE NC.connection_window = 0 \n" +
                    "   and NC.weight_on_bit >= 3.0 \n" +
                    "   and NC.standpipe_pressure >= 285.00 \n" +
                    "group by NC.hole_depth_segment_id\n" +
                    ",Case When NC.hole_depth_segment_id<1000 then cast(NC.hole_depth_segment_id as string) when NC.hole_depth_segment_id>=1000 and NC.hole_depth_segment_id<1000000 then concat(cast(cast(NC.hole_depth_segment_id/1000 as decimal(10,1)) as string),'K') else cast(NC.hole_depth_segment_id as string) END \n" +
                    "ORDER BY NC.hole_depth_segment_id ASC;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return myJSONArray;
    }

    public Collection DrillingTrendHKL(String wellnum, String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {
            long wellNum = Long.parseLong(wellnum);
            long endTime = DateTimeUtils.dateToEpoch(endtime + " 06:00");
            long startTime = endTime - 86400;
            long k = System.currentTimeMillis();
            String query = "WITH NOCONN AS (\n" +
                    "SELECT sdc.well_num \n" +
                    "   ,sdc.timepoint\n" +
                    "   , sdc.hole_depth_segment_id\n" +
                    "   ,sdc.actual_rop\n" +
                    "   ,sdc.weight_on_bit\n" +
                    "   ,sdc.differential_pressure\n" +
                    "   ,sdc.standpipe_pressure \n" +
                    "   ,SUM(CASE WHEN sdc.Connection_Events IS NOT NULL THEN 1 ELSE 0 END) OVER(PARTITION BY sdc.well_num ORDER BY sdc.well_num, sdc.timepoint ROWS BETWEEN 15 PRECEDING AND 15 FOLLOWING) AS connection_window \n" +
                    "FROM ptendrilling.ptendrilling_sensordatacalculations sdc \n" +
                    "inner join user_Security.user_mapping_details u on  sdc.well_num = u.well_num AND u.userid = " + getUser_id() + " \n" +
                    "WHERE sdc.tight_bottom = 1 \n" +
                    "   and sdc.WELL_NUM=" + wellNum + "\n" +
                    "   AND sdc.TIMEPOINT >=" + startTime + "\n" +
                    "   AND sdc.TIMEPOINT<" + endTime + "\n" +
                    ")\n" +
                    "SELECT NC.hole_depth_segment_id AS TIMEPOINT" +
                    ",Case When NC.hole_depth_segment_id<1000 then cast(NC.hole_depth_segment_id as string) when NC.hole_depth_segment_id>=1000 and NC.hole_depth_segment_id<1000000 then concat(cast(cast(NC.hole_depth_segment_id/1000 as decimal(10,1)) as string),'K') else cast(NC.hole_depth_segment_id as string) END AS hole_depth_segment_id\n" +
                    ",Cast(AVG(NC.differential_pressure) as Decimal(10,2)) as DPSI\n" +
                    ",Cast(AVG(NC.actual_rop) as Decimal(10,2)) as ROP\n" +
                    "FROM NOCONN NC \n" +
                    "WHERE NC.connection_window = 0 \n" +
                    "   and NC.weight_on_bit >= 3.0 \n" +
                    "   and NC.standpipe_pressure >= 285.00 \n" +
                    "group by NC.hole_depth_segment_id\n" +
                    ",Case When NC.hole_depth_segment_id<1000 then cast(NC.hole_depth_segment_id as string) when NC.hole_depth_segment_id>=1000 and NC.hole_depth_segment_id<1000000 then concat(cast(cast(NC.hole_depth_segment_id/1000 as decimal(10,1)) as string),'K') else cast(NC.hole_depth_segment_id as string) END \n" +
                    "ORDER BY NC.hole_depth_segment_id ASC;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            total = System.currentTimeMillis() - start;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        log.info("Response Sent " + fetchingtime + " " + total);
        return myJSONArray;
    }

    public Collection DrillingTrend(String wellnum, String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {
            long wellNum = Long.parseLong(wellnum);
            long endTime = DateTimeUtils.dateToEpoch(endtime + " 06:00");
            long startTime = endTime - 86400;
            long k = System.currentTimeMillis();
            String query = "SELECT\n" +
                    "WELL_NUM,\n" +
                    "starting_bit_depth AS TIMEPOINT,\n" +
                    "ROP,\n" +
                    "RPM,\n" +
                    "SPP,\n" +
                    "TORQUE,\n" +
                    "PTSR,\n" +
                    "HKL,\n" +
                    "WOB\n" +
                    "FROM\n" +
                    "t_welldata_calculations\n" +
                    "WHERE WELL_NUM=" + wellNum + " AND TIMEPOINT >=" + startTime + " AND TIMEPOINT<" + endTime + "\n" +
                    " ;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            total = System.currentTimeMillis() - start;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        log.info("Response Sent " + fetchingtime + " " + total);
        return myJSONArray;
    }

    public Collection totalRigStateBreakDown(String wellnum, String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {
            long wellNum = Long.parseLong(wellnum);
            long endTime = DateTimeUtils.dateToEpoch(endtime + " 06:00");
            long startTime = endTime - 86400;
            long k = System.currentTimeMillis();
            String query = "With TimeGenerator as (\n" +
                    "SELECT " + startTime + "+ID as Timepoint  \n" +
                    "   from ptendrilling.ptendrilling_timeseq timeseq\n" +
                    "), TimeDataGen as (\n" +
                    "SELECT " + wellNum + " as WELL_NUM\n" +
                    "   ,g.timepoint\n" +
                    "   ,t.timepoint as TimeDatatimepoint\n" +
                    "   ,t.hole_depth_feet\n" +
                    "   ,t.bit_depth_feet\n" +
                    "   ,Case when t.timepoint IS NULL THEN 'No Data' ELSE 'Rig Status' END AS rig_status_text\n" +
                    "   ,Case when t.timepoint IS NULL THEN  20 ELSE -1 END AS rig_status_id \n" +
//                    "FROM TimeGenerator g inner join user_Security.user_mapping_details u on  well_num = u.well_num AND u.userid = " + getUser_id() + " \n" +
                    "FROM TimeGenerator g \n" +
                    "LEFT OUTER JOIN ptendrilling.ptendrilling_timedata t \n" +
                    "   on g.timepoint = t.timepoint \n" +
                    "   and t.well_num=" + wellNum + " \n" +
                    "   and t.timepoint>=" + startTime + " \n" +
                    "   and t.timepoint<" + endTime + "\n" +
                    "), TimeDataSensorCal as (\n" +
                    "Select TG.Well_Num\n" +
                    "   ,CAST(TG.TIMEPOINT / 5 AS BIGINT) AS timepoint_group\n" +
                    "   ,TG.Timepoint\n" +
                    "   ,TG.hole_depth_feet\n" +
                    "   ,TG.bit_depth_feet\n" +
                    "   ,Case When TG.TimeDatatimepoint is not null and sc.timepoint IS NULL Then 'Insuf. DQ' WHEN sc.timepoint IS NOT NULL AND TG.rig_status_text='Rig Status' then sc.rig_status_text ELSE TG.rig_status_text END as rig_status_text\n" +
                    "   ,Case When sc.timepoint IS NULL Then 21 WHEN sc.timepoint IS NOT NULL AND TG.rig_status_text='Rig Status' then sc.rig_status_id ELSE TG.rig_status_id END as rig_status_id \n" +
                    "FROM TimeDataGen TG \n" +
                    "   LEFT OUTER JOIN ptendrilling.ptendrilling_sensordatacalculations sc \n" +
                    "       on TG.timepoint = sc.Timepoint \n" +
                    "       and sc.Well_Num=" + wellNum + " \n" +
                    "       and sc.timepoint>=" + startTime + " \n" +
                    "       and sc.timepoint<" + endTime + "" +
                    ")" +
                    "SELECT w.RIG_STATUS_TEXT  AS RIG_STATUS,\n" +
                    "   SUM(1) as TIMEINSECONDS,\n" +
                    "   CASE WHEN w.RIG_STATUS_TEXT='No Data' THEN '#ECECEC'\n" +
                    "   WHEN w.RIG_STATUS_TEXT='Insuf. DQ' THEN '#C8C8C8'\n" +
                    "   WHEN w.RIG_STATUS_TEXT='Rig U/D' THEN '#499894'  \n" +
                    "   WHEN w.RIG_STATUS_TEXT='Nipple U/D' THEN '#FF909A'  \n" +
                    "   WHEN w.RIG_STATUS_TEXT='Test BOP' THEN '#F1CE63'  \n" +
                    "   WHEN w.RIG_STATUS_TEXT='NPT Repair' THEN '#B07AA1'  \n" +
                    "   WHEN w.RIG_STATUS_TEXT='In Slips' THEN '#464646'  \n" +
                    "   WHEN w.RIG_STATUS_TEXT='Rot. Drill' THEN '#000099'  \n" +
                    "   WHEN w.RIG_STATUS_TEXT='Sld. Drill' THEN '#006DFF'  \n" +
                    "   WHEN w.RIG_STATUS_TEXT='Ream In' THEN '#00D500'  \n" +
                    "   WHEN w.RIG_STATUS_TEXT='Ream Out' THEN '#2B7E12'  \n" +
                    "   WHEN w.RIG_STATUS_TEXT='Wash In' THEN '#FF9900'  \n" +
                    "   WHEN w.RIG_STATUS_TEXT='Wash Out' THEN '#FFFF00'  \n" +
                    "   WHEN w.RIG_STATUS_TEXT='Trip In' THEN '#D62728'  \n" +
                    "   WHEN w.RIG_STATUS_TEXT='Trip Out' THEN '#8F0000'  \n" +
                    "   WHEN w.RIG_STATUS_TEXT='Wait on Cem.' THEN '#A993AA'  \n" +
                    "   WHEN w.RIG_STATUS_TEXT='Cut Line' THEN '#000000'  \n" +
                    "   WHEN w.RIG_STATUS_TEXT='Circulate' THEN '#8000FF'  \n" +
                    "   WHEN w.RIG_STATUS_TEXT='Circ. w/Rot.' THEN '#CF9FFF'  \n" +
                    "   WHEN w.RIG_STATUS_TEXT='Other' THEN '#948A54'  \n" +
                    "   WHEN w.RIG_STATUS_TEXT='Cement' THEN '#b2b4b7'  \n" +
                    "   WHEN w.RIG_STATUS_TEXT='Run Casing' THEN  '#7A2C47' ELSE '#FFFFFF' END AS ColorProperty \n" +
                    "FROM TimeDataSensorCal w \n" +
                    "GROUP BY w.RIG_STATUS_TEXT\n" +
                    ",ColorProperty \n" +
                    "ORDER BY SUM(1) DESC;\n";

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        log.info("Response Sent " + fetchingtime + " " + total);
        return myJSONArray;
    }

    public Collection trippingSpeedFTHr(String wellnum, String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {
            long wellNum = Long.parseLong(wellnum);
            long endTime = DateTimeUtils.dateToEpoch(endtime + " 06:00");
            long startTime = endTime - 86400;
            long k = System.currentTimeMillis();

            String query = "WITH StandTTimes AS (\n" +
                    "   SELECT a.well_num\n" +
                    "       ,Stand_ID\n" +
                    "       ,stand_start_time\n" +
                    "       ,cast(Starting_Bit_Depth as int) as Starting_Bit_Depth \n" +
                    "       ,Stand_Length\n" +
                    "       ,In_Out\n" +
                    "       ,SUM(3600 * (a.stand_length / a.total_sec )) over (Partition by a.stand_id) as total_ft_per_hour\t\n" +
                    "       ,Footage_Drilled\n" +
                    "       ,CASE WHEN (count(*) OVER (PARTITION BY In_Out) > 50 ) THEN CASE WHEN  (CAST(count(*) OVER (PARTITION BY In_Out) / 50 AS INT)) > 1 then (ROW_NUMBER() OVER (Partition by In_Out ORDER BY Stand_ID) % (CAST(count(*) OVER (PARTITION BY In_Out) / 50 AS INT))) else 1 END ELSE 1 END as rowNumN\n" +
                    "       ,count(*) OVER (PARTITION BY In_Out) AS countINOUT \n" +
                    "   FROM ptendrilling.vw_wellstands_tripping a \n" +
                    "   inner join user_Security.user_mapping_details u on  a.well_num = u.well_num AND u.userid = " + getUser_id() + "\n" +
                    "   Where a.WELL_NUM =CAST( " + wellNum + " AS bigint) \n" +
                    "       and stand_start_time >=   " + startTime + "   \n" +
                    "       AND   stand_start_time < " + endTime + "  \n" +
                    ")\n" +
                    "SELECT well_num\n" +
                    "   ,Stand_ID\n" +
                    "   ,stand_start_time\n" +
                    "   ,Starting_Bit_Depth\n" +
                    "   ,Footage_Drilled\n" +
                    "   ,Stand_Length\n" +
                    "   ,In_Out   \n" +
                    "   ,cast(total_ft_per_hour as decimal(10,0)) as total_ft_per_hour\n" +
                    "   ,case when UPPER(In_Out)='IN' then cast(total_ft_per_hour as decimal(10,0)) ELSE NULL END AS in_ft_per_hour\n" +
                    "   ,case when UPPER(In_Out)='OUT' then cast(total_ft_per_hour as decimal(10,0)) ELSE NULL END AS out_ft_per_hour\n" +
                    "   ,cast(avg(total_ft_per_hour) over (partition by well_num) as decimal(10,0)) as avg_ft_per_hour\n" +
                    "   ,concat('<div style=\"padding:4px;color:white;background:#002060;position:relative;bottom:5px\">'\n" +
                    "       ,cast(cast(avg(total_ft_per_hour) over (partition by well_num) as decimal(10,0)) as string)\n" +
                    "       ,' Avg ft/hr</div>'\n" +
                    "   ) as avg_ft_per_hour_custom_label\n" +
                    "   ,case when UPPER(In_Out)='IN' then '#2B7E12C7' When Upper(In_Out)='OUT' Then '#006DFFC7' END as ColorProperty \n" +
                    "   ,rownumn\n" +
                    "   ,countinout\n" +
                    "FROM StandTTimes \n" +
                    "order by in_out desc, stand_id; ";

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        log.info("Response Sent " + fetchingtime + " " + total);
        return myJSONArray;
    }

    public Collection trippingConnectionTime(String wellnum, String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {
            long wellNum = Long.parseLong(wellnum);
            long endTime = DateTimeUtils.dateToEpoch(endtime + " 06:00");
            long startTime = endTime - 86400;
            long k = System.currentTimeMillis();
            String query = "WITH StandTTimes AS (\n" +
                    "SELECT a.well_num\n" +
                    "   ,Stand_ID\n" +
                    "   ,stand_start_time\n" +
                    "   ,cast(Starting_Bit_Depth as int) as Starting_Bit_Depth \n" +
                    "   ,Stand_Length\n" +
                    "   ,In_Out\n" +
                    "   ,sec_S2S\n" +
                    "   ,Footage_Drilled\n" +
                    "FROM ptendrilling.vw_wellstands_tripping a \n" +
                    "inner join user_Security.user_mapping_details u on  a.well_num = u.well_num AND u.userid = " + getUser_id() + "\n" +
                    "Where a.WELL_NUM =CAST( " + wellNum + " AS bigint) \n" +
                    "and stand_start_time >=  " + startTime + "\n" +
                    "AND   stand_start_time <= " + endTime + "\n" +
                    ")\n" +
                    "SELECT well_num\n" +
                    "   ,Stand_ID\n" +
                    "   ,stand_start_time\n" +
                    "   ,Starting_Bit_Depth\n" +
                    "   ,Footage_Drilled\n" +
                    "   ,Stand_Length\n" +
                    "   ,In_Out\n" +
                    "   ,Cast(sec_S2S/60 as Decimal(9,2)) as TimeInMinutes\n" +
                    "   ,case when upper(In_Out)='IN' then Cast(sec_S2S/60 as Decimal(9,2)) Else NULL END As in_timeinminutes\n" +
                    "   ,case when upper(In_Out)='OUT' then Cast(sec_S2S/60 as Decimal(9,2)) Else NULL END as out_timeinminutes\n" +
                    "   ,round(AVG(sec_S2S/60) over (Partition by well_num),2)  AS avg_in_Slips\n" +
                    "   ,concat('<div style=\"padding:4px;color:white;background:#002060;position:relative;bottom:5px\">'\n" +
                    "   ,cast(cast(AVG(sec_S2S/60) over (Partition by well_num) as decimal(10,2)) as string),' Avg In Slips</div>' ) as avg_in_Slips_custom_label\n" +
                    "   ,case when UPPER(In_Out)='IN' then '#2B7E12C7' When Upper(In_Out)='OUT' Then '#006DFFC7' END as ColorProperty\n" +
                    "FROM StandTTimes\n" +
                    "order by Stand_ID;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        log.info("Response Sent " + fetchingtime + " " + total);
        return myJSONArray;
    }

    public Collection drillingROPByStand(String wellnum, String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {
            long wellNum = Long.parseLong(wellnum);
            long endTime = DateTimeUtils.dateToEpoch(endtime + " 06:00");
            long startTime = endTime - 86400;
            long k = System.currentTimeMillis();

            String query = "WITH StandTTimes AS (\n" +
                    "SELECT a.well_num \n" +
                    "  ,Stand_ID \n" +
                    "  ,stand_start_time " +
                    "  ,cast(Starting_Bit_Depth as int) as Starting_Bit_Depth \n" +
                    "  ,Ending_Bit_Depth \n" +
                    "  ,Stand_Length \n" +
                    "  ,In_Out \n" +
                    "  ,SUM(3600 * (a.stand_length / a.total_sec )) over (Partition by a.stand_id) as total_ft_per_hour\n" +
                    "  ,avg_ROP \n" +
                    "  ,sec_W2S \n" +
                    "  ,sec_S2S \n" +
                    "  ,sec_S2W \n" +
                    "  ,case when rotary_footage + slide_footage !=0 Then rotary_footage/(slide_footage+rotary_footage) Else 0 END as PercentVal \n" +
                    "FROM ptendrilling.vw_wellstands_drilling a\n" +
                    "  inner join user_Security.user_mapping_details u on  a.well_num = u.well_num AND u.userid = " + getUser_id() + "\n" +
                    "Where a.well_num = CAST( " + wellNum + " AS bigint) \n" +
                    "  and stand_start_time >=  " + startTime + "  \n" +
                    "  AND   stand_start_time <  " + endTime + " \n" +
                    ")\n" +
                    "SELECT well_num \n" +
                    "  ,Stand_ID \n" +
                    "  ,stand_start_time \n" +
                    "  ,Starting_Bit_Depth \n" +
                    "  ,Stand_Length \n" +
                    "  ,In_Out \n" +
                    "  ,avg_ROP \n" +
                    "  ,case When PercentVal < .2 Then \"#ff4c00\" When PercentVal >= .2 AND PercentVal < .4 Then \"#e0997a\" When PercentVal >= .4 And PercentVal < .6 Then \"#cccccc\" When PercentVal >= .6 And PercentVal < .8 Then \"#7a7ab8\" Else \"#000099\" End As SeriesColor  \n" +
                    "FROM StandTTimes\n" +
                    "order by Stand_ID;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        log.info("Response Sent " + fetchingtime + " " + total);
        return myJSONArray;
    }

    public Collection w2WDrillingConnection(String wellnum, String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {
            long wellNum = Long.parseLong(wellnum);
            long endTime = DateTimeUtils.dateToEpoch(endtime + " 06:00");
            long startTime = endTime - 86400;
            long k = System.currentTimeMillis();
            String query = "WITH StandTTimes AS (\n" +
                    "SELECT a.well_num \n" +
                    "  ,cast(Starting_Hole_Depth as int) as Stand_ID \n" +
                    "  ,stand_start_time\n" +
                    "  ,cast(Starting_Hole_Depth as int) as Starting_Hole_Depth \n" +
                    "  ,Stand_Length \n" +
                    "  ,sec_W2S \n" +
                    "  ,sec_S2S \n" +
                    "  ,sec_S2W \n" +
                    ",SUM(3600 * (a.stand_length / a.total_sec )) over (Partition by a.stand_id) as total_ft_per_hour \n" +
                    "  ,case when sec_Rotating+sec_Sliding!=0 Then sec_Rotating/(sec_Sliding+sec_Rotating) Else 0 END as PercentVal    \n" +
                    "FROM ptendrilling.vw_wellstands_drilling a \n" +
                    "  inner join user_Security.user_mapping_details u on  a.well_num = u.well_num AND u.userid = " + getUser_id() + "\n" +
                    "Where a.well_num =CAST( " + wellNum + " AS bigint) \n" +
                    "  and stand_start_time >=  " + startTime + "\n" +
                    "  AND   stand_start_time <  " + endTime + "\n" +
                    ")" +
                    "SELECT well_num \n" +
                    "  ,Stand_ID \n" +
                    "  ,stand_start_time \n" +
                    "  ,Starting_Hole_Depth \n" +
                    "  ,Stand_Length \n" +
                    "  ,cast(sec_W2S/60 as decimal(10,1)) as sec_W2S \n" +
                    "  ,cast(sec_S2S/60 as decimal(10,1)) as sec_S2S \n" +
                    "  ,Cast(sec_S2W/60 as decimal(10,1)) as sec_S2W \n" +
                    "  ,cast((AVG((sec_W2S + sec_S2W + sec_S2S)) over (Partition by well_num))/60 as Decimal(10,1))  AS avg_W2W  \n" +
                    "  ,cast((AVG(sec_S2S) over (Partition by well_num))/60 as decimal(10,1))  AS avg_in_Slips \n" +
                    "  ,concat('<div style=\"padding:4px;color:white;background:#002060;position:relative;bottom:5px\">'\n" +
                    "    ,cast(cast((AVG(sec_S2S) over (Partition by well_num))/60 as decimal(10,1)) as string)" +
                    "    ,' Avg In Slips</div>') as Avg_In_Slips_Custom_Label\n" +
                    "  ,concat('<div style=\"padding:4px;color:white;background:#002060;position:relative;bottom:5px\">'\n" +
                    "    ,cast(cast((AVG((sec_W2S + sec_S2W + sec_S2S)) over (Partition by well_num))/60 as Decimal(10,1)) as string)" +
                    "    ,' Avg W2W</div>') as Avg_W2W_Custom_Label \n" +
                    "FROM StandTTimes  \n" +
                    "order by Stand_ID;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            total = System.currentTimeMillis() - start;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        log.info("Response Sent " + fetchingtime + " " + total);
        return myJSONArray;
    }

    public Collection rigStateBreakdownTrend(String wellnum, String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {

            long wellNum = Long.parseLong(wellnum);
            long endTime = DateTimeUtils.dateToEpoch(endtime + " 06:00");
            long startTime = endTime - 86400;
            long k = System.currentTimeMillis();
            String query = "With TimeGenerator as (\n" +
                    "SELECT " + startTime + "+ID as Timepoint  \n" +
                    "from ptendrilling.ptendrilling_timeseq timeseq\n" +
                    "), TimeDataGen as (\n" +
                    "SELECT " + wellNum + " as WELL_NUM\n" +
                    "   ,g.timepoint\n" +
                    "   ,t.timepoint as TimeDatatimepoint\n" +
                    "   ,t.hole_depth_feet\n" +
                    "   ,t.bit_depth_feet\n" +
                    "   ,Case when t.timepoint IS NULL THEN 'No Data' ELSE 'Rig Status' END AS rig_status_text\n" +
                    "   ,Case when t.timepoint IS NULL THEN  20 ELSE -1 END AS rig_status_id \n" +
                    "FROM TimeGenerator g " +
                    "LEFT OUTER JOIN ptendrilling.ptendrilling_timedata t \n" +
                    "   on g.timepoint=t.timepoint \n" +
                    "   and t.well_num=" + wellNum + " \n" +
                    "   and t.timepoint>=" + startTime + " \n" +
                    "   and t.timepoint<" + endTime + "\n" +
                    "), TimeDataSensorCal as (\n" +
                    "Select TG.Well_Num\n" +
                    "   ,CAST(TG.TIMEPOINT / 5 AS BIGINT) AS timepoint_group\n" +
                    "   ,TG.Timepoint\n" +
                    "   ,TG.hole_depth_feet\n" +
                    "   ,TG.bit_depth_feet\n" +
                    "   , Case When TG.TimeDatatimepoint is not null and sc.timepoint IS NULL Then 'Insuf. DQ' WHEN sc.timepoint IS NOT NULL AND TG.rig_status_text='Rig Status' then sc.rig_status_text ELSE TG.rig_status_text END as rig_status_text\n" +
                    "   ,Case When sc.timepoint IS NULL Then 21 WHEN sc.timepoint IS NOT NULL AND TG.rig_status_text='Rig Status' then sc.rig_status_id ELSE TG.rig_status_id END as rig_status_id \n" +
                    "FROM TimeDataGen TG \n" +
                    "inner join user_Security.user_mapping_details u on TG.well_num = u.well_num AND u.userid = " + getUser_id() + " \n" +
                    "LEFT OUTER JOIN ptendrilling.ptendrilling_sensordatacalculations sc \n" +
                    "   on TG.timepoint=sc.Timepoint \n" +
                    "   and sc.Well_Num=" + wellNum + " \n" +
                    "   and sc.timepoint>=" + startTime + " \n" +
                    "   and sc.timepoint<" + endTime + "\n" +
                    "), RIGSTATUS AS (\n" +
                    "SELECT RIG_STATUS_TEXT\n" +
                    "   ,UNIX_TIMESTAMP(FROM_UNIXTIME(TIMEPOINT,'yyyy-MM-dd HH'),'yyyy-MM-dd HH') AS FORMATEDDATE\n" +
                    "   ,TIMEPOINT\n" +
                    "   ,CASE WHEN RIG_STATUS_TEXT=LAG (RIG_STATUS_TEXT) OVER (PARTITION BY UNIX_TIMESTAMP(FROM_UNIXTIME(TIMEPOINT,'yyyy-MM-dd HH'),'yyyy-MM-dd HH') ORDER BY TIMEPOINT) THEN 0 ELSE 1 END AS FLAG \n" +
                    "FROM  TimeDataSensorCal\n" +
                    ") \n" +
                    "SELECT FORMATEDDATE*1000 as TIMEPOINT\n" +
                    "   ,TIMEPOINT AS SPLITRANK\n" +
                    "   ,RIG_STATUS_TEXT\n" +
                    "   ,(IFNULL(LEAD(TIMEPOINT, 1) OVER (PARTITION BY FORMATEDDATE ORDER BY TIMEPOINT),FORMATEDDATE+3600)-TIMEPOINT)/60  AS TIMEMINUTES\n" +
                    "   ,CASE WHEN RIG_STATUS_TEXT='No Data' THEN '#ECECEC'\n" +
                    "       WHEN RIG_STATUS_TEXT='Insuf. DQ' THEN '#C8C8C8'\n" +
                    "       WHEN RIG_STATUS_TEXT='Rig U/D' THEN '#499894'  \n" +
                    "       WHEN RIG_STATUS_TEXT='Nipple U/D' THEN '#FF909A'  \n" +
                    "       WHEN RIG_STATUS_TEXT='Test BOP' THEN '#F1CE63'  \n" +
                    "       WHEN RIG_STATUS_TEXT='NPT Repair' THEN '#B07AA1'  \n" +
                    "       WHEN RIG_STATUS_TEXT='In Slips' THEN '#464646'  \n" +
                    "       WHEN RIG_STATUS_TEXT='Rot. Drill' THEN '#000099'  \n" +
                    "       WHEN RIG_STATUS_TEXT='Sld. Drill' THEN '#006DFF'  \n" +
                    "       WHEN RIG_STATUS_TEXT='Ream In' THEN '#00D500'  \n" +
                    "       WHEN RIG_STATUS_TEXT='Ream Out' THEN '#2B7E12'  \n" +
                    "       WHEN RIG_STATUS_TEXT='Wash In' THEN '#FF9900'  \n" +
                    "       WHEN RIG_STATUS_TEXT='Wash Out' THEN '#FFFF00'  \n" +
                    "       WHEN RIG_STATUS_TEXT='Trip In' THEN '#D62728'  \n" +
                    "       WHEN RIG_STATUS_TEXT='Trip Out' THEN '#8F0000'  \n" +
                    "       WHEN RIG_STATUS_TEXT='Wait on Cem.' THEN '#A993AA'  \n" +
                    "       WHEN RIG_STATUS_TEXT='Cut Line' THEN '#000000'  \n" +
                    "       WHEN RIG_STATUS_TEXT='Circulate' THEN '#8000FF'  \n" +
                    "       WHEN RIG_STATUS_TEXT='Circ. w/Rot.' THEN '#CF9FFF'  \n" +
                    "       WHEN RIG_STATUS_TEXT='Other' THEN '#948A54'  \n" +
                    "       WHEN RIG_STATUS_TEXT='Cement' THEN '#b2b4b7'  \n" +
                    "       WHEN RIG_STATUS_TEXT='Run Casing' THEN  '#7A2C47' ELSE '#FFFFFF' END AS ColorProperty, concat(RIG_STATUS_TEXT,'_',CAST(TIMEPOINT AS VARCHAR)) as TEXTRANK \n" +
                    "FROM  RIGSTATUS \n" +
                    "WHERE FLAG=1 \n" +
                    "ORDER BY FORMATEDDATE\n" +
                    ",SPLITRANK; ";

            myJSONArray = myUtils.runSQLGetJSONArray(query);
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);

        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;
    }

    public Collection WellDetails(String wellnum) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {

            long wellNum = Long.parseLong(wellnum);

            long k = System.currentTimeMillis();

            String query = "WITH MaxChecker AS (\n" +
                    "  SELECT well_num\n" +
                    "    ,max(report_date) AS compare_date\n" +
                    "  from pten_cmr.cmr_data_usalt7\n" +
                    "  GROUP BY well_num\n" +
                    ")\n" +
                    "SELECT cast(q.well_num AS BIGINT) AS well_num\n" +
                    "  ,q.well_name, \n" +
                    "  Concat('RIG ',q.rig_name) as Rig_Name\n" +
                    "  ,q.operator_name\n" +
                    "  ,q.county\n" +
                    "  ,q.state_province\n" +
                    "  ,q.country \n" +
                    "FROM MaxChecker d \n" +
                    "  inner join pten_cmr.cmr_data_usalt7 q ON d.well_num = q.well_num\n" +
                    "  inner join user_Security.user_mapping_Details u on q.well_num = u.well_num and u.userid = " + getUser_id() +
                    "    and q.report_date = d.compare_date\n" +
                    "    where q.Well_Num =" + wellNum + ";";

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();


            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);

        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;
    }

    public Collection wellParamList(String rigname, String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {
            long endTime = DateTimeUtils.dateToEpoch(endtime + " 06:00");
            long startTime = endTime - 86400;

            long k = System.currentTimeMillis();
            String query = "select distinct w.well_num,w.well_name from ptendrilling.ptendrilling_timedata t join ptendrilling.ptendrilling_welldata w on t.well_num=w.well_num and t.timepoint >=" + startTime + " and t.timepoint<=" + endTime + " where w.rig_name='" + rigname + "' order by well_num;";
            myJSONArray = myUtils.runSQLGetJSONArray(query);

            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);

        }

        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;
    }

    public Collection rigParamList() {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {


            long k = System.currentTimeMillis();
            String query = "select distinct rig_name from ptendrilling.ptendrilling_welldata order by rig_name;";
            myJSONArray = myUtils.runSQLGetJSONArray(query);
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);

        }

        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;
    }

    public Collection RigCMRPercentByMonth(String rigname, String starttime, String endtime, String subCodeText) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {

            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            long k = System.currentTimeMillis();

            String query = "WITH TopLevel AS ( \n" +
                    "SELECT a.rig_name \n" +
                    "\t , YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END)) AS npt_year \n" +
                    "\t , MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END)) AS npt_month \n" +
                    "\t , SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%Z%' " + ((subCodeText != null) ? " AND c.display_text = '" + subCodeText + "' \n" : "") + " THEN b.hours ELSE 0 END) OVER(PARTITION BY YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END)), MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END))) / SUM(b.hours) OVER(PARTITION BY YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END)), MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END))) AS npt_percentage \n" +
                    "\t , SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%Z%' " + ((subCodeText != null) ? " AND c.display_text = '" + subCodeText + "' \n" : "") + " THEN b.hours ELSE 0 END) OVER(PARTITION BY YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END))) / SUM(b.hours) OVER(PARTITION BY YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END))) AS annual_npt_percentage \n" +
                    "\t , SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%Z%' " + ((subCodeText != null) ? " AND c.display_text = '" + subCodeText + "' \n" : "") + " THEN b.hours ELSE 0 END) OVER(PARTITION BY a.rig_name) / SUM(b.hours) OVER(PARTITION BY a.rig_name) AS overall_npt_percentage \n" +
                    "  FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_Details u on  a.well_num=u.well_num \n" +
                    "\tLEFT OUTER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                    "   LEFT OUTER JOIN pten_cmr.CMRDecode c ON b.code = c.cmr_code AND split_part(b.sub_code, '.', 2) = c.cmr_sub_code AND c.cmr_sub_sub_code IS NULL\n" +
                    " WHERE u.userid=" + getUser_id() + " and a.rig_name = '" + rigname + "' AND (b.`from` BETWEEN " + startTime + " AND " + endTime + " OR b.`to` BETWEEN " + startTime + " AND " + endTime + ")) \n" +
                    "SELECT DISTINCT rig_name\n" +
                    "        , npt_year, npt_month\n" +
                    "        , CASE npt_month WHEN 1 THEN 'January' WHEN 2 THEN 'February' WHEN 3 THEN 'March' WHEN 4 THEN 'April' WHEN 5 THEN 'May' WHEN 6 THEN 'June' WHEN 7 THEN 'July' WHEN 8 THEN 'August' WHEN 9 THEN 'September' WHEN 10 THEN 'October' WHEN 11 THEN 'November' WHEN 12 THEN 'December' END AS month_long_text, CASE npt_month WHEN 1 THEN 'Jan' WHEN 2 THEN 'Feb' WHEN 3 THEN 'Mar' WHEN 4 THEN 'Apr' WHEN 5 THEN 'May' WHEN 6 THEN 'Jun' WHEN 7 THEN 'Jul' WHEN 8 THEN 'Aug' WHEN 9 THEN 'Sep' WHEN 10 THEN 'Oct' WHEN 11 THEN 'Nov' WHEN 12 THEN 'Dec' END AS month_short_text\n" +
                    "        , npt_percentage\n" +
                    "        , annual_npt_percentage\n" +
                    "        , overall_npt_percentage \n" +
                    "FROM TopLevel WHERE NPT_PERCENTAGE != 0 ORDER BY npt_year, npt_month;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();


            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);

        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;
    }

    //prepared statement example
    //optional parameters with in clause (list ) sample
    //Prepared statement example. redundant way of entering repeaded parameters
    public Collection RigCMRPercentByMonth(String rigname, List<Long> wells, String starttime, String endtime) {
        Collection myJSONArray = new ArrayList();
        try {

            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            StringBuilder query = new StringBuilder("WITH TopLevel AS ( \n" +
                    "SELECT a.rig_name \n" +
                    ", a.well_num \n" +
                    "\t , YEAR(to_timestamp(CASE WHEN b.`from` < ? THEN b.`to` ELSE b.`from` END)) AS npt_year \n" +
                    "\t , MONTH(to_timestamp(CASE WHEN b.`from` < ? THEN b.`to` ELSE b.`from` END)) AS npt_month \n" +
                    "\t , SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%Z%'  THEN b.hours ELSE 0 END) OVER(PARTITION BY YEAR(to_timestamp(CASE WHEN b.`from` < ? THEN b.`to` ELSE b.`from` END)), MONTH(to_timestamp(CASE WHEN b.`from` < ? THEN b.`to` ELSE b.`from` END))) / SUM(b.hours) OVER(PARTITION BY YEAR(to_timestamp(CASE WHEN b.`from` < ? THEN b.`to` ELSE b.`from` END)), MONTH(to_timestamp(CASE WHEN b.`from` < ? THEN b.`to` ELSE b.`from` END))) AS npt_percentage \n" +
                    "\t , SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%Z%'  THEN b.hours ELSE 0 END) OVER(PARTITION BY YEAR(to_timestamp(CASE WHEN b.`from` < ? THEN b.`to` ELSE b.`from` END))) / SUM(b.hours) OVER(PARTITION BY YEAR(to_timestamp(CASE WHEN b.`from` < ? THEN b.`to` ELSE b.`from` END))) AS annual_npt_percentage \n" +
                    " \t , SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%Z%'  THEN b.hours ELSE 0 END) OVER(PARTITION BY a.rig_name) / SUM(b.hours) OVER(PARTITION BY a.rig_name) AS overall_npt_percentage\n" +
                    "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_Details u on  a.well_num=u.well_num  \n" +
                    "\tLEFT OUTER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                    "   \tLEFT OUTER JOIN pten_cmr.CMRDecode c ON b.code = c.cmr_code AND split_part(b.sub_code, '.', 2) = c.cmr_sub_code AND c.cmr_sub_sub_code IS NULL\n" +
                    "WHERE u.userid=" + getUser_id() + " and a.rig_name = ? AND (b.`from` BETWEEN ? AND ? OR b.`to` BETWEEN ? AND ?)\n" +
                    "and a.well_num in ( ")

                    .append(myUtils.inClauseParams(wells.size()))

                    .append(") \n" +
                            ") \n" +
                            "SELECT DISTINCT rig_name\n" +
                            "\t, well_num\n" +
                            "\t, npt_year\n" +
                            "\t, npt_month\n" +
                            "\t, CASE npt_month WHEN 1 THEN 'January' WHEN 2 THEN 'February' WHEN 3 THEN 'March' WHEN 4 THEN 'April' WHEN 5 THEN 'May' WHEN 6 THEN 'June' WHEN 7 THEN 'July' WHEN 8 THEN 'August' WHEN 9 THEN 'September' WHEN 10 THEN 'October' WHEN 11 THEN 'November' WHEN 12 THEN 'December' END AS month_long_text\n" +
                            "\t, CASE npt_month WHEN 1 THEN 'Jan' WHEN 2 THEN 'Feb' WHEN 3 THEN 'Mar' WHEN 4 THEN 'Apr' WHEN 5 THEN 'May' WHEN 6 THEN 'Jun' WHEN 7 THEN 'Jul' WHEN 8 THEN 'Aug' WHEN 9 THEN 'Sep' WHEN 10 THEN 'Oct' WHEN 11 THEN 'Nov' WHEN 12 THEN 'Dec' END AS month_short_text\n" +
                            "\t, npt_percentage\n" +
                            "\t, annual_npt_percentage\n" +
                            "\t, overall_npt_percentage \n" +
                            "FROM TopLevel \n" +
                            "ORDER BY npt_year\n" +
                            "\t, npt_month;");

            ArrayList params = new ArrayList();
            for (int i = 0; i < 8; i++) {
                params.add(startTime);
            }

            params.add(rigname);
            params.add(startTime);
            params.add(endTime);
            params.add(startTime);
            params.add(endTime);
            params.add(wells);

            myJSONArray = myUtils.executePreparedStatement(jdbcTemplate.getDataSource().getConnection(), query.toString(), params);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);

        }
        return myJSONArray;
    }

    public Collection getLastCompletedWellsHardcoded(String rig, int limit) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(
                "with LastCompletedWells as (\n" +
                        "SELECT a.well_num\n" +
                        "\t,a.rig_name\n" +
                        "from pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_Details u on  a.well_num=u.well_num \n" +
                        " WHERE u.userid=" + getUser_id() + " and a.release_time is not null\n" +
                        "order by a.release_time desc\n" +
                        ")\n" +
                        "select distinct well_num\n" +
                        "from LastCompletedWells\n" +
                        "where rig_name =:rig");
        if (limit > 0) {
            stringBuilder.append(" limit :limit");
        }
        stringBuilder.append(";");

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("rig", rig)
                .addValue("limit", limit);


        Collection<Well> wells = namedParameterJdbcTemplate.query(
                stringBuilder.toString(),
                namedParameters,
                (rs, rowNum) -> new Well(
                        //rs.getString("well_num")
                )).stream().collect(toList());
        return wells;
    }


    public Collection RigCMRHoursByMonth(String rigname, String starttime, String endtime, String subCodeText) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {

            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            long k = System.currentTimeMillis();
            String query = "with topLevel as (\n" +
                    "SELECT a.rig_name \n" +
                    "   , YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END)) AS npt_year \n" +
                    "   , MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END)) AS npt_month \n" +
                    "   , b.code \n" +
                    "   , split_part(b.sub_code, '.', 2) AS sub_code \n" +
                    "   , SUM(CASE WHEN b.code = '8' THEN b.hours ELSE 0 END) AS npt_sub_code_hours \n" +
                    "   , c.display_color \n" +
                    "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_Details u on  a.well_num=u.well_num \n" +
                    "   LEFT OUTER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id AND b.code = '8' \n" +
                    "   LEFT OUTER JOIN pten_cmr.CMRDecode c ON b.code = c.cmr_code AND split_part(b.sub_code, '.', 2) = c.cmr_sub_code AND c.cmr_sub_sub_code IS NULL \n" +
                    " WHERE u.userid=" + getUser_id() + " and a.rig_name = '" + rigname + "' AND (b.`from` BETWEEN " + startTime + " AND " + endTime + " OR b.`to` BETWEEN " + startTime + " AND " + endTime + ") AND b.code = '8' AND b.sub_code NOT LIKE '%Z%' \n" +
                    ((subCodeText != null) ? " AND c.display_text = '" + subCodeText + "' \n" : "") +
                    " group by rig_name ,npt_year, npt_month, code, sub_code, display_color\n" +
                    " ) select distinct rig_name \n" +
                    "   ,npt_year\n" +
                    "   ,npt_sub_code_hours\n" +
                    "   , npt_month\n" +
                    "   ,CASE npt_month WHEN 1 THEN 'January' WHEN 2 THEN 'February' WHEN 3 THEN 'March' WHEN 4 THEN 'April' WHEN 5 THEN 'May' WHEN 6 THEN 'June' WHEN 7 THEN 'July' WHEN 8 THEN 'August' WHEN 9 THEN 'September' WHEN 10 THEN 'October' WHEN 11 THEN 'November' WHEN 12 THEN 'December' END AS month_long_text, CASE npt_month WHEN 1 THEN 'Jan' WHEN 2 THEN 'Feb' WHEN 3 THEN 'Mar' WHEN 4 THEN 'Apr' WHEN 5 THEN 'May' WHEN 6 THEN 'Jun' WHEN 7 THEN 'Jul' WHEN 8 THEN 'Aug' WHEN 9 THEN 'Sep' WHEN 10 THEN 'Oct' WHEN 11 THEN 'Nov' WHEN 12 THEN 'Dec' END AS month_short_text\n" +
                    "   , code \n" +
                    "   , sub_code \n" +
                    "   , display_color\n" +
                    " from topLevel ORDER BY npt_year,npt_month;";

            log.debug("{}", query);
            myJSONArray = myUtils.runSQLGetJSONArray(query);
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();


            total = System.currentTimeMillis() - start;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);

        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;
    }

    public Collection RigCMRHoursBySubCode(String rigname, String starttime, String endtime, String subCodeText) {
        Collection myJSONArray = null;
        try {

            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            String query =
                    "SELECT a.rig_name, b.code, split_part(b.sub_code, '.', 2) as sub_code, " +
                            "SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%Z%' THEN b.hours ELSE 0 END) as npt_sub_code_hours, " +
                            "cmrd.display_text AS npt_sub_code_text, " +
                            "cmrd.display_color AS npt_sub_code_color \n" +
                            "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_Details u on a.well_num=u.well_num " +
                            "left outer join pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id " +
                            "LEFT OUTER JOIN pten_cmr.CMRDecode cmrd ON b.code = cmrd.cmr_code and split_part(b.sub_code, '.', 2) = " +
                            "cmrd.cmr_sub_code and cmrd.cmr_sub_sub_code is null where u.userid=" + getUser_id() + " and  a.rig_name = '" + rigname + "' and (b.`from` between " + startTime + " and " + endTime + " OR b.`to` BETWEEN " + startTime + " AND " + endTime + ") and b.code = '8' AND b.sub_code NOT LIKE '%Z%' " +
                            ((subCodeText != null) ? " AND cmrd.display_text = '" + subCodeText + "' \n" : "") +
                            "group by a.rig_name, b.code, split_part(b.sub_code, '.', 2), cmrd.display_text, cmrd.display_color;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }

        return myJSONArray;
    }

    public Collection RigCMRDetailsBySubSubCode(String rigname, String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {

            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            long k = System.currentTimeMillis();
            String query =
                    "WITH TopTransform AS ( \n" +
                            "SELECT cmr_time_code_data_id \n" +
                            "	 , CONCAT(code, sub_code) AS full_code \n" +
                            "  FROM pten_cmr.cmr_time_code_data_usalt7 \n" +
                            "), TopMatch AS ( \n" +
                            "SELECT b.cmr_time_code_data_id \n" +
                            "	 , cmrd.display_text \n" +
                            "	 , 1 AS match_level \n" +
                            "  FROM TopTransform b  \n" +
                            "	INNER JOIN pten_cmr.CMRDecode cmrd ON split_part(b.full_code, '.', 1) = cmrd.cmr_code AND cmrd.cmr_sub_code IS NULL AND cmrd.cmr_sub_sub_code IS NULL \n" +
                            "), MiddleMatch AS ( \n" +
                            "SELECT b.cmr_time_code_data_id \n" +
                            "	 , cmrd.display_text \n" +
                            "	 , 2 AS match_level \n" +
                            "  FROM TopTransform b  \n" +
                            "	INNER JOIN pten_cmr.CMRDecode cmrd ON split_part(b.full_code, '.', 1) = cmrd.cmr_code AND split_part(b.full_code, '.', 2) = cmrd.cmr_sub_code AND cmrd.cmr_sub_sub_code IS NULL \n" +
                            "), BottomMatch AS ( \n" +
                            "SELECT b.cmr_time_code_data_id \n" +
                            "	 , cmrd.display_text \n" +
                            "	 , 3 AS match_level \n" +
                            "  FROM TopTransform b  \n" +
                            "	INNER JOIN pten_cmr.CMRDecode cmrd ON split_part(b.full_code, '.', 1) = cmrd.cmr_code AND split_part(b.full_code, '.', 2) = cmrd.cmr_sub_code AND split_part(b.full_code, '.', 3) = cmrd.cmr_sub_sub_code \n" +
                            ") \n" +
                            "SELECT from_timestamp(to_timestamp(a.report_date), 'MM/dd/yyyy') AS report_date \n" +
                            "	 , from_timestamp(to_timestamp(b.`from`), 'MM/dd/yy HH:mm') AS from_date \n" +
                            "	 , from_timestamp(to_timestamp(b.`to`), 'MM/dd/yy HH:mm') AS to_date \n" +
                            "	 , split_part(b.sub_code, '.', 2) AS sub_code \n" +
                            "	 , t2.display_text AS sub_code_text \n" +
                            "	 , split_part(b.sub_code, '.', 3) AS sub_sub_code \n" +
                            "	 , t3.display_text AS sub_sub_code_text \n" +
                            "	 , strleft(b.details, 50) AS details \n" +
                            "	 , b.hours \n" +
                            "  FROM pten_cmr.cmr_data_usalt7 a  inner join user_Security.user_mapping_Details u on a.well_num=u.well_num \n" +
                            "	INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                            "	INNER JOIN TopMatch t1 ON b.cmr_time_code_data_id = t1.cmr_time_code_data_id \n" +
                            "	LEFT OUTER JOIN MiddleMatch t2 ON b.cmr_time_code_data_id = t2.cmr_time_code_data_id \n" +
                            "	LEFT OUTER JOIN BottomMatch t3 ON b.cmr_time_code_data_id = t3.cmr_time_code_data_id \n" +
                            " WHERE u.userid=" + getUser_id() + " and a.rig_name = '" + rigname + "' \n" +
                            "   AND b.`from` BETWEEN " + startTime + " AND " + endTime + " \n" +
                            "   AND b.code = '8' \n" +
                            " ORDER BY a.report_date DESC \n" +
                            "        , b.`from` DESC;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();


            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);

        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;
    }

    public Collection NPTHoursByRegionSubCode(String starttime, String endtime) {
        Collection myJSONArray = null;

        // Performance Tracking
        long fetchingtime = 0L;
        long k = System.currentTimeMillis();

        try {
            // Date Conversion
            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            // Query and retrieval
            String query = "SELECT rm.region_name" +
                    ", cd.display_text" +
                    ", cd.display_color" +
                    ", SUM(b.hours) AS npt_hours " +
                    "FROM pten_cmr.cmr_data_usalt7 a " +
                    "inner join user_Security.user_mapping_Details u on a.well_num=u.well_num  " +
                    "INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id " +
                    "INNER JOIN pten_cmr.RegionMap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit) " +
                    "INNER JOIN pten_cmr.CMRDecode cd ON b.code = cd.cmr_code AND cd.cmr_sub_sub_code IS NULL AND split_part(b.sub_code, '.', 2) = cd.cmr_sub_code " +
                    "WHERE u.userid=" + getUser_id() + " and (b.`to` BETWEEN " + startTime + " AND " + endTime + " OR b.`from` BETWEEN " + startTime + " AND " + endTime + ") " +
                    "AND b.sub_code NOT LIKE '%z%' AND b.code = '8' GROUP BY rm.region_name, cd.display_text, cd.display_color;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            // More performance tracking
            fetchingtime = System.currentTimeMillis() - k;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }

        log.info("Response Sent" + fetchingtime);

        return myJSONArray;
    }

    public Collection NPTHoursByRegionSubCodeYQM(String starttime, String endtime) {
        Collection myJSONArray = null;

        // Performance Tracking
        long fetchingtime = 0L;
        long k = System.currentTimeMillis();

        try {
            // Date Conversion
            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            // Query and retrieval
            String query = "WITH TopLevel AS(\n" +
                    "SELECT \n" +
                    "\tcd.display_text\n" +
                    "\t, cd.display_color\n" +
                    "\t, YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN " + startTime + " ELSE b.`from` END)) AS npt_year\n" +
                    "\t, FLOOR((MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN " + startTime + " ELSE b.`from` END)) - 1) / 3) + 1 AS npt_qtr\n" +
                    "\t, MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN " + startTime + " ELSE b.`from` END)) AS npt_month\n" +
                    "\t, SUM(b.hours) AS npt_hours \n" +
                    "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_Details u on a.well_num=u.well_num \n" +
                    "\tINNER JOIN pten_cmr.cmr_time_code_data_usalt7 b \n" +
                    "\t\tON a.cmr_data_id = b.cmr_data_id \n" +
                    "\t\t\tINNER JOIN pten_cmr.CMRDecode cd \n" +
                    "\t\t\tON b.code = cd.cmr_code \n" +
                    "\t\t\tAND split_part(b.sub_code, '.', 2) = cd.cmr_sub_code \n" +
                    "\t\t\tAND cd.cmr_sub_sub_code IS NULL \n" +
                    " WHERE u.userid=" + getUser_id() + " and cd.cmr_code = '8' \n" +
                    "\tAND b.sub_code NOT LIKE '%z%' \n" +
                    "\tAND (b.`from` BETWEEN " + startTime + " AND " + endTime + " OR b.`to` BETWEEN " + startTime + " AND " + endTime + ")\n" +
                    "group by cd.display_text \n" +
                    "\t,cd.display_color\n" +
                    "\t,npt_year\n" +
                    "\t,npt_qtr\n" +
                    "\t,npt_month\n" +
                    ")\n" +
                    "SELECT distinct display_text \n" +
                    "\t,display_color\n" +
                    "\t,npt_year\n" +
                    "\t,npt_qtr\n" +
                    "\t,npt_month\n" +
                    "\t,CASE npt_month WHEN 1 THEN 'January' WHEN 2 THEN 'February' WHEN 3 THEN 'March' WHEN 4 THEN 'April' WHEN 5 THEN 'May' WHEN 6 THEN 'June' WHEN 7 THEN 'July' WHEN 8 THEN 'August' WHEN 9 THEN 'September' WHEN 10 THEN 'October' WHEN 11 THEN 'November' WHEN 12 THEN 'December' END AS month_long_text" +
                    "\t,CASE npt_month WHEN 1 THEN 'Jan' WHEN 2 THEN 'Feb' WHEN 3 THEN 'Mar' WHEN 4 THEN 'Apr' WHEN 5 THEN 'May' WHEN 6 THEN 'Jun' WHEN 7 THEN 'Jul' WHEN 8 THEN 'Aug' WHEN 9 THEN 'Sep' WHEN 10 THEN 'Oct' WHEN 11 THEN 'Nov' WHEN 12 THEN 'Dec' END AS month_short_text\n" +
                    "\t,CASE npt_qtr WHEN 1 THEN 'Q1' WHEN 2 THEN 'Q2' WHEN 3 THEN 'Q3' WHEN 4 THEN 'Q4' END AS npt_qtr_text\n" +
                    "\t,npt_hours" +
                    " FROM TopLevel ORDER BY npt_year, npt_month; ";

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            // More performance tracking
            fetchingtime = System.currentTimeMillis() - k;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }

        log.info("Response Sent" + fetchingtime);

        return myJSONArray;
    }

    public Collection NPTHoursByRYQM(String starttime, String endtime) {
        Collection myJSONArray = null;

        // Performance Tracking
        long fetchingtime = 0L;
        long k = System.currentTimeMillis();

        try {
            // Date Conversion
            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            // Query and retrieval
            String query = "--NPTHoursByRYQM\n" +
                    "WITH TopQuery AS (\n" +
                    "SELECT rm.region_name\n" +
                    ", YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN " + startTime + " ELSE b.`from` END)) AS npt_year\n" +
                    ", MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN " + startTime + " ELSE b.`from` END)) AS npt_month\n" +
                    ", FLOOR((MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN " + startTime + " ELSE b.`from` END)) - 1) / 3) + 1 AS npt_qtr\n" +
                    ", SUM(b.hours) AS npt_hours \n" +
                    "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                    "INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                    "INNER JOIN pten_cmr.RegionMap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                    "WHERE u.userid=" + getUser_id() + " and  b.code = '8' \n" +
                    "AND b.sub_code NOT LIKE '%z%' AND (b.`from` BETWEEN " + startTime + " AND " + endTime + " OR b.`to` BETWEEN " + startTime + " AND " + endTime + ") \n" +
                    "GROUP BY rm.region_name\n" +
                    ",npt_year\n" +
                    ",npt_month\n" +
                    ",npt_qtr\n" +
                    ")select region_name\n" +
                    ",npt_year\n" +
                    ",npt_month\n" +
                    ",npt_hours\n" +
                    ",npt_qtr\n " +
                    ",CASE npt_month WHEN 1 THEN 'January' WHEN 2 THEN 'February' WHEN 3 THEN 'March' WHEN 4 THEN 'April' WHEN 5 THEN 'May' WHEN 6 THEN 'June' WHEN 7 THEN 'July' WHEN 8 THEN 'August' WHEN 9 THEN 'September' WHEN 10 THEN 'October' WHEN 11 THEN 'November' WHEN 12 THEN 'December' END AS month_long_text" +
                    ",CASE npt_month WHEN 1 THEN 'Jan' WHEN 2 THEN 'Feb' WHEN 3 THEN 'Mar' WHEN 4 THEN 'Apr' WHEN 5 THEN 'May' WHEN 6 THEN 'Jun' WHEN 7 THEN 'Jul' WHEN 8 THEN 'Aug' WHEN 9 THEN 'Sep' WHEN 10 THEN 'Oct' WHEN 11 THEN 'Nov' WHEN 12 THEN 'Dec' END as month_short_text\n" +
                    "FROM TopQuery\n" +
                    "ORDER BY region_name, npt_year, npt_month;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            // More performance tracking
            fetchingtime = System.currentTimeMillis() - k;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }

        log.info("Response Sent" + fetchingtime);

        return myJSONArray;
    }

    public Collection NPTPercentageByRegion(String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {

            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            long k = System.currentTimeMillis();
            String query =
                    "WITH TopQuery AS ( " +
                            "SELECT rm.region_name " +
                            ", SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%z%' THEN b.hours ELSE 0 END) OVER(PARTITION BY rm.region_name) / SUM(b.hours) OVER(PARTITION BY rm.region_name) AS npt_percentage" +
                            ", SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%z%' THEN b.hours ELSE 0 END) OVER() / SUM(b.hours) OVER() AS npt_overall_percentage   " +
                            "FROM pten_cmr.cmr_data_usalt7 a " +
                            "inner join user_Security.user_mapping_Details u on a.well_num=u.well_num " +
                            "INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id " +
                            "INNER JOIN pten_cmr.RegionMap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)  " +
                            "WHERE WHERE u.userid=" + getUser_id() + " and a.report_date BETWEEN " + startTime + " AND " + endTime +
                            ") SELECT region_name, npt_percentage, npt_overall_percentage   FROM TopQuery  GROUP BY region_name , npt_percentage, npt_overall_percentage;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();


            total = System.currentTimeMillis() - start;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);

        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;
    }

    public Collection NPTHoursBySubCode(String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {

            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            long k = System.currentTimeMillis();
            String query =
                    "SELECT c.display_color" +
                            ", c.display_text" +
                            ", SUM(b.hours) AS code_hours" +
                            ", c.cmr_sub_code AS sub_code " +
                            "FROM pten_cmr.cmr_data_usalt7 a " +
                            "inner join user_Security.user_mapping_Details u on  a.well_num=u.well_num " +
                            "INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id " +
                            "INNER JOIN pten_cmr.CMRDecode c ON c.cmr_sub_sub_code IS NULL AND b.code = c.cmr_code AND c.cmr_sub_code = split_part(b.sub_code, '.', 2)  " +
                            "WHERE  u.userid=" + getUser_id() + " and b.code = '8'    " +
                            "AND a.report_date BETWEEN " + startTime + " AND " + endTime +
                            " GROUP BY c.display_text, c.display_color, sub_code ORDER BY code_hours DESC;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();


            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);

        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;
    }

    /**
     * overloaded for Date - Instant POC
     *
     * @param starttime
     * @param endtime
     * @return
     */
    public Collection NPTHoursBySubCode(Date starttime, Date endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {

            long startTime = starttime.toInstant().getEpochSecond(); //DateTimeUtils.dateToEpoch(starttime);
            long endTime = endtime.toInstant().getEpochSecond(); //DateTimeUtils.dateToEpoch(endtime);

            long k = System.currentTimeMillis();
            String query =
                    "SELECT c.display_color" +
                            ", c.display_text" +
                            ", SUM(b.hours) AS code_hours   " +
                            "FROM pten_cmr.cmr_data_usalt7 a " +
                            "inner join user_Security.user_mapping_Details u on  a.well_num=u.well_num  " +
                            "INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id " +
                            "INNER JOIN pten_cmr.CMRDecode c ON c.cmr_sub_sub_code IS NULL AND b.code = c.cmr_code AND c.cmr_sub_code = split_part(b.sub_code, '.', 2)  " +
                            "WHERE u.userid=" + getUser_id() + " and b.code = '8'    AND a.report_date BETWEEN " + startTime + " AND " + endTime +
                            " GROUP BY c.display_text, c.display_color;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();


            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);

        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;
    }

    public Collection NPTQuarterlyWithAverage(String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {

            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            long k = System.currentTimeMillis();

            String query =
                    "WITH TopQuery AS ( " +
                            "SELECT YEAR(to_timestamp(a.report_date)) AS npt_year " +
                            ", MONTH(to_timestamp(a.report_date)) AS npt_month " +
                            ", FLOOR((MONTH(to_timestamp(a.report_date)) - 1) / 3 + 1) AS npt_quarter " +
                            ", SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%z%' THEN b.hours ELSE 0 END) OVER(PARTITION BY YEAR(to_timestamp(a.report_date))" +
                            ", MONTH(to_timestamp(a.report_date))) / SUM(b.hours) OVER(PARTITION BY YEAR(to_timestamp(a.report_date))" +
                            ", MONTH(to_timestamp(a.report_date))) AS npt_month_pct" +
                            ", SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%z%' THEN b.hours ELSE 0 END) OVER(PARTITION BY YEAR(to_timestamp(a.report_date)), FLOOR((MONTH(to_timestamp(a.report_date)) - 1) / 3 + 1)) / SUM(b.hours) OVER(PARTITION BY YEAR(to_timestamp(a.report_date)), FLOOR((MONTH(to_timestamp(a.report_date)) - 1) / 3 + 1)) AS npt_qtr_pct" +
                            ", SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%z%' THEN b.hours ELSE 0 END) OVER() / SUM(b.hours) OVER() AS npt_overall_pct   " +
                            "FROM pten_cmr.cmr_data_usalt7 a " +
                            "inner join user_Security.user_mapping_details u on  a.well_num=u.well_num " +
                            "INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id  " +
                            "WHERE u.userid=" + getUser_id() + " and a.report_date BETWEEN " + startTime + " AND " + endTime +
                            ") SELECT rpt_year, rpt_month, rpt_quarter, npt_month_pct, npt_qtr_pct, npt_overall_pct " +
                            "FROM TopQuery " +
                            "GROUP BY rpt_year, rpt_month, rpt_quarter, npt_month_pct, npt_qtr_pct, npt_overall_pct ORDER BY rpt_year, rpt_quarter, rpt_month;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);

        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;
    }

    public Collection NPTPercentageByRegionMonth(String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {

            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            long k = System.currentTimeMillis();
            String query =
                    "SELECT rm.region_name " +
                            ", YEAR(to_timestamp(b.report_date)) AS npt_year" +
                            ", MONTH(to_timestamp(b.report_date)) AS npt_month" +
                            ", SUM(CASE WHEN code = '8' AND sub_code not like '%z%' then a.hours else 0 end) / SUM(a.hours) AS npt_pct   " +
                            "FROM pten_cmr.cmr_data_usalt7 b " +
                            "inner join user_Security.user_mapping_details u on  b.well_num=u.well_num " +
                            "INNER JOIN pten_cmr.cmr_time_code_data_usalt7 a ON b.cmr_data_id = a.cmr_data_id  " +
                            "INNER JOIN pten_cmr.RegionMap rm ON UPPER(b.contractor_business_unit) = UPPER(rm.contractor_business_unit) " +
                            "WHERE u.userid=" + getUser_id() + " and  b.report_date BETWEEN " + startTime + " AND " + endTime + " " +
                            "GROUP BY rm.region_name, year(to_timestamp(b.report_date)), month(to_timestamp(b.report_date))  " +
                            "ORDER BY rm.region_name, npt_year, npt_month ;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();


            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);

        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;
    }

    public Collection DateTest(String starttime, String endtime) {
        Collection myJSONArray = null;
        try {
            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);

        }

        return myJSONArray;
    }

    public Collection CMRRigList(String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {
            long endTime = DateTimeUtils.dateToEpoch(endtime + " 06:00");
            long startTime = endTime - 86400;
            long k = System.currentTimeMillis();
            String query =
                    "with ActiveWell as (" +
                            "Select distinct well_num from ptendrilling.ptendrilling_timedata " +
                            "where timepoint between " + startTime + " and " + endTime +
                            ") SELECT DISTINCT r.rig_name " +
                            "FROM pten_cmr.cmr_data_usalt7 r " +
                            "inner join user_Security.user_mapping_details u on  r.well_num=u.well " +
                            "inner join ActiveWell AW on r.well_num=AW.well_num " +
                            "WHERE u.userid=" + getUser_id() + " ORDER BY r.rig_name;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();


            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);

        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;
    }

    public Collection CMRDailyDetails(String well_num, String starttime, String endtime) {
        Collection myJSONArray = null;

        try {
            long endTime = DateTimeUtils.dateToEpoch(endtime + " 06:00");


            String query = "SELECT case when b.`from` is null then '' else from_unixtime(b.`from`,'HH:mm') end  as `from`" +
                    ", case when b.`to` is null then '' else from_unixtime(b.`to`,'HH:mm') end as `to`" +
                    ", b.code" +
                    ", b.sub_code" +
                    ", b.hours" +
                    ", b.details" +
                    ", case when b.code = '1' then '#ff0000' when b.code = '2' then '#ff4500' else '#008000' END as code_color" +
                    ", '#008b8b' as sub_code_color " +
                    "FROM pten_cmr.cmr_data_usalt7 a " +
                    "inner join user_Security.user_mapping_details u on  a.well_num=u.well_num " +
                    "INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id " +
                    "WHERE  u.userid=" + getUser_id() + " and a.well_num = " + well_num + " AND from_unixtime(a.report_date,'yyyy-MM-dd')=from_unixtime(" + endTime + ",'yyyy-MM-dd') " +
                    "ORDER BY b.cmr_time_code_data_id;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return myJSONArray;
    }

    public Collection NPTRegionQuarterlyWithAverage(String region, String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {

            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            long k = System.currentTimeMillis();
            String query = "--NPTRegionQuarterlyWithAverage\n" +
                    "WITH TopQuery AS (\n" +
                    "SELECT YEAR(to_timestamp(a.report_date)) AS npt_year \n" +
                    "\t,MONTH(to_timestamp(a.report_date)) AS npt_month \n" +
                    "\t,FLOOR((MONTH(to_timestamp(a.report_date)) - 1) / 3 + 1) AS npt_qtr \n" +
                    "\t,SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%z%' THEN b.hours ELSE 0 END) OVER(PARTITION BY YEAR(to_timestamp(a.report_date)), MONTH(to_timestamp(a.report_date))) / SUM(b.hours) OVER(PARTITION BY YEAR(to_timestamp(a.report_date)), MONTH(to_timestamp(a.report_date))) AS npt_month_pct\n" +
                    "\t,SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%z%' THEN b.hours ELSE 0 END) OVER(PARTITION BY YEAR(to_timestamp(a.report_date)), FLOOR((MONTH(to_timestamp(a.report_date)) - 1) / 3 + 1)) / SUM(b.hours) OVER(PARTITION BY YEAR(to_timestamp(a.report_date)), FLOOR((MONTH(to_timestamp(a.report_date)) - 1) / 3 + 1)) AS npt_qtr_pct\n" +
                    "\t,SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%z%' THEN b.hours ELSE 0 END) OVER() / SUM(b.hours) OVER() AS npt_overall_pct   \n" +
                    "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                    "\tINNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                    "\tINNER JOIN pten_cmr.RegionMap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit) \n" +
                    "\tWHERE UPPER(rm.region_name) = '" + region.toUpperCase() + "' AND a.report_date BETWEEN " + startTime + " AND " + endTime + "\n" +
                    "\t) \n" +
                    "\tSELECT npt_year, npt_month, npt_qtr, npt_month_pct, npt_qtr_pct, npt_overall_pct\n" +
                    "\t,CASE npt_month WHEN 1 THEN 'January' WHEN 2 THEN 'February' WHEN 3 THEN 'March' WHEN 4 THEN 'April' WHEN 5 THEN 'May' WHEN 6 THEN 'June' WHEN 7 THEN 'July' WHEN 8 THEN 'August' WHEN 9 THEN 'September' WHEN 10 THEN 'October' WHEN 11 THEN 'November' WHEN 12 THEN 'December' END AS month_long_text\n" +
                    ",CASE npt_month WHEN 1 THEN 'Jan' WHEN 2 THEN 'Feb' WHEN 3 THEN 'Mar' WHEN 4 THEN 'Apr' WHEN 5 THEN 'May' WHEN 6 THEN 'Jun' WHEN 7 THEN 'Jul' WHEN 8 THEN 'Aug' WHEN 9 THEN 'Sep' WHEN 10 THEN 'Oct' WHEN 11 THEN 'Nov' WHEN 12 THEN 'Dec' END AS month_short_text\n" +
                    ",CASE npt_qtr WHEN 1 THEN 'Q1' WHEN 2 THEN 'Q2' WHEN 3 THEN 'Q3' WHEN 4 THEN 'Q4' END AS npt_qtr_text\n" +
                    "\tFROM TopQuery WHERE u.userid=" + getUser_id() + " \n" +
                    "\tGROUP BY npt_year, npt_month, npt_qtr, npt_month_pct, npt_qtr_pct, npt_overall_pct \n" +
                    "\tORDER BY npt_year, npt_qtr, npt_month;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();

            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);

        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;
    }

    public Collection NPTRegionRigMonthAverageSort(String region, String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {

            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            long k = System.currentTimeMillis();
            String query = "WITH TopQuery AS (\n" +
                    "SELECT a.rig_name\n" +
                    "\t,YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END)) AS npt_year\n" +
                    "\t,MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END)) AS npt_month\n" +
                    "\t,FLOOR((MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN 1451628000 ELSE b.`from` END)) - 1) / 3) + 1 AS npt_qtr\n" +
                    "\t, SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%Z%' THEN b.hours ELSE 0 END) OVER(PARTITION BY rig_name, YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END))\n" +
                    "\t,MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END))) / SUM(b.hours) OVER(PARTITION BY rig_name, YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END))\n" +
                    "\t,MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END))) AS rig_npt_percentage\n" +
                    "\t,SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%Z%' THEN b.hours ELSE 0 END) OVER(PARTITION BY rig_name) / SUM(b.hours) OVER(PARTITION BY rig_name) AS rig_sort_order \n" +
                    "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num\n" +
                    "\tINNER JOIN pten_cmr.cmr_time_code_data_usalt7 b \n" +
                    "\tON a.cmr_data_id = b.cmr_data_id \n" +
                    "\t\tINNER JOIN pten_cmr.RegionMap rm \n" +
                    "\t\tON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit) \n" +
                    "WHERE u.userid=" + getUser_id() + " and UPPER(rm.region_name) = '" + region.toUpperCase() + "' \n" +
                    "AND CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END BETWEEN " + startTime + " AND " + endTime + " \n" +
                    ")SELECT DISTINCT npt_year\n" +
                    ",npt_month\n" +
                    ",rig_name\n" +
                    ",npt_qtr" +
                    ",rig_npt_percentage\n" +
                    ",rig_sort_order\n" +
                    ",CASE npt_month WHEN 1 THEN 'January' WHEN 2 THEN 'February' WHEN 3 THEN 'March' WHEN 4 THEN 'April' WHEN 5 THEN 'May' WHEN 6 THEN 'June' WHEN 7 THEN 'July' WHEN 8 THEN 'August' WHEN 9 THEN 'September' WHEN 10 THEN 'October' WHEN 11 THEN 'November' WHEN 12 THEN 'December' END AS month_long_text\n" +
                    ",CASE npt_month WHEN 1 THEN 'Jan' WHEN 2 THEN 'Feb' WHEN 3 THEN 'Mar' WHEN 4 THEN 'Apr' WHEN 5 THEN 'May' WHEN 6 THEN 'Jun' WHEN 7 THEN 'Jul' WHEN 8 THEN 'Aug' WHEN 9 THEN 'Sep' WHEN 10 THEN 'Oct' WHEN 11 THEN 'Nov' WHEN 12 THEN 'Dec' END AS month_short_text\n" +
                    " FROM TopQuery ORDER BY rig_sort_order, rig_name, npt_year, npt_month;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();


            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);

        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;
    }

    public Collection NPTRegionHoursByYQM(String region, String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {

            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            long k = System.currentTimeMillis();
            String query =
                    "--RegionalNPTHoursByMonth\n" +
                            "WITH TopQuery AS (\n" +
                            "SELECT YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN " + startTime + " ELSE b.`from` END)) AS npt_year\n" +
                            "\t, CASE MONTH(trunc(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN " + startTime + " ELSE b.`from` END), 'Q')) WHEN 1 THEN 1 WHEN 4 THEN 2 WHEN 7 THEN 3 WHEN 10 THEN 4 END AS npt_qtr\n" +
                            "\t, MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN " + startTime + " ELSE b.`from` END)) AS npt_month\n" +
                            "\t, cmrd.display_color\n" +
                            "\t, SUM(b.hours) AS npt_hours \n" +
                            "\t,split_part(b.sub_code, '.', 2) AS sub_code " +
                            "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num\n" +
                            "\tINNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                            "\tINNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit) \n" +
                            "\tINNER JOIN pten_cmr.cmrdecode cmrd ON b.code = cmrd.cmr_code AND split_part(b.sub_code, '.', 2) = cmrd.cmr_sub_code \n" +
                            "WHERE u.userid=" + getUser_id() + " and  b.code = '8' \n" +
                            "\tAND b.sub_code NOT LIKE '%z%' \n" +
                            "\tAND cmrd.cmr_sub_sub_code IS NULL\n" +
                            "\tAND UPPER(rm.region_name) = '" + region.toUpperCase() + "' \n" +
                            "\tAND (b.`from` BETWEEN " + startTime + " AND " + endTime + " OR b.`to` BETWEEN " + startTime + " AND " + endTime + ")\n" +
                            "GROUP BY npt_year\n" +
                            "\t,sub_code\n" +
                            "\t,npt_qtr\n" +
                            "\t,npt_month\n" +
                            "\t,display_color\n" +
                            "ORDER BY npt_year\n" +
                            "\t,npt_qtr\n" +
                            "\t,npt_month\n" +
                            "\t,display_color" +
                            ")SELECT npt_year\n" +
                            "\t,sub_code\n" +
                            ",npt_qtr\n" +
                            ",npt_month\n" +
                            ",display_color\n" +
                            ",npt_hours\n" +
                            ",CASE npt_month WHEN 1 THEN 'January' WHEN 2 THEN 'February' WHEN 3 THEN 'March' WHEN 4 THEN 'April' WHEN 5 THEN 'May' WHEN 6 THEN 'June' WHEN 7 THEN 'July' WHEN 8 THEN 'August' WHEN 9 THEN 'September' WHEN 10 THEN 'October' WHEN 11 THEN 'November' WHEN 12 THEN 'December' END AS month_long_text\n" +
                            ",CASE npt_month WHEN 1 THEN 'Jan' WHEN 2 THEN 'Feb' WHEN 3 THEN 'Mar' WHEN 4 THEN 'Apr' WHEN 5 THEN 'May' WHEN 6 THEN 'Jun' WHEN 7 THEN 'Jul' WHEN 8 THEN 'Aug' WHEN 9 THEN 'Sep' WHEN 10 THEN 'Oct' WHEN 11 THEN 'Nov' WHEN 12 THEN 'Dec' END AS month_short_text\n" +
                            ",CASE npt_qtr WHEN 1 THEN 'Q1' WHEN 2 THEN 'Q2' WHEN 3 THEN 'Q3' WHEN 4 THEN 'Q4' END AS npt_qtr_text\n" +
                            "from TopQuery tq\n" +
                            "order by tq.npt_year, tq.npt_qtr, tq.npt_month;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();


            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);

        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;
    }

    public Collection NPTRegionHoursByRYM(String region, String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {

            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);
            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime); //endTime - 14515200; //minus 6 months

            long k = System.currentTimeMillis();
            String query =
                    "WITH TopQuery AS (\n" +
                            "SELECT a.rig_name\n" +
                            "\t, YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN " + startTime + " ELSE b.`from` END)) AS npt_year\n" +
                            "\t, MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN " + startTime + " ELSE b.`from` END)) AS npt_month\n" +
                            "\t, SUM(b.hours) AS npt_hours \n" +
                            "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                            "\tINNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                            "\tINNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit) \n" +
                            "\tINNER JOIN pten_cmr.cmrdecode cmrd ON b.code = cmrd.cmr_code AND split_part(b.sub_code, '.', 2) = cmrd.cmr_sub_code \n" +
                            "WHERE u.userid=" + getUser_id() + " and  b.code = '8' \n" +
                            "\tAND b.sub_code NOT LIKE '%z%' \n" +
                            "\tAND cmrd.cmr_sub_sub_code IS NULL\n" +
                            "\tAND UPPER(rm.region_name) = '" + region.toUpperCase() + "' \n" +
                            "\tAND (b.`from` BETWEEN " + startTime + " AND " + endTime + " OR b.`to` BETWEEN " + startTime + " AND " + endTime + ")\n" +
                            "GROUP BY rig_name\n" +
                            "\t,npt_year\n" +
                            "\t,npt_month\n" +
                            "ORDER BY npt_year\n" +
                            "\t,npt_month\n" +
                            ")SELECT rig_name\n" +
                            ",npt_year\n" +
                            ",npt_month\n" +
                            ",npt_hours\n" +
                            ",CASE npt_month WHEN 1 THEN 'January' WHEN 2 THEN 'February' WHEN 3 THEN 'March' WHEN 4 THEN 'April' WHEN 5 THEN 'May' WHEN 6 THEN 'June' WHEN 7 THEN 'July' WHEN 8 THEN 'August' WHEN 9 THEN 'September' WHEN 10 THEN 'October' WHEN 11 THEN 'November' WHEN 12 THEN 'December' END AS month_long_text\n" +
                            ",CASE npt_month WHEN 1 THEN 'Jan' WHEN 2 THEN 'Feb' WHEN 3 THEN 'Mar' WHEN 4 THEN 'Apr' WHEN 5 THEN 'May' WHEN 6 THEN 'Jun' WHEN 7 THEN 'Jul' WHEN 8 THEN 'Aug' WHEN 9 THEN 'Sep' WHEN 10 THEN 'Oct' WHEN 11 THEN 'Nov' WHEN 12 THEN 'Dec' END AS month_short_text" +
                            ",CONCAT(CASE npt_month WHEN 1 THEN 'Jan' WHEN 2 THEN 'Feb' WHEN 3 THEN 'Mar' WHEN 4 THEN 'Apr' WHEN 5 THEN 'May' WHEN 6 THEN 'Jun' WHEN 7 THEN 'Jul' WHEN 8 THEN 'Aug' WHEN 9 THEN 'Sep' WHEN 10 THEN 'Oct' WHEN 11 THEN 'Nov' WHEN 12 THEN 'Dec' END, ' ', cast(npt_year as varchar(4))) as month_year\n" +
                            "from TopQuery tq\n" +
                            "order by tq.npt_year, tq.npt_month";

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();


            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);

        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;
    }

    public Collection NPTRegionHoursByRig(String region, String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {

            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            long k = System.currentTimeMillis();
            String query = "";

            query = "--npt hours by rig chart at the bottom\n" +
                    "WITH RigsInRegion AS (\n" +
                    "SELECT DISTINCT z.rig_name\n" +
                    "  FROM  pten_cmr.cmr_data_usalt7 z inner join user_Security.user_mapping_details u on  z.well_num=u.well_num\n" +
                    "                INNER JOIN pten_cmr.regionmap rmz ON UPPER(rmz.contractor_business_unit) = UPPER(z.contractor_business_unit)\n" +
                    "                INNER JOIN pten_cmr.cmr_time_code_data_usalt7 y ON z.cmr_data_id = y.cmr_data_id\n" +
                    "WHERE u.userid=" + getUser_id() + " and (y.`from` BETWEEN " + startTime + " AND " + endTime + " OR y.`to` BETWEEN " + startTime + " AND " + endTime + ") \n" +
                    "   AND UPPER(rmz.region_name) = '" + region.toUpperCase() + "'\n" +
                    "), RigNPT AS (\n" +
                    "SELECT a.rig_name\n" +
                    "                , cmrd.display_color\n" +
                    "                , SUM(b.hours) OVER(PARTITION BY a.rig_name) AS overall_npt_hours\n" +
                    "                , SUM(b.hours) OVER(PARTITION BY a.rig_name, cmrd.display_color) AS sub_code_npt_hours\n" +
                    "                ,split_part(b.sub_code, \".\", 2) as sub_code\n" +
                    "  FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num\n" +
                    "                INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id\n" +
                    "                INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                    "                INNER JOIN pten_cmr.cmrdecode cmrd ON b.code = cmrd.cmr_code AND split_part(b.sub_code, '.', 2) = cmrd.cmr_sub_code AND cmrd.cmr_sub_sub_code IS NULL\n" +
                    "WHERE u.userid=" + getUser_id() + " and  UPPER(rm.region_name) = '" + region.toUpperCase() + "'\n" +
                    "   AND b.code = '8'\n" +
                    "   AND b.sub_code NOT LIKE '%z%'\n" +
                    "   AND (b.`from` BETWEEN " + startTime + " AND " + endTime + " OR b.`to` BETWEEN " + startTime + " AND " + endTime + ")\n" +
                    ")\n" +
                    "SELECT DISTINCT rir.rig_name\n" +
                    "                , rnpt.display_color\n" +
                    "                , rnpt.sub_code\n" +
                    "                , COALESCE(rnpt.overall_npt_hours, 0.0) AS overall_npt_hours\n" +
                    "                , COALESCE(rnpt.sub_code_npt_hours, 0.0) AS npt_hours\n" +
                    "  FROM RigsInRegion rir\n" +
                    "                LEFT OUTER JOIN RigNPT rnpt ON rir.rig_name = rnpt.rig_name\n" +
                    "ORDER BY COALESCE(overall_npt_hours, 0.0) DESC;";


            myJSONArray = myUtils.runSQLGetJSONArray(query);

            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();


            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);

        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;
    }

    public Collection NPTRegionRigPercent(String region, String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {

            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            long k = System.currentTimeMillis();
            String query =
                    "SELECT a.rig_name" +
                            ", SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%Z%' THEN b.hours ELSE 0 END) / SUM(b.hours) AS rig_npt_percent " +
                            "FROM pten_cmr.cmr_data_usalt7 a " +
                            "inner join user_Security.user_mapping_details u on  a.well_num=u.well_num " +
                            "INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id " +
                            "INNER JOIN pten_cmr.RegionMap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit) " +
                            "WHERE u.userid=" + getUser_id() + " and  rm.region_name = '" + region + "' AND (b.`from` BETWEEN " + startTime + " AND " + endTime + " OR b.`to` BETWEEN " + startTime + " AND " + endTime + ") " +
                            "GROUP BY a.rig_name ORDER BY rig_npt_percent;";

            log.debug(query);
            myJSONArray = myUtils.runSQLGetJSONArray(query);
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();


            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);

        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;
    }

    public Collection NPTRegionPercentByRYM(String region, String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {

            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);
            long startTime = endTime - 14515200; //minus 6 months.

            long k = System.currentTimeMillis();
            String query =
                    "WITH TopQuery AS (\n" +
                            "SELECT a.rig_name\n" +
                            "\t,YEAR(to_timestamp(a.report_date)) AS npt_year\n" +
                            "\t,MONTH(to_timestamp(a.report_date)) AS npt_month\t\n" +
                            "\t,SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%Z%' THEN b.hours ELSE 0 END) / SUM(b.hours) AS rig_npt_percent \n" +
                            "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                            "\t\tINNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                            "\t\tINNER JOIN pten_cmr.RegionMap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                            "WHERE u.userid=" + getUser_id() + " and UPPER(rm.region_name) = '" + region.toUpperCase() + "' \n" +
                            "\tAND (b.`from` BETWEEN " + startTime + " AND " + endTime + " OR b.`to` BETWEEN " + startTime + " AND " + endTime + ")\n" +
                            "group by rig_name, npt_year, npt_month\n" +
                            ")SELECT rig_name\n" +
                            "\t,npt_year\n" +
                            "\t,npt_month\n" +
                            "\t,rig_npt_percent" +
                            "\t,CONCAT(CASE npt_month WHEN 1 THEN 'Jan' WHEN 2 THEN 'Feb' WHEN 3 THEN 'Mar' WHEN 4 THEN 'Apr' WHEN 5 THEN 'May' WHEN 6 THEN 'Jun' WHEN 7 THEN 'Jul' WHEN 8 THEN 'Aug' WHEN 9 THEN 'Sep' WHEN 10 THEN 'Oct' WHEN 11 THEN 'Nov' WHEN 12 THEN 'Dec' END, ' ', cast(npt_year as varchar(4))) as month_year\n" +
                            "FROM TopQuery \n" +
                            "ORDER BY npt_year, npt_month;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();


            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);

        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;
    }

    public Collection NPTRegionRigTypeNPTPercent(String region, String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try {

            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            long k = System.currentTimeMillis();

            String query =
                    "WITH TopQuery AS (" +
                            "SELECT rt.display_text" +
                            ", rt.display_color" +
                            ", SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%Z%' THEN b.hours ELSE 0 END) OVER(PARTITION BY rt.rig_type_id) / SUM(b.hours) OVER(PARTITION BY rt.rig_type_id) AS type_npt_pct" +
                            ", SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%Z%' THEN b.hours ELSE 0 END) OVER() / SUM(b.hours) OVER() AS overall_npt_pct " +
                            "FROM pten_cmr.cmr_data_usalt7 a " +
                            "inner join user_Security.user_mapping_details u on  a.well_num=u.well_num " +
                            "INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id " +
                            "INNER JOIN pten_cmr.RegionMap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit) " +
                            "INNER JOIN pten_cmr.Rig r ON r.display_text = a.rig_name " +
                            "INNER JOIN pten_cmr.RigType rt ON r.rig_type_id = rt.rig_type_id " +
                            "WHERE  u.userid=" + getUser_id() + " and  rm.region_name = '" + region + "' AND (b.`from` BETWEEN " + startTime + " AND " + endTime + " OR b.`to` BETWEEN " + startTime + " AND " + endTime + ")) " +
                            "SELECT DISTINCT * FROM TopQuery;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();


            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);

        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;
    }

    public Collection wellDrillingParameters(String wellnum, String starttime, String endtime, String targetRowCount) {
        Collection myJSONArray = null;

        // Performance Counters
        long fetchingtime = 0L;
        long total = 0L;

        try {
            // Convert parameters
            long startTime = DateTimeUtils.dateToEpoch(starttime);
            long endTime = DateTimeUtils.dateToEpoch(endtime);

            // Performance tracker
            long k = System.currentTimeMillis();

            // Guess total record count and get modifier to reach desired row count
            long totalRecords = endTime - startTime;
            long compressionFactor = totalRecords / Long.parseLong(targetRowCount);

            // Query Text and Execution
            String query = "WITH TopQuery AS ( SELECT td.timepoint  , td.well_num  , ROW_NUMBER() OVER(ORDER BY td.timepoint) AS SequenceTop  FROM ptendrilling.ptendrilling_TimeData td  WHERE td.well_num = " + wellnum + " AND td.timepoint BETWEEN " + startTime + " AND " + endTime + ")  SELECT CAST(tq.SequenceTop / " + compressionFactor + " AS INT) AS time_sequence  , MAX(NULLIF(td.bit_depth_feet, -999.25)) AS bit_depth  , MAX(NULLIF(td.hole_depth_feet, -999.25)) AS hole_depth  , MAX(NULLIF(td.rotary_rpm_rpm, -999.25)) AS rotary_rpm  , MAX(NULLIF(td.total_pump_output_gal_per_min, -999.25)) AS total_pump_output_gpm  , MAX(NULLIF(td.convertible_torque_kft_lb, -999.25)) AS convertible_torque  , MAX(NULLIF(td.differential_pressure_psi, -999.25)) AS differential_pressure  , MAX(NULLIF(td.standpipe_pressure_psi, -999.25)) AS standpipe_pressure  , MAX(NULLIF(td.on_bottom_rop_ft_per_hr, -999.25)) AS on_bottom_rop  , MAX(NULLIF(td.weight_on_bit_klbs, -999.25)) AS true_wob  , MIN(td.timepoint) AS start_time  , MAX(td.timepoint) AS end_time  FROM ptendrilling.ptendrilling_TimeData td  INNER JOIN TopQuery tq ON td.well_num = tq.well_num AND td.timepoint = tq.timepoint GROUP BY CAST(tq.SequenceTop / " + compressionFactor + " AS INT);";
            myJSONArray = myUtils.runSQLGetJSONArray(query);

            // Performance Tracking
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            total = System.currentTimeMillis() - start;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;

    }

    public Collection rigDrillingParameters(String rigname, String starttime, String endtime, String targetRowCount) {
        Collection myJSONArray = null;

        // Performance Counters
        long fetchingtime = 0L;
        long total = 0L;

        try {
            // Convert parameters
            long startTime = DateTimeUtils.dateToEpoch(starttime);
            long endTime = DateTimeUtils.dateToEpoch(endtime);

            // Performance tracker
            long k = System.currentTimeMillis();

            // Guess total record count and get modifier to reach desired row count
            long totalRecords = endTime - startTime;
            long compressionFactor = totalRecords / Long.parseLong(targetRowCount);

            // Query Text and Execution
            String query = "WITH TopQuery AS ( SELECT td.timepoint  , td.well_num,  wd.rig_name  , ROW_NUMBER() OVER(ORDER BY td.timepoint) AS SequenceTop  FROM ptendrilling.ptendrilling_TimeData td INNER JOIN ptendrilling.ptendrilling_WellData wd ON td.well_num = wd.well_num  WHERE wd.rig_name LIKE '%" + rigname + "%' AND td.timepoint BETWEEN " + startTime + " AND " + endTime + ")  SELECT CAST(tq.SequenceTop / " + compressionFactor + " AS INT) AS time_sequence  , MAX(NULLIF(td.bit_depth_feet, -999.25)) AS bit_depth  , MAX(NULLIF(td.hole_depth_feet, -999.25)) AS hole_depth  , MAX(NULLIF(td.rotary_rpm_rpm, -999.25)) AS rotary_rpm  , MAX(NULLIF(td.total_pump_output_gal_per_min, -999.25)) AS total_pump_output_gpm  , MAX(NULLIF(td.convertible_torque_kft_lb, -999.25)) AS convertible_torque  , MAX(NULLIF(td.differential_pressure_psi, -999.25)) AS differential_pressure  , MAX(NULLIF(td.standpipe_pressure_psi, -999.25)) AS standpipe_pressure  , MAX(NULLIF(td.on_bottom_rop_ft_per_hr, -999.25)) AS on_bottom_rop  , MAX(NULLIF(td.weight_on_bit_klbs, -999.25)) AS true_wob  , MIN(td.timepoint) AS start_time  , MAX(td.timepoint) AS end_time  FROM ptendrilling.ptendrilling_TimeData td  INNER JOIN TopQuery tq ON td.well_num = tq.well_num AND td.timepoint = tq.timepoint GROUP BY CAST(tq.SequenceTop / " + compressionFactor + " AS INT);";
            myJSONArray = myUtils.runSQLGetJSONArray(query);

            // Performance Tracking
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            total = System.currentTimeMillis() - start;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;

    }

    public Collection NPTHoursBySCRLYM(String starttime, String endtime, String[] riglist) {
        // Return Object
        Collection myJSONArray = null;

        // Performance Tracking
        long fetchingtime = 0L;
        long k = System.currentTimeMillis();

        try {
            // Date Conversion
            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            // Array to IN list
            String rigList = myUtils.arrayToInClause(riglist);

            // Query and retrieval
            String query = "--NPTHoursBySCRLYM\n" +
                    "WITH TopLevel AS (" +
                    " SELECT a.rig_name \n" +
                    "	 , YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END)) AS npt_year \n" +
                    "	 , MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END)) AS npt_month \n" +
                    "	 , b.code \n" +
                    "	 , split_part(b.sub_code, '.', 2) AS sub_code \n" +
                    "	 , SUM(CASE WHEN b.code = '8' THEN b.hours ELSE 0 END) AS npt_sub_code_hours \n" +
                    "    , c.display_color \n " +
                    "  FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                    "	LEFT OUTER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id AND b.code = '8' \n" +
                    "    LEFT OUTER JOIN pten_cmr.CMRDecode c ON b.code = c.cmr_code AND split_part(b.sub_code, '.', 2) = c.cmr_sub_code AND c.cmr_sub_sub_code IS NULL \n" +
                    " WHERE u.userid=" + getUser_id() + " and  a.rig_name IN (" + rigList + ") AND (b.`from` BETWEEN " + startTime + " AND " + endTime + " OR b.`to` BETWEEN " + startTime + " AND " + endTime + ") AND b.code = '8' AND b.sub_code NOT LIKE '%Z%' \n" +
                    "   group by rig_name ,npt_year, npt_month, code, sub_code, display_color\n" +
                    " )" +
                    "  SELECT DISTINCT rig_name\n" +
                    "   ,npt_year\n" +
                    "   ,npt_month\n" +
                    "   ,CASE npt_month WHEN 1 THEN 'January' WHEN 2 THEN 'February' WHEN 3 THEN 'March' WHEN 4 THEN 'April' WHEN 5 THEN 'May' WHEN 6 THEN 'June' WHEN 7 THEN 'July' WHEN 8 THEN 'August' WHEN 9 THEN 'September' WHEN 10 THEN 'October' WHEN 11 THEN 'November' WHEN 12 THEN 'December' END AS month_long_text, CASE npt_month WHEN 1 THEN 'Jan' WHEN 2 THEN 'Feb' WHEN 3 THEN 'Mar' WHEN 4 THEN 'Apr' WHEN 5 THEN 'May' WHEN 6 THEN 'Jun' WHEN 7 THEN 'Jul' WHEN 8 THEN 'Aug' WHEN 9 THEN 'Sep' WHEN 10 THEN 'Oct' WHEN 11 THEN 'Nov' WHEN 12 THEN 'Dec' END AS month_short_text\n" +
                    "   ,code\n" +
                    "   ,sub_code\n" +
                    "   ,npt_sub_code_hours\n" +
                    "   ,display_color \n" +
                    "FROM TopLevel ORDER BY npt_year, npt_month;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            // More performance tracking
            fetchingtime = System.currentTimeMillis() - k;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }

        log.info("Response Sent" + fetchingtime);


        return myJSONArray;
    }

    public Collection NPTHoursBySCRL(String starttime, String endtime, String[] riglist) {
        // Return Object
        Collection myJSONArray = null;

        // Performance Tracking
        long fetchingtime = 0L;
        long k = System.currentTimeMillis();

        try {
            // Date Conversion
            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            // Array to IN list
            String rigList = myUtils.arrayToInClause(riglist);

            // Query and retrieval
            String query = "SELECT a.rig_name \n" +
                    ", c.display_text" +
                    "	 , b.code \n" +
                    "	 , split_part(b.sub_code, '.', 2) AS sub_code \n" +
                    "	 , SUM(CASE WHEN b.code = '8' THEN b.hours ELSE 0 END) AS npt_sub_code_hours \n" +
                    "    , c.display_color \n " +
                    "  FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                    "	LEFT OUTER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id AND b.code = '8' \n" +
                    "    LEFT OUTER JOIN pten_cmr.CMRDecode c ON b.code = c.cmr_code AND split_part(b.sub_code, '.', 2) = c.cmr_sub_code AND c.cmr_sub_sub_code IS NULL \n" +
                    " WHERE u.userid=" + getUser_id() + " and  a.rig_name IN (" + rigList + ") AND (b.`from` BETWEEN " + startTime + " AND " + endTime + " OR b.`to` BETWEEN " + startTime + " AND " + endTime + ") AND b.code = '8' AND b.sub_code NOT LIKE '%Z%' \n" +
                    " GROUP BY a.rig_name \n" +
                    "		, c.display_text \n" +
                    "		, b.code \n" +
                    "		, split_part(b.sub_code, '.', 2) \n " +
                    "       , c.display_color ;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            // More performance tracking
            fetchingtime = System.currentTimeMillis() - k;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }

        log.info("Response Sent" + fetchingtime);


        return myJSONArray;
    }

    public Collection NPTSCHoursFromRL(String starttime, String endtime, String[] riglist) {
        // Return Object
        Collection myJSONArray = null;

        // Performance Tracking
        long fetchingtime = 0L;
        long k = System.currentTimeMillis();

        try {
            // Date Conversion
            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            // Array to IN list
            String rigList = myUtils.arrayToInClause(riglist);

            // Query and retrieval
            String query = "SELECT b.code \n" +
                    "	 , split_part(b.sub_code, '.', 2) AS sub_code \n" +
                    "	 , SUM(CASE WHEN b.code = '8' THEN b.hours ELSE 0 END) AS npt_sub_code_hours \n" +
                    "    , c.display_color \n " +
                    "  FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                    "	LEFT OUTER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id AND b.code = '8' \n" +
                    "    LEFT OUTER JOIN pten_cmr.CMRDecode c ON b.code = c.cmr_code AND split_part(b.sub_code, '.', 2) = c.cmr_sub_code AND c.cmr_sub_sub_code IS NULL \n" +
                    " WHERE u.userid=" + getUser_id() + " and  a.rig_name IN (" + rigList + ") AND (b.`from` BETWEEN " + startTime + " AND " + endTime + " OR b.`to` BETWEEN " + startTime + " AND " + endTime + ") AND b.code = '8' AND b.sub_code NOT LIKE '%Z%' \n" +
                    " GROUP BY b.code \n" +
                    "		, split_part(b.sub_code, '.', 2) \n " +
                    "       , c.display_color ;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            // More performance tracking
            fetchingtime = System.currentTimeMillis() - k;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }

        log.info("Response Sent" + fetchingtime);


        return myJSONArray;
    }

    public Collection NPTPctFromRL(String starttime, String endtime, String[] riglist) {
        // Return Object
        Collection myJSONArray = null;

        // Performance Tracking
        long fetchingtime = 0L;
        long k = System.currentTimeMillis();

        try {
            // Date Conversion
            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            // Array to IN list
            String rigList = myUtils.arrayToInClause(riglist);

            // Query and retrieval
            String query = "WITH TopQuery AS (" +
                    "SELECT a.rig_name" +
                    ", SUM(CASE WHEN b.code = '8' THEN b.hours ELSE 0 END) OVER(PARTITION BY rig_name) / SUM(b.hours) OVER(PARTITION BY rig_name) AS rig_npt_pct" +
                    ", SUM(CASE WHEN b.code = '8' THEN b.hours ELSE 0 END) OVER() / SUM(b.hours) OVER() AS overall_npt_pct " +
                    "FROM pten_cmr.cmr_data_usalt7 a " +
                    "inner join user_Security.user_mapping_details u on  a.well_num=u.well_num " +
                    "INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id " +
                    "WHERE u.userid=" + getUser_id() + " and  a.rig_name IN (" + rigList + ") AND (b.`from` BETWEEN " + startTime + " AND " + endTime + " OR b.`to` BETWEEN " + startTime + " AND " + endTime + "))" +
                    "SELECT DISTINCT tq.rig_name, tq.rig_npt_pct, tq.overall_npt_pct FROM TopQuery tq;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            // More performance tracking
            fetchingtime = System.currentTimeMillis() - k;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }

        log.info("Response Sent" + fetchingtime);


        return myJSONArray;
    }

    public Collection NPTPctByYMRL(String starttime, String endtime, String[] riglist) {
        // Return Object
        Collection myJSONArray = null;

        // Performance Tracking
        long fetchingtime = 0L;
        long k = System.currentTimeMillis();

        try {
            // Date Conversion
            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            // Array to IN list
            String rigList = myUtils.arrayToInClause(riglist);

            // Query and retrieval
            String query = "WITH TopLevel AS ( \n" +
                    "SELECT a.rig_name \n" +
                    "	 , YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END)) AS npt_year \n" +
                    "	 , MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END)) AS npt_month \n" +
                    "	 , SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%Z%' THEN b.hours ELSE 0 END) OVER(PARTITION BY a.rig_name, YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END)), MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END))) / SUM(b.hours) OVER(PARTITION BY a.rig_name), YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END)), MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END)) AS npt_percentage \n" +
                    "	 , SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%Z%'  THEN b.hours ELSE 0 END) OVER(PARTITION BY a.rig_name, YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END))) / SUM(b.hours) OVER(PARTITION BY a.rig_name, YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END))) AS annual_npt_percentage \n" +
                    " 	 , SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%Z%'  THEN b.hours ELSE 0 END) OVER(PARTITION BY a.rig_name) / SUM(b.hours) OVER(PARTITION BY a.rig_name) AS overall_npt_percentage \n" +
                    "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                    "	LEFT OUTER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                    "WHERE u.userid=" + getUser_id() + " and  a.rig_name IN (" + rigList + ") AND (b.`from` BETWEEN " + startTime + " AND " + endTime + " OR b.`to` BETWEEN " + startTime + " AND " + endTime + ")) \n" +
                    "SELECT DISTINCT rig_name, npt_year, npt_month, CASE npt_month WHEN 1 THEN 'January' WHEN 2 THEN 'February' WHEN 3 THEN 'March' WHEN 4 THEN 'April' WHEN 5 THEN 'May' WHEN 6 THEN 'June' WHEN 7 THEN 'July' WHEN 8 THEN 'August' WHEN 9 THEN 'September' WHEN 10 THEN 'October' WHEN 11 THEN 'November' WHEN 12 THEN 'December' END AS month_long_text, CASE npt_month WHEN 1 THEN 'Jan' WHEN 2 THEN 'Feb' WHEN 3 THEN 'Mar' WHEN 4 THEN 'Apr' WHEN 5 THEN 'May' WHEN 6 THEN 'Jun' WHEN 7 THEN 'Jul' WHEN 8 THEN 'Aug' WHEN 9 THEN 'Sep' WHEN 10 THEN 'Oct' WHEN 11 THEN 'Nov' WHEN 12 THEN 'Dec' END AS month_short_text, npt_percentage, annual_npt_percentage, overall_npt_percentage " +
                    "FROM TopLevel " +
                    "ORDER BY npt_year, npt_month;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            // More performance tracking
            fetchingtime = System.currentTimeMillis() - k;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }

        log.info("Response Sent" + fetchingtime);


        return myJSONArray;
    }

    public String StringTest(String[] riglist) {
        return riglist[1];
    }

    public Collection NPTPctArbitraryRegionsTimegroups(String starttime, String endtime, String grouptype, String[] regionlist) {
        // BJS 20180529 - I made this query somewhat more complicated than it needs to be for the monthly/quarterly return in the name of
        //                keeping the functionality isolated to a single block, instead of spread out over two or three.
        //                If performance is a big issue it can be refactored, but this is MUCH easier to read.

        // return object
        Collection myJSONArray = null;

        // Performance Tracking
        long fetchingtime = 0L;
        long k = System.currentTimeMillis();

        try {
            // Date Conversion
            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            // Array to IN list
            String regionList = myUtils.arrayToInClause(regionlist);

            // Set query chunk clause based on provided group type
            String chunkSQL = "";

            switch (grouptype) {
                // Quarterly and Monthly can use basic trunc function as root method
                case "Q":
                    chunkSQL = "SELECT TRUNC(to_timestamp(a.report_date), 'Q') AS chunk_start \n";
                    break;
                case "M":
                    chunkSQL = "SELECT trunc(to_timestamp(a.report_date), 'MM') AS chunk_start \n";
                    break;

                // Weeks have to start on Thursday, so we have to slightly modify the trunc output
                case "W":
                    chunkSQL = "SELECT adddate(trunc(to_timestamp(a.report_date), 'D'), 3) AS chunk_start \n";
                    break;
            }

            // Query and retrieval
            String query = "WITH AllChunks AS ( \n" +
                    chunkSQL +
                    "FROM pten_cmr.cmr_data_usalt7 a " +
                    "inner join user_Security.user_mapping_details u on  a.well_num=u.well_num AND u.userid = " + getUser_id() + " \n" +
                    "WHERE a.report_date BETWEEN " + startTime + " AND " + endTime + " \n" +
                    "), FunkyChunks AS ( \n" +
                    "SELECT ROW_NUMBER() OVER(ORDER BY chunk_start) AS chunk_index \n" +
                    "  , chunk_start \n" +
                    "  , lead(chunk_start, 1) OVER(order by chunk_start) AS chunk_end \n" +
                    "  FROM AllChunks \n" +
                    " GROUP BY chunk_start \n" +
                    "), RegionChunkPcts AS ( \n" +
                    "SELECT rm.region_name \n" +
                    "  , tw.chunk_start \n" +
                    "  , SUM(CASE WHEN b.code = '8' THEN b.hours ELSE 0 END) OVER(PARTITION BY rm.region_name, tw.chunk_start) / SUM(b.hours) OVER(PARTITION BY rm.region_name, tw.chunk_start) AS chunk_pct \n" +
                    "  , SUM(CASE WHEN b.code = '8' THEN b.hours ELSE 0 END) OVER(PARTITION BY rm.region_name) / SUM(b.hours) OVER(PARTITION BY rm.region_name) AS region_pct \n" +
                    "  FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num AND u.userid = " + getUser_id() + "\n" +
                    "  INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                    "  INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit) \n" +
                    "  INNER JOIN FunkyChunks tw ON to_timestamp(b.`to`) BETWEEN tw.chunk_start AND COALESCE(tw.chunk_end, '" + endTime + "')\n" +
                    " WHERE UPPER(rm.region_name) IN (" + regionList.toUpperCase() + ") \n" +
                    ") \n" +
                    "SELECT DISTINCT region_name, chunk_start, chunk_pct, region_pct  \n" +
                    "FROM RegionChunkPcts  \n" +
                    "ORDER BY chunk_start, region_name; \n";

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            // More performance tracking
            fetchingtime = System.currentTimeMillis() - k;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }

        log.info("Response Sent" + fetchingtime);


        return myJSONArray;
    }


    public Collection NPTHoursArbitraryRegionsTimegroups(String starttime, String endtime, String grouptype, String[] regionlist) {
        // BJS 20180529 - I made this query somewhat more complicated than it needs to be for the monthly/quarterly return in the name of
        //                keeping the functionality isolated to a single block, instead of spread out over two or three.
        //                If performance is a big issue it can be refactored, but this is MUCH easier to read.

        Collection myJSONArray = null;

        // Performance Tracking
        long fetchingtime = 0L;
        long k = System.currentTimeMillis();

        try {
            // Date Conversion
            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            // Array to IN list
            String regionList = myUtils.arrayToInClause(regionlist);

            // Set query chunk clause based on provided group type
            String chunkSQL = "";

            switch (grouptype) {
                // Quarterly and Monthly can use basic trunc function as root method
                case "Q":
                    chunkSQL = "SELECT TRUNC(to_timestamp(a.report_date), 'Q') AS chunk_start \n";
                    break;
                case "M":
                    chunkSQL = "SELECT trunc(to_timestamp(a.report_date), 'MM') AS chunk_start \n";
                    break;

                // Weeks have to start on Thursday, so we have to slightly modify the trunc output
                case "W":
                    chunkSQL = "SELECT adddate(trunc(to_timestamp(a.report_date), 'D'), 3) AS chunk_start \n";
                    break;
            }

            // Query and retrieval
            String query = "WITH AllChunks AS ( \n" +
                    chunkSQL +
                    "FROM pten_cmr.cmr_data_usalt7 a " +
                    "inner join user_Security.user_mapping_details u on  a.well_num=u.well_num AND u.userid = " + getUser_id() + " \n" +
                    "WHERE a.report_date BETWEEN " + startTime + " AND " + endTime + " \n" +
                    "), FunkyChunks AS ( \n" +
                    "SELECT ROW_NUMBER() OVER(ORDER BY chunk_start) AS chunk_index \n" +
                    "  , chunk_start \n" +
                    "  , lead(chunk_start, 1) OVER(order by chunk_start) AS chunk_end \n" +
                    "  FROM AllChunks \n" +
                    " GROUP BY chunk_start \n" +
                    ") \n" +
                    "SELECT rm.region_name \n" +
                    "  , fc.chunk_start \n" +
                    "  , cd.display_color \n" +
                    "  , cd.display_text \n" +
                    "  , cd.cmr_sub_code \n" +
                    "  , SUM(b.hours) AS chunk_code_hours \n" +
                    "  FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num AND u.userid = " + getUser_id() + " \n" +
                    "  INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                    "  INNER JOIN FunkyChunks fc ON to_timestamp(b.`to`) BETWEEN fc.chunk_start AND COALESCE(fc.chunk_end, '" + endTime + "')" +
                    "  INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit) \n" +
                    "  INNER JOIN pten_cmr.cmrdecode cd ON b.code = cd.cmr_code AND split_part(b.sub_code, '.', 2) = cd.cmr_sub_code AND cd.cmr_sub_sub_code IS NULL \n" +
                    " WHERE b.code = '8' \n" +
                    "   AND UPPER(rm.region_name) IN (" + regionList.toUpperCase() + ") \n" +
                    " GROUP BY rm.region_name, fc.chunk_start, cd.display_color, cd.display_text, cd.cmr_sub_code \n" +
                    " ORDER BY rm.region_name, fc.chunk_start, cd.display_text;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            // More performance tracking
            fetchingtime = System.currentTimeMillis() - k;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }

        log.info("Response Sent" + fetchingtime);


        return myJSONArray;
    }

    public Collection RegionList() {
        Collection myJSONArray = null;

        // Performance Tracking
        long fetchingtime = 0L;
        long k = System.currentTimeMillis();

        try {
            // Query and retrieval
            String query = "SELECT DISTINCT rm.region_name " +
                    "FROM pten_cmr.RegionMap rm " +
                    "INNER JOIN pten_cmr.cmr_data_usalt7 a ON  UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit) " +
                    "inner join (" +
                    "select well_num,max(report_Date) as max_report_date " +
                    "from pten_cmr.cmr_data_usalt7 " +
                    "group by well_num" +
                    ") b on a.well_num=b.well_num and a.report_date=b.max_report_date " +
                    "INNER JOIN user_security.user_mapping_Details u ON  a.well_num=u.well_num where u.userid=" + getUser_id() + " " +
                    "ORDER BY rm.region_name;";

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            // More performance tracking
            fetchingtime = System.currentTimeMillis() - k;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }

        log.info("Response Sent" + fetchingtime);


        return myJSONArray;
    }

    public Object testDates(Date from, Date to) {
        Instant fromInstant = from.toInstant();

        LocalDate localDate = fromInstant.atZone(ZoneId.systemDefault()).toLocalDate();

        YearMonth month = YearMonth.from(localDate);
        LocalDate start = month.atDay(1);
        LocalDate end = month.atEndOfMonth();


        List dates = new ArrayList();
        dates.add(localDate.atStartOfDay());
        dates.add(start.atStartOfDay().toInstant(ZoneOffset.UTC).toEpochMilli());
        dates.add(start.atStartOfDay().toInstant(ZoneOffset.UTC).getEpochSecond());
        dates.add(end.atStartOfDay().toInstant(ZoneOffset.UTC).toEpochMilli());
        dates.add(end.atStartOfDay().toInstant(ZoneOffset.UTC).getEpochSecond());
        dates.add(start.toString());
        dates.add(end.toString());
        dates.add(start);
        dates.add(end);
        dates.add(end.atStartOfDay().toLocalDate());
        dates.add(end.atTime(LocalTime.MAX));
        dates.add(end.atTime(LocalTime.MAX).toInstant(ZoneOffset.UTC).getEpochSecond()); //at end of day
        dates.add(end.format(DateTimeFormatter.ofPattern("MMM d,uuuu"))); //npt hours by week format
        dates.add(end.format(DateTimeFormatter.ofPattern("MMM uuuu"))); //npt hours by week format
        dates.add(end.format(DateTimeFormatter.ofPattern("'Q'q uuuu"))); //npt hours by quarter

        dates.add(end.atTime(LocalTime.MAX).toInstant(ZoneOffset.of("+05:00")));

        log.info("{}", TimeZone.getTimeZone("America/Chicago").getID());
        //dates.add(end.atTime(LocalTime.MAX).toInstant(ZoneOffset.of(ZoneOffset.ofOffset()) .UTC.of("+05:00")));

        return dates;
    }

    public Collection NPTHoursArbitraryRegionsTotal(String starttime, String endtime, String[] regionlist) {
        // BJS 20180627 - This one's pretty straightforward - it just pulls all the hours under
        //                an arbitrary list of regions for a span of time, groups them, and returns
        //                that output.  It currently orders by code, but it's just as easy to have
        //                it order by hours if that's what we decide we want (I'll even include the code
        //                section down there, so that we can just switch comments).
        //
        //                The idea was that this would be used for the legend at the bottom of the
        //                arbitrary region set column charts page which may or may not include a pie.

        Collection myJSONArray = null;

        try {
            // Date Conversion
            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            // Array to IN list
            String regionList = myUtils.arrayToInClause(regionlist);

            // Query and retrieval
            String query = "SELECT cmrd.cmr_sub_code, cmrd.display_text, cmrd.display_color, SUM(b.hours) AS npt_hours \n" +
                    "  FROM pten_cmr.cmr_time_code_data_usalt7 b \n" +
                    "	INNER JOIN pten_cmr.cmrdecode cmrd ON b.code = cmrd.cmr_code AND split_part(b.sub_code, '.', 2) = cmrd.cmr_sub_code AND cmrd.cmr_sub_sub_code IS NULL \n" +
                    "	INNER JOIN pten_cmr.cmr_data_usalt7 a ON a.cmr_data_id = b.cmr_data_id inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                    "	INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit) \n" +
                    " WHERE u.userid=" + getUser_id() + " and UPPER(rm.region_name) IN (" + regionList.toUpperCase() + ") \n" +
                    "   AND UPPER(b.sub_code) NOT LIKE '%Z%' \n" +
                    "   AND b.code = '8' \n" +
                    "   AND (b.`from` BETWEEN " + startTime + " AND " + endTime + " OR b.`to` BETWEEN " + startTime + " AND " + endTime + ") \n" +
                    " GROUP BY cmrd.cmr_sub_code, cmrd.display_text, cmrd.display_color \n" +
                    " ORDER BY cmrd.cmr_sub_code; ";

            myJSONArray = myUtils.runSQLGetJSONArray(query);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }


        return myJSONArray;
    }

    public String cmrOperator(String starttime, String endtime) {
        Collection json = new ArrayList();
        long fetchingtime = 0L;
        long total = 0L;

        try (Connection con = myUtils.connect(); Statement stmt = con.createStatement()) {
            long endTime = DateTimeUtils.dateToEpoch(endtime);
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");//dd/MM/yyyy
            sdfDate.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date now = new Date();
            long startTime = Math.min(DateTimeUtils.dateToEpoch(starttime), DateTimeUtils.dateToEpoch(sdfDate.format(now)) - 259200);
            stmt.setFetchSize(6000);
            long k = System.currentTimeMillis();

            String query = "SELECT DISTINCT REGEXP_REPLACE(OPERATOR_NAME,',','') AS OPERATOR_NAME " +
                    "FROM pten_cmr.cmr_data_usalt7 a " +
                    "inner join user_Security.user_mapping_Details u on a.well_num=u.well_num and u.userid=" + getUser_id() + " " +
                    "WHERE operator_name<>'' AND operator_name <> '1-UNKNOWN OPERATOR' and report_date between " + startTime + " and " + endTime + " " +
                    "ORDER BY operator_name asc;";

            ResultSet rs = stmt.executeQuery(query);
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            JSONObject obj;
            while (rs.next()) {
                obj = new JSONObject();
                obj.put("id", rs.getString("operator_name")); // api is sending json response as id and name as required in UI api
                obj.put("name", rs.getString("operator_name"));
                json.add(obj);
            }
            total = System.currentTimeMillis() - start;
            rs.close();
            stmt.close();
            con.close();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            json.add(ex.getMessage());
        }
        log.info("Response Sent " + fetchingtime + " " + total);
        return json.toString();
    }

    public String cmrRegionList(String operator, String starttime, String endtime) {
        Collection json = new ArrayList();
        long fetchingtime = 0L;
        long total = 0L;
        try (Connection con = myUtils.connect(); Statement stmt = con.createStatement()) {
            long endTime = DateTimeUtils.dateToEpoch(endtime);
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");//dd/MM/yyyy
            sdfDate.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date now = new Date();
            long startTime = Math.min(DateTimeUtils.dateToEpoch(starttime), DateTimeUtils.dateToEpoch(sdfDate.format(now)) - 259200);
            operator = ("'" + operator).replaceAll(",", "','") + "'";
            stmt.setFetchSize(6000);
            long k = System.currentTimeMillis();

            String query = "select distinct region_name " +
                    "from pten_cmr.regionmap r " +
                    "join pten_Cmr.cmr_data_usalt7 a on UPPER(a.contractor_business_unit) = UPPER(r.contractor_business_unit)  " +
                    "inner join user_Security.user_mapping_Details u on a.well_num = u.well_num and u.userid=" + getUser_id() + " " +
                    "WHERE REGEXP_REPLACE(OPERATOR_NAME,',','') in (" + operator + ") and report_date between " + startTime + " and " + endTime + ";";

            ResultSet rs = stmt.executeQuery(query);
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            JSONObject obj;
            while (rs.next()) {
                obj = new JSONObject();
                obj.put("id", rs.getString("region_name")); // api is sending json response as id and name as required in UI api
                obj.put("name", rs.getString("region_name"));
                json.add(obj);
            }
            total = System.currentTimeMillis() - start;
            rs.close();
            stmt.close();
            con.close();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            json.add(ex.getMessage());
        }
        log.info("Response Sent " + fetchingtime + " " + total);
        return json.toString();
    }

    public String cmrStateList(String starttime, String endtime) {
        Collection json = new ArrayList();
        long fetchingtime = 0L;
        long total = 0L;
        try (Connection con = myUtils.connect(); Statement stmt = con.createStatement()) {
            stmt.setFetchSize(6000);
            long endTime = DateTimeUtils.dateToEpoch(endtime+ " 00:00:00");
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");//dd/MM/yyyy
            sdfDate.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date now = new Date();
            long startTime = Math.min(DateTimeUtils.dateToEpoch(starttime+ " 00:00:00"), DateTimeUtils.dateToEpoch(sdfDate.format(now)) - 259200);
            long k = System.currentTimeMillis();

            String query = "SELECT DISTINCT STATE_PROVINCE " +
                    "FROM pten_cmr.cmr_data_usalt7 a " +
                    "join pten_cmr.regionmap r  on r.contractor_business_unit = a.contractor_business_unit " +
                    "inner join user_Security.user_mapping_Details u on a.well_num = u.well_num and u.userid=" + getUser_id() + " " +
                    "WHERE report_date between " + startTime + " and " + endTime + ";";

            ResultSet rs = stmt.executeQuery(query);
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            JSONObject obj;
            while (rs.next()) {
                obj = new JSONObject();
                obj.put("state", rs.getString("state_province"));
                json.add(obj);
            }
            total = System.currentTimeMillis() - start;
            rs.close();
            stmt.close();
            con.close();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            json.add(ex.getMessage());
        }

        log.info("Response Sent " + fetchingtime + " " + total);
        return json.toString();
    }

    public String cmrCountyList(String stateProvince[], String starttime, String endtime) {
        Collection json = new ArrayList();
        long fetchingtime = 0L;
        long total = 0L;
        try (Connection con = myUtils.connect(); Statement stmt = con.createStatement()) {
            String stateProvinceList = myUtils.arrayToInClause(stateProvince);
            long endTime = DateTimeUtils.dateToEpoch(endtime + " 00:00:00");
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");//dd/MM/yyyy
            sdfDate.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date now = new Date();
            long startTime = Math.min(DateTimeUtils.dateToEpoch(starttime+ " 00:00:00"), DateTimeUtils.dateToEpoch(sdfDate.format(now)) - 259200);
            stmt.setFetchSize(6000);
            long k = System.currentTimeMillis();

            String query = "SELECT DISTINCT COUNTY " +
                    "FROM pten_cmr.cmr_data_usalt7 d " +
                    "join pten_cmr.regionmap r  on r.contractor_business_unit=d.contractor_business_unit " +
                    "WHERE UPPER(STATE_PROVINCE) IN (" + stateProvinceList.toUpperCase() + ") AND report_date between " + startTime + " and " + endTime + ";";

            ResultSet rs = stmt.executeQuery(query);
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            JSONObject obj;
            while (rs.next()) {
                obj = new JSONObject();
                obj.put("county", rs.getString("county"));
                json.add(obj);
            }
            total = System.currentTimeMillis() - start;
            rs.close();
            stmt.close();
            con.close();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            json.add(ex.getMessage());
        }

        log.info("Response Sent " + fetchingtime + " " + total );
        return json.toString();
    }

    public Collection cmrCountyList(String region, Date startDate, Date endDate) {

        long startTime = DateTimeUtils.dateToEpoch(startDate);
        long endTime = DateTimeUtils.dateToEpoch(endDate);

        String query = "SELECT concat(county, ', ',state_province ) as county_state_province\n" +
                "FROM pten_cmr.cmr_data_usalt7 d \n" +
                " join pten_cmr.regionmap r  on UPPER(r.contractor_business_unit) = UPPER(d.contractor_business_unit) \n" +
                "WHERE UPPER(r.region_name) in (:region) \n" +
                " and report_date between :startDate and :endDate\n" +
                "group by county, state_province\n" +
                "order by county, state_province";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("region", region.toUpperCase())
                .addValue("startDate", startTime)
                .addValue("endDate", endTime)
                .addValue("user", getUser_id());

        return impalaDbUtil.executeNamedQuery(query, namedParameters);
    }

    public String cmrRigList(String region[], String stateProvince[], String county[], String operator[], String starttime, String endtime) {
        Collection json = new ArrayList();
        long fetchingtime = 0L;
        long total = 0L;
        try (Connection con = myUtils.connect(); Statement stmt = con.createStatement()) {
            long endTime = DateTimeUtils.dateToEpoch(endtime);
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");//dd/MM/yyyy
            sdfDate.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date now = new Date();
            long startTime = Math.min(DateTimeUtils.dateToEpoch(starttime), DateTimeUtils.dateToEpoch(sdfDate.format(now)) - 259200);
            String operatorList = myUtils.arrayToInClause(operator);
            String regionList = myUtils.arrayToInClause(region);
            String stateProvinceList = myUtils.arrayToInClause(stateProvince);
            String countyList = myUtils.arrayToInClause(county);
            stmt.setFetchSize(6000);
            long k = System.currentTimeMillis();
            String query = "SELECT DISTINCT RIG_NAME " +
                    "FROM pten_cmr.cmr_data_usalt7 d " +
                    "join pten_cmr.regionmap r  on UPPER(r.contractor_business_unit) = UPPER(d.contractor_business_unit) " +
                    "WHERE r.region_name in (" + regionList + ") and STATE_PROVINCE IN (" + stateProvinceList + ") AND COUNTY IN (" + countyList + ") AND REGEXP_REPLACE(OPERATOR_NAME,',','') in (" + operatorList + ") and report_date between " + startTime + " and " + endTime + ";";
            ResultSet rs = stmt.executeQuery(query);
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            JSONObject obj;
            while (rs.next()) {
                obj = new JSONObject();
                obj.put("id", rs.getString("rig_name")); // api is sending json response as id and name as required in UI api
                obj.put("name", rs.getString("rig_name"));
                json.add(obj);
            }
            total = System.currentTimeMillis() - start;
            rs.close();
            stmt.close();
            con.close();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            json.add(ex.getMessage());
        }
        log.info("Response Sent " + fetchingtime + " " + total);
        return json.toString();
    }

    public String cmrWellList(String region[], String stateProvince[], String county[], String rigname[], String operator[], String starttime, String endtime) {
        Collection json = new ArrayList();
        long fetchingtime = 0L;
        long total = 0L;
        try (Connection con = myUtils.connect(); Statement stmt = con.createStatement()) {
            long endTime = DateTimeUtils.dateToEpoch(endtime);
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");//dd/MM/yyyy
            sdfDate.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date now = new Date();
            long startTime = Math.min(DateTimeUtils.dateToEpoch(starttime), DateTimeUtils.dateToEpoch(sdfDate.format(now)) - 259200);
            long originalstartTime = DateTimeUtils.dateToEpoch(starttime);
            String operatorList = myUtils.arrayToInClause(operator);
            String regionList = myUtils.arrayToInClause(region);
            String stateProvinceList = myUtils.arrayToInClause(stateProvince);
            String countyList = myUtils.arrayToInClause(county);
            String rignameList = myUtils.arrayToInClause(rigname);
            stmt.setFetchSize(6000);
            long k = System.currentTimeMillis();

            String query = "WITH LatestCMR AS(\n" +
                    "        SELECT a.rig_name\n" +
                    "        , MAX(COALESCE(b.`to`, a.report_date)) AS latest_to \n" +
                    "        FROM pten_cmr.cmr_data_usalt7 a \n" +
                    "        INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                    "WHERE a.rig_name in (" + rignameList + ") " +
                    "GROUP BY a.rig_name\n" +
                    "), LatesterCMR AS (\n" +
                    "        SELECT z.rig_name, z.well_num, z.well_name, z.gps, lcmr.latest_to \n" +
                    "        FROM pten_cmr.cmr_data_usalt7 z\n" +
                    "         INNER JOIN pten_cmr.cmr_time_code_data_usalt7 y ON z.cmr_data_id = y.cmr_data_id \n" +
                    "         INNER JOIN LatestCMR lcmr ON lcmr.rig_name = z.rig_name AND y.`to` = lcmr.latest_to \n" +
                    "         WHERE lcmr.latest_to > " + startTime + "\n" +
                    "), LatestSensor AS (\n" +
                    "        SELECT strright(wd.rig_name, 3) AS rig_name \n" +
                    "        , wd.well_num \n" +
                    "        , wd.well_name \n" +
                    "        , a.gps \n" +
                    "        , wda.last_date_processed \n" +
                    "        FROM ptendrilling.ptendrilling_welldata wd \n" +
                    "        INNER JOIN ptendrilling.ptendrilling_welldata_audit_info wda ON wd.well_num = wda.well_num \n" +
                    "        LEFT OUTER JOIN pten_cmr.cmr_data_usalt7 a ON wd.well_num = a.well_num \n" +
                    "        WHERE wda.last_date_processed > " + startTime + " \n" +
                    "        AND NOT EXISTS (    \n" +
                    "                SELECT NULL \n" +
                    "                FROM ptendrilling.ptendrilling_welldata_audit_info wd2    \n" +
                    "                WHERE wd2.last_date_processed > wda.last_date_processed AND wd2.rig_name = wda.rig_name ) \n" +
                    "        AND NOT EXISTS (    \n" +
                    "                SELECT NULL    \n" +
                    "                FROM pten_cmr.cmr_data_usalt7 q    \n" +
                    "                WHERE a.report_date < q.report_date AND a.well_num = q.well_num )\n" +


                    ") SELECT COALESCE(ls.well_num, lcmr.well_num) as well_num\n" +
                    "        , COALESCE(ls.well_name, lcmr.well_name) AS well_name\n" +
                    "        FROM LatesterCMR lcmr \n" +
                    "        LEFT OUTER JOIN LatestSensor ls ON lcmr.rig_name = ls.rig_name \n" +
                    "        UNION SELECT a.well_num, a.well_name \n" +

                    "        FROM pten_cmr.cmr_data_usalt7 a \n" +
                    "        inner join user_Security.user_mapping_details u on  a.well_num=u.well_num AND u.userid = " + getUser_id() + "\n" +
                    "WHERE a.rig_name IN (" + rignameList + ") and a.report_date BETWEEN " + originalstartTime + " AND " + endTime + ";";
            ;


            ResultSet rs = stmt.executeQuery(query);
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            JSONObject obj;
            while (rs.next()) {
                obj = new JSONObject();
                obj.put("id", rs.getString("well_num")); // api is sending json response as id and name as required in UI api
                obj.put("name", rs.getString("well_name"));
                json.add(obj);
            }
            total = System.currentTimeMillis() - start;
            rs.close();
            stmt.close();
            con.close();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            json.add(ex.getMessage());
        }
        log.info("Response Sent " + fetchingtime + " " + total);
        return json.toString();
    }

    public String drillingRigMovement(String starttime, String endtime, String wellNum) {
        Collection json = new ArrayList();
        long fetchingtime = 0L;
        long total = 0L;
        try (Connection con = myUtils.connect(); Statement stmt = con.createStatement()) {
            if (wellNum.trim().equalsIgnoreCase("")) {
                log.error("Wellnum is null");
                return "Wellnum is null";
            }
            long endTime = DateTimeUtils.dateToEpoch(endtime);
            long startTime = DateTimeUtils.dateToEpoch(starttime) - 43200;
            stmt.setFetchSize(6000);
            long k = System.currentTimeMillis();

            String query = "SELECT distinct a.well_num as R_MOVING " +
                    "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on a.well_num = u.well_num AND u.userid = " + getUser_id() + " \n" +
                    "INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id " +
                    "WHERE (b.`from` BETWEEN " + startTime + " AND " + endTime + " OR b.`to` BETWEEN " + startTime + " AND " + endTime + ")\n " +
                    "AND b.code = '1' " +
                    "AND UPPER(b.sub_code) LIKE '%E%' " +
                    "AND a.well_num in (" + wellNum + ") ;";

            ResultSet rs = stmt.executeQuery(query);
            ResultSetMetaData rsmd = rs.getMetaData();
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            JSONObject obj;
            while (rs.next()) {
                int numColumns = rsmd.getColumnCount();
                obj = new JSONObject();
                for (int i = 1; i < numColumns + 1; i++) {
                    String column_name = rsmd.getColumnName(i);
                    obj.put(column_name.toUpperCase(), rs.getLong(column_name));
                }
                json.add(obj);
            }
            total = System.currentTimeMillis() - start;
            rs.close();
            stmt.close();
            con.close();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            json.add(ex.getMessage());
        }
        log.info("Response Sent " + fetchingtime + " " + total);
        return json.toString();
    }

    public String drillingRigDown(String starttime, String endtime, String wellNum) {
        Collection json = new ArrayList();
        long fetchingtime = 0L;
        long total = 0L;
        try (Connection con = myUtils.connect(); Statement stmt = con.createStatement()) {
            if (wellNum.trim().equalsIgnoreCase("")) {
                log.error("Wellnum is null");
                return "Wellnum is null";
            }
            long endTime = DateTimeUtils.dateToEpoch(endtime);
            long startTime = DateTimeUtils.dateToEpoch(starttime) - 43200;
            stmt.setFetchSize(6000);
            long k = System.currentTimeMillis();
            String query = "SELECT Distinct a.well_num as R_DOWNTIME " +
                    "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on a.well_num = u.well_num AND u.userid = " + getUser_id() + " \n" +
                    "INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id " +
                    "WHERE (b.`from` BETWEEN " + startTime + " AND " + endTime + "  OR b.`to` BETWEEN " + startTime + " AND " + endTime + ")\n " +
                    "AND b.code = '8' " +
                    "AND a.Well_Num in (" + wellNum + ");";
            ResultSet rs = stmt.executeQuery(query);
            ResultSetMetaData rsmd = rs.getMetaData();
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            JSONObject obj;
            while (rs.next()) {
                int numColumns = rsmd.getColumnCount();
                obj = new JSONObject();
                for (int i = 1; i < numColumns + 1; i++) {
                    String column_name = rsmd.getColumnName(i);
                    obj.put(column_name.toUpperCase(), rs.getLong(column_name));
                }
                json.add(obj);
            }

            total = System.currentTimeMillis() - start;
            rs.close();
            stmt.close();
            con.close();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            json.add(ex.getMessage());
        }
        log.info("Response Sent " + fetchingtime + " " + total);
        return json.toString();
    }

    public String rigDrilling_Old(String wellNum) {
        // 2018-08-10 B.Seiler - Altered procedure to ignore well list and only return results for prior 15 minutes

        Collection json = new ArrayList();
        long fetchingtime = 0L;
        long total = 0L;
        try (Connection con = myUtils.connect(); Statement stmt = con.createStatement()) {
            if (wellNum.trim().equalsIgnoreCase("")) {
                log.error("Wellnum is null");
                return "Wellnum is null";
            }
            stmt.setFetchSize(6000);
            long k = System.currentTimeMillis();
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");//dd/MM/yyyy
            sdfDate.setTimeZone(TimeZone.getTimeZone("America/Chicago"));
            Date now = new Date();
            long startTime = DateTimeUtils.dateToEpoch(sdfDate.format(now));

            String query = "SELECT DISTINCT well_num as R_DRILLING FROM ptendrilling.ptendrilling_timedata_last24hrs td INNER JOIN pten_cmr.ptendrilling_d_timezone_offset tz ON td.well_num = tz.well AND td.timepoint BETWEEN " + startTime + " + tz.raw_utc_offset + 21600 - 2100 AND " + startTime + " + tz.raw_utc_offset + 21600  AND td.well_num IN (" + wellNum + ") WHERE td.hole_depth_feet = td.bit_depth_feet AND td.bit_depth_feet > 500.0;";
            ;

            ResultSet rs = stmt.executeQuery(query);
            ResultSetMetaData rsmd = rs.getMetaData();
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            JSONObject obj;
            while (rs.next()) {
                int numColumns = rsmd.getColumnCount();
                obj = new JSONObject();
                for (int i = 1; i < numColumns + 1; i++) {
                    String column_name = rsmd.getColumnName(i);
                    obj.put(column_name.toUpperCase(), rs.getLong(column_name));
                }
                json.add(obj);
            }

            total = System.currentTimeMillis() - start;
            rs.close();
            stmt.close();
            con.close();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            json.add(ex.getMessage());
        }
        log.info("Response Sent " + fetchingtime + " " + total);
        return json.toString();
    }

    public String rigTripping_Old(String wellNum) {

        Collection json = new ArrayList();
        long fetchingtime = 0L;
        long total = 0L;
        try (Connection con = myUtils.connect(); Statement stmt = con.createStatement()) {
            if (wellNum.trim().equalsIgnoreCase("")) {
                log.error("Wellnum is null");
                return "Wellnum is null";
            }
            stmt.setFetchSize(6000);

            long k = System.currentTimeMillis();
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");//dd/MM/yyyy
            sdfDate.setTimeZone(TimeZone.getTimeZone("America/Chicago"));
            Date now = new Date();
            long startTime = DateTimeUtils.dateToEpoch(sdfDate.format(now));

            String query = "WITH TripIdentifier AS (SELECT well_num, ABS(FIRST_VALUE(td.bit_depth_feet) OVER(PARTITION BY well_num ORDER BY td.timepoint) - LAST_VALUE(td.bit_depth_feet) OVER(PARTITION BY well_num ORDER BY td.timepoint)) AS bit_delta FROM ptendrilling.ptendrilling_timedata_last24hrs td INNER JOIN pten_cmr.ptendrilling_d_timezone_offset tz ON td.well_num = tz.well AND td.timepoint BETWEEN " + startTime + " + tz.raw_utc_offset + 21600 - 2100 AND " + startTime + " + tz.raw_utc_offset + 21600 AND td.well_num IN (" + wellNum + ")) SELECT DISTINCT well_num as R_TRIPPING FROM TripIdentifier WHERE bit_delta > 500;";


            ResultSet rs = stmt.executeQuery(query);
            ResultSetMetaData rsmd = rs.getMetaData();
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            JSONObject obj;
            while (rs.next()) {
                int numColumns = rsmd.getColumnCount();
                obj = new JSONObject();
                for (int i = 1; i < numColumns + 1; i++) {
                    String column_name = rsmd.getColumnName(i);
                    obj.put(column_name.toUpperCase(), rs.getLong(column_name));
                }
                json.add(obj);
            }

            total = System.currentTimeMillis() - start;
            rs.close();
            stmt.close();
            con.close();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            json.add(ex.getMessage());
        }
        log.info("Response Sent " + fetchingtime + " " + total);
        return json.toString();
    }

    public String rigsOutOfHole_Old(String wellNum) {

        Collection json = new ArrayList();
        long fetchingtime = 0L;
        long total = 0L;
        try (Connection con = myUtils.connect(); Statement stmt = con.createStatement()) {
            if (wellNum.trim().equalsIgnoreCase("")) {
                log.error("Wellnum is null");
                return "Wellnum is null";
            }
            stmt.setFetchSize(6000);
            long k = System.currentTimeMillis();
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");//dd/MM/yyyy
            sdfDate.setTimeZone(TimeZone.getTimeZone("America/Chicago"));
            Date now = new Date();
            long startTime = DateTimeUtils.dateToEpoch(sdfDate.format(now));

            String query = "SELECT DISTINCT well_num as R_OUT_OF_HOLE FROM ptendrilling.ptendrilling_timedata_last24hrs td INNER JOIN pten_cmr.ptendrilling_d_timezone_offset tz ON td.well_num = tz.well AND td.timepoint BETWEEN " + startTime + " + tz.raw_utc_offset + 21600 - 2100 AND " + startTime + " + tz.raw_utc_offset + 21600  AND td.well_num IN (" + wellNum + ")WHERE td.bit_depth_feet <= 500.0;";

            ResultSet rs = stmt.executeQuery(query);
            ResultSetMetaData rsmd = rs.getMetaData();
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            JSONObject obj;
            while (rs.next()) {
                int numColumns = rsmd.getColumnCount();
                obj = new JSONObject();

                for (int i = 1; i < numColumns + 1; i++) {
                    String column_name = rsmd.getColumnName(i);
                    obj.put(column_name.toUpperCase(), rs.getLong(column_name));
                }
                json.add(obj);
            }
            total = System.currentTimeMillis() - start;
            rs.close();
            stmt.close();
            con.close();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            json.add(ex.getMessage());
        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return json.toString();
    }

    public String rigDrilling(String wellNum) {
        log.info("Inisde rigDrilling " + wellNum);
        Collection json = new ArrayList();
        long fetchingtime = 0L;
        long total = 0L;
        try {
            String[] wellArray = wellNum.split(",");
            HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
            for (int i = 0; i < wellArray.length; i++) {
                String isDrilling = hashOperations.get("KPI$" + wellArray[i], "DRILLING");
                if (isDrilling!=null && isDrilling.equals("1")) {
                    json.add(new JSONObject().put("R_DRILLING",wellArray[i]));
                }
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            json.add(ex.getMessage());
        }
        log.info("Response Sent " + fetchingtime + " " + total);
        String response = json.toString();
        json = null;
        System.gc();
        return response;
    }

    public String rigTripping(String wellNum) {

        Collection json = new ArrayList();
        long fetchingtime = 0L;
        long total = 0L;
        try {
            String[] wellArray = wellNum.split(",");
            HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
            for (int i = 0; i < wellArray.length; i++) {
                String isRigTripping = hashOperations.get("KPI$" + wellArray[i], "TRIPPING");
                if (isRigTripping!=null && isRigTripping.equals("1")) {
                    json.add(new JSONObject().put("R_TRIPPING",wellArray[i]));
                }
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            json.add(ex.getMessage());
        }
        log.info("Response Sent " + fetchingtime + " " + total);
        String response = json.toString();
        json = null;
        System.gc();
        return response;
    }

    public String rigsOutOfHole(String wellNum) {

        Collection json = new ArrayList();
        long fetchingtime = 0L;
        long total = 0L;
        try {
            String[] wellArray = wellNum.split(",");
            HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
            for (int i = 0; i < wellArray.length; i++) {
                String isrigOutOfHole = hashOperations.get("KPI$" + wellArray[i], "OUTOFHOLE");
                if (isrigOutOfHole!=null && isrigOutOfHole.equals("1")) {
                    json.add(new JSONObject().put("R_OUT_OF_HOLE",wellArray[i]));
                }
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            json.add(ex.getMessage());
        }
        log.info("Response Sent " + fetchingtime + " " + total);
        String response = json.toString();
        json = null;
        System.gc();
        return response;
    }

    public String wellRecentlyReleased(String starttime, String endtime, String wellNum) {
        Collection json = new ArrayList();
        long fetchingtime = 0L;
        long total = 0L;
        try (Connection con = myUtils.connect(); Statement stmt = con.createStatement()) {
            if (wellNum.trim().equalsIgnoreCase("")) {
                log.error("Wellnum is null");
                return "Wellnum is null";
            }
            long endTime = DateTimeUtils.dateToEpoch(endtime);
            long startTime = DateTimeUtils.dateToEpoch(starttime);
            stmt.setFetchSize(6000);
            long k = System.currentTimeMillis();

            String query = "SELECT distinct a.well_num as W_RECENTLY_RELEASED " +
                    "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num AND u.userid = " + getUser_id() + " \n" +
                    "WHERE a.release_time BETWEEN " + startTime + " and " + endTime + " AND a.Well_Num in (" + wellNum + ");";

            ResultSet rs = stmt.executeQuery(query);
            ResultSetMetaData rsmd = rs.getMetaData();
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            JSONObject obj;
            while (rs.next()) {
                int numColumns = rsmd.getColumnCount();
                obj = new JSONObject();

                for (int i = 1; i < numColumns + 1; i++) {
                    String column_name = rsmd.getColumnName(i);
                    obj.put(column_name.toUpperCase(), rs.getLong(column_name));
                }
                json.add(obj);
            }
            total = System.currentTimeMillis() - start;
            rs.close();
            stmt.close();
            con.close();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            json.add(ex.getMessage());
        }
        log.info("Response Sent " + fetchingtime + " " + total);
        return json.toString();
    }

    public String wellUpcomingRelease(String starttime, String endtime, String wellNum) {
        Collection json = new ArrayList();
        long fetchingtime = 0L;
        long total = 0L;
        try (Connection con = myUtils.connect(); Statement stmt = con.createStatement()) {
            if (wellNum.trim().equalsIgnoreCase("")) {
                log.error("Wellnum is null");
                return "Wellnum is null";
            }
            long endTime = DateTimeUtils.dateToEpoch(endtime);
            long startTime = DateTimeUtils.dateToEpoch(starttime);
            stmt.setFetchSize(6000);
            long k = System.currentTimeMillis();

            String query = "SELECT distinct a.well_num as W_UPCOMING_RELEASES " +
                    "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num AND u.userid = " + getUser_id() + " \n" +
                    "WHERE a.estimated_release between " + startTime + " and " + endTime + " AND a.Well_Num in (" + wellNum + ");";

            ResultSet rs = stmt.executeQuery(query);
            ResultSetMetaData rsmd = rs.getMetaData();
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            JSONObject obj;
            while (rs.next()) {
                int numColumns = rsmd.getColumnCount();
                obj = new JSONObject();

                for (int i = 1; i < numColumns + 1; i++) {
                    String column_name = rsmd.getColumnName(i);
                    obj.put(column_name.toUpperCase(), rs.getLong(column_name));
                }
                json.add(obj);
            }

            total = System.currentTimeMillis() - start;
            rs.close();
            stmt.close();
            con.close();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            json.add(ex.getMessage());
        }
        log.info("Response Sent " + fetchingtime + " " + total);
        return json.toString();
    }

    public String rigMetaInfo(String starttime, String endtime, String wellNum) {
        Collection json = new ArrayList();
        long fetchingtime = 0L;
        long total = 0L;
        try (Connection con = myUtils.connect(); Statement stmt = con.createStatement()) {
            if (wellNum.trim().equalsIgnoreCase("")) {
                log.error("Wellnum is null");
                return "Wellnum is null";
            }

            stmt.setFetchSize(6000);
            long k = System.currentTimeMillis();

            String query = "WITH LastCMR AS (\n" +
                    " SELECT a.rig_name\n" +
                    "        , a.well_num\n" +
                    "        , a.well_name\n" +
                    "        , a.gps\n" +
                    "        , a.report_date\n" +
                    "        , a.spud_time\n" +
                    "        , a.release_time\n" +
                    "        , a.operator_name\n" +
                    "        , a.state_province\n" +
                    "        , a.county, field\n" +
                    "        , a.target_formation\n" +
                    "        , a.contractor_business_unit\n" +
                    "        , a.operations_at_report_time\n" +
                    "        , a.operations_next_24_hours\n" +
                    "        , a.estimated_release \n" +
                    " FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num AND u.userid = " + getUser_id() + " \n" +

                    " WHERE NOT EXISTS (\n" +
                    "        SELECT NULL \n" +
                    "        FROM pten_cmr.cmr_data_usalt7 z \n" +
                    "        WHERE a.well_num = z.well_num \n" +
                    "        AND z.report_date > a.report_date\n" +
                    " )AND a.well_num IN ( " + wellNum + ")\n" +
                    "), CleanResults AS (\n" +
                    "        SELECT COALESCE(a.rig_name, STRRIGHT(wd.rig_name, 3)) AS rig_name\n" +
                    "          , COALESCE(a.well_num, wd.well_num) AS well_num\n" +
                    "          , COALESCE(a.well_name, wd.well_name) AS well_name\n" +
                    "          , CASE WHEN LENGTH(a.gps) > 21 THEN NULL ELSE CAST(SUBSTRING(a.gps, INSTR(a.gps,' ')\n" +
                    "          , INSTR(a.gps, ' ',INSTR(a.gps,' ') + 1 ) - 2) AS DECIMAL(10,4)) END  AS latitude\n" +
                    "          , CASE WHEN LENGTH(a.gps) > 21 THEN NULL ELSE CAST(STRRIGHT(a.gps,INSTR(REVERSE(a.gps),' ') - 1) AS DECIMAL(10,4)) end AS longitude\n" +
                    "          , a.report_date\n" +
                    "          , COALESCE(from_unixtime(a.spud_time,'yyyy-MM-dd HH:mm'), wd.spud_date) as spudDate\n" +
                    "          , from_unixtime(COALESCE(a.release_time, a.estimated_release),'yyyy-MM-dd HH:mm') as releaseDate\n" +
                    "          , COALESCE(a.operator_name, wd.operator) AS operatorName\n" +
                    "          , a.state_province AS district\n" +
                    "          , a.county\n" +
                    "          , a.field\n" +
                    "          , a.target_formation AS targetFormation\n" +
                    "          , a.contractor_business_unit\n" +
                    "          , a.operations_at_report_time\n" +
                    "          , a.operations_next_24_hours\n" +
                    "        FROM LastCMR a \n" +
                    "        FULL JOIN ptendrilling.ptendrilling_welldata wd ON a.well_num = wd.well_num \n" +
                    "        WHERE COALESCE(a.well_num, wd.well_num) IN ( " + wellNum + " )\n" +
                    "), TopQuery AS (\n" +
                    "        SELECT a.well_num\n" +
                    "          , cmrd.display_text\n" +
                    "          , SUM(b.hours) OVER(PARTITION BY well_num) AS overall_hours\n" +
                    "          , SUM(b.hours) OVER(PARTITION BY well_num, cmrd.display_text) AS code_hours \n" +
                    "        FROM pten_cmr.cmr_data_usalt7 a \n" +
                    "        INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                    "        INNER JOIN pten_cmr.cmrdecode cmrd ON b.code = cmrd.cmr_code \n" +
                    "          AND SPLIT_PART(b.sub_code, '.', 2) = cmrd.cmr_sub_code \n" +
                    "          AND cmrd.cmr_sub_sub_code IS NULL WHERE cmrd.cmr_code = '8' \n" +
                    "          AND a.well_num IN ( " + wellNum + " )\n" +
                    "), MiddleQuery AS (\n" +
                    "        SELECT tq.well_num\n" +
                    "          , tq.display_text\n" +
                    "          , tq.overall_hours\n" +
                    "          , ROW_NUMBER() OVER(PARTITION BY tq.well_num ORDER BY tq.code_hours DESC) AS code_ordinal\n" +
                    "          , tq.code_hours FROM TopQuery tq\n" +
                    "), WellBottomQuery AS (\n" +
                    "        SELECT mq.well_num\n" +
                    "          , mq.display_text\n" +
                    "          , mq.overall_hours\n" +
                    "          , mq.code_hours \n" +
                    "        FROM MiddleQuery mq \n" +
                    "        WHERE mq.code_ordinal = 1\n" +
                    ") SELECT COALESCE(rm.region_name, 'OTHER') AS Region\n" +
                    "        , cr.well_num\n" +
                    "        , cr.rig_name\n" +
                    "        , cr.well_name AS wellName\n" +
                    "        , cr.spudDate\n" +
                    "        , cr.releaseDate\n" +
                    "        , COALESCE(rt.display_text, 'UNAVAILABLE') AS RigTypeInfo\n" +
                    "        , cr.operatorName\n" +
                    "        , cr.district\n" +
                    "        , cr.county\n" +
                    "        , cr.field\n" +
                    "        , cr.targetFormation\n" +
                    "        , cr.operations_at_report_time\n" +
                    "        , cr.operations_next_24_hours\n" +
                    "        , wbq.overall_hours AS total_npt_hours\n" +
                    "        , wbq.display_text AS top_npt_code\n" +
                    "        , wbq.code_hours AS top_npt_code_hours\n" +
                    "        , CASE WHEN (cr.latitude IS NULL OR cr.latitude=0) AND (cr.longitude IS NULL OR cr.longitude=0) THEN 32.7178 ELSE cr.latitude END AS latitude\n" +
                    "        , CASE WHEN (cr.latitude IS NULL OR cr.latitude=0) AND (cr.longitude IS NULL OR cr.longitude=0) THEN -100.9176 ELSE cr.longitude END AS longitude \n" +
                    "FROM CleanResults cr \n" +
                    "LEFT OUTER JOIN WellBottomQuery wbq ON cr.well_num = wbq.well_num \n" +
                    "LEFT OUTER JOIN pten_cmr.regionmap rm  ON UPPER(cr.contractor_business_unit) = UPPER(rm.contractor_business_unit) \n" +
                    "LEFT OUTER JOIN pten_cmr.rig r ON r.display_text = cr.rig_name \n" +
                    "LEFT OUTER JOIN pten_cmr.rigtype rt ON r.rig_type_id = rt.rig_type_id;";

            ResultSet rs = stmt.executeQuery(query);
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            ResultSetMetaData rsmd = rs.getMetaData();
            JSONObject obj;
            while (rs.next()) {
                int numColumns = rsmd.getColumnCount();
                obj = new JSONObject();
                obj.put("well_num", rs.getLong("well_num"));
                obj.put("wellName", rs.getString("wellname"));
                obj.put("spudDate", rs.getString("spuddate"));
                obj.put("releaseDate", rs.getString("releasedate"));
                obj.put("operatorName", rs.getString("operatorname"));
                obj.put("district", rs.getString("district"));
                obj.put("RigTypeInfo", rs.getString("RigTypeInfo"));
                obj.put("county", rs.getString("county"));
                obj.put("Field", rs.getString("field"));
                obj.put("targetFormation", rs.getString("targetformation"));
                obj.put("longitude", rs.getDouble("longitude"));
                obj.put("latitude", rs.getDouble("latitude"));
                obj.put("region", rs.getString("region"));
                obj.put("rigName", "Pat-UTI " + rs.getString("rig_name"));
                obj.put("Previous_Days_Operation", rs.getString("operations_at_report_time"));
                obj.put("Todays_Planned_Operations", rs.getString("operations_next_24_hours"));
                obj.put("totalNPTHours", rs.getDouble("total_npt_hours"));
                obj.put("topNPTCode", rs.getString("top_npt_code"));
                obj.put("topNPTCodeHours", rs.getDouble("top_npt_code_hours"));
                json.add(obj);
            }
            total = System.currentTimeMillis() - start;
            con.close();
            rs.close();
            stmt.close();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            json.add(ex.getMessage());
        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return json.toString();
    }

    public String rigCurrentStatusInfo(String starttime, String endtime, String wellNum) {
        Collection json = new ArrayList();
        long fetchingtime = 0L;
        long total = 0L;
        try (Connection con = myUtils.connect(); Statement stmt = con.createStatement()) {
            if (wellNum.trim().equalsIgnoreCase("")) {
                log.error("Wellnum is null");
                return "Wellnum is null";
            }
            long endTime = DateTimeUtils.dateToEpoch(endtime);
            long startTime = DateTimeUtils.dateToEpoch(starttime);
            stmt.setFetchSize(6000);
            long k = System.currentTimeMillis();
            String query = "Select C.Rig_Name, C.Well_num, from_unixtime(Max(C.Report_Date),'yyyy-MM-dd') as lastCMRDate From pten_cmr.cmr_data_usalt7 C " +
                    " inner join user_Security.user_mapping_details u on  u.well_num = C.well_num AND u.userid = " + getUser_id() +
                    " join pten_cmr.rig r on C.rig_name=r.display_text inner join pten_cmr.rigtype rt on r.rig_type_id=rt.rig_type_id Where C.Well_Num in (" + wellNum + ") and C.Report_Date between " + startTime + " and " + endTime + " Group by C.Rig_name,C.Well_num; ";
            ResultSet rs = stmt.executeQuery(query);
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            ResultSetMetaData rsmd = rs.getMetaData();
            JSONObject obj;
            while (rs.next()) {
                int numColumns = rsmd.getColumnCount();
                obj = new JSONObject();
                obj.put("well_num", rs.getLong("well_num"));
                obj.put("lastCMRDate", rs.getString("lastcmrdate"));
                obj.put("rigName", "Pat-UTI " + rs.getString("rig_name"));

                json.add(obj);
            }
            log.info(json.toString());
            total = System.currentTimeMillis() - start;
            con.close();
            rs.close();
            stmt.close();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            json.add(ex.getMessage());
        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return json.toString();
    }

    public Collection vSWellList(String rig_name[], String starttime, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;
        try (Connection con = myUtils.connect(); Statement stmt = con.createStatement()) {
            String rig_nameList = myUtils.arrayToInClause(rig_name);
            long endTime = DateTimeUtils.dateToEpoch(endtime + " 06:00");
            long startTime = endTime - 86400;
            stmt.setFetchSize(6000);
            long k = System.currentTimeMillis();

            String query = "With ActiveWell as (" +
                    "Select distinct well_num " +
                    "from ptendrilling.ptendrilling_timedata " +
                    "where timepoint between " + startTime + " and " + endTime +
                    ") " +
                    "Select w.Well_Name" +
                    ",w.Well_Num " +
                    "from pten_cmr.cmr_data_usalt7 w inner join user_Security.user_mapping_details u on  w.well_num=u.well_num inner " +
                    "join ActiveWell AW on w.well_num=AW.well_num  " +
                    "Where u.userid=" + getUser_id() + " and  w.Rig_Name in (" + rig_nameList + ") " +
                    "Group By w.Well_Name,w.Well_Num " +
                    "Order by Max(w.Report_Date) Desc;";

            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();

            myJSONArray = myUtils.runSQLGetJSONArray(query);

            total = System.currentTimeMillis() - start;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;
    }

    public Collection patDPRRigList() {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;

        try {

            long k = System.currentTimeMillis();

            String query = "SELECT DISTINCT trim(D.rig_name) as rig_name FROM PTENDRILLING.PTENDRILLING_DAYWISE_SUMMARY D INNER JOIN USER_SECURITY.USER_MAPPING_DETAILS U ON D.WELL_NUM=U.WELL_NUM WHERE U.USERID=" + getUser_id() + "  ORDER BY trim(D.RIG_NAME);";

            myJSONArray = myUtils.runSQLGetJSONArray(query);
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();

            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;
    }

    public Collection patDPRWellList(String rig_name, String endtime) {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;

        try {
            long endTime = DateTimeUtils.dateToEpoch(endtime + " 00:00");
            long startTime = endTime - 86400;
            long k = System.currentTimeMillis();

            String query = "SELECT DISTINCT D.well_num,trim(D.Well_name) as well_name FROM PTENDRILLING.PTENDRILLING_DAYWISE_SUMMARY D INNER JOIN USER_SECURITY.USER_MAPPING_DETAILS U ON D.WELL_NUM=U.WELL_NUM WHERE U.USERID=" + getUser_id() + " AND D.RIG_NAME='" + rig_name + "' AND D.TIMEPOINT > " + startTime + " AND D.TIMEPOINT <=" + endTime + " ORDER BY trim(D.WELL_NAME);";
            myJSONArray = myUtils.runSQLGetJSONArray(query);
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();

            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);

        }
        log.info("Response Sent " + fetchingtime + " " + total);

        return myJSONArray;
    }

    public Collection patCMRRigList() {
        Collection myJSONArray = null;
        long fetchingtime = 0L;
        long total = 0L;

        try {
            long k = System.currentTimeMillis();

            String query = "select distinct C.rig_name " +
                    "from pten_cmr.cmr_data_usalt7 C INNER JOIN user_security.user_mapping_Details U on  C.Well_Num=U.Well_num Where U.userid=" + getUser_id() +
                    " order by C.rig_name";

            myJSONArray = myUtils.runSQLGetJSONArray(query);
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();

            total = System.currentTimeMillis() - start;

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);

        }
        log.info("Response Sent " + fetchingtime + " " + total);


        return myJSONArray;
    }

    public String totalRigList(String wellNum) {
        Collection json = new ArrayList();
        long fetchingtime = 0L;
        long total = 0L;
        try (Connection con = myUtils.connect(); Statement stmt = con.createStatement()) {
            stmt.setFetchSize(6000);
            long k = System.currentTimeMillis();

            String query = "SELECT COUNT(DISTINCT a.rig_name) AS total_rigs \n" +
                    "FROM pten_cmr.cmr_data_usalt7 a \n" +
                    "WHERE NOT EXISTS (\n" +
                    "  SELECT NULL FROM pten_cmr.cmr_data_usalt7 b \n" +
                    "  WHERE b.report_date > a.report_date and a.well_num = b.well_num\n" +
                    ") AND a.report_date > unix_timestamp(now()) - 302400;";

            ResultSet rs = stmt.executeQuery(query);
            ResultSetMetaData rsmd = rs.getMetaData();
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            JSONObject obj;
            while (rs.next()) {
                int numColumns = rsmd.getColumnCount();
                obj = new JSONObject();
                for (int i = 1; i < numColumns + 1; i++) {
                    String column_name = rsmd.getColumnName(i);
                    obj.put(column_name.toUpperCase(), rs.getInt(column_name));
                }
                json.add(obj);
            }

            total = System.currentTimeMillis() - start;
            rs.close();
            stmt.close();
            con.close();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            json.add(ex.getMessage());
        }
        log.info("Response Sent " + fetchingtime + " " + total);
        return json.toString();
    }

    public Collection getLastCompletedWells(String rig, int limit) {
        StringBuilder query = new StringBuilder();
        query.append(
                "with LastCompletedWells as (\n" +
                        "SELECT distinct a.well_num\n" +
                        " ,a.well_name\n" +
                        " ,a.rig_name\n" +
                        " ,a.release_time\n" +
                        "from pten_cmr.vw_cmr_data_usalt7_release a inner join user_Security.user_mapping_details u on  a.well_num = u.well_num AND u.userid = " + getUser_id() + " \n" +
                        "where release_time is not null\n" +
                        "and not exists (select null from pten_cmr.vw_cmr_data_usalt7_release z where a.well_num = z.well_num and a.report_date < z.report_date)\n" +
                        ")\n" +
                        "select well_num\n" +
                        ", to_timestamp(release_time)\n" +
                        ",well_name\n" +
                        "from LastCompletedWells\n" +
                        "where rig_name = :rig\n" +
                        "order by release_time desc");
        if (limit > 0) {
            query.append(" limit :limit");
        }
        query.append(";");

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("rig", rig)
                .addValue("limit", limit);

        return impalaDbUtil.executeNamedQuery(query.toString(), namedParameters);
    }

    public Collection getWells(Collection<Long> wells) {
        StringBuilder query = new StringBuilder();
        query.append(
                "SELECT a.well_num\n" +
                        ", a.well_name\n" +
                        ", CONCAT('Well ', CAST(ROW_NUMBER() OVER(ORDER BY a.release_time DESC) AS STRING)) AS well_alias\n" +
                        "FROM pten_cmr.vw_cmr_data_usalt7_release a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                        "WHERE u.userid=" + getUser_id() + " and  a.well_num IN (:wells)\n" +
                        "   AND NOT EXISTS (\n" +
                        "       SELECT NULL\n" +
                        "       FROM pten_cmr.vw_cmr_data_usalt7_release z\n" +
                        "       WHERE a.well_num = z.well_num\n" +
                        "           AND a.report_date < z.report_date)\n" +
                        "order by a.release_time desc");
        query.append(";");

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells);

        return impalaDbUtil.executeNamedQuery(query.toString(), namedParameters);
    }



    //Percent
    public Collection getNptPercent(Collection<Long> wells) {
        String query =
                "--NPT % Chart\n" +
                        "WITH ThisQueryIsUnsortableBecauseForSomeBizarreReasonWeGetANANLine AS (\n" +
                        "SELECT YEAR(to_timestamp(b.`from`)) AS year\n" +
                        "                , MONTH(to_timestamp(b.`from`)) AS month\n" +
                        "                , SUM(CASE WHEN b.code = '8' AND UPPER(b.sub_code) NOT LIKE '%Z%' THEN b.hours ELSE 0 END) / SUM(CASE WHEN UPPER(b.sub_code) NOT LIKE '%Z%' THEN b.hours ELSE 0 END) * 100.0 AS pct\n" +
                        "                                                                , SUM(CASE WHEN b.code = '8' AND UPPER(b.sub_code) NOT LIKE '%Z%' THEN b.hours ELSE 0 END) AS npt_total\n" +
                        "                                                                , SUM(CASE WHEN UPPER(b.sub_code) NOT LIKE '%Z%' THEN b.hours ELSE 0 END) AS hours_total\n" +
                        "                , CASE MONTH(to_timestamp(b.`from`))\n" +
                        "                                WHEN 1 THEN 'Jan'\n" +
                        "                                WHEN 2 THEN 'Feb'\n" +
                        "                                WHEN 3 THEN 'Mar'\n" +
                        "                                WHEN 4 THEN 'Apr'\n" +
                        "                                WHEN 5 THEN 'May'\n" +
                        "                                WHEN 6 THEN 'Jun'\n" +
                        "                                WHEN 7 THEN 'Jul'\n" +
                        "                                WHEN 8 THEN 'Aug'\n" +
                        "                               WHEN 9 THEN 'Sep'\n" +
                        "                                WHEN 10 THEN 'Oct'\n" +
                        "                                WHEN 11 THEN 'Nov'\n" +
                        "                                WHEN 12 THEN 'Dec'\n" +
                        "                  END AS month_short_text\n" +
                        "                , CASE MONTH(to_timestamp(b.`from`))\n" +
                        "                                WHEN 1 THEN 'January'\n" +
                        "                                WHEN 2 THEN 'February'\n" +
                        "                                WHEN 3 THEN 'March'\n" +
                        "                                WHEN 4 THEN 'April'\n" +
                        "                               WHEN 5 THEN 'May'\n" +
                        "                                WHEN 6 THEN 'June'\n" +
                        "                                WHEN 7 THEN 'July'\n" +
                        "                                WHEN 8 THEN 'August'\n" +
                        "                                WHEN 9 THEN 'September'\n" +
                        "                                WHEN 10 THEN 'October'\n" +
                        "                                WHEN 11 THEN 'November'\n" +
                        "                                WHEN 12 THEN 'December'\n" +
                        "                   END AS month_long_text\n" +
                        "  FROM pten_cmr.cmr_time_code_data_usalt7 b\n" +
                        "                INNER JOIN pten_cmr.cmr_data_usalt7 a ON b.cmr_data_id = a.cmr_data_id inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                        "WHERE u.userid=" + getUser_id() + " and  a.well_num IN (:wells)\n" +
                        "   AND b.hours IS NOT NULL\n" +
                        "GROUP BY YEAR(to_timestamp(b.`from`))\n" +
                        "                  , MONTH(to_timestamp(b.`from`))\n" +
                        ")\n" +
                        "SELECT CAST(year as string) as year\n" +
                        "                , CAST(month AS INT) AS month\n" +
                        "                , pct\n" +
                        "                , month_short_text\n" +
                        "                , month_long_text                                                          \n" +
                        "                                                                , (SUM(npt_total) OVER() / SUM(hours_total) OVER()) * 100.0 AS overall_avg\n" +
                        "FROM ThisQueryIsUnsortableBecauseForSomeBizarreReasonWeGetANANLine\n" +
                        "WHERE year IS NOT NULL\n" +
                        "order by year, month";


        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells);

        return impalaDbUtil.executeNamedQuery(query, namedParameters);
    }

    //NPTWellHours
    public Collection getNptHours(Collection<Long> wells) {
        String query =
                "SELECT YEAR(to_timestamp(b.`from`)) AS npt_year\n" +
                        "                , MONTH(to_timestamp(b.`from`)) AS npt_month\n" +
                        "                , CASE MONTH(to_timestamp(b.`from`))\n" +
                        "                                WHEN 1 THEN 'Jan'\n" +
                        "                                WHEN 2 THEN 'Feb'\n" +
                        "                                WHEN 3 THEN 'Mar'\n" +
                        "                                WHEN 4 THEN 'Apr'\n" +
                        "                                WHEN 5 THEN 'May'\n" +
                        "                                WHEN 6 THEN 'Jun'\n" +
                        "                                WHEN 7 THEN 'Jul'\n" +
                        "                                WHEN 8 THEN 'Aug'\n" +
                        "                                WHEN 9 THEN 'Sep'\n" +
                        "                                WHEN 10 THEN 'Oct'\n" +
                        "                                WHEN 11 THEN 'Nov'\n" +
                        "                                WHEN 12 THEN 'Dec'\n" +
                        "                  END AS npt_month_short_text\n" +
                        "                , CASE MONTH(to_timestamp(b.`from`))\n" +
                        "                                WHEN 1 THEN 'January'\n" +
                        "                                WHEN 2 THEN 'February'\n" +
                        "                                WHEN 3 THEN 'March'\n" +
                        "                                WHEN 4 THEN 'April'\n" +
                        "                                WHEN 5 THEN 'May'\n" +
                        "                                WHEN 6 THEN 'June'\n" +
                        "                                WHEN 7 THEN 'July'\n" +
                        "                                WHEN 8 THEN 'August'\n" +
                        "                                WHEN 9 THEN 'September'\n" +
                        "                                WHEN 10 THEN 'October'\n" +
                        "                                WHEN 11 THEN 'November'\n" +
                        "                                WHEN 12 THEN 'December'\n" +
                        "                  END AS npt_month_long_text\n" +
                        "                , cmrd.display_color\n" +
                        "                , cmrd.display_text\n" +
                        "                , SUM(b.`hours`) AS npt_hours\n" +
                        "  FROM pten_cmr.cmr_time_code_data_usalt7 b\n" +
                        "                INNER JOIN pten_cmr.cmr_data_usalt7 a ON b.cmr_data_id = a.cmr_data_id inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                        "                INNER JOIN pten_cmr.cmrdecode cmrd ON b.code = cmrd.cmr_code\n" +
                        "\t                AND split_part(b.sub_code, '.', 2) = cmrd.cmr_sub_code\n" +
                        "\t                AND cmrd.cmr_sub_sub_code IS NULL\n" +
                        "WHERE u.userid=" + getUser_id() + " and  a.well_num IN ( :wells)\n" +
                        "   AND UPPER(b.sub_code) NOT LIKE '%Z%'\n" +
                        "   AND b.hours IS NOT NULL\n" +
                        "   AND b.code = '8'\n" +
                        "GROUP BY YEAR(to_timestamp(b.`from`))\n" +
                        "                  , MONTH(to_timestamp(b.`from`))\n" +
                        "                  , cmrd.display_color\n" +
                        "                  , cmrd.display_text\n" +
                        "ORDER BY npt_year\n" +
                        "                  , npt_month\n" +
                        "                  , cmrd.display_color";


        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells);

        return impalaDbUtil.executeNamedQuery(query, namedParameters);
    }

    /**
     * --NPT Percent Breakdown
     *
     * @param wells
     * @return hours of wells by subcode
     */
    public Collection getNptHoursBySubCode(Collection<Long> wells) {
        String query = "SELECT cmrd.cmr_sub_code\n" +
                "                , cmrd.display_text\n" +
                "                , cmrd.display_color\n" +
                "                , SUM(b.hours) AS npt_sub_code_hours\n" +
                " FROM pten_cmr.cmr_time_code_data_usalt7 b\n" +
                "                INNER JOIN pten_cmr.cmrdecode cmrd ON b.code = cmrd.cmr_code\n" +
                "                   AND split_part(b.sub_code, '.', 2) = cmrd.cmr_sub_code\n" +
                "                   AND cmrd.cmr_sub_sub_code IS NULL\n" +
                "                INNER JOIN pten_cmr.cmr_data_usalt7 a ON a.cmr_data_id = b.cmr_data_id  inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                " WHERE u.userid=" + getUser_id() + " and  a.well_num IN (:wells)\n" +
                "   AND b.code = '8'\n" +
                "   AND UPPER(b.sub_code) NOT LIKE '%Z%'\n" +
                "GROUP BY cmrd.cmr_sub_code\n" +
                "                  , cmrd.display_text\n" +
                "                  , cmrd.display_color\n" +
                "ORDER BY npt_sub_code_hours desc";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells);

        return impalaDbUtil.executeNamedQuery(query, namedParameters);
    }

    public Collection getNptHoursAverage(Collection<Long> wells) {
        String query = "SELECT cmrd.cmr_code\n" +
                "                , cmrd.display_text\n" +
                "                , cmrd.display_color\n" +
                "                , SUM(b.hours) AS npt_code_hours\n" +
                "  FROM pten_cmr.cmr_time_code_data_usalt7 b\n" +
                "                INNER JOIN pten_cmr.cmrdecode cmrd ON b.code = cmrd.cmr_code\n" +
                "                   AND cmrd.cmr_sub_code IS NULL\n" +
                "                   AND cmrd.cmr_sub_sub_code IS NULL\n" +
                "                INNER JOIN pten_cmr.cmr_data_usalt7 a ON a.cmr_data_id = b.cmr_data_id  inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                "WHERE u.userid= " + getUser_id() + " and  a.well_num IN (:wells)\n" +
                "   AND UPPER(b.sub_code) NOT LIKE '%Z%'\n" +
                "GROUP BY cmrd.cmr_code\n" +
                "                  , cmrd.display_text\n" +
                "                  , cmrd.display_color\n" +
                "ORDER BY npt_code_hours desc;";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells);

        return impalaDbUtil.executeNamedQuery(query, namedParameters);
    }

    public Collection getSubcodeColors() {
        String query = "-- subcode colors\n" +
                "SELECT cmrd.display_color as color\n" +
                "\t , cmrd.display_text as name\n" +
                "FROM  pten_cmr.cmrdecode cmrd\n" +
                "GROUP BY  cmrd.display_color\n" +
                "\t\t, cmrd.display_text\n" +
                "ORDER BY cmrd.display_text;";

        return impalaDbUtil.executeNamedQuery(query, new MapSqlParameterSource());

    }

    public HashMap cmrAllFilterData(String starttime, String endtime) {
        JSONArray json = new JSONArray();
        long fetchingtime = 0L;
        long total = 0L;
        HashMap<String, HashMap<String, HashMap<String, HashMap<String, HashMap<String, HashMap<String, String>>>>>> mainMap = new HashMap<>();

        try (Connection con = myUtils.connect(); Statement stmt = con.createStatement()) {

            long endTime = DateTimeUtils.dateToEpoch(endtime);
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");//dd/MM/yyyy
            sdfDate.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date now = new Date();
            long startTime = DateTimeUtils.toStartDay(starttime);
            long originalstartTime = DateTimeUtils.dateToEpoch(starttime);

            stmt.setFetchSize(6000);
            long k = System.currentTimeMillis();

            String query = "WITH LatestCMR AS(" +
                    "  SELECT a.rig_name" +
                    "   , a.well_num" +
                    "   ,MAX(COALESCE(b.`to`, a.report_date)) AS latest_to " +
                    "  FROM pten_cmr.cmr_data_usalt7 a INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id " +
                    "  WHERE COALESCE(b.`from`, a.report_date)>=" + startTime + " AND COALESCE(b.`to`, a.report_date)<" + endTime +
                    "  GROUP BY a.rig_name,a.well_num" +
                    "), LatesterCMR AS (" +
                    "  SELECT z.rig_name" +
                    "    , z.well_num" +
                    "    , z.well_name" +
                    "    , z.gps" +
                    "    , lcmr.latest_to " +
                    "  FROM pten_cmr.cmr_data_usalt7 z " +
                    "  INNER JOIN pten_cmr.cmr_time_code_data_usalt7 y ON z.cmr_data_id = y.cmr_data_id " +
                    "  INNER JOIN LatestCMR lcmr ON lcmr.rig_name = z.rig_name " +
                    "  AND lcmr.well_num=z.well_num " +
                    "  AND COALESCE(y.`to`,z.Report_Date) = lcmr.latest_to WHERE lcmr.latest_to >= " + startTime + "" +
                    "), LatestSensor AS (" +
                    "  SELECT strright(wd.rig_name, 3) AS rig_name " +
                    "    , wd.well_num " +
                    "    , wd.well_name " +
                    "    , a.gps " +
                    "    , wda.last_date_processed " +
                    "  FROM ptendrilling.ptendrilling_welldata wd " +
                    "  INNER JOIN ptendrilling.ptendrilling_welldata_audit_info wda ON wd.well_num = wda.well_num " +
                    "  LEFT OUTER JOIN pten_cmr.cmr_data_usalt7 a ON wd.well_num = a.well_num " +
                    "  WHERE wda.last_date_processed >= " + startTime + " " +
                    "    AND NOT EXISTS ( " +
                    "     SELECT NULL FROM ptendrilling.ptendrilling_welldata_audit_info wd2 " +
                    "     WHERE wd2.last_date_processed > wda.last_date_processed " +
                    "     AND wd2.rig_name = wda.rig_name " +
                    "     AND wd2.well_num=wda.well_num" +
                    "    ) " +
                    "    AND NOT EXISTS ( " +
                    "     SELECT NULL " +
                    "     FROM pten_cmr.cmr_data_usalt7 q " +
                    "     WHERE a.report_date < q.report_date AND a.well_num = q.well_num" +
                    "    )" +
                    ") , WellList as (" +
                    "  SELECT COALESCE(ls.well_num, lcmr.well_num) AS well_num " +
                    "    , COALESCE(ls.well_name, lcmr.well_name) AS well_name " +
                    "  FROM LatesterCMR lcmr " +
                    "  LEFT OUTER JOIN LatestSensor ls ON lcmr.rig_name = ls.rig_name " +
                    "   AND lcmr.well_num=ls.well_num " +
                    "  UNION SELECT well_num, well_name " +
                    "  FROM pten_cmr.cmr_data_usalt7 " +
                    "  WHERE report_date BETWEEN " + originalstartTime + " AND " + endTime + "" +
                    ") Select Distinct REGEXP_REPLACE(z.OPERATOR_NAME,',','') AS OPERATOR_NAME" +
                    "   ,r.region_name" +
                    "   ,z.STATE_PROVINCE" +
                    "   ,z.COUNTY" +
                    "   ,z.RIG_NAME" +
                    "   ,w.well_num" +
                    "   ,w.well_name " +
                    "from WellList w " +
                    "inner join pten_cmr.cmr_data_usalt7 z on w.well_num=z.well_num " +
                    "inner join pten_cmr.regionmap r on trim(upper(r.contractor_business_unit))=trim(upper(z.contractor_business_unit)) " +
                    "inner join user_Security.user_mapping_Details u on  z.well_num=u.well_num where u.userid=" + getUser_id() + ";";

            log.info(query);
            ResultSet rs = stmt.executeQuery(query);
            ResultSetMetaData rsmd = rs.getMetaData();
            fetchingtime = System.currentTimeMillis() - k;
            long start = System.currentTimeMillis();
            JSONObject obj;

            String operator, region, state, county, rig_name, wellNum, wellName;
            while (rs.next()) {
                operator = rs.getString("operator_name");
                region = rs.getString("region_name");
                state = rs.getString("STATE_PROVINCE");
                county = rs.getString("county");
                rig_name = rs.getString("rig_name");
                wellNum = rs.getString("well_num");
                wellName = rs.getString("well_name");
                HashMap<String, HashMap<String, HashMap<String, HashMap<String, HashMap<String, String>>>>> regionMap =
                        mainMap.containsKey(operator) ? mainMap.get(operator) : new HashMap<>();
                HashMap<String, HashMap<String, HashMap<String, HashMap<String, String>>>> stateMap =
                        regionMap.containsKey(region) ? regionMap.get(region) : new HashMap<>();
                HashMap<String, HashMap<String, HashMap<String, String>>> countyMap =
                        stateMap.containsKey(state) ? stateMap.get(state) : new HashMap<>();
                HashMap<String, HashMap<String, String>> rigMap =
                        countyMap.containsKey(county) ? countyMap.get(county) : new HashMap<>();
                HashMap<String, String> wellMap = new HashMap<>();
                wellMap.put(wellNum, wellName);
                if (!rigMap.containsKey(rig_name))
                    rigMap.put(rig_name, wellMap);
                else {
                    rigMap.get(rig_name).putAll(wellMap);
                }
                if (!countyMap.containsKey(county)) {
                    countyMap.put(county, rigMap);
                } else {
                    countyMap.get(county).putAll(rigMap);
                }
                if (!stateMap.containsKey(state))
                    stateMap.put(state, countyMap);
                else {
                    stateMap.get(state).putAll(countyMap);
                }
                if (!regionMap.containsKey(region))
                    regionMap.put(region, stateMap);
                else {
                    regionMap.get(region).putAll(stateMap);
                }
                if (!mainMap.containsKey(operator))
                    mainMap.put(operator, regionMap);
                else {
                    mainMap.get(operator).putAll(regionMap);
                }

            }

            total = System.currentTimeMillis() - start;
            rs.close();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            json.put(ex.getMessage());
        }
        log.info("cmrAllFilterData - Response Sent " + fetchingtime + " " + total);

        return mainMap;
    }


    public Collection getDailyDepth(Collection<Long> wells) {
        StringBuilder query = new StringBuilder();

        query.append("WITH RankedWells AS (\n" +
                "SELECT a.well_num\n" +
                "                , a.current_depth - CASE a.preset WHEN 'Surface' THEN a.surface_depth WHEN 'Vertical' THEN a.vertical_depth ELSE 0 END AS footage_made\n" +
                "                , a.daywork_days AS days_on_well\n" +
                "                , ROW_NUMBER() OVER(ORDER BY (a.current_depth - CASE a.preset WHEN 'Surface' THEN a.surface_depth WHEN 'Vertical' THEN a.vertical_depth ELSE 0 END)/a.daywork_days DESC) AS performance_index\n" +
                "  FROM pten_cmr.cmr_data_usalt7 a\n" +
                "WHERE NOT EXISTS (\n" +
                "               SELECT NULL\n" +
                "                 FROM pten_cmr.cmr_data_usalt7 b inner join user_Security.user_mapping_Details u on b.well_num=u.well_num and u.userid=" + getUser_id() + " \n" +
                "                WHERE b.report_date > a.report_date\n" +
                "                  AND a.well_num = b.well_num)\n" +
                "  AND a.well_num IN (:wells)\n" +
                "), UnionWrapper AS (\n" +
                "SELECT z.well_num\n" +
                "                , z.daywork_days AS days_on_well\n" +
                "                , z.current_depth\n" +
                "                , rw.performance_index \n" +
                "                , z.report_date \n" +
                "  FROM pten_cmr.cmr_data_usalt7 z inner join user_Security.user_mapping_Details u on z.well_num=u.well_num and u.userid=" + getUser_id() + " \n" +
                "               INNER JOIN RankedWells rw ON z.well_num = rw.well_num\n" +
                "WHERE rw.performance_index <= 10\n" +
                "  AND z.current_depth IS NOT NULL\n" +
                "  AND z.current_depth > 0\n" +
                "ORDER BY z.daywork_days asc\n" +
                " UNION \n" +
                "SELECT q.well_num \n" +
                "        , 0.0 AS days_on_well \n" +
                "        , CASE UPPER(q.preset) WHEN 'SURFACE' THEN q.surface_depth WHEN 'VERTICAL' THEN q.vertical_depth ELSE 0.0 END AS current_depth \n" +
                "        , rw.performance_index \n" +
                "        , q.report_date \n" +
                "  FROM pten_cmr.cmr_data_usalt7 q \n" +
                "       INNER JOIN RankedWells rw ON q.well_num = rw.well_num \n" +
                " WHERE q.well_num IN (:wells) \n" +
                "   AND rw.performance_index <= 10\n" +
                "   AND q.current_depth > 0 \n" +
                "   AND q.current_depth IS NOT NULL \n" +
                "   AND NOT EXISTS ( \n" +
                "       SELECT NULL \n" +
                "         FROM pten_cmr.cmr_data_usalt7 x \n" +
                "        WHERE x.well_num = q.well_num \n" +
                "          AND x.report_date > q.report_date) \n" +
                ")\n" +
                "SELECT well_num, days_on_well, current_depth, performance_index, report_date FROM UnionWrapper order by 2 asc, current_depth asc\n");

        query.append(";");

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells);

        return impalaDbUtil.executeNamedQuery(query.toString(), namedParameters);
    }

    public Collection getROPActivityAllRigsWells(Collection<Long> wells) {

        String query = "WITH LatestCMR AS (\n" +
                "SELECT a.well_num\n" +
                "                , a.cmr_data_id\n" +
                "                , a.report_date\n" +
                "  FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_Details u on a.well_num=u.well_num and u.userid=" + getUser_id() + " \n" +
                "WHERE NOT EXISTS (\n" +
                "               SELECT NULL\n" +
                "                 FROM pten_cmr.cmr_data_usalt7 z inner join user_Security.user_mapping_Details u on a.well_num=u.well_num and z.userid=" + getUser_id() + " \n" +
                "                WHERE a.well_num = z.well_num\n" +
                "                  AND a.report_date < z.report_date)\n" +
                "), OperationHours AS (\n" +
                "SELECT j.well_num\n" +
                "                , SUM(CASE WHEN k.code = '2' THEN k.hours ELSE 0 END) AS drilling_hours\n" +
                "                , SUM(CASE WHEN k.code = '6' THEN k.hours ELSE 0 END) AS tripping_hours\n" +
                "                , SUM(CASE WHEN k.code = '12' THEN k.hours ELSE 0 END) AS casing_hours\n" +
                "                , SUM(CASE WHEN k.code IN ('14','15') THEN k.hours ELSE 0 END) AS bop_hours\n" +
                "                , SUM(CASE WHEN k.code = '8' THEN k.hours ELSE 0 END) AS npt_hours\n" +
                "                , SUM(CASE WHEN k.code = '1' THEN k.hours ELSE 0 END) AS move_hours\n" +
                "                , SUM(CASE WHEN k.code NOT IN ('1','2','6','8','12','13') THEN k.hours ELSE 0 END) AS other_hours\n" +
                "  FROM pten_cmr.cmr_data_usalt7 j inner join user_Security.user_mapping_Details u on a.well_num=u.well_num and j.userid=" + getUser_id() + " \n" +
                "                INNER JOIN pten_cmr.cmr_time_code_data_usalt7 k ON j.cmr_data_id = k.cmr_data_id\n" +
                "GROUP BY j.well_num\n" +
                "), StandAggregates AS (\n" +
                "SELECT wss.well_num\n" +
                "                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'SURFACE' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.rotating_footage ELSE 0 END) AS surface_rotary_footage\n" +
                "                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'SURFACE' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sec_rotating ELSE 0 END) AS surface_rotary_seconds\n" +
                "                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'SURFACE' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sliding_footage ELSE 0 END) AS surface_slide_footage\n" +
                "                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'SURFACE' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sec_sliding ELSE 0 END) AS surface_slide_seconds\n" +
                "                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'VERTICAL' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.rotating_footage ELSE 0 END) AS vertical_rotary_footage\n" +
                "                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'VERTICAL' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sec_rotating ELSE 0 END) AS vertical_rotary_seconds\n" +
                "                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'VERTICAL' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sliding_footage ELSE 0 END) AS vertical_slide_footage\n" +
                "                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'VERTICAL' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sec_sliding ELSE 0 END) AS vertical_slide_seconds\n" +
                "                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'CURVE' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.rotating_footage ELSE 0 END) AS curve_rotary_footage\n" +
                "                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'CURVE' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sec_rotating ELSE 0 END) AS curve_rotary_seconds\n" +
                "                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'CURVE' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sliding_footage ELSE 0 END) AS curve_slide_footage\n" +
                "                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'CURVE' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sec_sliding ELSE 0 END) AS curve_slide_seconds\n" +
                "                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'LATERAL' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.rotating_footage ELSE 0 END) AS lateral_rotary_footage\n" +
                "                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'LATERAL' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sec_rotating ELSE 0 END) AS lateral_rotary_seconds\n" +
                "                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'LATERAL' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sliding_footage ELSE 0 END) AS lateral_slide_footage\n" +
                "                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'LATERAL' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sec_sliding ELSE 0 END) AS lateral_slide_seconds\n" +
                "  FROM ptendrilling_new.ptendrilling_WellStandSeries wss\n" +
                "GROUP BY wss.well_num\n" +
                ")\n" +
                "SELECT a.well_num\n" +
                "                , a.well_name\n" +
                "                , a.rig_name\n" +
                "                , rt.display_text AS rig_type\n" +
                "                ,rt.rig_type_id\n" +
                "                , rm.region_name           \n" +
                "                , to_timestamp(COALESCE(a.release_time, a.estimated_release)) AS effective_release_time\n" +
                "                , CASE WHEN a.release_time IS NULL THEN 1 ELSE 0 END AS active_well_flag\n" +
                "                , a.days_on_well\n" +
                "                , COALESCE(oh.drilling_hours, 0) AS drilling_hours\n" +
                "                , COALESCE(oh.tripping_hours, 0) AS tripping_hours\n" +
                "                , COALESCE(oh.casing_hours, 0) AS casing_hours\n" +
                "                , COALESCE(oh.bop_hours, 0) AS bop_hours\n" +
                "                , COALESCE(oh.npt_hours, 0) AS npt_hours\n" +
                "                , COALESCE(oh.move_hours, 0) AS move_hours\n" +
                "                , COALESCE(oh.other_hours, 0) AS other_hours\n" +
                "                , a.total_depth / nullifzero(a.days_on_well) AS avg_feet_per_day\n" +
                "                , sa.surface_rotary_footage\n" +
                "                , sa.surface_rotary_seconds\n" +
                "                , sa.surface_slide_footage\n" +
                "                , sa.surface_slide_seconds\n" +
                "                , sa.vertical_rotary_footage\n" +
                "                , sa.vertical_rotary_seconds\n" +
                "                , sa.vertical_slide_footage\n" +
                "                , sa.vertical_slide_seconds\n" +
                "                , sa.curve_rotary_footage\n" +
                "                , sa.curve_rotary_seconds\n" +
                "                , sa.curve_slide_footage\n" +
                "                , sa.curve_slide_seconds\n" +
                "                , sa.lateral_rotary_footage\n" +
                "                , sa.lateral_rotary_seconds\n" +
                "                , sa.lateral_slide_footage\n" +
                "                , sa.lateral_slide_seconds\n" +
                "  FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_Details u on a.well_num=u.well_num and u.userid=" + getUser_id() + " \n" +
                "                INNER JOIN LatestCMR b ON a.cmr_data_id = b.cmr_data_id\n" +
                "                INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "                INNER JOIN pten_cmr.rig r ON r.display_text = a.rig_name\n" +
                "                INNER JOIN pten_cmr.rigtype rt ON r.rig_type_id = rt.rig_type_id\n" +
                "                LEFT OUTER JOIN StandAggregates sa ON sa.well_num = a.well_num\n" +
                "                INNER JOIN OperationHours oh ON a.well_num = oh.well_num\n" +
                " WHERE a.well_num IN (:wells)";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells);

        return impalaDbUtil.executeNamedQuery(query, namedParameters);
        }
}
