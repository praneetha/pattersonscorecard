package com.flutura.ImpalaRestApiController;

import com.pten.ui.service.CMRService;
import com.pten.ui.service.NPTService;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;


@RequestMapping("/assetReport")
@RestController
public class ApiController {
    private static final Logger log = LoggerFactory.getLogger(ApiController.class);

    @Autowired
    CerebraAPIDataLayer cerebraAPIDataLayer;

    @Autowired
    NPTService nptService;

    @Autowired
    CMRService cmrService;

    @Autowired
    private RequestHandler requestHandler;

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/inSlipsCasingConnection", produces = "application/json")
    @ResponseBody
    public String inSlipsCasingMinutes(@RequestParam String well_no,
                                       @RequestParam("starttime") String startTime,
                                       @RequestParam("endtime") String endTime) {
        return requestHandler.inSlipsCasingMinutes(well_no, startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/inSlipsCasingConnection", produces = "application/json")
    @ResponseBody
    public String inSlipsCasingMinutesPost(HttpServletRequest request, @RequestBody String body) {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String well_no = jsonOb.getString("well_no");
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            response = requestHandler.inSlipsCasingMinutes(well_no, startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/inSlipsCasingFTHr", produces = "application/json")
    @ResponseBody
    public String inSlipsCasingFTHr(@RequestParam String well_no,
                                    @RequestParam("starttime") String startTime,
                                    @RequestParam("endtime") String endTime) {
        return requestHandler.inSlipsCasingFTHr(well_no, startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/inSlipsCasingFTHr", produces = "application/json")
    @ResponseBody
    public String inSlipsCasingFTHrPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";

        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String well_no = jsonOb.getString("well_no");
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            response = requestHandler.inSlipsCasingFTHr(well_no, startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/depthTrend", produces = "application/json")
    @ResponseBody
    public String DepthVsTime(@RequestParam("well_no") String well_no,
                              @RequestParam("starttime") String startTime,
                              @RequestParam("endtime") String endTime,
                              @RequestParam("rig_name") String rig_name) {
        return requestHandler.DepthVsTime(well_no, startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/depthTrend", produces = "application/json")
    @ResponseBody
    public String DepthVsTimePost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String well_no = jsonOb.getString("well_no");
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            response = requestHandler.DepthVsTime(well_no, startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }

    @CrossOrigin
    @GetMapping(value = "/wells/cmr/daily/depth", produces = "application/json")
    @ResponseBody
    public Object getDepth(@RequestParam(value = "welllist") Collection<Long> wells) {
        //TODO: add error handling. empty wells list. (ie. Object getWells)
        //return cerebraAPIDataLayer.getWellsCmrDailyDepth(wells).toString();
        return cmrService.getWellsDailyDepth(wells);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/drillingTrend", produces = "application/json")
    @ResponseBody
    public String DrillingTrend(@RequestParam("well_no") String well_no,
                                @RequestParam("starttime") String startTime,
                                @RequestParam("endtime") String endTime) {
        return requestHandler.DrillingTrend(well_no, startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/drillingTrend", produces = "application/json")
    @ResponseBody
    public String DrillingTrendPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String well_no = jsonOb.getString("well_no");
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            response = requestHandler.DrillingTrend(well_no, startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/drillingWobRop", produces = "application/json")
    @ResponseBody
    public String DrillingTrendROP(@RequestParam("well_no") String well_no,
                                   @RequestParam("starttime") String startTime,
                                   @RequestParam("endtime") String endTime) {
        return requestHandler.DrillingTrendROP(well_no, startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/drillingWobRop", produces = "application/json")
    @ResponseBody
    public String DrillingTrendROPPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String well_no = jsonOb.getString("well_no");
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            response = requestHandler.DrillingTrendROP(well_no, startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/drillingRpmTorque", produces = "application/json")
    @ResponseBody
    public String DrillingTrendRPM(@RequestParam("well_no") String well_no,
                                   @RequestParam("starttime") String startTime,
                                   @RequestParam("endtime") String endTime) {
        return requestHandler.DrillingTrendRPM(well_no, startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/drillingRpmTorque", produces = "application/json")
    @ResponseBody
    public String DrillingTrendRPMPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String well_no = jsonOb.getString("well_no");
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            response = requestHandler.DrillingTrendRPM(well_no, startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/drillingSppPtsr", produces = "application/json")
    @ResponseBody
    public String DrillingTrendSPP(@RequestParam("well_no") String well_no,
                                   @RequestParam("starttime") String startTime,
                                   @RequestParam("endtime") String endTime) {
        return requestHandler.DrillingTrendSPP(well_no, startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/drillingSppPtsr", produces = "application/json")
    @ResponseBody
    public String DrillingTrendSPPPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String well_no = jsonOb.getString("well_no");
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            response = requestHandler.DrillingTrendSPP(well_no, startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/drillingHklWob", produces = "application/json")
    @ResponseBody
    public String DrillingTrendHKL(@RequestParam("well_no") String well_no,
                                   @RequestParam("starttime") String startTime,
                                   @RequestParam("endtime") String endTime) {
        return requestHandler.DrillingTrendHKL(well_no, startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/drillingHklWob", produces = "application/json")
    @ResponseBody
    public String DrillingTrendHKLPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String well_no = jsonOb.getString("well_no");
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            response = requestHandler.DrillingTrendHKL(well_no, startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rigStateBreakDown", produces = "application/json")
    @ResponseBody
    public String totalRigStateBreakDown(@RequestParam("well_no") String well_no,
                                         @RequestParam("starttime") String startTime,
                                         @RequestParam("endtime") String endTime) {
        return requestHandler.totalRigStateBreakDown(well_no, startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/rigStateBreakDown", produces = "application/json")
    @ResponseBody
    public String totalRigStateBreakDownPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String well_no = jsonOb.getString("well_no");
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            response = requestHandler.totalRigStateBreakDown(well_no, startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/trippingConnectionTime", produces = "application/json")
    @ResponseBody
    public String trippingConnectionTime(@RequestParam("well_no") String well_no,
                                         @RequestParam("starttime") String startTime,
                                         @RequestParam("endtime") String endTime) {
        return requestHandler.trippingConnectionTime(well_no, startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/trippingConnectionTime", produces = "application/json")
    @ResponseBody
    public String trippingConnectionTimePost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String well_no = jsonOb.getString("well_no");
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            response = requestHandler.trippingConnectionTime(well_no, startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/trippingSpeedFTHr", produces = "application/json")
    @ResponseBody
    public String trippingSpeedFTHr(@RequestParam("well_no") String well_no,
                                    @RequestParam("starttime") String startTime,
                                    @RequestParam("endtime") String endTime) {
        return requestHandler.trippingSpeedFTHr(well_no, startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/trippingSpeedFTHr", produces = "application/json")
    @ResponseBody
    public String trippingSpeedFTHrPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String well_no = jsonOb.getString("well_no");
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            response = requestHandler.trippingSpeedFTHr(well_no, startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/drillingRopByStand", produces = "application/json")
    @ResponseBody
    public String drillingROPByStand(@RequestParam("well_no") String well_no,
                                     @RequestParam("starttime") String startTime,
                                     @RequestParam("endtime") String endTime) {
        return requestHandler.drillingROPByStand(well_no, startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/drillingRopByStand", produces = "application/json")
    @ResponseBody
    public String drillingROPByStandPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String well_no = jsonOb.getString("well_no");
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            response = requestHandler.drillingROPByStand(well_no, startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/w2wDrillingConnection", produces = "application/json")
    @ResponseBody
    public String w2WDrillingConnection(@RequestParam("well_no") String well_no,
                                        @RequestParam("starttime") String startTime,
                                        @RequestParam("endtime") String endTime) {
        return requestHandler.w2WDrillingConnection(well_no, startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/w2wDrillingConnection", produces = "application/json")
    @ResponseBody
    public String w2WDrillingConnectionPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String well_no = jsonOb.getString("well_no");
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            response = requestHandler.w2WDrillingConnection(well_no, startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rigStateBreakDownTrend", produces = "application/json")
    @ResponseBody
    public String rigStateBreakdownTrend(@RequestParam("well_no") String well_no,
                                         @RequestParam("starttime") String startTime,
                                         @RequestParam("endtime") String endTime) {
        return requestHandler.rigStateBreakdownTrend(well_no, startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/rigStateBreakDownTrend", produces = "application/json")
    @ResponseBody
    public String rigStateBreakdownTrendPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String well_no = jsonOb.getString("well_no");
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            response = requestHandler.rigStateBreakdownTrend(well_no, startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }


    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/wellDetails", produces = "application/json")
    @ResponseBody
    public String WellDetails(HttpServletRequest request) throws SQLException, ParseException, JSONException {
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + ", param : " + request.getQueryString());
        String well_no = request.getParameter("well_no");
        String startTime = request.getParameter("starttime");
        String endTime = request.getParameter("endtime");
        return requestHandler.WellDetails(well_no);
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/wellDetails", produces = "application/json")
    @ResponseBody
    public String WellDetailsPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String well_no = jsonOb.getString("well_no");
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            return requestHandler.WellDetails(well_no);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/getWellList", produces = "application/json")
    @ResponseBody
    public String wellParamList(@RequestParam("rig_name") String rig_name,
                                @RequestParam("starttime") String startTime,
                                @RequestParam("endtime") String endTime) {
        return requestHandler.wellParamList(rig_name, startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/getWellList", produces = "application/json")
    @ResponseBody
    public String wellParamListPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String rig_name = jsonOb.getString("rig_name");
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            return requestHandler.wellParamList(rig_name, startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/getRigList", produces = "application/json")
    @ResponseBody
    public String rigParamList() {
        return requestHandler.rigParamList();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rigCMRPercentByMonth", produces = "application/json")
    @ResponseBody
    public Collection RigCMRPercentByMonth_GET(@RequestParam("rigname") String rigname,
                                               @RequestParam("starttime") String startTime,
                                               @RequestParam("endtime") String endTime,
                                               @RequestParam(value = "subcodetext", required = false) Optional<String> subCodeText) {
        return cmrService.getRigCMRPercentByMonth(rigname, startTime, endTime, subCodeText);

    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rigCMRPercentByMonth/byWells", produces = "application/json")
    @ResponseBody
    public Collection RigCMRPercentByMonth_GET(@RequestParam("rigname") String rigname,
                                               @RequestParam("welllist") List<Long> wells,
                                               @RequestParam("starttime") String startTime,
                                               @RequestParam("endtime") String endTime) {
        return cmrService.getRigCMRPercentByMonth(rigname, wells, startTime, endTime);

    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/rigCMRPercentByMonth", produces = "application/json")
    @ResponseBody
    public String RigCMRPercentByMonth_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String rigname = jsonOb.getString("rigname");
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            response = requestHandler.RigCMRPercentByMonth(rigname, startTime, endTime, null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rigCMRHoursByMonth", produces = "application/json")
    @ResponseBody
    public Collection RigCMRHoursByMonth_GET(@RequestParam("rigname") String rigname,
                                         @RequestParam("starttime") String startTime,
                                         @RequestParam("endtime") String endTime,
                                         @RequestParam(value = "subcodetext", required = false) String subCodeText) {
        return cmrService.getRigCMRHoursByMonth(rigname, startTime, endTime, subCodeText);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/rigCMRHoursByMonth", produces = "application/json")
    @ResponseBody
    public String RigCMRHoursByMonth_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String rigname = jsonOb.getString("rigname");
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            response = requestHandler.RigCMRHoursByMonth(rigname, startTime, endTime, null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rigCMRHoursBySubCode", produces = "application/json")
    @ResponseBody
    public String RigCMRHoursBySubCode_GET(@RequestParam("rigname") String rigname,
                                           @RequestParam("starttime") String startTime,
                                           @RequestParam("endtime") String endTime,
                                           @RequestParam(value = "subcodetext", required = false) Optional<String> subCodeText) {
        return requestHandler.RigCMRHoursBySubCode(rigname, startTime, endTime, subCodeText);

    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/rigCMRHoursBySubCode", produces = "application/json")
    @ResponseBody
    public String RigCMRHoursBySubCode_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String rigname = jsonOb.getString("rigname");
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            response = requestHandler.RigCMRHoursBySubCode(rigname, startTime, endTime, null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rigCMRDetailsBySubSubCode", produces = "application/json")
    @ResponseBody
    public Collection RigCMRDetailsBySubSubCode_GET(@RequestParam("rigname") String rigname,
                                                    @RequestParam("starttime") String startTime,
                                                    @RequestParam("endtime") String endTime,
                                                    @RequestParam(value = "subcode", required = false) String subCode) {

        return cmrService.RigCMRDetailsBySubSubCode(rigname, startTime, endTime, subCode);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/rigCMRDetailsBySubSubCode", produces = "application/json")
    @ResponseBody
    public String RigCMRDetailsBySubSubCode_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String rigname = jsonOb.getString("rigname");
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            response = requestHandler.RigCMRDetailsBySubSubCode(rigname, startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }


    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/DumbTest", produces = "application/json")
    @ResponseBody
    public String DumbTest_Get() {
        return "{\"word\":\"Bird\"}";
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/DateTest", produces = "application/json")
    @ResponseBody
    public Object DateTest_Get(@RequestParam("starttime") String startTime,
                               @RequestParam("endtime") String endTime) {
        return requestHandler.DateTest(startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/CMRRigList", produces = "application/json")
    @ResponseBody
    public String CMRRigList_GET(@RequestParam("starttime") String startTime,
                                 @RequestParam("endtime") String endTime) {

        return requestHandler.CMRRigList(startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/CMRRigList", produces = "application/json")
    @ResponseBody
    public String CMRRigList_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String starttime = jsonOb.getString("starttime");
            String endtime = jsonOb.getString("endtime");
            response = requestHandler.CMRRigList(starttime, endtime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/CMRDailyDetails", produces = "application/json")
    @ResponseBody
    public String CMRDailyDetails_GET(@RequestParam("well_no") String wellnum,
                                      @RequestParam("starttime") String starttime,
                                      @RequestParam("endtime") String endtime) {
        return requestHandler.CMRDailyDetails(wellnum, starttime, endtime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/CMRDailyDetails", produces = "application/json")
    @ResponseBody
    public String CMRDailyDetails_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String wellnumber = jsonOb.getString("wellnumber");
            String starttime = jsonOb.getString("starttime");
            String endtime = jsonOb.getString("endtime");
            response = requestHandler.CMRDailyDetails(wellnumber, starttime, endtime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }

        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/wellDrillingParameters", produces = "application/json")
    @ResponseBody
    public String wellDrillingParameters_GET(@RequestParam("wellnum") String wellnum,
                                             @RequestParam("starttime") String startTime,
                                             @RequestParam("endtime") String endTime,
                                             @RequestParam("targetrows") String targetrows) {
        return requestHandler.wellDrillingParameters(wellnum, startTime, endTime, targetrows);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/wellDrillingParameters", produces = "application/json")
    @ResponseBody
    public String wellDrillingParameters_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String wellnum = jsonOb.getString("wellnum");
            String starttime = jsonOb.getString("starttime");
            String endtime = jsonOb.getString("endtime");
            String targetrows = jsonOb.getString("targetrows");
            response = requestHandler.wellDrillingParameters(wellnum, starttime, endtime, targetrows);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }

        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rigDrillingParameters", produces = "application/json")
    @ResponseBody
    public String rigDrillingParameters_GET(@RequestParam("rigname") String rigname,
                                            @RequestParam("starttime") String startTime,
                                            @RequestParam("endtime") String endTime,
                                            @RequestParam("targetrows") String targetrows) {
        return requestHandler.rigDrillingParameters(rigname, startTime, endTime, targetrows);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/rigDrillingParameters", produces = "application/json")
    @ResponseBody
    public String rigDrillingParameters_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String rigname = jsonOb.getString("rigname");
            String starttime = jsonOb.getString("starttime");
            String endtime = jsonOb.getString("endtime");
            String targetrows = jsonOb.getString("targetrows");
            response = requestHandler.rigDrillingParameters(rigname, starttime, endtime, targetrows);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }

        return response;
    }


    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/StringTest", produces = "application/json")
    @ResponseBody
    public String StringTest(HttpServletRequest request) {
        return requestHandler.StringTest(request.getParameter("riglist"));
    }

    @CrossOrigin
    @GetMapping(value = "/dateTest", produces = "application/json")
    @ResponseBody
    public ResponseEntity dateTest(@RequestParam @DateTimeFormat(pattern = "yyyy/MM") Date from,
                                   @RequestParam @DateTimeFormat(pattern = "yyyy/MM") Date to) {
        return ResponseEntity.ok(requestHandler.dateTest(from, to));
    }


    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/RegionList", produces = "application/json")
    @ResponseBody
    public String RegionList_GET(HttpServletRequest request) {

        return requestHandler.RegionList();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/groupTypeList", produces = "application/json")
    @ResponseBody
    public Object groupTypeList(HttpServletRequest request) {

        return nptService.getGroupTypes();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/RegionList", produces = "application/json")
    @ResponseBody
    public String RegionList_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            response = requestHandler.RegionList();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }

        return response;
    }


    /****************************************************************************************/
    /**
     * examples of straight forward Data to REST.  (where no middle layer operations needed)
     *
     * @param wells
     * @return
     */

    @CrossOrigin
    @GetMapping(value = "/wells/npt/percent", produces = "application/json")
    @ResponseBody
    public Object getWellNptPercent(@RequestParam("welllist") Collection<Long> wells) {
        //return cerebraAPIDataLayer.getNptPercent(wells).toString();
        return nptService.getWellNptPercent(wells);
    }

    @CrossOrigin
    @GetMapping(value = "/wells/npt/hours", produces = "application/json")
    @ResponseBody
    public Object getWellNptHours(@RequestParam("welllist") Collection<Long> wells) {
        return nptService.getWellNptHours(wells); //if middle layer ops needed.
        //return cerebraAPIDataLayer.getNptHours(wells).toString();
    }

    @CrossOrigin
    @GetMapping(value = "/wells/npt", produces = "application/json")
    @ResponseBody
    public Object getNptBreakdown(@RequestParam("welllist") Collection<Long> wells) {
        //return nptService.getBreakdown(wells); //if middle layer ops needed. when subcode interaction is required (maybe)
        return cerebraAPIDataLayer.getNptHoursBySubCode(wells).toString();
    }

    @CrossOrigin
    @GetMapping(value = "/wells", produces = "application/json")
    @ResponseBody
    public Object getWells(@RequestParam("welllist") Collection<Long> wells) {
        //return cerebraAPIDataLayer.getNptPercent(wells).toString();
        return cmrService.getWells(wells);
    }

    @CrossOrigin
    @GetMapping(value = "/wells/npt/hours/avg", produces = "application/json")
    @ResponseBody
    public Object getNptAverage(@RequestParam("welllist") Collection<Long> wells) {
        //return nptService.getNptHoursAverage(wells); //if middle layer ops needed. when subcode interaction is required (maybe)
        return cerebraAPIDataLayer.getNptHoursAverage(wells).toString();
    }

    /****************************************************************************************/

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cmrOperator", produces = "application/json")
    @ResponseBody
    public String cmrOperator(HttpServletRequest request) throws SQLException, ParseException, JSONException {
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + ", param : " + request.getQueryString());
        String startTime = request.getParameter("starttime");
        String endTime = request.getParameter("endtime");
        return requestHandler.cmrOperator(startTime, endTime);
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/cmrOperator", produces = "application/json")
    @ResponseBody
    public String cmrOperatorPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            return requestHandler.cmrOperator(startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cmrRegionList", produces = "application/json")
    @ResponseBody
    public String cmrRegionList(HttpServletRequest request) throws SQLException, ParseException, JSONException {
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + ", param : " + request.getQueryString());
        String operator = request.getParameter("operator");
        String startTime = request.getParameter("starttime");
        String endTime = request.getParameter("endtime");
        return requestHandler.cmrRegionList(operator, startTime, endTime);
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/cmrRegionList", produces = "application/json")
    @ResponseBody
    public String cmrRegionListPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String operator = jsonOb.getString("operator");
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            return requestHandler.cmrRegionList(operator, startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }///// End of method


     @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cmrStateList", produces = "application/json")
    @ResponseBody
    public String cmrStateList(HttpServletRequest request) throws SQLException, ParseException, JSONException {
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + ", param : " + request.getQueryString());
        String startTime = request.getParameter("starttime");
        String endTime = request.getParameter("endtime");
        return requestHandler.cmrStateList(startTime, endTime);
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/cmrStateList", produces = "application/json")
    @ResponseBody
    public String cmrStateListPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            return requestHandler.cmrStateList(startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cmrCountyList", produces = "application/json")
    @ResponseBody
    public String cmrCountyList(HttpServletRequest request) throws SQLException, ParseException, JSONException {
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + ", param : " + request.getQueryString());
        String stateProvince = request.getParameter("stateProvince");
        String startTime = request.getParameter("starttime");
        String endTime = request.getParameter("endtime");
        return requestHandler.cmrCountyList(stateProvince, startTime, endTime);
    }///// End of method


    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/cmrCountyList", produces = "application/json")
    @ResponseBody
    public String cmrCountyListPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String stateProvince = jsonOb.getString("stateProvince");
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            return requestHandler.cmrCountyList(stateProvince, startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }///// End of method


    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cmrRigList", produces = "application/json")
    @ResponseBody
    public String cmrRigList(HttpServletRequest request) throws SQLException, ParseException, JSONException {
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + ", param : " + request.getQueryString());
        String stateProvince = request.getParameter("stateProvince");
        String region = request.getParameter("region");
        String county = request.getParameter("county");
        String startTime = request.getParameter("starttime");
        String endTime = request.getParameter("endtime");
        String operator = request.getParameter("operator");
        return requestHandler.cmrRigList(region, stateProvince, county, operator, startTime, endTime);
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/cmrRigList", produces = "application/json")
    @ResponseBody
    public String cmrRigListPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String stateProvince = jsonOb.getString("stateProvince");
            String region = jsonOb.getString("region");
            String county = jsonOb.getString("county");
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            String operator = jsonOb.getString("operator");
            return requestHandler.cmrRigList(region, stateProvince, county, operator, startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cmrWellList", produces = "application/json")
    @ResponseBody
    public String cmrWellList(HttpServletRequest request) throws SQLException, ParseException, JSONException {
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + ", param : " + request.getQueryString());
        String state = request.getParameter("stateProvince");
        String region = request.getParameter("region");
        String county = request.getParameter("county");
        String rig_name = request.getParameter("rig_name");
        String operator = request.getParameter("operator");
        String startTime = request.getParameter("starttime");
        String endTime = request.getParameter("endtime");
        return requestHandler.cmrWellList(region, state, county, rig_name, operator, startTime, endTime);
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/cmrWellList", produces = "application/json")
    @ResponseBody
    public String cmrWellListPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String state = jsonOb.getString("stateProvince");
            String region = jsonOb.getString("region");
            String county = jsonOb.getString("county");
            String rig_name = jsonOb.getString("rig_name");
            String operator = jsonOb.getString("operator");
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            return requestHandler.cmrWellList(region, state, county, rig_name, operator, startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }///// End of method


    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/drillingRigMovement", produces = "application/json")
    @ResponseBody
    public String drillingRigMovement(HttpServletRequest request) throws SQLException, ParseException, JSONException {
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + ", param : " + request.getQueryString());
        String startTime = request.getParameter("starttime");
        String endTime = request.getParameter("endtime");
        String well_no = request.getParameter("well_no");
        return requestHandler.drillingRigMovement(startTime, endTime, well_no);
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/drillingRigMovement", produces = "application/json")
    @ResponseBody
    public String drillingRigMovementPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            String well_no = jsonOb.getString("well_no");
            return requestHandler.drillingRigMovement(startTime, endTime, well_no);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }///// End of method


    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/drillingRigDown", produces = "application/json")
    @ResponseBody
    public String drillingRigDown(HttpServletRequest request) throws SQLException, ParseException, JSONException {
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + ", param : " + request.getQueryString());
        String startTime = request.getParameter("starttime");
        String endTime = request.getParameter("endtime");
        String well_no = request.getParameter("well_no");
        return requestHandler.drillingRigDown(startTime, endTime, well_no);
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/drillingRigDown", produces = "application/json")
    @ResponseBody
    public String drillingRigDownPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            String well_no = jsonOb.getString("well_no");
            return requestHandler.drillingRigDown(startTime, endTime, well_no);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }///// End of method


    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rigDrilling", produces = "application/json")
    @ResponseBody
    public String rigDrilling(HttpServletRequest request) throws SQLException, ParseException, JSONException {
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + ", param : " + request.getQueryString());
        String well_no = request.getParameter("well_no");
        return requestHandler.rigDrilling(well_no);
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/rigDrilling", produces = "application/json")
    @ResponseBody
    public String rigDrillingPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String well_no = jsonOb.getString("well_no");
            return requestHandler.rigDrilling(well_no);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rigTripping", produces = "application/json")
    @ResponseBody
    public String rigTripping(HttpServletRequest request) throws SQLException, ParseException, JSONException {
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + ", param : " + request.getQueryString());
        String well_no = request.getParameter("well_no");
        return requestHandler.rigTripping(well_no);
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/rigTripping", produces = "application/json")
    @ResponseBody
    public String rigTrippingPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String well_no = jsonOb.getString("well_no");
            return requestHandler.rigTripping(well_no);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }///// End of method


    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rigsOutOfHole", produces = "application/json")
    @ResponseBody
    public String rigsOutOfHole(HttpServletRequest request) throws SQLException, ParseException, JSONException {
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + ", param : " + request.getQueryString());
        String well_no = request.getParameter("well_no");
        return requestHandler.rigsOutOfHole(well_no);
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/rigsOutOfHole", produces = "application/json")
    @ResponseBody
    public String rigsOutOfHolePost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String well_no = jsonOb.getString("well_no");
            return requestHandler.rigsOutOfHole(well_no);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }///// End of method


    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/wellRecentlyReleased", produces = "application/json")
    @ResponseBody
    public String wellRecentlyReleased(HttpServletRequest request) throws SQLException, ParseException, JSONException {
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + ", param : " + request.getQueryString());
        String startTime = request.getParameter("starttime");
        String endTime = request.getParameter("endtime");
        String well_no = request.getParameter("well_no");
        return requestHandler.wellRecentlyReleased(startTime, endTime, well_no);
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/wellRecentlyReleased", produces = "application/json")
    @ResponseBody
    public String wellRecentlyReleasedPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            String well_no = jsonOb.getString("well_no");
            return requestHandler.wellRecentlyReleased(startTime, endTime, well_no);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }///// End of method


    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/wellUpcomingRelease", produces = "application/json")
    @ResponseBody
    public String wellUpcomingRelease(HttpServletRequest request) throws SQLException, ParseException, JSONException {
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + ", param : " + request.getQueryString());
        String startTime = request.getParameter("starttime");
        String endTime = request.getParameter("endtime");
        String well_no = request.getParameter("well_no");
        return requestHandler.wellUpcomingRelease(startTime, endTime, well_no);
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/wellUpcomingRelease", produces = "application/json")
    @ResponseBody
    public String wellUpcomingReleasePost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            String well_no = jsonOb.getString("well_no");
            return requestHandler.wellUpcomingRelease(startTime, endTime, well_no);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }///// End of method


    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rigMetaInfo", produces = "application/json")
    @ResponseBody
    public String rigMetaInfo(HttpServletRequest request) throws SQLException, ParseException, JSONException {
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + ", param : " + request.getQueryString());
        String well_no = request.getParameter("well_no");
        String startTime = request.getParameter("starttime");
        String endTime = request.getParameter("endtime");
        return requestHandler.rigMetaInfo(startTime, endTime, well_no);
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/rigMetaInfo", produces = "application/json")
    @ResponseBody
    public String rigMetaInfoPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String well_no = jsonOb.getString("well_no");
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            return requestHandler.rigMetaInfo(startTime, endTime, well_no);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rigCurrentStatusInfo", produces = "application/json")
    @ResponseBody
    public String rigCurrentStatusInfo(HttpServletRequest request) throws SQLException, ParseException, JSONException {
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + ", param : " + request.getQueryString());
        String well_no = request.getParameter("well_no");
        String startTime = request.getParameter("starttime");
        String endTime = request.getParameter("endtime");
        return requestHandler.rigCurrentStatusInfo(startTime, endTime, well_no);
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/rigCurrentStatusInfo", produces = "application/json")
    @ResponseBody
    public String rigCurrentStatusInfoPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String well_no = jsonOb.getString("well_no");
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            return requestHandler.rigCurrentStatusInfo(startTime, endTime, well_no);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/vSWellList", produces = "application/json")
    @ResponseBody
    public String vSWellList(HttpServletRequest request) throws SQLException, ParseException, JSONException {
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + ", param : " + request.getQueryString());
        String rig_name = request.getParameter("rig_name");
        String startTime = request.getParameter("starttime");
        String endTime = request.getParameter("endtime");

        return requestHandler.vSWellList(rig_name, startTime, endTime);
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/vSWellList", produces = "application/json")
    @ResponseBody
    public String vSWellList_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String rig_name = jsonOb.getString("rig_name");
            String startTime = request.getParameter("starttime");
            String endTime = request.getParameter("endtime");
            response = requestHandler.vSWellList(rig_name, startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/patDPRRigList", produces = "application/json")
    @ResponseBody
    public String patDPRRigList(HttpServletRequest request) throws SQLException, ParseException, JSONException {
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + ", param : " + request.getQueryString());
        return requestHandler.patDRPRigList();
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/patDPRRigList", produces = "application/json")
    @ResponseBody
    public String patDPRRigListPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            return requestHandler.patDRPRigList();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/patDPRWellList", produces = "application/json")
    @ResponseBody
    public String patWellList(HttpServletRequest request) throws SQLException, ParseException, JSONException {
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + ", param : " + request.getQueryString());
        String rig_name = request.getParameter("rig_name");
        String end_time = request.getParameter("endtime");
        return requestHandler.patDRPWellList(rig_name, end_time);
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/patDPRWellList", produces = "application/json")
    @ResponseBody
    public String patWellList_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String rig_name = jsonOb.getString("rig_name");
            String end_time = jsonOb.getString("endtime");
            response = requestHandler.patDRPWellList(rig_name, end_time);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/patCMRRigList", produces = "application/json")
    @ResponseBody
    public String patCMRRigList(HttpServletRequest request) throws SQLException, ParseException, JSONException {
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + ", param : " + request.getQueryString());
        return requestHandler.patCMRRigList();
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/patCMRRigList", produces = "application/json")
    @ResponseBody
    public String patCMRRigListPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            return requestHandler.patCMRRigList();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }///// End of method


    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cmrLastNWellsList", produces = "application/json")
    @ResponseBody
    public Collection cmrLastNWellsList(@RequestParam("welllist") String wellList,
                                        @RequestParam("alias") boolean alias) {
        return cmrService.RigCMRPerformanceMetricsWellList(wellList, alias);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/cmrLastNWellsList", produces = "application/json")
    @ResponseBody
    public String cmrLastNWellsListPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String wellList = jsonOb.getString("welllist");
            //TODO: GO BACK AND FIX THIS LATER ... NOT REALLY NEEDED (TOSTRING)
            return cmrService.RigCMRPerformanceMetricsWellList(wellList, false).toString();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/totalRigList", produces = "application/json")
    @ResponseBody
    public String totalRigList(HttpServletRequest request) throws SQLException, ParseException, JSONException {
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + ", param : " + request.getQueryString());
        String wellnum = request.getParameter("well_no");
        return requestHandler.totalRigList(wellnum);
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/totalRigList", produces = "application/json")
    @ResponseBody
    public String totalRigListPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String wellnum = jsonOb.getString("well_no");
            return requestHandler.totalRigList(wellnum);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }///// End of method

    //TODO: at controller level: separate cmr from npt. within cmr separate by rigs, wells, etc


    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cmrAllFilterData", produces = "application/json")
    @ResponseBody
    public HashMap cmrAllFilterData(HttpServletRequest request) throws SQLException, ParseException, JSONException {
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + ", param : " + request.getQueryString());
        String startTime = request.getParameter("starttime");
        String endTime = request.getParameter("endtime");
        return requestHandler.cmrAllFilterData(startTime, endTime);
    }///// End of method

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/cmrAllFilterData", produces = "application/json")
    @ResponseBody
    public HashMap cmrAllFilterDataPost(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        HashMap<String, String> response = new HashMap<>();
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            return requestHandler.cmrAllFilterData(startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            //response = e.getMessage();
        }
        return response;
    }///// End of method

}
