package com.flutura.ImpalaRestApiController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

@RestController
public class PdfController {
    Logger log = LoggerFactory.getLogger(PdfController.class);

    @Value("${export.rootPath}")
    String rootPath;

    @GetMapping(value = "/{pdf_type}/{rig_name}/{file_name}", produces = "application/pdf")
    public ResponseEntity<InputStreamResource> OpenCMR(@PathVariable String pdf_type, @PathVariable String rig_name, @PathVariable String file_name) throws FileNotFoundException {

        String fileName = "\\" + rootPath + "\\" + pdf_type + "\\" + rig_name + "\\" + file_name + ".pdf";

        File file = new File(fileName);

        log.info("{}", fileName );
        ResponseEntity<InputStreamResource> response = new ResponseEntity<>(
                new InputStreamResource(new FileInputStream(file))
                , HttpStatus.OK
        );

        return response;
    }


}
