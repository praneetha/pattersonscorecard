package com.pten.scorecard.repo;

import com.flutura.ImpalaRestApiController.ProcessBuilderWrapper;
import com.flutura.ImpalaRestApiController.UploadFileToPostgres;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.ByteBuffer;
import java.sql.*;
import java.util.*;

import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

@Component
public class ScorecardMaintenanceRepo {

    private static final Logger log = LoggerFactory.getLogger(ScorecardMaintenanceRepo.class);

    @Value("${file.destination}")
    private String destination;
    @Value("${file.intermediateLocalPath}")
    private String intermediateLocalPath;
    @Value("${file.intermediateServerPath}")
    private String intermediateServerPath;
    @Value("${file.uploadUser}")
    private String uploadUser;
    @Value("${file.jarFileLocation}")
    private String jarfileLocation;

    public ResponseEntity<String> uploadSourceFile(String year, String month, MultipartFile file) {

        int result = fileUpload(year, month, file, destination, intermediateLocalPath);
        if (result == 0) {
            return ResponseEntity.unprocessableEntity().body("File upload unsuccessful");
        } else {
            return ResponseEntity.ok("File upload successful");
        }
    }

    private int fileUpload(String year, String month, MultipartFile file, String destination,
                           String intermediateLocalPath) {

        int result = 0;

        File formattedFile = new File(intermediateLocalPath + file.getOriginalFilename()
                .replace(" ", "_"));

        try (FileOutputStream fileOutputStream = new FileOutputStream(formattedFile)) {

            ByteBuffer wrap = ByteBuffer.wrap(file.getBytes());
            fileOutputStream.getChannel().write(wrap);
            fileOutputStream.flush();

            if (formattedFile.length() > 0.0) {
                if ((double) formattedFile.length() / (1024 * 1024) < 50.0) {
                    if (FilenameUtils.getExtension(formattedFile.getName()).equals("xls") ||
                            FilenameUtils.getExtension(formattedFile.getName()).equals("xlsx")) {
                        result = checkForUploadDirectory(formattedFile, year, month, destination);
                    } else {
                        throw new Exception("Unsupported file type");
                    }
                } else {
                    throw new Exception("File size is greater than expected limit");
                }
            } else {
                throw new Exception("File is empty");
            }

        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in fileUpload: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in fileUpload: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in fileUpload: " + e.getMessage(),e);
        }
        return result;
    }

    private int checkForUploadDirectory(File file, String year, String month, String destination) {

        int result = 0;
        String newDestination;

        try {
            File destinationDirectory = new File(destination);
            if (destinationDirectory.exists()) {
                newDestination = directoryCheck(destination, year);
                newDestination = directoryCheck(newDestination, month);
            } else {
                boolean res = destinationDirectory.mkdir();
                if (res) {
                    System.out.println("Creation successful");
                    newDestination = directoryCheck(destination, year);
                    newDestination = directoryCheck(newDestination, month);
                } else {
                    throw new Exception("Directory creation unsuccessful");
                }
            }

            FileUtils.copyFile(file, new File(newDestination + "/" + file.getName()));
            FileUtils.copyFile(file, new File(intermediateServerPath + file.getName()));

            int res1 = fileUploadStatus(file, newDestination);
            int res2 = fileUploadStatus(file, intermediateServerPath);

            if (res1 != 0 && res2 != 0) {
                result = 1;
                file.delete();
            } else {
                throw new Exception("File upload unsuccessful");
            }

        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in checkForUploadDirectory: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in checkForUploadDirectory: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in checkForUploadDirectory: " + e.getMessage(),e);
        }
        return result;
    }

    private String directoryCheck(String destination, String directoryName) {

        String path = null;

        try {
            File directory = new File(destination + "/" + directoryName);
            if (directory.exists()) {
                path = directory.getAbsolutePath();
            } else {
                boolean res = directory.mkdir();
                if (res) {
                    path = directory.getAbsolutePath();
                } else {
                    throw new Exception("Directory creation unsuccessful");
                }
            }
        } catch (Exception e) {
            log.error("Exception in directoryCheck: " + e.getMessage(),e);
        }
        return path;
    }

    private int fileUploadStatus(File filename, String destination) {

        int flag = 0;

        try {
            File destionationDir = new File(destination);
            File[] files = destionationDir.listFiles();
            if (files != null) {
                for (File file : files) {
                    if (file.getName().equals(filename.getName())) {
                        flag = 1;
                        break;
                    }
                }
            }
        } catch (Exception e) {
            log.error("Exception in fileUploadStatus: " + e.getMessage(),e);
        }
        return flag;
    }

    public JSONObject connectToExternalDatabase(String year, String month, int fileTypeID, String uploadUser) {

        JSONObject jsonObject = new JSONObject();
        Properties properties = new Properties();
        String configFileName = "application.properties";
        Connection connection = null;

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {
            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);

            if (connection != null) {
                String fileTypeName = getFileTypeName(connection, fileTypeID);
                if (fileTypeName != null) {
                    int fileUploadID = insertToFileUploadTable(connection, fileTypeID, year, month, fileTypeName, uploadUser);
                    if (fileUploadID != 0) {
                        ArrayList<String> command = new ArrayList<>();
                        command.add("./jobrunner.sh");
                        command.add(year);
                        command.add(month);
                        command.add(fileTypeName);
                        command.add(Integer.toString(fileUploadID));
                        ProcessBuilderWrapper pb = new ProcessBuilderWrapper(command);
                        System.out.println("Status: " + pb.getStatus());
                        System.out.println("Info: " + pb.getInfos());
                        System.out.println("Errors: " + pb.getErrors());
                        if (pb.getStatus() == 1) {
                            UploadFileToPostgres uftp = new UploadFileToPostgres();
                            Timestamp updatedTime = uftp.getLastUpdatedDate(connection, fileUploadID);
                            jsonObject.put("FileUploadID", fileUploadID);
                            jsonObject.put("LastUpdatedDate", updatedTime);
                            System.out.println("File upload to Postgres successful");
                        } else {
                            jsonObject.put("FileUploadID", -1);
                            jsonObject.put("LastUpdatedDate", "NA");
                            throw new Exception("File upload to Postgres not successful");
                        }
                    }
                }
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in connectToExternalDatabase: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in connectToExternalDatabase: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in connectToExternalDatabase: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in connectToExternalDatabase: " + e.getMessage(),e);
            }
        }
        return jsonObject;
    }

    private Connection connectToPostgres(Properties properties) {

        Connection connection = null;

        try {
            String username = properties.getProperty("ptenInternal.datasource.scorecard.username");
            String password = properties.getProperty("ptenInternal.datasource.scorecard.password");
            String connectionURL = properties.getProperty("ptenInternal.datasource.scorecard.jdbc-url");

            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(connectionURL, username, password);

        } catch (Exception e) {
            log.error("Exception in connectToPostgres: " + e.getMessage(),e);
        }
        return connection;
    }

    private String getFileTypeName(Connection connection, int fileTypeID) {

        String fileTypeName = null;

        try (Statement statement = connection.createStatement()) {

            String query = "SELECT file_type_name as file_type_name" +
                    " FROM filetype " +
                    "WHERE file_type_id = '" + fileTypeID + "'";

            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                fileTypeName = result.getString("file_type_name");
            }
        } catch (SQLException e) {
            log.error("SQLException in getFileTypeName: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getFileTypeName: " + e.getMessage(),e);
        }
        return fileTypeName;
    }

    private int insertToFileUploadTable(Connection connection, int fileTypeID, String year, String month,
                                        String systemDataType, String uploadUser) {

        String effectiveDate = year + "-" + month + "-01";
        String fileName = systemDataType + " " + year + month + ".csv";
        int fileUploadID = 0;

        try (Statement statement = connection.createStatement()) {

            String checkQuery = "SELECT * FROM FileUpload " +
                    "WHERE effective_date = '" + effectiveDate + "' AND file_type_id = " + fileTypeID +
                    " AND upload_user = '" + uploadUser + "'";
            ResultSet result = statement.executeQuery(checkQuery);

            if (result == null) {
                String insertQuery = "INSERT INTO FileUpload " +
                        "(effective_date, upload_date, upload_user, filename, file_type_id, active) " +
                        "VALUES ('" + effectiveDate + "',now(),'" + uploadUser + "', '" + fileName + "', " +
                        fileTypeID + ", true)";
                statement.executeUpdate(insertQuery);
                String selectQuery = "SELECT effective_date FROM FileUpload " +
                        "WHERE filename = '" + fileName + "' and active = 't' AND upload_user = '" + uploadUser + "'";
                result = statement.executeQuery(selectQuery);
                while (result.next()) {
                    effectiveDate = result.getDate("effective_date").toString();
                }

            } else {
                String updateQuery = "UPDATE FileUpload SET " +
                        "active = false WHERE effective_date = '" + effectiveDate + "' AND " +
                        "file_type_id = " + fileTypeID +
                        " AND upload_user = '" + uploadUser + "'";
                statement.executeUpdate(updateQuery);
                String insertQuery = "INSERT INTO FileUpload " +
                        "(effective_date, upload_date, upload_user, filename, file_type_id, active) " +
                        "VALUES ('" + effectiveDate + "',now(),'" + uploadUser + "', '" + fileName + "', " +
                        fileTypeID + ", true)";
                statement.executeUpdate(insertQuery);
                String selectQuery = "SELECT effective_date FROM FileUpload " +
                        "WHERE filename = '" + fileName + "' and active = 't' AND upload_user = '" + uploadUser + "'";
                result = statement.executeQuery(selectQuery);
                while (result.next()) {
                    effectiveDate = result.getDate("effective_date").toString();
                }
            }
            fileUploadID = getFileUploadID(connection, effectiveDate, fileName, uploadUser);
        } catch (SQLException e) {
            log.error("SQLException in insertToFileUploadTable: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in insertToFileUploadTable: " + e.getMessage(),e);
        }
        return fileUploadID;
    }

    private int getFileUploadID(Connection connection, String effectiveDate, String fileName,
                                String uploadUser) {

        int fileUploadID = 0;

        try (Statement statement = connection.createStatement()) {
            String query = "SELECT file_upload_id " +
                    "FROM FileUpload " +
                    "WHERE filename = '" + fileName + "' AND effective_date = '" + effectiveDate + "' " +
                    "AND upload_user = '" + uploadUser + "'";
            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                fileUploadID = result.getInt("file_upload_id");
            }
        } catch (Exception e) {
            log.error("Exception in getFileUploadID: " + e.getMessage(),e);
        }
        return fileUploadID;
    }

    public ResponseEntity<Object> getFileTypeInfo(String year, String month) {

        JSONArray allSourceFileTypeDetails = new JSONArray();
        JSONArray allSystemDataDetails = new JSONArray();
        JSONObject allFileTypeDetails = new JSONObject();
        Properties properties = new Properties();
        String configFileName = "application.properties";
        Connection connection = null;

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);

            if (connection != null) {
                ArrayList<Integer> fileTypeIDList = getFileTypeList(connection);
                for (Integer integer : fileTypeIDList) {
                    String sourceType = getSourceType(connection, integer.toString());
                    JSONObject fileDetails = getFileDetails(year, month, integer, connection);
                    if (sourceType != null || !sourceType.isEmpty()) {
                        if (sourceType.equalsIgnoreCase("SourceFile")) {
                            allSourceFileTypeDetails.put(fileDetails);
                        } else if (sourceType.equalsIgnoreCase("SystemData")) {
                            allSystemDataDetails.put(fileDetails);
                        }
                    }
                }
                allFileTypeDetails.put("Source", allSourceFileTypeDetails);
                allFileTypeDetails.put("System", allSystemDataDetails);
            } else {
                throw new Exception("Connection to Postgres failed");
            }

        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in getFileTypeInfo: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in getFileTypeInfo: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getFileTypeInfo: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception e) {
                log.error("SQLException in getFileTypeInfo: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(allFileTypeDetails, HttpStatus.OK);
    }

    private String getSourceType(Connection connection, String fileTypeID) {

        String sourceType = null;

        try (Statement statement = connection.createStatement()) {

            String query = "SELECT source_type from filetype where file_type_id = " + fileTypeID;
            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                sourceType = result.getString("source_type");
            }
        } catch (SQLException e) {
            log.error("SQLException in getSourceType: " + e.getMessage(),e);
        }
        return sourceType;
    }

    private ArrayList<Integer> getFileTypeList(Connection connection) {

        ArrayList<Integer> fileTypeIDList = new ArrayList<>();

        try (Statement statement = connection.createStatement()) {

            String query = "SELECT file_type_id from filetype";
            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                fileTypeIDList.add(result.getInt("file_type_id"));
            }
        } catch (SQLException e) {
            log.error("SQLException in getFileTypeList: " + e.getMessage(),e);
        }
        return fileTypeIDList;
    }

    private JSONObject getFileDetails(String year, String month, int fileTypeID, Connection connection) {

        String fileType = null;
        int fileUploadID = 0;
        String uploadDate = null;
        String status = null;
        JSONObject jsonObject = new JSONObject();

        try (Statement statement = connection.createStatement()) {

            String query1 = "SELECT file_type_name from filetype where file_type_id = " + fileTypeID;
            ResultSet result1 = statement.executeQuery(query1);
            while (result1.next()) {
                fileType = result1.getString("file_type_name");
            }

            String query2 = "SELECT file_upload_id,upload_date,active from fileupload where file_type_id = " + fileTypeID +
                    "AND EXTRACT(MONTH FROM effective_date) = " + month + " AND EXTRACT(YEAR FROM effective_date) = " +
                    year + " and active = 't'";
            ResultSet result2 = statement.executeQuery(query2);
            while (result2.next()) {
                fileUploadID = result2.getInt("file_upload_id");
                uploadDate = result2.getString("upload_date");
                status = result2.getString("active");
            }

            jsonObject.put("FileTypeID", fileTypeID);
            jsonObject.put("FileTypeName", fileType);
            if (uploadDate == null) {
                jsonObject.put("FileUploadID", "NA");
                jsonObject.put("UploadDate", "NA");
                jsonObject.put("Status", "NA");
            } else {
                jsonObject.put("FileUploadID", fileUploadID);
                jsonObject.put("UploadDate", uploadDate);
                jsonObject.put("Status", status);
            }
        } catch (SQLException e) {
            log.error("SQLException in getFileDetails: " + e.getMessage(),e);
        } catch (JSONException e) {
            log.error("JSONException in getFileDetails: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getFileDetails: " + e.getMessage(),e);
        }
        return jsonObject;
    }

    public ResponseEntity<Object> getFileInfo(String fileTypeID, String fileUploadID) {

        int tableCount = 0;
        Boolean isEditable = false;
        JSONObject jsonObject = new JSONObject();
        JSONArray columnMapping = new JSONArray();
        Properties properties = new Properties();
        String configFileName = "application.properties";
        Connection connection = null;

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                String tableName = getTableName(connection, fileTypeID);
                if (tableName != null) {
                    tableCount = getTableCount(connection, fileUploadID, tableName);
                    columnMapping = getColumnMapping(connection, fileTypeID);
                    isEditable = getEditableFlag(connection, fileTypeID);
                }
            } else {
                throw new Exception("Connection to Postgres failed");
            }
            jsonObject.put("table_count", tableCount);
            jsonObject.put("column_mapping", columnMapping);
            jsonObject.put("isEditable", isEditable);

        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in getFileInfo: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in getFileInfo: " + e.getMessage(),e);
        } catch (JSONException e) {
            log.error("JSONException in getFileInfo: " + e.getMessage(),e);
        } catch (SQLException e) {
            log.error("SQLException in getFileInfo: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getFileInfo: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in getFileInfo: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(jsonObject, HttpStatus.OK);
    }

    private String getTableName(Connection connection, String fileTypeID) {

        String tableName = null;

        try (Statement statement = connection.createStatement()) {

            String query = "SELECT file_table_name from filetype where file_type_id = " + fileTypeID;
            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                tableName = result.getString("file_table_name");
            }
        } catch (SQLException e) {
            log.error("SQLException in getTableName: " + e.getMessage(),e);
        }
        return tableName;
    }

    private String getKeyColumn(Connection connection, String fileTypeID) {

        String keyColumn = null;

        try (Statement statement = connection.createStatement()) {

            String query = "SELECT key_column from filetype where file_type_id = " + fileTypeID;
            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                keyColumn = result.getString("key_column");
            }
        } catch (SQLException e) {
            log.error("SQLException in getKeyColumn: " + e.getMessage(),e);
        }
        return keyColumn;
    }

    private Boolean getEditableFlag(Connection connection, String fileTypeID) {

        int flag = -1;
        boolean Result = false;

        try (Statement statement = connection.createStatement()) {

            String query = "SELECT isEditable from filetype where file_type_id = " + fileTypeID;
            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                flag = result.getInt("isEditable");
            }

            Result = flag != 0;
        } catch (SQLException e) {
            log.error("SQLException in getEditableFlag: " + e.getMessage(),e);
        }
        return Result;
    }

    private int getTableCount(Connection connection, String fileUploadID, String tableName) {

        int count = 0;

        try (Statement statement = connection.createStatement()) {

            String query = "SELECT count(*) as count from " + tableName + " WHERE file_upload_id = " + fileUploadID;
            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                count = result.getInt("count");
            }

        } catch (SQLException e) {
            log.error("SQLException in getTableCount: " + e.getMessage(),e);
        }
        return count;
    }

    private JSONArray getColumnMapping(Connection connection, String fileTypeID) {

        JSONArray fullJSONArray = new JSONArray();

        try (Statement statement = connection.createStatement()) {

            String query = "SELECT column_mapping FROM FileType WHERE file_type_id = " + fileTypeID;
            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                JSONArray jsonArray = new JSONArray(result.getString("column_mapping"));
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject newJsonObject = jsonArray.getJSONObject(i);
                    Iterator keys = newJsonObject.keys();
                    while (keys.hasNext()) {
                        JSONArray newJSONArray = new JSONArray();
                        String table_column_name = keys.next().toString();
                        String visual_column_name = newJsonObject.getString(table_column_name);
                        newJSONArray.put(table_column_name);
                        newJSONArray.put(visual_column_name);
                        fullJSONArray.put(newJSONArray);
                    }
                }
            }
        } catch (SQLException e) {
            log.error("SQLException in getColumnMapping: " + e.getMessage(),e);
        } catch (JSONException e) {
            log.error("JSONException in getColumnMapping: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getColumnMapping: " + e.getMessage(),e);
        }
        return fullJSONArray;
    }

    public ResponseEntity<Object> getFileDetails(String fileTypeID, String fileUploadID, int startRow, int endRow) {

        JSONArray jsonArray = new JSONArray();
        Properties properties = new Properties();
        String configFileName = "application.properties";
        Connection connection = null;

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                String tableName = getTableName(connection, fileTypeID);
                String keyColumn = getKeyColumn(connection, fileTypeID);
                if (tableName != null) {
                    jsonArray = getTableData(connection, keyColumn, tableName, fileUploadID, startRow, endRow);
                }
            } else {
                throw new Exception("Connection to Postgres failed");
            }

        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in getFileDetails: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in getFileDetails: " + e.getMessage(),e);
        } catch (SQLException e) {
            log.error("SQLException in getFileDetails: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getFileDetails: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in getFileDetails: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(jsonArray, HttpStatus.OK);
    }

    private JSONArray getTableData(Connection connection, String keyColumn, String tableName,
                                   String fileUploadID, int startRow, int endRow) {

        JSONArray jsonArray = new JSONArray();

        try (Statement statement = connection.createStatement()) {

            if (keyColumn.equals("line_order")) {
                String query = "SELECT * from " + tableName + " WHERE file_upload_id = " + fileUploadID + " AND " + keyColumn + " >= " +
                        startRow + " AND " + keyColumn + " <= " + endRow;
                ResultSet result = statement.executeQuery(query);
                ResultSetMetaData rsmd = result.getMetaData();
                int columnsNumber = rsmd.getColumnCount();
                while (result.next()) {
                    JSONObject jsonObject = new JSONObject();
                    for (int i = 1; i <= columnsNumber; i++) {
                        if (result.getString(i) == null || result.getString(i).isEmpty() || result.getString(i).equals("")) {
                            String value = "";
                            jsonObject.put(rsmd.getColumnName(i), value);
                        } else {
                            jsonObject.put(rsmd.getColumnName(i), result.getString(i));
                        }
                    }
                    jsonArray.put(jsonObject);
                }
            } else {
                String query = "WITH CTE AS (SELECT row_number() over(order by file_upload_id," + keyColumn + "),* from " + tableName +
                        " WHERE file_upload_id = " + fileUploadID + ") SELECT * FROM CTE WHERE row_number >= " + startRow + " AND row_number <= " + endRow;
                ResultSet result = statement.executeQuery(query);
                ResultSetMetaData rsmd = result.getMetaData();
                int columnsNumber = rsmd.getColumnCount();
                while (result.next()) {
                    JSONObject jsonObject = new JSONObject();
                    for (int i = 1; i <= columnsNumber; i++) {
                        if (result.getString(i) == null || result.getString(i).isEmpty() || result.getString(i).equals("")) {
                            String value = "";
                            jsonObject.put(rsmd.getColumnName(i), value);
                        } else {
                            jsonObject.put(rsmd.getColumnName(i), result.getString(i));
                        }
                    }
                    jsonArray.put(jsonObject);
                }
            }
        } catch (SQLException e) {
            log.error("SQLException in getTableData: " + e.getMessage(),e);
        } catch (JSONException e) {
            log.error("JSONException in getTableData: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getTableData: " + e.getMessage(),e);
        }
        return jsonArray;
    }

    public ResponseEntity<Object> saveUpdatedData(String fileTypeID, String fileUploadID, String data) {

        int flag;
        String result = null;
        Properties properties = new Properties();
        String configFileName = "application.properties";
        Connection connection = null;

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                String tableName = getTableName(connection, fileTypeID);
                String keyColumn = getKeyColumn(connection, fileTypeID);
                boolean isEditable = getEditableFlag(connection, fileTypeID);
                flag = updateData(connection, tableName, fileUploadID, keyColumn, isEditable, data);
                if (flag == 0) {
                    result = "Update successful";
                } else {
                    throw new Exception("Update successful");
                }
            } else {
                throw new Exception("Connection to Postgres failed");
            }

        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in saveUpdatedData: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in saveUpdatedData: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in saveUpdatedData: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in saveUpdatedData: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    private int updateData(Connection connection, String tableName, String fileUploadID,
                           String keyColumn, boolean isEditable, String data) {

        int flag = 0;
        String newVal;

        try (Statement statement = connection.createStatement()) {

            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String keyColumnVal = jsonObject.get(keyColumn).toString();
                Iterator keys = jsonObject.keys();
                while (keys.hasNext()) {
                    String key = keys.next().toString();
                    if (!isEditable) {
                        if (key.equals("exemption_flag")) {
                            String value = jsonObject.get(key).toString();
                            if (value.equals("f")) {
                                newVal = "false";
                            } else {
                                newVal = "true";
                            }
                            String query = "UPDATE " + tableName + " SET " + key + " = " + newVal + " WHERE file_upload_id = " +
                                    fileUploadID + " AND " + keyColumn + " = " + keyColumnVal;
                            System.out.println(query);
                            statement.executeUpdate(query);
                        }
                    } else {
                        if (!(key.equals(keyColumn) || key.equals("file_upload_id") || key.equals("row_number"))) {
                            if (key.equals("exemption_flag")) {
                                String value = jsonObject.get(key).toString();
                                if (value.equals("f")) {
                                    newVal = "false";
                                } else {
                                    newVal = "true";
                                }
                                String query = "UPDATE " + tableName + " SET " + key + " = " + newVal + " WHERE file_upload_id = " +
                                        fileUploadID + " AND " + keyColumn + " = " + keyColumnVal;
                                System.out.println(query);
                                statement.executeUpdate(query);
                            } else {
                                String value = jsonObject.get(key).toString();
                                if (value.isEmpty()) {
                                    String query = "UPDATE " + tableName + " SET " + key + " = NULL" + " WHERE file_upload_id = " +
                                            fileUploadID + " AND " + keyColumn + " = " + keyColumnVal;
                                    System.out.println(query);
                                    statement.executeUpdate(query);
                                } else {
                                    String query = "UPDATE " + tableName + " SET " + key + " = '" + jsonObject.get(key) + "' WHERE file_upload_id = " +
                                            fileUploadID + " AND " + keyColumn + " = " + keyColumnVal;
                                    System.out.println(query);
                                    statement.executeUpdate(query);
                                }
                            }
                        }
                    }
                }
            }
        } catch (SQLException e) {
            log.error("SQLException in updateData: " + e.getMessage(),e);
            flag = 1;
        } catch (JSONException e) {
            log.error("JSONException in updateData: " + e.getMessage(),e);
            flag = 1;
        }
        return flag;
    }

    public ResponseEntity<Object> filterData(String fileTypeID, String fileUploadID, String data, int startRow, int endRow) {

        JSONObject jsonObject = new JSONObject();
        Properties properties = new Properties();
        String configFileName = "application.properties";
        Connection connection = null;

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                String tableName = getTableName(connection, fileTypeID);
                String keyColumn = getKeyColumn(connection, fileTypeID);
                jsonObject = getFilteredData(connection, fileUploadID, tableName, keyColumn, data, startRow, endRow);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in filterData: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in filterData: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in filterData: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in filterData: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(jsonObject, HttpStatus.OK);
    }

    private JSONObject getFilteredData(Connection connection, String fileUploadID, String tableName, String keyColumn,
                                       String data, int startRow, int endRow) {

        JSONArray jsonArray = new JSONArray();
        JSONObject finalJSON = new JSONObject();
        int filtercount = 0;

        try (Statement statement = connection.createStatement()) {
            StringBuilder query = new StringBuilder();
            StringBuilder query1 = new StringBuilder();
            JSONObject jsonObject = new JSONObject(data);
            if (keyColumn.equals("line_order")) {
                query.append("SELECT * FROM ")
                        .append(tableName)
                        .append(" WHERE file_upload_id = ")
                        .append(fileUploadID);
                Iterator keys = jsonObject.keys();
                while (keys.hasNext()) {
                    String key = keys.next().toString();
                    String value = jsonObject.get(key).toString();
                    query.append(" AND ")
                            .append(key)
                            .append(" ILIKE '%")
                            .append(value)
                            .append("%'");
                }
                query.append(" OFFSET ")
                        .append(startRow-1)
                        .append(" ROWS ")
                        .append("FETCH FIRST 500 ROWS ONLY");
                ResultSet result = statement.executeQuery(query.toString());
                ResultSetMetaData rsmd = result.getMetaData();
                int columnsNumber = rsmd.getColumnCount();
                while (result.next()) {
                    JSONObject jsonObject1 = new JSONObject();
                    for (int i = 1; i <= columnsNumber; i++) {
                        if (result.getString(i) == null || result.getString(i).isEmpty() || result.getString(i).equals("")) {
                            String value = "";
                            jsonObject1.put(rsmd.getColumnName(i), value);
                        } else {
                            jsonObject1.put(rsmd.getColumnName(i), result.getString(i));
                        }
                    }
                    jsonArray.put(jsonObject1);
                }
            } else {
                query.append("SELECT * from ")
                        .append(tableName)
                        .append(" WHERE file_upload_id = ")
                        .append(fileUploadID);
                Iterator keys = jsonObject.keys();
                while (keys.hasNext()) {
                    String key = keys.next().toString();
                    String value = jsonObject.get(key).toString();
                    query.append(" AND ")
                            .append(key)
                            .append(" ILIKE '%")
                            .append(value)
                            .append("%'");
                }
                query.append(" OFFSET ")
                        .append(startRow-1)
                        .append(" ROWS ")
                        .append("FETCH FIRST 500 ROWS ONLY");
                ResultSet result = statement.executeQuery(query.toString());
                ResultSetMetaData rsmd = result.getMetaData();
                int columnsNumber = rsmd.getColumnCount();
                while (result.next()) {
                    JSONObject jsonObject1 = new JSONObject();
                    for (int i = 1; i <= columnsNumber; i++) {
                        if (result.getString(i) == null || result.getString(i).isEmpty() || result.getString(i).equals("")) {
                            String value = "";
                            jsonObject1.put(rsmd.getColumnName(i), value);
                        } else {
                            jsonObject1.put(rsmd.getColumnName(i), result.getString(i));
                        }
                    }
                    jsonArray.put(jsonObject1);
                }
            }

            query1.append("SELECT count(*) as filtercount FROM ")
                    .append(tableName)
                    .append(" WHERE file_upload_id = ")
                    .append(fileUploadID);
            Iterator keys1 = jsonObject.keys();
            while (keys1.hasNext()) {
                String key = keys1.next().toString();
                String value = jsonObject.get(key).toString();
                query1.append(" AND ")
                        .append(key)
                        .append(" LIKE '%")
                        .append(value)
                        .append("%'");
            }
            ResultSet result1 = statement.executeQuery(query1.toString());
            while (result1.next()) {
                filtercount = result1.getInt("filtercount");
            }

            finalJSON.put("filteredData", jsonArray);
            finalJSON.put("filterCount", filtercount);

        } catch (SQLException e) {
            log.error("SQLException in filterData: " + e.getMessage(),e);
        } catch (JSONException e) {
            log.error("JSONException in filterData: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in filterData: " + e.getMessage(),e);
        }
        return finalJSON;
    }
}

