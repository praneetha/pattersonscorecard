package com.pten.scorecard.repo;



import com.pten.Utilities.DbUtils.PtenInternalDbUtilLib;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;


import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;



@Component
public class ScoreCardRepo {
    private static final Logger log = LoggerFactory.getLogger(com.pten.scorecard.repo.ScoreCardRepo.class);

    @Autowired
    PtenInternalDbUtilLib utils;


    public Collection getScoreCardrigList(String date) throws ParseException{

        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date scrdDate = formatter.parse(date);
        Timestamp scrdTime = new Timestamp(scrdDate.getTime());

        String query = "SELECT DISTINCT rig_name \n" +
                        "               ,scorecard_id\n"+
                       "FROM vw_ActiveScoreCardsRanked\n" +
                       "WHERE scorecard_date = :date\n" +
                       "ORDER BY rig_name ";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("date", scrdTime);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getTopScoreCard(String date) throws ParseException{

        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date scrdDate = formatter.parse(date);
        Timestamp scrdTime = new Timestamp(scrdDate.getTime());

        String query = "select rig_name\n" +
                "       , scorecard_id \n" +
                "From \n" +
                "vw_ActiveScoreCardsRanked \n" +
                "WHERE scorecard_date = :date\n" +
                "ORDER BY patterson_rank ASC\n" +
                "Limit 1";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("date", scrdTime);
        return utils.executeNamedQuery(query, namedParameters);
    }



    public Collection getLatestReportDate() throws ParseException{
        String query = "select Max(scorecard_date) AS latest_date\n" +
                        "From vw_ActiveScoreCardLineItems vasl\n" +
                        "INNER JOIN vw_ActiveScoreCardsRanked vasc on vasl.scorecard_id = vasc.scorecard_id";

        return utils.executeNamedQuery(query, null);
    }

    /**
     *
     * @return
     * @throws ParseException
     */
    public Collection getAllActiveScoreCards() throws ParseException {

        String query = "SELECT scorecard_id\n" +
                "        , rig_name\n" +
                "        , scorecard_date\n" +
                "        , overall_score\n" +
                "        , region_rank AS area_rank\n" +
                "        , division_rank AS region_rank\n" +
                "        , patterson_rank\n" +
                "        , region_average AS area_average\n" +
                "        , division_average AS region_average\n" +
                "        , patterson_average \n" +
                "        , division_name AS region_name\n" +
                "        , region_name AS area_name\n" +
                "FROM vw_ActiveScoreCardsRanked\n" +
                "ORDER BY scorecard_date DESC";

        SqlParameterSource namedParameters = new MapSqlParameterSource();
        return utils.executeNamedQuery(query, namedParameters);
    }

    /**
     *
     * @param rigName
     * @param date
     * @return
     * @throws ParseException
     */
    public Collection getScoreCard(String rigName, String date)throws ParseException {

        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date endDate = formatter.parse(date);

        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        cal.add(Calendar.MONTH, -6);

        Date startDate = cal.getTime();

        Timestamp startTime = new Timestamp(startDate.getTime());
        Timestamp endTime = new Timestamp(endDate.getTime());


        String query = "SELECT scorecard_id\n" +
                "        , rig_name\n" +
                "        , scorecard_date\n" +
                "        , overall_score\n" +
                "        , region_rank AS area_rank\n" +
                "        , division_rank AS region_rank\n" +
                "        , patterson_rank\n" +
                "        , region_average AS area_average\n" +
                "        , division_average AS region_average\n" +
                "        , patterson_average \n" +
                "        , division_name AS region_name\n" +
                "        , region_name AS area_name\n" +
                "        , region_count AS area_count\n" +
                "        , division_count AS region_count\n" +
                "        , patterson_count\n" +
                "        , north_top AS north_top_rig\n" +
                "        , CAST(north_top_score AS DECIMAL(10,2)) AS north_top_score\n" +
                "        , south_top AS south_top_rig\n" +
                "        , CAST(south_top_score AS DECIMAL(10,2)) AS south_top_score\n" +
                "        , patterson_top AS pat_top_rig\n" +
                "        , CAST(patterson_top_score AS DECIMAL(10,2)) AS pat_top_score\n" +
                "FROM vw_ActiveScoreCardsRanked\n" +
                "WHERE rig_name = :rig AND scorecard_date Between :start and :end\n" +
                "ORDER BY scorecard_date DESC";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("start", startTime)
                .addValue("end", endTime)
                .addValue( "rig", rigName);
        return utils.executeNamedQuery(query, namedParameters);

    }

    public Collection getScoreCardDetails(int id) throws ParseException{

        String query = "SELECT vasd.scorecard_id\n" +
                "        , category\n" +
                "        , metric\n" +
                "        , points\n" +
                "        , metric_description\n" +
                "        , Nullif(avg_value, 'NaN') AS avg_value\n" +
                "        , Nullif(avg_points, 'NaN') AS avg_points\n" +
                "        , Nullif(raw_value, 'NaN') AS raw_value\n" +
                "        , Nullif(raw_points, 'NaN') AS raw_points\n" +
                "        , Nullif(prior_month_value, 'NaN') AS prior_month_value\n" +
                "        , Nullif(prior_month_points, 'NaN') AS prior_month_points\n" +
                "        , Nullif(two_month_value, 'NaN') AS two_month_value\n" +
                "        , Nullif(two_month_points, 'NaN') AS two_month_points\n" +
                "        , avg_color AS avg_points_color\n" +
                "        , raw_color AS raw_points_color\n" +
                "        , prior_month_color prior_month_points_color \n" +
                "        , two_month_color AS two_month_points_color\n" +
                "        , display_order\n" +
                "        , division_name AS region_name\n" +
                "        , region_name AS area_name\n" +
                "FROM vw_ActiveScoreCardLineItems vasd\n" +
                "INNER JOIN vw_ActiveScoreCardsRanked vas ON vas.scorecard_id = vasd.scorecard_id\n" +
                "WHERE vas.scorecard_id = :id;";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", id);
        return utils.executeNamedQuery(query, namedParameters);
    }


    public Collection getScoreCardDetails(String rigName, String date) throws ParseException{

        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date dtDate = formatter.parse(date);

        Timestamp time = new Timestamp(dtDate.getTime());


        String query = "SELECT vasd.scorecard_id\n" +
                "        , category\n" +
                "        , metric\n" +
                "        , points\n" +
                "        , metric_description\n" +
                "        , Nullif(avg_value, 'NaN') AS avg_value\n" +
                "        , Nullif(avg_points, 'NaN') AS avg_points\n" +
                "        , Nullif(raw_value, 'NaN') AS raw_value\n" +
                "        , Nullif(raw_points, 'NaN') AS raw_points\n" +
                "        , Nullif(prior_month_value, 'NaN') AS prior_month_value\n" +
                "        , Nullif(prior_month_points, 'NaN') AS prior_month_points\n" +
                "        , Nullif(two_month_value, 'NaN') AS two_month_value\n" +
                "        , Nullif(two_month_points, 'NaN') AS two_month_points\n" +
                "        , avg_color AS avg_points_color\n" +
                "        , raw_color AS raw_points_color\n" +
                "        , prior_month_color prior_month_points_color \n" +
                "        , two_month_color AS two_month_points_color\n" +
                "        , display_order\n" +
                "        , division_name AS region_name\n" +
                "        , region_name AS area_name\n" +
                "FROM vw_ActiveScoreCardLineItems vasd\n" +
                "INNER JOIN vw_ActiveScoreCardsRanked vas ON vas.scorecard_id = vasd.scorecard_id\n" +
                "WHERE vas.rig_name = :rig and vas.scorecard_date = :time";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("time", time)
                .addValue( "rig", rigName);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getCRIDetails(Collection<Integer> id) throws ParseException{

        String query = "SELECT vsd.scorecard_id\n" +
                "       , superintendent\n" +
                "       , superintendent_score\n" +
                "       , superintendent_date\n" +
                "       , hse_auditor\n" +
                "       , hse_score\n" +
                "       , hse_date \n" +
                "       , cri_evaluator\n" +
                "       , vasr.rig_name\n" +
                "       , vasr.scorecard_date\n" +
                "FROM vw_scorecardcridetails vsd\n" +
                "INNER JOIN vw_ActiveScoreCardsRanked vasr on vasr.scorecard_id = vsd.scorecard_id\n" +
                "WHERE  vsd.scorecard_id in (:id);";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", id);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getTripSpeedDetails(Collection<Integer> id) throws ParseException{

        String query = "SELECT vsd.scorecard_id\n" +
                "       , tripping_speed AS metric\n" +
                "       , num_tripping_stands AS num_stands\n" +
                "       , vasr.rig_name\n" +
                "       , vasr.scorecard_date\n" +
                "FROM vw_scorecardcasedtrippingspeeddetails vsd\n" +
                "INNER JOIN vw_ActiveScoreCardsRanked vasr on vasr.scorecard_id = vsd.scorecard_id\n" +
                "WHERE vsd.scorecard_id in (:id);";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", id);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getTripConnDetails(Collection<Integer> id) throws ParseException{

        String query = "SELECT vsd.scorecard_id\n" +
                "        , tripping_connection_s2s AS metric\n" +
                "        , num_tripping_stands AS num_stands\n" +
                "        , vasr.rig_name\n" +
                "        , vasr.scorecard_date\n" +
                "FROM vw_scorecardtrippingconnectiondetails vsd\n" +
                "INNER JOIN vw_ActiveScoreCardsRanked vasr on vasr.scorecard_id = vsd.scorecard_id\n" +
                "WHERE vsd.scorecard_id in(:id);";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", id);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getDrillConnDetails(Collection<Integer> id) throws ParseException{

        String query = "SELECT vsd.scorecard_id\n" +
                "       , drilling_connection_s2s AS metric\n" +
                "       , num_drilling_stands AS num_stands\n" +
                "       , vasr.rig_name\n" +
                "       , vasr.scorecard_date\n" +
                "FROM vw_scorecarddrillingconnectiondetails vsd\n" +
                "INNER JOIN vw_ActiveScoreCardsRanked vasr on vasr.scorecard_id = vsd.scorecard_id\n" +
                "WHERE vsd.scorecard_id in(:id);";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", id);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getAssetInveDetails(Collection<Integer> id) throws ParseException{

        String query = "SELECT vsd.scorecard_id\n" +
                "        , vsd.rig_name\n" +
                "        , category_code\n" +
                "        , working_overhead\n" +
                "        , oracle_asset_number\n" +
                "        , compliant_scanned\n" +
                "        , rfid_tag\n" +
                "        , last_scanned_date\n" +
                "        , num_scanned\n" +
                "        , num_assets \n" +
                "        , vasr.scorecard_date \n" +
                "FROM vw_scorecardassetinventorydetails vsd\n" +
                "INNER JOIN vw_ActiveScoreCardsRanked vasr on vasr.scorecard_id = vsd.scorecard_id\n" +
                "WHERE vsd.scorecard_id in(:id);";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", id);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getTurnoverDetails(Collection<Integer> id) throws ParseException{

        String query = "    SELECT vsd.scorecard_id\n" +
                "            , employee_number\n" +
                "            , first_name\n" +
                "            , last_name\n" +
                "            , leaving_reason\n" +
                "            , job_name\n" +
                "            , latest_hire_date\n" +
                "            , period_of_service_term_date\n" +
                "            , months_of_service\n" +
                "            , vasr.rig_name \n" +
                "            , vasr.scorecard_date \n" +
                "    FROM vw_scorecardturnoverdetails vsd\n" +
                "    INNER JOIN vw_ActiveScoreCardsRanked vasr on vasr.scorecard_id = vsd.scorecard_id\n" +
                "    WHERE vsd.scorecard_id in(:id);";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", id);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getOpsWoDetails(Collection<Integer> id) throws ParseException{

        String query = "SELECT vsd.scorecard_id\n" +
                "        , asset_number\n" +
                "        , asset_description\n" +
                "        , model_number\n" +
                "        , work_order_number\n" +
                "        , work_order_description\n" +
                "        , date_released\n" +
                "        , days_since_released \n" +
                "        , make \n" +
                "        , vasr.rig_name \n" +
                "        , vasr.scorecard_date \n" +
                "FROM vw_scorecardopsworkorderdetails vsd\n" +
                "INNER JOIN vw_ActiveScoreCardsRanked vasr on vasr.scorecard_id = vsd.scorecard_id\n" +
                "WHERE vsd.scorecard_id in(:id);";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", id);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getTechWoDetails(Collection<Integer> id) throws ParseException{

        String query = "SELECT vsd.scorecard_id\n" +
                "        , asset_number\n" +
                "        , asset_description\n" +
                "        , model_number\n" +
                "        , work_order_number\n" +
                "        , work_order_description\n" +
                "        , date_released\n" +
                "        , days_since_released \n" +
                "        , make \n" +
                "        , vasr.rig_name \n" +
                "        , vasr.scorecard_date \n" +
                "FROM vw_scorecardtechworkorderdetails vsd\n" +
                "INNER JOIN vw_ActiveScoreCardsRanked vasr on vasr.scorecard_id = vsd.scorecard_id\n" +
                "WHERE vsd.scorecard_id in(:id);";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", id);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getTibWoDetails(Collection<Integer> id) throws ParseException{

        String query = "SELECT vsd.scorecard_id\n" +
                "        , asset_number\n" +
                "        , asset_description\n" +
                "        , model_number\n" +
                "        , work_order_number\n" +
                "        , work_order_description\n" +
                "        , work_order_status\n" +
                "        , date_released\n" +
                "        , days_since_released\n" +
                "        , make\n" +
                "        , vasr.rig_name \n" +
                "        , vasr.scorecard_date \n" +
                "FROM vw_scorecardtibtsldetails vsd\n" +
                "INNER JOIN vw_ActiveScoreCardsRanked vasr on vasr.scorecard_id = vsd.scorecard_id\n" +
                "WHERE vsd.scorecard_id in(:id);";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", id);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getCMRErrorDetails(Collection<Integer> id) throws ParseException{

        String query = "SELECT vsd.scorecard_id\n" +
                "        , report_date\n" +
                "        , error_first_observed\n" +
                "        , error_last_observed\n" +
                "        , error_text \n" +
                "        , vasr.rig_name \n" +
                "        , vasr.scorecard_date \n" +
                "FROM vw_scorecardcmrerrordetails vsd\n" +
                "INNER JOIN vw_ActiveScoreCardsRanked vasr on vasr.scorecard_id = vsd.scorecard_id\n" +
                "WHERE vsd.scorecard_id in(:id);";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", id);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getImsOpenedDetails(Collection<Integer> id) throws ParseException{

        String query = "SELECT vsd.scorecard_id\n" +
                "        , incident_number\n" +
                "        , incident_date\n" +
                "        , incident_submitted_date AS investigation_date\n" +
                "        , potential_severity\n" +
                "        , consequences\n" +
                "        , summary \n" +
                "        , vasr.rig_name\n" +
                "        , vasr.scorecard_date\n" +
                "FROM vw_scorecardims24hourdetails vsd\n" +
                "INNER JOIN vw_ActiveScoreCardsRanked vasr on vasr.scorecard_id = vsd.scorecard_id\n" +
                "WHERE vsd.scorecard_id in(:id)";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", id);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getImsClosedDetails(Collection<Integer> id) throws ParseException{

        String query = "SELECT vsd.scorecard_id\n" +
                "        , incident_number\n" +
                "        , incident_date\n" +
                "        , investigation_completion_date AS investigation_date\n" +
                "        , potential_severity\n" +
                "        , consequences\n" +
                "        , summary\n" +
                "        , vasr.rig_name\n" +
                "        , vasr.scorecard_date\n" +
                " FROM vw_scorecardims72hourdetails vsd\n" +
                " INNER JOIN vw_ActiveScoreCardsRanked vasr on vasr.scorecard_id = vsd.scorecard_id\n" +
                " WHERE vsd.scorecard_id in(:id);";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", id);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getImsHseEventDetails(Collection<Integer> id) throws ParseException{

        String query = "SELECT vsd.scorecard_id\n" +
                "        , incident_number\n" +
                "        , incident_date\n" +
                "        , potential_severity\n" +
                "        , consequences\n" +
                "        , summary\n" +
                "        , event_type \n" +
                "        , vasr.rig_name\n" +
                "        , vasr.scorecard_date\n" +
                "FROM vw_scorecardhseeventsdetails vsd\n" +
                "INNER JOIN vw_ActiveScoreCardsRanked vasr on vasr.scorecard_id = vsd.scorecard_id\n" +
                "WHERE vsd.scorecard_id in(:id);";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", id);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getCapexMonthlyDetails(Collection<Integer> id) throws ParseException{

        String query = "SELECT vsd.scorecard_id\n" +
                "        , project_number\n" +
                "        , project_name\n" +
                "        , current_month_spend AS spend\n" +
                "        , vasr.rig_name\n" +
                "        , vasr.scorecard_date\n" +
                "FROM vw_scorecardcapexmonthlydetails vsd\n" +
                "INNER JOIN vw_ActiveScoreCardsRanked vasr on vasr.scorecard_id = vsd.scorecard_id\n" +
                "WHERE vsd.scorecard_id in(:id);";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", id);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getCapexYearlyDetails(Collection<Integer> id) throws ParseException{

        String query = "SELECT vsd.scorecard_id\n" +
                "        , project_number\n" +
                "        , project_name\n" +
                "        , total_spend_last_12_months AS spend\n" +
                "        , vasr.rig_name\n" +
                "        , vasr.scorecard_date\n" +
                "FROM vw_scorecardcapexannualdetails vsd\n" +
                "INNER JOIN vw_ActiveScoreCardsRanked vasr on vasr.scorecard_id = vsd.scorecard_id\n" +
                "WHERE vsd.scorecard_id in(:id);";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", id);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getWalkDetails(Collection<Integer> id) throws ParseException{

        String query = "SELECT wd.scorecard_id\n" +
                "                , file_upload_id\n" +
                "                , wd.well_num\n" +
                "                , wd.well_name\n" +
                "                , wd.new_walk_ordinal\n" +
                "                , wd.start_time\n" +
                "                , wd.end_time\n" +
                "                , wd.overall_walk_time\n" +
                "                , wd.f_walk_time\n" +
                "                , wd.from_timestamp\n" +
                "                , wd.to_timestamp\n" +
                "                , wd.hours\n" +
                "                , concat(wd.code, wd.sub_code) AS code\n" +
                "                , wd.sub_code\n" +
                "                , wd.details\n" +
                "                , rw.score\n" +
                "                , vasr.rig_name\n" +
                "                , vasr.scorecard_date\n" +
                "\tFROM public.vw_scorecardwalkdetails wd\n" +
                "\tINNER JOIN vw_scorecardgroupedwalks rw on wd.scorecard_id = rw.scorecard_id \n" +
                "\t       and rw.walk_num = wd.new_walk_ordinal  \n" +
                "\tINNER JOIN vw_ActiveScoreCardsRanked vasr on vasr.scorecard_id = wd.scorecard_id\n" +
                "\twhere wd.scorecard_id in (:id)";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", id);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getRmctDetails(Collection<Integer> id) throws ParseException{

        String query = "SELECT vsd.scorecard_id\n" +
                "       , metric_id\n" +
                "       , vsd.rig_name\n" +
                "       , move_number\n" +
                "       , move_ordinal\n" +
                "       , move_start_date\n" +
                "       , move_end_date\n" +
                "       , rig_move_distance\n" +
                "       , rig_move_time\n" +
                "       , target_time\n" +
                "       , partial_credit_time\n" +
                "       , score\n" +
                "       , from_timestamp\n" +
                "       , to_timestamp\n" +
                "       , concat(code, sub_code) AS code\n" +
                "       , sub_code\n" +
                "       , hours\n" +
                "       , details\n" +
                "       , vasr.scorecard_date\n" +
                "FROM public.vw_scorecardrmctdetails vsd\n" +
                "INNER JOIN vw_ActiveScoreCardsRanked vasr on vasr.scorecard_id = vsd.scorecard_id\n" +
                "where vsd.scorecard_id in (:id)";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", id);
        return utils.executeNamedQuery(query, namedParameters);
    }

}
