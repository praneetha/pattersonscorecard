package com.pten.scorecard.repo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

@Component
public class ScorecardPeopleManagerRepo {

    private static final Logger log = LoggerFactory.getLogger(ScorecardPeopleManagerRepo.class);

    private Connection connectToPostgres(Properties properties) {

        Connection connection = null;

        try {
            String username = properties.getProperty("ptenInternal.datasource.username");
            String password = properties.getProperty("ptenInternal.datasource.password");
            String connectionURL = properties.getProperty("ptenInternal.datasource.scorecard.jdbc-url");

            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(connectionURL, username, password);

        } catch (Exception e) {
            log.error("Exception in connectToPostgres: " + e.getMessage(),e);
        }
        return connection;
    }

    public ResponseEntity<Object> getOwnerList() {

        Connection connection = null;
        Properties properties = new Properties();
        String configFileName = "application.properties";
        JSONArray ownerInfo = new JSONArray();

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                ownerInfo = getOwners(connection);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in getOwnerList: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in getOwnerList: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getOwnerList: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in getOwnerList: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(ownerInfo, HttpStatus.OK);
    }

    private JSONArray getOwners(Connection connection) {

        JSONArray jsonArray = new JSONArray();

        try (Statement statement = connection.createStatement()) {

            String query = "SELECT o.owner_id, concat(o.first_name,' ',o.last_name) as owner_name, ot.owner_type_name " +
                            "FROM owner o JOIN ownertype ot ON o.owner_type_id = ot.owner_type_id ORDER BY owner_name";
            ResultSet resultSet = statement.executeQuery(query);

            while(resultSet.next()) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("ownerID",resultSet.getInt("owner_id"));
                jsonObject.put("ownerName",resultSet.getString("owner_name"));
                jsonObject.put("ownerTypeName",resultSet.getString("owner_type_name"));
                jsonArray.put(jsonObject);
            }

        } catch (SQLException e) {
            log.error("SQLException in getOwners: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getOwners: " + e.getMessage(),e);
        }
        return jsonArray;
    }

    public ResponseEntity<Object> getOwnerTypeList() {

        Connection connection = null;
        Properties properties = new Properties();
        String configFileName = "application.properties";
        JSONArray ownerTypeInfo = new JSONArray();

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                ownerTypeInfo = getOwnerTypes(connection);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in getOwnerTypeList: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in getOwnerTypeList: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getOwnerTypeList: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in getOwnerTypeList: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(ownerTypeInfo, HttpStatus.OK);
    }

    private JSONArray getOwnerTypes(Connection connection) {

        JSONArray jsonArray = new JSONArray();

        try (Statement statement = connection.createStatement()) {

            String query = "SELECT owner_type_id, owner_type_name FROM ownertype ORDER BY owner_type_name";
            ResultSet resultSet = statement.executeQuery(query);

            while(resultSet.next()) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("ownerTypeID",resultSet.getInt("owner_type_id"));
                jsonObject.put("ownerTypeName",resultSet.getString("owner_type_name"));
                jsonArray.put(jsonObject);
            }

        } catch (SQLException e) {
            log.error("SQLException in getOwnerTypes: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getOwnerTypes: " + e.getMessage(),e);
        }
        return jsonArray;
    }

    public ResponseEntity<Object> getLocationByOwner(int ownerID) {

        Connection connection = null;
        Properties properties = new Properties();
        String configFileName = "application.properties";
        JSONArray locationInfo = new JSONArray();

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                locationInfo = getLocationInfo(connection,ownerID);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in getLocationByOwner: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in getLocationByOwner: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getLocationByOwner: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in getLocationByOwner: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(locationInfo, HttpStatus.OK);
    }

    private JSONArray getLocationInfo(Connection connection, int ownerID) {

        JSONArray locationList = new JSONArray();

        try (Statement statement = connection.createStatement()) {

            String query = "SELECT otr.region_id, r.region_name, otr.start_date, otr.end_date FROM ownertoregion otr " +
                    "JOIN region r ON r.region_id = otr.region_id " +
                    "WHERE otr.owner_id = " + ownerID + " ORDER BY otr.start_date desc";
            ResultSet resultSet = statement.executeQuery(query);

            while(resultSet.next()) {
                JSONObject locationInfo = new JSONObject();
                locationInfo.put("regionID",resultSet.getInt("region_id"));
                locationInfo.put("regionName",resultSet.getString("region_name"));
                locationInfo.put("startDate",resultSet.getString("start_date"));
                locationInfo.put("endDate",resultSet.getString("end_date"));
                locationList.put(locationInfo);
            }

        } catch (SQLException e) {
            log.error("SQLException in getLocationInfo: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getLocationInfo: " + e.getMessage(),e);
        }
        return locationList;
    }

    public ResponseEntity<Object> getSuperintendentByOwner(int ownerID) {

        Connection connection = null;
        Properties properties = new Properties();
        String configFileName = "application.properties";
        JSONArray superintendentInfo = new JSONArray();

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                superintendentInfo = getSuperintendentInfo(connection,ownerID);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in getSuperintendentByOwner: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in getSuperintendentByOwner: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getSuperintendentByOwner: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in getSuperintendentByOwner: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(superintendentInfo, HttpStatus.OK);
    }

    private JSONArray getSuperintendentInfo(Connection connection, int ownerID) {

        JSONArray superintendentList = new JSONArray();

        try (Statement statement = connection.createStatement()) {

            String query = "SELECT oto.owned_owner_id, concat(o.first_name, ' ', o.last_name) as owner_name, oto.start_date, " +
                    "oto.end_date FROM ownertoowner oto " +
                    "JOIN owner o on o.owner_id = oto.owned_owner_id " +
                    "WHERE oto.owner_id != oto.owned_owner_id AND oto.owner_id = " + ownerID + " ORDER BY oto.start_date desc";
            ResultSet resultSet = statement.executeQuery(query);

            while(resultSet.next()) {
                JSONObject superintendentInfo = new JSONObject();
                superintendentInfo.put("ownerID",resultSet.getInt("owned_owner_id"));
                superintendentInfo.put("ownerName",resultSet.getString("owner_name"));
                superintendentInfo.put("startDate",resultSet.getString("start_date"));
                superintendentInfo.put("endDate",resultSet.getString("end_date"));
                superintendentList.put(superintendentInfo);
            }

        } catch (SQLException e) {
            log.error("SQLException in getSuperintendentInfo: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getSuperintendentInfo: " + e.getMessage(),e);
        }
        return superintendentList;
    }

    public ResponseEntity<Object> addNewOwner(String ownerName, int ownerTypeID, String empNum) {

        Connection connection = null;
        JSONObject result = new JSONObject();
        Properties properties = new Properties();
        String configFileName = "application.properties";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                result = newOwnerAddition(connection,ownerName,ownerTypeID,empNum);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in addNewOwner: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in addNewOwner: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in addNewOwner: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in addNewOwner: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    private JSONObject newOwnerAddition(Connection connection, String ownerName, int ownerTypeID, String empNum) {

        int result1;
        int ownerID = 0;
        String message;
        String ownerType = null;
        JSONObject jsonObject = new JSONObject();

        try (Statement statement = connection.createStatement()) {

            String ownerTypeQuery = "SELECT owner_type_name FROM ownertype WHERe owner_type_id = " + ownerTypeID;
            ResultSet ownerTypeResult = statement.executeQuery(ownerTypeQuery);

            while(ownerTypeResult.next()) {
                ownerType = ownerTypeResult.getString("owner_type_name");
            }

            if(ownerName.contains(" ")) {
                String firstName = ownerName.split(" ")[0];
                String lastName = ownerName.split(" ")[1];

                String insertRigQuery = "INSERT INTO owner (first_name,last_name,emp_num,owner_type_id) VALUES (" +
                        "'" + firstName + "','" + lastName + "','" + empNum + "'," + ownerTypeID + ")";
                result1 = statement.executeUpdate(insertRigQuery);

                if(result1 != 0) {
                    String ownerIDQuery = "SELECT owner_id FROM owner WHERE first_name = '" + firstName +
                            "' AND last_name = '" + lastName + "' AND emp_num = '" + empNum + "'";
                    ResultSet resultSet = statement.executeQuery(ownerIDQuery);

                    while(resultSet.next()) {
                        ownerID = resultSet.getInt("owner_id");
                    }
                }
            } else {
                String insertRigQuery = "INSERT INTO owner (first_name,last_name,emp_num,owner_type_id) VALUES (" +
                        "'" + ownerName + "',' ','" + empNum + "'," + ownerTypeID + ")";
                result1 = statement.executeUpdate(insertRigQuery);

                if(result1 != 0) {
                    String ownerIDQuery = "SELECT owner_id FROM owner WHERE first_name = '" + ownerName +
                            "' AND emp_num = '" + empNum + "'";
                    ResultSet resultSet = statement.executeQuery(ownerIDQuery);

                    while(resultSet.next()) {
                        ownerID = resultSet.getInt("owner_id");
                    }
                }
            }

            if(ownerID != 0) {
                message = "Success";
            } else {
                message = "Failure";
            }

            jsonObject.put("message",message);
            jsonObject.put("ownerID",ownerID);
            jsonObject.put("ownerTypeName",ownerType);

        } catch (JSONException e) {
            log.error("JSONException in newOwnerAddition: " + e.getMessage(),e);
            message = "Failure";
            try {
                jsonObject.put("message",message);
                jsonObject.put("ownerID",0);
                jsonObject.put("ownerTypeName",ownerType);
            } catch (JSONException ex) {
                log.error("JSONException in newOwnerAddition: " + ex.getMessage(),ex);
            }
        } catch (SQLException e) {
            log.error("SQLException in newOwnerAddition: " + e.getMessage(),e);
            message = "Failure";
            try {
                jsonObject.put("message",message);
                jsonObject.put("ownerID",0);
                jsonObject.put("ownerTypeName",ownerType);
            } catch (JSONException ex) {
                log.error("JSONException in newOwnerAddition: " + ex.getMessage(),ex);
            }
        } catch (Exception e) {
            log.error("Exception in newOwnerAddition: " + e.getMessage(),e);
            message = "Failure";
            try {
                jsonObject.put("message",message);
                jsonObject.put("ownerID",0);
                jsonObject.put("ownerTypeName",ownerType);
            } catch (JSONException ex) {
                log.error("JSONException in newOwnerAddition: " + ex.getMessage(),ex);
            }
        }
        return jsonObject;
    }

    public ResponseEntity<Object> addOwnerLocation(int ownerID, int regionID, String startDate, String endDate) {

        String message = null;
        Connection connection = null;
        Properties properties = new Properties();
        String configFileName = "application.properties";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                message = addNewRegion(connection,ownerID,regionID,startDate,endDate);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in addOwnerLocation: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in addOwnerLocation: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in addOwnerLocation: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in addOwnerLocation: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(message,HttpStatus.OK);
    }

    private String addNewRegion(Connection connection, int ownerID, int regionID, String startDate, String endDate) {

        String message;
        String oldStartDate1 = null;
        String oldStartDate2 = null;
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try (Statement statement = connection.createStatement()) {

            if(endDate == null || endDate.isEmpty()) {

                String query1 = "SELECT * FROM ownertoregion WHERE start_date = " +
                        "(SELECT max(start_date) FROM ownertoregion WHERE owner_id = " + ownerID +
                        "AND start_date < '" + startDate + "')";
                ResultSet resultSet1 = statement.executeQuery(query1);

                while (resultSet1.next()) {
                    oldStartDate1 = resultSet1.getString("start_date");
                }

                if(oldStartDate1 != null) {

                    Date oldDate1 = formatter.parse(startDate);
                    cal.setTime(oldDate1);
                    cal.add(Calendar.SECOND,-1);
                    Date newDate1 = cal.getTime();
                    String newEndDate = formatter.format(newDate1);

                    String query2 = "UPDATE ownertoregion SET end_date = '" + newEndDate + "' WHERE owner_id = " + ownerID +
                            " AND start_date = '" + oldStartDate1 + "'";
                    statement.executeUpdate(query2);

                    String query3 = "DELETE FROM ownertoregion WHERE owner_id = " + ownerID + " AND start_date > '" +
                            startDate + "'";
                    statement.executeUpdate(query3);

                    String query4 = "INSERT INTO ownertoregion (region_id,owner_id,start_date,end_date) VALUES (" +
                            regionID + "," + ownerID + ",'" + startDate + "', null)";
                    int result = statement.executeUpdate(query4);

                    if(result != 0) {
                        message = "Success";
                    } else {
                        message = "Failure";
                    }
                } else {

                    String query3 = "DELETE FROM ownertoregion WHERE owner_id = " + ownerID + " AND start_date > '" +
                            startDate + "'";
                    statement.executeUpdate(query3);

                    String query4 = "INSERT INTO ownertoregion (region_id,owner_id,start_date,end_date) VALUES (" +
                            regionID + "," + ownerID + ",'" + startDate + "', null)";
                    int result = statement.executeUpdate(query4);

                    if(result != 0) {
                        message = "Success";
                    } else {
                        message = "Failure";
                    }
                }
            }
            else {

                String query1 = "SELECT * FROM ownertoregion WHERE start_date = " +
                        "(SELECT max(start_date) FROM ownertoregion WHERE owner_id = " + ownerID +
                        "AND start_date < '" + startDate + "')";
                ResultSet resultSet1 = statement.executeQuery(query1);

                while (resultSet1.next()) {
                    oldStartDate1 = resultSet1.getString("start_date");
                }

                if(oldStartDate1 != null) {
                    Date oldDate1 = formatter.parse(startDate);
                    cal.setTime(oldDate1);
                    cal.add(Calendar.SECOND,-1);
                    Date newDate1 = cal.getTime();
                    String newEndDate = formatter.format(newDate1);

                    String query2 = "UPDATE ownertoregion SET end_date = '" + newEndDate + "' WHERE owner_id = " + ownerID +
                            " AND start_date = '" + oldStartDate1 + "'";
                    statement.executeUpdate(query2);

                    String query3 = "DELETE FROM ownertoregion WHERE owner_id = " + ownerID + " AND start_date > '" + startDate +
                            "' AND end_date < '" + endDate + "'";
                    statement.executeUpdate(query3);

                    String query4 = "SELECT * FROM ownertoregion WHERE start_date = " +
                            "(SELECT min(start_date) FROM ownertoregion WHERE owner_id = " + ownerID +
                            "AND start_date > '" + startDate + "')";
                    ResultSet resultSet4 = statement.executeQuery(query4);

                    while (resultSet4.next()) {
                        oldStartDate2 = resultSet4.getString("start_date");
                    }

                    if(oldStartDate2 != null) {

                        Date oldDate2 = formatter.parse(endDate);
                        cal.setTime(oldDate2);
                        cal.add(Calendar.SECOND,1);
                        Date newDate2 = cal.getTime();
                        String newStartDate = formatter.format(newDate2);

                        String query5 = "UPDATE ownertoregion SET start_date = '" + newStartDate + "' WHERE owner_id = " + ownerID +
                                " AND start_date = '" + oldStartDate2 + "'";
                        statement.executeUpdate(query5);

                        String query6 = "INSERT INTO ownertoregion (region_id,owner_id,start_date,end_date) VALUES (" +
                                regionID + "," + ownerID + ",'" + startDate + "','" + endDate + "')";
                        int result = statement.executeUpdate(query6);

                        if(result != 0) {
                            message = "Success";
                        } else {
                            message = "Failure";
                        }
                    } else {

                        String query6 = "INSERT INTO ownertoregion (region_id,owner_id,start_date,end_date) VALUES (" +
                                regionID + "," + ownerID + ",'" + startDate + "','" + endDate + "')";
                        int result = statement.executeUpdate(query6);

                        if(result != 0) {
                            message = "Success";
                        } else {
                            message = "Failure";
                        }
                    }
                } else {
                    String query4 = "SELECT * FROM ownertoregion WHERE start_date = " +
                            "(SELECT min(start_date) FROM ownertoregion WHERE owner_id = " + ownerID +
                            "AND start_date > '" + startDate + "')";
                    ResultSet resultSet4 = statement.executeQuery(query4);

                    while (resultSet4.next()) {
                        oldStartDate2 = resultSet4.getString("start_date");
                    }

                    if(oldStartDate2 != null) {

                        Date oldDate2 = formatter.parse(endDate);
                        cal.setTime(oldDate2);
                        cal.add(Calendar.SECOND,1);
                        Date newDate2 = cal.getTime();
                        String newStartDate = formatter.format(newDate2);

                        String query5 = "UPDATE ownertoregion SET start_date = '" + newStartDate + "' WHERE owner_id = " + ownerID +
                                " AND start_date = '" + oldStartDate2 + "'";
                        statement.executeUpdate(query5);

                        String query6 = "INSERT INTO ownertoregion (region_id,owner_id,start_date,end_date) VALUES (" +
                                regionID + "," + ownerID + ",'" + startDate + "','" + endDate + "')";
                        int result = statement.executeUpdate(query6);

                        if(result != 0) {
                            message = "Success";
                        } else {
                            message = "Failure";
                        }
                    } else {

                        String query6 = "INSERT INTO ownertoregion (region_id,owner_id,start_date,end_date) VALUES (" +
                                regionID + "," + ownerID + ",'" + startDate + "','" + endDate + "')";
                        int result = statement.executeUpdate(query6);

                        if(result != 0) {
                            message = "Success";
                        } else {
                            message = "Failure";
                        }
                    }
                }
            }

        } catch (SQLException e) {
            log.error("SQLException in addNewRegion: " + e.getMessage(),e);
            return "Failure";
        } catch (Exception e) {
            log.error("Exception in addNewRegion: " + e.getMessage(),e);
            return "Failure";
        }
        return message;
    }

    public ResponseEntity<Object> addOwnerOwner(int ownerID, int ownedOwnerID, String startDate) {

        String message = null;
        Connection connection = null;
        Properties properties = new Properties();
        String configFileName = "application.properties";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                message = addNewOwner(connection,ownerID,ownedOwnerID,startDate);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in addOwnerOwner: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in addOwnerOwner: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in addOwnerOwner: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in addOwnerOwner: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(message,HttpStatus.OK);
    }

    private String addNewOwner(Connection connection, int ownerID, int ownedOwnerID, String startDate) {

        String message;

        try (Statement statement = connection.createStatement()) {


            String insertQuery = "INSERT INTO ownertoowner (owner_id,owned_owner_id,start_date,end_date) VALUES (" +
                    ownerID + "," + ownedOwnerID + ",'" + startDate + "',null)";
            int result1 = statement.executeUpdate(insertQuery);

            if(result1 != 0) {
                message = "Success";
            }
            else {
                message = "Failure";
            }

        } catch (SQLException e) {
            log.error("SQLException in addNewOwner: " + e.getMessage(),e);
            return "Failure";
        } catch (Exception e) {
            log.error("Exception in addNewOwner: " + e.getMessage(),e);
            return "Failure";
        }
        return message;
    }

    public ResponseEntity<Object> editOwnerLocation(String oldRecord, String newRecord) {

        Connection connection = null;
        JSONObject jsonObject = new JSONObject();
        Properties properties = new Properties();
        String configFileName = "application.properties";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                jsonObject = editOwnerLocationInfo(connection,oldRecord,newRecord);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in editOwnerLocation: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in editOwnerLocation: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in editOwnerLocation: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in editOwnerLocation: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(jsonObject,HttpStatus.OK);
    }

    private JSONObject editOwnerLocationInfo(Connection connection, String oldRecord, String newRecord) {

        int count = 0;
        String message = null;
        JSONObject jsonObject = new JSONObject();
        HashMap<String,Integer> changedKeys= new HashMap<>();

        try (Statement statement = connection.createStatement()){

            JSONObject oldRecordJSON = new JSONObject(oldRecord);
            JSONObject newRecordJSON = new JSONObject(newRecord);

            changedKeys.put("regionName",0);
            changedKeys.put("startDate",0);
            changedKeys.put("endDate",0);

            Iterator keys = oldRecordJSON.keys();
            while (keys.hasNext()) {
                String key = keys.next().toString();
                if(changedKeys.containsKey(key)) {
                    if(!oldRecordJSON.get(key).equals(newRecordJSON.get(key))) {
                        changedKeys.replace(key,1);
                        count ++;
                    }
                }
            }

            if(count != 0) {

                String query = "DELETE FROM ownertoregion WHERE owner_id = " + oldRecordJSON.getString("ownerID") +
                        " AND region_id = " + oldRecordJSON.getString("regionID") + " AND start_date = '" +
                        oldRecordJSON.getString("startDate") + "'";
                statement.executeUpdate(query);

                message = addNewRegion(connection,newRecordJSON.getInt("ownerID"),newRecordJSON.getInt("regionID"),
                        newRecordJSON.getString("startDate"),newRecordJSON.getString("endDate"));
            } else {
                jsonObject.put("message","Nothing to be updated");
            }

            jsonObject.put("message",message);

        } catch (JSONException e) {
            log.error("JSONException in editOwnerLocationInfo: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in editOwnerLocationInfo: " + e.getMessage(),e);
        }
        return  jsonObject;
    }

    public ResponseEntity<Object> editOwnerOwner(String oldRecord, String newRecord) {

        Connection connection = null;
        JSONObject jsonObject = new JSONObject();
        Properties properties = new Properties();
        String configFileName = "application.properties";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                jsonObject = editOwnerOwnerInfo(connection,oldRecord,newRecord);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in editOwnerOwner: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in editOwnerOwner: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in editOwnerOwner: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in editOwnerOwner: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(jsonObject,HttpStatus.OK);
    }

    private JSONObject editOwnerOwnerInfo(Connection connection, String oldRecord, String newRecord) {

        JSONObject jsonObject = new JSONObject();

        try (Statement statement = connection.createStatement()) {

            JSONObject oldRecordJSON = new JSONObject(oldRecord);
            JSONObject newRecordJSON = new JSONObject(newRecord);

            String query = "UPDATE ownertoowner SET owned_owner_id = " + newRecordJSON.getString("ownedOwnerID") +
                        ", start_date = '" + newRecordJSON.getString("startDate") + "', end_date = '" +
                        newRecordJSON.getString("endDate") + "' WHERE owner_id = " +
                        oldRecordJSON.getString("ownerID") + " AND owned_owner_id = " +
                        oldRecordJSON.getString("ownedOwnerID") + " AND start_date = '" +
                        oldRecordJSON.getString("startDate") + "'";
            statement.executeUpdate(query);
            jsonObject.put("message", "Success");

        } catch (JSONException e) {
            log.error("JSONException in editOwnerOwnerInfo: " + e.getMessage(),e);
        } catch (SQLException e) {
            log.error("SQLException in editOwnerOwnerInfo: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in editOwnerOwnerInfo: " + e.getMessage(),e);
        }
        return  jsonObject;
    }

    public ResponseEntity<Object> deleteOwnerLocation(String record) {

        Connection connection = null;
        JSONObject jsonObject = new JSONObject();
        Properties properties = new Properties();
        String configFileName = "application.properties";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                jsonObject = deleteRigLocationInfo(connection,record);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in deleteOwnerLocation: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in deleteOwnerLocation: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in deleteOwnerLocation: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in deleteOwnerLocation: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(jsonObject,HttpStatus.OK);
    }

    private JSONObject deleteRigLocationInfo(Connection connection, String record) {

        String message;
        String nextEndDate = null;
        String prevStartDate = null;
        String nextStartDate = null;
        Calendar cal = Calendar.getInstance();
        JSONObject jsonObject = new JSONObject();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try (Statement statement = connection.createStatement()) {

            JSONObject recordJSON = new JSONObject(record);

            String prevSelectQuery = "SELECT * FROM ownertoregion WHERE owner_id = " + recordJSON.getString("ownerID") +
                    " AND start_date = (SELECT min(start_date) FROM ownertoregion WHERE owner_id = " +
                    recordJSON.getString("ownerID") + " AND start_date > '" +
                    recordJSON.getString("startDate") + "')";
            ResultSet resultSet1 = statement.executeQuery(prevSelectQuery);

            while (resultSet1.next()) {
                prevStartDate = resultSet1.getString("start_date");
            }

            String nextSelectQuery = "SELECT * FROM ownertoregion WHERE owner_id = " + recordJSON.getString("ownerID") +
                    " AND start_date = (SELECT max(start_date) FROM ownertoregion WHERE owner_id = " +
                    recordJSON.getString("ownerID") + " AND start_date < '" +
                    recordJSON.getString("startDate") + "')";
            ResultSet resultSet2 = statement.executeQuery(nextSelectQuery);

            while (resultSet2.next()) {
                nextStartDate = resultSet2.getString("start_date");
                nextEndDate = resultSet2.getString("end_date");
            }

            if(prevStartDate == null || nextStartDate == null) {

                String deleteQuery = "DELETE FROM ownertoregion WHERE owner_id = " + recordJSON.getString("ownerID") +
                        " AND region_id = " + recordJSON.getString("regionID") + " AND start_date = '" +
                        recordJSON.getString("startDate") + "'";
                int result = statement.executeUpdate(deleteQuery);

                if(result != 0) {
                    message = "Success";
                } else {
                    message = "Failure";
                }
            }
            else {

                Date oldDate = formatter.parse(nextEndDate);
                cal.setTime(oldDate);
                cal.add(Calendar.SECOND,1);
                Date newDate = cal.getTime();
                String newStartDate = formatter.format(newDate);

                String deleteQuery = "DELETE FROM ownertoregion WHERE owner_id = " + recordJSON.getString("ownerID") +
                        " AND region_id = " + recordJSON.getString("regionID") + " AND start_date = '" +
                        recordJSON.getString("startDate") + "'";
                int result = statement.executeUpdate(deleteQuery);

                String updateQuery1 = "UPDATE ownertoregion SET start_date = '" + newStartDate + "' WHERE owner_id = " +
                        recordJSON.getString("ownerID") + " AND start_date = '" + prevStartDate + "'";
                statement.executeUpdate(updateQuery1);

                if(result != 0) {
                    message = "Success";
                } else {
                    message = "Failure";
                }
            }

            jsonObject.put("message",message);

        } catch(JSONException e) {
            log.error("JSONException in deleteRigLocationInfo: " +e.getMessage(),e);
        } catch(SQLException e) {
            log.error("SQLException in deleteRigLocationInfo: " +e.getMessage(),e);
        } catch(Exception e) {
            log.error("Exception in deleteRigLocationInfo: " +e.getMessage(),e);
        }
        return jsonObject;
    }

    public ResponseEntity<Object> deleteOwnerOwner(String record) {

        Connection connection = null;
        JSONObject jsonObject = new JSONObject();
        Properties properties = new Properties();
        String configFileName = "application.properties";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                jsonObject = deleteOwnerOwnerInfo(connection,record);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in deleteOwnerOwner: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in deleteOwnerOwner: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in deleteOwnerOwner: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in deleteOwnerOwner: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(jsonObject,HttpStatus.OK);
    }

    private JSONObject deleteOwnerOwnerInfo(Connection connection, String record) {

        String message;
        JSONObject jsonObject = new JSONObject();

        try (Statement statement = connection.createStatement()) {

            JSONObject recordJSON = new JSONObject(record);

            String query = "DELETE FROM ownertoowner WHERE owner_id = " + recordJSON.getString("ownerID") +
                    " AND owned_owner_id = " + recordJSON.getString("ownedOwnerID") + " AND start_date = '" +
                    recordJSON.getString("startDate") + "'";
            int result = statement.executeUpdate(query);

            if(result != 0) {
                message = "Success";
            } else {
                message = "Failure";
            }
            jsonObject.put("message",message);

        } catch(JSONException e) {
            log.error("JSONException in deleteOwnerOwnerInfo: " +e.getMessage(),e);
        } catch(SQLException e) {
            log.error("SQLException in deleteOwnerOwnerInfo: " +e.getMessage(),e);
        } catch(Exception e) {
            log.error("Exception in deleteOwnerOwnerInfo: " +e.getMessage(),e);
        }
        return jsonObject;
    }
}
