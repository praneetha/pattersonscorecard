package com.pten.scorecard.repo;

import com.pten.Utilities.DbUtils.PtenInternalDbUtilLib;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;


@Component
public class CompetitionScoreCardRepo {

    private static final Logger log = LoggerFactory.getLogger(com.pten.scorecard.repo.CompetitionScoreCardRepo.class);

    @Autowired
    PtenInternalDbUtilLib utils;


    public Collection getScoreCardrigList(String date) throws ParseException {

        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date scrdDate = formatter.parse(date);
        Timestamp scrdTime = new Timestamp(scrdDate.getTime());

        String query = "SELECT DISTINCT rig_name \n" +
                "               ,scorecard_id\n" +
                "               ,patterson_rank\n"+
                "FROM vw_activescorecardsranked_competitive\n" +
                "WHERE scorecard_date = :date\n" +
                "ORDER BY patterson_rank";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("date", scrdTime);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getTopScoreCard(String date) throws ParseException{

        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date scrdDate = formatter.parse(date);
        Timestamp scrdTime = new Timestamp(scrdDate.getTime());

        String query = "select rig_name\n" +
                "       , scorecard_id \n" +
                "From \n" +
                "vw_activescorecardsranked_competitive \n" +
                "WHERE scorecard_date = :date\n" +
                "ORDER BY patterson_rank ASC\n" +
                "Limit 1";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("date", scrdTime);
        return utils.executeNamedQuery(query, namedParameters);
    }



    public Collection getLatestReportDate() throws ParseException{
        String query = "select Max(scorecard_date) AS latest_date\n" +
                "From vw_activescorecardlineitems_competitive vasl\n" +
                "INNER JOIN vw_activescorecardsranked_competitive vasc on vasl.scorecard_id = vasc.scorecard_id";

        return utils.executeNamedQuery(query, null);
    }

    /**
     *
     * @return
     * @throws ParseException
     */
    public Collection getAllActiveScoreCards() throws ParseException {

        String query = "SELECT scorecard_id\n" +
                "        , rig_name\n" +
                "        , scorecard_date\n" +
                "        , overall_score\n" +
                "        , region_rank AS area_rank\n" +
                "        , division_rank AS region_rank\n" +
                "        , patterson_rank\n" +
                "        , region_average AS area_average\n" +
                "        , division_average AS region_average\n" +
                "        , patterson_average \n" +
                "        , division_name AS region_name\n" +
                "        , region_name AS area_name\n" +
                "FROM vw_activescorecardsranked_competitive\n" +
                "ORDER BY scorecard_date DESC";

        SqlParameterSource namedParameters = new MapSqlParameterSource();
        return utils.executeNamedQuery(query, namedParameters);
    }

    /**
     *
     * @param rigName
     * @param date
     * @return
     * @throws ParseException
     */
    public Collection getScoreCard(String rigName, String date)throws ParseException {

        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date startDate = formatter.parse(date);

        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        cal.add(Calendar.MONTH, +2);

        Date endDate = cal.getTime();

        Timestamp startTime = new Timestamp(startDate.getTime());
        Timestamp endTime = new Timestamp(endDate.getTime());


        String query = "SELECT scorecard_id\n" +
                "        , rig_name\n" +
                "        , scorecard_date\n" +
                "        , overall_score\n" +
                "        , region_rank AS area_rank\n" +
                "        , division_rank AS region_rank\n" +
                "        , patterson_rank\n" +
                "        , region_average AS area_average\n" +
                "        , division_average AS region_average\n" +
                "        , patterson_average \n" +
                "        , division_name AS region_name\n" +
                "        , region_name AS area_name\n" +
                "        , region_count AS area_count\n" +
                "        , division_count AS region_count\n" +
                "        , patterson_count\n" +
                "        , north_top AS north_top_rig\n" +
                "        , CAST(north_top_score AS DECIMAL(10,2)) AS north_top_score\n" +
                "        , south_top AS south_top_rig\n" +
                "        , CAST(south_top_score AS DECIMAL(10,2)) AS south_top_score\n" +
                "        , patterson_top AS pat_top_rig\n" +
                "        , CAST(patterson_top_score AS DECIMAL(10,2)) AS pat_top_score\n" +
                "FROM vw_activescorecardsranked_competitive\n" +
                "WHERE rig_name = :rig AND scorecard_date Between :start and :end\n" +
                "ORDER BY scorecard_date DESC";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("start", startTime)
                .addValue("end", endTime)
                .addValue( "rig", rigName);
        return utils.executeNamedQuery(query, namedParameters);

    }

    public Collection getScoreCardDetails(int id) throws ParseException{

        String query = "SELECT vasd.scorecard_id\n" +
                "        , category\n" +
                "        , metric\n" +
                "        , points\n" +
                "        , metric_description\n" +
                "        , Nullif(avg_value, 'NaN') AS avg_value\n" +
                "        , Nullif(avg_points, 'NaN') AS avg_points\n" +
                "        , Nullif(raw_value, 'NaN') AS raw_value\n" +
                "        , Nullif(raw_points, 'NaN') AS raw_points\n" +
                "        , Nullif(prior_month_value, 'NaN') AS prior_month_value\n" +
                "        , Nullif(prior_month_points, 'NaN') AS prior_month_points\n" +
                "        , Nullif(two_month_value, 'NaN') AS two_month_value\n" +
                "        , Nullif(two_month_points, 'NaN') AS two_month_points\n" +
                "        , avg_color AS avg_points_color\n" +
                "        , raw_color AS raw_points_color\n" +
                "        , prior_month_color prior_month_points_color \n" +
                "        , two_month_color AS two_month_points_color\n" +
                "        , display_order\n" +
                "        , division_name AS region_name\n" +
                "        , region_name AS area_name\n" +
                "FROM vw_activescorecardlineitems_competitive vasd\n" +
                "INNER JOIN vw_activescorecardsranked_competitive vas ON vas.scorecard_id = vasd.scorecard_id\n" +
                "WHERE vas.scorecard_id = :id;";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", id);
        return utils.executeNamedQuery(query, namedParameters);
    }


    public Collection getScoreCardDetails(String rigName, String date) throws ParseException{

        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date dtDate = formatter.parse(date);

        Timestamp time = new Timestamp(dtDate.getTime());


        String query = "SELECT vasd.scorecard_id\n" +
                "        , category\n" +
                "        , metric\n" +
                "        , points\n" +
                "        , metric_description\n" +
                "        , Nullif(avg_value, 'NaN') AS avg_value\n" +
                "        , Nullif(avg_points, 'NaN') AS avg_points\n" +
                "        , Nullif(raw_value, 'NaN') AS raw_value\n" +
                "        , Nullif(raw_points, 'NaN') AS raw_points\n" +
                "        , Nullif(prior_month_value, 'NaN') AS prior_month_value\n" +
                "        , Nullif(prior_month_points, 'NaN') AS prior_month_points\n" +
                "        , Nullif(two_month_value, 'NaN') AS two_month_value\n" +
                "        , Nullif(two_month_points, 'NaN') AS two_month_points\n" +
                "        , avg_color AS avg_points_color\n" +
                "        , raw_color AS raw_points_color\n" +
                "        , prior_month_color prior_month_points_color \n" +
                "        , two_month_color AS two_month_points_color\n" +
                "        , display_order\n" +
                "        , division_name AS region_name\n" +
                "        , region_name AS area_name\n" +
                "FROM vw_activescorecardlineitems_competitive vasd\n" +
                "INNER JOIN vw_activescorecardsranked_competitive vas ON vas.scorecard_id = vasd.scorecard_id\n" +
                "WHERE vas.rig_name = :rig and vas.scorecard_date = :time";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("time", time)
                .addValue( "rig", rigName);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getScoreCardTrend(int scorecard_id) throws ParseException{
        String query = "SELECT vascr.scorecard_id, \n" +
                "       vascr.scorecard_Date,\n" +
                "       vasclf.overall_score  \n" +
                "FROM vw_activescorecardsourcecards vascsc\n" +
                "INNER JOIN vw_activescorecardlinefeeder_competitive vasclf ON vascsc.source_scorecard_id = vasclf.scorecard_id\n" +
                "INNER JOIN vw_activescorecardsranked vascr ON vascsc.source_scorecard_id = vascr.scorecard_id\n" +
                "WHERE vascsc.scorecard_id = :scorecard_id\n" +
                "ORDER BY vascr.scorecard_Date ASC";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("scorecard_id", scorecard_id);
        return utils.executeNamedQuery(query, namedParameters);
    }
}