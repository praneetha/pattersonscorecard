package com.pten.scorecard.repo;



import com.pten.Utilities.DbUtils.PtenInternalDbUtilLib;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;


import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;



@Component
public class SuperintendentScoreCardRepo {
    private static final Logger log = LoggerFactory.getLogger(com.pten.scorecard.repo.SuperintendentScoreCardRepo.class);

    @Autowired
    PtenInternalDbUtilLib utils;

    public Collection getLatestReportDate() throws ParseException{
        String query = "SELECT MAX(scorecard_date) AS latest_date\n" +
                "FROM   vw_ActiveSuperScoreCardsRanked vassc\n" +
                "INNER JOIN vw_ActiveSuperScoreCardLineItems vassl ON vassl.scorecard_id = vassc.scorecard_id\n" +
                "WHERE owner_type_id = 1;";

        return utils.executeNamedQuery(query, null);
    }

    public Collection getSuperintendentList(String date) throws ParseException {

        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date scrdDate = formatter.parse(date);
        Timestamp scrdTime = new Timestamp(scrdDate.getTime());

        String query = "SELECT DISTINCT(owner_name) AS superintendent \n" +
                        "      ,scorecard_id\n"+
                        "FROM vw_ActiveSuperScoreCardsRanked " +
                        "WHERE scorecard_date = :date\n" +
                        "AND owner_type_id = 1\n" +
                        "ORDER BY owner_name;";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("date", scrdTime);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getScoreCard(String superintendent, String date)throws ParseException {

        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date endDate = formatter.parse(date);

        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        cal.add(Calendar.MONTH, -6);

        Date startDate = cal.getTime();

        Timestamp startTime = new Timestamp(startDate.getTime());
        Timestamp endTime = new Timestamp(endDate.getTime());


        String query = "SELECT scorecard_id\n" +
                "        , owner_name\n" +
                "        , scorecard_date\n" +
                "        , overall_score\n" +
                "        , region_rank AS area_rank\n" +
                "        , division_rank AS region_rank\n" +
                "        , patterson_rank\n" +
                "        , region_average AS area_average\n" +
                "        , division_average AS region_average\n" +
                "        , patterson_average \n" +
                "        , division_name AS region_name\n" +
                "        , region_name AS area_name \n" +
                "        , region_count AS area_count\n" +
                "        , division_count AS region_count\n" +
                "        , patterson_count\n" +
                "        , north_top AS north_top_super\n" +
                "        , CAST(north_top_score AS DECIMAL(10,2)) AS north_top_score\n" +
                "        , south_top AS south_top_super\n" +
                "        , CAST(south_top_score AS DECIMAL(10,2)) AS south_top_score\n" +
                "        , patterson_top AS pat_top_super\n" +
                "        , CAST(patterson_top_score AS DECIMAL(10,2)) AS pat_top_score\n" +
                "FROM vw_activesuperscorecardsranked\n" +
                "WHERE owner_name = :superintendent AND scorecard_date Between :start and :end\n" +
                "AND owner_type_id = 1\n" +
                "ORDER BY scorecard_date DESC";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("start", startTime)
                .addValue("end", endTime)
                .addValue( "superintendent", superintendent);
        return utils.executeNamedQuery(query, namedParameters);

    }

    public Collection getScoreCard(int id)throws ParseException {

        String query = "SELECT scorecard_id\n" +
                "        , owner_name\n" +
                "        , scorecard_date\n" +
                "        , overall_score\n" +
                "        , region_rank AS area_rank\n" +
                "        , division_rank AS region_rank\n" +
                "        , patterson_rank\n" +
                "        , region_average AS area_average\n" +
                "        , division_average AS region_average\n" +
                "        , patterson_average \n" +
                "        , division_name AS region_name\n" +
                "        , region_name AS area_name \n" +
                "        , region_count AS area_count\n" +
                "        , division_count AS region_count\n" +
                "        , patterson_count\n" +
                "        , north_top AS north_top_super\n" +
                "        , CAST(north_top_score AS DECIMAL(10,2)) AS north_top_score\n" +
                "        , south_top AS south_top_super\n" +
                "        , CAST(south_top_score AS DECIMAL(10,2)) AS south_top_score\n" +
                "        , patterson_top AS pat_top_super\n" +
                "        , CAST(patterson_top_score AS DECIMAL(10,2)) AS pat_top_score\n" +
                "FROM vw_activesuperscorecardsranked\n" +
                "WHERE scorecard_id = :id\n" +
                "ORDER BY scorecard_date DESC;";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", id);
        return utils.executeNamedQuery(query, namedParameters);

    }

    public Collection getScoreCardTrend(int id)throws ParseException {

        String query = "SELECT scorecard_id\n" +
                "            , observation_date\n" +
                "            , source_name\n" +
                "            , observation_score\n" +
                "    FROM vw_activesuperscorecardranklines\n" +
                "    where scorecard_id = :id\n" +
                "    AND observation_date IS NOT NULL;";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", id);
        return utils.executeNamedQuery(query, namedParameters);

    }

    public Collection getScoreCardDetails(int id) throws ParseException{

        String query = "SELECT vassc.scorecard_id\n" +
                "       , source_name\n" +
                "       , category\n" +
                "       , metric\n" +
                "       , points\n" +
                "       , metric_description\n" +
                "       , Nullif(current_month_points, 'NaN') AS raw_points\n" +
                "       , Nullif(current_month_value, 'NaN') AS raw_value\n" +
                "       , current_month_scorecard_id\n" +
                "       , Nullif(prior_month_points, 'NaN') AS prior_month_points\n" +
                "       , Nullif(prior_month_value, 'NaN') AS prior_month_value\n" +
                "       , prior_month_scorecard_id\n" +
                "       , Nullif(two_month_points, 'NaN') AS two_month_points\n" +
                "       , Nullif(two_month_value, 'NaN') AS two_month_value\n" +
                "       , two_month_scorecard_id\n" +
                "       , current_month_color AS raw_points_color\n" +
                "       , prior_month_color AS prior_month_points_color\n" +
                "       , two_month_color AS two_month_points_color\n" +
                "       , display_order\n" +
                "       , division_name AS region_name\n" +
                "       , region_name AS area_name \n" +
                "FROM vw_activesuperscorecardlineitems vassl\n" +
                "INNER JOIN vw_ActiveSuperScoreCardsRanked vassc on vassc.scorecard_id = vassl.scorecard_id\n" +
                "WHERE vassc.scorecard_id = :id;";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", id);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getScoreCardDetails(String superintendent, String date) throws ParseException{

        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date dtDate = formatter.parse(date);

        Timestamp time = new Timestamp(dtDate.getTime());


        String query = "SELECT vassc.scorecard_id\n" +
                "       , source_name\n" +
                "       , category\n" +
                "       , metric\n" +
                "       , points\n" +
                "       , metric_description\n" +
                "       , Nullif(current_month_points, 'NaN') AS avg_points\n" +
                "       , Nullif(current_month_value, 'NaN') AS avg_value\n" +
                "       , current_month_scorecard_id\n" +
                "       , Nullif(prior_month_points, 'NaN') AS prior_month_points\n" +
                "       , Nullif(prior_month_value, 'NaN') AS prior_month_value\n" +
                "       , prior_month_scorecard_id\n" +
                "       , Nullif(two_month_points, 'NaN') AS two_month_points\n" +
                "       , Nullif(two_month_value, 'NaN') AS two_month_value\n" +
                "       , two_month_scorecard_id\n" +
                "       , current_month_color AS raw_points_color\n" +
                "       , prior_month_color AS prior_month_points_color\n" +
                "       , two_month_color AS two_month_points_color\n" +
                "       , display_order\n" +
                "       , division_name AS region_name\n" +
                "       , region_name AS area_name \n" +
                "FROM vw_activesuperscorecardlineitems vassl\n" +
                "INNER JOIN vw_ActiveSuperScoreCardsRanked vassc on vassc.scorecard_id = vassl.scorecard_id\n" +
                "WHERE vassc.owner_name = :superintendent and vas.scorecard_date = :time";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("time", time)
                .addValue( "superintendent", superintendent);
        return utils.executeNamedQuery(query, namedParameters);
    }


    public Collection getRigScoreCardIds(Collection<String> rigs, String date)throws ParseException {

        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date endDate = formatter.parse(date);

        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        cal.add(Calendar.MONTH, -6);

        Date startDate = cal.getTime();

        Timestamp startTime = new Timestamp(startDate.getTime());
        Timestamp endTime = new Timestamp(endDate.getTime());


        String query = "SELECT scorecard_id\n" +
                "        , rig_name\n" +
                "        , scorecard_date\n" +
                "FROM vw_ActiveScoreCardsRanked\n" +
                "WHERE rig_name in (:rig) AND scorecard_date Between :start and :end\n" +
                "ORDER BY scorecard_date DESC";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("start", startTime)
                .addValue("end", endTime)
                .addValue( "rig", rigs);
        return utils.executeNamedQuery(query, namedParameters);

    }

    public Collection getRigScoreCardIds(Collection<Integer> id)throws ParseException {

        String query = "SELECT DISTINCT rig_name\n" +
                "                ,source_scorecard_id AS scorecard_id\n" +
                "                ,vasr.scorecard_date AS scorecard_date\n" +
                "FROM vw_activescorecardsourcecards vsd\n" +
                "INNER JOIN vw_ActiveScoreCardsRanked vasr ON vasr.scorecard_id = vsd.source_scorecard_id\n" +
                "WHERE vsd.scorecard_id IN (:id) AND scorecard_ordinal IN (0,1,2,3,4)\n" +
                "ORDER BY rig_name";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", id);
        return utils.executeNamedQuery(query, namedParameters);

    }

    public Collection getTopScoreCard(String date) throws ParseException{

        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date scrdDate = formatter.parse(date);
        Timestamp scrdTime = new Timestamp(scrdDate.getTime());

        String query = "SELECT scorecard_id\n" +
                "        ,owner_name AS superintendent\n" +
                "FROM vw_activesuperscorecardsranked\n" +
                "WHERE owner_type_id = 1 \n" +
                "AND scorecard_date = :date\n" +
                "ORDER BY patterson_rank ASC\n" +
                "LIMIT 1";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("date", scrdTime);
        return utils.executeNamedQuery(query, namedParameters);
    }


}
