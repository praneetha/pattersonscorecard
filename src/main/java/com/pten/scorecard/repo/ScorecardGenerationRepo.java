package com.pten.scorecard.repo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Properties;

@Component
public class ScorecardGenerationRepo {

    private static final Logger log = LoggerFactory.getLogger(ScorecardMaintenanceRepo.class);

    public ArrayList<String> getScorecardDate() {

        Connection connection = null;
        ArrayList<String> res = new ArrayList<>();
        Properties properties = new Properties();
        String configFileName = "application.properties";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                res = getDistinctDates(connection);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in getScorecardDate: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in getScorecardDate: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getScorecardDate: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in getScorecardDate: " + e.getMessage(),e);
            }
        }
        return res;
    }

    private Connection connectToPostgres(Properties properties) {

        Connection connection = null;

        try {
            String username = properties.getProperty("ptenInternal.datasource.username");
            String password = properties.getProperty("ptenInternal.datasource.password");
            String connectionURL = properties.getProperty("ptenInternal.datasource.scorecard.jdbc-url");

            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(connectionURL, username, password);

        } catch (Exception e) {
            log.error("Exception in connectToPostgres: " + e.getMessage(),e);
        }
        return connection;
    }

    private ArrayList<String> getDistinctDates(Connection connection) {

        ArrayList<String> scorecardDates = new ArrayList<>();

        try (Statement statement = connection.createStatement()) {

            String query = "select distinct date(effective_date) as scorecardDate " +
                    "from fileupload order by date(effective_date) desc";

            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                scorecardDates.add(result.getDate("scorecarddate").toString());
            }
        } catch (SQLException e) {
            log.error("SQLException in getDistinctDates: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getDistinctDates: " + e.getMessage(),e);
        }
        return scorecardDates;
    }

    public ResponseEntity<Object> getScorecardRevision(String scDate) {

        Connection connection = null;
        JSONArray res = new JSONArray();
        Properties properties = new Properties();
        String configFileName = "application.properties";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                res = getRevisionDates(connection, scDate);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in getScorecardRevision: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in getScorecardRevision: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getScorecardRevision: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in getScorecardRevision: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    private JSONArray getRevisionDates(Connection connection, String scDate) {

        JSONArray scorecardRevisions = new JSONArray();
        try (Statement statement = connection.createStatement()) {

            String query = "select execution_date,active_flag,live_flag,in_progress_flag,viewing_flag " +
                    "from scorecardrunstatus where scorecard_date = '" + scDate + "' order by execution_date desc";

            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                JSONObject jsonObject = new JSONObject();
                //SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String revision = result.getString("execution_date");
                if(revision == null || revision.isEmpty()) {
                    jsonObject.put("revision", "");
                }
                else {
                    /*Date rev_date = formatter.parse(revision);
                    String revision_date = formatter.format(rev_date);*/
                    jsonObject.put("revision", revision);
                }
                jsonObject.put("active_flag", result.getString("active_flag"));
                jsonObject.put("live_flag", result.getString("live_flag"));
                jsonObject.put("in_progress_flag", result.getString("in_progress_flag"));
                jsonObject.put("viewing_flag", result.getString("viewing_flag"));
                scorecardRevisions.put(jsonObject);
            }
        } catch (SQLException e) {
            log.error("SQLException in getRevisionDates: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getRevisionDates: " + e.getMessage(),e);
        }
        return scorecardRevisions;
    }

    public ResponseEntity<Object> getRigPersonMetaData() {

        Connection connection = null;
        JSONArray jsonArray = new JSONArray();
        Properties properties = new Properties();
        String configFileName = "application.properties";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                JSONObject rigJSON = getRigMetaData(properties);
                JSONObject personJSON = getPersonMetaData(properties);
                jsonArray.put(rigJSON);
                jsonArray.put(personJSON);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in getRigPersonMetaData: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in getRigPersonMetaData: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getRigPersonMetaData: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in getRigPersonMetaData: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(jsonArray, HttpStatus.OK);
    }

    private JSONObject getRigMetaData(Properties properties) {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("tableName", "Rig");
            jsonObject.put("is_downloadable", true);
            String rigColumns = properties.getProperty("rig.column.list");
            String[] columns = rigColumns.split(",");
            jsonObject.put("tableColumns", columns);
        } catch (JSONException e) {
            log.error("JSONException in getRigMetaData: " + e.getMessage(),e);
        }
        return jsonObject;
    }

    private JSONObject getPersonMetaData(Properties properties) {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("tableName", "Person");
            jsonObject.put("is_downloadable", true);
            String personColumns = properties.getProperty("person.column.list");
            String[] columns = personColumns.split(",");
            jsonObject.put("tableColumns", columns);
        } catch (JSONException e) {
            log.error("JSONException in getPersonMetaData: " + e.getMessage(),e);
        }
        return jsonObject;
    }

    public ResponseEntity<Object> getRigPersonData(String scDate, String type, String scRevision) {

        Connection connection = null;
        JSONArray jsonArray = new JSONArray();
        Properties properties = new Properties();
        String configFileName = "application.properties";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                if (type.equalsIgnoreCase("rig")) {
                    jsonArray = getRigData(connection, properties, scDate, scRevision);
                } else {
                    jsonArray = getPersonData(connection, properties, scDate, scRevision);
                }
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in getRigPersonData: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in getRigPersonData: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getRigPersonData: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in getRigPersonData: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(jsonArray, HttpStatus.OK);
    }

    private JSONArray getRigData(Connection connection, Properties properties, String scDate, String scRevision) {

        JSONArray jsonArray = new JSONArray();

        try (Statement statement = connection.createStatement()) {

            int viewingFlag = updateViewingFlag(connection, scDate);
            System.out.println("Updated viewing flag to false for existing run ID - Flag : " + viewingFlag);

            String viewingFlagQuery = "UPDATE scorecardrunstatus SET viewing_flag = true WHERE scorecard_date = '" +
                                        scDate + "' AND execution_date = '" + scRevision + "'";
            statement.executeUpdate(viewingFlagQuery);

            int refreshFlag = refreshMatViews(connection, properties.getProperty("matviews.viewing.list"));
            System.out.println("Refreshed mat views - Flag : " + refreshFlag);

            String rigColumns = properties.getProperty("rig.column.list");
            String[] columns = rigColumns.split(",");

            String query = "WITH RigList AS ( " +
                    "SELECT rig_name, sc.scorecard_id, sc.overall_score " +
                    "FROM vw_ActiveScoreCardsRanked_viewing sc " +
                    "WHERE sc.scorecard_date = '" + scDate + "'" +
                    ") " +
                    "SELECT rl.rig_name " +
                    ", TRUNC(MAX(lines1.raw_value),2) AS \"High Potential Events Value\" " +
                    ", TRUNC(MAX(lines1.raw_points),2) AS \"High Potential Events Points\" " +
                    ", TRUNC(MAX(lines2.raw_value),2) AS \"IMS Incidents Reported Late Value\" " +
                    ", TRUNC(MAX(lines2.raw_points),2) AS \"IMS Incidents Reported Late Points\" " +
                    ", TRUNC(MAX(lines3.raw_value),2) AS \"IMS Investigations Completed Late Value\" " +
                    ", TRUNC(MAX(lines3.raw_points),2) AS \"IMS Investigations Completed Late Points\" " +
                    ", TRUNC(MAX(lines4.raw_value),2) AS \"CRI Value\" " +
                    ", TRUNC(MAX(lines4.raw_points),2) AS \"CRI Points\" " +
                    ", TRUNC(MAX(lines5.raw_value),2) AS \"Tripping Speed - Cased Hole Value\" " +
                    ", TRUNC(MAX(lines5.raw_points),2) AS \"Tripping Speed - Cased Hole Points\" " +
                    ", TRUNC(MAX(lines6.raw_value),2) AS \"Tripping Connection Value\" " +
                    ", TRUNC(MAX(lines6.raw_points),2) AS \"Tripping Connection Points\" " +
                    ", TRUNC(MAX(lines7.raw_value),2) AS \"Drilling Connection Value\" " +
                    ", TRUNC(MAX(lines7.raw_points),2) AS \"Drilling Connection Points\" " +
                    ", TRUNC(MAX(lines8.raw_value),2) AS \"Rig Skid/Walk Value\" " +
                    ", TRUNC(MAX(lines8.raw_points),2) AS \"Rig Skid/Walk Points\" " +
                    ", TRUNC(MAX(lines9.raw_value),2) AS \"RMCT vs. Target Value\" " +
                    ", TRUNC(MAX(lines9.raw_points),2) AS \"RMCT vs Target Points\" " +
                    ", TRUNC(MAX(lines10.raw_value),2) AS \"NPT % Value\" " +
                    ", TRUNC(MAX(lines10.raw_points),2) AS \"NPT % Points\" " +
                    ", TRUNC(MAX(lines11.raw_value),2) AS \"Open Ops Level Work Orders Values\" " +
                    ", TRUNC(MAX(lines11.raw_points),2) AS \"Open Ops Level Work Orders Points\" " +
                    ", TRUNC(MAX(lines12.raw_value),2) AS \"Open Tech Level Work Orders Values\" " +
                    ", TRUNC(MAX(lines12.raw_points),2) AS \"Open Tech Level Work Orders Points\" " +
                    ", TRUNC(MAX(lines13.raw_value),2) as \"Open TIBTSA Work Orders Value\" " +
                    ", TRUNC(MAX(lines13.raw_points),2) as \"Open TIBTSA Work Orders Points\" " +
                    ", TRUNC(MAX(lines14.raw_value),2) AS \"Asset Inventory % Value\" " +
                    ", TRUNC(MAX(lines14.raw_points),2) AS \"Asset Inventory % Points\" " +
                    ", TRUNC(MAX(lines15.raw_value),2) AS \"Voluntary Turnover Value\" " +
                    ", TRUNC(MAX(lines15.raw_points),2) AS \"Voluntary Turnover Points\" " +
                    ", TRUNC(MAX(lines17.raw_value),2) as \"Capital Spend vs. Target Value\" " +
                    ", TRUNC(MAX(lines17.raw_points),2) as \"Capital Spend vw Target Points\" " +
                    ", TRUNC(MAX(lines18.raw_value),2) as \"R&M Value\" " +
                    ", TRUNC(MAX(lines18.raw_points),2) as \"R&M Points\" " +
                    ", TRUNC(MAX(lines20.raw_value),2) as \"CMR Errors Value\" " +
                    ", TRUNC(MAX(lines20.raw_points),2) as \"CMR Errors Points\" " +
                    ", TRUNC(MAX(linesO.raw_points),2) as \"overall_score\" " +
                    ", rl.overall_score AS \"score_avg_months\" " +
                    "FROM RigList rl " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines1 ON rl.scorecard_id = lines1.scorecard_id AND lines1.metric = 'High Potential Events' " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines2 ON rl.scorecard_id = lines2.scorecard_id AND lines2.metric = 'IMS Incidents Reported Late' " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines3 ON rl.scorecard_id = lines3.scorecard_id AND lines3.metric = 'IMS Investigations Completed Late' " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines4 ON rl.scorecard_id = lines4.scorecard_id AND lines4.metric = 'CRI' " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines5 ON rl.scorecard_id = lines5.scorecard_id AND lines5.metric = 'Tripping Speed - Cased Hole (ft/hr)' " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines6 ON rl.scorecard_id = lines6.scorecard_id AND lines6.metric = 'Tripping Connection Time (mins)' " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines7 ON rl.scorecard_id = lines7.scorecard_id AND lines7.metric = 'Drilling Connection Time - S2S (mins)' " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines8 ON rl.scorecard_id = lines8.scorecard_id AND lines8.metric = 'Rig Skid/Walk (hrs)' " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines9 ON rl.scorecard_id = lines9.scorecard_id AND lines9.metric = 'RMCT vs. Target (days)' " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines10 ON rl.scorecard_id = lines10.scorecard_id AND lines10.metric = 'NPT %' " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines11 ON rl.scorecard_id = lines11.scorecard_id AND lines11.metric = 'Open Ops Level Work Orders' " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines12 ON rl.scorecard_id = lines12.scorecard_id AND lines12.metric = 'Open Tech Level Work Orders' " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines13 ON rl.scorecard_id = lines13.scorecard_id AND lines13.metric = 'Open TIB and TSA Work Orders' " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines14 ON rl.scorecard_id = lines14.scorecard_id AND lines14.metric = 'Asset Inventory %' " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines15 ON rl.scorecard_id = lines15.scorecard_id AND lines15.metric = 'Voluntary Turnover' " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines17 ON rl.scorecard_id = lines17.scorecard_id AND lines17.metric = 'Capital Spend vs. Target (Rolling 12-Months)' " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines18 ON rl.scorecard_id = lines18.scorecard_id AND lines18.metric = 'R&M and Supplies Spend vs. Target' " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines20 ON rl.scorecard_id = lines20.scorecard_id AND lines20.metric = 'CMRs with Error Days' " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing linesO ON rl.scorecard_id = linesO.scorecard_id AND linesO.display_order = 999 " +
                    "GROUP BY rl.rig_name, rl.overall_score " +
                    "ORDER BY rig_name";

            ResultSet result = statement.executeQuery(query);
            ResultSetMetaData rsmd = result.getMetaData();
            int columnsNumber = rsmd.getColumnCount();
            while (result.next()) {
                JSONObject jsonObject = new JSONObject();
                for (int i = 1; i <= columnsNumber; i++) {
                    if (result.getString(i) == null || result.getString(i).isEmpty() || result.getString(i).equals("")) {
                        String value = "";
                        jsonObject.put(columns[i - 1], value);
                    } else {
                        jsonObject.put(columns[i - 1], result.getString(i));
                    }
                }
                jsonArray.put(jsonObject);
            }
        } catch (SQLException e) {
            log.error("SQLException in getRigData: " + e.getMessage(),e);
        } catch (JSONException e) {
            log.error("JSONException in getRigData: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getRigData: " + e.getMessage(),e);
        }
        return jsonArray;
    }

    private JSONArray getPersonData(Connection connection, Properties properties, String scDate, String scRevision) {

        JSONArray jsonArray = new JSONArray();

        try (Statement statement = connection.createStatement()) {

            int viewingFlag = updateViewingFlag(connection, scDate);
            System.out.println("Updated viewing flag to false for existing run ID - Flag : " + viewingFlag);

            String viewingFlagQuery = "UPDATE scorecardrunstatus SET viewing_flag = true WHERE scorecard_date = '" +
                    scDate + "' AND execution_date = '" + scRevision + "'";
            statement.executeUpdate(viewingFlagQuery);

            int refreshFlag = refreshMatViews(connection, properties.getProperty("matviews.viewing.list"));
            System.out.println("Refreshed mat views - Flag : " + refreshFlag);

            String personColumns = properties.getProperty("person.column.list");
            String[] columns = personColumns.split(",");

            String query = "WITH RigList AS ( " +
                    "SELECT owner_name, sc.scorecard_id, owner_type_id, overall_score " +
                    "FROM vw_ActiveSuperScoreCardsRanked_viewing sc " +
                    "WHERE sc.scorecard_date = '" + scDate + "'" +
                    ") " +
                    "SELECT rl.owner_name AS person_name " +
                    ", CASE owner_type_id WHEN 1 THEN 'Superintendent' WHEN 2 THEN 'Operations Manager' END AS person_type " +
                    ", TRUNC(MAX(CASE WHEN display_order = 1 THEN lines.raw_value ELSE NULL END),2) AS \"High Potential Events Value\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 1 THEN lines.raw_points ELSE NULL END),2) AS \"High Potential Events Points\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 2 THEN lines.raw_value ELSE NULL END),2) AS \"IMS Incidents Reported Late Value\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 2 THEN lines.raw_points ELSE NULL END),2) AS \"IMS Incidents Reported Late Points\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 3 THEN lines.raw_value ELSE NULL END),2) AS \"IMS Investigations Completed Late Value\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 3 THEN lines.raw_points ELSE NULL END),2) AS \"IMS Investigations Completed Late Points\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 4 THEN lines.raw_value ELSE NULL END),2) AS \"CRI Value\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 4 THEN lines.raw_points ELSE NULL END),2) AS \"CRI Points\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 5 THEN lines.raw_value ELSE NULL END),2) AS \"Tripping Speed - Cased Hole Value\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 5 THEN lines.raw_points ELSE NULL END),2) AS \"Tripping Speed - Cased Hole Points\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 6 THEN lines.raw_value ELSE NULL END),2) AS \"Tripping Connection Value\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 6 THEN lines.raw_points ELSE NULL END),2) AS \"Tripping Connection Points\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 7 THEN lines.raw_value ELSE NULL END),2) AS \"Drilling Connection Value\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 7 THEN lines.raw_points ELSE NULL END),2) AS \"Drilling Connection Points\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 8 THEN lines.raw_value ELSE NULL END),2) AS \"Rig Skid/Walk Value\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 8 THEN lines.raw_points ELSE NULL END),2) AS \"Rig Skid/Walk Points\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 9 THEN lines.raw_value ELSE NULL END),2) AS \"RMCT vs. Target Value\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 9 THEN lines.raw_points ELSE NULL END),2) AS \"RMCT vs Target Points\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 10 THEN lines.raw_value ELSE NULL END),2) AS \"NPT % Value\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 10 THEN lines.raw_points ELSE NULL END),2) AS \"NPT % Points\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 11 THEN lines.raw_value ELSE NULL END),2) AS \"Open Ops Level Work Orders Values\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 11 THEN lines.raw_points ELSE NULL END),2) AS \"Open Ops Level Work Orders Points\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 12 THEN lines.raw_value ELSE NULL END),2) AS \"Open Tech Level Work Orders Values\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 12 THEN lines.raw_points ELSE NULL END),2) AS \"Open Tech Level Work Orders Points\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 13 THEN lines.raw_value ELSE NULL END),2) as \"Open TIBTSA Work Orders Value\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 13 THEN lines.raw_points ELSE NULL END),2) as \"Open TIBTSA Work Orders Points\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 14 THEN lines.raw_value ELSE NULL END),2) AS \"Asset Inventory % Value\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 14 THEN lines.raw_points ELSE NULL END),2) AS \"Asset Inventory % Points\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 15 THEN lines.raw_value ELSE NULL END),2) AS \"Voluntary Turnover Value\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 15 THEN lines.raw_points ELSE NULL END),2) AS \"Voluntary Turnover Points\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 17 THEN lines.raw_value ELSE NULL END),2) as \"Capital Spend vs. Target Value\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 17 THEN lines.raw_points ELSE NULL END),2) as \"Capital Spend vw Target Points\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 18 THEN lines.raw_value ELSE NULL END),2) as \"R&M Value\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 18 THEN lines.raw_points ELSE NULL END),2) as \"R&M Points\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 20 THEN lines.raw_value ELSE NULL END),2) as \"CMR Errors Value\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 20 THEN lines.raw_points ELSE NULL END),2) as \"CMR Errors Points\" " +
                    ", TRUNC(MAX(CASE WHEN display_order = 99999 THEN lines.raw_value ELSE NULL END),2) AS \"overall_score\" " +
                    ", rl.overall_score AS \"score_avg_months\" " +
                    "FROM RigList rl " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines1 ON rl.scorecard_id = lines1.scorecard_id AND lines1.display_order = 1 " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines2 ON rl.scorecard_id = lines2.scorecard_id AND lines2.display_order = 2 " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines3 ON rl.scorecard_id = lines3.scorecard_id AND lines3.display_order = 3 " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines4 ON rl.scorecard_id = lines4.scorecard_id AND lines4.display_order = 4 " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines5 ON rl.scorecard_id = lines5.scorecard_id AND lines5.display_order = 5 " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines6 ON rl.scorecard_id = lines6.scorecard_id AND lines6.display_order = 6 " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines7 ON rl.scorecard_id = lines7.scorecard_id AND lines7.display_order = 7 " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines8 ON rl.scorecard_id = lines8.scorecard_id AND lines8.display_order = 8 " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines9 ON rl.scorecard_id = lines9.scorecard_id AND lines9.display_order = 9 " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines10 ON rl.scorecard_id = lines10.scorecard_id AND lines10.display_order = 10 " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines11 ON rl.scorecard_id = lines11.scorecard_id AND lines11.display_order = 11 " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines12 ON rl.scorecard_id = lines12.scorecard_id AND lines12.display_order = 12 " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines13 ON rl.scorecard_id = lines13.scorecard_id AND lines13.display_order = 13 " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines14 ON rl.scorecard_id = lines14.scorecard_id AND lines14.display_order = 14 " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines15 ON rl.scorecard_id = lines15.scorecard_id AND lines15.display_order = 15 " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines17 ON rl.scorecard_id = lines17.scorecard_id AND lines17.display_order = 17 " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines18 ON rl.scorecard_id = lines18.scorecard_id AND lines18.display_order = 18 " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing lines20 ON rl.scorecard_id = lines20.scorecard_id AND lines20.display_order = 20 " +
                    "LEFT OUTER JOIN vw_ActiveScoreCardLineItems_viewing linesO ON rl.scorecard_id = linesO.scorecard_id AND linesO.display_order = 99999 " +
                    "GROUP BY rl.owner_name, rl.owner_type_id, rl.overall_score " +
                    "ORDER BY person_type, owner_name";

            ResultSet result = statement.executeQuery(query);
            ResultSetMetaData rsmd = result.getMetaData();
            int columnsNumber = rsmd.getColumnCount();
            while (result.next()) {
                JSONObject jsonObject = new JSONObject();
                for (int i = 1; i <= columnsNumber; i++) {
                    if (result.getString(i) == null || result.getString(i).isEmpty() || result.getString(i).equals("")) {
                        String value = "";
                        jsonObject.put(columns[i - 1], value);
                    } else {
                        jsonObject.put(columns[i - 1], result.getString(i));
                    }
                }
                jsonArray.put(jsonObject);
            }
        } catch (SQLException e) {
            log.error("SQLException in getPersonData: " + e.getMessage(),e);
        } catch (JSONException e) {
            log.error("JSONException in getPersonData: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getPersonData: " + e.getMessage(),e);
        }
        return jsonArray;
    }

    public ResponseEntity<Object> getGenerateMeta() {

        JSONArray jsonArray = new JSONArray();
        ArrayList<String> tables = new ArrayList<>();

        try {
            tables.add("Rig");
            tables.add("Person");
            for (String table : tables) {
                JSONObject jsonObject = new JSONObject();
                if (table.equalsIgnoreCase("rig")) {
                    jsonObject.put("tableName", table);
                    ArrayList<String> columns = new ArrayList<>();
                    columns.add("Rig");
                    jsonObject.put("tableColumns", columns);
                } else {
                    jsonObject.put("tableName", table);
                    ArrayList<String> columns = new ArrayList<>();
                    columns.add("Person");
                    columns.add("Person Type");
                    jsonObject.put("tableColumns", columns);
                }
                jsonArray.put(jsonObject);
            }
        } catch (JSONException e) {
            log.error("JSONException in getGenerateMeta: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getGenerateMeta: " + e.getMessage(),e);
        }
        return new ResponseEntity<>(jsonArray, HttpStatus.OK);
    }

    public ResponseEntity<Object> getGenerateData(String scDate, String type) {

        Connection connection = null;
        JSONArray jsonArray = new JSONArray();
        Properties properties = new Properties();
        String configFileName = "application.properties";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                if (type.equalsIgnoreCase("rig")) {
                    jsonArray = getRigList(connection, scDate);
                } else {
                    jsonArray = getPersonList(connection, scDate);
                }
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in getGenerateData: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in getGenerateData: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getGenerateData: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in getGenerateData: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(jsonArray, HttpStatus.OK);
    }

    private JSONArray getRigList(Connection connection, String scDate) {

        JSONArray jsonArray = new JSONArray();

        try (Statement statement = connection.createStatement()) {

            String[] columns = {"ID", "Rig"};
            String query = "SELECT distinct r.rig_id,c.rig from cri c join rig r on c.rig = r.rig_name " +
                    "where c.file_upload_id = (select file_upload_id from fileupload where file_type_id = 3 " +
                    "and active = 't' and effective_date = '" + scDate + "') order by 2";

            ResultSet result = statement.executeQuery(query);
            ResultSetMetaData rsmd = result.getMetaData();
            int columnsNumber = rsmd.getColumnCount();
            while (result.next()) {
                JSONObject jsonObject = new JSONObject();
                for (int i = 1; i <= columnsNumber; i++) {
                    jsonObject.put(columns[i - 1], result.getString(i));
                }
                jsonArray.put(jsonObject);
            }
        } catch (SQLException e) {
            log.error("SQLException in getRigList: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getRigList: " + e.getMessage(),e);
        }
        return jsonArray;
    }

    private JSONArray getPersonList(Connection connection, String scDate) {

        JSONArray jsonArray = new JSONArray();

        try (Statement statement = connection.createStatement()) {

            String[] columns = {"ID", "Person", "Person Type"};
            String query = "SELECT oto.owner_id, o.first_name || ' ' || o.last_name AS person, ot.owner_type_name AS person_type " +
                    "FROM OwnerToOwner oto INNER JOIN Owner o ON oto.owner_id = o.owner_id " +
                    "INNER JOIN OwnerType ot ON o.owner_type_id = ot.owner_type_id " +
                    "WHERE '" + scDate + "' BETWEEN oto.start_date AND COALESCE(oto.end_date, '" + scDate + "') " +
                    "UNION SELECT oto.owned_owner_id AS owner_id, o.first_name || ' ' || o.last_name AS person, " +
                    "ot.owner_type_name AS person_type FROM OwnerToOwner oto " +
                    "INNER JOIN Owner o ON oto.owned_owner_id = o.owner_id " +
                    "INNER JOIN OwnerType ot ON o.owner_type_id = ot.owner_type_id " +
                    "WHERE '" + scDate + "' BETWEEN oto.start_date AND COALESCE(oto.end_date, '" + scDate + "') " +
                    "ORDER BY 1";

            ResultSet result = statement.executeQuery(query);
            ResultSetMetaData rsmd = result.getMetaData();
            int columnsNumber = rsmd.getColumnCount();
            while (result.next()) {
                JSONObject jsonObject = new JSONObject();
                for (int i = 1; i <= columnsNumber; i++) {
                    jsonObject.put(columns[i - 1], result.getString(i));
                }
                jsonArray.put(jsonObject);
            }
        } catch (SQLException e) {
            log.error("SQLException in getPersonList: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getPersonList: " + e.getMessage(),e);
        }
        return jsonArray;
    }

    public int generateScorecard(String scDate, String data) {

        int runID;
        int scorecardID = 0;
        Connection connection = null;
        Properties properties = new Properties();
        String configFileName = "application.properties";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                runID = checkForExistingRunID(connection, scDate);
                System.out.println("Scorecard Run ID: " + runID);
                if (runID == 0) {
                    System.out.println("Inserting into scorecardrunstatus table");
                    int insertFlag = entryToScorecardRunStatusTable(connection, scDate);
                    System.out.println("Row inserted into scorecardrunstatus table");
                    if (insertFlag == 1) {
                        scorecardID = getScorecardRunID(connection, scDate);
                        System.out.println("New scorecard run ID: " + scorecardID);
                        int rigEntryFlag = entryForEachRigIntoScorecard(connection, scDate, data, scorecardID);
                        System.out.println("Inserted rigs into scorecard table - Flag : " + rigEntryFlag);
                        int ownerEntryFlag = entryForEachOwnerIntoScorecard(connection, scDate, data, scorecardID);
                        System.out.println("Inserted people into scorecard table - Flag : " + ownerEntryFlag);
                        if (rigEntryFlag != 0 && ownerEntryFlag != 0) {
                            ArrayList<Integer> fileUploadIDList = getAllActiveFileUploadIDS(connection, scDate);
                            int sourceFilesEntryFlag = entryIntoScorecardSourceFilesRunVersion(connection, scorecardID, fileUploadIDList);
                            System.out.println("Inserted into scorecardsourcefiles_runversion table - Flag : " + sourceFilesEntryFlag);
                            int sourcecardEntryFlag11 = entryIntoScorecardSourceCardRunVersionForType1(connection, scDate, scorecardID);
                            System.out.println("Inserted into scorecardsourcecards_runversion table for type 1 - Flag : " + sourcecardEntryFlag11);
                            int sourcecardEntryFlag12 = entryIntoScorecardSourceCardRunVersionForType2(connection, scDate, scorecardID);
                            System.out.println("Inserted into scorecardsourcecards_runversion table for type 2 - Flag : " + sourcecardEntryFlag12);
                            if (sourceFilesEntryFlag != 0 && sourcecardEntryFlag11 != 0 && sourcecardEntryFlag12 != 0) {
                                int refreshFlag = refreshMatViews(connection, properties.getProperty("matviews.viewing.list"));
                                System.out.println("Refreshed mat views - Flag : " + refreshFlag);
                            }
                        }
                    }
                } else {
                    int viewingFlag = updateViewingFlag(connection, scDate);
                    System.out.println("Updated viewing flag to false for existing run ID - Flag : " + viewingFlag);
                    if (viewingFlag == 1) {
                        int inProgressFlag = updateInProgressFlag(connection, runID);
                        System.out.println("Updated in progress flag to false for existing run ID - Flag : " + inProgressFlag);
                        if (inProgressFlag != 0) {
                            System.out.println("Deleting from tables");
                            int deleteSourceFilesFlag = deleteFromScorecardSourceFilesRunVersion(connection, runID);
                            System.out.println("Deleted from scorecardsourcefiles_runversion - Flag : " + deleteSourceFilesFlag);
                            int deleteSourceCardsFlag = deleteFromScorecardSourceCardsRunVersion(connection, runID);
                            System.out.println("Deleted from scorecardsourcecards_runversion - Flag : " + deleteSourceCardsFlag);
                            int deleteFlag = deleteFromScorecard(connection, runID);
                            System.out.println("Deleted from scorecard - Flag : " + deleteFlag);
                            if (deleteFlag != 0 && deleteSourceCardsFlag != 0 && deleteSourceFilesFlag != 0) {
                                int insertFlag = entryToScorecardRunStatusTable(connection, scDate);
                                System.out.println("New entry into scorecardrunstatus table - Flag : " + insertFlag);
                                if (insertFlag == 1) {
                                    scorecardID = getScorecardRunID(connection, scDate);
                                    System.out.println("New scorecard run ID: " + scorecardID);
                                    int rigEntryFlag = entryForEachRigIntoScorecard(connection, scDate, data, scorecardID);
                                    System.out.println("Inserted rigs into scorecard table - Flag : " + rigEntryFlag);
                                    int ownerEntryFlag = entryForEachOwnerIntoScorecard(connection, scDate, data, scorecardID);
                                    System.out.println("Inserted people into scorecard table - Flag : " + ownerEntryFlag);
                                    if (rigEntryFlag != 0 && ownerEntryFlag != 0) {
                                        ArrayList<Integer> fileUploadIDList = getAllActiveFileUploadIDS(connection, scDate);
                                        int sourceFilesEntryFlag = entryIntoScorecardSourceFilesRunVersion(connection, scorecardID, fileUploadIDList);
                                        System.out.println("Inserted into scorecardsourcefiles_runversion table - Flag : " + sourceFilesEntryFlag);
                                        int sourcecardEntryFlag11 = entryIntoScorecardSourceCardRunVersionForType1(connection, scDate, scorecardID);
                                        System.out.println("Inserted into scorecardsourcecards_runversion table for type 1 - Flag : " + sourcecardEntryFlag11);
                                        int sourcecardEntryFlag12 = entryIntoScorecardSourceCardRunVersionForType2(connection, scDate, scorecardID);
                                        System.out.println("Inserted into scorecardsourcecards_runversion table for type 2 - Flag : " + sourcecardEntryFlag12);
                                        if (sourceFilesEntryFlag != 0 && sourcecardEntryFlag11 != 0 && sourcecardEntryFlag12 != 0) {
                                            int refreshFlag = refreshMatViews(connection, properties.getProperty("matviews.viewing.list"));
                                            System.out.println("Refreshed mat views - Flag : " + refreshFlag);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in generateScorecard: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in generateScorecard: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in generateScorecard: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in generateScorecard: " + e.getMessage(),e);
            }
        }
        return scorecardID;
    }

    private int checkForExistingRunID(Connection connection, String scDate) {

        int runID = 0;

        try (Statement statement = connection.createStatement()) {

            String query = "SELECT scorecard_run_id FROM scorecardRunStatus WHERE scorecard_date = '" + scDate + "'" +
                    " AND in_progress_flag = true";

            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                runID = result.getInt("scorecard_run_id");
            }
        } catch (SQLException e) {
            log.error("SQLException in checkForExistingRunID: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in checkForExistingRunID: " + e.getMessage(),e);
        }
        return runID;
    }

    private int updateViewingFlag(Connection connection, String scDate) {

        int count = 0;
        int result2;
        int result = 0;

        try (Statement statement = connection.createStatement()) {

            String query1 = "SELECT count(*) from scorecardRunStatus WHERE scorecard_date = '" + scDate + "'";
            ResultSet result1 = statement.executeQuery(query1);
            while (result1.next()) {
                count = result1.getInt("count");
            }

            String query2 = "update scorecardRunStatus " +
                    "SET viewing_flag = CASE WHEN viewing_flag = true THEN false ELSE viewing_flag END " +
                    "WHERE scorecard_date = '" + scDate + "'";
            result2 = statement.executeUpdate(query2);

            if (result2 == count) {
                result = 1;
            }
        } catch (SQLException e) {
            log.error("SQLException in updateViewingFlag: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in updateViewingFlag: " + e.getMessage(),e);
        }
        return result;
    }

    private int updateInProgressFlag(Connection connection, int runID) {

        int result = 0;

        try (Statement statement = connection.createStatement()) {

            String query = "UPDATE scorecardRunStatus set in_progress_flag = false WHERE scorecard_run_id = " +
                    runID;

            result = statement.executeUpdate(query);

        } catch (SQLException e) {
            log.error("SQLException in updateInProgressFlag: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in updateInProgressFlag: " + e.getMessage(),e);
        }
        return result;
    }

    private int entryToScorecardRunStatusTable(Connection connection, String scDate) {

        int flag = 0;

        try (Statement statement = connection.createStatement()) {

            String query = "INSERT INTO scorecardRunStatus (scorecard_date,live_flag,active_flag,execution_date," +
                    "viewing_flag,in_progress_flag) VALUES ('" + scDate + "',false,false,now(),true,true)";

            int result = statement.executeUpdate(query);
            if (result != 0) {
                flag = 1;
            }
        } catch (SQLException e) {
            log.error("SQLException in entryToScorecardRunStatusTable: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in entryToScorecardRunStatusTable: " + e.getMessage(),e);
        }
        return flag;
    }

    private int getScorecardRunID(Connection connection, String scDate) {

        int scorecardID = 0;

        try (Statement statement = connection.createStatement()) {

            String query = "SELECT scorecard_run_id FROM scorecardRunStatus WHERE scorecard_date = '" + scDate + "' AND " +
                    "in_progress_flag = true";

            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                scorecardID = result.getInt("scorecard_run_id");
            }
        } catch (SQLException e) {
            log.error("SQLException in getScorecardRunID: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getScorecardRunID: " + e.getMessage(),e);
        }
        return scorecardID;
    }

    private int entryForEachRigIntoScorecard(Connection connection, String scDate, String data, int scorecardID) {

        //{"Rig":["14","16","19"],"Person":["4","8","9"]}
        int result;
        int flag = 0;
        int finalResult = 0;

        try (Statement statement = connection.createStatement()) {

            JSONObject jsonObject = new JSONObject(data);
            JSONArray jsonArray = jsonObject.getJSONArray("Rig");
            for (int i = 0; i < jsonArray.length(); i++) {
                int rigID = jsonArray.getInt(i);
                String query = "INSERT INTO scorecard (rig_id,scorecard_date,generated_date,scorecard_type_id,scorecard_run_id) " +
                        "VALUES(" + rigID + ",'" + scDate + "',now(),1," + scorecardID + ")";

                result = statement.executeUpdate(query);
                if (result != 0) {
                    flag = flag + 1;
                }
            }

            if (flag == jsonArray.length()) {
                finalResult = 1;
            }
        } catch (SQLException e) {
            log.error("SQLException in entryForEachRigIntoScorecard: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in entryForEachRigIntoScorecard: " + e.getMessage(),e);
        }
        return finalResult;
    }

    private int entryForEachOwnerIntoScorecard(Connection connection, String scDate, String data, int scorecardID) {

        int result;
        int flag = 0;
        int finalResult = 0;
        try (Statement statement = connection.createStatement()) {

            JSONObject jsonObject = new JSONObject(data);
            JSONArray jsonArray = jsonObject.getJSONArray("Person");
            for (int i = 0; i < jsonArray.length(); i++) {
                int ownerID = jsonArray.getInt(i);
                String query = "INSERT INTO scorecard (scorecard_date,generated_date,scorecard_type_id,owner_id,scorecard_run_id) " +
                        "VALUES('" + scDate + "',now(),2," + ownerID + "," + scorecardID + ")";

                result = statement.executeUpdate(query);
                if (result != 0) {
                    flag = flag + 1;
                }
            }

            if (flag == jsonArray.length()) {
                finalResult = 1;
            }
        } catch (SQLException e) {
            log.error("SQLException in entryForEachOwnerIntoScorecard: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in entryForEachOwnerIntoScorecard: " + e.getMessage(),e);
        }
        return finalResult;
    }

    private ArrayList<Integer> getAllActiveFileUploadIDS(Connection connection, String scDate) {

        ArrayList<Integer> fileUploadIDList = new ArrayList<>();

        try (Statement statement = connection.createStatement()) {

            String query = "SELECT file_upload_id FROM fileUpload WHERE date(effective_date) = '" + scDate + "' AND " +
                    "active = true";

            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                fileUploadIDList.add(result.getInt("file_upload_id"));
            }
        } catch (SQLException e) {
            log.error("SQLException in getAllActiveFileUploadIDS: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getAllActiveFileUploadIDS: " + e.getMessage(),e);
        }
        return fileUploadIDList;
    }

    private int deleteFromScorecard(Connection connection, int runID) {

        int result = 0;

        try (Statement statement = connection.createStatement()) {

            String query = "DELETE FROM Scorecard WHERE scorecard_run_id = " + runID;

            result = statement.executeUpdate(query);
        } catch (SQLException e) {
            log.error("SQLException in deleteFromScorecard: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in deleteFromScorecard: " + e.getMessage(),e);
        }
        return result;
    }

    private int deleteFromScorecardSourceFilesRunVersion(Connection connection, int runID) {

        int result = 0;

        try (Statement statement = connection.createStatement()) {

            String query = "DELETE FROM ScorecardSourceFiles_RunVersion WHERE scorecard_run_id = " + runID;

            result = statement.executeUpdate(query);
        } catch (SQLException e) {
            log.error("SQLException in deleteFromScorecardSourceFilesRunVersion: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in deleteFromScorecardSourceFilesRunVersion: " + e.getMessage(),e);
        }
        return result;
    }

    private int deleteFromScorecardSourceCardsRunVersion(Connection connection, int runID) {

        int result = 0;

        try (Statement statement = connection.createStatement()) {

            String query = "DELETE FROM ScorecardSourceCards_RunVersion WHERE scorecard_run_id = " + runID;

            result = statement.executeUpdate(query);
        } catch (SQLException e) {
            log.error("SQLException in deleteFromScorecardSourceCardsRunVersion: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in deleteFromScorecardSourceCardsRunVersion: " + e.getMessage(),e);
        }
        return result;
    }

    private int entryIntoScorecardSourceFilesRunVersion(Connection connection, int scorecardID,
                                                        ArrayList<Integer> fileUploadList) {

        int flag = 0;
        int finalResult = 0;

        try (Statement statement = connection.createStatement()) {

            for (int fileUploadID : fileUploadList) {
                String query = "INSERT INTO scorecardSourceFiles_RunVersion (scorecard_run_id, file_upload_id) " +
                        "VALUES(" + scorecardID + "," + fileUploadID + ")";
                int result = statement.executeUpdate(query);
                if (result != 0) {
                    flag = flag + 1;
                }
            }

            if (flag == fileUploadList.size()) {
                finalResult = 1;
            }
        } catch (SQLException e) {
            log.error("SQLException in entryIntoScorecardSourceFilesRunVersion: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in entryIntoScorecardSourceFilesRunVersion: " + e.getMessage(),e);
        }
        return finalResult;
    }

    private int entryIntoScorecardSourceCardRunVersionForType1(Connection connection, String scDate, int runID) {

        int result = 0;

        try (Statement statement = connection.createStatement()) {

            String query = "WITH cte AS (SELECT " + runID + " AS scorecard_run_id,scorecard_run_id AS scorecard_id," +
                    "scorecard_date,row_number() OVER (ORDER BY scorecard_date DESC) - 1 AS scorecard_ordinal " +
                    "FROM scorecardrunstatus WHERE scorecard_date " +
                    "BETWEEN (to_timestamp('" + scDate + "','YYYY-mm-dd') - interval '6 months') " +
                    "AND to_timestamp('" + scDate + "','YYYY-mm-dd') " +
                    "AND active_flag = true ORDER BY scorecard_date DESC) " +
                    "INSERT INTO scorecardsourcecards_runversion " +
                    "SELECT scorecard_run_id,1 AS scorecard_type_id," +
                    "CASE WHEN scorecard_ordinal = 0 THEN scorecard_run_id ELSE scorecard_id END AS source_scorecard_run_id," +
                    "1 AS source_scorecard_type_id,scorecard_ordinal FROM cte";

            result = statement.executeUpdate(query);

        } catch (SQLException e) {
            log.error("SQLException in entryIntoScorecardSourceCardRunVersionForType1: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in entryIntoScorecardSourceCardRunVersionForType1: " + e.getMessage(),e);
        }
        return result;
    }

    private int entryIntoScorecardSourceCardRunVersionForType2(Connection connection, String scDate, int runID) {

        int result = 0;

        try (Statement statement = connection.createStatement()) {

            for (int i = 1; i <= 2; i++) {
                String query = "WITH cte AS (SELECT " + runID + " AS scorecard_run_id,scorecard_run_id AS scorecard_id," +
                        "scorecard_date,row_number() OVER (ORDER BY scorecard_date DESC) - 1 AS scorecard_ordinal " +
                        "FROM scorecardrunstatus WHERE scorecard_date " +
                        "BETWEEN (to_timestamp('" + scDate + "','YYYY-mm-dd') - interval '6 months') " +
                        "AND to_timestamp('" + scDate + "','YYYY-mm-dd') " +
                        "AND active_flag = true ORDER BY scorecard_date DESC) " +
                        "INSERT INTO scorecardsourcecards_runversion " +
                        "SELECT scorecard_run_id,2 AS scorecard_type_id," +
                        "CASE WHEN scorecard_ordinal = 0 THEN scorecard_run_id ELSE scorecard_id END AS source_scorecard_run_id," +
                        i + " AS source_scorecard_type_id,scorecard_ordinal FROM cte";

                result = statement.executeUpdate(query);
            }

        } catch (SQLException e) {
            log.error("SQLException in entryIntoScorecardSourceCardRunVersionForType2: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in entryIntoScorecardSourceCardRunVersionForType2: " + e.getMessage(),e);
        }
        return result;
    }

    private int refreshMatViews(Connection connection, String matViews) {

        int result = 0;

        try (Statement statement = connection.createStatement()) {
            String[] matViewsList = matViews.split(",");

            for (String s : matViewsList) {
                String query = "REFRESH MATERIALIZED VIEW " + s;

                result = statement.executeUpdate(query);
            }

        } catch (SQLException e) {
            log.error("SQLException in refreshMatViews: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in refreshMatViews: " + e.getMessage(),e);
        }
        return result;
    }

    public int publishScorecard(String scDate) {

        int runID = 0;
        Connection connection = null;
        Properties properties = new Properties();
        String configFileName = "application.properties";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                int activeFlagChange = changeAllActiveFlagsToFalse(connection, scDate);
                if (activeFlagChange == 1) {
                    runID = checkForExistingRunID(connection, scDate);
                    int activeFlagTrue = changeActiveAndLiveFlagToTrue(connection, runID);
                    if (activeFlagTrue != 0) {
                        int inProgressFlagFalse = changeInProgressFlagToFalse(connection, runID);
                        if (inProgressFlagFalse != 0) {
                            refreshMatViews(connection, properties.getProperty("matviews.live.list"));
                        }
                    }
                }
            }

        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in publishScorecard: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in publishScorecard: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in publishScorecard: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in publishScorecard: " + e.getMessage(),e);
            }
        }
        return runID;
    }

    private int changeAllActiveFlagsToFalse(Connection connection, String scDate) {

        int count = 0;
        int result = 0;

        try (Statement statement = connection.createStatement()) {

            String query1 = "SELECT count(*) as count from scorecardrunstatus WHERE scorecard_date = '" + scDate +
                    "' AND active_flag = true";
            ResultSet result1 = statement.executeQuery(query1);
            while (result1.next()) {
                count = result1.getInt("count");
            }

            String query2 = "UPDATE scorecardrunstatus SET active_flag = false WHERE scorecard_date = '" + scDate +
                    "' AND active_flag = true";
            int result2 = statement.executeUpdate(query2);

            if (result2 == count) {
                result = 1;
            }

        } catch (SQLException e) {
            log.error("SQLException in changeAllActiveFlagsToFalse: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in changeAllActiveFlagsToFalse: " + e.getMessage(),e);
        }
        return result;
    }

    private int changeActiveAndLiveFlagToTrue(Connection connection, int runID) {

        int result = 0;

        try (Statement statement = connection.createStatement()) {

            String query = "UPDATE scorecardrunstatus set active_flag = true,live_flag = true WHERE scorecard_run_id = "
                    + runID;
            result = statement.executeUpdate(query);

        } catch (SQLException e) {
            log.error("SQLException in changeActiveFlagToTrue: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in changeActiveFlagToTrue: " + e.getMessage(),e);
        }
        return result;
    }

    private int changeInProgressFlagToFalse(Connection connection, int runID) {

        int result = 0;

        try (Statement statement = connection.createStatement()) {

            String query = "UPDATE scorecardrunstatus set in_progress_flag = false WHERE scorecard_run_id = " + runID;
            result = statement.executeUpdate(query);

        } catch (SQLException e) {
            log.error("SQLException in changeInProgressFlagToFalse: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in changeInProgressFlagToFalse: " + e.getMessage(),e);
        }
        return result;
    }

    public int unPublishScorecard(String scDate, String scRevision) {

        int runID = 0;
        Connection connection = null;
        Properties properties = new Properties();
        String configFileName = "application.properties";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                runID = getRunIDWithActiveFlagTrue(connection, scDate);
                if (runID != 0) {
                    boolean liveFlag = checkLiveFlag(connection, runID);
                    if(liveFlag) {
                        archiveScorecard(scDate,scRevision);
                        int activeFlagChange = changeActiveAndLiveFlagToFalse(connection, runID);
                        if (activeFlagChange != 0) {
                            refreshMatViews(connection, properties.getProperty("matviews.live.list"));
                        }
                    }
                    else {
                        archiveScorecard(scDate,scRevision);
                        int activeFlagChange = changeActiveFlagToFalse(connection, runID);
                        if (activeFlagChange != 0) {
                            refreshMatViews(connection, properties.getProperty("matviews.live.list"));
                        }
                    }
                }
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in unPublishScorecard: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in unPublishScorecard: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in unPublishScorecard: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in unPublishScorecard: " + e.getMessage(),e);
            }
        }
        return runID;
    }

    private boolean checkLiveFlag(Connection connection, int runID) {

        boolean live_flag = false;
        try (Statement statement = connection.createStatement()) {

            String query = "SELECT live_flag FROM scorecardrunstatus WHERE scorecard_run_id = " + runID;

            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                live_flag = resultSet.getBoolean("live_flag");
            }

        } catch (SQLException e) {
            log.error("SQLException in checkLiveFlag: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in checkLiveFlag: " + e.getMessage(),e);
        }
        return live_flag;
    }

    private int getRunIDWithActiveFlagTrue(Connection connection, String scDate) {

        int runID = 0;
        try (Statement statement = connection.createStatement()) {

            String query = "SELECT scorecard_run_id FROM scorecardrunstatus WHERE scorecard_date = '" + scDate +
                    "' AND active_flag = true";
            ResultSet result = statement.executeQuery(query);

            while (result.next()) {
                runID = result.getInt("scorecard_run_id");
            }

        } catch (SQLException e) {
            log.error("SQLException in getRunIDWithActiveFlagTrue: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getRunIDWithActiveFlagTrue: " + e.getMessage(),e);
        }
        return runID;
    }

    private int changeActiveAndLiveFlagToFalse(Connection connection, int runID) {

        int result = 0;

        try (Statement statement = connection.createStatement()) {

            String query = "UPDATE scorecardrunstatus SET active_flag = false,live_flag = false WHERE scorecard_run_id = " + runID;
            result = statement.executeUpdate(query);

        } catch (SQLException e) {
            log.error("SQLException in changeActiveFlagToFalse: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in changeActiveFlagToFalse: " + e.getMessage(),e);
        }
        return result;
    }

    private int changeActiveFlagToFalse(Connection connection, int runID) {

        int result = 0;

        try (Statement statement = connection.createStatement()) {

            String query = "UPDATE scorecardrunstatus SET active_flag = false WHERE scorecard_run_id = " + runID;
            result = statement.executeUpdate(query);

        } catch (SQLException e) {
            log.error("SQLException in changeActiveFlagToFalse: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in changeActiveFlagToFalse: " + e.getMessage(),e);
        }
        return result;
    }

    public int archiveScorecard(String scDate, String scRevision) {

        int liveFlag = 0;
        Connection connection = null;
        Properties properties = new Properties();
        String configFileName = "application.properties";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                int runID = getCurrentRunID(connection,scDate,scRevision);
                String scorecardIDs = getScorecardIDList(connection,runID);
                int result = copyDataFromViewsToTables(connection,scorecardIDs);
                if(result == 1) {
                    liveFlag = changeLiveFlagToFalse(connection, scDate, scRevision);
                }
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in archiveScorecard: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in archiveScorecard: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in archiveScorecard: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in archiveScorecard: " + e.getMessage(),e);
            }
        }
        return liveFlag;
    }

    private int getCurrentRunID(Connection connection, String scDate, String scRevision) {

        int runID = 0;

        try (Statement statement = connection.createStatement()) {

            String query = "SELECT scorecard_run_id from scorecardrunstatus WHERE scorecard_date = '" + scDate +
                    "' AND execution_date = '" + scRevision + "'";
            ResultSet resultSet = statement.executeQuery(query);

            while(resultSet.next()) {
                runID = resultSet.getInt("scorecard_run_id");
            }

        } catch (SQLException e) {
            log.error("SQLException in getCurrentrunID : " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getCurrentrunID : " + e.getMessage(),e);
        }
        return runID;
    }

    private String getScorecardIDList(Connection connection, int runID) {

        StringBuilder stringBuilder = new StringBuilder();
        try (Statement statement = connection.createStatement()) {

            String query = "SELECT scorecard_id FROM scorecard where scorecard_run_id = " + runID;
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                stringBuilder.append(resultSet.getInt("scorecard_id")).append(",");
            }

            stringBuilder.setLength(stringBuilder.length() - 1);
        } catch (SQLException e) {
            log.error("SQLException in getScorecardIDList; " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getScorecardIDList; " + e.getMessage(),e);
        }
        return stringBuilder.toString();
    }

    private int changeLiveFlagToFalse(Connection connection, String scDate, String scRevision) {

        int result = 0;
        try (Statement statement = connection.createStatement()) {

            String query = "UPDATE scorecardrunstatus SET live_flag = CASE WHEN live_flag = false THEN live_flag ELSE " +
                    "live_flag = false END WHERE scorecard_date = '" + scDate + "' AND execution_date = '" + scRevision + "'" ;
            result = statement.executeUpdate(query);

        } catch (SQLException e) {
            log.error("SQLException in getRunIDWithLiveFlagTrue: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getRunIDWithLiveFlagTrue: " + e.getMessage(),e);
        }
        return result;
    }

    private int copyDataFromViewsToTables(Connection connection, String scorecardIDs) {

        int count = 0;
        int result = 0;
        int sp_result = 0;

        try (Statement statement = connection.createStatement()) {

            String query1 = "SELECT fn_archive_scorecard('" + scorecardIDs + "')";
            /*String query = "INSERT INTO " + tableName + "(" + columnMapping + ")" +
                    " SELECT " + columnMapping + " FROM " + viewName + " WHERE scorecard_id IN ("
                    + scorecardIDs + ")";*/

            ResultSet resultSet1 = statement.executeQuery(query1);
            while (resultSet1.next()) {
                sp_result = resultSet1.getInt("fn_archive_scorecard");
            }

            String query2 = "SELECT count(*) as table_count from tablemapping";
            ResultSet resultSet2 = statement.executeQuery(query2);
            while(resultSet2.next()) {
                count = resultSet2.getInt("table_count");
            }

            if(sp_result == count) {
                result = 1;
            }

        } catch (SQLException e) {
            log.error("SQLException in copyDataFromViewsToTables: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in copyDataFromViewsToTables: " + e.getMessage(),e);
        }
        return result;
    }

    public StringBuilder exportScorecard(String scDate, String scTab) {

        Connection connection = null;
        Properties properties = new Properties();
        String configFileName = "application.properties";
        StringBuilder stringBuilder = new StringBuilder();

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);

            if (connection != null) {
                String scRevision = getRevisionDate(connection,scDate);
                if(scTab.equalsIgnoreCase("rig")) {
                    stringBuilder = exportRigData(connection, properties, scDate, scRevision);
                }
                else if(scTab.equalsIgnoreCase("person")) {
                    stringBuilder = exportPersonData(connection, properties, scDate, scRevision);
                }
            }

        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in exportScorecard: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in exportScorecard: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in exportScorecard: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in exportScorecard: " + e.getMessage(),e);
            }
        }
        return stringBuilder;
    }

    private String getRevisionDate(Connection connection, String scDate) {

        String scRevision = null;

        try (Statement statement = connection.createStatement()) {

            String query = "SELECT execution_date FROM scorecardrunstatus WHERE scorecard_date = '" + scDate +
                            "' AND viewing_flag = true";
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                scRevision = resultSet.getString("execution_date");
            }

        } catch (SQLException e) {
            log.error("JSONException in getRevisionDate: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getRevisionDate: " + e.getMessage(),e);
        }
        return scRevision;
    }

    private StringBuilder exportRigData(Connection connection, Properties properties, String scDate, String scRevision) {

        StringBuilder stringBuilder = new StringBuilder();
        String columnList = properties.getProperty("rig.column.list");
        stringBuilder.append(columnList).append("\n");
        String[] columns = columnList.split(",");

        try {

            JSONArray rigData = getRigData(connection, properties, scDate, scRevision);

            for (int i = 0; i < rigData.length(); i++) {
                JSONObject jsonObject = rigData.getJSONObject(i);

                for (String column : columns) {
                    stringBuilder.append(jsonObject.get(column)).append(",");
                }
                stringBuilder.setLength(stringBuilder.length() - 1);
                stringBuilder.append("\n");
            }
            stringBuilder.setLength(stringBuilder.length() - 1);

        } catch (JSONException e) {
            log.error("JSONException in exportRigData: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in exportRigData: " + e.getMessage(),e);
        }
        return stringBuilder;
    }

    private StringBuilder exportPersonData(Connection connection, Properties properties, String scDate, String scRevision) {

        StringBuilder stringBuilder = new StringBuilder();
        String columnList = properties.getProperty("person.column.list");
        stringBuilder.append(columnList).append("\n");
        String[] columns = columnList.split(",");

        try {

            JSONArray rigData = getPersonData(connection, properties, scDate, scRevision);

            for (int i = 0; i < rigData.length(); i++) {
                JSONObject jsonObject = rigData.getJSONObject(i);

                for (String column : columns) {
                    stringBuilder.append(jsonObject.get(column)).append(",");
                }
                stringBuilder.setLength(stringBuilder.length() - 1);
                stringBuilder.append("\n");
            }
            stringBuilder.setLength(stringBuilder.length() - 1);

        } catch (JSONException e) {
            log.error("JSONException in exportPersonData: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in exportPersonData: " + e.getMessage(),e);
        }
        return stringBuilder;
    }
}
