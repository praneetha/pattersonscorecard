package com.pten.scorecard.repo;

import com.pten.Utilities.DateUtils.DateTimeUtils;
import com.pten.Utilities.DbUtils.PtenInternalDbUtilLib;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

@Component
public class ScoreCardSummaryRepo {
    private static final Logger log = LoggerFactory.getLogger(com.pten.scorecard.repo.ScoreCardRepo.class);

    @Autowired
    PtenInternalDbUtilLib utils;

    private static String righeader = "RIGHEADER AS (\n" +
            "WITH SUPERINTENDENT_RIGS AS (\n" +
            "        SELECT  vasc.scorecard_id AS superintendent_scorecard_id\n" +
            "                ,va2scr.owner_name AS superintendent_name\n" +
            "                ,va2scr.owner_id AS superintendent_id\n" +
            "                ,source_scorecard_id AS rig_scorecard_id \n" +
            "                ,va2scr.scorecard_date\n" +
            "                ,vascr.rig_name\n" +
            "                ,vascr.division_name AS region_name\n" +
            "                ,vascr.region_name AS area_name\n" +
            "                ,null AS operator_name\n" +
            "                ,vascr.rig_master_type_name as rig_type\n" +
            "        FROM \n" +
            "        vw_activescorecardsourcecards vasc\n" +
            "        INNER JOIN vw_ActiveScoreCardsRanked vascr ON vascr.scorecard_id = vasc.source_scorecard_id\n" +
            "        INNER JOIN vw_activesuperscorecardsranked va2scr ON va2scr.scorecard_id = vasc.scorecard_id\n" +
            "        WHERE source_scorecard_id IN (SELECT scorecard_id FROM vw_ActiveScoreCardsRanked \n" +
            "                                        WHERE scorecard_date = :date\n" +
            "                                        ORDER BY scorecard_id)\n" +
            "        AND scorecard_ordinal = 0                                \n" +
            ")\n" +
            "SELECT  vassr.owner_name AS opsmanager_name \n" +
            "        , vassr.scorecard_id AS opsmanager_scorecard_id\n" +
            "        , vassr.owner_id AS opsmanager_id\n" +
            "        , sr.*\n" +
            "FROM SUPERINTENDENT_RIGS sr\n" +
            "INNER JOIN vw_activescorecardsourcecards vasc ON vasc.source_scorecard_id = sr.superintendent_scorecard_id\n" +
            "INNER JOIN vw_activesuperscorecardsranked vassr ON vassr.scorecard_id = vasc.scorecard_id\n" +
            "WHERE vasc.scorecard_ordinal = 0  AND owner_type_id =2 \n" +
            "ORDER BY rig_name    \n" +
            ")";

    private static String everything = "WITH Everything AS(\n" +
            "        SELECT  source_scorecard_id AS rig_scorecard_id \n" +
            "                ,vascr.scorecard_date\n" +
            "                ,vascr.rig_name\n" +
            "        FROM \n" +
            "        vw_activescorecardsourcecards vasc\n" +
            "        INNER JOIN vw_ActiveScoreCardsRanked vascr ON vascr.scorecard_id = vasc.source_scorecard_id\n" +
            "        WHERE source_scorecard_id IN (SELECT scorecard_id FROM vw_ActiveScoreCardsRanked \n" +
            "                                        WHERE scorecard_date between :startDate and :endDate\n" +
            "                                        ORDER BY scorecard_id)\n" +
            "        AND scorecard_ordinal = 0\n" +
            ")";

    public Collection getLatestDate() throws ParseException {
        String query = "select Max(scorecard_date) AS latest_date\n" +
                "From vw_ActiveScoreCardLineItems vasl\n" +
                "INNER JOIN vw_ActiveScoreCardsRanked vasc on vasl.scorecard_id = vasc.scorecard_id";

        return utils.executeNamedQuery(query, null);
    }

    public Collection getScoreCardrigList(String date) throws ParseException {
        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date scrdDate = formatter.parse(date);
        Timestamp scrdTime = new Timestamp(scrdDate.getTime());

        String query = "WITH SUPERINTENDENT_RIGS AS (\n" +
                "        SELECT  vasc.scorecard_id AS superintendent_scorecard_id\n" +
                "                ,va2scr.owner_name AS superintendent_name\n" +
                "                ,va2scr.owner_id AS superintendent_id\n" +
                "                ,source_scorecard_id AS rig_scorecard_id \n" +
                "                ,vascr.rig_name\n" +
                "                ,vascr.scorecard_date\n" +
                "                ,vascr.overall_score\n" +
                "                ,vascr.region_rank AS area_rank\n" +
                "                ,vascr.division_rank AS region_rank\n" +
                "                ,vascr.patterson_rank\n" +
                "                ,vascr.region_average\n" +
                "                ,vascr.division_average\n" +
                "                ,vascr.patterson_average\n" +
                "                ,vascr.division_name AS region_name\n" +
                "                ,vascr.region_name AS area_name\n" +
                "                ,null AS operator_name\n" +
                "                ,vascr.rig_master_type_name as rig_type\n" +
                "        FROM \n" +
                "        vw_activescorecardsourcecards vasc\n" +
                "        INNER JOIN vw_ActiveScoreCardsRanked vascr ON vascr.scorecard_id = vasc.source_scorecard_id\n" +
                "        INNER JOIN vw_activesuperscorecardsranked va2scr ON va2scr.scorecard_id = vasc.scorecard_id\n" +
                "        WHERE source_scorecard_id IN (SELECT scorecard_id FROM vw_ActiveScoreCardsRanked \n" +
                "                                        WHERE scorecard_date = :date\n" +
                "                                        ORDER BY scorecard_id) --- will be an actual list of scorecard_ids sent from the UI\n" +
                "        AND scorecard_ordinal = 0                                \n" +
                ")\n" +
                "SELECT  vassr.owner_name AS opsmanager_name \n" +
                "        , vassr.scorecard_id AS opsmanager_scorecard_id\n" +
                "        , vassr.owner_id AS opsmanager_id\n" +
                "        , sr.*\n" +
                "FROM SUPERINTENDENT_RIGS sr\n" +
                "INNER JOIN vw_activescorecardsourcecards vasc ON vasc.source_scorecard_id = sr.superintendent_scorecard_id\n" +
                "INNER JOIN vw_activesuperscorecardsranked vassr ON vassr.scorecard_id = vasc.scorecard_id\n" +
                "WHERE vasc.scorecard_ordinal = 0  AND owner_type_id =2 \n" +
                "ORDER BY rig_name ";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("date", scrdTime);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getScoreCardrigList(Collection<Integer> id) throws ParseException {

        String query = "WITH SUPERINTENDENT_RIGS AS (\n" +
                "        SELECT  vasc.scorecard_id AS superintendent_scorecard_id\n" +
                "                ,va2scr.owner_name AS superintendent_name\n" +
                "                ,va2scr.owner_id AS superintendent_id\n" +
                "                ,source_scorecard_id AS rig_scorecard_id \n" +
                "                ,vascr.rig_name\n" +
                "                ,vascr.scorecard_date\n" +
                "                ,vascr.overall_score\n" +
                "                ,vascr.region_rank AS area_rank\n" +
                "                ,vascr.division_rank AS region_rank\n" +
                "                ,vascr.patterson_rank\n" +
                "                ,vascr.region_average\n" +
                "                ,vascr.division_average\n" +
                "                ,vascr.patterson_average\n" +
                "                ,vascr.division_name AS region_name\n" +
                "                ,vascr.region_name AS area_name\n" +
                "                ,null AS operator_name\n" +
                "                ,vascr.rig_master_type_name as rig_type\n" +
                "        FROM \n" +
                "        vw_activescorecardsourcecards vasc\n" +
                "        INNER JOIN vw_ActiveScoreCardsRanked vascr ON vascr.scorecard_id = vasc.source_scorecard_id\n" +
                "        INNER JOIN vw_activesuperscorecardsranked va2scr ON va2scr.scorecard_id = vasc.scorecard_id\n" +
                "        WHERE source_scorecard_id IN (:id)\n" +
                "        AND scorecard_ordinal = 0                               \n" +
                ")\n" +
                "SELECT  vassr.owner_name AS opsmanager \n" +
                "        , vassr.scorecard_id AS opsmanager_scorecard_id\n" +
                "        , vassr.owner_id AS opsmanager_id\n" +
                "        , sr.*\n" +
                "FROM SUPERINTENDENT_RIGS sr\n" +
                "INNER JOIN vw_activescorecardsourcecards vasc ON vasc.source_scorecard_id = sr.superintendent_scorecard_id\n" +
                "INNER JOIN vw_activesuperscorecardsranked vassr ON vassr.scorecard_id = vasc.scorecard_id\n" +
                "WHERE vasc.scorecard_ordinal = 0 AND owner_type_id =2 \n" +
                "ORDER BY rig_name";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("id", id);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getOverallTrend(String date) throws ParseException {

        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date endDate = formatter.parse(date);

        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        cal.add(Calendar.MONTH, -6);

        Date startDate = cal.getTime();

        Timestamp startTime = new Timestamp(startDate.getTime());
        Timestamp endTime = new Timestamp(endDate.getTime());


        String query = "SELECT  source_scorecard_id AS scorecard_id \n" +
                "                ,vascr.scorecard_date\n" +
                "                ,vascr.rig_name\n" +
                "                ,vascr.overall_score\n" +
                "        FROM \n" +
                "        vw_activescorecardsourcecards vasc\n" +
                "        INNER JOIN vw_ActiveScoreCardsRanked vascr ON vascr.scorecard_id = vasc.source_scorecard_id\n" +
                "        WHERE source_scorecard_id IN (SELECT scorecard_id FROM vw_ActiveScoreCardsRanked \n" +
                "                                        WHERE scorecard_date between :startDate AND :endDate \n" +
                "                                        ORDER BY scorecard_id)";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("startDate", startTime)
                .addValue("endDate", endTime);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getScoreCardsRigPerformance(String date) throws ParseException {

        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date scrdDate = formatter.parse(date);
        Timestamp scrdTime = new Timestamp(scrdDate.getTime());

        String query = "WITH METRIC AS(\n" +
                "SELECT vasd.scorecard_id\n" +
                "        , MAX(CASE WHEN metric_id = 5 THEN points END) AS trip_speed_total_points\n" +
                "        , MAX(CASE WHEN metric_id = 5 THEN Nullif(raw_value, 'NaN') END) AS trip_speed_value\n" +
                "        , MAX(CASE WHEN metric_id = 5 THEN Nullif(raw_points, 'NaN') END) AS trip_speed_points\n" +
                "        , MAX(CASE WHEN metric_id = 6 THEN points END) AS trip_conn_total_points\n" +
                "        , MAX(CASE WHEN metric_id = 6 THEN Nullif(raw_value, 'NaN') END) AS trip_conn_value\n" +
                "        , MAX(CASE WHEN metric_id = 6 THEN Nullif(raw_points, 'NaN') END) AS trip_conn_points\n" +
                "        , MAX(CASE WHEN metric_id = 7 THEN points END) AS drill_conn_total_points\n" +
                "        , MAX(CASE WHEN metric_id = 7 THEN Nullif(raw_value, 'NaN') END) AS drill_conn_value\n" +
                "        , MAX(CASE WHEN metric_id = 7 THEN Nullif(raw_points, 'NaN') END) AS drill_conn_points\n" +
                "        , MAX(CASE WHEN metric_id = 8 THEN points END) AS skid_total_points\n" +
                "        , MAX(CASE WHEN metric_id = 8 THEN Nullif(raw_value, 'NaN') END) AS skid_value\n" +
                "        , MAX(CASE WHEN metric_id = 8 THEN Nullif(raw_points, 'NaN') END) AS skid_points\n" +
                "        , MAX(CASE WHEN metric_id = 9 THEN points END) AS rmct_total_points\n" +
                "        , MAX(CASE WHEN metric_id = 9 THEN Nullif(raw_value, 'NaN') END) AS rmct_value\n" +
                "        , MAX(CASE WHEN metric_id = 9 THEN Nullif(raw_points, 'NaN') END) AS rmct_points\n"+
                "FROM vw_ActiveScoreCardLineItems vasd\n" +
                "INNER JOIN vw_ActiveScoreCardsRanked vas ON vas.scorecard_id = vasd.scorecard_id\n" +
                "WHERE vas.scorecard_id IN (SELECT scorecard_id FROM vw_ActiveScoreCardsRanked \n" +
                "                                WHERE scorecard_date = :date\n" +
                "                                ORDER BY scorecard_id)\n" +
                "AND metric_id in (5,6,7,8,9)\n" +
                "GROUP BY vasd.scorecard_id\n" +
                "), "+righeader+"SELECT * \n" +
                "FROM RIGHEADER\n" +
                "LEFT JOIN METRIC ON METRIC.scorecard_id = RIGHEADER.rig_scorecard_id";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("date", scrdTime);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getRigPerformanceTrend(String date) throws ParseException {

        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date endDate = formatter.parse(date);

        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        cal.add(Calendar.MONTH, -6);

        Date startDate = cal.getTime();

        Timestamp startTime = new Timestamp(startDate.getTime());
        Timestamp endTime = new Timestamp(endDate.getTime());


        String query = everything +
                "SELECT Everything.rig_scorecard_id AS scorecard_id\n" +
                "        , Everything.scorecard_date\n" +
                "        , Everything.rig_name\n" +
                "        , MAX(CASE WHEN metric_id = 5 THEN points END) AS trip_speed_total_points\n" +
                "        , MAX(CASE WHEN metric_id = 5 THEN Nullif(raw_value, 'NaN') END) AS trip_speed_value\n" +
                "        , MAX(CASE WHEN metric_id = 5 THEN Nullif(raw_points, 'NaN') END) AS trip_speed_points\n" +
                "        , MAX(CASE WHEN metric_id = 6 THEN points END) AS trip_conn_total_points\n" +
                "        , MAX(CASE WHEN metric_id = 6 THEN Nullif(raw_value, 'NaN') END) AS trip_conn_value\n" +
                "        , MAX(CASE WHEN metric_id = 6 THEN Nullif(raw_points, 'NaN') END) AS trip_conn_points\n" +
                "        , MAX(CASE WHEN metric_id = 7 THEN points END) AS drill_conn_total_points\n" +
                "        , MAX(CASE WHEN metric_id = 7 THEN Nullif(raw_value, 'NaN') END) AS drill_conn_value\n" +
                "        , MAX(CASE WHEN metric_id = 7 THEN Nullif(raw_points, 'NaN') END) AS drill_conn_points\n" +
                "        , MAX(CASE WHEN metric_id = 8 THEN points END) AS skid_total_points\n" +
                "        , MAX(CASE WHEN metric_id = 8 THEN Nullif(raw_value, 'NaN') END) AS skid_value\n" +
                "        , MAX(CASE WHEN metric_id = 8 THEN Nullif(raw_points, 'NaN') END) AS skid_points\n" +
                "        , MAX(CASE WHEN metric_id = 9 THEN points END) AS rmct_total_points\n" +
                "        , MAX(CASE WHEN metric_id = 9 THEN Nullif(raw_value, 'NaN') END) AS rmct_value\n" +
                "        , MAX(CASE WHEN metric_id = 9 THEN Nullif(raw_points, 'NaN') END) AS rmct_points\n"+
                "FROM Everything  \n" +
                "LEFT JOIN vw_ActiveScoreCardLineItems vasd on vasd.scorecard_id = Everything.rig_scorecard_id\n" +
                "GROUP BY Everything.rig_scorecard_id, Everything.scorecard_date, Everything.rig_name\n" +
                "ORDER By Everything.rig_scorecard_id, Everything.scorecard_date";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("startDate", startTime)
                .addValue("endDate", endTime);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getScoreCardsHSE(String date) throws ParseException {

        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date scrdDate = formatter.parse(date);
        Timestamp scrdTime = new Timestamp(scrdDate.getTime());

        String query = "WITH METRIC AS(\n" +
                "SELECT vasd.scorecard_id\n" +
                "        , MAX(CASE WHEN metric_id = 4 THEN points END) AS CRI_points  \n" +
                "        , MAX(CASE WHEN metric_id = 4 THEN Nullif(raw_value, 'NaN') END) AS CRI_avg_value  \n" +
                "        , MAX(CASE WHEN metric_id = 4 THEN Nullif(raw_points, 'NaN') END) AS CRI_avg_points  \n" +
                "        , MAX(CASE WHEN metric_id = 1 THEN points END) AS HPE_points\n" +
                "        , MAX(CASE WHEN metric_id = 1 THEN Nullif(raw_value, 'NaN') END) AS HPE_avg_value  \n" +
                "        , MAX(CASE WHEN metric_id = 1 THEN Nullif(raw_points, 'NaN') END) AS HPE_avg_points    \n" +
                "        , MAX(CASE WHEN metric_id = 2 THEN points END) AS IMS_start_late_points\n" +
                "        , MAX(CASE WHEN metric_id = 2 THEN Nullif(raw_value, 'NaN') END) AS IMS_start_late_avg_value  \n" +
                "        , MAX(CASE WHEN metric_id = 2 THEN Nullif(raw_points, 'NaN') END) AS IMS_start_late_avg_points    \n" +
                "        , MAX(CASE WHEN metric_id = 3 THEN points END) AS IMS_end_late_points\n" +
                "        , MAX(CASE WHEN metric_id = 3 THEN Nullif(raw_value, 'NaN') END) AS IMS_end_late_avg_value  \n" +
                "        , MAX(CASE WHEN metric_id = 3 THEN Nullif(raw_points, 'NaN') END) AS IMS_end_late_avg_points\n" +
                "FROM vw_ActiveScoreCardLineItems vasd\n" +
                "INNER JOIN vw_ActiveScoreCardsRanked vas ON vas.scorecard_id = vasd.scorecard_id\n" +
                "WHERE vas.scorecard_id IN (SELECT scorecard_id FROM vw_ActiveScoreCardsRanked \n" +
                "                                WHERE scorecard_date = :date\n" +
                "                                ORDER BY scorecard_id)\n" +
                "AND metric_id in (1,2,3,4)\n" +
                "GROUP BY vasd.scorecard_id\n" +
                "), "+righeader+"SELECT * \n" +
                "FROM RIGHEADER\n" +
                "LEFT JOIN METRIC ON METRIC.scorecard_id = RIGHEADER.rig_scorecard_id";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("date", scrdTime);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getHSETrend(String date) throws ParseException {

        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date endDate = formatter.parse(date);

        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        cal.add(Calendar.MONTH, -6);

        Date startDate = cal.getTime();

        Timestamp startTime = new Timestamp(startDate.getTime());
        Timestamp endTime = new Timestamp(endDate.getTime());


        String query = everything +
                "SELECT Everything.rig_scorecard_id AS scorecard_id\n" +
                "        , Everything.scorecard_date\n" +
                "        , Everything.rig_name\n" +
                "        , MAX(CASE WHEN metric_id = 4 THEN points END) AS CRI_points  \n" +
                "        , MAX(CASE WHEN metric_id = 4 THEN Nullif(raw_value, 'NaN') END) AS CRI_avg_value  \n" +
                "        , MAX(CASE WHEN metric_id = 4 THEN Nullif(raw_points, 'NaN') END) AS CRI_avg_points  \n" +
                "        , MAX(CASE WHEN metric_id = 1 THEN points END) AS HPE_points\n" +
                "        , MAX(CASE WHEN metric_id = 1 THEN Nullif(raw_value, 'NaN') END) AS HPE_avg_value  \n" +
                "        , MAX(CASE WHEN metric_id = 1 THEN Nullif(raw_points, 'NaN') END) AS HPE_avg_points    \n" +
                "        , MAX(CASE WHEN metric_id = 2 THEN points END) AS IMS_start_late_points\n" +
                "        , MAX(CASE WHEN metric_id = 2 THEN Nullif(raw_value, 'NaN') END) AS IMS_start_late_avg_value  \n" +
                "        , MAX(CASE WHEN metric_id = 2 THEN Nullif(raw_points, 'NaN') END) AS IMS_start_late_avg_points    \n" +
                "        , MAX(CASE WHEN metric_id = 3 THEN points END) AS IMS_end_late_points\n" +
                "        , MAX(CASE WHEN metric_id = 3 THEN Nullif(raw_value, 'NaN') END) AS IMS_end_late_avg_value  \n" +
                "        , MAX(CASE WHEN metric_id = 3 THEN Nullif(raw_points, 'NaN') END) AS IMS_end_late_avg_points\n" +
                "FROM Everything  \n" +
                "LEFT JOIN vw_ActiveScoreCardLineItems vasd on vasd.scorecard_id = Everything.rig_scorecard_id\n" +
                "GROUP BY Everything.rig_scorecard_id, Everything.scorecard_date, Everything.rig_name\n" +
                "ORDER By Everything.rig_scorecard_id, Everything.scorecard_date";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("startDate", startTime)
                .addValue("endDate", endTime);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getScoreCardFinancial(String date) throws ParseException {

        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date scrdDate = formatter.parse(date);
        Timestamp scrdTime = new Timestamp(scrdDate.getTime());

        String query = "WITH METRIC AS(\n" +
                "SELECT vasd.scorecard_id\n" +
                "        , MAX(CASE WHEN metric_id = 17 THEN points END) AS cap_spend_total_points\n" +
                "        , MAX(CASE WHEN metric_id = 17 THEN Nullif(raw_value, 'NaN') END) AS cap_spend_value\n" +
                "        , MAX(CASE WHEN metric_id = 17 THEN Nullif(raw_points, 'NaN') END) AS cap_spend_points\n" +
                "        , MAX(CASE WHEN metric_id = 18 THEN points END) AS rm_supp_total_points\n" +
                "        , MAX(CASE WHEN metric_id = 18 THEN Nullif(raw_value, 'NaN') END) AS rm_supp_value\n" +
                "        , MAX(CASE WHEN metric_id = 18 THEN Nullif(raw_points, 'NaN') END) AS rm_supp_points\n" +
                "FROM vw_ActiveScoreCardLineItems vasd\n" +
                "INNER JOIN vw_ActiveScoreCardsRanked vas ON vas.scorecard_id = vasd.scorecard_id\n" +
                "WHERE vas.scorecard_id IN (SELECT scorecard_id FROM vw_ActiveScoreCardsRanked \n" +
                "                                WHERE scorecard_date = :date\n" +
                "                                ORDER BY scorecard_id)\n" +
                "AND metric_id in (17,18)\n" +
                "GROUP BY vasd.scorecard_id\n" +
                "), "+righeader+"SELECT * \n" +
                "FROM RIGHEADER\n" +
                "LEFT JOIN METRIC ON METRIC.scorecard_id = RIGHEADER.rig_scorecard_id";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("date", scrdTime);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getFinancialTrend(String date) throws ParseException {

        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date endDate = formatter.parse(date);

        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        cal.add(Calendar.MONTH, -6);

        Date startDate = cal.getTime();

        Timestamp startTime = new Timestamp(startDate.getTime());
        Timestamp endTime = new Timestamp(endDate.getTime());


        String query = everything +
                "SELECT Everything.rig_scorecard_id AS scorecard_id\n" +
                "        , Everything.scorecard_date\n" +
                "        , Everything.rig_name\n" +
                "        , MAX(CASE WHEN metric_id = 17 THEN points END) AS cap_spend_total_points\n" +
                "        , MAX(CASE WHEN metric_id = 17 THEN Nullif(raw_value, 'NaN') END) AS cap_spend_value\n" +
                "        , MAX(CASE WHEN metric_id = 17 THEN Nullif(raw_points, 'NaN') END) AS cap_spend_points\n" +
                "        , MAX(CASE WHEN metric_id = 18 THEN points END) AS rm_supp_total_points\n" +
                "        , MAX(CASE WHEN metric_id = 18 THEN Nullif(raw_value, 'NaN') END) AS rm_supp_value\n" +
                "        , MAX(CASE WHEN metric_id = 18 THEN Nullif(raw_points, 'NaN') END) AS rm_supp_points\n" +
                "FROM Everything  \n" +
                "LEFT JOIN vw_ActiveScoreCardLineItems vasd on vasd.scorecard_id = Everything.rig_scorecard_id\n" +
                "GROUP BY Everything.rig_scorecard_id, Everything.scorecard_date, Everything.rig_name\n" +
                "ORDER By Everything.rig_scorecard_id, Everything.scorecard_date";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("startDate", startTime)
                .addValue("endDate", endTime);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getScoreCardPeopleData(String date) throws ParseException {

        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date scrdDate = formatter.parse(date);
        Timestamp scrdTime = new Timestamp(scrdDate.getTime());

        String query = "WITH METRIC AS(\n" +
                "SELECT vasd.scorecard_id\n" +
                "        , MAX(CASE WHEN metric_id = 15 THEN points END) AS turnover_total_points\n" +
                "        , MAX(CASE WHEN metric_id = 15 THEN Nullif(raw_value, 'NaN') END) AS turnover_value\n" +
                "        , MAX(CASE WHEN metric_id = 15 THEN Nullif(raw_points, 'NaN') END) AS turnover_points\n" +
                "        , MAX(CASE WHEN metric_id = 20 THEN points END) AS cmr_err_total_points\n" +
                "        , MAX(CASE WHEN metric_id = 20 THEN Nullif(raw_value, 'NaN') END) AS cmr_err_value\n" +
                "        , MAX(CASE WHEN metric_id = 20 THEN Nullif(raw_points, 'NaN') END) AS cmr_err_points\n" +
                "FROM vw_ActiveScoreCardLineItems vasd\n" +
                "INNER JOIN vw_ActiveScoreCardsRanked vas ON vas.scorecard_id = vasd.scorecard_id\n" +
                "WHERE vas.scorecard_id IN (SELECT scorecard_id FROM vw_ActiveScoreCardsRanked \n" +
                "                                WHERE scorecard_date = :date\n" +
                "                                ORDER BY scorecard_id)\n" +
                "AND metric_id in (15,20)\n" +
                "GROUP BY vasd.scorecard_id\n" +
                "), "+righeader+"SELECT * \n" +
                "FROM RIGHEADER\n" +
                "LEFT JOIN METRIC ON METRIC.scorecard_id = RIGHEADER.rig_scorecard_id";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("date", scrdTime);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getPeopleDataTrend(String date) throws ParseException {

        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date endDate = formatter.parse(date);

        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        cal.add(Calendar.MONTH, -6);

        Date startDate = cal.getTime();

        Timestamp startTime = new Timestamp(startDate.getTime());
        Timestamp endTime = new Timestamp(endDate.getTime());


        String query = everything +
                "SELECT Everything.rig_scorecard_id AS scorecard_id\n" +
                "        , Everything.scorecard_date\n" +
                "        , Everything.rig_name\n" +
                "        , MAX(CASE WHEN metric_id = 15 THEN points END) AS turnover_total_points\n" +
                "        , MAX(CASE WHEN metric_id = 15 THEN Nullif(raw_value, 'NaN') END) AS turnover_value\n" +
                "        , MAX(CASE WHEN metric_id = 15 THEN Nullif(raw_points, 'NaN') END) AS turnover_points\n" +
                "        , MAX(CASE WHEN metric_id = 20 THEN points END) AS cmr_err_total_points\n" +
                "        , MAX(CASE WHEN metric_id = 20 THEN Nullif(raw_value, 'NaN') END) AS cmr_err_value\n" +
                "        , MAX(CASE WHEN metric_id = 20 THEN Nullif(raw_points, 'NaN') END) AS cmr_err_points\n" +
                "FROM Everything  \n" +
                "LEFT JOIN vw_ActiveScoreCardLineItems vasd on vasd.scorecard_id = Everything.rig_scorecard_id\n" +
                "GROUP BY Everything.rig_scorecard_id, Everything.scorecard_date, Everything.rig_name\n" +
                "ORDER By Everything.rig_scorecard_id, Everything.scorecard_date";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("startDate", startTime)
                .addValue("endDate", endTime);
        return utils.executeNamedQuery(query, namedParameters);
    }

    public Collection getScoreCardEquipment(Date date){

        String query = "with Equipment as (\n" +
                "SELECT vasd.scorecard_id\n" +
                "        , MAX(CASE WHEN metric_id = 10 THEN points END) AS NPT_total_points  \n" +
                "        , MAX(CASE WHEN metric_id = 10 THEN Nullif(raw_value, 'NaN') END) AS NPT_value  \n" +
                "        , MAX(CASE WHEN metric_id = 10 THEN Nullif(raw_points, 'NaN') END) AS NPT_points  \n" +
                "        \n" +
                "        , MAX(CASE WHEN metric_id = 11 THEN points END) AS ops_wo_total_points\n" +
                "        , MAX(CASE WHEN metric_id = 11 THEN Nullif(raw_value, 'NaN') END) AS ops_wo_value  \n" +
                "        , MAX(CASE WHEN metric_id = 11 THEN Nullif(raw_points, 'NaN') END) AS ops_wo_points    \n" +
                "        \n" +
                "        , MAX(CASE WHEN metric_id = 12 THEN points END) AS tech_wo_total_points\n" +
                "        , MAX(CASE WHEN metric_id = 12 THEN Nullif(raw_value, 'NaN') END) AS tech_wo_value  \n" +
                "        , MAX(CASE WHEN metric_id = 12 THEN Nullif(raw_points, 'NaN') END) AS tech_wo_points    \n" +
                "        \n" +
                "        , MAX(CASE WHEN metric_id = 13 THEN points END) AS tib_tsa_wo_total_points\n" +
                "        , MAX(CASE WHEN metric_id = 13 THEN Nullif(raw_value, 'NaN') END) AS tib_tsa_wo_value  \n" +
                "        , MAX(CASE WHEN metric_id = 13 THEN Nullif(raw_points, 'NaN') END) AS tib_tsa_wo_points\n" +
                "        \n" +
                "        , MAX(CASE WHEN metric_id = 14 THEN points END) AS asset_inventory_total_points\n" +
                "        , MAX(CASE WHEN metric_id = 14 THEN Nullif(raw_value, 'NaN') END) AS asset_inventory_value  \n" +
                "        , MAX(CASE WHEN metric_id = 14 THEN Nullif(raw_points, 'NaN') END) AS asset_inventory_points\n" +
                "FROM vw_ActiveScoreCardLineItems vasd\n" +
                "INNER JOIN vw_ActiveScoreCardsRanked vas ON vas.scorecard_id = vasd.scorecard_id\n" +
                "WHERE vas.scorecard_id IN (SELECT scorecard_id FROM vw_ActiveScoreCardsRanked \n" +
                "                                WHERE scorecard_date = :date\n" +
                "                                ORDER BY scorecard_id)\n" +
                "        AND metric_id in (10,11,12,13,14)\n" +
                "GROUP BY vasd.scorecard_id\n" +
                "), " + righeader +
                "select * from RIGHEADER\n" +
                "LEFT JOIN Equipment ON Equipment.scorecard_id = RIGHEADER.rig_scorecard_id;";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("date", date)
                ;
        return utils.executeNamedQuery(query, namedParameters);
    }



    public Collection getScoreCardEquipmentTrend(String date) throws ParseException {

        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date endDate = formatter.parse(date);

        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        cal.add(Calendar.MONTH, -6);

        Date startDate = cal.getTime();

        Timestamp startTime = new Timestamp(startDate.getTime());
        Timestamp endTime = new Timestamp(endDate.getTime());

        String query = everything +
                "SELECT Everything.rig_scorecard_id AS scorecard_id\n" +
                "        , Everything.scorecard_date\n" +
                "        , Everything.rig_name        \n" +
                "        \n" +
                "        , MAX(CASE WHEN metric_id = 10 THEN points END) AS NPT_total_points  \n" +
                "        , MAX(CASE WHEN metric_id = 10 THEN Nullif(raw_value, 'NaN') END) AS NPT_value  \n" +
                "        , MAX(CASE WHEN metric_id = 10 THEN Nullif(raw_points, 'NaN') END) AS NPT_points  \n" +
                "        \n" +
                "        , MAX(CASE WHEN metric_id = 11 THEN points END) AS ops_wo_total_points\n" +
                "        , MAX(CASE WHEN metric_id = 11 THEN Nullif(raw_value, 'NaN') END) AS ops_wo_value  \n" +
                "        , MAX(CASE WHEN metric_id = 11 THEN Nullif(raw_points, 'NaN') END) AS ops_wo_points    \n" +
                "        \n" +
                "        , MAX(CASE WHEN metric_id = 12 THEN points END) AS tech_wo_total_points\n" +
                "        , MAX(CASE WHEN metric_id = 12 THEN Nullif(raw_value, 'NaN') END) AS tech_wo_value  \n" +
                "        , MAX(CASE WHEN metric_id = 12 THEN Nullif(raw_points, 'NaN') END) AS tech_wo_points    \n" +
                "        \n" +
                "        , MAX(CASE WHEN metric_id = 13 THEN points END) AS tib_tsa_wo_total_points\n" +
                "        , MAX(CASE WHEN metric_id = 13 THEN Nullif(raw_value, 'NaN') END) AS tib_tsa_wo_value  \n" +
                "        , MAX(CASE WHEN metric_id = 13 THEN Nullif(raw_points, 'NaN') END) AS tib_tsa_wo_points\n" +
                "        \n" +
                "        , MAX(CASE WHEN metric_id = 14 THEN points END) AS asset_inventory_total_points\n" +
                "        , MAX(CASE WHEN metric_id = 14 THEN Nullif(raw_value, 'NaN') END) AS asset_inventory_value  \n" +
                "        , MAX(CASE WHEN metric_id = 14 THEN Nullif(raw_points, 'NaN') END) AS asset_inventory_points\n" +
                "FROM Everything  \n" +
                "LEFT JOIN vw_ActiveScoreCardLineItems vasd on vasd.scorecard_id = Everything.rig_scorecard_id\n" +
                "GROUP BY Everything.rig_scorecard_id, Everything.scorecard_date, Everything.rig_name\n" +
                "ORDER By Everything.rig_scorecard_id, Everything.scorecard_date;";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("endDate", endTime)
                .addValue("startDate",startTime)
                ;
        return utils.executeNamedQuery(query, namedParameters);
    }

}
