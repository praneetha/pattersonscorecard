package com.pten.scorecard.repo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

@Component
public class ScorecardRigManagerRepo {

    private static final Logger log = LoggerFactory.getLogger(ScorecardRigManagerRepo.class);

    private Connection connectToPostgres(Properties properties) {

        Connection connection = null;

        try {
            String username = properties.getProperty("ptenInternal.datasource.username");
            String password = properties.getProperty("ptenInternal.datasource.password");
            String connectionURL = properties.getProperty("ptenInternal.datasource.scorecard.jdbc-url");

            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(connectionURL, username, password);

        } catch (Exception e) {
            log.error("Exception in connectToPostgres: " + e.getMessage(),e);
        }
        return connection;
    }

    public ResponseEntity<Object> getRigList() {

        Connection connection = null;
        Properties properties = new Properties();
        String configFileName = "application.properties";
        JSONArray rigInfo = new JSONArray();

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                rigInfo = getDistinctRigs(connection);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in getRigList: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in getRigList: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getRigList: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in getRigList: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(rigInfo, HttpStatus.OK);
    }

    private JSONArray getDistinctRigs(Connection connection) {

        JSONArray rigInfo = new JSONArray();

        try (Statement statement = connection.createStatement()) {

            String query = "SELECT r.rig_id, r.rig_name, rt.rig_type_name, rmt.rig_master_type_name FROM rig r " +
                            "JOIN rigtorigtype rtr ON r.rig_id = rtr.rig_id " +
                            "JOIN rigtype rt ON rtr.rig_type_id = rt.rig_type_id " +
                            "JOIN rigmastertype rmt ON rt.rig_master_type_id = rmt.rig_master_type_id " +
                            "ORDER BY r.rig_name";
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()) {
                JSONObject rigNames = new JSONObject();
                rigNames.put("rigID",resultSet.getString("rig_id"));
                rigNames.put("rigName",resultSet.getString("rig_name"));
                rigNames.put("rigTypeName",resultSet.getString("rig_type_name"));
                rigNames.put("rigMasterTypeName",resultSet.getString("rig_master_type_name"));
                rigInfo.put(rigNames);
            }

        } catch (SQLException e) {
            log.error("SQLException in getDistinctRigs: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getDistinctRigs: " + e.getMessage(),e);
        }
        return rigInfo;
    }

    public ResponseEntity<Object> getRigTypeList() {

        Connection connection = null;
        Properties properties = new Properties();
        String configFileName = "application.properties";
        JSONArray rigTypeList = new JSONArray();

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                rigTypeList = getRigTypes(connection);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in getRigTypeList: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in getRigTypeList: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getRigTypeList: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in getRigTypeList: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(rigTypeList, HttpStatus.OK);
    }

    private JSONArray getRigTypes(Connection connection) {

        JSONArray rigTypeList = new JSONArray();

        try (Statement statement = connection.createStatement()) {

            String query = "SELECT rig_type_id,rig_type_name FROM rigtype ORDER BY rig_type_name";
            ResultSet resultSet = statement.executeQuery(query);

            while(resultSet.next()) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("rigTypeID",resultSet.getInt("rig_type_id"));
                jsonObject.put("rigTypeName",resultSet.getString("rig_type_name"));
                rigTypeList.put(jsonObject);
            }

        } catch (SQLException e) {
            log.error("SQLException in getRigTypes: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getRigTypes: " + e.getMessage(),e);
        }
        return rigTypeList;
    }

    public ResponseEntity<Object> getRigMasterTypeList() {

        Connection connection = null;
        Properties properties = new Properties();
        String configFileName = "application.properties";
        JSONArray rigMasterTypeList = new JSONArray();

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                rigMasterTypeList = getRigMasterTypes(connection);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in getRigMasterTypeList: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in getRigMasterTypeList: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getRigMasterTypeList: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in getRigMasterTypeList: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(rigMasterTypeList, HttpStatus.OK);
    }

    private JSONArray getRigMasterTypes(Connection connection) {

        JSONArray rigMasterTypeList = new JSONArray();

        try (Statement statement = connection.createStatement()) {

            String query = "SELECT rig_master_type_id,rig_master_type_name FROM rigmastertype ORDER BY rig_master_type_name";
            ResultSet resultSet = statement.executeQuery(query);

            while(resultSet.next()) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("rigMasterTypeID",resultSet.getInt("rig_master_type_id"));
                jsonObject.put("rigMasterTypeName",resultSet.getString("rig_master_type_name"));
                rigMasterTypeList.put(jsonObject);
            }

        } catch (SQLException e) {
            log.error("SQLException in getRigMasterTypes: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getRigMasterTypes: " + e.getMessage(),e);
        }
        return rigMasterTypeList;
    }

    public ResponseEntity<Object> getRegionList() {

        Connection connection = null;
        Properties properties = new Properties();
        String configFileName = "application.properties";
        JSONArray regionList = new JSONArray();

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                regionList = getRegions(connection);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in getRegionList: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in getRegionList: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getRegionList: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in getRegionList: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(regionList, HttpStatus.OK);
    }

    private JSONArray getRegions(Connection connection) {

        JSONArray regionList = new JSONArray();

        try (Statement statement = connection.createStatement()) {

            String query = "SELECT region_id,region_name FROM region ORDER BY region_name";
            ResultSet resultSet = statement.executeQuery(query);

            while(resultSet.next()) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("regionID",resultSet.getInt("region_id"));
                jsonObject.put("regionName",resultSet.getString("region_name"));
                regionList.put(jsonObject);
            }

        } catch (SQLException e) {
            log.error("SQLException in getRegions: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getRegions: " + e.getMessage(),e);
        }
        return regionList;
    }

    public ResponseEntity<Object> getOwnerList() {

        Connection connection = null;
        Properties properties = new Properties();
        String configFileName = "application.properties";
        JSONArray ownerList = new JSONArray();

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                ownerList = getOwners(connection);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in getOwnerList: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in getOwnerList: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getOwnerList: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in getOwnerList: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(ownerList, HttpStatus.OK);
    }

    private JSONArray getOwners(Connection connection) {

        JSONArray ownerList = new JSONArray();

        try (Statement statement = connection.createStatement()) {

            String query = "SELECT owner_id,concat(first_name , ' ' , last_name) as owner_name FROM owner ORDER BY owner_name";
            ResultSet resultSet = statement.executeQuery(query);

            while(resultSet.next()) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("ownerID",resultSet.getInt("owner_id"));
                jsonObject.put("ownerName",resultSet.getString("owner_name"));
                ownerList.put(jsonObject);
            }

        } catch (SQLException e) {
            log.error("SQLException in getOwners: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getOwners: " + e.getMessage(),e);
        }
        return ownerList;
    }

    public ResponseEntity<Object> getLocationByRig(int rigID) {

        Connection connection = null;
        Properties properties = new Properties();
        String configFileName = "application.properties";
        JSONArray locationInfo = new JSONArray();

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                locationInfo = getLocationInfo(connection,rigID);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in getLocationByRig: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in getLocationByRig: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getLocationByRig: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in getLocationByRig: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(locationInfo, HttpStatus.OK);
    }

    private JSONArray getLocationInfo(Connection connection, int rigID) {

        JSONArray locationList = new JSONArray();

        try (Statement statement = connection.createStatement()) {

            String query = "SELECT rtr.region_id, r.region_name, rtr.start_date, rtr.end_date FROM rigtoregion rtr " +
                            "JOIN region r ON r.region_id = rtr.region_id " +
                            "WHERE rtr.rig_id = " + rigID + " ORDER BY rtr.start_date desc";
            ResultSet resultSet = statement.executeQuery(query);

            while(resultSet.next()) {
                JSONObject locationInfo = new JSONObject();
                locationInfo.put("regionID",resultSet.getInt("region_id"));
                locationInfo.put("regionName",resultSet.getString("region_name"));
                locationInfo.put("startDate",resultSet.getString("start_date"));
                locationInfo.put("endDate",resultSet.getString("end_date"));
                locationList.put(locationInfo);
            }

        } catch (SQLException e) {
            log.error("SQLException in getLocationInfo: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getLocationInfo: " + e.getMessage(),e);
        }
        return locationList;
    }

    public ResponseEntity<Object> getSuperintendentByRig(int rigID) {

        Connection connection = null;
        Properties properties = new Properties();
        String configFileName = "application.properties";
        JSONArray superintendentInfo = new JSONArray();

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                superintendentInfo = getSuperintendentInfo(connection,rigID);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in getSuperintendentByRig: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in getSuperintendentByRig: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getSuperintendentByRig: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in getSuperintendentByRig: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(superintendentInfo, HttpStatus.OK);
    }

    private JSONArray getSuperintendentInfo(Connection connection, int rigID) {

        JSONArray superintendentList = new JSONArray();

        try (Statement statement = connection.createStatement()) {

            String query = "SELECT otr.owner_id, concat(o.first_name, ' ', o.last_name) as owner_name, otr.start_date, " +
                            "otr.end_date FROM ownertorig otr " +
                            "JOIN owner o on o.owner_id = otr.owner_id " +
                            "WHERE otr.rig_id = " + rigID + " ORDER BY otr.start_date desc";
            ResultSet resultSet = statement.executeQuery(query);

            while(resultSet.next()) {
                JSONObject superintendentInfo = new JSONObject();
                superintendentInfo.put("ownerID",resultSet.getInt("owner_id"));
                superintendentInfo.put("ownerName",resultSet.getString("owner_name"));
                superintendentInfo.put("startDate",resultSet.getString("start_date"));
                superintendentInfo.put("endDate",resultSet.getString("end_date"));
                superintendentList.put(superintendentInfo);
            }

        } catch (SQLException e) {
            log.error("SQLException in getSuperintendentInfo: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in getSuperintendentInfo: " + e.getMessage(),e);
        }
        return superintendentList;
    }

    public ResponseEntity<Object> addNewRig(String rigName, int rigTypeID) {

        Connection connection = null;
        JSONObject result = new JSONObject();
        Properties properties = new Properties();
        String configFileName = "application.properties";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                result = newRigAddition(connection,rigName,rigTypeID);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in addNewRig: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in addNewRig: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in addNewRig: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in addNewRig: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    private JSONObject newRigAddition(Connection connection, String rigName, int rigTypeID) {

        int result1;
        int rigID = 0;
        int result2 = 0;
        String message = null;
        String rigMasterType = null;
        JSONObject jsonObject = new JSONObject();

        try (Statement statement = connection.createStatement()) {

            String rigMasterTypeQuery = "SELECT rmt.rig_master_type_name FROM rigmastertype rmt " +
                                        "JOIN rigtype rt ON rmt.rig_master_type_id = rt.rig_master_type_id " +
                                        "WHERE rig_type_id = " + rigTypeID;
            ResultSet queryResult = statement.executeQuery(rigMasterTypeQuery);

            while(queryResult.next()) {
                rigMasterType = queryResult.getString("rig_master_type_name");
            }

            String insertRigQuery = "INSERT INTO rig (rig_name,calculation_flag,start_date) VALUES (" +
                                    rigName + ",true,'2015-01-01 00:00:00')";
            result1 = statement.executeUpdate(insertRigQuery);

            if(result1 != 0) {
                String rigIDQuery = "SELECT rig_id FROM rig WHERE rig_name = '" + rigName + "'";
                ResultSet resultSet = statement.executeQuery(rigIDQuery);

                while(resultSet.next()) {
                    rigID = resultSet.getInt("rig_id");
                }
            }

            if(rigID != 0) {
                String insertRigToRigTypeQuery = "INSERT INTO rigTorigtype (rig_id,rig_type_id,start_date) VALUES (" +
                    rigID + "," + rigTypeID + ",'2015-01-01 00:00:00')";
                result2 = statement.executeUpdate(insertRigToRigTypeQuery);
            }
            else {
                String deleteRigQuery = "DELETE FROM rig WHERE rig_name = '" + rigName + "'";
                int result = statement.executeUpdate(deleteRigQuery);
                if(result != 0) {
                    message = "Failure";
                }
            }

            if(result2 != 0) {
                message = "Success";
            }
            else {
                String deleteRigQuery = "DELETE FROM rig WHERE rig_name = '" + rigName + "'";
                int result = statement.executeUpdate(deleteRigQuery);
                if(result != 0) {
                    message = "Failure";
                }
            }

            jsonObject.put("message",message);
            jsonObject.put("rigID",rigID);
            jsonObject.put("rigMasterTypeName",rigMasterType);

        } catch (JSONException e) {
            log.error("JSONException in newRigAddition: " + e.getMessage(),e);
            message = "Failure";
            try {
                jsonObject.put("message",message);
                jsonObject.put("rigID",0);
                jsonObject.put("rigMasterTypeName",rigMasterType);
            } catch (JSONException ex) {
                log.error("JSONException in newRigAddition: " + ex.getMessage(),ex);
            }
        } catch (SQLException e) {
            log.error("SQLException in newRigAddition: " + e.getMessage(),e);
            message = "Failure";
            try {
                jsonObject.put("message",message);
                jsonObject.put("rigID",0);
                jsonObject.put("rigMasterTypeName",rigMasterType);
            } catch (JSONException ex) {
                log.error("JSONException in newRigAddition: " + ex.getMessage(),ex);
            }
        } catch (Exception e) {
            log.error("Exception in newRigAddition: " + e.getMessage(),e);
            message = "Failure";
            try {
                jsonObject.put("message",message);
                jsonObject.put("rigID",0);
                jsonObject.put("rigMasterTypeName",rigMasterType);
            } catch (JSONException ex) {
                log.error("JSONException in newRigAddition: " + ex.getMessage(),ex);
            }
        }
        return jsonObject;
    }

    public ResponseEntity<Object> addRigLocation(int rigID, int regionID, String startDate, String endDate) {

        String message = null;
        Connection connection = null;
        Properties properties = new Properties();
        String configFileName = "application.properties";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                message = addNewRegion(connection,rigID,regionID,startDate,endDate);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in addRigLocation: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in addRigLocation: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in addRigLocation: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in addRigLocation: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(message,HttpStatus.OK);
    }

    private String addNewRegion(Connection connection, int rigID, int regionID, String startDate, String endDate) {

        String message;
        String oldStartDate1 = null;
        String oldStartDate2 = null;
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try (Statement statement = connection.createStatement()) {

            if(endDate == null || endDate.isEmpty()) {

                String query1 = "SELECT * FROM rigtoregion WHERE start_date = " +
                        "(SELECT max(start_date) FROM rigtoregion WHERE rig_id = " + rigID +
                        "AND start_date < '" + startDate + "')";
                ResultSet resultSet1 = statement.executeQuery(query1);

                while (resultSet1.next()) {
                    oldStartDate1 = resultSet1.getString("start_date");
                }

                if(oldStartDate1 != null) {
                    Date oldDate1 = formatter.parse(startDate);
                    cal.setTime(oldDate1);
                    cal.add(Calendar.SECOND,-1);
                    Date newDate1 = cal.getTime();
                    String newEndDate = formatter.format(newDate1);

                    String query2 = "UPDATE rigtoregion SET end_date = '" + newEndDate + "' WHERE rig_id = " + rigID +
                            " AND start_date = '" + oldStartDate1 + "'";
                    statement.executeUpdate(query2);

                    String query3 = "DELETE FROM rigtoregion WHERE rig_id = " + rigID + " AND start_date > '" +
                            startDate + "'";
                    statement.executeUpdate(query3);

                    String query4 = "INSERT INTO rigtoregion (region_id,rig_id,start_date,end_date) VALUES (" +
                            regionID + "," + rigID + ",'" + startDate + "', null)";
                    int result = statement.executeUpdate(query4);

                    if(result != 0) {
                        message = "Success";
                    } else {
                        message = "Failure";
                    }
                } else {

                    String query3 = "DELETE FROM rigtoregion WHERE rig_id = " + rigID + " AND start_date > '" +
                            startDate + "'";
                    statement.executeUpdate(query3);

                    System.out.println("Deleted records");

                    String query4 = "INSERT INTO rigtoregion (region_id,rig_id,start_date,end_date) VALUES (" +
                            regionID + "," + rigID + ",'" + startDate + "', null)";
                    int result = statement.executeUpdate(query4);

                    System.out.println("Inserted new record");

                    if(result != 0) {
                        message = "Success";
                    } else {
                        message = "Failure";
                    }
                }
            }
            else {

                String query1 = "SELECT * FROM rigtoregion WHERE start_date = " +
                                "(SELECT max(start_date) FROM rigtoregion WHERE rig_id = " + rigID +
                                "AND start_date < '" + startDate + "')";
                ResultSet resultSet1 = statement.executeQuery(query1);

                while (resultSet1.next()) {
                    oldStartDate1 = resultSet1.getString("start_date");
                }

                if(oldStartDate1 != null) {
                    Date oldDate1 = formatter.parse(startDate);
                    cal.setTime(oldDate1);
                    cal.add(Calendar.SECOND,-1);
                    Date newDate1 = cal.getTime();
                    String newEndDate = formatter.format(newDate1);

                    String query2 = "UPDATE rigtoregion SET end_date = '" + newEndDate + "' WHERE rig_id = " + rigID +
                            " AND start_date = '" + oldStartDate1 + "'";
                    statement.executeUpdate(query2);

                    String query3 = "DELETE FROM rigtoregion WHERE rig_id = " + rigID + " AND start_date > '" + startDate +
                            "' AND end_date < '" + endDate + "'";
                    statement.executeUpdate(query3);

                    String query4 = "SELECT * FROM rigtoregion WHERE start_date = " +
                            "(SELECT min(start_date) FROM rigtoregion WHERE rig_id = " + rigID +
                            "AND start_date > '" + startDate + "')";
                    ResultSet resultSet4 = statement.executeQuery(query4);

                    while (resultSet4.next()) {
                        oldStartDate2 = resultSet4.getString("start_date");
                    }

                    if(oldStartDate2 != null) {
                        Date oldDate2 = formatter.parse(endDate);
                        cal.setTime(oldDate2);
                        cal.add(Calendar.SECOND,1);
                        Date newDate2 = cal.getTime();
                        String newStartDate = formatter.format(newDate2);

                        String query5 = "UPDATE rigtoregion SET start_date = '" + newStartDate + "' WHERE rig_id = " + rigID +
                                " AND start_date = '" + oldStartDate2 + "'";
                        statement.executeUpdate(query5);

                        String query6 = "INSERT INTO rigtoregion (region_id,rig_id,start_date,end_date) VALUES (" +
                                regionID + "," + rigID + ",'" + startDate + "','" + endDate + "')";
                        int result = statement.executeUpdate(query6);

                        if(result != 0) {
                            message = "Success";
                        } else {
                            message = "Failure";
                        }
                    } else {

                        String query6 = "INSERT INTO rigtoregion (region_id,rig_id,start_date,end_date) VALUES (" +
                                regionID + "," + rigID + ",'" + startDate + "','" + endDate + "')";
                        int result = statement.executeUpdate(query6);

                        if(result != 0) {
                            message = "Success";
                        } else {
                            message = "Failure";
                        }
                    }
                } else {
                    String query4 = "SELECT * FROM rigtoregion WHERE start_date = " +
                            "(SELECT min(start_date) FROM rigtoregion WHERE rig_id = " + rigID +
                            "AND start_date > '" + startDate + "')";
                    ResultSet resultSet4 = statement.executeQuery(query4);

                    while (resultSet4.next()) {
                        oldStartDate2 = resultSet4.getString("start_date");
                    }

                    if(oldStartDate2 != null) {
                        Date oldDate2 = formatter.parse(endDate);
                        cal.setTime(oldDate2);
                        cal.add(Calendar.SECOND,1);
                        Date newDate2 = cal.getTime();
                        String newStartDate = formatter.format(newDate2);

                        String query5 = "UPDATE rigtoregion SET start_date = '" + newStartDate + "' WHERE rig_id = " + rigID +
                                " AND start_date = '" + oldStartDate2 + "'";
                        statement.executeUpdate(query5);

                        String query6 = "INSERT INTO rigtoregion (region_id,rig_id,start_date,end_date) VALUES (" +
                                regionID + "," + rigID + ",'" + startDate + "','" + endDate + "')";
                        int result = statement.executeUpdate(query6);

                        if(result != 0) {
                            message = "Success";
                        } else {
                            message = "Failure";
                        }
                    } else {

                        String query6 = "INSERT INTO rigtoregion (region_id,rig_id,start_date,end_date) VALUES (" +
                                regionID + "," + rigID + ",'" + startDate + "','" + endDate + "')";
                        int result = statement.executeUpdate(query6);

                        if(result != 0) {
                            message = "Success";
                        } else {
                            message = "Failure";
                        }
                    }
                }
            }

        } catch (SQLException e) {
            log.error("SQLException in addNewRegion: " + e.getMessage(),e);
            return "Failure";
        } catch (Exception e) {
            log.error("Exception in addNewRegion: " + e.getMessage(),e);
            return "Failure";
        }
        return message;
    }

    public ResponseEntity<Object> addRigOwner(int rigID, int ownerID, String startDate) {

        String message = null;
        Connection connection = null;
        Properties properties = new Properties();
        String configFileName = "application.properties";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                message = addNewOwner(connection,rigID,ownerID,startDate);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in addRigOwner: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in addRigOwner: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in addRigOwner: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in addRigOwner: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(message,HttpStatus.OK);
    }

    private String addNewOwner(Connection connection, int rigID, int ownerID, String startDate) {

        String message;

        try (Statement statement = connection.createStatement()) {

            String insertQuery = "INSERT INTO ownertorig (owner_id,rig_id,start_date,end_date) VALUES (" +
                    ownerID + "," + rigID + ",'" + startDate + "',null)";
            int result = statement.executeUpdate(insertQuery);

            if(result != 0) {
                message = "Success";
            }
            else {
                message = "Failure";
            }

        } catch (SQLException e) {
            log.error("SQLException in addNewOwner: " + e.getMessage(),e);
            return "Failure";
        } catch (Exception e) {
            log.error("Exception in addNewOwner: " + e.getMessage(),e);
            return "Failure";
        }
        return message;
    }

    public ResponseEntity<Object> editRigLocation(String oldRecord, String newRecord) {

        Connection connection = null;
        JSONObject jsonObject = new JSONObject();
        Properties properties = new Properties();
        String configFileName = "application.properties";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                jsonObject = editRigLocationInfo(connection,oldRecord,newRecord);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in editRigLocation: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in editRigLocation: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in editRigLocation: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in editRigLocation: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(jsonObject,HttpStatus.OK);
    }

    private JSONObject editRigLocationInfo(Connection connection, String oldRecord, String newRecord) {

        int count = 0;
        String message = null;
        JSONObject jsonObject = new JSONObject();
        HashMap<String,Integer> changedKeys= new HashMap<>();

        try (Statement statement = connection.createStatement()){

            JSONObject oldRecordJSON = new JSONObject(oldRecord);
            JSONObject newRecordJSON = new JSONObject(newRecord);

            changedKeys.put("regionName",0);
            changedKeys.put("startDate",0);
            changedKeys.put("endDate",0);

            Iterator keys = oldRecordJSON.keys();
            while (keys.hasNext()) {
                String key = keys.next().toString();
                if(changedKeys.containsKey(key)) {
                    if(!oldRecordJSON.get(key).equals(newRecordJSON.get(key))) {
                        changedKeys.replace(key,1);
                        count ++;
                    }
                }
            }

            if(count != 0) {

                String query = "DELETE FROM rigtoregion WHERE rig_id = " + oldRecordJSON.getString("rigID") +
                                " AND region_id = " + oldRecordJSON.getString("regionID") + " AND start_date = '" +
                                oldRecordJSON.getString("startDate") + "'";
                statement.executeUpdate(query);

                message = addNewRegion(connection,newRecordJSON.getInt("rigID"),newRecordJSON.getInt("regionID"),
                        newRecordJSON.getString("startDate"),newRecordJSON.getString("endDate"));

                jsonObject.put("message",message);
            } else {
                jsonObject.put("message","Nothing to be updated");
            }

        } catch (JSONException e) {
            log.error("JSONException in editRigLocationInfo: " + e.getMessage(),e);
        } catch (SQLException e) {
            log.error("SQLException in editRigLocationInfo: " + e.getMessage(),e);
        } catch(Exception e) {
            log.error("Exception in editRigLocationInfo: " + e.getMessage(),e);
        }
        return  jsonObject;
    }

    public ResponseEntity<Object> editRigOwner(String oldRecord, String newRecord) {

        Connection connection = null;
        JSONObject jsonObject = new JSONObject();
        Properties properties = new Properties();
        String configFileName = "application.properties";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                jsonObject = editRigOwnerInfo(connection,oldRecord,newRecord);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in editRigOwner: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in editRigOwner: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in editRigOwner: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in editRigOwner: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(jsonObject,HttpStatus.OK);
    }

    private JSONObject editRigOwnerInfo(Connection connection, String oldRecord, String newRecord) {

        JSONObject jsonObject = new JSONObject();

        try (Statement statement = connection.createStatement()) {

            JSONObject oldRecordJSON = new JSONObject(oldRecord);
            JSONObject newRecordJSON = new JSONObject(newRecord);

            String query = "UPDATE ownertorig SET owner_id = " + newRecordJSON.getString("ownerID") +
                                ", start_date = '" + newRecordJSON.getString("startDate") + "', end_date = '" +
                                newRecordJSON.getString("endDate") + "' WHERE rig_id = " +
                                oldRecordJSON.getString("rigID") + " AND owner_id = " +
                                oldRecordJSON.getString("ownerID") + " AND start_date = '" +
                                oldRecordJSON.getString("startDate") + "'";
            statement.executeUpdate(query);
            jsonObject.put("message", "Success");

        } catch (JSONException e) {
            log.error("JSONException in editRigOwnerInfo: " + e.getMessage(),e);
        } catch (SQLException e) {
            log.error("SQLException in editRigOwnerInfo: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in editRigOwnerInfo: " + e.getMessage(),e);
        }
        return  jsonObject;
    }

    public ResponseEntity<Object> deleteRigLocation(String record) {

        Connection connection = null;
        JSONObject jsonObject = new JSONObject();
        Properties properties = new Properties();
        String configFileName = "application.properties";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                jsonObject = deleteRigLocationInfo(connection,record);
            } else {
                throw new Exception("Connection to Postgres failed");
            }

        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in deleteRigLocation: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in deleteRigLocation: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in deleteRigLocation: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in deleteRigLocation: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(jsonObject,HttpStatus.OK);
    }

    private JSONObject deleteRigLocationInfo(Connection connection, String record) {

        String message;
        String nextEndDate = null;
        String prevStartDate = null;
        String nextStartDate = null;
        Calendar cal = Calendar.getInstance();
        JSONObject jsonObject = new JSONObject();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try (Statement statement = connection.createStatement()) {

            JSONObject recordJSON = new JSONObject(record);

            String prevSelectQuery = "SELECT * FROM rigtoregion WHERE rig_id = " + recordJSON.getString("rigID") +
                            " AND start_date = (SELECT min(start_date) FROM rigtoregion WHERE rig_id = " +
                            recordJSON.getString("rigID") + " AND start_date > '" +
                            recordJSON.getString("startDate") + "')";
            ResultSet resultSet1 = statement.executeQuery(prevSelectQuery);

            while (resultSet1.next()) {
                prevStartDate = resultSet1.getString("start_date");
            }

            String nextSelectQuery = "SELECT * FROM rigtoregion WHERE rig_id = " + recordJSON.getString("rigID") +
                    " AND start_date = (SELECT max(start_date) FROM rigtoregion WHERE rig_id = " +
                    recordJSON.getString("rigID") + " AND start_date < '" +
                    recordJSON.getString("startDate") + "')";
            ResultSet resultSet2 = statement.executeQuery(nextSelectQuery);

            while (resultSet2.next()) {
                nextStartDate = resultSet2.getString("start_date");
                nextEndDate = resultSet2.getString("end_date");
            }

            if(prevStartDate == null || nextStartDate == null) {

                String deleteQuery = "DELETE FROM rigtoregion WHERE rig_id = " + recordJSON.getString("rigID") +
                        " AND region_id = " + recordJSON.getString("regionID") + " AND start_date = '" +
                        recordJSON.getString("startDate") + "'";
                int result = statement.executeUpdate(deleteQuery);

                if(result != 0) {
                    message = "Success";
                } else {
                    message = "Failure";
                }
            }
            else {

                Date oldDate = formatter.parse(nextEndDate);
                cal.setTime(oldDate);
                cal.add(Calendar.SECOND,1);
                Date newDate = cal.getTime();
                String newStartDate = formatter.format(newDate);

                String deleteQuery = "DELETE FROM rigtoregion WHERE rig_id = " + recordJSON.getString("rigID") +
                        " AND region_id = " + recordJSON.getString("regionID") + " AND start_date = '" +
                        recordJSON.getString("startDate") + "'";
                int result = statement.executeUpdate(deleteQuery);

                String updateQuery1 = "UPDATE rigtoregion SET start_date = '" + newStartDate + "' WHERE rig_id = " +
                        recordJSON.getString("rigID") + " AND start_date = '" + prevStartDate + "'";
                statement.executeUpdate(updateQuery1);

                if(result != 0) {
                    message = "Success";
                } else {
                    message = "Failure";
                }
            }

            jsonObject.put("message",message);

        } catch(JSONException e) {
            log.error("JSONException in deleteRigLocationInfo: " +e.getMessage(),e);
        } catch(SQLException e) {
            log.error("SQLException in deleteRigLocationInfo: " +e.getMessage(),e);
        } catch(Exception e) {
            log.error("Exception in deleteRigLocationInfo: " +e.getMessage(),e);
        }
        return jsonObject;
    }

    public ResponseEntity<Object> deleteRigOwner(String record) {

        Connection connection = null;
        JSONObject jsonObject = new JSONObject();
        Properties properties = new Properties();
        String configFileName = "application.properties";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            connection = connectToPostgres(properties);
            if (connection != null) {
                jsonObject = deleteRigOwnerInfo(connection,record);
            } else {
                throw new Exception("Connection to Postgres failed");
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in deleteRigOwner: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in deleteRigOwner: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in deleteRigOwner: " + e.getMessage(),e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in deleteRigOwner: " + e.getMessage(),e);
            }
        }
        return new ResponseEntity<>(jsonObject,HttpStatus.OK);
    }

    private JSONObject deleteRigOwnerInfo(Connection connection, String record) {

        String message;
        JSONObject jsonObject = new JSONObject();

        try (Statement statement = connection.createStatement()) {

            JSONObject recordJSON = new JSONObject(record);

            String query = "DELETE FROM ownertorig WHERE rig_id = " + recordJSON.getString("rigID") +
                            " AND owner_id = " + recordJSON.getString("ownerID") + " AND start_date = '" +
                            recordJSON.getString("startDate") + "'";
            int result = statement.executeUpdate(query);

            if(result != 0) {
                message = "Success";
            } else {
                message = "Failure";
            }
            jsonObject.put("message",message);

        } catch(JSONException e) {
            log.error("JSONException in deleteRigOwnerInfo: " +e.getMessage(),e);
        } catch(SQLException e) {
            log.error("SQLException in deleteRigOwnerInfo: " +e.getMessage(),e);
        } catch(Exception e) {
            log.error("Exception in deleteRigOwnerInfo: " +e.getMessage(),e);
        }
        return jsonObject;
    }
}
