package com.pten.scorecard.service;

import com.flutura.ImpalaRestApiController.UploadFileToPostgres;
import com.pten.scorecard.repo.ScorecardMaintenanceRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.json.JSONObject;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Service
public class ScorecardMaintenanceService {

    private static final Logger log = LoggerFactory.getLogger(ScorecardMaintenanceService.class);

    @Autowired
    private ScorecardMaintenanceRepo scrdMntRepo;

    ExecutorService executor = Executors.newFixedThreadPool(10);

    public ResponseEntity<Object> getFileTypeInfo(String year, String month) {

        ResponseEntity<Object> jsonObject = null;
        try {
            jsonObject = scrdMntRepo.getFileTypeInfo(year, month);
        } catch (Exception e) {
            log.error("Exception in getFileTypeInfo: " + e.getMessage(),e);
        }
        return jsonObject;
    }

    public ResponseEntity<Object> getFileInfo(String fileTypeID, String fileUploadID) {

        ResponseEntity<Object> jsonObject = null;
        try {
            jsonObject = scrdMntRepo.getFileInfo(fileTypeID, fileUploadID);
        } catch (Exception e) {
            log.error("Exception in getFileInfo: " + e.getMessage(),e);
        }
        return jsonObject;
    }

    public ResponseEntity<Object> getFileDetails(String fileTypeID, String fileUploadID, int startRow, int endRow) {

        ResponseEntity<Object> jsonObject = null;
        try {
            jsonObject = scrdMntRepo.getFileDetails(fileTypeID, fileUploadID, startRow, endRow);
        } catch (Exception e) {
            log.error("Exception in getFileDetails: " + e.getMessage(),e);
        }
        return jsonObject;
    }

    public ResponseEntity<Object> uploadSourceFile(String year, String month, int fileTypeID, MultipartFile file,
                                                   String uploadUser) {

        ResponseEntity<String> result;
        JSONObject jsonObject = new JSONObject();

        try {
            result = scrdMntRepo.uploadSourceFile(year, month, file);
            if (result.getBody().equals("File upload successful")) {
                Callable<JSONObject> newThread = new UploadFileToPostgres(year, month, file.getOriginalFilename(),
                        fileTypeID, uploadUser);
                Future<JSONObject> future = executor.submit(newThread);
                jsonObject = future.get();
                if (jsonObject.getInt("FileUploadID") == -1) {
                    throw new Exception("Uploading File to Postgres failed");
                } else {
                    return new ResponseEntity<>(jsonObject, HttpStatus.OK);
                }
            }
            else {
                throw new Exception(result.getBody());
            }
        } catch (Exception e) {
            log.error("Exception in uploadSourceFile: " + e.getMessage(),e);
            return new ResponseEntity<>(jsonObject, HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<Object> uploadSystemData(String year, String month, int fileTypeID, String uploadUser) {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject = scrdMntRepo.connectToExternalDatabase(year, month, fileTypeID, uploadUser);
            if (jsonObject.getInt("FileUploadID") == -1) {
                throw new Exception("System data upload to Postgres failed");
            } else {
                return new ResponseEntity<>(jsonObject, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("Exception in uploadSystemData: " + e.getMessage(),e);
            return new ResponseEntity<>(jsonObject, HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<Object> saveUpdatedData(String fileTypeID, String fileUploadID, String data) {

        ResponseEntity<Object> result;
        try {
            result = scrdMntRepo.saveUpdatedData(fileTypeID, fileUploadID, data);
            if (result.getBody().toString().equals("Update successful")) {
                return new ResponseEntity<>(result.getBody().toString(), HttpStatus.OK);
            } else {
                throw new Exception("Data update failed");
            }
        } catch (Exception e) {
            log.error("Exception in saveUpdatedData: " + e.getMessage(),e);
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<Object> filterData(String fileTypeID, String fileUploadID, String data, int startRow, int endRow) {

        ResponseEntity<Object> result = null;
        try {
            result = scrdMntRepo.filterData(fileTypeID, fileUploadID, data, startRow, endRow);
        } catch (Exception e) {
            log.error("Exception in filterData: " + e.getMessage(),e);
        }
        return new ResponseEntity<>(result.getBody().toString(), HttpStatus.OK);
    }
}

