package com.pten.scorecard.service;

import com.pten.Utilities.DbUtils.PtenInternalDbUtilLib;
import com.pten.scorecard.repo.CompetitionScoreCardRepo;
import com.pten.scorecard.repo.ScoreCardRepo;
import com.pten.scorecard.view.DetailGrid.*;
import com.pten.scorecard.view.Headers.RigScoreCardHeader;
import com.pten.scorecard.view.ScoreCard.ScoreCardRig;
import com.pten.ui.util.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.*;

@Service
public class CompetitionScoreCardService {


    private static final Logger log = LoggerFactory.getLogger(CompetitionScoreCardService.class);

    @Autowired
    Mapper mapper;

    @Autowired
    private CompetitionScoreCardRepo compScrdRepo;

    @Autowired
    private PtenInternalDbUtilLib utils;

    private List<String> sortOrder = Arrays.asList("HSE", "Rig Performance", "Equipment", "People", "Financial", "Data");

    public Collection getAllActiveScoreCards() {
        Collection result;
        try {
            result = compScrdRepo.getAllActiveScoreCards();
            return mapper.unmarshall(result, RigScoreCardHeader.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getScoreCard(String rigName, String date) {
        Collection result;
        try {
            result = compScrdRepo.getScoreCard(rigName, date);
            return mapper.unmarshall(result, RigScoreCardHeader.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getLatestScoreCardDate(){
        String Result;
        try {
            Result = compScrdRepo.getLatestReportDate().toString();
            return Result;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }


   public Collection getScoreCardDetails(String rigName, String date) {
        Collection result;
        ArrayList<ScoreCardDetail> sorted;
        try {
            result = compScrdRepo.getScoreCardDetails(rigName, date);
            sorted =  (ArrayList<ScoreCardDetail>)mapper.unmarshall(result, ScoreCardDetail.class);

            sorted.sort(Comparator.comparing(detail -> {
                int catIndex  = sortOrder.indexOf(detail.getCategory());
                detail.setCategory_order(catIndex + "_" + detail.getCategory());
                return catIndex;
            }));

            return sorted;

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getScoreCardDetails(int id) {
        Collection result;
        ArrayList<ScoreCardDetail> sorted;
        try {
            result = compScrdRepo.getScoreCardDetails(id);
            sorted =  (ArrayList<ScoreCardDetail>)mapper.unmarshall(result, ScoreCardDetail.class);

            sorted.sort(Comparator.comparing( detail -> {
                int catIndex  = sortOrder.indexOf(detail.getCategory());
                detail.setCategory_order(catIndex + "_" + detail.getCategory());
                return catIndex;
            }));

            return sorted;

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getScoreCardRigList(String date) {
        Collection result;
        try {
            result = compScrdRepo.getScoreCardrigList(date);
            return mapper.unmarshall(result, ScoreCardRig.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getTopScoreCard(String date) {
        Collection result;
        try {
            result = compScrdRepo.getTopScoreCard(date);
            return mapper.unmarshall(result, ScoreCardRig.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }


    public Collection getScoreCardTrend(int scorecard_id) {
        Collection result;
        try {
            result = compScrdRepo.getScoreCardTrend(scorecard_id);
            return mapper.unmarshall(result, RigScoreCardHeader.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
