package com.pten.scorecard.service;

import com.pten.Utilities.DbUtils.PtenInternalDbUtilLib;
import com.pten.scorecard.repo.OpsManagerScoreCardRepo;
import com.pten.scorecard.view.DetailGrid.OpsManagerDetailGrid.*;
import com.pten.scorecard.view.DetailGrid.SuperintendentScoreCardDetail;
import com.pten.scorecard.view.Headers.OpsManagerScoreCardHeader;
import com.pten.scorecard.view.ScoreCard.ScoreCardOpsManager;
import com.pten.scorecard.view.ScoreCard.ScoreCardRigSuperintendent;
import com.pten.ui.util.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class OpsManagerScoreCardService {

    private static final Logger log = LoggerFactory.getLogger(OpsManagerScoreCardService.class);

    @Autowired
    Mapper mapper;

    @Autowired
    private OpsManagerScoreCardRepo opsScrdRepo;

    @Autowired
    private PtenInternalDbUtilLib utils;



    public String getLatestScoreCardDate(){
        String Result;
        try {
            Result = opsScrdRepo.getLatestReportDate().toString();
            return Result;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getOpsManagerList(String date) {
        Collection result;
        try {
            result = opsScrdRepo.getOpsManagerList(date);
            return mapper.unmarshall(result, ScoreCardOpsManager.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getScoreCard(int id) {
        Collection result;
        try {
            result = opsScrdRepo.getScoreCard(id);
            return mapper.unmarshall(result, OpsManagerScoreCardHeader.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getSuperintendentRigScoreCardIds(Collection<Integer> ids) {
        Collection result;
        try {
            result =  opsScrdRepo.getSuperintendentScoreCardDetails(ids);
            //get the records for all of the superintedents
            Collection<SuperintendentScoreCardDetail> superintendentScoreCards =  mapper.unmarshall(result, SuperintendentScoreCardDetail.class);

            //get an array for each set of ids and combine them
            Collection<Integer> current_ids = superintendentScoreCards.stream().map(sc -> sc.getCurrent_month_scorecard_id()).collect(Collectors.toList());
            Collection<Integer> prior_ids = superintendentScoreCards.stream().map(sc -> sc.getPrior_month_scorecard_id()).collect(Collectors.toList());
            Collection<Integer> twomo_ids = superintendentScoreCards.stream().map(sc -> sc.getTwo_month_scorecard_id()).collect(Collectors.toList());

            Collection<Integer> someIds = Stream.concat(Stream.concat(current_ids.stream(), prior_ids.stream()), twomo_ids.stream()).collect(Collectors.toList());

            //get a list of all of the superintendent rig pairs
            result = opsScrdRepo.getSuperintendentRigScoreCardIds(someIds);

            return mapper.unmarshall(result, ScoreCardRigSuperintendent.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getHseEventDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = opsScrdRepo.getImsHseEventDetails(ids);
            return mapper.unmarshall(result, OpsManHseEventDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getImsOpenedDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = opsScrdRepo.getImsOpenedDetails(ids);
            return mapper.unmarshall(result, OpsManHseEventDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getImsClosedDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = opsScrdRepo.getImsClosedDetails(ids);
            return mapper.unmarshall(result, OpsManHseEventDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getCRIDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = opsScrdRepo.getCRIDetails(ids);
            return mapper.unmarshall(result, OpsManagerCRIDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getTripSpeedDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = opsScrdRepo.getTripSpeedDetails(ids);
            return mapper.unmarshall(result, OpsManagerGenericRigMetricDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getTripConnDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = opsScrdRepo.getTripConnDetails(ids);
            return mapper.unmarshall(result, OpsManagerGenericRigMetricDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getDrillConnDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = opsScrdRepo.getDrillConnDetails(ids);
            return mapper.unmarshall(result, OpsManagerGenericRigMetricDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getWalkDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = opsScrdRepo.getWalkDetails(ids);
            return mapper.unmarshall(result, OpsManagerWalkDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getRmctDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = opsScrdRepo.getRmctDetails(ids);
            return mapper.unmarshall(result, OpsManagerRMCTDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getTibWoDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = opsScrdRepo.getTibWoDetails(ids);
            return mapper.unmarshall(result, OpsManWorkOrderDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getTechWoDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = opsScrdRepo.getTechWoDetails(ids);
            return mapper.unmarshall(result, OpsManWorkOrderDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getOpsWoDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = opsScrdRepo.getOpsWoDetails(ids);
            return mapper.unmarshall(result, OpsManWorkOrderDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getAssetInvDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = opsScrdRepo.getAssetInveDetails(ids);
            return mapper.unmarshall(result, OpsManagerAssetInvDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getTurnOverDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = opsScrdRepo.getTurnoverDetails(ids);
            return mapper.unmarshall(result, OpsManagerTurnoverDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getCapexYearlyDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = opsScrdRepo.getCapexYearlyDetails(ids);
            return mapper.unmarshall(result, OpsManagerCapexDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getCMRErrorDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = opsScrdRepo.getCMRErrorDetails(ids);
            return mapper.unmarshall(result, OpsManagerCMRErrorDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getTopScoreCard(String date) {
        Collection result;
        try {
            result = opsScrdRepo.getTopScoreCard(date);
            return mapper.unmarshall(result, ScoreCardOpsManager.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

}
