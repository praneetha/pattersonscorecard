package com.pten.scorecard.service;

import com.pten.scorecard.repo.ScorecardGenerationRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ScorecardGenerationService {

    private static final Logger log = LoggerFactory.getLogger(ScorecardMaintenanceService.class);

    @Autowired
    private ScorecardGenerationRepo scrdGnrRepo = new ScorecardGenerationRepo();

    public ArrayList<String> getScorecardDate() {

        ArrayList<String> result = new ArrayList<>();
        try {
            result = scrdGnrRepo.getScorecardDate();
        } catch (Exception e) {
            log.error("Exception in getScorecardDate: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> getScorecardRevision(String scDate) {

        ResponseEntity<Object> result = null;
        try {
            result = scrdGnrRepo.getScorecardRevision(scDate);
        } catch (Exception e) {
            log.error("Exception in getScorecardRevision: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> getRigPersonMetaData() {

        ResponseEntity<Object> result = null;
        try {
            result = scrdGnrRepo.getRigPersonMetaData();
        } catch (Exception e) {
            log.error("Exception in getRigPersonMetaData: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> getRigPersonData(String scDate, String type, String scRevision) {

        ResponseEntity<Object> result = null;
        try {
            result = scrdGnrRepo.getRigPersonData(scDate, type, scRevision);
        } catch (Exception e) {
            log.error("Exception in getRigPersonMetaData: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> getGenerateMeta() {

        ResponseEntity<Object> result = null;
        try {
            result = scrdGnrRepo.getGenerateMeta();
        } catch (Exception e) {
            log.error("Exception in getGenerateMeta: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> getGenerateData(String scDate, String type) {

        ResponseEntity<Object> result = null;
        try {
            result = scrdGnrRepo.getGenerateData(scDate, type);
        } catch (Exception e) {
            log.error("Exception in getGenerateData: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> generateScorecard(String scDate, String data) {

        int result = 0;
        try {
            result = scrdGnrRepo.generateScorecard(scDate, data);
        } catch (Exception e) {
            log.error("Exception in generateScorecard: " + e.getMessage(),e);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    public ResponseEntity<Object> publishScorecard(String scDate) {

        int result = 0;
        try {
            result = scrdGnrRepo.publishScorecard(scDate);
        } catch (Exception e) {
            log.error("Exception in publishScorecard: " + e.getMessage(),e);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    public ResponseEntity<Object> unPublishScorecard(String scDate, String scRevision) {

        int result = 0;
        try {
            result = scrdGnrRepo.unPublishScorecard(scDate,scRevision);
        } catch (Exception e) {
            log.error("Exception in unPublishScorecard: " + e.getMessage(),e);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    public ResponseEntity<Object> archiveScorecard(String scDate, String scRevision) {

        int result = 0;
        try {
            result = scrdGnrRepo.archiveScorecard(scDate, scRevision);
        } catch (Exception e) {
            log.error("Exception in archiveScorecard: " + e.getMessage(),e);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    public String exportScorecard(String scDate, String scTab) {

        StringBuilder result = new StringBuilder();
        try {
            result = scrdGnrRepo.exportScorecard(scDate,scTab);
        } catch (Exception e) {
            log.error("Exception in exportScorecard: " + e.getMessage(),e);
        }
        return result.toString();
    }
}
