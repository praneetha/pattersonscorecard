package com.pten.scorecard.service;

import com.pten.Utilities.DbUtils.PtenInternalDbUtilLib;
import com.pten.scorecard.repo.ScoreCardSummaryRepo;
import com.pten.scorecard.view.SummaryGrid.*;
import com.pten.scorecard.view.SummaryTrend.*;
import com.pten.ui.util.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Collection;
import java.util.Date;

@Service
public class ScoreCardSummaryService {

    private static final Logger log = LoggerFactory.getLogger(ScoreCardService.class);

    @Autowired
    Mapper mapper;

    @Autowired
    private ScoreCardSummaryRepo scrdSumRepo;

    @Autowired

    private PtenInternalDbUtilLib utils;

    public String getLatestDate() {
        String Result;
        try {
            Result = scrdSumRepo.getLatestDate().toString();
            return Result;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getSummaryRigs(String date) {
        Collection result;
        try {
            result = scrdSumRepo.getScoreCardrigList(date);
            return mapper.unmarshall(result, OverallGrid.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getSummaryRigsById(Collection<Integer> ids) {
        Collection result;
        try {
            result = scrdSumRepo.getScoreCardrigList(ids);
            return mapper.unmarshall(result, OverallGrid.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getSummaryTrend(String date) {
        Collection result;
        try {
            result = scrdSumRepo.getOverallTrend(date);
            return mapper.unmarshall(result, OverallTrend.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getScoreCardRigPerformance(String date) {
        Collection result;
        try {
            result = scrdSumRepo.getScoreCardsRigPerformance(date);
            return mapper.unmarshall(result, RigPerformanceGrid.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getScoreCardRigPerformanceTrend(String date) {
        Collection result;
        try {
            result = scrdSumRepo.getRigPerformanceTrend(date);
            return mapper.unmarshall(result, RigPerfromanceTrend.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getScoreCardHSE(String date) {
        Collection result;
        try {
            result = scrdSumRepo.getScoreCardsHSE(date);
            return mapper.unmarshall(result, HSEGrid.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getScoreCardHSETrend(String date) {
        Collection result;
        try {
            result = scrdSumRepo.getHSETrend(date);
            return mapper.unmarshall(result, HSETrend.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getScoreCardEquipment(Date date) {
        return mapper.unmarshall(scrdSumRepo.getScoreCardEquipment(date), Equipment.class);
    }

    public Collection getScoreCardEquipmentTrend(String date) {
        Collection result;
        try {
            result = scrdSumRepo.getScoreCardEquipmentTrend(date);
            return mapper.unmarshall(result, EquipmentTrend.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getScoreCardFinancial(String date) {
        Collection result;
        try {
            result = scrdSumRepo.getScoreCardFinancial(date);
            return mapper.unmarshall(result, FinancialGrid.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getScoreCardFinancialTrend(String date) {
        Collection result;
        try {
            result = scrdSumRepo.getFinancialTrend(date);
            return mapper.unmarshall(result, FinancialTrend.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getScoreCardPeopleData(String date) {
        Collection result;
        try {
            result = scrdSumRepo.getScoreCardPeopleData(date);
            return mapper.unmarshall(result, PeopleDataGrid.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getScoreCardPeopleDataTrend(String date) {
        Collection result;
        try {
            result = scrdSumRepo.getPeopleDataTrend(date);
            return mapper.unmarshall(result, PeopleDataTrend.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

}
