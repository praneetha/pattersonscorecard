package com.pten.scorecard.service;

import com.pten.scorecard.repo.ScorecardRigManagerRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ScorecardRigManagerService {

    private static final Logger log = LoggerFactory.getLogger(ScorecardRigManagerService.class);

    @Autowired
    private ScorecardRigManagerRepo scrdRigMgrRepo = new ScorecardRigManagerRepo();

    public ResponseEntity<Object> getRigList() {

        ResponseEntity<Object> result = null;
        try {
            result = scrdRigMgrRepo.getRigList();
        } catch (Exception e) {
            log.error("Exception in getRigList: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> getRigTypeList() {

        ResponseEntity<Object> result = null;
        try {
            result = scrdRigMgrRepo.getRigTypeList();
        } catch (Exception e) {
            log.error("Exception in getRigTypeList: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> getRigMasterTypeList() {

        ResponseEntity<Object> result = null;
        try {
            result = scrdRigMgrRepo.getRigMasterTypeList();
        } catch (Exception e) {
            log.error("Exception in getRigMasterTypeList: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> getRegionList() {

        ResponseEntity<Object> result = null;
        try {
            result = scrdRigMgrRepo.getRegionList();
        } catch (Exception e) {
            log.error("Exception in getRegionList: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> getOwnerList() {

        ResponseEntity<Object> result = null;
        try {
            result = scrdRigMgrRepo.getOwnerList();
        } catch (Exception e) {
            log.error("Exception in getOwnerList: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> getLocationByRig(int rigID) {

        ResponseEntity<Object> result = null;
        try {
            result = scrdRigMgrRepo.getLocationByRig(rigID);
        } catch (Exception e) {
            log.error("Exception in getLocationByRig: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> getSuperintendentByRig(int rigID) {

        ResponseEntity<Object> result = null;
        try {
            result = scrdRigMgrRepo.getSuperintendentByRig(rigID);
        } catch (Exception e) {
            log.error("Exception in getSuperintendentByRig: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> addNewRig(String rigName, int rigTypeID) {

        ResponseEntity<Object> result = null;
        try {
            result = scrdRigMgrRepo.addNewRig(rigName,rigTypeID);
        } catch (Exception e) {
            log.error("Exception in addNewRig: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> addRigLocation(int rigID, int regionID, String startDate, String endDate) {

        ResponseEntity<Object> result = null;
        try {
            result = scrdRigMgrRepo.addRigLocation(rigID,regionID,startDate, endDate);
        } catch (Exception e) {
            log.error("Exception in addRigLocation: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> addRigOwner(int rigID, int ownerID, String startDate) {

        ResponseEntity<Object> result = null;
        try {
            result = scrdRigMgrRepo.addRigOwner(rigID,ownerID,startDate);
        } catch (Exception e) {
            log.error("Exception in addRigOwner: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> editRigLocation(String oldRecord, String newRecord) {

        ResponseEntity<Object> result = null;
        try {
            result = scrdRigMgrRepo.editRigLocation(oldRecord,newRecord);
        } catch (Exception e) {
            log.error("Exception in editRigLocation: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> editRigOwner(String oldRecord, String newRecord) {

        ResponseEntity<Object> result = null;
        try {
            result = scrdRigMgrRepo.editRigOwner(oldRecord,newRecord);
        } catch (Exception e) {
            log.error("Exception in editRigOwner: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> deleteRigLocation(String record) {

        ResponseEntity<Object> result = null;
        try {
            result = scrdRigMgrRepo.deleteRigLocation(record);
        } catch (Exception e) {
            log.error("Exception in deleteRigLocation: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> deleteRigOwner(String record) {

        ResponseEntity<Object> result = null;
        try {
            result = scrdRigMgrRepo.deleteRigOwner(record);
        } catch (Exception e) {
            log.error("Exception in deleteRigOwner: " + e.getMessage(),e);
        }
        return result;
    }
}
