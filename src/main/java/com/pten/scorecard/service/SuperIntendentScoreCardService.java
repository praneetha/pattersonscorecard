package com.pten.scorecard.service;

import com.pten.Utilities.DbUtils.PtenInternalDbUtilLib;
import com.pten.scorecard.repo.SuperintendentScoreCardRepo;
import com.pten.scorecard.view.DetailGrid.SuperintendentScoreCardDetail;
import com.pten.scorecard.view.Headers.RigScoreCardHeader;
import com.pten.scorecard.view.Headers.SuperintendentScoreCardHeader;
import com.pten.scorecard.view.ScoreCard.MultiSeriesTrend;
import com.pten.scorecard.view.ScoreCard.ScoreCardSuperintendenent;
import com.pten.ui.util.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.*;


@Service
public class SuperIntendentScoreCardService {

    private static final Logger log = LoggerFactory.getLogger(SuperIntendentScoreCardService.class);

    @Autowired
    Mapper mapper;

    @Autowired
    private SuperintendentScoreCardRepo sprScrdRepo;

    @Autowired
    private PtenInternalDbUtilLib utils;

    private List<String> sortOrder = Arrays.asList("HSE", "Rig Performance", "Equipment", "People", "Financial", "Data");

    public String getLatestScoreCardDate(){
        String Result;
        try {
            Result = sprScrdRepo.getLatestReportDate().toString();
            return Result;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getSuperintendentList(String date) {
        Collection result;
        try {
            result = sprScrdRepo.getSuperintendentList(date);
            return mapper.unmarshall(result, ScoreCardSuperintendenent.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getScoreCard(String superintendent, String date) {
        Collection result;
        try {
            result = sprScrdRepo.getScoreCard(superintendent, date);
            return mapper.unmarshall(result, SuperintendentScoreCardHeader.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getScoreCard(int id) {
        Collection result;
        try {
            result = sprScrdRepo.getScoreCard(id);
            return mapper.unmarshall(result, SuperintendentScoreCardHeader.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getScoreCardTrend(int id) {
        Collection result;
        try {
            result = sprScrdRepo.getScoreCardTrend(id);
            return mapper.unmarshall(result, MultiSeriesTrend.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getScoreCardDetails(int id) {
        Collection result;
        ArrayList<SuperintendentScoreCardDetail> sorted;
        try {
            result = sprScrdRepo.getScoreCardDetails(id);
            sorted =  (ArrayList<SuperintendentScoreCardDetail>)mapper.unmarshall(result, SuperintendentScoreCardDetail.class);

            sorted.sort(Comparator.comparing( detail -> {
                int catIndex  = sortOrder.indexOf(detail.getCategory());
                detail.setCategory_order(catIndex + "_" + detail.getCategory());
                return catIndex;
            }));

            return sorted;

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getScoreCardDetails(String rigName, String date) {
        Collection result;
        ArrayList<SuperintendentScoreCardDetail> sorted;
        try {
            result = sprScrdRepo.getScoreCardDetails(rigName, date);
            sorted =  (ArrayList<SuperintendentScoreCardDetail>)mapper.unmarshall(result, SuperintendentScoreCardDetail.class);

            sorted.sort(Comparator.comparing( detail -> {
                int catIndex  = sortOrder.indexOf(detail.getCategory());
                detail.setCategory_order(catIndex + "_" + detail.getCategory());
                return catIndex;
            }));

            return sorted;

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getRigScoreCardIds(Collection<String> rigs, String date) {
        Collection result;
        try {
            result =  sprScrdRepo.getRigScoreCardIds(rigs, date);
            return mapper.unmarshall(result, RigScoreCardHeader.class);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getRigScoreCardIds(Collection<Integer> ids) {
        Collection result;
        try {
            result =  sprScrdRepo.getRigScoreCardIds(ids);
            return mapper.unmarshall(result, RigScoreCardHeader.class);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getTopScoreCard(String date) {
        Collection result;
        try {
            result = sprScrdRepo.getTopScoreCard(date);
            return mapper.unmarshall(result, ScoreCardSuperintendenent.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }



}
