package com.pten.scorecard.service;

import com.pten.Utilities.DbUtils.PtenInternalDbUtilLib;
import com.pten.scorecard.repo.ScoreCardRepo;
import com.pten.scorecard.view.DetailGrid.*;
import com.pten.scorecard.view.Headers.RigScoreCardHeader;
import com.pten.scorecard.view.ScoreCard.ScoreCardRig;
import com.pten.ui.util.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.*;


@Service
public class ScoreCardService {

    private static final Logger log = LoggerFactory.getLogger(ScoreCardService.class);

    @Autowired
    Mapper mapper;

    @Autowired
    private ScoreCardRepo scrdRepo;

    @Autowired
    private PtenInternalDbUtilLib utils;

    private List<String> sortOrder = Arrays.asList("HSE", "Rig Performance", "Equipment", "People", "Financial", "Data");

    public Collection getAllActiveScoreCards() {
        Collection result;
        try {
            result = scrdRepo.getAllActiveScoreCards();
            return mapper.unmarshall(result, RigScoreCardHeader.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getScoreCard(String rigName, String date) {
        Collection result;
        try {
            result = scrdRepo.getScoreCard(rigName, date);
            return mapper.unmarshall(result, RigScoreCardHeader.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getLatestScoreCardDate(){
        String Result;
        try {
            Result = scrdRepo.getLatestReportDate().toString();
            return Result;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }


    public Collection getScoreCardDetails(String rigName, String date) {
        Collection result;
        ArrayList<ScoreCardDetail> sorted;
        try {
            result = scrdRepo.getScoreCardDetails(rigName, date);
            sorted =  (ArrayList<ScoreCardDetail>)mapper.unmarshall(result, ScoreCardDetail.class);

            sorted.sort(Comparator.comparing( detail -> {
                int catIndex  = sortOrder.indexOf(detail.getCategory());
                detail.setCategory_order(catIndex + "_" + detail.getCategory());
                return catIndex;
            }));

            return sorted;

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getScoreCardDetails(int id) {
        Collection result;
        ArrayList<ScoreCardDetail> sorted;
        try {
            result = scrdRepo.getScoreCardDetails(id);
            sorted =  (ArrayList<ScoreCardDetail>)mapper.unmarshall(result, ScoreCardDetail.class);

            sorted.sort(Comparator.comparing( detail -> {
                int catIndex  = sortOrder.indexOf(detail.getCategory());
                detail.setCategory_order(catIndex + "_" + detail.getCategory());
                return catIndex;
            }));

            return sorted;

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getScoreCardRigList(String date) {
        Collection result;
        try {
            result = scrdRepo.getScoreCardrigList(date);
            return mapper.unmarshall(result, ScoreCardRig.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getTopScoreCard(String date) {
        Collection result;
        try {
            result = scrdRepo.getTopScoreCard(date);
            return mapper.unmarshall(result, ScoreCardRig.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }


    public Collection getCRIDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = scrdRepo.getCRIDetails(ids);
            return mapper.unmarshall(result, CRIDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getTripSpeedDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = scrdRepo.getTripSpeedDetails(ids);
            return mapper.unmarshall(result, GenericRigMetricDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getTripConnDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = scrdRepo.getTripConnDetails(ids);
            return mapper.unmarshall(result, GenericRigMetricDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getDrillConnDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = scrdRepo.getDrillConnDetails(ids);
            return mapper.unmarshall(result, GenericRigMetricDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getAssetInvDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = scrdRepo.getAssetInveDetails(ids);
            return mapper.unmarshall(result, AssetInvDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getTurnOverDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = scrdRepo.getTurnoverDetails(ids);
            return mapper.unmarshall(result, TurnoverDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getTibWoDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = scrdRepo.getTibWoDetails(ids);
            return mapper.unmarshall(result, WorkOrderDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getTechWoDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = scrdRepo.getTechWoDetails(ids);
            return mapper.unmarshall(result, WorkOrderDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getOpsWoDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = scrdRepo.getOpsWoDetails(ids);
            return mapper.unmarshall(result, WorkOrderDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getCMRErrorDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = scrdRepo.getCMRErrorDetails(ids);
            return mapper.unmarshall(result, CMRErrorDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getImsOpenedDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = scrdRepo.getImsOpenedDetails(ids);
            return mapper.unmarshall(result, HseEventDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getImsClosedDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = scrdRepo.getImsClosedDetails(ids);
            return mapper.unmarshall(result, HseEventDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getHseEventDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = scrdRepo.getImsHseEventDetails(ids);
            return mapper.unmarshall(result, HseEventDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getCapexMonthlyDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = scrdRepo.getCapexMonthlyDetails(ids);
            return mapper.unmarshall(result, CapexDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getCapexYearlyDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = scrdRepo.getCapexYearlyDetails(ids);
            return mapper.unmarshall(result, CapexDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getWalkDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = scrdRepo.getWalkDetails(ids);
            return mapper.unmarshall(result, WalkDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection getRmctDetails(Collection<Integer> ids) {
        Collection result;
        try {
            result = scrdRepo.getRmctDetails(ids);
            return mapper.unmarshall(result, RMCTDetail.class);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}