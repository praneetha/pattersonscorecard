package com.pten.scorecard.service;

import com.pten.scorecard.repo.ScorecardPeopleManagerRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ScorecardPeopleManagerService {

    private static final Logger log = LoggerFactory.getLogger(ScorecardPeopleManagerService.class);

    @Autowired
    private ScorecardPeopleManagerRepo scrdPplMgrRepo = new ScorecardPeopleManagerRepo();

    public ResponseEntity<Object> getOwnerList() {

        ResponseEntity<Object> result = null;
        try {
            result = scrdPplMgrRepo.getOwnerList();
        } catch (Exception e) {
            log.error("Exception in getOwnerList: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> getOwnerTypeList() {

        ResponseEntity<Object> result = null;
        try {
            result = scrdPplMgrRepo.getOwnerTypeList();
        } catch (Exception e) {
            log.error("Exception in getOwnerTypeList: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> getLocationByOwner(int ownerID) {

        ResponseEntity<Object> result = null;
        try {
            result = scrdPplMgrRepo.getLocationByOwner(ownerID);
        } catch (Exception e) {
            log.error("Exception in getLocationByOwner: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> getSuperintendentByOwner(int ownerID) {

        ResponseEntity<Object> result = null;
        try {
            result = scrdPplMgrRepo.getSuperintendentByOwner(ownerID);
        } catch (Exception e) {
            log.error("Exception in getSuperintendentByOwner: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> addNewOwner(String ownerName,int ownerTypeID,String empNum) {

        ResponseEntity<Object> result = null;
        try {
            result = scrdPplMgrRepo.addNewOwner(ownerName,ownerTypeID,empNum);
        } catch (Exception e) {
            log.error("Exception in addNewOwner: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> addOwnerLocation(int ownerID,int regionID,String startDate, String endDate) {

        ResponseEntity<Object> result = null;
        try {
            result = scrdPplMgrRepo.addOwnerLocation(ownerID,regionID,startDate,endDate);
        } catch (Exception e) {
            log.error("Exception in addOwnerLocation: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> addOwnerOwner(int ownerID,int ownedOwnerID,String startDate) {

        ResponseEntity<Object> result = null;
        try {
            result = scrdPplMgrRepo.addOwnerOwner(ownerID,ownedOwnerID,startDate);
        } catch (Exception e) {
            log.error("Exception in addOwnerOwner: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> editOwnerLocation(String oldRecord, String newRecord) {

        ResponseEntity<Object> result = null;
        try {
            result = scrdPplMgrRepo.editOwnerLocation(oldRecord,newRecord);
        } catch (Exception e) {
            log.error("Exception in editOwnerLocation: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> editOwnerOwner(String oldRecord, String newRecord) {

        ResponseEntity<Object> result = null;
        try {
            result = scrdPplMgrRepo.editOwnerOwner(oldRecord,newRecord);
        } catch (Exception e) {
            log.error("Exception in editOwnerOwner: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> deleteOwnerLocation(String record) {

        ResponseEntity<Object> result = null;
        try {
            result = scrdPplMgrRepo.deleteOwnerLocation(record);
        } catch (Exception e) {
            log.error("Exception in deleteOwnerLocation: " + e.getMessage(),e);
        }
        return result;
    }

    public ResponseEntity<Object> deleteOwnerOwner(String record) {

        ResponseEntity<Object> result = null;
        try {
            result = scrdPplMgrRepo.deleteOwnerOwner(record);
        } catch (Exception e) {
            log.error("Exception in deleteOwnerOwner: " + e.getMessage(),e);
        }
        return result;
    }
}
