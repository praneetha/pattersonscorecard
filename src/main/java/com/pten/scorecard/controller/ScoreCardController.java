package com.pten.scorecard.controller;

import com.pten.scorecard.service.ScoreCardService;
import com.pten.ui.util.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(value = {"/assetReport/scorecard"})
public class ScoreCardController {

    @Autowired
    Mapper mapper;

    @Autowired
    private ScoreCardService scrdService;

    private static final Logger log = LoggerFactory.getLogger(ScoreCardController.class);

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/allactivescorecards", produces = "application/json")
    @ResponseBody
    public Collection activeScoreCards() {
        return scrdService.getAllActiveScoreCards();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rigscorecard", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCard(@RequestParam("rig") String rigName,
                                   @RequestParam("date") String date) {
        return scrdService.getScoreCard(rigName, date);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rigscorecarddetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardDetails(@RequestParam("rig") String rigName,
                                   @RequestParam("date") String date) {
        return scrdService.getScoreCardDetails(rigName, date);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rigscorecarddetailsbyid", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardDetails(@RequestParam("id") int id) {
        return scrdService.getScoreCardDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/riglist", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardRigList( @RequestParam("date") String date) {
        return scrdService.getScoreCardRigList(date);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/topranked", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardTopRanked( @RequestParam("date") String date) {
        return scrdService.getTopScoreCard(date);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cridetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardCRIDetails(@RequestParam("id") Collection<Integer> id) {
        return scrdService.getCRIDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/tripspeeddetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardTripSpeedDetails(@RequestParam("id") Collection<Integer> id) {
        return scrdService.getTripSpeedDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/tripconndetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardTripConnDetails(@RequestParam("id") Collection<Integer> id) {
        return scrdService.getTripConnDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/drillconndetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardDrillConnDetails(@RequestParam("id") Collection<Integer> id) {
        return scrdService.getDrillConnDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/assetinvdetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardAssetInvDetails(@RequestParam("id") Collection<Integer> id) {
        return scrdService.getAssetInvDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/turnoverdetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardTurnoverDetails(@RequestParam("id") Collection<Integer> id) {
        return scrdService.getTurnOverDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/tibwodetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardTibWoDetails(@RequestParam("id") Collection<Integer> id) {
        return scrdService.getTibWoDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/techwodetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardTechWoDetails(@RequestParam("id") Collection<Integer> id) {
        return scrdService.getTechWoDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/opswodetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardOpsWoDetails(@RequestParam("id") Collection<Integer> id) {
        return scrdService.getOpsWoDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cmrerrordetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardCMRErrorDetails(@RequestParam("id") Collection<Integer> id) {
        return scrdService.getCMRErrorDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/imsopeneddetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardImsOpenedDetails(@RequestParam("id") Collection<Integer> id) {
        return scrdService.getImsOpenedDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/imscloseddetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardImsClosedDetails(@RequestParam("id") Collection<Integer> id) {
        return scrdService.getImsClosedDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/hseevnetdetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardHseEventDetails(@RequestParam("id") Collection<Integer> id) {
        return scrdService.getHseEventDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/capexmonthlydetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardCapexMonthlyDetails(@RequestParam("id") Collection<Integer> id) {
        return scrdService.getCapexMonthlyDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/capexyearlydetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardCapexYearlyDetails(@RequestParam("id") Collection<Integer> id) {
        return scrdService.getCapexYearlyDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/walkdetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardWalkDetails(@RequestParam("id") Collection<Integer> id) {
        return scrdService.getWalkDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rmctdetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardRmctDetails(@RequestParam("id") Collection<Integer> id) {
        return scrdService.getRmctDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/latestdate", produces = "application/json")
    @ResponseBody
    public String rigScoreCardlatestDate() {
        return scrdService.getLatestScoreCardDate();
    }


}