package com.pten.scorecard.controller;

import com.pten.scorecard.service.ScorecardRigManagerService;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@ControllerAdvice
@RequestMapping(value = {"/assetReport/scorecard/rigmapping/"})

public class ScorecardRigManagerController {

    @Autowired
    private ScorecardRigManagerService scrdRigMgrService = new ScorecardRigManagerService();

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/getRigList", produces = "application/json")
    @ResponseBody
    public String getRigList() {

        ResponseEntity<Object> result;
        result = scrdRigMgrService.getRigList();
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/getRigTypeList", produces = "application/json")
    @ResponseBody
    public String getRigTypeList() {

        ResponseEntity<Object> result;
        result = scrdRigMgrService.getRigTypeList();
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/getRigMasterTypeList", produces = "application/json")
    @ResponseBody
    public String getRigMasterTypeList() {

        ResponseEntity<Object> result;
        result = scrdRigMgrService.getRigMasterTypeList();
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/getRegionList", produces = "application/json")
    @ResponseBody
    public String getRegionList() {

        ResponseEntity<Object> result;
        result = scrdRigMgrService.getRegionList();
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/getOwnerList", produces = "application/json")
    @ResponseBody
    public String getOwnerList() {

        ResponseEntity<Object> result;
        result = scrdRigMgrService.getOwnerList();
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/getLocationByRig", produces = "application/json")
    @ResponseBody
    public String getLocationByRig(@RequestBody String body) throws JSONException {

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        int rigID = jsonOb.getInt("rigID");
        result = scrdRigMgrService.getLocationByRig(rigID);
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/getSuperintendentByRig", produces = "application/json")
    @ResponseBody
    public String getSuperintendentByRig(@RequestBody String body) throws JSONException {

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        int rigID = jsonOb.getInt("rigID");
        result = scrdRigMgrService.getSuperintendentByRig(rigID);
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/addNewRig", produces = "application/json")
    @ResponseBody
    public String addNewRig(@RequestBody String body) throws JSONException {

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        String rigName = jsonOb.getString("rigName");
        int rigTypeID = jsonOb.getInt("rigTypeID");
        result = scrdRigMgrService.addNewRig(rigName,rigTypeID);
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/addRigLocation", produces = "application/json")
    @ResponseBody
    public String addRigLocation(@RequestBody String body) throws JSONException{

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        int rigID = jsonOb.getInt("rigID");
        int regionID = jsonOb.getInt("regionID");
        String startDate = jsonOb.getString("startDate");
        String endDate = jsonOb.getString("endDate");
        System.out.println(startDate);
        result = scrdRigMgrService.addRigLocation(rigID,regionID,startDate,endDate);
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/addRigOwner", produces = "application/json")
    @ResponseBody
    public String addRigOwner(@RequestBody String body) throws JSONException{

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        int rigID = jsonOb.getInt("rigID");
        int ownerID = jsonOb.getInt("ownerID");
        String startDate = jsonOb.getString("startDate");
        result = scrdRigMgrService.addRigOwner(rigID,ownerID,startDate);
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/editRigLocation", produces = "application/json")
    @ResponseBody
    public String editRigLocation(@RequestBody String body) throws JSONException{

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        String locationInfo = jsonOb.getString("locationInfo");
        JSONObject editInfo = new JSONObject(locationInfo);
        String oldRecord = editInfo.getString("oldRecord");
        String newRecord = editInfo.getString("newRecord");
        result = scrdRigMgrService.editRigLocation(oldRecord,newRecord);
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/editRigOwner", produces = "application/json")
    @ResponseBody
    public String editRigOwner(@RequestBody String body) throws JSONException{

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        String personInfo = jsonOb.getString("personInfo");
        JSONObject editInfo = new JSONObject(personInfo);
        String oldRecord = editInfo.getString("oldRecord");
        String newRecord = editInfo.getString("newRecord");
        result = scrdRigMgrService.editRigOwner(oldRecord,newRecord);
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/deleteRigLocation", produces = "application/json")
    @ResponseBody
    public String deleteRigLocation(@RequestBody String body) throws JSONException{

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        String locationInfo = jsonOb.getString("record");
        result = scrdRigMgrService.deleteRigLocation(locationInfo);
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/deleteRigOwner", produces = "application/json")
    @ResponseBody
    public String deleteRigOwner(@RequestBody String body) throws JSONException{

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        String ownerInfo = jsonOb.getString("record");
        result = scrdRigMgrService.deleteRigOwner(ownerInfo);
        return result.getBody().toString();
    }
}
