package com.pten.scorecard.controller;

import com.pten.scorecard.service.ScoreCardSummaryService;
import com.pten.ui.util.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Date;

@RestController
@RequestMapping(value = {"/assetReport/scorecard/summary"})
public class ScoreCardSummaryController {

    @Autowired
    Mapper mapper;

    @Autowired
    private ScoreCardSummaryService scrdSumService;

    private static final Logger log = LoggerFactory.getLogger(ScoreCardSummaryController.class);

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/latestdate", produces = "application/json")
    @ResponseBody
    public String rigScoreCardlatestDate() {
        return scrdSumService.getLatestDate();
    }


    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/scorecardsummarylist", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCard(@RequestParam("date") String date) {
        return scrdSumService.getSummaryRigs(date);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/scorecardsummarylistbyid", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardDetails(@RequestParam("id") Collection<Integer> id) {
        return scrdSumService.getSummaryRigsById(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/summarytrend", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardSummaryTrend(@RequestParam("date") String date) {
        return scrdSumService.getSummaryTrend(date);
    }

    @CrossOrigin
    @GetMapping(value = "/hse", produces = "application/json")
    @ResponseBody
    public Collection getScoreCardHSE(@RequestParam String date) {
        return scrdSumService.getScoreCardHSE(date);
    }

    @CrossOrigin

    @GetMapping(value = "/hsetrend", produces = "application/json")
    @ResponseBody
    public Collection getScoreCardHSETrend(@RequestParam String date) {
        return scrdSumService.getScoreCardHSETrend(date);
    }


    @GetMapping(value = "/equipment", produces = "application/json")
    @ResponseBody
    public Collection getScoreCardEquipment(@RequestParam Date date) {
        return scrdSumService.getScoreCardEquipment(date);
    }

    @GetMapping(value = "/equipmenttrend", produces = "application/json")
    @ResponseBody
    public Collection getScoreCardEquipmentTrend(@RequestParam String date) {
        return scrdSumService.getScoreCardEquipmentTrend(date);
    }

    @CrossOrigin
    @GetMapping(value = "/rigperformance", produces = "application/json")
    @ResponseBody
    public Collection getScoreCardRigPerformance(@RequestParam String date) {
        return scrdSumService.getScoreCardRigPerformance(date);
    }

    @CrossOrigin
    @GetMapping(value = "/rigperfomancetrend", produces = "application/json")
    @ResponseBody
    public Collection getScoreCardRigPerformanceTrend(@RequestParam String date) {
        return scrdSumService.getScoreCardRigPerformanceTrend(date);
    }

    @CrossOrigin
    @GetMapping(value = "/financial", produces = "application/json")
    @ResponseBody
    public Collection getScoreCardFinancial(@RequestParam String date) {
        return scrdSumService.getScoreCardFinancial(date);
    }

    @CrossOrigin
    @GetMapping(value = "/financialtrend", produces = "application/json")
    @ResponseBody
    public Collection getScoreCardFinancialTrend(@RequestParam String date) {
        return scrdSumService.getScoreCardFinancialTrend(date);
    }

    @CrossOrigin
    @GetMapping(value = "/peopledata", produces = "application/json")
    @ResponseBody
    public Collection getScoreCardPeopleData(@RequestParam String date) {
        return scrdSumService.getScoreCardPeopleData(date);
    }

    @CrossOrigin
    @GetMapping(value = "/peopledatatrend", produces = "application/json")
    @ResponseBody
    public Collection getScoreCardPeopleDataTrend(@RequestParam String date) {
        return scrdSumService.getScoreCardPeopleDataTrend(date);
    }

}
