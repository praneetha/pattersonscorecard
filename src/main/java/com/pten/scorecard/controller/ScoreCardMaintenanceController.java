package com.pten.scorecard.controller;

import com.pten.scorecard.service.ScorecardMaintenanceService;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@ControllerAdvice
@RequestMapping(value = {"/assetReport/scorecard/dataUpload/"})
public class ScoreCardMaintenanceController {

    @Autowired
    private ScorecardMaintenanceService scrdMntService;


    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/getFileTypeInfo", produces = "application/json")
    @ResponseBody
    public String getFileTypeInfo(@RequestBody String body) throws JSONException {

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        String year = jsonOb.getString("year");
        String month = jsonOb.getString("month");
        result = scrdMntService.getFileTypeInfo(year, month);
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/getFileInfo", produces = "application/json")
    @ResponseBody
    public String getFileInfo(@RequestBody String body) throws JSONException {

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        String fileTypeID = jsonOb.getString("fileTypeID");
        String fileUploadID = jsonOb.getString("fileUploadID");
        result = scrdMntService.getFileInfo(fileTypeID, fileUploadID);
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/getFileDetails", produces = "application/json")
    @ResponseBody
    public String getFileDetails(@RequestBody String body) throws JSONException {

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        String fileTypeID = jsonOb.getString("fileTypeID");
        String fileUploadID = jsonOb.getString("fileUploadID");
        int startRow = jsonOb.getInt("startRow");
        int endRow = jsonOb.getInt("endRow");
        result = scrdMntService.getFileDetails(fileTypeID, fileUploadID, startRow, endRow);
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/uploadSourceFile", produces = "application/json")
    @ResponseBody
    public String uploadSourceFile(@RequestParam("file") MultipartFile file, @RequestParam("year") String year,
                                   @RequestParam("month") String month, @RequestParam("fileTypeID") int fileTypeID,
                                   @RequestParam("userName") String uploadUser) {

        ResponseEntity<Object> result ;
        System.out.println(file + " " + year + " " + month + " " + fileTypeID + " " + uploadUser);
        result = scrdMntService.uploadSourceFile(year, month, fileTypeID, file, uploadUser);
        System.out.println("Upload Result: " + result.getBody().toString());
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/uploadSystemData", produces = "application/json")
    @ResponseBody
    public String uploadSystemData(@RequestBody String body) throws JSONException {

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        String year = jsonOb.getString("year");
        String month = jsonOb.getString("month");
        int fileTypeID = jsonOb.getInt("fileTypeID");
        String uploadUser = jsonOb.getString("userName");
        result = scrdMntService.uploadSystemData(year, month, fileTypeID, uploadUser);
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/saveUpdatedData", produces = "application/json")
    @ResponseBody
    public String saveUpdatedData(@RequestBody String body) throws JSONException {

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        String fileTypeID = jsonOb.getString("fileTypeID");
        String fileUploadID = jsonOb.getString("fileUploadID");
        String data = jsonOb.getString("updates");
        result = scrdMntService.saveUpdatedData(fileTypeID, fileUploadID, data);
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/filterData", produces = "application/json")
    @ResponseBody
    public String filterData(@RequestBody String body) throws JSONException {

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        String fileTypeID = jsonOb.getString("fileTypeID");
        String fileUploadID = jsonOb.getString("fileUploadID");
        String data = jsonOb.getString("columns");
        int startRow = jsonOb.getInt("startRow");
        int endRow = jsonOb.getInt("endRow");
        result = scrdMntService.filterData(fileTypeID, fileUploadID, data, startRow, endRow);
        return result.getBody().toString();
    }
}

