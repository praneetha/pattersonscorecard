package com.pten.scorecard.controller;

import com.pten.scorecard.service.CompetitionScoreCardService;
import com.pten.scorecard.service.ScoreCardService;
import com.pten.ui.util.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(value = {"/assetReport/scorecard/competition"})
public class CompetitionScoreCardController {

    @Autowired
    Mapper mapper;

    @Autowired
    private CompetitionScoreCardService compScrdService;

    private static final Logger log = LoggerFactory.getLogger(ScoreCardController.class);

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/allactivescorecards", produces = "application/json")
    @ResponseBody
    public Collection activeScoreCards() {
        return compScrdService.getAllActiveScoreCards();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rigscorecard", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCard(@RequestParam("rig") String rigName,
                                   @RequestParam("date") String date) {
        return compScrdService.getScoreCard(rigName, date);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rigscorecarddetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardDetails(@RequestParam("rig") String rigName,
                                          @RequestParam("date") String date) {
        return compScrdService.getScoreCardDetails(rigName, date);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rigscorecarddetailsbyid", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardDetails(@RequestParam("id") int id) {
        return compScrdService.getScoreCardDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/riglist", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardRigList( @RequestParam("date") String date) {
        return compScrdService.getScoreCardRigList(date);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/topranked", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardTopRanked( @RequestParam("date") String date) {
        return compScrdService.getTopScoreCard(date);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/latestdate", produces = "application/json")
    @ResponseBody
    public String rigScoreCardlatestDate() {
        return compScrdService.getLatestScoreCardDate();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rigscorecardtrend", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardTrend(@RequestParam("scorecard_id") int scorecard_id) {
        return compScrdService.getScoreCardTrend(scorecard_id);
    }
}
