package com.pten.scorecard.controller;

import com.pten.scorecard.service.ScorecardPeopleManagerService;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@ControllerAdvice
@RequestMapping(value = {"/assetReport/scorecard/peoplemapping/"})

public class ScorecardPeopleManagerController {

    @Autowired
    private ScorecardPeopleManagerService scrdPplMgrService = new ScorecardPeopleManagerService();

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/getOwnerList", produces = "application/json")
    @ResponseBody
    public String getOwnerList() {

        ResponseEntity<Object> result;
        result = scrdPplMgrService.getOwnerList();
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/getOwnerTypeList", produces = "application/json")
    @ResponseBody
    public String getOwnerTypeList() {

        ResponseEntity<Object> result;
        result = scrdPplMgrService.getOwnerTypeList();
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/getLocationByOwner", produces = "application/json")
    @ResponseBody
    public String getLocationByOwner(@RequestBody String body) throws JSONException{

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        int ownerID = jsonOb.getInt("ownerID");
        result = scrdPplMgrService.getLocationByOwner(ownerID);
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/getSuperintendentByOwner", produces = "application/json")
    @ResponseBody
    public String getSuperintendentByOwner(@RequestBody String body) throws JSONException{

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        int ownerID = jsonOb.getInt("ownerID");
        result = scrdPplMgrService.getSuperintendentByOwner(ownerID);
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/addNewOwner", produces = "application/json")
    @ResponseBody
    public String addNewOwner(@RequestBody String body) throws JSONException {

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        String ownerName = jsonOb.getString("ownerName");
        int ownerTypeID = jsonOb.getInt("ownerTypeID");
        String empNum = jsonOb.getString("empNum");
        result = scrdPplMgrService.addNewOwner(ownerName,ownerTypeID,empNum);
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/addOwnerLocation", produces = "application/json")
    @ResponseBody
    public String addOwnerLocation(@RequestBody String body) throws JSONException{

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        int ownerID = jsonOb.getInt("ownerID");
        int regionID = jsonOb.getInt("regionID");
        String startDate = jsonOb.getString("startDate");
        String endDate = jsonOb.getString("endDate");
        result = scrdPplMgrService.addOwnerLocation(ownerID,regionID,startDate,endDate);
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/addOwnerOwner", produces = "application/json")
    @ResponseBody
    public String addOwnerOwner(@RequestBody String body) throws JSONException{

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        int ownerID = jsonOb.getInt("ownerID");
        int ownedOwnerID = jsonOb.getInt("ownedOwnerID");
        String startDate = jsonOb.getString("startDate");
        result = scrdPplMgrService.addOwnerOwner(ownerID,ownedOwnerID,startDate);
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/editOwnerLocation", produces = "application/json")
    @ResponseBody
    public String editOwnerLocation(@RequestBody String body) throws JSONException{

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        String locationInfo = jsonOb.getString("locationInfo");
        JSONObject editInfo = new JSONObject(locationInfo);
        String oldRecord = editInfo.getString("oldRecord");
        String newRecord = editInfo.getString("newRecord");
        result = scrdPplMgrService.editOwnerLocation(oldRecord,newRecord);
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/editOwnerOwner", produces = "application/json")
    @ResponseBody
    public String editOwnerOwner(@RequestBody String body) throws JSONException{

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        String personInfo = jsonOb.getString("personInfo");
        JSONObject editInfo = new JSONObject(personInfo);
        String oldRecord = editInfo.getString("oldRecord");
        String newRecord = editInfo.getString("newRecord");
        result = scrdPplMgrService.editOwnerOwner(oldRecord,newRecord);
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/deleteOwnerLocation", produces = "application/json")
    @ResponseBody
    public String deleteOwnerLocation(@RequestBody String body) throws JSONException{

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        String locationInfo = jsonOb.getString("record");
        result = scrdPplMgrService.deleteOwnerLocation(locationInfo);
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/deleteOwnerOwner", produces = "application/json")
    @ResponseBody
    public String deleteOwnerOwner(@RequestBody String body) throws JSONException{

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        String ownerInfo = jsonOb.getString("record");
        result = scrdPplMgrService.deleteOwnerOwner(ownerInfo);
        return result.getBody().toString();
    }
}
