package com.pten.scorecard.controller;

import com.pten.scorecard.service.ScorecardGenerationService;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;

@RestController
@ControllerAdvice
@RequestMapping(value = {"/assetReport/scorecard/generation/"})

public class ScorecardGenerationController {

    @Autowired
    private ScorecardGenerationService scrdGnrService = new ScorecardGenerationService();

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/getScorecardDate", produces = "application/json")
    @ResponseBody
    public ArrayList<String> getScorecardDate(@RequestBody String body) {

        ArrayList<String> result;
        result = scrdGnrService.getScorecardDate();
        return result;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/getScorecardRevision", produces = "application/json")
    @ResponseBody
    public String getScorecardRevision(@RequestBody String body) throws JSONException {

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        String scDate = jsonOb.getString("scDate");
        result = scrdGnrService.getScorecardRevision(scDate);
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/getRigPersonMetaData", produces = "application/json")
    @ResponseBody
    public String getRigPersonMetaData(@RequestBody String body) {

        ResponseEntity<Object> result;
        result = scrdGnrService.getRigPersonMetaData();
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/getRigPersonData", produces = "application/json")
    @ResponseBody
    public String getRigPersonData(@RequestBody String body) throws JSONException {

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        String scDate = jsonOb.getString("scDate");
        String type = jsonOb.getString("scTab");
        String scRevision = jsonOb.getString("scRevision");
        result = scrdGnrService.getRigPersonData(scDate, type, scRevision);
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/getGenerateMeta", produces = "application/json")
    @ResponseBody
    public String getGenerateMeta(@RequestBody String body) {

        ResponseEntity<Object> result;
        result = scrdGnrService.getGenerateMeta();
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/getGenerateData", produces = "application/json")
    @ResponseBody
    public String getGenerateData(@RequestBody String body) throws JSONException {

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        String scDate = jsonOb.getString("scDate");
        String type = jsonOb.getString("genTab");
        result = scrdGnrService.getGenerateData(scDate, type);
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/generateScorecard", produces = "application/json")
    @ResponseBody
    public String generateScorecard(@RequestBody String body) throws JSONException {

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        String scDate = jsonOb.getString("scDate");
        String data = jsonOb.getString("genData");
        result = scrdGnrService.generateScorecard(scDate, data);
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/publishScorecard", produces = "application/json")
    @ResponseBody
    public String publishScorecard(@RequestBody String body) throws JSONException {

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        String scDate = jsonOb.getString("scDate");
        result = scrdGnrService.publishScorecard(scDate);
        return result.getBody().toString();
    }


    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/unPublishScorecard", produces = "application/json")
    @ResponseBody
    public String unPublishScorecard(@RequestBody String body) throws JSONException {

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        String scDate = jsonOb.getString("scDate");
        String scRevision = jsonOb.getString("scRevision");
        result = scrdGnrService.unPublishScorecard(scDate,scRevision);
        return result.getBody().toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/archiveScorecard", produces = "application/json")
    @ResponseBody
    public String archiveScorecard(@RequestBody String body) throws JSONException {

        ResponseEntity<Object> result;
        JSONObject jsonOb = new JSONObject(body);
        String scDate = jsonOb.getString("scDate");
        String scRevision = jsonOb.getString("scRevision");
        result = scrdGnrService.archiveScorecard(scDate,scRevision);
        return result.getBody().toString();
    }


    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/exportScorecard", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public ResponseEntity<byte[]> exportScorecard(@RequestParam("scDate") String scDate,
                                                  @RequestParam("scTab") String scTab) {

        String result;
        result = scrdGnrService.exportScorecard(scDate,scTab);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        if(scTab.equalsIgnoreCase("rig")) {
            httpHeaders.add(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=rigData.csv");
        }
        else {
            httpHeaders.add(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=personData.csv");
        }
        httpHeaders.setContentLength(result.getBytes().length);
        return ResponseEntity.ok().headers(httpHeaders).body(result.getBytes());
    }
}
