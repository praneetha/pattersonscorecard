package com.pten.scorecard.controller;
import com.pten.scorecard.service.SuperIntendentScoreCardService;
import com.pten.ui.util.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(value = {"/assetReport/scorecard/superintendent"})

public class SuperIntendentScoreCardController {

    @Autowired
    Mapper mapper;

    @Autowired
    private SuperIntendentScoreCardService sprScrdService;

    private static final Logger log = LoggerFactory.getLogger(SuperIntendentScoreCardController.class);

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/latestdate", produces = "application/json")
    @ResponseBody
    public String rigScoreCardlatestDate() {
        return sprScrdService.getLatestScoreCardDate();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/superintendentlist", produces = "application/json")
    @ResponseBody
    public Collection superScoreCardSuperList( @RequestParam("date") String date) {
        return sprScrdService.getSuperintendentList(date);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/scorecard", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCard(@RequestParam("superintendent") String superintendent,
                                   @RequestParam("date") String date) {
        return sprScrdService.getScoreCard(superintendent, date);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/scorecardbyid", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardbyid(@RequestParam("id") int id) {
        return sprScrdService.getScoreCard(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/scorecardtrend", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardTrend(@RequestParam("id") int id) {
        return sprScrdService.getScoreCardTrend(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/scorecarddetailsbyid", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardDetails(@RequestParam("id") int id) {
        return sprScrdService.getScoreCardDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/scorecarddetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardDetails(@RequestParam("superintendent") String superintendent,
                                          @RequestParam("date") String date) {
        return sprScrdService.getScoreCardDetails(superintendent, date);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rigscorecardids", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardDetails(@RequestParam("rigs") Collection<String>rigs,
                                          @RequestParam("date") String date) {
        return sprScrdService.getRigScoreCardIds(rigs, date);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rigscorecardidsbyid", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardDetails(@RequestParam("id") Collection<Integer> id) {
        return sprScrdService.getRigScoreCardIds(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/topranked", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardTopRanked( @RequestParam("date") String date) {
        return sprScrdService.getTopScoreCard(date);
    }


}
