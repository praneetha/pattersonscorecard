package com.pten.scorecard.controller;
import com.pten.scorecard.service.OpsManagerScoreCardService;
import com.pten.scorecard.service.SuperIntendentScoreCardService;
import com.pten.ui.util.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;


@RestController
@RequestMapping(value = {"/assetReport/scorecard/opsmanager"})

public class OpsManagerScoreCardController {

    @Autowired
    Mapper mapper;

    @Autowired
    private OpsManagerScoreCardService opsScrdService;


    private static final Logger log = LoggerFactory.getLogger(OpsManagerScoreCardController.class);

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/latestdate", produces = "application/json")
    @ResponseBody
    public String rigScoreCardlatestDate() {
        return opsScrdService.getLatestScoreCardDate();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/opsmanagerlist", produces = "application/json")
    @ResponseBody
    public Collection opsManagerScoreCardSuperList(@RequestParam("date") String date) {
        return opsScrdService.getOpsManagerList(date);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/scorecardbyid", produces = "application/json")
    @ResponseBody
    public Collection opsManScoreCardbyid(@RequestParam("id") int id) {
        return opsScrdService.getScoreCard(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/superintendentscorecardrigids", produces = "application/json")
    @ResponseBody
    public Collection superintendentScoreCardRigIds(@RequestParam("id") Collection<Integer> id) {
        return opsScrdService.getSuperintendentRigScoreCardIds(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/hseevnetdetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardHseEventDetails(@RequestParam("id") Collection<Integer> id) {
        return opsScrdService.getHseEventDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/imsopeneddetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardImsOpenedDetails(@RequestParam("id") Collection<Integer> id) {
        return opsScrdService.getImsOpenedDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/imscloseddetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardImsClosedDetails(@RequestParam("id") Collection<Integer> id) {
        return opsScrdService.getImsClosedDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cridetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardCRIDetails(@RequestParam("id") Collection<Integer> id) {
        return opsScrdService.getCRIDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/tripspeeddetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardTripSpeedDetails(@RequestParam("id") Collection<Integer> id) {
        return opsScrdService.getTripSpeedDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/tripconndetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardTripConnDetails(@RequestParam("id") Collection<Integer> id) {
        return opsScrdService.getTripConnDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/drillconndetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardDrillConnDetails(@RequestParam("id") Collection<Integer> id) {
        return opsScrdService.getDrillConnDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/walkdetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardWalkDetails(@RequestParam("id") Collection<Integer> id) {
        return opsScrdService.getWalkDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rmctdetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardRmctDetails(@RequestParam("id") Collection<Integer> id) {
        return opsScrdService.getRmctDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/tibwodetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardTibWoDetails(@RequestParam("id") Collection<Integer> id) {
        return opsScrdService.getTibWoDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/techwodetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardTechWoDetails(@RequestParam("id") Collection<Integer> id) {
        return opsScrdService.getTechWoDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/opswodetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardOpsWoDetails(@RequestParam("id") Collection<Integer> id) {
        return opsScrdService.getOpsWoDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/assetinvdetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardAssetInvDetails(@RequestParam("id") Collection<Integer> id) {
        return opsScrdService.getAssetInvDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/turnoverdetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardTurnoverDetails(@RequestParam("id") Collection<Integer> id) {
        return opsScrdService.getTurnOverDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/capexyearlydetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardCapexYearlyDetails(@RequestParam("id") Collection<Integer> id) {
        return opsScrdService.getCapexYearlyDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cmrerrordetails", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardCMRErrorDetails(@RequestParam("id") Collection<Integer> id) {
        return opsScrdService.getCMRErrorDetails(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/topranked", produces = "application/json")
    @ResponseBody
    public Collection rigScoreCardTopRanked( @RequestParam("date") String date) {
        return opsScrdService.getTopScoreCard(date);
    }


}


