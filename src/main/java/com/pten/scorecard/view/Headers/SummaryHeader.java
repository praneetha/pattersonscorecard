package com.pten.scorecard.view.Headers;

public class SummaryHeader {
    private Integer opsmanager_scorecard_id;
    private String opsmanager_name;
    private Integer opsmanager_id;
    private Integer superintendent_scorecard_id;
    private String superintendent_name;
    private String superintendent_id;
    private Integer rig_scorecard_id;
    private String rig_name;
    private String region_name;
    private String area_name;
    private String operator_name;
    private Integer operator_id;
    private String rig_type;

    public SummaryHeader() {
    }

    public Integer getOpsmanager_scorecard_id() {
        return opsmanager_scorecard_id;
    }

    public void setOpsmanager_scorecard_id(Integer opsmanager_scorecard_id) {
        this.opsmanager_scorecard_id = opsmanager_scorecard_id;
    }

    public String getOpsmanager_name() {
        return opsmanager_name;
    }

    public void setOpsmanager_name(String opsmanager_name) {
        this.opsmanager_name = opsmanager_name;
    }

    public Integer getOpsmanager_id() {
        return opsmanager_id;
    }

    public void setOpsmanager_id(Integer opsmanager_id) {
        this.opsmanager_id = opsmanager_id;
    }

    public Integer getSuperintendent_scorecard_id() {
        return superintendent_scorecard_id;
    }

    public void setSuperintendent_scorecard_id(Integer superintendent_scorecard_id) {
        this.superintendent_scorecard_id = superintendent_scorecard_id;
    }

    public String getSuperintendent_name() {
        return superintendent_name;
    }

    public void setSuperintendent_name(String superintendent_name) {
        this.superintendent_name = superintendent_name;
    }

    public String getSuperintendent_id() {
        return superintendent_id;
    }

    public void setSuperintendent_id(String superintendent_id) {
        this.superintendent_id = superintendent_id;
    }

    public Integer getRig_scorecard_id() {
        return rig_scorecard_id;
    }

    public void setRig_scorecard_id(Integer rig_scorecard_id) {
        this.rig_scorecard_id = rig_scorecard_id;
    }

    public String getRig_name() {
        return rig_name;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    public String getArea_name() {
        return area_name;
    }

    public void setArea_name(String area_name) {
        this.area_name = area_name;
    }

    public String getOperator_name() {
        return operator_name;
    }

    public void setOperator_name(String operator_name) {
        this.operator_name = operator_name;
    }

    public Integer getOperator_id() {
        return operator_id;
    }

    public void setOperator_id(Integer operator_id) {
        this.operator_id = operator_id;
    }

    public String getRig_type() {
        return rig_type;
    }

    public void setRig_type(String rig_type) {
        this.rig_type = rig_type;
    }
}
