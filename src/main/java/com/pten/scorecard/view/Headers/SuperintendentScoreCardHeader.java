package com.pten.scorecard.view.Headers;

import com.pten.scorecard.view.Headers.BaseScoreCardHeader;

public class SuperintendentScoreCardHeader extends BaseScoreCardHeader {

    String superintendent;

    public SuperintendentScoreCardHeader() {
    }

    public String getSuperintendent() {
        return superintendent;
    }
    private String pat_top_super;
    private String pat_top_score;
    private String north_top_super;
    private String north_top_score;
    private String south_top_super;
    private String south_top_score;

    public void setSuperintendent(String superintendent) {
        this.superintendent = superintendent;
    }

    public String getPat_top_super() {
        return pat_top_super;
    }

    public void setPat_top_super(String pat_top_super) {
        this.pat_top_super = pat_top_super;
    }

    public String getPat_top_score() {
        return pat_top_score;
    }

    public void setPat_top_score(String pat_top_score) {
        this.pat_top_score = pat_top_score;
    }

    public String getNorth_top_super() {
        return north_top_super;
    }

    public void setNorth_top_super(String north_top_super) {
        this.north_top_super = north_top_super;
    }

    public String getNorth_top_score() {
        return north_top_score;
    }

    public void setNorth_top_score(String north_top_score) {
        this.north_top_score = north_top_score;
    }

    public String getSouth_top_super() {
        return south_top_super;
    }

    public void setSouth_top_super(String south_top_super) {
        this.south_top_super = south_top_super;
    }

    public String getSouth_top_score() {
        return south_top_score;
    }

    public void setSouth_top_score(String south_top_score) {
        this.south_top_score = south_top_score;
    }
}
