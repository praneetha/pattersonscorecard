package com.pten.scorecard.view.Headers;



public class OpsManagerScoreCardHeader extends BaseScoreCardHeader {

    String opsmanager;

    public OpsManagerScoreCardHeader() {
    }


    public String getOpsmanager() {
        return opsmanager;
    }
    private String pat_top_opsmanager;
    private String pat_top_score;
    private String north_top_opsmanager;
    private String north_top_score;
    private String south_top_opsmanager;
    private String south_top_score;

    public void setOpsmanager(String opsmanager) {
        this.opsmanager = opsmanager;
    }

    public String getPat_top_opsmanager() {
        return pat_top_opsmanager;
    }

    public void setPat_top_opsmanager(String pat_top_opsmanager) {
        this.pat_top_opsmanager = pat_top_opsmanager;
    }

    public String getPat_top_score() {
        return pat_top_score;
    }

    public void setPat_top_score(String pat_top_score) {
        this.pat_top_score = pat_top_score;
    }

    public String getNorth_top_opsmanager() {
        return north_top_opsmanager;
    }

    public void setNorth_top_opsmanager(String north_top_opsmanager) {
        this.north_top_opsmanager = north_top_opsmanager;
    }

    public String getNorth_top_score() {
        return north_top_score;
    }

    public void setNorth_top_score(String north_top_score) {
        this.north_top_score = north_top_score;
    }

    public String getSouth_top_opsmanager() {
        return south_top_opsmanager;
    }

    public void setSouth_top_opsmanager(String south_top_opsmanager) {
        this.south_top_opsmanager = south_top_opsmanager;
    }

    public String getSouth_top_score() {
        return south_top_score;
    }

    public void setSouth_top_score(String south_top_score) {
        this.south_top_score = south_top_score;
    }
}
