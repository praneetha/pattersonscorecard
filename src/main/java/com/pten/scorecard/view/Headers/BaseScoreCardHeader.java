package com.pten.scorecard.view.Headers;

public class BaseScoreCardHeader {
    int scorecard_id;

    private String scorecard_date;
    private Double overall_score;
    private Double area_rank;
    private int area_count;
    private Double region_rank;
    private int region_count;
    private Double patterson_rank;
    private int patterson_count;
    private Double area_average;
    private Double region_average;
    private Double patterson_average;
    private String region_name;
    private String area_name;

    public BaseScoreCardHeader() {
    }

    public int getScorecard_id() {
        return scorecard_id;
    }

    public void setScorecard_id(int scorecard_id) {
        this.scorecard_id = scorecard_id;
    }

    public String getScorecard_date() {
        return scorecard_date;
    }

    public void setScorecard_date(String scorecard_date) {
        this.scorecard_date = scorecard_date;
    }

    public Double getOverall_score() {
        return overall_score;
    }

    public void setOverall_score(Double overall_score) {
        this.overall_score = overall_score;
    }

    public Double getArea_rank() {
        return area_rank;
    }

    public void setArea_rank(Double area_rank) {
        this.area_rank = area_rank;
    }

    public int getArea_count() {
        return area_count;
    }

    public void setArea_count(int area_count) {
        this.area_count = area_count;
    }

    public Double getRegion_rank() {
        return region_rank;
    }

    public void setRegion_rank(Double region_rank) {
        this.region_rank = region_rank;
    }

    public int getRegion_count() {
        return region_count;
    }

    public void setRegion_count(int region_count) {
        this.region_count = region_count;
    }

    public Double getPatterson_rank() {
        return patterson_rank;
    }

    public void setPatterson_rank(Double patterson_rank) {
        this.patterson_rank = patterson_rank;
    }

    public int getPatterson_count() {
        return patterson_count;
    }

    public void setPatterson_count(int patterson_count) {
        this.patterson_count = patterson_count;
    }

    public Double getArea_average() {
        return area_average;
    }

    public void setArea_average(Double area_average) {
        this.area_average = area_average;
    }

    public Double getRegion_average() {
        return region_average;
    }

    public void setRegion_average(Double region_average) {
        this.region_average = region_average;
    }

    public Double getPatterson_average() {
        return patterson_average;
    }

    public void setPatterson_average(Double patterson_average) {
        this.patterson_average = patterson_average;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    public String getArea_name() {
        return area_name;
    }

    public void setArea_name(String area_name) {
        this.area_name = area_name;
    }


}
