package com.pten.scorecard.view.Headers;

import com.pten.scorecard.view.Headers.BaseScoreCardHeader;

public class RigScoreCardHeader extends BaseScoreCardHeader {

    String rig_name;

    public RigScoreCardHeader() {
    }

    public String getRig_name() {
        return rig_name;
    }
    private String pat_top_rig;
    private String pat_top_score;
    private String north_top_rig;
    private String north_top_score;
    private String south_top_rig;
    private String south_top_score;

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public String getPat_top_rig() {
        return pat_top_rig;
    }

    public void setPat_top_rig(String pat_top_rig) {
        this.pat_top_rig = pat_top_rig;
    }

    public String getPat_top_score() {
        return pat_top_score;
    }

    public void setPat_top_score(String pat_top_score) {
        this.pat_top_score = pat_top_score;
    }

    public String getNorth_top_rig() {
        return north_top_rig;
    }

    public void setNorth_top_rig(String north_top_rig) {
        this.north_top_rig = north_top_rig;
    }

    public String getNorth_top_score() {
        return north_top_score;
    }

    public void setNorth_top_score(String north_top_score) {
        this.north_top_score = north_top_score;
    }

    public String getSouth_top_rig() {
        return south_top_rig;
    }

    public void setSouth_top_rig(String south_top_rig) {
        this.south_top_rig = south_top_rig;
    }

    public String getSouth_top_score() {
        return south_top_score;
    }

    public void setSouth_top_score(String south_top_score) {
        this.south_top_score = south_top_score;
    }
}
