package com.pten.scorecard.view.DetailGrid;

public class CRIDetail {
    private int scorecard_id;
    private String superintendent;
    private Double superintendent_score;
    private String superintendent_date;
    private String hse_auditor;
    private Double hse_score;
    private String hse_date;
    private String cri_evaluator;
    private String rig_name;
    private String scorecard_date;

    public CRIDetail() {    }

    public int getScorecard_id() {
        return scorecard_id;
    }

    public void setScorecard_id(int scorecard_id) {
        this.scorecard_id = scorecard_id;
    }

    public String getSuperintendent() {
        return superintendent;
    }

    public void setSuperintendent(String superintendent) {
        this.superintendent = superintendent;
    }

    public Double getSuperintendent_score() {
        return superintendent_score;
    }

    public void setSuperintendent_score(Double superintendent_score) {
        this.superintendent_score = superintendent_score;
    }

    public String getSuperintendent_date() {
        return superintendent_date;
    }

    public void setSuperintendent_date(String superintendent_date) {
        this.superintendent_date = superintendent_date;
    }

    public String getHse_auditor() {
        return hse_auditor;
    }

    public void setHse_auditor(String hse_auditor) {
        this.hse_auditor = hse_auditor;
    }

    public Double getHse_score() {
        return hse_score;
    }

    public void setHse_score(Double hse_score) {
        this.hse_score = hse_score;
    }

    public String getHse_date() {
        return hse_date;
    }

    public void setHse_date(String hse_date) {
        this.hse_date = hse_date;
    }

    public String getCri_evaluator() {
        return cri_evaluator;
    }

    public void setCri_evaluator(String cri_evaluator) {
        this.cri_evaluator = cri_evaluator;
    }

    public String getRig_name() {
        return rig_name;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public String getScorecard_date() {
        return scorecard_date;
    }

    public void setScorecard_date(String scorecard_date) {
        this.scorecard_date = scorecard_date;
    }
}
