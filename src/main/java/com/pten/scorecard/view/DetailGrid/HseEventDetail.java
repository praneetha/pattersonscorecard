package com.pten.scorecard.view.DetailGrid;

public class HseEventDetail {

    private int scorecard_id;
    private String incident_number;
    private String incident_date;
    private String investigation_date;
    private String potential_severity;
    private String consequences;
    private String summary;
    private String event_type;
    private String rig_name;
    private String scorecard_date;

    public HseEventDetail() {
    }

    public int getScorecard_id() {
        return scorecard_id;
    }

    public void setScorecard_id(int scorecard_id) {
        this.scorecard_id = scorecard_id;
    }

    public String getIncident_number() {
        return incident_number;
    }

    public void setIncident_number(String incident_number) {
        this.incident_number = incident_number;
    }

    public String getIncident_date() {
        return incident_date;
    }

    public void setIncident_date(String incident_date) {
        this.incident_date = incident_date;
    }

    public String getInvestigation_date() {
        return investigation_date;
    }

    public void setInvestigation_date(String investigation_date) {
        this.investigation_date = investigation_date;
    }

    public String getPotential_severity() {
        return potential_severity;
    }

    public void setPotential_severity(String potential_severity) {
        this.potential_severity = potential_severity;
    }

    public String getConsequences() {
        return consequences;
    }

    public void setConsequences(String consequences) {
        this.consequences = consequences;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getRig_name() {
        return rig_name;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public String getScorecard_date() {
        return scorecard_date;
    }

    public void setScorecard_date(String scorecard_date) {
        this.scorecard_date = scorecard_date;
    }
}
