package com.pten.scorecard.view.DetailGrid;

public class CapexDetail {
    private int scorecard_id;
    private String project_number;
    private String project_name;
    private Double Spend;
    private String rig_name;
    private String scorecard_date;

    public CapexDetail() {
    }

    public int getScorecard_id() {
        return scorecard_id;
    }

    public void setScorecard_id(int scorecard_id) {
        this.scorecard_id = scorecard_id;
    }

    public String getProject_number() {
        return project_number;
    }

    public void setProject_number(String project_number) {
        this.project_number = project_number;
    }

    public String getProject_name() {
        return project_name;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public Double getSpend() {
        return Spend;
    }

    public void setSpend(Double spend) {
        Spend = spend;
    }

    public String getRig_name() {
        return rig_name;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public String getScorecard_date() {
        return scorecard_date;
    }

    public void setScorecard_date(String scorecard_date) {
        this.scorecard_date = scorecard_date;
    }
}
