package com.pten.scorecard.view.DetailGrid;

public class RMCTDetail {
    private int scorecard_id;
    private int metric_id;
    private String rig_name;
    private int move_number;
    private int move_ordinal;
    private String move_start_date;
    private String move_end_date;
    private Double rig_move_distance;
    private Double  rig_move_time;
    private Double target_time;
    private Double partial_credit_time;
    private Double score;
    private String from_timestamp;
    private String to_timestamp;
    private String code;
    private String sub_code;
    private Double hours;
    private String details;
    private String scorecard_date;

    public RMCTDetail() {
    }

    public int getScorecard_id() {
        return scorecard_id;
    }

    public void setScorecard_id(int scorecard_id) {
        this.scorecard_id = scorecard_id;
    }

    public int getMetric_id() {
        return metric_id;
    }

    public void setMetric_id(int metric_id) {
        this.metric_id = metric_id;
    }

    public String getRig_name() {
        return rig_name;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public int getMove_number() {
        return move_number;
    }

    public void setMove_number(int move_number) {
        this.move_number = move_number;
    }

    public int getMove_ordinal() {
        return move_ordinal;
    }

    public void setMove_ordinal(int move_ordinal) {
        this.move_ordinal = move_ordinal;
    }

    public String getMove_start_date() {
        return move_start_date;
    }

    public void setMove_start_date(String move_start_date) {
        this.move_start_date = move_start_date;
    }

    public String getMove_end_date() {
        return move_end_date;
    }

    public void setMove_end_date(String move_end_date) {
        this.move_end_date = move_end_date;
    }

    public Double getRig_move_distance() {
        return rig_move_distance;
    }

    public void setRig_move_distance(Double rig_move_distance) {
        this.rig_move_distance = rig_move_distance;
    }

    public Double getRig_move_time() {
        return rig_move_time;
    }

    public void setRig_move_time(Double rig_move_time) {
        this.rig_move_time = rig_move_time;
    }

    public Double getTarget_time() {
        return target_time;
    }

    public void setTarget_time(Double target_time) {
        this.target_time = target_time;
    }

    public Double getPartial_credit_time() {
        return partial_credit_time;
    }

    public void setPartial_credit_time(Double partial_credit_time) {
        this.partial_credit_time = partial_credit_time;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getFrom_timestamp() {
        return from_timestamp;
    }

    public void setFrom_timestamp(String from_timestamp) {
        this.from_timestamp = from_timestamp;
    }

    public String getTo_timestamp() {
        return to_timestamp;
    }

    public void setTo_timestamp(String to_timestamp) {
        this.to_timestamp = to_timestamp;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSub_code() {
        return sub_code;
    }

    public void setSub_code(String sub_code) {
        this.sub_code = sub_code;
    }

    public Double getHours() {
        return hours;
    }

    public void setHours(Double hours) {
        this.hours = hours;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getScorecard_date() {
        return scorecard_date;
    }

    public void setScorecard_date(String scorecard_date) {
        this.scorecard_date = scorecard_date;
    }
}
