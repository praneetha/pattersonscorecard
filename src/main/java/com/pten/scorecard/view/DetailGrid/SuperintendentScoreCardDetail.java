package com.pten.scorecard.view.DetailGrid;

public class SuperintendentScoreCardDetail extends ScoreCardDetail{
    private String source_name;
    private int current_month_scorecard_id;
    private int prior_month_scorecard_id;
    private int two_month_scorecard_id;
    private String rig_name;
    private String scorecard_date;

    public SuperintendentScoreCardDetail() {
    }

    public String getSource_name() {
        return source_name;
    }

    public void setSource_name(String source_name) {
        this.source_name = source_name;
    }

    public int getCurrent_month_scorecard_id() {
        return current_month_scorecard_id;
    }

    public void setCurrent_month_scorecard_id(int current_month_scorecard_id) {
        this.current_month_scorecard_id = current_month_scorecard_id;
    }

    public int getPrior_month_scorecard_id() {
        return prior_month_scorecard_id;
    }

    public void setPrior_month_scorecard_id(int prior_month_scorecard_id) {
        this.prior_month_scorecard_id = prior_month_scorecard_id;
    }

    public int getTwo_month_scorecard_id() {
        return two_month_scorecard_id;
    }

    public void setTwo_month_scorecard_id(int two_month_scorecard_id) {
        this.two_month_scorecard_id = two_month_scorecard_id;
    }

    @Override
    public String getRig_name() {
        return rig_name;
    }

    @Override
    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    @Override
    public String getScorecard_date() {
        return scorecard_date;
    }

    @Override
    public void setScorecard_date(String scorecard_date) {
        this.scorecard_date = scorecard_date;
    }
}
