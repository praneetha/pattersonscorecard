package com.pten.scorecard.view.DetailGrid;

public class CMRErrorDetail {

    private int scorecard_id;
    private String report_date;
    private String error_first_observed;
    private String error_last_observed;
    private String error_text;
    private String rig_name;
    private String scorecard_date;

    public CMRErrorDetail() {
    }

    public int getScorecard_id() {
        return scorecard_id;
    }

    public void setScorecard_id(int scorecard_id) {
        this.scorecard_id = scorecard_id;
    }

    public String getReport_date() {
        return report_date;
    }

    public void setReport_date(String report_date) {
        this.report_date = report_date;
    }

    public String getError_first_observed() {
        return error_first_observed;
    }

    public void setError_first_observed(String error_first_observed) {
        this.error_first_observed = error_first_observed;
    }

    public String getError_last_observed() {
        return error_last_observed;
    }

    public void setError_last_observed(String error_last_observed) {
        this.error_last_observed = error_last_observed;
    }

    public String getError_text() {
        return error_text;
    }

    public void setError_text(String error_text) {
        this.error_text = error_text;
    }

    public String getRig_name() {
        return rig_name;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public String getScorecard_date() {
        return scorecard_date;
    }

    public void setScorecard_date(String scorecard_date) {
        this.scorecard_date = scorecard_date;
    }
}
