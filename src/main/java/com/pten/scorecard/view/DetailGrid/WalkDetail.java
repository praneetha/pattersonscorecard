package com.pten.scorecard.view.DetailGrid;

public class WalkDetail {
    private int scorecard_id;
    private int file_upload_id;
    private int well_num;
    private String well_name;
    private int new_walk_ordinal;
    private String start_time;
    private String end_time;
    private Double overall_walk_time;
    private Double f_walk_time;
    private String from_timestamp;
    private String to_timestamp;
    private Double hours;
    private String code;
    private String sub_code;
    private String details;
    private Double score;
    private String rig_name;
    private String scorecard_date;

    public WalkDetail() {
    }

    public int getScorecard_id() {
        return scorecard_id;
    }

    public void setScorecard_id(int scorecard_id) {
        this.scorecard_id = scorecard_id;
    }

    public int getFile_upload_id() {
        return file_upload_id;
    }

    public void setFile_upload_id(int file_upload_id) {
        this.file_upload_id = file_upload_id;
    }

    public int getWell_num() {
        return well_num;
    }

    public void setWell_num(int well_num) {
        this.well_num = well_num;
    }

    public String getWell_name() {
        return well_name;
    }

    public void setWell_name(String well_name) {
        this.well_name = well_name;
    }

    public int getNew_walk_ordinal() {
        return new_walk_ordinal;
    }

    public void setNew_walk_ordinal(int new_walk_ordinal) {
        this.new_walk_ordinal = new_walk_ordinal;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public Double getOverall_walk_time() {
        return overall_walk_time;
    }

    public void setOverall_walk_time(Double overall_walk_time) {
        this.overall_walk_time = overall_walk_time;
    }

    public Double getF_walk_time() {
        return f_walk_time;
    }

    public void setF_walk_time(Double f_walk_time) {
        this.f_walk_time = f_walk_time;
    }

    public String getFrom_timestamp() {
        return from_timestamp;
    }

    public void setFrom_timestamp(String from_timestamp) {
        this.from_timestamp = from_timestamp;
    }

    public String getTo_timestamp() {
        return to_timestamp;
    }

    public void setTo_timestamp(String to_timestamp) {
        this.to_timestamp = to_timestamp;
    }

    public Double getHours() {
        return hours;
    }

    public void setHours(Double hours) {
        this.hours = hours;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSub_code() {
        return sub_code;
    }

    public void setSub_code(String sub_code) {
        this.sub_code = sub_code;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getRig_name() {
        return rig_name;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public String getScorecard_date() {
        return scorecard_date;
    }

    public void setScorecard_date(String scorecard_date) {
        this.scorecard_date = scorecard_date;
    }
}
