package com.pten.scorecard.view.DetailGrid;

public class WorkOrderDetail {

    private int scorecard_id;
    private String asset_number;
    private String asset_description;
    private String model_number;
    private String work_order_number;
    private String work_order_description;
    private String work_order_status;
    private String date_released;
    private String days_since_released;
    private String make;
    private String rig_name;
    private String scorecard_date;

    public WorkOrderDetail() {
    }

    public int getScorecard_id() {
        return scorecard_id;
    }

    public void setScorecard_id(int scorecard_id) {
        this.scorecard_id = scorecard_id;
    }

    public String getAsset_number() {
        return asset_number;
    }

    public void setAsset_number(String asset_number) {
        this.asset_number = asset_number;
    }

    public String getAsset_description() {
        return asset_description;
    }

    public void setAsset_description(String asset_description) {
        this.asset_description = asset_description;
    }

    public String getModel_number() {
        return model_number;
    }

    public void setModel_number(String model_number) {
        this.model_number = model_number;
    }

    public String getWork_order_number() {
        return work_order_number;
    }

    public void setWork_order_number(String work_order_number) {
        this.work_order_number = work_order_number;
    }

    public String getWork_order_description() {
        return work_order_description;
    }

    public void setWork_order_description(String work_order_description) {
        this.work_order_description = work_order_description;
    }

    public String getWork_order_status() {
        return work_order_status;
    }

    public void setWork_order_status(String work_order_status) {
        this.work_order_status = work_order_status;
    }

    public String getDate_released() {
        return date_released;
    }

    public void setDate_released(String date_released) {
        this.date_released = date_released;
    }

    public String getDays_since_released() {
        return days_since_released;
    }

    public void setDays_since_released(String days_since_release) {
        this.days_since_released = days_since_release;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getRig_name() {
        return rig_name;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public String getScorecard_date() {
        return scorecard_date;
    }

    public void setScorecard_date(String scorecard_date) {
        this.scorecard_date = scorecard_date;
    }
}
