package com.pten.scorecard.view.DetailGrid;

public class ScoreCardDetail {

    private int scorecard_id;
    private String category;
    private String metric;
    private Double points = Double.NaN;
    private String metric_description;
    private Double avg_value = Double.NaN;
    private Double avg_points = Double.NaN;
    private Double raw_value = Double.NaN;
    private Double raw_points = Double.NaN;
    private Double prior_month_value = Double.NaN;
    private Double prior_month_points = Double.NaN;
    private Double two_month_value = Double.NaN;
    private Double two_month_points = Double.NaN;
    private String avg_points_color;
    private String raw_points_color;
    private String prior_month_points_color;
    private String two_month_points_color;
    private String category_order;
    private int display_order;
    private String region_name;
    private String area_name;
    private String rig_name;
    private String scorecard_date;

    public ScoreCardDetail() { }

    public int getScorecard_id() {
        return scorecard_id;
    }

    public void setScorecard_id(int scorecard_id) {
        this.scorecard_id = scorecard_id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public Double getPoints() {
        return points;
    }

    public void setPoints(Double points) {
        this.points = points;
    }

    public String getMetric_description() {
        return metric_description;
    }

    public void setMetric_description(String metric_description) {
        this.metric_description = metric_description;
    }

    public Double getAvg_value() {
        return avg_value;
    }

    public void setAvg_value(Double avg_value) {
        this.avg_value = avg_value;
    }

    public Double getAvg_points() {
        return avg_points;
    }

    public void setAvg_points(Double avg_points) {
        this.avg_points = avg_points;
    }

    public Double getRaw_value() {
        return raw_value;
    }

    public void setRaw_value(Double raw_value) {
        this.raw_value = raw_value;
    }

    public Double getRaw_points() {
        return raw_points;
    }

    public void setRaw_points(Double raw_points) {
        this.raw_points = raw_points;
    }

    public Double getPrior_month_value() {
        return prior_month_value;
    }

    public void setPrior_month_value(Double prior_month_value) {
        this.prior_month_value = prior_month_value;
    }

    public Double getPrior_month_points() {
        return prior_month_points;
    }

    public void setPrior_month_points(Double prior_month_points) {
        this.prior_month_points = prior_month_points;
    }

    public Double getTwo_month_value() {
        return two_month_value;
    }

    public void setTwo_month_value(Double two_month_value) {
        this.two_month_value = two_month_value;
    }

    public Double getTwo_month_points() {
        return two_month_points;
    }

    public void setTwo_month_points(Double two_month_points) {
        this.two_month_points = two_month_points;
    }

    public String getAvg_points_color() {
        return avg_points_color;
    }

    public void setAvg_points_color(String avg_points_color) {
        this.avg_points_color = avg_points_color;
    }

    public String getRaw_points_color() {
        return raw_points_color;
    }

    public void setRaw_points_color(String raw_points_color) {
        this.raw_points_color = raw_points_color;
    }

    public String getPrior_month_points_color() {
        return prior_month_points_color;
    }

    public void setPrior_month_points_color(String prior_month_points_color) { this.prior_month_points_color = prior_month_points_color; }

    public String getTwo_month_points_color() {
        return two_month_points_color;
    }

    public void setTwo_month_points_color(String two_month_points_color) { this.two_month_points_color = two_month_points_color; }

    public String getCategory_order() {
        return category_order;
    }

    public void setCategory_order(String category_order) {
        this.category_order = category_order;
    }

    public int getDisplay_order() {
        return display_order;
    }

    public void setDisplay_order(int display_order) {
        this.display_order = display_order;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    public String getArea_name() {
        return area_name;
    }

    public void setArea_name(String area_name) {
        this.area_name = area_name;
    }

    public String getRig_name() {
        return rig_name;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public String getScorecard_date() {
        return scorecard_date;
    }

    public void setScorecard_date(String scorecard_date) {
        this.scorecard_date = scorecard_date;
    }
}

