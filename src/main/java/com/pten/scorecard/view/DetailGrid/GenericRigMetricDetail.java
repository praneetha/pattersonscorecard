package com.pten.scorecard.view.DetailGrid;

public class GenericRigMetricDetail {
    private int scorecard_id;
    private Double metric;
    private int num_stands;
    private String rig_name;
    private String scorecard_date;

    public GenericRigMetricDetail() {
    }

    public int getScorecard_id() {
        return scorecard_id;
    }

    public void setScorecard_id(int scorecard_id) {
        this.scorecard_id = scorecard_id;
    }

    public Double getMetric() {
        return metric;
    }

    public void setMetric(Double metric) {
        this.metric = metric;
    }

    public int getNum_stands() {
        return num_stands;
    }

    public void setNum_stands(int num_stands) {
        this.num_stands = num_stands;
    }

    public String getRig_name() {
        return rig_name;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public String getScorecard_date() {
        return scorecard_date;
    }

    public void setScorecard_date(String scorecard_date) {
        this.scorecard_date = scorecard_date;
    }
}
