package com.pten.scorecard.view.DetailGrid;

public class AssetInvDetail {
    private int scorecard_id;
    private String rig_name;
    private String category_code;
    private String working_overhead;
    private String oracle_asset_number;
    private String compliant_scanned;
    private String rfid_tag;
    private String last_scanned_date;
    private int num_scanned;
    private int num_assets;
    private String scorecard_date;

    public AssetInvDetail() {
    }

    public int getScorecard_id() {
        return scorecard_id;
    }

    public void setScorecard_id(int scorecard_id) {
        this.scorecard_id = scorecard_id;
    }

    public String getRig_name() {
        return rig_name;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public String getCategory_code() {
        return category_code;
    }

    public void setCategory_code(String category_code) {
        this.category_code = category_code;
    }

    public String getWorking_overhead() {
        return working_overhead;
    }

    public void setWorking_overhead(String working_overhead) {
        this.working_overhead = working_overhead;
    }

    public String getOracle_asset_number() {
        return oracle_asset_number;
    }

    public void setOracle_asset_number(String oracle_asset_number) {
        this.oracle_asset_number = oracle_asset_number;
    }

    public String getCompliant_scanned() {
        return compliant_scanned;
    }

    public void setCompliant_scanned(String compliant_scanned) {
        this.compliant_scanned = compliant_scanned;
    }

    public String getRfid_tag() {
        return rfid_tag;
    }

    public void setRfid_tag(String rfid_tag) {
        this.rfid_tag = rfid_tag;
    }

    public String getLast_scanned_date() {
        return last_scanned_date;
    }

    public void setLast_scanned_date(String last_scanned_date) {
        this.last_scanned_date = last_scanned_date;
    }

    public int getNum_scanned() {
        return num_scanned;
    }

    public void setNum_scanned(int num_scanned) {
        this.num_scanned = num_scanned;
    }

    public int getNum_assets() {
        return num_assets;
    }

    public void setNum_assets(int num_assets) {
        this.num_assets = num_assets;
    }

    public String getScorecard_date() {
        return scorecard_date;
    }

    public void setScorecard_date(String scorecard_date) {
        this.scorecard_date = scorecard_date;
    }
}
