package com.pten.scorecard.view.DetailGrid.OpsManagerDetailGrid;

import com.pten.scorecard.view.DetailGrid.CRIDetail;

public class OpsManagerCRIDetail extends CRIDetail {
    private int superintendent_scorecard_id;
    private String superintendent_owner;

    public OpsManagerCRIDetail() {
    }

    public int getSuperintendent_scorecard_id() {
        return superintendent_scorecard_id;
    }

    public void setSuperintendent_scorecard_id(int superintendent_scorecard_id) {
        this.superintendent_scorecard_id = superintendent_scorecard_id;
    }

    public String getSuperintendent_owner() {
        return superintendent_owner;
    }

    public void setSuperintendent_owner(String superintendent_owner) {
        this.superintendent_owner = superintendent_owner;
    }
}
