package com.pten.scorecard.view.DetailGrid.OpsManagerDetailGrid;

import com.pten.scorecard.view.DetailGrid.WorkOrderDetail;

public class OpsManWorkOrderDetail extends WorkOrderDetail {

    private int superintendent_scorecard_id;
    private String superintendent;

    public OpsManWorkOrderDetail() {
    }

    public int getSuperintendent_scorecard_id() {
        return superintendent_scorecard_id;
    }

    public void setSuperintendent_scorecard_id(int superintendent_scorecard_id) {
        this.superintendent_scorecard_id = superintendent_scorecard_id;
    }

    public String getSuperintendent() {
        return superintendent;
    }

    public void setSuperintendent(String superintendent) {
        this.superintendent = superintendent;
    }
}
