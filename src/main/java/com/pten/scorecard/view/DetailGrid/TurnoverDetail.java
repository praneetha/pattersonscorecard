package com.pten.scorecard.view.DetailGrid;

public class TurnoverDetail {
    private int scorecard_id;
    private String employee_number;
    private String first_name;
    private String last_name;
    private String leaving_reason;
    private String job_name;
    private String latest_hire_date;
    private String period_of_service_term_date;
    private String months_of_service;
    private String rig_name;
    private String scorecard_date;

    public TurnoverDetail() {
    }

    public int getScorecard_id() {
        return scorecard_id;
    }

    public void setScorecard_id(int scorecard_id) {
        this.scorecard_id = scorecard_id;
    }

    public String getEmployee_number() {
        return employee_number;
    }

    public void setEmployee_number(String employee_number) {
        this.employee_number = employee_number;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getLeaving_reason() {
        return leaving_reason;
    }

    public void setLeaving_reason(String leaving_reason) {
        this.leaving_reason = leaving_reason;
    }

    public String getJob_name() {
        return job_name;
    }

    public void setJob_name(String job_name) {
        this.job_name = job_name;
    }

    public String getLatest_hire_date() {
        return latest_hire_date;
    }

    public void setLatest_hire_date(String latest_hire_date) {
        this.latest_hire_date = latest_hire_date;
    }

    public String getPeriod_of_service_term_date() {
        return period_of_service_term_date;
    }

    public void setPeriod_of_service_term_date(String period_of_service_term_date) {
        this.period_of_service_term_date = period_of_service_term_date;
    }

    public String getMonths_of_service() {
        return months_of_service;
    }

    public void setMonths_of_service(String months_of_service) {
        this.months_of_service = months_of_service;
    }

    public String getRig_name() {
        return rig_name;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public String getScorecard_date() {
        return scorecard_date;
    }

    public void setScorecard_date(String scorecard_date) {
        this.scorecard_date = scorecard_date;
    }
}
