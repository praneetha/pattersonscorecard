package com.pten.scorecard.view.SummaryGrid;

import com.pten.scorecard.view.Headers.SummaryHeader;

public class OverallGrid extends SummaryHeader {

    private String scorecard_date;
    private Double overall_score;
    private int area_rank;
    private int region_rank;
    private int patterson_rank;
    private Double region_average;
    private Double division_average;
    private Double patterson_average;

    public OverallGrid() {
    }

    public String getScorecard_date() {
        return scorecard_date;
    }

    public void setScorecard_date(String scorecard_date) {
        this.scorecard_date = scorecard_date;
    }

    public Double getOverall_score() {
        return overall_score;
    }

    public void setOverall_score(Double overall_score) {
        this.overall_score = overall_score;
    }

    public int getArea_rank() {
        return area_rank;
    }

    public void setArea_rank(int area_rank) {
        this.area_rank = area_rank;
    }

    public int getRegion_rank() {
        return region_rank;
    }

    public void setRegion_rank(int region_rank) {
        this.region_rank = region_rank;
    }

    public int getPatterson_rank() {
        return patterson_rank;
    }

    public void setPatterson_rank(int patterson_rank) {
        this.patterson_rank = patterson_rank;
    }

    public Double getRegion_average() {
        return region_average;
    }

    public void setRegion_average(Double region_average) {
        this.region_average = region_average;
    }

    public Double getDivision_average() {
        return division_average;
    }

    public void setDivision_average(Double division_average) {
        this.division_average = division_average;
    }

    public Double getPatterson_average() {
        return patterson_average;
    }

    public void setPatterson_average(Double patterson_average) {
        this.patterson_average = patterson_average;
    }
}
