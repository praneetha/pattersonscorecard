package com.pten.scorecard.view.SummaryGrid;

import com.pten.scorecard.view.Headers.SummaryHeader;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class FinancialGrid  extends SummaryHeader {
    private String scorecard_date;
    private Double cap_spend_total_points;
    private Double cap_spend_value;
    private Double cap_spend_points;
    private Double rm_supp_total_points;
    private Double rm_supp_value;
    private Double rm_supp_points;

}
