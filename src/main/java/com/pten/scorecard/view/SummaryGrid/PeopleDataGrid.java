package com.pten.scorecard.view.SummaryGrid;

import com.pten.scorecard.view.Headers.SummaryHeader;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class PeopleDataGrid extends SummaryHeader {

    private String scorecard_date;
    private Double turnover_total_points;
    private Double  turnover_value;
    private Double  turnover_points;
    private Double  cmr_err_total_points;
    private Double  cmr_err_value;
    private Double  cmr_err_points;


}
