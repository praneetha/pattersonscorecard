package com.pten.scorecard.view.SummaryGrid;

import com.pten.scorecard.view.Headers.SummaryHeader;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class RigPerformanceGrid extends SummaryHeader {
    private Double trip_speed_total_points;
    private Double trip_speed_value;
    private Double trip_speed_points;
    private Double trip_conn_total_points;
    private Double trip_conn_value;
    private Double trip_conn_points;
    private Double drill_conn_total_points;
    private Double drill_conn_value;
    private Double drill_conn_points;
    private Double skid_total_points;
    private Double skid_value;
    private Double skid_points;
    private Double rmct_total_points;
    private Double rmct_value;
    private Double rmct_points;

}
