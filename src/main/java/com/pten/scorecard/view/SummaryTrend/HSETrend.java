package com.pten.scorecard.view.SummaryTrend;

import com.pten.scorecard.view.ScoreCard.ScoreCardRig;

public class HSETrend extends ScoreCardRig {

    private String scorecard_date;
    private Double CRI_points;
    private Double CRI_avg_value;
    private Double CRI_avg_points;
    private Double HPE_points;
    private Double HPE_avg_value;
    private Double HPE_avg_points;
    private Double IMS_start_late_points;
    private Double IMS_start_late_avg_value;
    private Double IMS_start_late_avg_points;
    private Double IMS_end_late_points;
    private Double IMS_end_late_avg_value;
    private Double IMS_end_late_avg_points;

    public HSETrend() {
    }

    public String getScorecard_date() {
        return scorecard_date;
    }

    public void setScorecard_date(String scorecard_date) {
        this.scorecard_date = scorecard_date;
    }

    public Double getCRI_points() {
        return CRI_points;
    }

    public void setCRI_points(Double CRI_points) {
        this.CRI_points = CRI_points;
    }

    public Double getCRI_avg_value() {
        return CRI_avg_value;
    }

    public void setCRI_avg_value(Double CRI_avg_value) {
        this.CRI_avg_value = CRI_avg_value;
    }

    public Double getCRI_avg_points() {
        return CRI_avg_points;
    }

    public void setCRI_avg_points(Double CRI_avg_points) {
        this.CRI_avg_points = CRI_avg_points;
    }

    public Double getHPE_points() {
        return HPE_points;
    }

    public void setHPE_points(Double HPE_points) {
        this.HPE_points = HPE_points;
    }

    public Double getHPE_avg_value() {
        return HPE_avg_value;
    }

    public void setHPE_avg_value(Double HPE_avg_value) {
        this.HPE_avg_value = HPE_avg_value;
    }

    public Double getHPE_avg_points() {
        return HPE_avg_points;
    }

    public void setHPE_avg_points(Double HPE_avg_points) {
        this.HPE_avg_points = HPE_avg_points;
    }

    public Double getIMS_start_late_points() {
        return IMS_start_late_points;
    }

    public void setIMS_start_late_points(Double IMS_start_late_points) {
        this.IMS_start_late_points = IMS_start_late_points;
    }

    public Double getIMS_start_late_avg_value() {
        return IMS_start_late_avg_value;
    }

    public void setIMS_start_late_avg_value(Double IMS_start_late_avg_value) {
        this.IMS_start_late_avg_value = IMS_start_late_avg_value;
    }

    public Double getIMS_start_late_avg_points() {
        return IMS_start_late_avg_points;
    }

    public void setIMS_start_late_avg_points(Double IMS_start_late_avg_points) {
        this.IMS_start_late_avg_points = IMS_start_late_avg_points;
    }

    public Double getIMS_end_late_points() {
        return IMS_end_late_points;
    }

    public void setIMS_end_late_points(Double IMS_end_late_points) {
        this.IMS_end_late_points = IMS_end_late_points;
    }

    public Double getIMS_end_late_avg_value() {
        return IMS_end_late_avg_value;
    }

    public void setIMS_end_late_avg_value(Double IMS_end_late_avg_value) {
        this.IMS_end_late_avg_value = IMS_end_late_avg_value;
    }

    public Double getIMS_end_late_avg_points() {
        return IMS_end_late_avg_points;
    }

    public void setIMS_end_late_avg_points(Double IMS_end_late_avg_points) {
        this.IMS_end_late_avg_points = IMS_end_late_avg_points;
    }
}
