package com.pten.scorecard.view.SummaryTrend;

import com.pten.scorecard.view.ScoreCard.ScoreCardRig;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class EquipmentTrend extends ScoreCardRig {
    private String scorecard_date;
    double npt_total_points;
    double npt_value;
    double npt_points;
    double ops_wo_total_points;
    double ops_wo_value;
    double ops_wo_points;
    double tech_wo_total_points;
    double tech_wo_value;
    double tech_wo_points;
    double tib_tsa_total_wo_points;
    double tib_tsa_wo_value;
    double tib_tsa_wo_points;
    double asset_inventory_total_points;
    double asset_inventory_value;
    double asset_inventory_points;
}
