package com.pten.scorecard.view.SummaryTrend;

import com.pten.scorecard.view.ScoreCard.ScoreCardRig;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class PeopleDataTrend extends ScoreCardRig {

    private String scorecard_date;
    private Double turnover_total_points;
    private Double  turnover_value;
    private Double  turnover_points;
    private Double  cmr_err_total_points;
    private Double  cmr_err_value;
    private Double  cmr_err_points;

}
