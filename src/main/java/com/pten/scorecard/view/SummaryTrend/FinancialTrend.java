package com.pten.scorecard.view.SummaryTrend;

import com.pten.scorecard.view.ScoreCard.ScoreCardRig;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class FinancialTrend extends ScoreCardRig {
    private String scorecard_date;
    private Double cap_spend_total_points;
    private Double cap_spend_value;
    private Double cap_spend_points;
    private Double rm_supp_total_points;
    private Double rm_supp_value;
    private Double rm_supp_points;

}
