package com.pten.scorecard.view.SummaryTrend;

import com.pten.scorecard.view.ScoreCard.ScoreCardRig;

public class OverallTrend extends ScoreCardRig {

    String scorecard_date;
    Double overall_score;

    public OverallTrend() {
    }

    public String getScorecard_date() {
        return scorecard_date;
    }

    public void setScorecard_date(String scorecard_date) {
        this.scorecard_date = scorecard_date;
    }

    public Double getOverall_score() {
        return overall_score;
    }

    public void setOverall_score(Double overall_score) {
        this.overall_score = overall_score;
    }
}


