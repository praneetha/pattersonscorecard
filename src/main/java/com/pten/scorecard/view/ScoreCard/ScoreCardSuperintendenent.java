package com.pten.scorecard.view.ScoreCard;

import com.pten.scorecard.view.ScoreCard.ScoreCard;

public class ScoreCardSuperintendenent extends ScoreCard {

    String superintendent;

    public ScoreCardSuperintendenent() {
    }

    public String getSuperintendent() {
        return superintendent;
    }

    public void setSuperintendent(String superintendent) {
        this.superintendent = superintendent;
    }
}
