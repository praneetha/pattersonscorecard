package com.pten.scorecard.view.ScoreCard;

public class ScoreCardRigSuperintendent extends ScoreCard {
    private String rig_name;
    private String scorecard_date;
    private String superintendent_scorecard_date;
    private int superintendent_scorecard_id;
    private String superintendent;

    public ScoreCardRigSuperintendent() {
    }

    public String getRig_name() {
        return rig_name;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public String getScorecard_date() {
        return scorecard_date;
    }

    public void setScorecard_date(String scorecard_date) {
        this.scorecard_date = scorecard_date;
    }

    public int getSuperintendent_scorecard_id() {
        return superintendent_scorecard_id;
    }

    public void setSuperintendent_scorecard_id(int superintendent_scorecard_id) {
        this.superintendent_scorecard_id = superintendent_scorecard_id;
    }

    public String getSuperintendent() {
        return superintendent;
    }

    public void setSuperintendent(String superintendent) {
        this.superintendent = superintendent;
    }

    public String getSuperintendent_scorecard_date() {
        return superintendent_scorecard_date;
    }

    public void setSuperintendent_scorecard_date(String superintendent_scorecard_date) {
        this.superintendent_scorecard_date = superintendent_scorecard_date;
    }
}
