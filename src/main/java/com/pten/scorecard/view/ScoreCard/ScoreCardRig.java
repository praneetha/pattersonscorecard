package com.pten.scorecard.view.ScoreCard;

public class ScoreCardRig extends ScoreCard {

    private String rig_name;

    public ScoreCardRig() {}

    public ScoreCardRig(String name) {
        this.rig_name = name;
    }

    public String getRig_name() { return rig_name; }

    public void setRig_name(String rig_name) { this.rig_name = rig_name; }


}
