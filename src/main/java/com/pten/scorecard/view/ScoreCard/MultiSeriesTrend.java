package com.pten.scorecard.view.ScoreCard;

public class MultiSeriesTrend {

    int scorecard_id;
    String observation_date;
    String source_name;
    Double observation_score;

    public MultiSeriesTrend() {
    }

    public int getScorecard_id() {
        return scorecard_id;
    }

    public void setScorecard_id(int scorecard_id) {
        this.scorecard_id = scorecard_id;
    }

    public String getObservation_date() {
        return observation_date;
    }

    public void setObservation_date(String observation_date) {
        this.observation_date = observation_date;
    }

    public String getSource_name() {
        return source_name;
    }

    public void setSource_name(String source_name) {
        this.source_name = source_name;
    }

    public Double getObservation_score() {
        return observation_score;
    }

    public void setObservation_score(Double observation_score) {
        this.observation_score = observation_score;
    }
}
