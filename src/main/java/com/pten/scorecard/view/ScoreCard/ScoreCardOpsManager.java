package com.pten.scorecard.view.ScoreCard;

public class ScoreCardOpsManager extends ScoreCard {

    String opsmanager;

    public ScoreCardOpsManager() {
    }

    public String getOpsmanager() {
        return opsmanager;
    }

    public void setOpsmanager(String opsmanager) {
        this.opsmanager = opsmanager;
    }
}
