package com.pten.scorecard.view.ScoreCard;

public class ScoreCard {

    private int scorecard_id;

    public ScoreCard() {
    }

    public int getScorecard_id() {
        return scorecard_id;
    }

    public void setScorecard_id(int scorecard_id) {
        this.scorecard_id = scorecard_id;
    }
}
