package com.pten.ui.service;

import com.pten.Utilities.UtilityLibrary;
import com.pten.ui.data.custom.Glossary;
import com.pten.ui.data.custom.KPIMonthly;
import com.pten.ui.data.custom.Operator;
import com.pten.ui.data.custom.OperatorDetails;
import com.pten.ui.repo.CustomKPIRepo;
import com.pten.ui.util.Mapper;
import com.pten.ui.view.CustomKPIWeekly;
import com.pten.ui.view.CustomRig;
import com.pten.ui.view.custom.Casing;
import com.pten.ui.view.custom.DrillingKPI;
import com.pten.ui.view.custom.TrippingKPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

@Service
public class CustomKPIService {
    private static final Logger log = LoggerFactory.getLogger(CustomKPIService.class);

    @Autowired
    CustomKPIRepo customKPIRepo;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    Mapper mapper;

    @Autowired
    UtilityLibrary myUtils;


    /**
     * Declare @selecteddate datetime =  $P{selectedDate} ;*
     * Declare @end_date datetime = DATEADD(DAY, -1, @selecteddate);
     * DECLARE @start_date datetime = DATEADD(DAY, -6,@end_date);
     * declare @operator varchar(50) =  $P{operator} ;
     */
    public Collection getMetricsMonthly(String operator, Date selecteddate) {
        return mapper.unmarshall(customKPIRepo.getMetricsMonthly(operator, selecteddate), KPIMonthly.class);

    }

    public Collection getMetricsWeekly(String operator, Date selectedDate) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(selectedDate);
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        Date endTime = calendar.getTime();

        calendar = Calendar.getInstance();
        calendar.setTime(endTime);
        calendar.add(Calendar.DAY_OF_YEAR, -6);
        Date startTime = calendar.getTime();

        return mapper.unmarshall(customKPIRepo.getMetricsWeekly(operator, startTime, endTime), CustomKPIWeekly.class);
    }

    /**
     * Adds regional average on each result due to flutura plotting limitations.
     * @param operator
     * @param selectedDate
     * @return
     */
    public Collection getDrilling(String operator, Date selectedDate) {
        Collection<DrillingKPI> kpis = mapper.unmarshall(customKPIRepo.getDrilling(operator, selectedDate), DrillingKPI.class);

        ArrayList<DrillingKPI> avg = new ArrayList<>(
                kpis.stream()
                .filter(k -> k.getRig_name().contains("Regional Average"))
                .collect(Collectors.toList())
        );

        //setting slips to slips as regional average for drilling
        if (avg.size() > 0){
            kpis.forEach(dKpi -> {
                DrillingKPI d = avg.get(0);
                dKpi.setRegional_average_s2s(d.getAvg_s2s());
                dKpi.setRegional_average_s2w(d.getAvg_s2w());
                dKpi.setRegional_average_w2s(d.getAvg_w2s());
            });
        }

        kpis.forEach(k -> {
            k.setAvg_s2s(k.getAvg_s2s().setScale(2, RoundingMode.HALF_EVEN));
            k.setAvg_w2s(k.getAvg_w2s().setScale(2, RoundingMode.HALF_EVEN));
            k.setAvg_s2w(k.getAvg_s2w().setScale(2, RoundingMode.HALF_EVEN));
            k.setRegional_average_s2s(k.getRegional_average_s2s().setScale(2, RoundingMode.HALF_EVEN));
            k.setRegional_average_w2s(k.getRegional_average_w2s().setScale(2, RoundingMode.HALF_EVEN));
            k.setRegional_average_s2w(k.getRegional_average_s2w().setScale(2, RoundingMode.HALF_EVEN));
            if (k.getRig_name().contains("Regional Average")) {
                k.setColor("#A23333");
            }
        });

        return kpis;
    }


    public Collection getGlossary() {
        return mapper.unmarshall(customKPIRepo.getGlossary(), Glossary.class);
    }

    public Collection getOperator() {
        return mapper.unmarshall(customKPIRepo.getOperator(), OperatorDetails.class);
    }

    public Collection getOperators(String operator, Date startTime) {
        return mapper.unmarshall(customKPIRepo.getOperators(operator, startTime), Operator.class);
    }

    public Collection getHeader(String operator, Date selectedDate) {
        return mapper.unmarshall(customKPIRepo.getHeader(operator, selectedDate), CustomRig.class);
    }

    /**
     * Adds regional average on each result due to flutura plotting limitations.
     * @param operator
     * @param selectedDate
     * @return
     */
    public Collection getCasing(String operator, Date selectedDate) {

        Collection<Casing> kpis = mapper.unmarshall(customKPIRepo.getCasing(operator, selectedDate), Casing.class);

        ArrayList<Casing> avg = new ArrayList<>(
                kpis.stream()
                        .filter(k -> k.getRig_name().contains("Regional Average"))
                        .collect(Collectors.toList())
        );

        //setting slips to slips as regional average for casing
        if (avg.size() > 0){
            kpis.forEach(cKpi -> {
                cKpi.setRegional_average_in_slips(avg.get(0).getAvg_in_Slips());
                cKpi.setRegional_average_in_fthr(avg.get(0).getAvg_in_fthr());
            });
        }

        kpis.forEach(k -> {
            if (k.getRig_name().contains("Regional Average")) {
                k.setColor("#A23333");
            }
        });

        return kpis;
    }

    /**
     * Adds regional average on each result due to flutura plotting limitations.
     * @param operator
     * @param selectedDate
     * @return
     */
    public Collection getTripping(String operator, Date selectedDate) {

        Collection<TrippingKPI> kpis = mapper.unmarshall(customKPIRepo.getTripping(operator, selectedDate), TrippingKPI.class);

        ArrayList<TrippingKPI> avg = new ArrayList<>(
                kpis.stream()
                        .filter(k -> k.getRig_name().contains("Regional Average"))
                        .collect(Collectors.toList())
        );

        //setting slips to slips as regional average for tripping
        if (avg.size() > 0){
            TrippingKPI s = avg.get(0);
            kpis.forEach(tKpi -> {
                tKpi.setRegional_average_in(s.getAvg_in_fthr());
                tKpi.setRegional_average_out(s.getAvg_out_fthr());
            });
        }

        kpis.forEach(k -> {
            k.setAvg_in_Slips(k.getAvg_in_Slips().setScale(2, RoundingMode.HALF_EVEN));
            if (k.getRig_name().contains("Regional Average")) {
                k.setColor("#A23333");
            }
        });

        return kpis;
    }
}
