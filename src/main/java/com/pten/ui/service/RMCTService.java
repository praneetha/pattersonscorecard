package com.pten.ui.service;

import com.pten.Utilities.UtilityLibrary;
import com.pten.ui.repo.RMCTRepo;
import com.pten.ui.util.Mapper;
import com.pten.ui.view.rmct.RegionActivations;
import com.pten.ui.view.rmct.RegionByMonth;
import com.pten.ui.view.rmct.RegionByRigType;
import com.pten.ui.view.rmct.RegionTopRigs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class RMCTService {

    private static final Logger log = LoggerFactory.getLogger(RMCTService.class);
    @Autowired
    Mapper mapper;
    @Autowired
    private RMCTRepo rmctRepo;
    @Autowired
    private UtilityLibrary utils;

    public Collection getRMCTByRegionRigTypeTimeRangeAggMonth(String regions, String rigtypes, String starttime, String endtime) {
        Collection result;
        List<String> regionList = Arrays.asList(regions.toUpperCase().split(","));
        List<Integer> rigTypeList = convertRigTypes(rigtypes);

        try {
            result = rmctRepo.getRMCTByRegionRigTypeTimeRangeAggMonth(regionList, rigTypeList, starttime, endtime);
            result.stream().sorted(Comparator.comparing(RegionByMonth::getMove_year).thenComparing(RegionByMonth::getMove_month));
        } catch (ParseException pe) {
            log.debug(pe.getMessage());
            return null;
        }

        return mapper.unmarshall(result, RegionByMonth.class);
    }

    public Collection getRMCTByRegionRigTypeTimeRangeAggRigType(String regions, String rigtypes, String starttime, String endtime) {
        Collection result;
        List<String> regionList = Arrays.asList(regions.toUpperCase().split(","));
        List<Integer> rigTypeList = convertRigTypes(rigtypes);

        try {
            result = new ArrayList<RegionByRigType>(rmctRepo.getRMCTByRegionRigTypeTimeRangeAggRigType(regionList, rigTypeList, starttime, endtime));
        } catch (ParseException pe) {
            log.debug(pe.getMessage());
            return null;
        }

        return mapper.unmarshall(result, RegionByRigType.class);
    }

    public Collection getRMCTReactivatedRegionRigTypeTimeRange(String regions, String rigtypes, String starttime, String endtime) {
        Collection result;
        List<String> regionList = Arrays.asList(regions.toUpperCase().split(","));
        List<Integer> rigTypeList = convertRigTypes(rigtypes);

        try {
            result = new ArrayList<RegionActivations>(rmctRepo.getRMCTReactivatedRegionRigTypeTimeRange(regionList, rigTypeList, starttime, endtime));
        } catch (ParseException pe) {
            log.debug(pe.getMessage());
            return null;
        }

        return mapper.unmarshall(result, RegionActivations.class);
    }

    public Collection getRMCTTopNRigsRegionRigTypeTimeRange(String regions, String rigtypes, String starttime, String endtime) {
        Collection result;
        List<String> regionList = Arrays.asList(regions.toUpperCase().split(","));
        List<Integer> rigTypeList = convertRigTypes(rigtypes);
        try {
            result = new ArrayList<RegionTopRigs>(rmctRepo.getRMCTTopNRigsRegionRigTypeTimeRange(regionList, rigTypeList, starttime, endtime));
        } catch (ParseException pe) {
            log.debug(pe.getMessage());
            return null;
        }

        return mapper.unmarshall(result, RegionTopRigs.class);
    }

    private List<Integer> convertRigTypes(String rigtypes) {
        List<Integer> rigTypeList = null;
        if (rigtypes.trim() != "") {
            rigTypeList = Arrays.stream(rigtypes.split(",")).mapToInt(Integer::parseInt).boxed().collect(Collectors.toList());
        } else {
            rigTypeList = rigTypeList = new ArrayList<Integer>();
        }

        return rigTypeList;
    }




}
