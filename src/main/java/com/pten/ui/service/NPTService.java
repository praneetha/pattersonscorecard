package com.pten.ui.service;

import com.pten.ui.data.GroupType;
import com.pten.ui.data.NPT;
import com.pten.ui.data.SubCode;
import com.pten.ui.repo.NPTRepo;
import com.pten.ui.util.Mapper;
import com.pten.ui.util.ViewBuilder;
import com.pten.ui.view.npt.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class NPTService {
    private static final Logger log = LoggerFactory.getLogger(NPTService.class);

    @Autowired
    NPTRepo repo;

    @Autowired
    Mapper mapper;

    @Autowired
    ViewBuilder viewBuilder;

    public Collection getNPTByRegionSubCodeYQM(Optional<String> type, String startTime, String endTime, Optional<String> subCode) {

        Collection<NPTBySCRLYM> result;
        double npt_overall_avg;
        String custom_label;

        try {

            if (type.get().toLowerCase().equals("percent")) {
                if (subCode.isPresent() && subCode.get().trim() != "") {
                    result = mapper.unmarshall(repo.getNPTPctByRegionSubCodeYQM(startTime, endTime, subCode.get()), NPTBySCRLYM.class);
                } else {
                    result = mapper.unmarshall(repo.getNPTPctByRegionSubCodeYQM(startTime, endTime, null),  NPTBySCRLYM.class);
                }

                npt_overall_avg = result.stream().mapToDouble(a -> a.getValue()).average().getAsDouble();
                custom_label = ("<div style=\"padding:4px;color:white;background:#002060;position:relative;bottom:5px\">" +
                        new BigDecimal(npt_overall_avg).setScale(2, RoundingMode.HALF_UP) + " %</div>");

            } else { // if hours
                if (subCode.isPresent() && subCode.get().trim() != "") {
                    result = mapper.unmarshall(repo.getNPTHoursByRegionSubCodeYQM(startTime, endTime, subCode.get()),  NPTBySCRLYM.class);
                } else {
                    result = mapper.unmarshall(repo.getNPTHoursByRegionSubCodeYQM(startTime, endTime, null),  NPTBySCRLYM.class);
                }

                custom_label = "";
            }

            result.forEach(h -> {

                h.setCustom_label(custom_label);
            });

            return result;

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }


    }


    public Collection getNPTHoursByRegionSubCodeYQM(String startTime, String endTime) throws ParseException {

        List<NPT> view = new ArrayList<>(repo.getNPTHoursByRegionSubCodeYQM(startTime, endTime, null));

        view.sort(
                Comparator.comparing(NPT::getNpt_year)
                        .thenComparing(NPT::getNpt_month)
        );

//        view.forEach(h ->
//            log.debug("{}", h.toString())
//        );

        log.debug("collection size: {}", view.size());

        return view;

    }

    public Collection getNPTHoursByRegionSubCode(String startTime, String endTime) {

        List<NPTBySCRLYM> view = null;
        try {
            view = new ArrayList<>(alignByRegion(repo.getNPTHoursByRegionSubCode(startTime, endTime, null)));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        view.sort(
                Comparator.comparing(NPTBySCRLYM::getRegion_name)
                        .thenComparing(NPTBySCRLYM::getDisplay_text)
        );
//        view.forEach(h ->
//                log.debug("{}", h.toString())
//        );

        return view;
    }

    public Collection<NPTHrPct> getNPTByRegionSubCode(Optional<String> type, String startTime, String endTime, Optional<String> subCode) {

        List<NPTBySCRLYM> result;
        String custom_label;

        try {
            if (type.get().toLowerCase().equals("percent")) {

                if (subCode.isPresent() && subCode.get().trim() != "") {
                    result =  new ArrayList<NPTBySCRLYM>(repo.NPTPercentageByRegion(startTime, endTime, subCode.get()));
                } else {
                    result =  new ArrayList<NPTBySCRLYM>(repo.NPTPercentageByRegion(startTime, endTime, null));
                }
                Collection<NPTHrPct> results =  mapper.unmarshall(result, NPTBySCRLYM.class);

                custom_label = ("<div style=\"padding:4px;color:white;background:#002060;position:relative;bottom:5px\">" +
                        new BigDecimal((results.iterator().next()).getOverall_npt_pct()).setScale(2, RoundingMode.HALF_UP) + " %</div>");

                results.forEach(h -> {
                    h.setCustom_label(custom_label);
                });

                return results;

            } else { // if hours
                if (subCode.isPresent() && subCode.get().trim() != "") {
                    result = new ArrayList<NPTBySCRLYM>(repo.getNPTHoursByRegionSubCode(startTime, endTime, subCode.get()));
                } else {
                    result = new ArrayList<NPTBySCRLYM>(repo.getNPTHoursByRegionSubCode(startTime, endTime, null));
                }
            }

            return mapper.unmarshall(result, NPTBySCRLYM.class);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Collection NPTByRYQM(Optional<String> type, String starttime, String endtime, Optional<String> subCode) {
        try {

            if (type.get().toLowerCase().equals("percent")) {

                if (subCode.isPresent() && subCode.get().trim() != "") {
                    return new ArrayList<>(repo.getNPTPercentByRYQM(starttime, endtime, subCode.get(), -99));
                } else {
                    return new ArrayList<>(repo.getNPTPercentByRYQM(starttime, endtime, null, -99));
                }
            } else { // if hours
                if (subCode.isPresent() && subCode.get().trim() != "") {
                    return new ArrayList<>(repo.getNPTHoursByRYQM(starttime, endtime, subCode.get()));
                } else {
                    return new ArrayList<>(repo.getNPTHoursByRYQM(starttime, endtime, null));
                }
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;

    }

    public Collection NPTByYQM(Optional<String> type, String region, String starttime, String endtime, Optional<String> rigType, Optional<String> subCodeText) {

        try {

            List<NPTByRYQM> results;
            double npt_overall_avg;
            String custom_label;
            int rigTypeId = (!rigType.isPresent() || "All Rig Types".trim().equalsIgnoreCase(rigType.get().trim())?-99:Integer.parseInt(rigType.get()));

            if (type.get().equalsIgnoreCase("percent")) {
                if (subCodeText.isPresent() && subCodeText.get().trim() != "") {
                    results = new ArrayList<NPTByRYQM>(repo.getNPTPercentByRYQM(starttime, endtime, subCodeText.get(), rigTypeId));
                }else{
                    results = new ArrayList<NPTByRYQM>(repo.getNPTPercentByRYQM(starttime, endtime, null, rigTypeId));
                }

                npt_overall_avg = results.stream().filter(f -> f.getRegion_name().equalsIgnoreCase(region))
                        .collect(Collectors.toList()).stream().mapToDouble(v -> v.getValue()).average().getAsDouble();

                custom_label = ("<div style=\"padding:4px;color:white;background:#002060;position:relative;bottom:5px\">" +
                        new BigDecimal(npt_overall_avg).setScale(2, RoundingMode.HALF_UP) + " %</div>");

            } else { // if hours
                if (subCodeText.isPresent() && subCodeText.get().trim() != "") {
                    results = new ArrayList<NPTByRYQM>(repo.getNPTHoursByYQM(starttime, endtime, rigTypeId, subCodeText.get()));
                }else{
                    results = new ArrayList<NPTByRYQM>(repo.getNPTHoursByYQM(starttime, endtime, rigTypeId, null));
                }

                npt_overall_avg = Double.NaN;
                custom_label = "";
            }

            results = results.stream().filter(f -> f.getRegion_name().equalsIgnoreCase(region))
                    .collect(Collectors.toList());

            results.forEach(h -> {
                h.setOverall_npt_pct(npt_overall_avg);
                h.setCustom_label(custom_label);
            });

            return results;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Collection NPTHoursByRYQM(String starttime, String endtime) {

        List<NPTByRYQM> view = null;
        try {
            view = new ArrayList<>(repo.getNPTHoursByRYQM(starttime, endtime, null));

            view.sort(
                    Comparator.comparing(NPTByRYQM::getRegion_name)
                            .thenComparing(NPTByRYQM::getNpt_year)
                            .thenComparing(NPTByRYQM::getNpt_month)
            );

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return view;

    }

    public String NPTRegionHoursBySubCode(String region, String starttime, String endtime, Optional<String> rigType) {
        int rigTypeId = (!rigType.isPresent() || "All Rig Types".trim().equalsIgnoreCase(rigType.get().trim())?-99:Integer.parseInt(rigType.get()));
        return repo.NPTRegionHoursBySubCode(region, starttime, endtime, rigTypeId).toString();
    }

    public Collection NPTHoursByRYQM(String region, String starttime, String endtime) {
        ArrayList<NPTByRYQM> view;

        if (null != region) {

            view = new ArrayList<>(repo.getNPTHoursByRYQMperRegion(region, starttime, endtime));

            view.sort(
                    Comparator.comparing(NPTByRYQM::getNpt_year)
                            .thenComparing(NPTByRYQM::getNpt_month)
                            .thenComparing(NPTByRYQM::getRegion_name)
            );

            return view;

        } else {
            return NPTHoursByRYQM(starttime, endtime);
        }
    }


    public Collection getNPTHoursBySCRL(String riglist, String starttime, String endtime) {
        try {
            return repo.getNPTHoursOverall(riglist.split(","), starttime, endtime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Collection NPTByRig(Optional<String> type, String region, String starttime, String endtime, Optional<String> rigType, Optional<String> subCodeText) {
        int rigTypeId = (!rigType.isPresent() || "All Rig Types".trim().equalsIgnoreCase(rigType.get().trim())?-99:Integer.parseInt(rigType.get()));
        Collection result;
        try {
            if (type.get().equalsIgnoreCase("percent")) {
                if (subCodeText.isPresent() && subCodeText.get().trim() != "") {
                    result = new ArrayList<NPTHrPct>(repo.getNPTPercentByRig(region, starttime, endtime, rigTypeId, subCodeText.get()));
                }else{
                    result = new ArrayList<NPTHrPct>(repo.getNPTPercentByRig(region, starttime, endtime, rigTypeId, null));
                }
            } else {
                if (subCodeText.isPresent() && subCodeText.get().trim() != "") {
                    result = new ArrayList<NPTHrPct>(repo.getNPTHoursByRig(region, starttime, endtime, rigTypeId, subCodeText.get()));
                }else{
                    result = new ArrayList<NPTHrPct>(repo.getNPTHoursByRig(region, starttime, endtime, rigTypeId, null));
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }

        return mapper.unmarshall(result, NPTHrPct.class);

    }

    public Collection getNPTByRigType(Optional<String> type, Optional<String> region, String starttime, String endtime, Optional<String> subCode) {
        Collection result;
        String subCodeTxt = (subCode.isPresent() && subCode.get().trim() != "")?subCode.get().trim():null;

        try {
            if (type.get().equalsIgnoreCase("percent")) {
                if (region.isPresent() && !"".equalsIgnoreCase(region.get().trim())){
                    result = new ArrayList<>(repo.getNPTPercentByRigType(region.get(), starttime, endtime, subCodeTxt));
                }else{
                    result = new ArrayList<>(repo.getNPTPercentByRigType(null, starttime, endtime, subCodeTxt));
                }
            } else { // if hours
                if (region.isPresent() && !"".equalsIgnoreCase(region.get().trim())){
                    result = new ArrayList<>(repo.getNPTHoursByRigType(region.get(), starttime, endtime, subCodeTxt));
                }else{
                    result = new ArrayList<>(repo.getNPTHoursByRigType(null, starttime, endtime, subCodeTxt));
                }
            }

            return mapper.unmarshall(result, NPTHrPct.class);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Collection getNPTHoursByRigType(String region, String starttime, String endtime) {
        try {
            return repo.getNPTHoursByRigType(region, starttime, endtime, null);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Collection NPTByRYM(Optional<String> type, String region, String starttime, String endtime, Optional<String> rigType, Optional<String> subCodeText) {
        int rigTypeId = (!rigType.isPresent() || "All Rig Types".trim().equalsIgnoreCase(rigType.get().trim())?-99:Integer.parseInt(rigType.get()));
        Collection result;

        try {
            if (type.get().equalsIgnoreCase("percent")) {
                if (subCodeText.isPresent() && subCodeText.get().trim() != "") {
                    result = new ArrayList<NPTBySCRLYM>(repo.getNPTPercentByRYM(region, starttime, endtime, rigTypeId, subCodeText.get()));
                }else{
                    result = new ArrayList<NPTBySCRLYM>(repo.getNPTPercentByRYM(region, starttime, endtime, rigTypeId, null));
                }
            } else { // if hours
                if (subCodeText.isPresent() && subCodeText.get().trim() != "") {
                    result = new ArrayList<NPTBySCRLYM>(repo.getNPTHoursByRYM(region, starttime, endtime, rigTypeId, subCodeText.get()));
                }else{
                    result = new ArrayList<NPTBySCRLYM>(repo.getNPTHoursByRYM(region, starttime, endtime, rigTypeId, null));
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }

        return mapper.unmarshall(result, NPTBySCRLYM.class);

    }

    private Collection<NPTBySCRLYM> alignByRegion(Collection<NPTBySCRLYM> hrs) {
        Set<NPTBySCRLYM> result = hrs.stream().collect(Collectors.toSet());

        Set<String> regions = new TreeSet<>();
        Set<String> displayTexts = new TreeSet<>();
        Map<String, String> colormap = new HashMap<>();

        for (NPTBySCRLYM hr : result) {
            regions.add(hr.getRegion_name());
            displayTexts.add(hr.getDisplay_text());
            colormap.put(hr.getDisplay_text(), hr.getDisplay_color());
        }

        log.debug("regions: {}, size: {}", regions, regions.size());

        for (String displayText : displayTexts) {
            for (String region : regions) {
                //generate region - displaytext combinations.
                result.add(new NPTBySCRLYM(displayText, region));
            }
        }

        for (NPTBySCRLYM hr : result) {
            hr.setDisplay_color(colormap.get(hr.getDisplay_text()));
        }

        return result;
    }

//    private Collection<NPTByRYQM> alignRYM(Collection<NPTByRYQM> hrs) {
//        log.debug("aligning: {}", hrs.size());
//
//        Set<String> regionNames = hrs.stream().map(NPTByRYQM::getRegion_name).collect(Collectors.toSet());
//
//        //get the entire input collection
//        Set<NPTByRYQM> result = new TreeSet<>(hrs);
//
//        log.debug("regions set size: {}", regionNames.size());
//
//        int min, max;
//
//        NPT hr = Collections.min(hrs, Comparator.comparing(c -> c.getNpt_year()));
//
//        min = hr.getNpt_year();
//        hr = Collections.max(hrs, Comparator.comparing(c -> c.getNpt_year()));
//        max = hr.getNpt_year();
//
//        NPTByRYQM tmp;
//
//        // for all region, try to insert a new record, for each month
//        // inserting blanks
//        for (String region : regionNames) {
//            //generate all months from year to year
//            for (int j = min; j <= max; j++) {
//                for (int i = 1; i <= 12; i++) {
//                    tmp = new NPTByRYQM();
//                    tmp.setNpt_year(j);
//                    tmp.setNpt_month(i);
//                    tmp.setRegion_name(region);
//                    if (result.add(tmp))
//                        log.debug("{}", tmp);
//                }
//            }
//
//        }
//        return result;
//    }

    public Collection<NPTHrPct> getNPT(Optional<String> type, String riglist, String starttime, String endtime) {
        try {

            List<NPTHrPct> temp = new ArrayList<NPTHrPct>();
            Double overall_avg;

            if (type.get().toLowerCase().equals("percent")) {
                temp = new ArrayList<NPTHrPct>(repo.getNPTPctBySCRL(starttime, endtime, riglist.split(",")));
                overall_avg = new BigDecimal(temp.stream().mapToDouble(a -> a.getValue()).average().getAsDouble()).setScale(2, RoundingMode.HALF_UP).doubleValue();

                NPTHrPct avgOverall = new NPTHrPct();
                avgOverall.setRig_name("Avg");
                avgOverall.setDisplay_color("#FF0000");
                avgOverall.setValue(overall_avg);
                temp.add(avgOverall);

            } else { // if hours
                temp = new ArrayList<>(repo.getNPTHoursBySCRL(starttime, endtime, riglist.split(",")));
                overall_avg = Double.NaN;
            }



            temp.forEach(h -> {
                h.setOverall_npt_pct(overall_avg.doubleValue());
                h.setCustom_label("<div style=\"padding:4px;color:white;background:#002060;position:relative;bottom:5px\">" + overall_avg.doubleValue() + " %</div>");
            });



            return temp;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Object getNPTBySCRLYM(Optional<String> type, String riglist, String starttime, String endtime) {

        Collection<NPTBySCRLYM> result = null;
        Double max;

        String[] rigList = riglist.split(",");

        try {
            if (type.get().toLowerCase().equals("percent")) {
                result = repo.getNPTPctBySCRLYM(starttime, endtime, rigList);
                max = result.stream().mapToDouble(a -> a.getValue()).max().getAsDouble() * 1.05;

            } else { // if hours
                result = repo.getNPTHoursBySCRLYM(starttime, endtime, rigList);
                max = result.stream().collect(Collectors.groupingBy(g -> g.getMonth_short_text(),
                        Collectors.summingDouble(v -> v.getValue()))).values().stream().mapToDouble(a -> a).max().getAsDouble() * 1.05;

            }

            result = viewBuilder.alignBySCRLYM(result);

            result.forEach(h -> {
                h.setMax_value(max);
            });

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return result;
    }

    public Collection getGroupTypes() {
        ArrayList<GroupType> groupTypes = new ArrayList();

        groupTypes.add(new GroupType("Month", "M"));
        groupTypes.add(new GroupType("Quarter", "Q"));
        groupTypes.add(new GroupType("Week", "W"));

        return groupTypes;
    }

    public Collection getNPTRegionRigMonthAverageSort(String region, String startTime, String endTime) {

        List<NPT> view = null; //new ArrayList<>(repo.getNPTHoursByRegionRigMonthAverageSort(region, startTime, endTime));

        view.sort(
                Comparator.comparing(NPT::getNpt_year)
                        .thenComparing(NPT::getNpt_month)
        );

//        view.forEach(h ->
//            log.debug("{}", h.toString())
//        );

        log.debug("collection size: {}", view.size());

        return view;
    }

    public Collection NPTByTimegroups(Optional<String> type, String regionlist, String startTime, String endTime, String grouptype) {

        Collection<NPTByTimeGroup> result = null;
        Double max_value;
        String custom_label;
        Double overall_npt_pct;

        try {
            if (type.get().toLowerCase().equals("percent")) {
                result = new ArrayList<NPTByTimeGroup>(repo.getNPTPercentByTimegroups(startTime, endTime, regionlist.split(","), grouptype));
                max_value = result.stream().mapToDouble(v -> v.getValue()).max().getAsDouble() * 1.05;
                overall_npt_pct = result.stream().mapToDouble(v -> v.getValue()).average().getAsDouble();
                custom_label = ("<div style=\"padding:4px;color:white;background:#002060;position:relative;bottom:5px\">" +
                        new BigDecimal(overall_npt_pct).setScale(2, RoundingMode.HALF_UP) + " %</div>");
            } else { // if hours
                result = new ArrayList<NPTByTimeGroup>(repo.getNPTHoursByTimegroups(startTime, endTime, regionlist.split(","), grouptype));
                max_value = result.stream().collect(Collectors.groupingBy(g -> g.getGroup_start_date_text(),
                        Collectors.groupingBy(z -> z.getRegion_name(), Collectors.summingDouble(
                                a -> a.getValue())))).entrySet().stream().flatMapToDouble(
                        b -> b.getValue().entrySet().stream().mapToDouble(c -> c.getValue())).max().getAsDouble() * 1.05;

                overall_npt_pct = Double.NaN;
                custom_label = "";
            }

            result.forEach(h ->
                    {
                        h.setOverall_npt_pct(overall_npt_pct);
                        h.setCustom_label(custom_label);
                        h.setMax_value(max_value);
                    }
            );

        } catch (ParseException e) {
            e.printStackTrace();
        }


        return result;
    }

    public Collection getWellNptPercent(Collection<Long> wells) {
        Collection<NPTWrapped> result;

        ArrayList<Percent> percents = new ArrayList<>(repo.getWellNptPercentage(wells));


        percents.forEach(p -> {
            p.setCustomLabel("<div style=\"padding:4px;color:white;background:#002060;position:relative;bottom:5px\">" + new BigDecimal(p.getOverall_avg()).setScale(2, RoundingMode.HALF_UP).doubleValue() + " %</div>");
        });


        Collection aligned = viewBuilder.alignNPTByYM(percents, NPT::getDisplay_text);

        //add Series to all NPTs
        result = viewBuilder.getWrappedNPT(aligned);
        for (NPTWrapped nptw: result) {
            nptw.add("series","percent");
        }



        return result;
    }

    public Collection getWellNptHours(Collection<Long> wells) {
        List result = new ArrayList();

        Collection<SubCode> codes = repo.getSubcodes();

        ArrayList<NPT> hours = new ArrayList<NPT>(viewBuilder.alignNPTByYM(repo.getWellNptHours(wells)));

        for (NPT npt : hours) {
            if (null == npt.getNpt_month_long_text()) {
                npt.setNpt_month_long_text(viewBuilder.getLongMonth(npt.getNpt_month()));
                npt.setNpt_month_short_text(viewBuilder.getShortMonth(npt.getNpt_month()));
            }

            npt.setDisplay_color(codes.stream()
                    .filter(subCode -> subCode.getName().equals(npt.getDisplay_text()))
                    .findFirst().get().getColor()
            );

            //TODO: comment once flutura fixes NaN in this particular chart
//            if (npt.getNpt_hours() == 0){
//                npt.setNpt_hours(Double.NaN);
//            }

            result.add(npt);
        }

        Collections.sort(result);

        return result;
    }

    public Collection getBreakdown(Collection<Long> wells) {
        return repo.getNptHoursBySubCode(wells);
    }

    public Collection getNptHoursAverage(Collection<Long> wells) {
        return repo.getNptHoursAverage(wells);
    }

    public Collection getRigs(String region) {
        return repo.getRigs(region);
    }
}
