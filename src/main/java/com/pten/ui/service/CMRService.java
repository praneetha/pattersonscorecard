package com.pten.ui.service;

import com.pten.Utilities.DateUtils.DateTimeUtils;
import com.pten.Utilities.UtilityLibrary;
import com.pten.ui.data.CMRDetails;
import com.pten.ui.data.RigCMRHoursByMonthObj;
import com.pten.ui.data.WellActivity;
import com.pten.ui.data.WellActivityROP;
import com.pten.ui.repo.CMRRepo;
import com.pten.ui.util.Mapper;
import com.pten.ui.util.ViewBuilder;
import com.pten.ui.view.*;
import com.pten.ui.view.cmr.ROPDepth;
import com.pten.ui.view.cmr.TrippingStand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CMRService {
    private static final Logger log = LoggerFactory.getLogger(CMRService.class);

    @Autowired
    CMRRepo cmrRepo;

    @Autowired
    Mapper mapper;

    @Value("${export.rootPath}")
    String rootPath;

    @Autowired
    UtilityLibrary myUtils;

    @Autowired
    ViewBuilder viewBuilder;

    public Collection getRigCMRPercentByMonth(String rigname, String startTime, String endTime, Optional<String> subCodeText) {

        List<RigCMRPercentByMonth> view;
        if (subCodeText.isPresent() && subCodeText.get().trim() != "") {
            view = new ArrayList<>(cmrRepo.getRigCMRPercentByMonth(rigname, startTime, endTime, subCodeText.get()));
        } else {
            view = new ArrayList<>(cmrRepo.getRigCMRPercentByMonth(rigname, startTime, endTime, null));
        }

        Collection aligned = viewBuilder.alignRigCMRPercentByMonth(view);

        List<RigCMRPercentByMonth> sorted = new ArrayList<>(aligned);

        sorted.sort(
                Comparator.comparing(RigCMRPercentByMonth::getNpt_year)
                        .thenComparing(RigCMRPercentByMonth::getNpt_month)
        );

        // post processing for view(sorted) object / building view from data model / calculated values
        sorted.forEach(h -> {
            h.setNpt_percentage_pct(
                    BigDecimal.valueOf(h.getNpt_percentage() * 100)
                            .setScale(2, RoundingMode.HALF_UP).doubleValue());
            h.setOverall_npt_percentage(
                    BigDecimal.valueOf(h.getOverall_npt_percentage() * 100)
                            .setScale(2, RoundingMode.HALF_UP).doubleValue());
            h.setCustom_label("<div style=\"padding:4px;color:white;background:#002060;position:relative;bottom:5px\">" +
                    h.getOverall_npt_percentage() + " %</div>");
            // log.debug("{}", h.toString());
        });

        log.debug("collection size: {}", sorted.size());

        return sorted;
    }

    public Collection getRigCMRHoursByMonth(String rigname, String startTime, String endTime, String subCodeText) {

        List<RigCMRHoursByMonthObj> view;

        view = new ArrayList<>(cmrRepo.getRigCMRHoursByMonth(rigname, startTime, endTime, subCodeText));

        Collection aligned = viewBuilder.alignRigCMRHoursByMonth(view);

        List<RigCMRHoursByMonthObj> sorted = new ArrayList<>(aligned);

        sorted.sort(
                Comparator.comparing(RigCMRHoursByMonthObj::getNpt_year)
                        .thenComparing(RigCMRHoursByMonthObj::getNpt_month)
        );

        log.debug("collection size: {}", sorted.size());

        return sorted;
    }

    public Collection RigCMRDetailsBySubSubCode(String rigName, String startTime, String endTime) {
        return new ArrayList<CMRDetails>(cmrRepo.getCMRDetails(rigName, startTime, endTime));
    }

    public Collection RigCMRDetailsBySubSubCode(String rigName, String startTime, String endTime, String subCode) {
        ArrayList<CMRDetails> view;

        if (null != subCode && !"".equals(subCode.trim())) {

            view = new ArrayList<>(cmrRepo.getCMRDetails(rigName, startTime, endTime, subCode));

            return view;

        } else {
            return RigCMRDetailsBySubSubCode(rigName, startTime, endTime);
        }
    }


    public Collection RigCMRPerformanceMetricsWellList(String wellList, Boolean alias) {
        List<PerformanceMetrics> view;
        view = new ArrayList<>(cmrRepo.getLastTwelveGrid(wellList, alias));
        return view;
    }

    public Collection RigCMROperators() {
        List<Operator> view;
        view = new ArrayList<>(cmrRepo.getCMROperators());
        view.add(0, new Operator("All Operators"));
        return view;

    }

    public Collection RigCMROperatorRegions(String operator) {

        List<String> operatorList = Arrays.asList(operator.split(","));
        String operatorListCS = sqlifylist(Arrays.asList(operator.split(",")), "All Operators", true);

        List<Region> view;
        ;
        view = new ArrayList<Region>(cmrRepo.getCMROperatorRegions(operatorListCS));
        view.add(0, new Region("All Regions"));
        return view;

    }

    public Collection RigCMROperatorRegionRigTypes(String operator, String region) {

        String operatorListCS = sqlifylist(Arrays.asList(operator.split(",")), "All Operators", true);
        String regionListCS = sqlifylist(Arrays.asList(region.split(",")), "All Regions", true);

        List<RigType> view;
        view = new ArrayList<RigType>(cmrRepo.getCMROperatorRegionRigTypes(operatorListCS, regionListCS));
        view.add(0, new RigType(-99, "All Rig Types"));
        return view;
    }

    public Collection RigCMROperatorRegionRigTypes(String operator, String region, String startTime, String endTime) {

        List<String> operatorListCS = checkForAllType(Arrays.asList(operator.split(",")), "All Operators", false);
        List<String> regionListCS = checkForAllType(Arrays.asList(region.split(",")), "All Regions", false);
        List<RigType> result;

        try {
            result = new ArrayList<RigType>(mapper.unmarshall(cmrRepo.getCMROperatorRegionRigTypes(operatorListCS, regionListCS, startTime, endTime), RigType.class));
            result.add(0, new RigType(-99, "All Rig Types"));
            return result;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }

    }

    public Collection RigCMRRiglistbyOperatorRegionRigType(String operator, String region, String rigType) {
        String operatorListCS = sqlifylist(Arrays.asList(operator.split(",")), "All Operators", true);
        String regionListCS = sqlifylist(Arrays.asList(region.split(",")), "All Regions", true);
        //String rigTypeListCS = sqlifylist(Arrays.asList(region.split(",")), "-99", false);

        List<RigType> view;
        view = new ArrayList<RigType>(cmrRepo.getCMROperatorRegionRigTypeRigList(operatorListCS, regionListCS, rigType));
        return view;
    }

    public Collection RigCMRRigListByTimeType(String startTime, String endTime, String rigType) {
        Collection result;

        int rigTypeId = (rigType == null || "All Rig Types".trim().equalsIgnoreCase(rigType.trim()) ? -99 : Integer.parseInt(rigType));
        try {
            result = new ArrayList<Rig>(cmrRepo.getCMRORigTypeTimeRigList(startTime, endTime, rigTypeId, Optional.empty()));
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }

        return mapper.unmarshall(result, Rig.class);

    }


    public Collection RigCMRRigListByTimeType(String startTime, String endTime, String rigType, Optional<Integer> selection) {
        Collection result;

        int rigTypeId = (rigType == null || "All Rig Types".trim().equalsIgnoreCase(rigType.trim()) ? -99 : Integer.parseInt(rigType));
        try {
            result = new ArrayList<Rig>(cmrRepo.getCMRORigTypeTimeRigList(startTime, endTime, rigTypeId, selection));
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }

        return mapper.unmarshall(result, Rig.class);

    }

    private String sqlifylist(List<String> items, String allType, boolean isString) {

        long matches = items.stream().filter(str -> str.equalsIgnoreCase(allType)).collect(Collectors.counting());
        if (matches > 0 || (items.size() == 1 && items.get(0).trim() == "")) {
            items = new ArrayList<String>();
            items.add(0, allType);
        }
        if (isString) {
            return items.stream().collect(Collectors.joining("','", "'", "'")).toString();
        } else {
            return items.stream().collect(Collectors.joining(",")).toString();
        }
    }

    private List<String> checkForAllType(List<String> items, String allType, boolean isString) {

        long matches = items.stream().filter(str -> str.equalsIgnoreCase(allType)).collect(Collectors.counting());
        if (matches > 0 || (items.size() == 1 && items.get(0).trim() == "")) {
            return null;
        } else {
            return items;
        }
    }

    /**
     * List of wells
     *
     * @return collection of strings.
     */
    //todo: move wells to it's own service.
    public Collection getWells(String rig, int limit) {
        //todo: rename to getLast(int) once moved to proper service.
        return cmrRepo.getLastCompletedWells(rig, limit);
    }

    public Collection getWells(Collection<Long> wells) {
        return cmrRepo.getWells(wells);
    }


    public Collection getRigCMRPercentByMonth(String rigname, List<Long> wells, String startTime, String endTime) {
        return cmrRepo.getRigCMRPercentByMonth(rigname, wells, startTime, endTime);
    }

    public Collection getWellsDailyDepth(Collection<Long> wells) {

        return cmrRepo.getWellDailyDepth(wells);
    }

    public Collection getWellActivtyAllWells() {
        List<WellActivity> result;
        try {
            result = new ArrayList<WellActivity>(cmrRepo.getTimePerActivityAllRigsWells());
        } catch (ParseException e) {
            return null;
        }

        return mapper.unmarshall(result, WellActivity.class);
    }

    public Collection getRopDepthData(Collection<Long> wells){
        List<ROPDepth> result;
        try {
            result = new ArrayList<ROPDepth>(cmrRepo.getROPDepthData(wells));
        } catch (ParseException e) {
            return null;
        }

        return mapper.unmarshall(result, ROPDepth.class);
    }

    public Collection getTrippingData(Collection<Long> wells){
        List<TrippingStand> result;
        try {
            result = new ArrayList<TrippingStand>(cmrRepo.getTrippingStandsData(wells));
        } catch (ParseException e) {
            return null;
        }

        return mapper.unmarshall(result, TrippingStand.class);
    }



    public Collection getAllRegions() {
        List<Region> result;
        result = new ArrayList<Region>(cmrRepo.getCMRRegions());
        result.add(0, new Region("All Regions"));

        return result;
    }

    public ResponseEntity OpenFile(String pdf_type, String rig_name, String date_input) throws FileNotFoundException {
        ResponseEntity<InputStreamResource> response;
        String file_name = "";

        if (date_input.isEmpty()) {
            long report_date = cmrRepo.getLastReportDate(rig_name);
            date_input = DateTimeUtils.convertUnixDate(report_date);
        } else if (date_input.toUpperCase().equals("TODAY")) {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDateTime now = LocalDateTime.now();
            date_input = dtf.format(now);
        }

        if (pdf_type.toUpperCase().equals("CMR") && DateTimeUtils.isNumeric(rig_name) && DateTimeUtils.isDate(date_input)) {
            file_name = String.format("cmr_pat-uti%1$s_%2$s.pdf", rig_name, date_input.replace("-", "_"));
            log.info(file_name);
        } else if (pdf_type.toUpperCase().equals("TOUR") && DateTimeUtils.isNumeric(rig_name) && DateTimeUtils.isDate(date_input)) {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            date_input = LocalDate.parse(date_input, dtf).minusDays(1).toString();
            file_name = String.format("tour_all-in-one_pat-uti_%1$s_%2$s.pdf", rig_name, date_input.replace("-", ""));
            log.info(file_name);
        } else {
            log.info("Bad Input!");
            response = new ResponseEntity(HttpStatus.BAD_REQUEST);
            return response;
        }

        String fileName = String.format("%1$s"+File.separator+"%2$s"+File.separator+"%3$s"+File.separator+"%4$s", rootPath, pdf_type, rig_name, file_name);

        log.info(fileName);

        File file = new File(fileName);
        if (file.exists()) {
            response = new ResponseEntity<>(
                    new InputStreamResource(new FileInputStream(file))
                    , HttpStatus.OK
            );
        } else {
            fileName = String.format("%1$s"+File.separator+"%2$s", rootPath, "NoContent.pdf");
            log.info(fileName);
            file = new File(fileName);
            log.info("No Content!");

            response = new ResponseEntity<>(
                    new InputStreamResource(new FileInputStream(file))
                    , HttpStatus.OK
            );
            //response = new ResponseEntity( HttpStatus.NO_CONTENT);
        }

        return response;
    }
    public Collection getWellActivtyAllWellsROP(){
        List<WellActivityROP> result;
        try {
            result = new ArrayList<WellActivityROP>(cmrRepo.getActivityAllRigsWellsROP());
        } catch (Exception e) {
            return null;
        }

        return mapper.unmarshall(result, WellActivityROP.class);
    }

    public Collection getRopDepthDataTop() {

        List<String> result;
        result = new ArrayList<String>(cmrRepo.getRopDepthDataTop());

        return result;
    }

    public Collection getFootagePerDayWells(Date starttime, Date endtime, String county, String stateProvince, boolean alias) {
        return cmrRepo.getFootagePerDayWells(starttime, endtime, county, stateProvince, alias);
    }

    public Collection getTopFootagePerDayWells(String region) {
        return cmrRepo.getTopFootagePerDayWells(region);
    }

    public Collection getTopFootagePerDayWells(Date starttime, Date endtime, String county, String stateProvince, boolean alias) {
        return cmrRepo.getTopFootagePerDayWells(starttime, endtime, county, stateProvince, alias);
    }

    public Collection getTopWellsByFootageAndSpudToTDLegend(String region) {
        return cmrRepo.getTopWellsByFootageAndSpudToTDLegend(region);
    }


    public Collection getSpudToTDWells(String region) {
        return cmrRepo.getSpudToTDWells(region);
    }

    public Collection getSpudToTDWells(Date starttime, Date endtime, String county, String stateProvince, boolean alias) {
        return cmrRepo.getSpudToTDWells(starttime, endtime, county, stateProvince, alias);
    }

    public Collection getLastProjects(String rig_name, int limit) {
        return cmrRepo.getLastProjects(rig_name, limit);
    }

    public Collection getCasingStatus(Collection<Long> welllist) {
        return cmrRepo.getCasingStatus(welllist);
    }

    public Collection getCompletedWells(String region, Date startTime, boolean alias) {
        return cmrRepo.getCompletedWells(region, startTime, alias);
    }

    public Collection getCompletedWellsList(String region, Date startTime) {
        return cmrRepo.getCompletedWellsList(region, startTime);
    }
    public Collection getDepth(Collection<Long> wells) {
        return cmrRepo.getDepth(wells);
    }

    public Collection getJobWells(String rig, String job_no) {
        return cmrRepo.getJobWells(rig, job_no);
    }
}

