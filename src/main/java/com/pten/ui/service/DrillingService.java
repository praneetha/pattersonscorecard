package com.pten.ui.service;

import com.pten.ui.data.drilling.*;
import com.pten.ui.repo.DrillingRepo;
import com.pten.ui.util.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Date;

@Service
public class DrillingService {
    private static final Logger log = LoggerFactory.getLogger(DrillingService.class);

    @Autowired
    DrillingRepo drillingRepo;

    @Autowired
    Mapper mapper;

    public Collection getConnectionTime(Collection<Long> wells, Date startTime, Date endTime, String entityType, String section, String order, int returnCount, int userId) {
        return mapper.unmarshall(drillingRepo.getConnectionTime(wells, startTime, endTime, entityType, section, order, returnCount, userId), Times.class);
    }

    public Collection getTimesTrend(Collection<Long> wells, Date startTime, Date endTime, String entityType, String direction, String holestatus, String order, int userId) {
        return mapper.unmarshall(drillingRepo.getTimesTrend(wells, startTime, endTime, direction, holestatus, order, userId), TimesTrend.class);
    }

    public Collection getConnectionDistribution(Collection<Long> wells, Date startTime, Date endTime, String type, String section, int userId) {
        return mapper.unmarshall(drillingRepo.getConnectionDistribution(wells, startTime, endTime, type, section, userId), ConnectionDistribution.class);
    }

    public Collection getS2WConnectionTime(Collection<Long> wells, Date startTime, Date endTime, String entityType, String section, String order, int returnCount, int userId) {
        return mapper.unmarshall(drillingRepo.getS2WConnectionTime(wells, startTime, endTime, entityType, section, order, returnCount, userId), S2WTimes.class);
    }

    public Collection getS2SConnectionBrakedown(Collection<Long> wells, Date startTime, Date endTime, String entityType, String section, String order, int returnCount, int userId) {
        return mapper.unmarshall(drillingRepo.getS2SConnectionBreakdown(wells, startTime, endTime, entityType, section, order, returnCount, userId), S2SConnectionBreakdown.class);
    }
}
