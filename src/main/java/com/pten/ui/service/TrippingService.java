package com.pten.ui.service;

import com.pten.ui.data.tripping.*;
import com.pten.ui.repo.TrippingRepo;
import com.pten.ui.util.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Date;

@Service
public class TrippingService {
    private static final Logger log = LoggerFactory.getLogger(TrippingService.class);

    @Autowired
    TrippingRepo trippingRepo;

    @Autowired
    Mapper mapper;

    public Collection getReasons(Collection<Long> wells, Date startTime, Date endTime, int userId) {
        return mapper.unmarshall(trippingRepo.getReasons(wells, startTime, endTime, userId), Reason.class);
    }

    public Collection getSpeeds(Collection<Long> wells, Date startTime, Date endTime, String entityType, String direction, String holestatus, int userId) {
        switch (entityType) {
            case "RIG":
                return mapper.unmarshall(trippingRepo.getSpeeds(wells, startTime, endTime, entityType, direction, holestatus, userId), SpeedRig.class);
            case "WELL":
                return mapper.unmarshall(trippingRepo.getSpeeds(wells, startTime, endTime, entityType, direction, holestatus, userId), SpeedWell.class);
            default:
                return mapper.unmarshall(trippingRepo.getSpeeds(wells, startTime, endTime, entityType, direction, holestatus, userId), SpeedCrew.class);
        }
    }

    public Collection getSpeedTrend(Collection<Long> wells, Date startTime, Date endTime, String entityType, String direction, String holestatus, String order, int userId) {
        switch (entityType) {
            case "RIG":
                return mapper.unmarshall(trippingRepo.getRigSpeedTrend(wells, startTime, endTime, direction, holestatus, order, userId), SpeedTrend.class);
            case "RIGTYPE":
                return mapper.unmarshall(trippingRepo.getRigTypeSpeedTrend(wells, startTime, endTime, direction, holestatus, order, userId), SpeedTrend.class);
            default:
                return mapper.unmarshall(trippingRepo.getRegionSpeedTrend(wells, startTime, endTime, direction, holestatus, order, userId), SpeedTrend.class);

        }
    }

    public Collection getConnectionDistribution(Collection<Long> wells, Date startTime, Date endTime, String direction, String holeStatus, int userId) {
        return mapper.unmarshall(trippingRepo.getConnectionDistribution(wells, startTime, endTime, direction, holeStatus, userId), ConnectionDistribution.class);
    }

    public Collection getConnectionTime(Collection<Long> wells, Date startTime, Date endTime, String entityType, int returnCount, int userId) {
        return mapper.unmarshall(trippingRepo.getConnectionTime(wells, startTime, endTime, entityType, returnCount, userId), ConnectionTime.class);
    }

    public Collection getConnectionBrakedown(Collection<Long> wells, Date startTime, Date endTime, String entityType, String direction, int userId) {
        switch (direction) {
            case "IN":
                return mapper.unmarshall(trippingRepo.getConnectionBreakdown(wells, startTime, endTime, entityType, direction, userId), ConnectionBreakdownIn.class);
            default: //"OUT"
                return mapper.unmarshall(trippingRepo.getConnectionBreakdown(wells, startTime, endTime, entityType, direction, userId), ConnectionBreakdownOut.class);

        }

    }
}
