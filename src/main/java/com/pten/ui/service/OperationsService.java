package com.pten.ui.service;

import com.pten.ui.repo.OperationsRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Date;

@Service
public class OperationsService {
    private static final Logger log = LoggerFactory.getLogger(OperationsService.class);

    @Autowired
    OperationsRepo repo;

    public Collection getOperators(Date startTime, Date endTime) {
        return repo.getOperators(startTime, endTime);
    }

    public Collection getAreas(Date startTime, Date endTime) {
        return repo.getAreas(startTime, endTime);
    }

    public Collection getRigs(Date startTime, Date endTime) {
        return repo.getRigs(startTime, endTime);
    }

    public Collection getRigTypes(Date startTime, Date endTime) {
        return repo.getRigTypes(startTime, endTime);
    }

    public Collection getWells(Date startTime, Date endTime) {
        return repo.getWells(startTime, endTime);
    }
}
