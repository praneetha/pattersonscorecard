package com.pten.ui.view;

public class CasingKPI {

    String rigName;
    Double avgInSlips;
    Double avgInFthr;


    public CasingKPI(String rigName, Double avgInSlips, Double avgInFthr) {
        this.rigName = rigName;
        this.avgInSlips = avgInSlips;
        this.avgInFthr = avgInFthr;
    }

    public String geRigName() {
        return rigName;
    }

    public void setRigName(String rigName) {
        this.rigName = rigName;
    }

    public Double getAvgInSlips() {
        return avgInSlips;
    }

    public void setAvgInSlips(Double avgInSlips) {
        this.avgInSlips = avgInSlips;
    }

    public Double getAvgInFthr() {
        return avgInFthr;
    }

    public void setAvgInFthr(Double avgInFthr) {
        this.avgInFthr = avgInFthr;
    }
}
