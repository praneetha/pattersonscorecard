package com.pten.ui.view;

public class Rig {
    private String name;

    public Rig() {}

    public Rig(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
