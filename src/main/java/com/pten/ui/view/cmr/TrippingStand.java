package com.pten.ui.view.cmr;

public class TrippingStand {
    String division;
    String region_name;
    String rig;
    String rig_type;
    String well_name;
    String stand_start_time;
    String driller;
    String stand_type;
    String stand_direction;
    String hole_status;
//    double sec_w2s;
    double sec_s2s;
//    double sec_s2w;
    double sec_tih_moving;
    double sec_tih_stab;
    double sec_tih_makeup;
    double sec_tooh_breakout;
    double sec_tooh_racking;
    double sec_tooh_moving;
    double sec_tooh_slips;
//    double sec_drilling_breakout;
//    double sec_drilling_latch;
//    double sec_drilling_moving;
//    double sec_drilling_stab;
//    double sec_drilling_makeup;
//    double sec_drilling_slips;
//    boolean has_recycles;
    String trip_reason;
    double total_sec;
    double stand_length;
    double feet_hr;


    public TrippingStand() {
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    public String getRig() {
        return rig;
    }

    public void setRig(String rig) {
        this.rig = rig;
    }

    public String getRig_type() {
        return rig_type;
    }

    public void setRig_type(String rig_type) {
        this.rig_type = rig_type;
    }

    public String getWell_name() {
        return well_name;
    }

    public void setWell_name(String well_name) {
        this.well_name = well_name;
    }

    public String getStand_start_time() {
        return stand_start_time;
    }

    public void setStand_start_time(String stand_start_time) {
        this.stand_start_time = stand_start_time;
    }

    public String getDriller() {
        return driller;
    }

    public void setDriller(String driller) {
        this.driller = driller;
    }

    public String getStand_type() {
        return stand_type;
    }

    public void setStand_type(String stand_type) {
        this.stand_type = stand_type;
    }

    public String getStand_direction() {
        return stand_direction;
    }

    public void setStand_direction(String stand_direction) {
        this.stand_direction = stand_direction;
    }

    public String getHole_status() {
        return hole_status;
    }

    public void setHole_status(String hole_status) {
        this.hole_status = hole_status;
    }

//    public double getSec_w2s() {
//        return sec_w2s;
//    }
//
//    public void setSec_w2s(double sec_w2s) {
//        this.sec_w2s = sec_w2s;
//    }

    public double getSec_s2s() {
        return sec_s2s;
    }

    public void setSec_s2s(double sec_s2s) {
        this.sec_s2s = sec_s2s;
    }

//    public double getSec_s2w() {
//        return sec_s2w;
//    }
//
//    public void setSec_s2w(double sec_s2w) {
//        this.sec_s2w = sec_s2w;
//    }

    public double getSec_tih_moving() {
        return sec_tih_moving;
    }

    public void setSec_tih_moving(double sec_tih_moving) {
        this.sec_tih_moving = sec_tih_moving;
    }

    public double getSec_tih_stab() {
        return sec_tih_stab;
    }

    public void setSec_tih_stab(double sec_tih_stab) {
        this.sec_tih_stab = sec_tih_stab;
    }

    public double getSec_tih_makeup() {
        return sec_tih_makeup;
    }

    public void setSec_tih_makeup(double sec_tih_makeup) {
        this.sec_tih_makeup = sec_tih_makeup;
    }

    public double getSec_tooh_breakout() {
        return sec_tooh_breakout;
    }

    public void setSec_tooh_breakout(double sec_tooh_breakout) {
        this.sec_tooh_breakout = sec_tooh_breakout;
    }

    public double getSec_tooh_racking() {
        return sec_tooh_racking;
    }

    public void setSec_tooh_racking(double sec_tooh_racking) {
        this.sec_tooh_racking = sec_tooh_racking;
    }

    public double getSec_tooh_moving() {
        return sec_tooh_moving;
    }

    public void setSec_tooh_moving(double sec_tooh_moving) {
        this.sec_tooh_moving = sec_tooh_moving;
    }

    public double getSec_tooh_slips() {
        return sec_tooh_slips;
    }

    public void setSec_tooh_slips(double sec_tooh_slips) {
        this.sec_tooh_slips = sec_tooh_slips;
    }

//    public double getSec_drilling_breakout() {
//        return sec_drilling_breakout;
//    }
//
//    public void setSec_drilling_breakout(double sec_drilling_breakout) {
//        this.sec_drilling_breakout = sec_drilling_breakout;
//    }
//
//    public double getSec_drilling_latch() {
//        return sec_drilling_latch;
//    }
//
//    public void setSec_drilling_latch(double sec_drilling_latch) {
//        this.sec_drilling_latch = sec_drilling_latch;
//    }
//
//    public double getSec_drilling_moving() {
//        return sec_drilling_moving;
//    }
//
//    public void setSec_drilling_moving(double sec_drilling_moving) {
//        this.sec_drilling_moving = sec_drilling_moving;
//    }
//
//    public double getSec_drilling_stab() {
//        return sec_drilling_stab;
//    }
//
//    public void setSec_drilling_stab(double sec_drilling_stab) {
//        this.sec_drilling_stab = sec_drilling_stab;
//    }
//
//    public double getSec_drilling_makeup() {
//        return sec_drilling_makeup;
//    }
//
//    public void setSec_drilling_makeup(double sec_drilling_makeup) {
//        this.sec_drilling_makeup = sec_drilling_makeup;
//    }
//
//    public double getSec_drilling_slips() {
//        return sec_drilling_slips;
//    }
//
//    public void setSec_drilling_slips(double sec_drilling_slips) {
//        this.sec_drilling_slips = sec_drilling_slips;
//    }

//    public boolean isHas_recycles() {
//        return has_recycles;
//    }
//
//    public void setHas_recycles(String has_recycles) {
//        this.has_recycles = has_recycles.contains("YES");
//    }

    public String getTrip_reason() {
        return trip_reason;
    }

    public void setTrip_reason(String trip_reason) {
        this.trip_reason = trip_reason;
    }

    public double getTotal_sec() {
        return total_sec;
    }

    public void setTotal_sec(double total_sec) {
        this.total_sec = total_sec;
    }

    public double getStand_length() {
        return stand_length;
    }

    public void setStand_length(double stand_length) {
        this.stand_length = stand_length;
    }

    public double getFeet_hr() {
        return feet_hr;
    }

    public void setFeet_hr(double feet_hr) {
        this.feet_hr = feet_hr;
    }
}
