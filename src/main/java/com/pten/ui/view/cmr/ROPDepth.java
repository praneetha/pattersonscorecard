package com.pten.ui.view.cmr;

public class ROPDepth {
    Double start_footage;
    Double weight_on_bit;
    Double rop;
    Double rpm;;
    Double pump_total_strokes_rate;
    Double standpipe_pressure;
    Double differential_pressure;
    Double convertible_torque;
    Double flow_paddle;
    Double flow_in;
    String sld_rot;
    int well_num;
    String well_name;
    String rig_name;



    public ROPDepth() {
    }

    public Double getStart_footage() {
        return start_footage;
    }

    public void setStart_footage(Double start_footage) {
        this.start_footage = start_footage;
    }

    public Double getWeight_on_bit() {
        return weight_on_bit;
    }

    public void setWeight_on_bit(Double weight_on_bit) {
        this.weight_on_bit = weight_on_bit;
    }

    public Double getRop() {
        return rop;
    }

    public void setRop(Double rop) {
        this.rop = rop;
    }

    public Double getRpm() {
        return rpm;
    }

    public void setRpm(Double rpm) {
        this.rpm = rpm;
    }

    public Double getPump_total_strokes_rate() {
        return pump_total_strokes_rate;
    }

    public void setPump_total_strokes_rate(Double pump_total_strokes_rate) {
        this.pump_total_strokes_rate = pump_total_strokes_rate;
    }

    public Double getStandpipe_pressure() {
        return standpipe_pressure;
    }

    public void setStandpipe_pressure(Double standpipe_pressure) {
        this.standpipe_pressure = standpipe_pressure;
    }

    public Double getDifferential_pressure() {
        return differential_pressure;
    }

    public void setDifferential_pressure(Double differential_pressure) {
        this.differential_pressure = differential_pressure;
    }

    public Double getConvertible_torque() {
        return convertible_torque;
    }

    public void setConvertible_torque(Double convertible_torque) {
        this.convertible_torque = convertible_torque;
    }

    public Double getFlow_paddle() {
        return flow_paddle;
    }

    public void setFlow_paddle(Double flow_paddle) {
        this.flow_paddle = flow_paddle;
    }

    public Double getFlow_in() {
        return flow_in;
    }

    public void setFlow_in(Double flow_in) {
        this.flow_in = flow_in;
    }

    public String getSld_rot() {
        return sld_rot;
    }

    public void setSld_rot(String sld_rot) {
        this.sld_rot = sld_rot;
    }

    public int getWell_num() {
        return well_num;
    }

    public void setWell_num(int well_num) {
        this.well_num = well_num;
    }

    public String getWell_name() {
        return well_name;
    }

    public void setWell_name(String well_name) {
        this.well_name = well_name;
    }

    public String getRig_name() {
        return rig_name;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }
}
