package com.pten.ui.view.cmr;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

import java.util.HashMap;
import java.util.Map;

public class WellDailyDepthsFlatMap {

    int day_num;
    Map<String, Object> depthsMap = new HashMap<>();

    public int getDay_num() {
        return day_num;
    }

    public void setDay_num(int day_num) {
        this.day_num = day_num;
    }

    @JsonAnyGetter
    public Map<String, Object> getDepthsMap() {
        return depthsMap;
    }

    @JsonAnySetter
    public void add(String key, Object value) {
        this.depthsMap.put(key, value);
    }
}
