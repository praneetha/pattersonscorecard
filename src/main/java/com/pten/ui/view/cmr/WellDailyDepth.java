package com.pten.ui.view.cmr;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.pten.ui.data.Well;

public class WellDailyDepth {

    @JsonUnwrapped
    Well well;
    int day_num;
    double day_depth;


    public int getDay_num() {
        return day_num;
    }

    public void setDay_num(int day_num) {
        this.day_num = day_num;
    }

    public double getDay_depth() {
        return day_depth;
    }

    public void setDay_depth(double day_depth) {
        this.day_depth = day_depth;
    }

    public Well getWell() {
        return well;
    }

    public void setWell(Well well) {
        this.well = well;
    }
}
