package com.pten.ui.view;

import com.pten.ui.data.RigCMRPercentByMonthObj;

public class RigCMRPercentByMonth extends RigCMRPercentByMonthObj {

    private double npt_percentage_pct;
    private String custom_label;

    public RigCMRPercentByMonth() {
    }

    public double getNpt_percentage_pct() {
        return npt_percentage_pct;
    }

    public void setNpt_percentage_pct(double npt_percentage_pct) {
        this.npt_percentage_pct = npt_percentage_pct;
    }

    public String getCustom_label() {
        return custom_label;
    }

    public void setCustom_label(String custom_label) {
        this.custom_label = custom_label;
    }
}
