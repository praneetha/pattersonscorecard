package com.pten.ui.view;

import java.util.Date;

public class PerformanceMetrics {

    int wellNum;
    int cmrDataId;
    String wellName;
    Date releaseDate;
    String county;
    String state;
    boolean preset;
    boolean batchDrilling;
    Double surfaceDepth;
    Double totalDepth;
    Double rmct;
    Double trucksMoveDistance;
    Double totalWalkTime;
    Double calcSpudtoTD;
    Double spudtoTD;
    Double tdtoRelease;
    Double spudToRelease;
    Double totalCyleTime;
    Double avgFtperDay;

    public PerformanceMetrics(boolean alias, int wellNum, int cmrDataId, String wellName, String wellAlias, Date releaseDate, String county, String state,
                              boolean preset,boolean batchDrilling, Double trucksMoveDistance, Double surfaceDepth, Double totalDepth, Double rmct, Double totalWalkTime,
                              Double calcSpudtoTD, Double spudtoTD, Double tdtoRelease, Double totalCyleTime, Double avgFtperDay) {
        this.wellNum = wellNum;
        this.cmrDataId = cmrDataId;
        this.wellName = (alias) ? wellAlias : wellName;
        this.releaseDate = releaseDate;
        this.county = county;
        this.state = state;
        this.preset = preset;
        this.batchDrilling = batchDrilling;
        this.trucksMoveDistance = trucksMoveDistance;
        this.surfaceDepth = surfaceDepth;
        this.totalDepth = totalDepth;
        this.rmct = rmct;
        this.totalWalkTime = totalWalkTime;
        this.calcSpudtoTD = calcSpudtoTD;
        this.spudtoTD = spudtoTD;
        this.tdtoRelease = tdtoRelease;
        this.spudToRelease = spudtoTD + tdtoRelease;
        this.totalCyleTime = totalCyleTime;
        this.avgFtperDay = avgFtperDay;
    }


    public int getWellNum() {
        return wellNum;
    }

    public void setWellNum(int wellNum) {
        this.wellNum = wellNum;
    }

    public int getCmrDataId() {
        return cmrDataId;
    }

    public void setCmrDataId(int cmrDataId) {
        this.cmrDataId = cmrDataId;
    }

    public String getWellName() {
        return wellName;
    }

    public void setWellName(String wellName) {
        this.wellName = wellName;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public boolean isPreset() {
        return preset;
    }

    public void setPreset(boolean preset) {
        this.preset = preset;
    }

    public boolean isBatchDrilling() {
        return batchDrilling;
    }

    public void setBatchDrilling(boolean batchDrilling) {
        this.batchDrilling = batchDrilling;
    }

    public Double getTrucksMoveDistance() {
        return trucksMoveDistance;
    }

    public void setTrucksMoveDistance(Double trucksMoveDistance) {
        this.trucksMoveDistance = trucksMoveDistance;
    }

    public Double getSurfaceDepth() {
        return surfaceDepth;
    }

    public void setSurfaceDepth(Double surfaceDepth) {
        this.surfaceDepth = surfaceDepth;
    }

    public Double getTotalDepth() {
        return totalDepth;
    }

    public void setTotalDepth(Double totalDepth) {
        this.totalDepth = totalDepth;
    }

    public Double getRmct() {
        return rmct;
    }

    public void setRmct(Double rmct) {
        this.rmct = rmct;
    }

    public Double getTotalWalkTime() {
        return totalWalkTime;
    }

    public void setTotalWalkTime(Double totalWalkTime) {
        this.totalWalkTime = totalWalkTime;
    }

    public Double getCalcSpudtoTD() {
        return calcSpudtoTD;
    }

    public void setCalcSpudtoTD(Double calcSpudtoTD) {
        this.calcSpudtoTD = calcSpudtoTD;
    }

    public Double getSpudtoTD() {
        return spudtoTD;
    }

    public void setSpudtoTD(Double spudtoTD) {
        this.spudtoTD = spudtoTD;
    }

    public Double getTdtoRelease() {
        return tdtoRelease;
    }

    public void setTdtoRelease(Double tdtoRelease) {
        this.tdtoRelease = tdtoRelease;
    }

    public Double getSpudToRelease() {
        return spudToRelease;
    }

    public void setSpudToRelease(Double spudToRelease) {
        this.spudToRelease = spudToRelease;
    }

    public Double getTotalCyleTime() {
        return totalCyleTime;
    }

    public void setTotalCyleTime(Double totalCyleTime) {
        this.totalCyleTime = totalCyleTime;
    }

    public Double getAvgFtperDay() {
        return avgFtperDay;
    }

    public void setAvgFtperDay(Double avgFtperDay) {
        this.avgFtperDay = avgFtperDay;
    }

}
