package com.pten.ui.view;

public class CustomRig {

    String rig_name;
    Double move_time_average;
    Double average_nipple_hours;
    Double average_walk_time;
    Double npt_percentage;
    String operator;
    String selectedMonth;
    Double nptptd;

    public String getRig_name() {
        return rig_name.trim();
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public Double getMove_time_average() {
        return move_time_average;
    }

    public void setMove_time_average(Double move_time_average) {
        this.move_time_average = move_time_average;
    }

    public Double getAverage_nipple_hours() {
        return average_nipple_hours;
    }

    public void setAverage_nipple_hours(Double average_nipple_hours) {
        this.average_nipple_hours = average_nipple_hours;
    }

    public Double getAverage_walk_time() {
        return average_walk_time;
    }

    public void setAverage_walk_time(Double average_walk_time) {
        this.average_walk_time = average_walk_time;
    }

    public Double getNpt_percentage() {
        return npt_percentage;
    }

    public void setNpt_percentage(Double npt_percentage) {
        this.npt_percentage = npt_percentage;
    }

    public String getOperator() {
        if (null != operator){
            return operator.trim();
        }
        return null;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getSelectedMonth() {
        if (null != selectedMonth) {
            return selectedMonth.trim();
        }
        return null;
    }

    public void setSelectedMonth(String selectedMonth) {
        this.selectedMonth = selectedMonth;
    }

    public Double getNptptd() {
        return nptptd;
    }

    public void setNptptd(Double nptptd) {
        this.nptptd = nptptd;
    }
}
