package com.pten.ui.view.npt;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class NPTByTimeGroup extends NPTHrPct {

    LocalDate group_start_date;
    String group_start_date_text;
    double overall_value;
    String region_name;
    String groupType;

    final String Q_PATTERN = "'Q'q uuuu";
    final String M_PATTERN = "MMM uuuu";
    final String W_PATTERN = "MMM d,uuuu";

    public NPTByTimeGroup(String region_name, Date chunk_start, double chunk_pct, double region_pct, String type) {
        setRegion_name(region_name);
        setGroup_start_date(chunk_start.toLocalDate());
        setPct(chunk_pct);
        setOverall_value(region_pct);
        setGroupType(type);

    }

    public NPTByTimeGroup(String region_name, Date chunk_start, String display_color, String display_text, String sub_code, double chunk_code_hours, String type) {
        setRegion_name(region_name);
        setGroup_start_date(chunk_start.toLocalDate());
        setDisplay_color(display_color);
        setDisplay_text(display_text);
        setSub_code(sub_code);
        setValue(chunk_code_hours);
        setGroupType(type);
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    public LocalDate getGroup_start_date() {
        return group_start_date;
    }

    public void setGroup_start_date(LocalDate group_start_date) {
        this.group_start_date = group_start_date;
    }

    public String getGroup_start_date_text() {
        return getGroup_start_date_text(getGroupType());
    }

    public String getGroup_start_date_text(String type) {
        switch (type.toUpperCase()){
            case "M":
                return getGroup_start_date().format(DateTimeFormatter.ofPattern(M_PATTERN));
            case "Q":
                return getGroup_start_date().format(DateTimeFormatter.ofPattern(Q_PATTERN));
            case "W":
                return getGroup_start_date().format(DateTimeFormatter.ofPattern(W_PATTERN));
            default:
                return getGroup_start_date().toString();
        }
    }

    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public void setGroup_start_date_text(String group_start_date_text) {
        this.group_start_date_text = group_start_date_text;
    }

    public double getOverall_value() {
        return overall_value;
    }

    public void setOverall_value(double overall_value) {
        this.overall_value = overall_value;
    }
}
