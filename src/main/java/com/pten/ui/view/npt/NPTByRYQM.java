package com.pten.ui.view.npt;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

//@JsonIgnoreProperties({"", ""})
@JsonInclude(Include.NON_NULL)
public class NPTByRYQM extends NPTHrPct {

    String npt_qtr_text;
    String month_short_text;
    Double annual_npt_percentage;
    Double npt_percentage;
    String rig_name;
    String region_name;
    String month_long_text;
    int npt_month;
    double npt_hours;
    int npt_year;
    int npt_qtr;


    public NPTByRYQM() {
    }


    public NPTByRYQM(String display_text, String display_color, int npt_year, int npt_qtr, int npt_month, String month_long_text, String month_short_text, double npt_hours) {
        setDisplay_text(display_text);
        setDisplay_color(display_color);
        setNpt_year(npt_year);
        setNpt_qtr(npt_qtr);
        setNpt_qtr_text("Q" + npt_qtr);
        setNpt_month(npt_month);
        setMonth_long_text(month_long_text);
        setMonth_short_text(month_short_text);
        setNpt_hours(npt_hours);
    }

    //for hours
    public NPTByRYQM(String region_name, int npt_year, int npt_qtr, int npt_month, String month_long_text, String month_short_text, double npt_hours, String sub_code) {
        setRegion_name(region_name);
        setNpt_year(npt_year);
        setNpt_qtr(npt_qtr);
        setNpt_month(npt_month);
        setMonth_long_text(month_long_text);
        setMonth_short_text(month_short_text);
        //todo: fix this redundancy
        setValue(npt_hours);
        setNpt_hours(npt_hours);
        setSub_code(sub_code);
    }

    //for pct
    public NPTByRYQM(String region_name, int npt_year, int npt_qtr, int npt_month, double value, String month_long_text, String month_short_text) {
        setRegion_name(region_name);
        setNpt_year(npt_year);
        setNpt_qtr(npt_qtr);
        setNpt_qtr_text("Q" + npt_qtr);
        setNpt_month(npt_month);
        setMonth_long_text(month_long_text);
        setMonth_short_text(month_short_text);
        setPct(value);
    }

    public NPTByRYQM(String region_name, String sub_code, String display_color, String display_text, int npt_year, int npt_qtr, int npt_month, double npt_hours, String month_long_text, String month_short_text) {
        setRegion_name(region_name);
        setSub_code(sub_code);
        setDisplay_color(display_color);
        setDisplay_text(display_text);
        setNpt_year(npt_year);
        setNpt_qtr(npt_qtr);
        setNpt_qtr_text("Q" + npt_qtr);
        setNpt_month(npt_month);
        setValue(npt_hours);
        setNpt_hours(npt_hours);
        setMonth_long_text(month_long_text);
        setMonth_short_text(month_short_text);
    }

    public String getNpt_qtr_text() {
        return npt_qtr_text;
    }

    public void setNpt_qtr_text(String npt_qtr_text) {
        this.npt_qtr_text = npt_qtr_text;
    }

    public String getMonth_short_text() {
        return month_short_text;
    }

    public void setMonth_short_text(String month_short_text) {
        this.month_short_text = month_short_text;
    }

    public Double getAnnual_npt_percentage() {
        return annual_npt_percentage;
    }

    public void setAnnual_npt_percentage(Double annual_npt_percentage) {
        this.annual_npt_percentage = annual_npt_percentage;
    }

    public Double getNpt_percentage() {
        return npt_percentage;
    }

    public void setNpt_percentage(Double npt_percentage) {
        this.npt_percentage = npt_percentage;
    }

    public String getRig_name() {
        return rig_name;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public String getMonth_long_text() {
        return month_long_text;
    }

    public void setMonth_long_text(String month_long_text) {
        this.month_long_text = month_long_text;
    }

    public int getNpt_month() {
        return npt_month;
    }

    public void setNpt_month(int npt_month) {
        this.npt_month = npt_month;
    }

    public double getNpt_hours() {
        return npt_hours;
    }

    public void setNpt_hours(double npt_hours) {
        this.npt_hours = npt_hours;
    }

    public int getNpt_year() {
        return npt_year;
    }

    public void setNpt_year(int npt_year) {
        this.npt_year = npt_year;
    }

    public int getNpt_qtr() {
        return npt_qtr;
    }

    public void setNpt_qtr(int npt_qtr) {
        this.npt_qtr = npt_qtr;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }


    @Override
    public String toString() {
        return "NPTByRYQM{" +
                "npt_month=" + getNpt_month() +
                ",npt_hours=" + getNpt_hours() +
                ", npt_year=" + getNpt_year() +
                ",npt_qtr=" + getNpt_qtr() +
                ",region_name='" + getRegion_name() + '\'' +
                ",month_short_text=" + getMonth_short_text() +
                ",month_long_text=" + getMonth_long_text() +
                '}';
    }

}
