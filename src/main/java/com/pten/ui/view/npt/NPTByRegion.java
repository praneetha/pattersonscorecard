package com.pten.ui.view.npt;

import com.pten.ui.data.NPT;

import java.util.Objects;

public class NPTByRegion extends NPT {

    String region_name;

    public NPTByRegion() {

    }

    public NPTByRegion(String displayText, String regionName) {
        super();
        setDisplay_text(displayText);
        setRegion_name(regionName);
    }

    public NPTByRegion(String region_name, String display_text, String display_color, double value) {
        setRegion_name(region_name);
        setDisplay_text(display_text);
        setDisplay_color(display_color);
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        NPTByRegion that = (NPTByRegion) o;
        return Objects.equals(getRegion_name(), that.getRegion_name());
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), getRegion_name());
    }

    @Override
    public String toString() {
        return "NPTByRegion{" +
                "region_name='" + region_name + '\'' +
                '}';
    }
}
