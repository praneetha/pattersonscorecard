package com.pten.ui.view.npt;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.pten.ui.data.NPT;

@JsonIgnoreProperties({"npt_month", "npt_hours", "npt_year"})
public class NPTBySCRL extends NPT {

    String code;
    String sub_code;
    double npt_sub_code_hours;

    public NPTBySCRL(String code, String sub_code, double npt_sub_code_hours, String display_color, String display_text) {
        super();
        setCode(code);
        setSub_code(sub_code);
        setNpt_sub_code_hours(npt_sub_code_hours);
        setDisplay_color(display_color);
        setDisplay_text(display_text);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSub_code() {
        return sub_code;
    }

    public void setSub_code(String sub_code) {
        this.sub_code = sub_code;
    }

    public double getNpt_sub_code_hours() {
        return npt_sub_code_hours;
    }

    public void setNpt_sub_code_hours(double npt_sub_code_hours) {
        this.npt_sub_code_hours = npt_sub_code_hours;
    }

    @Override
    public String toString() {
        return "NPTBySCRL{" +
                "code='" + code + '\'' +
                ", sub_code='" + sub_code + '\'' +
                ", npt_sub_code_hours=" + npt_sub_code_hours +
                '}';
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        if (!super.equals(o)) return false;
//        NPTBySCRL that = (NPTBySCRL) o;
//        return Double.compare(that.getNpt_sub_code_hours(), getNpt_sub_code_hours()) == 0 &&
//                Objects.equals(getCode(), that.getCode()) &&
//                Objects.equals(getSub_code(), that.getSub_code());
//    }
//
//    @Override
//    public int hashCode() {
//
//        return Objects.hash(super.hashCode(), getCode(), getSub_code(), getNpt_sub_code_hours());
//    }
}
