package com.pten.ui.view.npt;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.pten.ui.data.NPT;

import java.util.HashMap;
import java.util.Map;

public class NPTWrapped {

    @JsonUnwrapped
    NPT npt;
    Map<String, Object> properties = new HashMap<>();

    public NPT getNpt() {
        return npt;
    }

    public void setNpt(NPT npt) {
        this.npt = npt;
    }

    @JsonAnyGetter
    public Map<String, Object> getProperties() {
        return properties;
    }

    @JsonAnySetter
    public void add(String key, Object value) {
        this.properties.put(key, value);
    }
}
