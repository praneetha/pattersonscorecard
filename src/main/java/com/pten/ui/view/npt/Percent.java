package com.pten.ui.view.npt;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.pten.ui.data.NPT;


@JsonIgnoreProperties({"npt_month", "npt_hours", "npt_year", "npt_qtr", "npt_month_long_text", "npt_month_short_text"})
public class Percent extends NPT {

    String month_long_text;
    String year;
    Integer month;
    double pct;
    String month_short_text;
    double overall_avg;
    String customLabel;

    public String getMonth_long_text() {
        return getNpt_month_long_text();
    }

    public void setMonth_long_text(String month_long_text) {
        setNpt_month_long_text(month_long_text);
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Integer getMonth() {
        return getNpt_month();
    }

    public void setMonth(Integer month) {
        setNpt_month(month);
    }

    public double getPct() {
        return pct;
    }

    public void setPct(double pct) {
        this.pct = pct;
    }

    public String getMonth_short_text() {
        return getNpt_month_short_text();
    }

    public void setMonth_short_text(String month_short_text) {
        setNpt_month_short_text(month_short_text);
    }

    public double getOverall_avg() {
        return overall_avg;
    }

    public void setOverall_avg(double overall_avg) {
        this.overall_avg = overall_avg;
    }

    public String getCustomLabel() {
        return customLabel;
    }

    public void setCustomLabel(String customLabel) {
        this.customLabel = customLabel;
    }
}
