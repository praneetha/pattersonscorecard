package com.pten.ui.view.npt;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class NPTHrPct {

    //TODO: move default values out of the class. Add a visitor to populate with defaults and static values. ie short month text
    String code = "88";
    String sub_code = "-99";
    double value = Double.NaN; // "npt_sub_code_hours": 3.75,
    String display_color = "#69aaa5";
    String display_text = "default";
    String rig_name;
    private double overall_npt_pct = Double.NaN;
    private String custom_label;
    private double max_value;

    public NPTHrPct(){
    }

    /**
     *
     * @param rig_name
     * @param value percent
     */
    public NPTHrPct(String rig_name, double value){
        setRig_name(rig_name);
        setValue(BigDecimal.valueOf(value * 100)
                .setScale(2, RoundingMode.HALF_UP).doubleValue());
    }

    public NPTHrPct(String rig_type, double npt_percent, String display_color, String display_text) {
        setRig_name(rig_type);
        setPct(npt_percent);
        setDisplay_color(display_color);
        setDisplay_text(display_text);

    }

    //for hours
    public NPTHrPct(String rig_name, String display_text, String code, String sub_code, double value, String display_color) {
        setRig_name(rig_name);
        setDisplay_text(display_text);
        setCode(code);
        setSub_code(sub_code);
        setValue(value);
        setDisplay_color(display_color);
    }

    public NPTHrPct(String display_text, String code, String sub_code, double value, String display_color) {
        setDisplay_text(display_text);
        setCode(code);
        setSub_code(sub_code);
        setValue(value);
        setDisplay_color(display_color);
    }

    public NPTHrPct(String rig_name, String display_color, String sub_code, double value) {
        setRig_name(rig_name);
        setDisplay_color(display_color);
        setSub_code(sub_code);
        setPct(value);
    }

    public void setPct(double value){
        setValue(BigDecimal.valueOf(value * 100)
                .setScale(2, RoundingMode.HALF_UP).doubleValue());
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSub_code() {
        return sub_code;
    }

    public void setSub_code(String sub_code) {
        this.sub_code = sub_code;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getDisplay_color() {
        return display_color;
    }

    public void setDisplay_color(String display_color) {
        this.display_color = display_color;
    }

    public String getDisplay_text() {
        return display_text;
    }

    public void setDisplay_text(String display_text) {
        this.display_text = display_text;
    }

    public String getRig_name() {
        return rig_name;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public double getOverall_npt_pct() {
        return overall_npt_pct;
    }

    public void setOverall_npt_pct(double overall_npt_pct) {
        this.overall_npt_pct = overall_npt_pct;
    }

    public String getCustom_label() {
        return custom_label;
    }

    public void setCustom_label(String custom_label) {
        this.custom_label = custom_label;
    }

    public double getMax_value() {
        return max_value;
    }

    public void setMax_value(double max_value) {
        this.max_value = max_value;
    }
}
