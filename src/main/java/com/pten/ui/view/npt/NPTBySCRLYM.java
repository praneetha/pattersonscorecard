package com.pten.ui.view.npt;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;


public class NPTBySCRLYM extends NPTHrPct {

    int npt_year;
    int npt_month;
    int npt_qtr;
    String month_long_text;
    String month_short_text;
    String month_year_text;
    Double annual_npt_percentage;
    Double overall_npt_percentage = Double.NaN;
    String region_name;
    //Instant startTime;

    public NPTBySCRLYM(){}

    public NPTBySCRLYM(String rig_name, int npt_year, int npt_month, String month_long_text, String month_short_text, double value, double annual_npt_percentage, double overall_npt_percentage) {
        super(rig_name, value);
        setNpt_year(npt_year);
        setNpt_month(npt_month);
        setMonth_long_text(month_long_text);
        setMonth_short_text(month_short_text);
        setAnnual_npt_percentage(annual_npt_percentage);
        setOverall_npt_percentage(overall_npt_percentage);
    }

    //for hours
    public NPTBySCRLYM(String rig_name, int npt_year, int npt_month, String month_long_text, String month_short_text, String code, String sub_code, double hours, String display_color) {
        setRig_name(rig_name);
        setValue(hours);
        setNpt_year(npt_year);
        setNpt_month(npt_month);
        setMonth_long_text(month_long_text);
        setMonth_short_text(month_short_text);
        setCode(code);
        setSub_code(sub_code);
        setDisplay_color(display_color);
    }

    public NPTBySCRLYM(String rig_name, int npt_year, int npt_month, String month_long_text, String month_short_text, String code, String sub_code, double value, String display_color, Timestamp start_time) {
        super(rig_name, value);
        setNpt_year(npt_year);
        setNpt_month(npt_month);
        setMonth_long_text(month_long_text);
        setMonth_short_text(month_short_text);
        setCode(code);
        setSub_code(sub_code);
        setDisplay_color(display_color);
        //setStartTime(start_time.toInstant());
    }

    public NPTBySCRLYM(String region_name, String display_text, String display_color, String sub_code, double value) {
        setRegion_name(region_name);
        setDisplay_text(display_text);
        setDisplay_color(display_color);
        setSub_code(sub_code);
        setValue(value);
    }

    public NPTBySCRLYM(String displayText, String region) {
        setDisplay_text(displayText);
        setRegion_name(region);
    }

    public NPTBySCRLYM(String region_name, double value) {
        setRegion_name(region_name);
        setValue(BigDecimal.valueOf(value * 100)
                .setScale(2, RoundingMode.HALF_UP).doubleValue());
    }

    public NPTBySCRLYM(String display_text, String display_color, int npt_year, int npt_qtr, int npt_month, String month_long_text, String month_short_text, String sub_code, double value) {
        setDisplay_text(display_text);
        setDisplay_color(display_color);
        setNpt_year(npt_year);
        setNpt_qtr(npt_qtr);
        setNpt_month(npt_month);
        setMonth_long_text(month_long_text);
        setMonth_short_text(month_short_text);
        setSub_code(sub_code);
        setValue(value);
    }

    /*
    for hours
     */
    public NPTBySCRLYM(int npt_year, int npt_qtr, int npt_month, String month_long_text, String month_short_text, double value) {
        setNpt_year(npt_year);
        setNpt_qtr(npt_qtr);
        setNpt_month(npt_month);
        setMonth_long_text(month_long_text);
        setMonth_short_text(month_short_text);
        setValue(BigDecimal.valueOf(value * 100)
                .setScale(2, RoundingMode.HALF_UP).doubleValue());
    }

    public NPTBySCRLYM(String rig_name, int npt_year, int npt_month, double npt_percent, String month_year) {
        setRig_name(rig_name);
        setNpt_year(npt_year);
        setNpt_month(npt_month);
        setPct(npt_percent);
        setMonth_year_text(month_year);
    }

    public NPTBySCRLYM(String rig_name, int npt_year, int npt_month, double npt_hours, String month_long_text, String month_short_text, String month_year) {
        setRig_name(rig_name);
        setNpt_year(npt_year);
        setNpt_month(npt_month);
        setValue(npt_hours);
        setMonth_long_text(month_long_text);
        setMonth_short_text(month_short_text);
        setMonth_year_text(month_year);
    }

    public NPTBySCRLYM(String rig_name, int npt_year, int month) {
        setRig_name(rig_name);
        setNpt_year(npt_year);
        setNpt_month(month);
    }

    public int getNpt_year() {
        return npt_year;
    }

    public void setNpt_year(int npt_year) {
        this.npt_year = npt_year;
    }

    public int getNpt_month() {
        return npt_month;
    }

    public void setNpt_month(int npt_month) {
        this.npt_month = npt_month;
    }

    public String getMonth_long_text() {
        return month_long_text;
    }

    public void setMonth_long_text(String month_long_text) {
        this.month_long_text = month_long_text;
    }

    public String getMonth_short_text() {
        return month_short_text;
    }

    public void setMonth_short_text(String month_short_text) {
        this.month_short_text = month_short_text;
    }

    public Double getAnnual_npt_percentage() {
        return annual_npt_percentage;
    }

    public void setAnnual_npt_percentage(Double annual_npt_percentage) {
        this.annual_npt_percentage = annual_npt_percentage;
    }

    public Double getOverall_npt_percentage() {
        return overall_npt_percentage;
    }

    public void setOverall_npt_percentage(Double overall_npt_percentage) {
        this.overall_npt_percentage = overall_npt_percentage;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    public int getNpt_qtr() {
        return npt_qtr;
    }

    public void setNpt_qtr(int npt_qtr) {
        this.npt_qtr = npt_qtr;
    }

    public String getNpt_qtr_text() {
        return "Q" + npt_qtr;
    }

    public String getMonth_year_text() {
        return month_year_text;
    }

    public void setMonth_year_text(String month_year_text) {
        this.month_year_text = month_year_text;
    }


    //    public Instant getStartTime() {
//        return startTime;
//    }
//
//    public void setStartTime(Instant startTime) {
//        this.startTime = startTime;
//    }
}
