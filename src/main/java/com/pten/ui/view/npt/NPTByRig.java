package com.pten.ui.view.npt;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.pten.ui.data.NPT;

@JsonIgnoreProperties({"npt_month", "npt_year"})
public class NPTByRig extends NPT {

    private double npt_sub_code_hours;
    String sub_code;

    public NPTByRig() {

    }

    public NPTByRig(String display_text, String display_color, double npt_hours, double npt_sub_code_hours) {
        setDisplay_text(display_text);
        setDisplay_color(display_color);
        setNpt_hours(npt_hours);
        setNpt_sub_code_hours(npt_sub_code_hours);
    }

    public double getNpt_sub_code_hours() {
        return npt_sub_code_hours;
    }

    public void setNpt_sub_code_hours(double npt_sub_code_hours) {
        this.npt_sub_code_hours = npt_sub_code_hours;
    }

    public String getSub_code() {
        return sub_code;
    }

    public void setSub_code(String sub_code) {
        this.sub_code = sub_code;
    }

    @Override
    public String toString() {
        return "NPTByRig{" +
                "npt_sub_code_hours=" + npt_sub_code_hours +
                ", npt_hours=" + getNpt_hours() +
                '}';
    }
}
