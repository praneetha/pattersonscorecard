package com.pten.ui.view;

public class RigType {

    int id;
    String type_name;

    public RigType(){}

    public RigType(int id, String type){
        this.id = id;
        this.type_name = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

}
