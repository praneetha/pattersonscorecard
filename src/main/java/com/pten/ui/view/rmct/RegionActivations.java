package com.pten.ui.view.rmct;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RegionActivations {
    // Base returns
    private String rig_name;
    private double rig_move_days;
    private String rig_move_date;
    private double rig_move_miles;
    private String rig_move_miles_display_text;;


    public RegionActivations() {}

    public RegionActivations(String rigName, double rigMoveDays, String rigMoveDate, double rigMoveMiles) {
        setRig_name(rigName);
        setRig_move_days(rigMoveDays);
        setRig_move_date(rigMoveDate);
        setRig_move_miles(rigMoveMiles);
    }

    public String getRig_name() {
        return rig_name;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public double getRig_move_days() {
        return rig_move_days;
    }

    public void setRig_move_days(double rig_move_days) {
        this.rig_move_days = rig_move_days;
    }

    public String getRig_move_date() {
        return rig_move_date;
    }

    public void setRig_move_date(String rig_move_date) {

        this.rig_move_date = new SimpleDateFormat("M/yy").format(new Date(Long.parseLong(rig_move_date)*1000));
    }

    public double getRig_move_miles() {
        return rig_move_miles;
    }

    public void setRig_move_miles(double rig_move_miles) {
        this.rig_move_miles = rig_move_miles;
        this.rig_move_miles_display_text = BigDecimal.valueOf(this.rig_move_miles).setScale(0, RoundingMode.HALF_UP) + " mi";
    }

    public String getRig_move_miles_display_text() {
        return rig_move_miles_display_text;
    }

    public void setRig_move_miles_display_text(String rig_move_miles_display_text) {
        this.rig_move_miles_display_text = rig_move_miles_display_text;
    }
}
