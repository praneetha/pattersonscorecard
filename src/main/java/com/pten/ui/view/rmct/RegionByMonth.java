package com.pten.ui.view.rmct;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.Locale;

public class RegionByMonth {
    // Root returns
    private int move_year;
    private int move_month;
    private int move_count;
    private double move_avg_days;
    private int year_move_count;
    private double year_move_avg_days;
    private int overall_move_count;
    private double overall_move_avg_days;
    private String category = "-99";

    // Derived returns
    private String move_month_short_text;
    private String move_month_long_text;
    private String move_count_display_text;
    private String overall_move_avg_days_display_label;

    // Bare constructor
    public RegionByMonth() {
    }

    // Construct from query return
    public RegionByMonth(int moveYear, int moveMonth, int moveCount, double moveAvgDays, int yearMoveCount, double yearMoveAvgDays, int overallMoveCount, double overallMoveAvgDays) {
        setMove_year(moveYear);
        setMove_month(moveMonth);
        setMove_count(moveCount);
        setMove_avg_days(moveAvgDays);
        setYear_move_count(yearMoveCount);
        setYear_move_avg_days(yearMoveAvgDays);
        setOverall_move_count((overallMoveCount));
        setOverall_move_avg_days(overallMoveAvgDays);
    }

    public int getMove_year() {
        return move_year;
    }

    public void setMove_year(int move_year) {
        this.move_year = move_year;
    }

    public int getMove_month() {
        return move_month;
    }

    public void setMove_month(int move_month) {
        // TODO: validate month in range
        this.move_month = move_month;

        // Convert month number to long and short text
        this.setMove_month_short_text(Month.of(move_month).getDisplayName(TextStyle.SHORT, Locale.US));
        this.setMove_month_long_text(Month.of(move_month).getDisplayName(TextStyle.FULL, Locale.US));
    }

    public int getMove_count() {
        return move_count;
    }

    public void setMove_count(int move_count) {
        this.move_count = move_count;
        this.move_count_display_text = BigDecimal.valueOf(this.move_count).setScale(0, RoundingMode.HALF_UP) + " mvs";
    }

    public double getMove_avg_days() {
        return move_avg_days;
    }

    public void setMove_avg_days(double move_avg_days) {
        this.move_avg_days = move_avg_days;
    }

    public int getYear_move_count() {
        return year_move_count;
    }

    public void setYear_move_count(int year_move_count) {
        this.year_move_count = year_move_count;
    }

    public double getYear_move_avg_days() {
        return year_move_avg_days;
    }

    public void setYear_move_avg_days(double year_move_avg_days) {
        this.year_move_avg_days = year_move_avg_days;
    }

    public int getOverall_move_count() {
        return overall_move_count;
    }

    public void setOverall_move_count(int overall_move_count) {
        this.overall_move_count = overall_move_count;
    }

    public double getOverall_move_avg_days() {
        return overall_move_avg_days;
    }

    public void setOverall_move_avg_days(double overall_move_avg_days) {
        this.overall_move_avg_days = overall_move_avg_days;
        this.overall_move_avg_days_display_label = ("<div style=\"padding:4px;color:white;background:#002060;position:relative;bottom:5px\">" +
                new BigDecimal( this.overall_move_avg_days).setScale(2, RoundingMode.HALF_UP) + "</div>");
    }

    public String getMove_month_short_text() {
        return move_month_short_text;
    }

    public void setMove_month_short_text(String move_month_short_text) {
        this.move_month_short_text = move_month_short_text;
    }

    public String getMove_month_long_text() {
        return move_month_long_text;
    }

    public void setMove_month_long_text(String move_month_long_text) {
        this.move_month_long_text = move_month_long_text;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMove_count_display_text() {
        return move_count_display_text;
    }

    public void setMove_count_display_text(String move_count_display_text) {
        this.move_count_display_text = move_count_display_text;
    }

    public String getOverall_move_avg_days_display_label() {
        return overall_move_avg_days_display_label;
    }

    public void setOverall_move_avg_days_display_label(String overall_move_avg_days_display_label) {
        this.overall_move_avg_days_display_label = overall_move_avg_days_display_label;
    }
}
