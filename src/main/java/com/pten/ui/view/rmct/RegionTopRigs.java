package com.pten.ui.view.rmct;

import java.math.BigDecimal;

public class RegionTopRigs {
    // Base returns
    private String rig_name;
    private int move_count;
    private double move_avg_days;
    private String move_count_display_text;
    private String rig_type;
    private String rig_color;

    public RegionTopRigs() {}

    public RegionTopRigs(String rigName, int moveCount, double moveAvgDays) {
        setRig_name(rigName);
        setMove_count(moveCount);
        setMove_avg_days(moveAvgDays);
    }


    public String getRig_name() {
        return rig_name;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public int getMove_count() {
        return move_count;
    }

    public void setMove_count(int move_count) {
        this.move_count = move_count;
        this.move_count_display_text = BigDecimal.valueOf(this.move_count).setScale(0) + " mvs.";
    }

    public double getMove_avg_days() {
        return move_avg_days;
    }

    public void setMove_avg_days(double move_avg_days) {
        this.move_avg_days = move_avg_days;
    }

    public String getMove_count_display_text() {
        return move_count_display_text;
    }

    public void setMove_count_display_text(String move_count_display_text) {
        this.move_count_display_text = move_count_display_text;
    }

    public String getRig_type() {
        return rig_type;
    }

    public void setRig_type(String rig_type) {
        this.rig_type = rig_type;
    }

    public String getRig_color() {
        return rig_color;
    }

    public void setRig_color(String rig_color) {
        this.rig_color = rig_color;
    }
}
