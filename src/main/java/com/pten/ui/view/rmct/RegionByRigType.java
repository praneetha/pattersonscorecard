package com.pten.ui.view.rmct;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class RegionByRigType {
    // Root returns
    private String rig_type;
    private int move_count;
    private double move_avg_days;
    private int overall_move_count;
    private double overall_move_avg_days;
    private String rig_color;
    private String overall_move_avg_days_custom_label;

    // Bare constructor
    public RegionByRigType() {}

    public RegionByRigType(String rigType, int moveCount, double moveAvgDays, int overallMoveCount, double overallMoveAvgDays) {
        setRig_type(rigType);
        setMove_count(moveCount);
        setMove_avg_days(moveAvgDays);
        setOverall_move_count(overallMoveCount);
        setOverall_move_avg_days(overallMoveAvgDays);
    }

    public String getRig_type() {
        return rig_type;
    }

    public void setRig_type(String rig_type) {
        this.rig_type = rig_type;
    }

    public int getMove_count() {
        return move_count;
    }

    public void setMove_count(int move_count) {
        this.move_count = move_count;
    }

    public double getMove_avg_days() {
        return move_avg_days;
    }

    public void setMove_avg_days(double move_avg_days) {
        this.move_avg_days = move_avg_days;
    }

    public int getOverall_move_count() {
        return overall_move_count;
    }

    public void setOverall_move_count(int overall_move_count) {
        this.overall_move_count = overall_move_count;
    }

    public double getOverall_move_avg_days() {
        return overall_move_avg_days;
    }

    public void setOverall_move_avg_days(double overall_move_avg_days) {
        this.overall_move_avg_days = overall_move_avg_days;
        this.overall_move_avg_days_custom_label = ("<div style=\"padding:4px;color:white;background:#002060;position:relative;bottom:5px\">" +
                new BigDecimal( this.overall_move_avg_days).setScale(2,RoundingMode.HALF_UP) + "</div>");
    }

    public String getRig_color() {
        return rig_color;
    }

    public void setRig_color(String rig_color) {
        this.rig_color = rig_color;
    }

    public String getOverall_move_avg_days_custom_label() {
        return overall_move_avg_days_custom_label;
    }

    public void setOverall_move_avg_days_custom_label(String overall_move_avg_days_custom_label) {
        this.overall_move_avg_days_custom_label = overall_move_avg_days_custom_label;
    }
}
