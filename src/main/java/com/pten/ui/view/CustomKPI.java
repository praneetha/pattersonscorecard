package com.pten.ui.view;

import java.util.Date;

public class CustomKPI {

    String rigName;
    int reportYear;
    int reportMonth;
    Double avgRih;
    Double avgPOOH;
    Double avgS2S;
    Double avgBopNuHr;
    Double averageWalkTime;
    Double move;

    public CustomKPI(String rigName,  int reportYear, int reportMonth, Double avgRih, Double avgPOOH, Double avgS2S,Double avgBopNuHr, Double averageWalkTime,Double move) {
        this.rigName = rigName;
        this.reportYear = reportYear;
        this.reportMonth = reportMonth;
        this.avgRih = avgRih;
        this.avgPOOH  = avgPOOH;
        this.avgS2S = avgS2S;
        this.avgBopNuHr = avgBopNuHr;
        this.averageWalkTime = averageWalkTime;
        this.move = move;
    }

    public String geRigName() {
        return rigName;
    }

    public void setRigName(String rigName) {
        this.rigName = rigName;
    }

    public int getReportYear() {
        return reportYear;
    }

    public void setReportYear(int reportYear) {
        this.reportYear = reportYear;
    }

    public int getReportMonth() {
        return reportMonth;
    }

    public void setReportMonth(int reportMonth) {
        this.reportMonth = reportMonth;
    }

    public Double getAvgRih() {
        return avgRih;
    }

    public void setAvgRih( Double avgRih) {
        this.avgRih = avgRih;
    }

    public Double getAvgPOOH() {
        return avgPOOH;
    }

    public void setAvgPOOH( Double avgPOOH) {
        this.avgPOOH = avgPOOH;
    }

    public Double getAvgS2S() {
        return avgS2S;
    }

    public void setAvgS2S( Double avgS2S) {
        this.avgS2S = avgS2S;
    }

    public Double getAvgBopNuHr() {
        return avgBopNuHr;
    }

    public void setAvgBopNuHr( Double avgBopNuHr) {
        this.avgBopNuHr = avgBopNuHr;
    }

    public Double getAverageWalkTime() {
        return averageWalkTime;
    }

    public void setAverageWalkTime( Double averageWalkTime) {
        this.averageWalkTime = averageWalkTime;
    }

    public Double getMove() {
        return move;
    }

    public void setMove( Double move) {
        this.move = move;
    }
}
