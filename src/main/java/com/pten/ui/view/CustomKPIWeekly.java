package com.pten.ui.view;

public class CustomKPIWeekly {

    String rig_name;
    int report_year;
    int report_month;
    int week_num;
    Double avg_rih;

    public String getRig_name() {
        if (null !=rig_name){
            return rig_name.trim();
        }
        return null;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public int getReport_year() {
        return report_year;
    }

    public void setReport_year(int report_year) {
        this.report_year = report_year;
    }

    public int getReport_month() {
        return report_month;
    }

    public void setReport_month(int report_month) {
        this.report_month = report_month;
    }

    public int getWeek_num() {
        return week_num;
    }

    public void setWeek_num(int week_num) {
        this.week_num = week_num;
    }

    public Double getAvg_rih() {
        return avg_rih;
    }

    public void setAvg_rih(Double avg_rih) {
        this.avg_rih = avg_rih;
    }

    public Double getAvg_POOH() {
        return avg_POOH;
    }

    public void setAvg_POOH(Double avg_POOH) {
        this.avg_POOH = avg_POOH;
    }

    public Double getAvg_S2S() {
        return avg_S2S;
    }

    public void setAvg_S2S(Double avg_S2S) {
        this.avg_S2S = avg_S2S;
    }

    public Double getAvg_n_hr() {
        return avg_n_hr;
    }

    public void setAvg_n_hr(Double avg_n_hr) {
        this.avg_n_hr = avg_n_hr;
    }

    Double avg_POOH;
    Double avg_S2S;
    Double avg_n_hr;


    
}
