package com.pten.ui.view.custom;

import java.math.BigDecimal;

public class TrippingKPI {

    String rig_name;
    BigDecimal avg_in_Slips;
    Double avg_in_fthr;
    Double avg_out_fthr;
    double regional_average_in;
    double regional_average_out;
    String color = "#AFA699";

    public String getRig_name() {
        return rig_name.trim();
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public BigDecimal getAvg_in_Slips() {
        return avg_in_Slips;
    }

    public void setAvg_in_Slips(BigDecimal avg_in_Slips) {
        this.avg_in_Slips = avg_in_Slips;
    }

    public Double getAvg_in_fthr() {
        return avg_in_fthr;
    }

    public void setAvg_in_fthr(Double avg_in_fthr) {
        this.avg_in_fthr = avg_in_fthr;
    }

    public Double getAvg_out_fthr() {
        return avg_out_fthr;
    }

    public void setAvg_out_fthr(Double avg_out_fthr) {
        this.avg_out_fthr = avg_out_fthr;
    }

    public double getRegional_average_in() {
        return regional_average_in;
    }

    public void setRegional_average_in(double regional_average_in) {
        this.regional_average_in = regional_average_in;
    }

    public double getRegional_average_out() {
        return regional_average_out;
    }

    public void setRegional_average_out(double regional_average_out) {
        this.regional_average_out = regional_average_out;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
