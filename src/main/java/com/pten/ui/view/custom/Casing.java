package com.pten.ui.view.custom;


public class Casing {

    String rig_name;
    double avg_in_Slips;
    double avg_in_fthr;
    double regional_average_in_slips;
    double regional_average_in_fthr;
    String color = "#AFA699";

    public String getRig_name() {

        if (null != rig_name) {
            return rig_name.trim();
        }
        return null;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public double getAvg_in_Slips() {
        return avg_in_Slips;
    }

    public void setAvg_in_Slips(double avg_in_Slips) {
        this.avg_in_Slips = avg_in_Slips;
    }

    public double getAvg_in_fthr() {
        return avg_in_fthr;
    }

    public void setAvg_in_fthr(double avg_in_fthr) {
        this.avg_in_fthr = avg_in_fthr;
    }

    public double getRegional_average_in_slips() {
        return regional_average_in_slips;
    }

    public void setRegional_average_in_slips(double regional_average_in_slips) {
        this.regional_average_in_slips = regional_average_in_slips;
    }

    public double getRegional_average_in_fthr() {
        return regional_average_in_fthr;
    }

    public void setRegional_average_in_fthr(double regional_average_in_fthr) {
        this.regional_average_in_fthr = regional_average_in_fthr;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
