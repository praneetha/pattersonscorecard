package com.pten.ui.view.custom;

import java.math.BigDecimal;

public class DrillingKPI {

    String rig_name;
    BigDecimal avg_s2s;
    BigDecimal avg_w2s;
    BigDecimal avg_s2w;
    BigDecimal regional_average_s2s;
    BigDecimal regional_average_w2s;
    BigDecimal regional_average_s2w;
    String color = "#AFA699";

    public String getRig_name() {
        if (null != rig_name) {
            return rig_name.trim();
        }
        return null;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public BigDecimal getAvg_s2s() {
        return avg_s2s;
    }

    public void setAvg_s2s(BigDecimal avg_s2s) {
        this.avg_s2s = avg_s2s;
    }

    public BigDecimal getAvg_w2s() {
        return avg_w2s;
    }

    public void setAvg_w2s(BigDecimal avg_w2s) {
        this.avg_w2s = avg_w2s;
    }

    public BigDecimal getAvg_s2w() {
        return avg_s2w;
    }

    public void setAvg_s2w(BigDecimal avg_s2w) {
        this.avg_s2w = avg_s2w;
    }

    public BigDecimal getRegional_average_s2s() {
        return regional_average_s2s;
    }

    public void setRegional_average_s2s(BigDecimal regional_average_s2s) {
        this.regional_average_s2s = regional_average_s2s;
    }

    public BigDecimal getRegional_average_w2s() {
        return regional_average_w2s;
    }

    public void setRegional_average_w2s(BigDecimal regional_average_w2s) {
        this.regional_average_w2s = regional_average_w2s;
    }

    public BigDecimal getRegional_average_s2w() {
        return regional_average_s2w;
    }

    public void setRegional_average_s2w(BigDecimal regional_average_s2w) {
        this.regional_average_s2w = regional_average_s2w;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
