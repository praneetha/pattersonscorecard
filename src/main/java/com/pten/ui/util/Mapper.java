package com.pten.ui.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;

@Component
public class Mapper {
    private static final Logger log = LoggerFactory.getLogger(Mapper.class);

    @Autowired
    ObjectMapper mapper;

    private Object unmarshall(String s, String template) {
        try {
            return mapper.readValue(s, Class.forName(template));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * gets around type erasure by implicitly converting
     * @param c
     * @param itemType
     * @return
     */
    public Collection unmarshall (Collection<?> c, Object itemType){
        Collection items = new ArrayList();
        for (Object o : c) {
            items.add (unmarshall(o.toString(), itemType.getClass().getName()));
        }
        return items;
    }

    public Collection unmarshall (Collection<?> c, Class clazz){
        Collection items = new ArrayList();
        for (Object o : c) {
            items.add (unmarshall(o.toString(), clazz.getName()));
        }
        return items;
    }

    public Collection convertValues(Collection<?> c, Class clazz){
        Collection items = new ArrayList();
        for (Object o : c) {
            items.add (mapper.convertValue(o, clazz));
        }
        return items;
    }

    public Object unmarshall(String s, Object template) {
        try {
            log.info("clazz:{}", template.getClass().getName());
            return mapper.readValue(s, Class.forName(template.getClass().getName()));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

}
