package com.pten.ui.util;

import com.pten.ui.data.NPT;
import com.pten.ui.data.RigCMRHoursByMonthObj;
import com.pten.ui.view.RigCMRPercentByMonth;
import com.pten.ui.view.npt.NPTBySCRLYM;
import com.pten.ui.view.npt.NPTWrapped;
import com.pten.ui.view.npt.Percent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.temporal.IsoFields;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * currently not builder pattern.
 * this class is used to postprocess DAOs (sql)
 */
@Component
public class ViewBuilder {
    private static final Logger log = LoggerFactory.getLogger(ViewBuilder.class);

    public Collection<NPTBySCRLYM> alignBySCRLYM(Collection<NPTBySCRLYM> hours) {

        //all rig_names
        Set<String> rigNames = hours.stream()
                .map(NPTBySCRLYM::getRig_name).collect(Collectors.toSet());

        //all years
        Set<Integer> years = hours.stream()
                .map(NPTBySCRLYM::getNpt_year).collect(Collectors.toSet());

        Collection<NPTBySCRLYM> aligned = new ArrayList<>();

        //hours.forEach(c -> populateByTimeStamp(c));
        aligned.addAll(hours);

        int minMonth = 1;
        int maxMonth = 12;
        Set<Integer> months;
        Set<Double> last_overall_npt_percentages;
        NPTBySCRLYM added;
        for (int year : years) {

            months = hours.stream()
                    .filter(y -> y.getNpt_year() == year)
                    .map(NPTBySCRLYM::getNpt_month).collect(Collectors.toSet());

            last_overall_npt_percentages = hours.stream()
                    .filter(y -> y.getNpt_year() == year)
                    .map(NPTBySCRLYM::getOverall_npt_percentage).collect(Collectors.toSet());

            minMonth = Collections.min(months);
            maxMonth = Collections.max(months);

            double lastOverallNptPct = (Double) last_overall_npt_percentages.stream().findAny().get();

            for (String rigName : rigNames) {
                months = hours.stream()
                        .filter(y -> y.getNpt_year() == year && y.getRig_name().equalsIgnoreCase(rigName))
                        .map(NPTBySCRLYM::getNpt_month).collect(Collectors.toSet());

                for (int month = minMonth; month <= maxMonth; month++) {
                    if (months.add(month)) {
                        log.debug("{} {} {}", rigName, year, month);
                        added = createBySCRLYM(
                                rigName,
                                year,
                                month);
                        added.setOverall_npt_percentage(lastOverallNptPct);
                        aligned.add(added
                        );
                    }
                }
            }
        }

        List<NPTBySCRLYM> sorted = new ArrayList<>(aligned);

        sorted.sort(
                Comparator.comparing(NPTBySCRLYM::getRig_name)
                        .thenComparing(NPTBySCRLYM::getNpt_year)
                        .thenComparing(NPTBySCRLYM::getNpt_month)
                        .thenComparing(NPTBySCRLYM::getSub_code)
        );

        return sorted;
    }

    public Collection<NPT> alignNPTByYM(Collection<NPT> hours) {

        //categories
        Set<String> categories = hours.stream()
                .map(NPT::getDisplay_text).collect(Collectors.toSet());

        //all years
        Set<Integer> years = hours.stream()
                .map(NPT::getNpt_year).collect(Collectors.toSet());

        Collection<NPT> aligned = new ArrayList<>();

        //hours.forEach(c -> populateByTimeStamp(c));
        aligned.addAll(hours);

        int minMonth;
        int maxMonth;
        Set<Integer> months;
        for (int year : years) {

            months = hours.stream()
                    .filter(y -> y.getNpt_year() == year)
                    .map(NPT::getNpt_month).collect(Collectors.toSet());

            minMonth = Collections.min(months);
            maxMonth = Collections.max(months);

            for (String category : categories) {
                months = hours.stream()
                        .filter(y -> y.getNpt_year() == year && y.getDisplay_text().equalsIgnoreCase(category))
                        .map(NPT::getNpt_month).collect(Collectors.toSet());

                for (int month = minMonth; month <= maxMonth; month++) {
                    if (months.add(month)) {
                        log.trace("{} {} {}", category, year, month);
                        aligned.add(new NPT(year, month, category, category));
                    }
                }
            }
        }

        List<NPT> sorted = new ArrayList<>(aligned);

        sorted.sort(
                Comparator.comparing(NPT::getNpt_year)
                        .thenComparing(NPT::getNpt_month)
                        .thenComparing(NPT::getDisplay_text)
        );

        return sorted;
    }


    public Collection<RigCMRPercentByMonth> alignRigCMRPercentByMonth(List<RigCMRPercentByMonth> percents) {

        //categories
        Set<String> categories = percents.stream()
                .map(RigCMRPercentByMonth::getRig_name).collect(Collectors.toSet());

        //all years
        Set<Integer> years = percents.stream()
                .map(RigCMRPercentByMonth::getNpt_year).collect(Collectors.toSet());

        Collection<RigCMRPercentByMonth> aligned = new ArrayList<>();

        aligned.addAll(percents);

        int minMonth;
        int maxMonth;
        int minYear = Collections.min(years);
        int maxYear = Collections.max(years);

        Set<Integer> months;
        RigCMRPercentByMonth appended = null;
        for (int year : years) {

            months = percents.stream()
                    .filter(y -> y.getNpt_year() == year)
                    .map(RigCMRPercentByMonth::getNpt_month).collect(Collectors.toSet());

            minMonth = Collections.min(months);
            maxMonth = Collections.max(months);

            for (String category : categories) {
                months = percents.stream()
                        .filter(y -> y.getNpt_year() == year && y.getRig_name().equalsIgnoreCase(category))
                        .map(RigCMRPercentByMonth::getNpt_month).collect(Collectors.toSet());

                if (year != minYear) {
                    minMonth = 1;
                }
                if (year != maxYear) {
                    maxMonth = 12;
                }

                for (int month = minMonth; month <= maxMonth; month++) {
                    if (months.add(month)) {
                        log.trace("{} {}", year, month);
                        appended = new RigCMRPercentByMonth();
                        appended.setMonth_short_text(getShortMonth(month));
                        appended.setAnnual_npt_percentage(0.0);
                        appended.setNpt_year(year);
                        appended.setNpt_month(month);
                        appended.setNpt_percentage(0.0);
                        appended.setOverall_npt_percentage(0.0);
                        appended.setRig_name(category);
                        appended.setMonth_long_text(getLongMonth(month));
                        aligned.add(appended);
                    }
                }
            }
        }

        return aligned;
    }

    public Collection<RigCMRHoursByMonthObj> alignRigCMRHoursByMonth(List<RigCMRHoursByMonthObj> hours) {

        //categories
        Set<String> categories = hours.stream()
                .map(RigCMRHoursByMonthObj::getRig_name).collect(Collectors.toSet());

        //all years
        Set<Integer> years = hours.stream()
                .map(RigCMRHoursByMonthObj::getNpt_year).collect(Collectors.toSet());

        Collection<RigCMRHoursByMonthObj> aligned = new ArrayList<>();

        aligned.addAll(hours);

        int minMonth;
        int maxMonth;
        Set<Integer> months;
        RigCMRHoursByMonthObj appended = null;
        int minYear = Collections.min(years);
        int maxYear = Collections.max(years);
        for (int year : years) {

            months = hours.stream()
                    .filter(y -> y.getNpt_year() == year)
                    .map(RigCMRHoursByMonthObj::getNpt_month).collect(Collectors.toSet());

            minMonth = Collections.min(months);
            maxMonth = Collections.max(months);


            for (String category : categories) {
                months = hours.stream()
                        .filter(y -> y.getNpt_year() == year && y.getRig_name().equalsIgnoreCase(category))
                        .map(RigCMRHoursByMonthObj::getNpt_month).collect(Collectors.toSet());

                if (year != minYear) {
                    minMonth = 1;
                }
                if (year != maxYear) {
                    maxMonth = 12;
                }

                for (int month = minMonth; month <= maxMonth; month++) {
                    if (months.add(month)) {
                        log.trace("{} {}", year, month);
                        appended = new RigCMRHoursByMonthObj();
                        appended.setMonth_short_text(getShortMonth(month));

                        //default nulls
                        appended.setCode("");
                        appended.setSub_code("");
                        appended.setDisplay_color("");
                        appended.setNpt_sub_code_hours(0.0);
                        appended.setNpt_year(year);
                        appended.setNpt_month(month);
                        appended.setRig_name(category);
                        appended.setMonth_long_text(getLongMonth(month));
                        aligned.add(appended);
                    }
                }

            }
        }

        return aligned;
    }


    //TODO: generalize this method
    public Collection alignNPTByYM(Collection<Percent> npt, Function<Percent, String> series) {

        //categories TODO: generalize getting set of categories for NPT and NPT subclasses.
        Set<String> categories = npt.stream()
                .map(series).collect(Collectors.toSet());

        //all years : TODO: generalize getting set of years for NPT and NPT subclasses.
        Set<String> years = npt.stream()
                .filter(y -> !y.getYear().isEmpty())
                .map(Percent::getYear).collect(Collectors.toSet());

        Collection aligned = new ArrayList<>();

        //npt.forEach(c -> populateByTimeStamp(c));
        aligned.addAll(npt);

        int minMonth;
        int maxMonth;
        Set<Integer> months;
        for (String year : years) {

            months = npt.stream()
                    .filter(y -> y.getYear() == year)
                    .map(NPT::getNpt_month).collect(Collectors.toSet());

            minMonth = Collections.min(months);
            maxMonth = Collections.max(months);

            Percent tmp;
            for (String category : categories) {
                if (null == category) {
                    months = npt.stream()
                            .filter(y -> y.getYear() == year)
                            .map(Percent::getMonth).collect(Collectors.toSet());
                } else {
                    months = npt.stream()
                            .filter(y -> y.getYear() == year && series.apply(y).equalsIgnoreCase(category))
                            .map(Percent::getMonth).collect(Collectors.toSet());
                }

                for (int month = minMonth; month <= maxMonth; month++) {
                    if (months.add(month)) {
                        log.trace("{} {} {}", category, year, month);
                        tmp = new Percent();
                        tmp.setYear(year);
                        tmp.setMonth(month);
                        tmp.setMonth_long_text(getLongMonth(month));
                        tmp.setMonth_short_text(getShortMonth(month));
                        tmp.setDisplay_text(category);
                        aligned.add(tmp);
                    }
                }
            }
        }

        List<Percent> sorted = new ArrayList<>(aligned);

        sorted.sort(
                Comparator.comparing(Percent::getYear)
                        .thenComparing(Percent::getMonth)
                        .thenComparing(Percent::getDisplay_text)
        );

        return sorted;
    }

    public String getShortMonth(int month) {
        return getShortMonth(getDate(getYearMonth(month)));
    }

    public String getLongMonth(int month) {
        return getLongMonth(getDate(getYearMonth(month)));
    }

    private NPTBySCRLYM createBySCRLYM(String rig_name, int npt_year, int month) {
        NPTBySCRLYM result = new NPTBySCRLYM(rig_name, npt_year, month);
        populateDateTime(result);
        return result;
    }

    //for timestamp
    public void populateDateTime(NPTBySCRLYM npt) {
        populateDateTime(npt, npt.getNpt_year(), npt.getNpt_month());
    }

    public void populateDateTime(NPTBySCRLYM npt, int year, int month) {
        populateDateTime(npt, YearMonth.of(year, month));
    }

    public YearMonth getYearMonth(int month) {
        return YearMonth.of(1970, month); //regardless the year
    }

    public LocalDate getDate(YearMonth date) {
        return date.atDay(1);
    }

    public String getLongMonth(LocalDate ld) {
        return ld.format(DateTimeFormatter.ofPattern("MMMM"));
    }

    public String getShortMonth(LocalDate ld) {
        return ld.format(DateTimeFormatter.ofPattern("MMM"));
    }

    //from year/month
    public void populateDateTime(NPTBySCRLYM npt, YearMonth date) {
        LocalDateTime timeStamp = date.atDay(1).atStartOfDay();
        populateDateTime(npt, timeStamp);
    }

    public void populateDateTime(NPTBySCRLYM npt, LocalDateTime ldt) {
        npt.setNpt_month(ldt.getMonthValue());
        npt.setNpt_year(ldt.getYear());
        npt.setNpt_qtr(ldt.get(IsoFields.QUARTER_OF_YEAR));
        npt.setMonth_long_text(ldt.format(DateTimeFormatter.ofPattern("MMMM")));
        npt.setMonth_short_text(ldt.format(DateTimeFormatter.ofPattern("MMM")));
        npt.setMonth_year_text(ldt.format(DateTimeFormatter.ofPattern("MMM uuuu")));
    }

    public Collection<NPTWrapped> getWrappedNPT(Collection<NPT> aligned) {
        Collection result = new ArrayList();
        NPTWrapped tmp;
        for (NPT npt : aligned) {
            tmp = new NPTWrapped();
            tmp.setNpt(npt);
            result.add(tmp);
        }
        return result;
    }

}