package com.pten.ui.data;

public class RigCMRPercentByMonthObj {
    private String month_short_text;
    private Double annual_npt_percentage;
    private Integer npt_year;
    private Integer npt_month;
    private Double npt_percentage;
    private Double overall_npt_percentage;
    private String rig_name;
    private String month_long_text;

    public Double getOverall_npt_percentage() {
        return overall_npt_percentage;
    }

    public void setOverall_npt_percentage(Double overall_npt_percentage) {
        this.overall_npt_percentage = overall_npt_percentage;
    }

    public Double getNpt_percentage() {
        return npt_percentage;
    }

    public void setNpt_percentage(Double npt_percentage) {
        this.npt_percentage = npt_percentage;
    }

    public Integer getNpt_month() {
        return npt_month;
    }

    public void setNpt_month(Integer npt_month) {
        this.npt_month = npt_month;
    }

    public String getRig_name() {
        return rig_name;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public String getMonth_short_text() {
        return month_short_text;
    }

    public void setMonth_short_text(String month_short_text) {
        this.month_short_text = month_short_text;
    }

    public Integer getNpt_year() {
        return npt_year;
    }

    public void setNpt_year(Integer npt_year) {
        this.npt_year = npt_year;
    }

    public Double getAnnual_npt_percentage() {
        return annual_npt_percentage;
    }

    public void setAnnual_npt_percentage(Double annual_npt_percentage) {
        this.annual_npt_percentage = annual_npt_percentage;
    }

    public String getMonth_long_text() {
        return month_long_text;
    }

    public void setMonth_long_text(String month_long_text) {
        this.month_long_text = month_long_text;
    }

    @Override
    public String toString() {
        return "RigCMRPercentByMonthObj [overall_npt_percentage = " + overall_npt_percentage + ", npt_percentage = " + npt_percentage + ", npt_month = " + npt_month + ", rig_name = " + rig_name + ", month_short_text = " + month_short_text + ", npt_year = " + npt_year + ", annual_npt_percentage = " + annual_npt_percentage + ", month_long_text = " + month_long_text + "]";
    }
}
