package com.pten.ui.data;

public class CMRDetails {
    String report_date;
    String from_date;
    String to_date;
    String sub_code;
    String sub_code_text;
    String sub_sub_code;
    String sub_sub_code_text;
    String details;
    String hours;

    public String getReport_date() {
        return report_date;
    }

    public void setReport_date(String report_date) {
        this.report_date = report_date;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    public String getSub_code() {
        return sub_code;
    }

    public void setSub_code(String sub_code) {
        this.sub_code = sub_code;
    }

    public String getSub_code_text() {
        return sub_code_text;
    }

    public void setSub_code_text(String sub_code_text) {
        this.sub_code_text = sub_code_text;
    }

    public String getSub_sub_code() {
        return sub_sub_code;
    }

    public void setSub_sub_code(String sub_sub_code) {
        this.sub_sub_code = sub_sub_code;
    }

    public String getSub_sub_code_text() {
        return sub_sub_code_text;
    }

    public void setSub_sub_code_text(String sub_sub_code_text) {
        this.sub_sub_code_text = sub_sub_code_text;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }
}
