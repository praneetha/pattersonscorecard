package com.pten.ui.data;

import com.fasterxml.jackson.annotation.JsonInclude;

public class Well {

    String well_num;
    String well_name;
    String well_alias;

    public String getWell_num() {
        return well_num;
    }

    public void setWell_num(String well_num) {
        this.well_num = well_num;
    }

    public String getWell_name() {
        return well_name;
    }

    public void setWell_name(String well_name) {
        this.well_name = well_name;
    }

    public String getWell_alias() {
        return well_alias;
    }

    public void setWell_alias(String well_alias) {
        this.well_alias = well_alias;
    }
}
