package com.pten.ui.data.drilling;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class S2WTimes {

    String entity_name;
    double s2w_recycles;
    double s2w_no_recycles;
    double s2w_recylces_pct;

}
