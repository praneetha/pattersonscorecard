package com.pten.ui.data.drilling;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class S2SConnectionBreakdown {

    String entity_name;
    double sec_drilling_breakout;
    double sec_drilling_latch;
    double sec_drilling_moving;
    double sec_drilling_stab;
    double sec_drilling_makeup;
    double sec_drilling_slips;
}
