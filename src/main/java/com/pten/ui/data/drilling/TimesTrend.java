package com.pten.ui.data.drilling;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class TimesTrend {

    int observation_year;
    int observation_month;
    double sec_w2s;
    double sec_s2s;
    double sec_s2w;

}
