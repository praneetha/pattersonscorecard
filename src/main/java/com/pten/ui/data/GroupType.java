package com.pten.ui.data;

public class GroupType {
    String name;
    String value;

    public GroupType(String name, String value){
        setName(name);
        setValue(value);
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
