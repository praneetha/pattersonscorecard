package com.pten.ui.data;

import java.util.List;

public class HighChartsSeriesObj {
    public List<String> name;
    public List<Double> data;
    public List<String> color;
}
