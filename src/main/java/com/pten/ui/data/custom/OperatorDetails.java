package com.pten.ui.data.custom;

import java.math.BigDecimal;

public class OperatorDetails {

    Long well_num;
	String rig_name;
	String well_name;
	String operator;
	String spud_date;
	String url;
	String first_cmr_date;
	String last_cmr_date;
	String last_cmr_date_edited;
	String last_date_processed;
	String last_date_calculated;
	String last_date_status;
	String last_date_stand;
	Integer processed;
	String operator_name;
	String country;
	String county;
	String gps;
	String daywork_start_time;
	String spud_time;
	String surface_end_time;
	String drill_out_from_under_surface_time;
	String vertical_end_time;
	String curve_start_time;
	String curve_end_time;
	String lateral_start_time;
	String lateral_end_time;
	String job_no;
	String wells_on_pad;
	String previous_well_release_time;
	String release_time;
	String target_formation;
	String well_type;
	String preset;
	String state_province;
	BigDecimal rig_move_days;
	BigDecimal rig_move_distance;
	BigDecimal daywork_to_spud_days;
	BigDecimal spud_to_td_days;
	BigDecimal td_to_release_days;
	BigDecimal spud_to_release_days;
	BigDecimal total_cycle_time;
	BigDecimal average_footage_per_day;
	BigDecimal days_to_drill_surface;
	BigDecimal days_to_drill_vertical;
	BigDecimal days_to_drill_curve;
	BigDecimal days_to_drill_lateral;
	Integer sample_size;
	BigDecimal paused_days;
	String lease;
	Boolean end_of_well_eligible;
	BigDecimal total_depth;
	Boolean end_of_well_warning;
	Boolean force_flag;

    public Long getWell_num() {
        return well_num;
    }

    public void setWell_num(Long well_num) {
        this.well_num = well_num;
    }

    public String getRig_name() {
        return rig_name;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public String getWell_name() {
        return well_name;
    }

    public void setWell_name(String well_name) {
        this.well_name = well_name;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getSpud_date() {
        return spud_date;
    }

    public void setSpud_date(String spud_date) {
        this.spud_date = spud_date;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFirst_cmr_date() {
        return first_cmr_date;
    }

    public void setFirst_cmr_date(String first_cmr_date) {
        this.first_cmr_date = first_cmr_date;
    }

    public String getLast_cmr_date() {
        return last_cmr_date;
    }

    public void setLast_cmr_date(String last_cmr_date) {
        this.last_cmr_date = last_cmr_date;
    }

    public String getLast_cmr_date_edited() {
        return last_cmr_date_edited;
    }

    public void setLast_cmr_date_edited(String last_cmr_date_edited) {
        this.last_cmr_date_edited = last_cmr_date_edited;
    }

    public String getLast_date_processed() {
        return last_date_processed;
    }

    public void setLast_date_processed(String last_date_processed) {
        this.last_date_processed = last_date_processed;
    }

    public String getLast_date_calculated() {
        return last_date_calculated;
    }

    public void setLast_date_calculated(String last_date_calculated) {
        this.last_date_calculated = last_date_calculated;
    }

    public String getLast_date_status() {
        return last_date_status;
    }

    public void setLast_date_status(String last_date_status) {
        this.last_date_status = last_date_status;
    }

    public String getLast_date_stand() {
        return last_date_stand;
    }

    public void setLast_date_stand(String last_date_stand) {
        this.last_date_stand = last_date_stand;
    }

    public Integer getProcessed() {
        return processed;
    }

    public void setProcessed(Integer processed) {
        this.processed = processed;
    }

    public String getOperator_name() {
        return operator_name;
    }

    public void setOperator_name(String operator_name) {
        this.operator_name = operator_name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getGps() {
        return gps;
    }

    public void setGps(String gps) {
        this.gps = gps;
    }

    public String getDaywork_start_time() {
        return daywork_start_time;
    }

    public void setDaywork_start_time(String daywork_start_time) {
        this.daywork_start_time = daywork_start_time;
    }

    public String getSpud_time() {
        return spud_time;
    }

    public void setSpud_time(String spud_time) {
        this.spud_time = spud_time;
    }

    public String getSurface_end_time() {
        return surface_end_time;
    }

    public void setSurface_end_time(String surface_end_time) {
        this.surface_end_time = surface_end_time;
    }

    public String getDrill_out_from_under_surface_time() {
        return drill_out_from_under_surface_time;
    }

    public void setDrill_out_from_under_surface_time(String drill_out_from_under_surface_time) {
        this.drill_out_from_under_surface_time = drill_out_from_under_surface_time;
    }

    public String getVertical_end_time() {
        return vertical_end_time;
    }

    public void setVertical_end_time(String vertical_end_time) {
        this.vertical_end_time = vertical_end_time;
    }

    public String getCurve_start_time() {
        return curve_start_time;
    }

    public void setCurve_start_time(String curve_start_time) {
        this.curve_start_time = curve_start_time;
    }

    public String getCurve_end_time() {
        return curve_end_time;
    }

    public void setCurve_end_time(String curve_end_time) {
        this.curve_end_time = curve_end_time;
    }

    public String getLateral_start_time() {
        return lateral_start_time;
    }

    public void setLateral_start_time(String lateral_start_time) {
        this.lateral_start_time = lateral_start_time;
    }

    public String getLateral_end_time() {
        return lateral_end_time;
    }

    public void setLateral_end_time(String lateral_end_time) {
        this.lateral_end_time = lateral_end_time;
    }

    public String getJob_no() {
        return job_no;
    }

    public void setJob_no(String job_no) {
        this.job_no = job_no;
    }

    public String getWells_on_pad() {
        return wells_on_pad;
    }

    public void setWells_on_pad(String wells_on_pad) {
        this.wells_on_pad = wells_on_pad;
    }

    public String getPrevious_well_release_time() {
        return previous_well_release_time;
    }

    public void setPrevious_well_release_time(String previous_well_release_time) {
        this.previous_well_release_time = previous_well_release_time;
    }

    public String getRelease_time() {
        return release_time;
    }

    public void setRelease_time(String release_time) {
        this.release_time = release_time;
    }

    public String getTarget_formation() {
        return target_formation;
    }

    public void setTarget_formation(String target_formation) {
        this.target_formation = target_formation;
    }

    public String getWell_type() {
        return well_type;
    }

    public void setWell_type(String well_type) {
        this.well_type = well_type;
    }

    public String getPreset() {
        return preset;
    }

    public void setPreset(String preset) {
        this.preset = preset;
    }

    public String getState_province() {
        return state_province;
    }

    public void setState_province(String state_province) {
        this.state_province = state_province;
    }

    public BigDecimal getRig_move_days() {
        return rig_move_days;
    }

    public void setRig_move_days(BigDecimal rig_move_days) {
        this.rig_move_days = rig_move_days;
    }

    public BigDecimal getRig_move_distance() {
        return rig_move_distance;
    }

    public void setRig_move_distance(BigDecimal rig_move_distance) {
        this.rig_move_distance = rig_move_distance;
    }

    public BigDecimal getDaywork_to_spud_days() {
        return daywork_to_spud_days;
    }

    public void setDaywork_to_spud_days(BigDecimal daywork_to_spud_days) {
        this.daywork_to_spud_days = daywork_to_spud_days;
    }

    public BigDecimal getSpud_to_td_days() {
        return spud_to_td_days;
    }

    public void setSpud_to_td_days(BigDecimal spud_to_td_days) {
        this.spud_to_td_days = spud_to_td_days;
    }

    public BigDecimal getTd_to_release_days() {
        return td_to_release_days;
    }

    public void setTd_to_release_days(BigDecimal td_to_release_days) {
        this.td_to_release_days = td_to_release_days;
    }

    public BigDecimal getSpud_to_release_days() {
        return spud_to_release_days;
    }

    public void setSpud_to_release_days(BigDecimal spud_to_release_days) {
        this.spud_to_release_days = spud_to_release_days;
    }

    public BigDecimal getTotal_cycle_time() {
        return total_cycle_time;
    }

    public void setTotal_cycle_time(BigDecimal total_cycle_time) {
        this.total_cycle_time = total_cycle_time;
    }

    public BigDecimal getAverage_footage_per_day() {
        return average_footage_per_day;
    }

    public void setAverage_footage_per_day(BigDecimal average_footage_per_day) {
        this.average_footage_per_day = average_footage_per_day;
    }

    public BigDecimal getDays_to_drill_surface() {
        return days_to_drill_surface;
    }

    public void setDays_to_drill_surface(BigDecimal days_to_drill_surface) {
        this.days_to_drill_surface = days_to_drill_surface;
    }

    public BigDecimal getDays_to_drill_vertical() {
        return days_to_drill_vertical;
    }

    public void setDays_to_drill_vertical(BigDecimal days_to_drill_vertical) {
        this.days_to_drill_vertical = days_to_drill_vertical;
    }

    public BigDecimal getDays_to_drill_curve() {
        return days_to_drill_curve;
    }

    public void setDays_to_drill_curve(BigDecimal days_to_drill_curve) {
        this.days_to_drill_curve = days_to_drill_curve;
    }

    public BigDecimal getDays_to_drill_lateral() {
        return days_to_drill_lateral;
    }

    public void setDays_to_drill_lateral(BigDecimal days_to_drill_lateral) {
        this.days_to_drill_lateral = days_to_drill_lateral;
    }

    public Integer getSample_size() {
        return sample_size;
    }

    public void setSample_size(Integer sample_size) {
        this.sample_size = sample_size;
    }

    public BigDecimal getPaused_days() {
        return paused_days;
    }

    public void setPaused_days(BigDecimal paused_days) {
        this.paused_days = paused_days;
    }

    public String getLease() {
        return lease;
    }

    public void setLease(String lease) {
        this.lease = lease;
    }

    public Boolean getEnd_of_well_eligible() {
        return end_of_well_eligible;
    }

    public void setEnd_of_well_eligible(Boolean end_of_well_eligible) {
        this.end_of_well_eligible = end_of_well_eligible;
    }

    public BigDecimal getTotal_depth() {
        return total_depth;
    }

    public void setTotal_depth(BigDecimal total_depth) {
        this.total_depth = total_depth;
    }

    public Boolean getEnd_of_well_warning() {
        return end_of_well_warning;
    }

    public void setEnd_of_well_warning(Boolean end_of_well_warning) {
        this.end_of_well_warning = end_of_well_warning;
    }

    public Boolean getForce_flag() {
        return force_flag;
    }

    public void setForce_flag(Boolean force_flag) {
        this.force_flag = force_flag;
    }
}
