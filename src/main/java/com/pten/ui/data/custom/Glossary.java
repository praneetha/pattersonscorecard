package com.pten.ui.data.custom;

public class Glossary {
    String headline_text;
    String section_text;
    String detail_text;
    String header_description;

    public String getHeadline_text() {
        return headline_text;
    }

    public void setHeadline_text(String headline_text) {
        this.headline_text = headline_text;
    }

    public String getSection_text() {
        return section_text;
    }

    public void setSection_text(String section_text) {
        this.section_text = section_text;
    }

    public String getDetail_text() {
        return detail_text;
    }

    public void setDetail_text(String detail_text) {
        this.detail_text = detail_text;
    }

    public String getHeader_description() {
        return header_description;
    }

    public void setHeader_description(String header_description) {
        this.header_description = header_description;
    }
}
