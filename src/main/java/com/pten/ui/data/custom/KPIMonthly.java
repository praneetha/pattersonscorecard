package com.pten.ui.data.custom;

import java.math.BigDecimal;

public class KPIMonthly {

    String rig_name;
    int report_year;
    int report_month;
    Double avg_rih;
    Double avg_POOH;
    Double avg_S2S;
    BigDecimal avg_bop_nu_hr;
    BigDecimal average_walk_time;
    BigDecimal move;

    public String getRig_name() {
        if (null != rig_name){
            return rig_name.trim();
        }
        return null;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public int getReport_year() {
        return report_year;
    }

    public void setReport_year(int report_year) {
        this.report_year = report_year;
    }

    public int getReport_month() {
        return report_month;
    }

    public void setReport_month(int report_month) {
        this.report_month = report_month;
    }

    public double getAvg_rih() {
        return avg_rih;
    }

    public void setAvg_rih(double avg_rih) {
        this.avg_rih = avg_rih;
    }

    public double getAvg_POOH() {
        return avg_POOH;
    }

    public void setAvg_POOH(double avg_POOH) {
        this.avg_POOH = avg_POOH;
    }

    public double getAvg_S2S() {
        return avg_S2S;
    }

    public void setAvg_S2S(double avg_S2S) {
        this.avg_S2S = avg_S2S;
    }

    public BigDecimal getAvg_bop_nu_hr() {
        return avg_bop_nu_hr;
    }

    public void setAvg_bop_nu_hr(BigDecimal avg_bop_nu_hr) {
        this.avg_bop_nu_hr = avg_bop_nu_hr;
    }

    public BigDecimal getAverage_walk_time() {
        return average_walk_time;
    }

    public void setAverage_walk_time(BigDecimal average_walk_time) {
        this.average_walk_time = average_walk_time;
    }

    public BigDecimal getMove() {
        return move;
    }

    public void setMove(BigDecimal move) {
        this.move = move;
    }
}
