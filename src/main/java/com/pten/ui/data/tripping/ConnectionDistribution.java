package com.pten.ui.data.tripping;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class ConnectionDistribution {

    String bucket;
    int value;
    double category;
    double mean;
    int target;
}
