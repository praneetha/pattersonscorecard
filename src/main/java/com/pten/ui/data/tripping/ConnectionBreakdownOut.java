package com.pten.ui.data.tripping;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class ConnectionBreakdownOut {

    String entity_name;
    double sec_tooh_breakout;
    double sec_tooh_racking;
    double sec_tooh_moving;
    double sec_tooh_slips;

}
