package com.pten.ui.data.tripping;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class ConnectionBreakdownIn {

    String entity_name;
    double sec_tih_moving;
    double sec_tih_stab;
    double sec_tih_makeup;

}
