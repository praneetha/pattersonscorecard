package com.pten.ui.data.tripping;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class SpeedTrend {

    String entity_name;
    int observation_year;
    int observation_month;
    int observation_day;
    double tripping_speed;

}
