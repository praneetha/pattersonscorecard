package com.pten.ui.data.tripping;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.pten.Utilities.DateUtils.CustomDateDeserializer;

import java.util.Collection;
import java.util.Date;


public class Request {

    Collection<Long> welllist; //:[1513758792,1530397310,1527099983,1516585366,1530543250,1521228951,1531180950,1530596175,1524753780,1513476213,1521115203,1531929300,1522779802,1527322721,1528367142,23646466,1521292143,1518717367,1521659430,1529583460],
    Date starttime; //:'2018/05/24',

    Date endtime; //:'2018/07/20',

    String entitytype; //:'RIG',
    String direction; //:'OUT',
    String holestatus; //:'OPEN',
    String order; //:'bottom' | top

    String section; // lateral |

    int returncount;

    String distribution_type;

    public Collection<Long> getWelllist() {
        return welllist;
    }

    public void setWelllist(Collection<Long> welllist) {
        this.welllist = welllist;
    }

    public Date getStarttime() {
        return starttime;
    }

    @JsonDeserialize(using = CustomDateDeserializer.class)
    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    public Date getEndtime() {
        return endtime;
    }

    @JsonDeserialize(using = CustomDateDeserializer.class)
    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    public String getEntitytype() {
        return entitytype;
    }

    public void setEntitytype(String entitytype) {
        this.entitytype = entitytype;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getHolestatus() {
        return holestatus;
    }

    public void setHolestatus(String holestatus) {
        this.holestatus = holestatus;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public int getReturncount() {
        return returncount;
    }

    public void setReturncount(int returncount) {
        this.returncount = returncount;
    }

    public String getDistribution_type() {
        return distribution_type;
    }

    public void setDistribution_type(String distribution_type) {
        this.distribution_type = distribution_type;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }
}
