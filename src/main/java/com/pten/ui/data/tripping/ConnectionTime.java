package com.pten.ui.data.tripping;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class ConnectionTime {

    String entity_name;
    double in_time;
    double out_time;
    int total_time;
    double in_count;
    double out_count;


}
