package com.pten.ui.data.tripping;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class SpeedCrew {

    String crew;
    double tripping_speed_in;
    double tripping_speed_out;

}
