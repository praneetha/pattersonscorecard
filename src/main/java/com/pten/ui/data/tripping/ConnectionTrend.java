package com.pten.ui.data.tripping;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class ConnectionTrend {

    String in_out;
    int entity_year;
    int entity_month;
    double sec_s2s;

}
