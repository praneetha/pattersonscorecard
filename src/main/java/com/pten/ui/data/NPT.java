package com.pten.ui.data;

import java.util.Objects;

/**
 * //TODO: use NPT data as DAO. core properties should exist there.
 * //TODO: child classes of this should be the view/value classes for serialization.
 */

public class NPT implements Comparable{

    int npt_month;
    double npt_hours;
    int npt_year;
    int npt_qtr;
    String display_color;
    String display_text;
    String npt_month_long_text;
    String npt_month_short_text;


    public NPT() {

    }

//    public NPT(String displayText, String displayColor, int year, int month, double hours) {
//        setDisplay_text(displayText);
//        setDisplay_color(displayColor);
//        setNpt_year(year);
//        setNpt_month(month);
//        setNpt_hours(hours);
//    }
//
//    public NPT(int year, int month, String subCode) {
//        setDisplay_text(subCode);
//        setNpt_year(year);
//        setNpt_month(month);
//    }

    public NPT(int year, int month, String subCode, String color) {
        setDisplay_text(subCode);
        setNpt_year(year);
        setNpt_month(month);
        setDisplay_color(color);
    }
//
//    public NPT(String subcode, String color) {
//        setDisplay_text(subcode);
//        setDisplay_color(color);
//    }

//    @Override
//    public String toString() {
//        return "NPT{" +
//                "rpt_month=" + npt_month +
//                ", rpt_hours=" + npt_hours +
//                ", rpt_year=" + npt_year +
//                ", display_color='" + display_color + '\'' +
//                ", display_text='" + display_text + '\'' +
//                '}';
//    }

    public int getNpt_month() {
        return npt_month;
    }

    public void setNpt_month(int npt_month) {
        this.npt_month = npt_month;
    }

    public double getNpt_hours() {
        return npt_hours;
    }

    public void setNpt_hours(double npt_hours) {
        this.npt_hours = npt_hours;
    }

    public int getNpt_year() {
        return npt_year;
    }

    public void setNpt_year(int npt_year) {
        this.npt_year = npt_year;
    }

    public String getDisplay_color() {
        return display_color;
    }

    public void setDisplay_color(String display_color) {
        this.display_color = display_color;
    }

    public String getDisplay_text() {
        return display_text;
    }

    public void setDisplay_text(String display_text) {
        this.display_text = display_text;
    }

    public int getNpt_qtr() {
        return npt_qtr;
    }

    public void setNpt_qtr(int npt_qtr) {
        this.npt_qtr = npt_qtr;
    }

    public String getNpt_month_long_text() {
        return npt_month_long_text;
    }

    public void setNpt_month_long_text(String npt_month_long_text) {
        this.npt_month_long_text = npt_month_long_text;
    }

    public String getNpt_month_short_text() {
        return npt_month_short_text;
    }

    public void setNpt_month_short_text(String npt_month_short_text) {
        this.npt_month_short_text = npt_month_short_text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NPT nptHours = (NPT) o;
        return getNpt_month() == nptHours.getNpt_month() &&
                getNpt_year() == nptHours.getNpt_year() &&
                Objects.equals(getDisplay_text(), nptHours.getDisplay_text());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNpt_month(), getNpt_year(), getDisplay_text());
    }

    @Override
    public int compareTo(Object o) {
        int result = getNpt_year() - ((NPT)o).getNpt_year();
        if (result == 0){
            return getNpt_month() -((NPT)o).getNpt_month();
        }
        return result;
    }
}
