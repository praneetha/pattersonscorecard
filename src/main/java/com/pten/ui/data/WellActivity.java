package com.pten.ui.data;

public class WellActivity {

    private int well_num;
    private String well_name;
    private String rig_name;
    private String rig_type_id;
    private String rig_type_text;
    private boolean completed_well;
    private String release_time;
    private String region_name;
    private String operator_name;
    private Double drilling_hours;
    private Double tripping_hours;
    private Double casing_hours;
    private Double bop_hours;
    private Double npt_hours;
    private Double move_hours;
    private Double other_hours;
    private Double total_hours;
    private String day;
    private String month;
    private String year;

    public WellActivity() {

    }

    public int getWell_num() {
        return well_num;
    }

    public void setWell_num(int well_num) {
        this.well_num = well_num;
    }

    public String getWell_name() {
        return well_name;
    }

    public void setWell_name(String well_name) {
        this.well_name = well_name;
    }

    public String getRig_name() {
        return rig_name;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public String getRig_type_id() {
        return rig_type_id;
    }

    public void setRig_type_id(String rig_type_id) {
        this.rig_type_id = rig_type_id;
    }

    public String getRig_type_text() {
        return rig_type_text;
    }

    public void setRig_type_text(String rig_type_text) {
        this.rig_type_text = rig_type_text;
    }

    public boolean isCompleted_well() {
        return completed_well;
    }

    public void setCompleted_well(boolean completed_well) {
        this.completed_well = completed_well;
    }

    public String getRelease_time() {
        return release_time;
    }

    public void setRelease_time(String release_time) {
        this.release_time = release_time;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    public String getOperator_name() {
        return operator_name;
    }

    public void setOperator_name(String operator_name) {
        this.operator_name = operator_name;
    }

    public Double getDrilling_hours() {
        return drilling_hours;
    }

    public void setDrilling_hours(Double drilling_hours) {
        this.drilling_hours = drilling_hours;
    }

    public Double getTripping_hours() {
        return tripping_hours;
    }

    public void setTripping_hours(Double tripping_hours) {
        this.tripping_hours = tripping_hours;
    }

    public Double getCasing_hours() {
        return casing_hours;
    }

    public void setCasing_hours(Double casing_hours) {
        this.casing_hours = casing_hours;
    }

    public Double getBop_hours() {
        return bop_hours;
    }

    public void setBop_hours(Double bop_hours) {
        this.bop_hours = bop_hours;
    }

    public Double getNpt_hours() {
        return npt_hours;
    }

    public void setNpt_hours(Double npt_hours) {
        this.npt_hours = npt_hours;
    }

    public Double getMove_hours() {
        return move_hours;
    }

    public void setMove_hours(Double move_hours) {
        this.move_hours = move_hours;
    }

    public Double getOther_hours() {
        return other_hours;
    }

    public void setOther_hours(Double other_hours) {
        this.other_hours = other_hours;
    }

    public Double getTotal_hours() {
        return total_hours;
    }

    public void setTotal_hours(Double total_hours) {
        this.total_hours = total_hours;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
