package com.pten.ui.data;


public class RigCMRHoursByMonthObj {

    private String rig_name;
    private Integer npt_year;
    private Integer npt_month;
    private Double npt_sub_code_hours;
    private String code;
    private String sub_code;
    private String month_short_text;
    private String month_long_text;
    private String display_color;

    public String getRig_name() {
        return rig_name;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public Integer getNpt_year() {
        return npt_year;
    }

    public void setNpt_year(Integer npt_year) {
        this.npt_year = npt_year;
    }

    public Integer getNpt_month() {
        return npt_month;
    }

    public void setNpt_month(Integer npt_month) {
        this.npt_month = npt_month;
    }

    public Double getNpt_sub_code_hours() {
        return npt_sub_code_hours;
    }

    public void setNpt_sub_code_hours(Double npt_sub_code_hours) {
        this.npt_sub_code_hours = npt_sub_code_hours;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSub_code() {
        return sub_code;
    }

    public void setSub_code(String sub_code) {
        this.sub_code = sub_code;
    }

    public String getDisplay_color() {
        return display_color;
    }

    public void setDisplay_color(String display_color) {
        this.display_color = display_color;
    }

    public String getMonth_short_text() {
        return month_short_text;
    }

    public void setMonth_short_text(String month_short_text) {
        this.month_short_text = month_short_text;
    }

    public String getMonth_long_text() {
        return month_long_text;
    }

    public void setMonth_long_text(String month_long_text) {
        this.month_long_text = month_long_text;
    }

    @Override
    public String toString() {
        return "ClassPojo [npt_month = " + npt_month + ", rig_name = " + rig_name + ", npt_sub_code_hours = " + npt_sub_code_hours + ", code = " + code + ", sub_code = " + sub_code + ", npt_year = " + npt_year + "]";
    }

}

