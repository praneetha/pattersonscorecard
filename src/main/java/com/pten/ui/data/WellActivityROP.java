package com.pten.ui.data;

public class WellActivityROP {

    private int well_num;
    private String well_name;
    private String rig_name;
    private String rig_type_id;
    private String rig_type_text;
    private boolean completed_well;
    private String release_time;
    private String region_name;
    private String operator_name;
    private Double drilling_hours;
    private Double tripping_hours;
    private Double casing_hours;
    private Double bop_hours;
    private Double npt_hours;
    private Double move_hours;
    private Double other_hours;
    private Double total_hours;
    private String day;
    private String month;
    private String year;
    private Double days_on_well;
    private Double avg_feet_per_day;
    private Double surface_rotary_footage;
    private Double surface_rotary_seconds;
    private Double surface_slide_footage;
    private Double surface_slide_seconds;
    private Double vertical_rotary_footage;
    private Double vertical_rotary_seconds;
    private Double vertical_slide_footage;
    private Double vertical_slide_seconds;
    private Double curve_rotary_footage;
    private Double curve_rotary_seconds;
    private Double curve_slide_footage;
    private Double curve_slide_seconds;
    private Double lateral_rotary_footage;
    private Double lateral_rotary_seconds;
    private Double lateral_slide_footage;
    private Double lateral_slide_seconds;
    private Integer depth_data;




    public WellActivityROP() {

    }

    public int getWell_num() {
        return well_num;
    }

    public void setWell_num(int well_num) {
        this.well_num = well_num;
    }

    public String getWell_name() {
        return well_name;
    }

    public void setWell_name(String well_name) {
        this.well_name = well_name;
    }

    public String getRig_name() {
        return rig_name;
    }

    public void setRig_name(String rig_name) {
        this.rig_name = rig_name;
    }

    public String getRig_type_id() {
        return rig_type_id;
    }

    public void setRig_type_id(String rig_type_id) {
        this.rig_type_id = rig_type_id;
    }

    public String getRig_type_text() {
        return rig_type_text;
    }

    public void setRig_type_text(String rig_type_text) {
        this.rig_type_text = rig_type_text;
    }

    public boolean isCompleted_well() {
        return completed_well;
    }

    public void setCompleted_well(boolean completed_well) {
        this.completed_well = completed_well;
    }

    public String getRelease_time() {
        return release_time;
    }

    public void setRelease_time(String release_time) {
        this.release_time = release_time;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    public String getOperator_name() {
        return operator_name;
    }

    public void setOperator_name(String operator_name) {
        this.operator_name = operator_name;
    }

    public Double getDrilling_hours() {
        return drilling_hours;
    }

    public void setDrilling_hours(Double drilling_hours) {
        this.drilling_hours = drilling_hours;
    }

    public Double getTripping_hours() {
        return tripping_hours;
    }

    public void setTripping_hours(Double tripping_hours) {
        this.tripping_hours = tripping_hours;
    }

    public Double getCasing_hours() {
        return casing_hours;
    }

    public void setCasing_hours(Double casing_hours) {
        this.casing_hours = casing_hours;
    }

    public Double getBop_hours() {
        return bop_hours;
    }

    public void setBop_hours(Double bop_hours) {
        this.bop_hours = bop_hours;
    }

    public Double getNpt_hours() {
        return npt_hours;
    }

    public void setNpt_hours(Double npt_hours) {
        this.npt_hours = npt_hours;
    }

    public Double getMove_hours() {
        return move_hours;
    }

    public void setMove_hours(Double move_hours) {
        this.move_hours = move_hours;
    }

    public Double getOther_hours() {
        return other_hours;
    }

    public void setOther_hours(Double other_hours) {
        this.other_hours = other_hours;
    }

    public Double getTotal_hours() {
        return total_hours;
    }

    public void setTotal_hours(Double total_hours) {
        this.total_hours = total_hours;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getQuarter(){
        return "Q" + ((Integer.parseInt(this.getMonth()) - 1) + 3) / 3;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Double getDays_on_well() {
        return days_on_well;
    }

    public void setDays_on_well(Double days_on_well) {
        this.days_on_well = days_on_well;
    }

    public Double getAvg_feet_per_day() {
        return avg_feet_per_day;
    }

    public void setAvg_feet_per_day(Double avg_feet_per_day) {
        this.avg_feet_per_day = avg_feet_per_day;
    }

    public Double getSurface_rotary_footage() {
        return surface_rotary_footage;
    }

    public void setSurface_rotary_footage(Double surface_rotary_footage) {
        this.surface_rotary_footage = surface_rotary_footage;
    }

    public Double getSurface_rotary_seconds() {
        return surface_rotary_seconds;
    }

    public void setSurface_rotary_seconds(Double surface_rotary_seconds) {
        this.surface_rotary_seconds = surface_rotary_seconds;
    }

    public Double getSurface_slide_footage() {
        return surface_slide_footage;
    }

    public void setSurface_slide_footage(Double surface_slide_footage) {
        this.surface_slide_footage = surface_slide_footage;
    }

    public Double getSurface_slide_seconds() {
        return surface_slide_seconds;
    }

    public void setSurface_slide_seconds(Double surface_slide_seconds) {
        this.surface_slide_seconds = surface_slide_seconds;
    }

    public Double getVertical_rotary_footage() {
        return vertical_rotary_footage;
    }

    public void setVertical_rotary_footage(Double vertical_rotary_footage) {
        this.vertical_rotary_footage = vertical_rotary_footage;
    }

    public Double getVertical_rotary_seconds() {
        return vertical_rotary_seconds;
    }

    public void setVertical_rotary_seconds(Double vertical_rotary_seconds) {
        this.vertical_rotary_seconds = vertical_rotary_seconds;
    }

    public Double getVertical_slide_footage() {
        return vertical_slide_footage;
    }

    public void setVertical_slide_footage(Double vertical_slide_footage) {
        this.vertical_slide_footage = vertical_slide_footage;
    }

    public Double getVertical_slide_seconds() {
        return vertical_slide_seconds;
    }

    public void setVertical_slide_seconds(Double vertical_slide_seconds) {
        this.vertical_slide_seconds = vertical_slide_seconds;
    }

    public Double getCurve_rotary_footage() {
        return curve_rotary_footage;
    }

    public void setCurve_rotary_footage(Double curve_rotary_footage) {
        this.curve_rotary_footage = curve_rotary_footage;
    }

    public Double getCurve_rotary_seconds() {
        return curve_rotary_seconds;
    }

    public void setCurve_rotary_seconds(Double curve_rotary_seconds) {
        this.curve_rotary_seconds = curve_rotary_seconds;
    }

    public Double getCurve_slide_footage() {
        return curve_slide_footage;
    }

    public void setCurve_slide_footage(Double curve_slide_footage) {
        this.curve_slide_footage = curve_slide_footage;
    }

    public Double getCurve_slide_seconds() {
        return curve_slide_seconds;
    }

    public void setCurve_slide_seconds(Double curve_slide_seconds) {
        this.curve_slide_seconds = curve_slide_seconds;
    }

    public Double getLateral_rotary_footage() {
        return lateral_rotary_footage;
    }

    public void setLateral_rotary_footage(Double lateral_rotary_footage) {
        this.lateral_rotary_footage = lateral_rotary_footage;
    }

    public Double getLateral_rotary_seconds() {
        return lateral_rotary_seconds;
    }

    public void setLateral_rotary_seconds(Double lateral_rotary_seconds) {
        this.lateral_rotary_seconds = lateral_rotary_seconds;
    }

    public Double getLateral_slide_footage() {
        return lateral_slide_footage;
    }

    public void setLateral_slide_footage(Double lateral_slide_footage) {
        this.lateral_slide_footage = lateral_slide_footage;
    }

    public Double getLateral_slide_seconds() {
        return lateral_slide_seconds;
    }

    public void setLateral_slide_seconds(Double lateral_slide_seconds) {
        this.lateral_slide_seconds = lateral_slide_seconds;
    }

    public Integer getDepth_data() {
        return depth_data;
    }

    public void setDepth_data(Integer depth_data) {
        this.depth_data = depth_data;
    }
}
