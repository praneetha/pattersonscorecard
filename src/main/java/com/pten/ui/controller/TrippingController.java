package com.pten.ui.controller;

import com.pten.ui.data.tripping.Request;
import com.pten.ui.service.TrippingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Collection;
import java.util.Date;

@Controller
@RequestMapping(value = "/assetReport/tripping")
public class TrippingController {

    @Autowired
    private TrippingService service;

    private static final Logger log = LoggerFactory.getLogger(TrippingController.class);

    @CrossOrigin
    @GetMapping(value = "/reason", produces = "application/json")
    @ResponseBody
    public Collection getReasons(Principal user,
                                 @RequestParam(name = "welllist") Collection<Long> wells,
                                 @RequestParam("starttime") @DateTimeFormat(pattern = "yyyy/MM/dd") Date startTime,
                                 @RequestParam("endtime") @DateTimeFormat(pattern = "yyyy/MM/dd") Date endTime) {

        int userid = Integer.parseInt(user.getName());

        return service.getReasons(wells, startTime, endTime, userid);
    }

    @CrossOrigin
    @PostMapping(value = "/reason", produces = "application/json")
    @ResponseBody
    public Collection getReasons(Principal user, @RequestBody Request r) {
        int userid = Integer.parseInt(user.getName());

        return service.getReasons(r.getWelllist(), r.getStarttime(), r.getEndtime(), userid);
    }

    @CrossOrigin
    @GetMapping(value = "/speed", produces = "application/json")
    @ResponseBody
    public Collection getSpeeds(Principal user,
                                @RequestParam(name = "welllist") Collection<Long> wells,
                                @RequestParam("starttime") @DateTimeFormat(pattern = "yyyy/MM/dd") Date startTime,
                                @RequestParam("endtime") @DateTimeFormat(pattern = "yyyy/MM/dd") Date endTime,
                                @RequestParam String entitytype,
                                @RequestParam String direction,
                                @RequestParam String holestatus
    ) {
        int userid = Integer.parseInt(user.getName());

        return service.getSpeeds(wells, startTime, endTime, entitytype.toUpperCase(), direction.toUpperCase(), holestatus.toUpperCase(), Integer.parseInt(user.getName()));
    }

    @CrossOrigin
    @PostMapping(value = "/speed", produces = "application/json")
    @ResponseBody
    public Collection getSpeeds(Principal user, @RequestBody Request r) {

        int userid = Integer.parseInt(user.getName());
        return service.getSpeeds(r.getWelllist(), r.getStarttime(), r.getEndtime(), r.getEntitytype().toUpperCase(), r.getDirection().toUpperCase(), r.getHolestatus().toUpperCase(), userid);
    }

    @CrossOrigin
    @GetMapping(value = "/speed/trend", produces = "application/json")
    @ResponseBody
    public Collection getSpeedTrendByTime(Principal user,
                                          @RequestParam(name = "welllist") Collection<Long> wells,
                                          @RequestParam("starttime") @DateTimeFormat(pattern = "yyyy/MM/dd") Date startTime,
                                          @RequestParam("endtime") @DateTimeFormat(pattern = "yyyy/MM/dd") Date endTime,
                                          @RequestParam String entitytype,
                                          @RequestParam String direction,
                                          @RequestParam String holestatus,
                                          @RequestParam String order
    ) {
        return service.getSpeedTrend(wells, startTime, endTime, entitytype.toUpperCase(), direction.toUpperCase(), holestatus.toUpperCase(), order.toUpperCase(), Integer.parseInt(user.getName()));
    }

    @PostMapping(value = "/speed/trend", produces = "application/json")
    @ResponseBody
    public Collection getSpeedTrendByTime(Principal user, @RequestBody Request r) {
        return service.getSpeedTrend(r.getWelllist(), r.getStarttime(), r.getEndtime(), r.getEntitytype().toUpperCase(), r.getDirection().toUpperCase(), r.getHolestatus().toUpperCase(), r.getOrder().toUpperCase(), Integer.parseInt(user.getName()));
    }

    @CrossOrigin
    @GetMapping(value = "/connection/distribution", produces = "application/json")
    @ResponseBody
    public Collection getConnectionDistribution(Principal user,
                                                @RequestParam(name = "welllist") Collection<Long> wells,
                                                @RequestParam("starttime") @DateTimeFormat(pattern = "yyyy/MM/dd") Date startTime,
                                                @RequestParam("endtime") @DateTimeFormat(pattern = "yyyy/MM/dd") Date endTime,
                                                @RequestParam String direction,
                                                @RequestParam String holestatus
    ) {
        return service.getConnectionDistribution(wells, startTime, endTime, direction.toUpperCase(), holestatus.toUpperCase(), Integer.parseInt(user.getName()));
    }


    @PostMapping(value = "connection/distribution", produces = "application/json")
    @ResponseBody
    public Collection getConnectionDistribution(Principal user, @RequestBody Request r) {
        return service.getConnectionDistribution(r.getWelllist(), r.getStarttime(), r.getEndtime(), r.getDirection().toUpperCase(), r.getHolestatus().toUpperCase(), Integer.parseInt(user.getName()));
    }

    @CrossOrigin
    @GetMapping(value = "/connection/time", produces = "application/json")
    @ResponseBody
    public Collection getConnectionTime(Principal user,
                                        @RequestParam(name = "welllist") Collection<Long> wells,
                                        @RequestParam("starttime") @DateTimeFormat(pattern = "yyyy/MM/dd") Date startTime,
                                        @RequestParam("endtime") @DateTimeFormat(pattern = "yyyy/MM/dd") Date endTime,
                                        @RequestParam String entitytype,
                                        @RequestParam(name = "returncount") int returnCount
    ) {
        return service.getConnectionTime(wells, startTime, endTime, entitytype.toUpperCase(), returnCount, Integer.parseInt(user.getName()));
    }


    @PostMapping(value = "connection/time", produces = "application/json")
    @ResponseBody
    public Collection getConnectionTime(Principal user, @RequestBody Request r) {
        return service.getConnectionTime(r.getWelllist(), r.getStarttime(), r.getEndtime(), r.getEntitytype().toUpperCase(), r.getReturncount(), Integer.parseInt(user.getName()));
    }

    @CrossOrigin
    @GetMapping(value = "/connection/breakdown/{inOut}", produces = "application/json")
    @ResponseBody
    public Collection getConnectionBreakdown(Principal user,
                                             @RequestParam(name = "welllist") Collection<Long> wells,
                                             @RequestParam("starttime") @DateTimeFormat(pattern = "yyyy/MM/dd") Date startTime,
                                             @RequestParam("endtime") @DateTimeFormat(pattern = "yyyy/MM/dd") Date endTime,
                                             @RequestParam String entitytype,
                                             @PathVariable(value = "inOut") String direction
    ) {
        return service.getConnectionBrakedown(wells, startTime, endTime, entitytype.toUpperCase(), direction.toUpperCase(), Integer.parseInt(user.getName()));
    }

    @PostMapping(value = "connection/breakdown/{inOut}", produces = "application/json")
    @ResponseBody
    public Collection getConnectionBreakdown(Principal user, @RequestBody Request r, @PathVariable(value = "inOut") String direction) {
        return service.getConnectionBrakedown(r.getWelllist(), r.getStarttime(), r.getEndtime(), r.getEntitytype().toUpperCase(), direction.toUpperCase(), Integer.parseInt(user.getName()));
    }


}
