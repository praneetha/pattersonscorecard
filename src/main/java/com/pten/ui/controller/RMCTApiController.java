package com.pten.ui.controller;

import com.pten.ui.repo.RMCTRepo;
import com.pten.ui.service.RMCTService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Date;

@RestController
@RequestMapping(value = {"/assetReport/rmct"})
public class RMCTApiController {

    private static final Logger log = LoggerFactory.getLogger(RMCTApiController.class);


    @Autowired
    private RMCTService service;

    @Autowired
    RMCTRepo rmctRepo;

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/regionRigTypeTimeRangeAggMonth", produces = "application/json")
    @ResponseBody
    public Collection getRMCTByRegionRigTypeTimeRangeAggMonth(@RequestParam("regions") String regions,
                                                              @RequestParam("rigtypes") String rigTypes,
                                                              @RequestParam("starttime") String startTime,
                                                              @RequestParam("endtime") String endTime) {
        return service.getRMCTByRegionRigTypeTimeRangeAggMonth(regions, rigTypes, startTime, endTime);

    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/regionRigTypeTimeRangeAggRigType", produces = "application/json")
    @ResponseBody
    public Collection getRMCTByRegionRigTypeTimeRangeAggRigType(@RequestParam("regions") String regions,
                                                                @RequestParam("rigtypes") String rigTypes,
                                                                @RequestParam("starttime") String startTime,
                                                                @RequestParam("endtime") String endTime) {
        return service.getRMCTByRegionRigTypeTimeRangeAggRigType(regions, rigTypes, startTime, endTime);

    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/reactivatedRegionRigTypeTimeRange", produces = "application/json")
    @ResponseBody
    public Collection getRMCTReactivatedRegionRigTypeTimeRange(@RequestParam("regions") String regions,
                                                               @RequestParam("rigtypes") String rigTypes,
                                                               @RequestParam("starttime") String startTime,
                                                               @RequestParam("endtime") String endTime) {
        return service.getRMCTReactivatedRegionRigTypeTimeRange(regions, rigTypes, startTime, endTime);

    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/topNRigsRegionRigTypeTimeRange", produces = "application/json")
    @ResponseBody
    public Collection getRMCTTopNRigsRegionRigTypeTimeRange(@RequestParam("regions") String regions,
                                                            @RequestParam("rigtypes") String rigTypes,
                                                            @RequestParam("starttime") String startTime,
                                                            @RequestParam("endtime") String endTime) {
        return service.getRMCTTopNRigsRegionRigTypeTimeRange(regions, rigTypes, startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rig/moves/days", produces = "application/json")
    @ResponseBody
    public Object getCompaniesAverageMoveDays(@RequestParam(value = "regions", required = false) Collection<String> regions,
                                              @RequestParam(value = "rigtypes", required = false) Collection<String> rigTypes,
                                              @RequestParam("starttime") Date startTime,
                                              @RequestParam("endtime") Date endTime,
                                              @RequestParam(value = "rmcttype", required = false) String rmctType) {
        return rmctRepo.getRigAverageMoveDays(regions, rigTypes, startTime, endTime, rmctType).toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rig/moves/total", produces = "application/json")
    @ResponseBody
    public Object getCompanyTotalMoves(@RequestParam(value = "regions", required = false) Collection<String> regions,
                                       @RequestParam(value = "rigtypes", required = false) Collection<String> rigTypes,
                                       @RequestParam("starttime") Date startTime,
                                       @RequestParam("endtime") Date endTime) {
        return rmctRepo.getRigTotalMoves(regions, rigTypes, startTime, endTime).toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/rig/moves", produces = "application/json")
    @ResponseBody
    public Object getRigMoves(@RequestParam(value = "regions", required = false) Collection<String> regions,
                              @RequestParam(value = "rigtypes", required = false) Collection<String> rigTypes,
                              @RequestParam("starttime") Date startTime,
                              @RequestParam("endtime") Date endTime,
                              @RequestParam(value = "rmcttype", required = false) String rmctType) {
        return rmctRepo.getRigMoves(regions, rigTypes, startTime, endTime, rmctType).toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/top/wells", produces = "application/json")
    @ResponseBody
    public Object getTopRMCTWellsInRegion(@RequestParam("region") String region) {
        return rmctRepo.getTopRMCTWells(region).toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/top/wells/legend", produces = "application/json")
    @ResponseBody

    public Object getTopRMCTWellsInRegionLegend(@RequestParam("region") String region) {
        return rmctRepo.getTopRMCTWellsLegend(region).toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/totalTime/top/active/wells", produces = "application/json")
    @ResponseBody
    public Object getTopWellsByCycleTimeInRegion(@RequestParam("region") String region) {
        return rmctRepo.getTopTotalCTActiveWells(region).toString();
    }

}


