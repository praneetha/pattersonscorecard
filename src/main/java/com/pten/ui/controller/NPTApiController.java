package com.pten.ui.controller;

import com.flutura.ImpalaRestApiController.CerebraAPIDataLayer;
import com.flutura.ImpalaRestApiController.RequestHandler;
import com.pten.ui.service.NPTService;
import com.pten.ui.view.npt.NPTHrPct;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping(value = "/assetReport")
public class NPTApiController {

    private static final Logger log = LoggerFactory.getLogger(NPTApiController.class);
    @Autowired
    CerebraAPIDataLayer cerebraAPIDataLayer;

    @Autowired
    NPTService nptService;

    @Autowired
    private RequestHandler requestHandler;

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/NPTRegionQuarterlyWithAverage", produces = "application/json")
    @ResponseBody
    public String NPTRegionQuarterlyWithAverage_GET(@RequestParam("region") String region,
                                                    @RequestParam("starttime") String startTime,
                                                    @RequestParam("endtime") String endTime) {
        return requestHandler.NPTRegionQuarterlyWithAverage(region, startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/NPTRegionQuarterlyWithAverage", produces = "application/json")
    @ResponseBody
    public String NPTRegionQuarterlyWithAverage_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String region = jsonOb.getString("region");
            String starttime = jsonOb.getString("starttime");
            String endtime = jsonOb.getString("endtime");
            response = requestHandler.NPTRegionQuarterlyWithAverage(region, starttime, endtime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }

        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/NPTHoursArbitraryRegionsTotal", produces = "application/json")
    @ResponseBody
    public String NPTHoursArbitraryRegionsTotal_GET(HttpServletRequest request) {
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + ", param : " + request.getQueryString());
        String regionlist = request.getParameter("regionlist");
        String starttime = request.getParameter("starttime");
        String endtime = request.getParameter("endtime");
        return requestHandler.NPTHoursArbitraryRegionsTotal(regionlist, starttime, endtime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/NPTHoursArbitraryRegionsTotal", produces = "application/json")
    @ResponseBody
    public String NPTHoursArbitraryRegionsTotal_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String regionlist = jsonOb.getString("regionlist");
            String starttime = jsonOb.getString("starttime");
            String endtime = jsonOb.getString("endtime");
            response = requestHandler.NPTHoursArbitraryRegionsTotal(regionlist, starttime, endtime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }

        return response;
    }

    @CrossOrigin
    @GetMapping(value = "/NPTByTimeGroups", produces = "application/json")
    @ResponseBody
    public Collection NPTByTimegroups(@RequestParam("regionlist") String regionlist,
                                      @RequestParam("starttime") String startTime,
                                      @RequestParam("endtime") String endTime,
                                      @RequestParam("grouptype") String grouptype,
                                      @RequestParam(defaultValue = "hours") Optional<String> type) {
        return nptService.NPTByTimegroups(type, regionlist, startTime, endTime, grouptype);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/NPTPctArbitraryRigsTimegroups", produces = "application/json")
    @ResponseBody
    public String NPTPctArbitraryRigsTimegroups_GET(@RequestParam("regionlist") String regionlist,
                                                    @RequestParam("starttime") String startTime,
                                                    @RequestParam("endtime") String endTime,
                                                    @RequestParam("grouptype") String grouptype) {
        return requestHandler.NPTPctArbitraryRigsTimegroups(regionlist, startTime, endTime, grouptype);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/NPTPctArbitraryRigsTimegroups", produces = "application/json")
    @ResponseBody
    public String NPTPctArbitraryRigsTimegroups_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String regionlist = jsonOb.getString("regionlist");
            String starttime = jsonOb.getString("starttime");
            String endtime = jsonOb.getString("endtime");
            String grouptype = jsonOb.getString("grouptype");
            response = requestHandler.NPTPctArbitraryRigsTimegroups(regionlist, starttime, endtime, grouptype);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }

        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/NPTHoursArbitraryRigsTimegroups", produces = "application/json")
    @ResponseBody
    public String NPTHoursArbitraryRigsTimegroups_GET(@RequestParam("regionlist") String regionlist,
                                                      @RequestParam("starttime") String startTime,
                                                      @RequestParam("endtime") String endTime,
                                                      @RequestParam("grouptype") String grouptype) {
        return requestHandler.NPTHoursArbitraryRigsTimegroups(regionlist, startTime, endTime, grouptype);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/NPTHoursArbitraryRigsTimegroups", produces = "application/json")
    @ResponseBody
    public String NPTHoursArbitraryRigsTimegroups_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String regionlist = jsonOb.getString("regionlist");
            String starttime = jsonOb.getString("starttime");
            String endtime = jsonOb.getString("endtime");
            String grouptype = jsonOb.getString("grouptype");
            response = requestHandler.NPTHoursArbitraryRigsTimegroups(regionlist, starttime, endtime, grouptype);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }

        return response;
    }

    @CrossOrigin
    @GetMapping(value = "/NPTByRegionSubCode", produces = "application/json")
    @ResponseBody
    public Collection<NPTHrPct> getNPTByRegionSubCode(@RequestParam("starttime") String startTime,
                                                      @RequestParam("endtime") String endTime,
                                                      @RequestParam(defaultValue = "hours") Optional<String> type,
                                                      @RequestParam(value = "subcode", required = false) Optional<String> subCode) {
        return nptService.getNPTByRegionSubCode(type, startTime, endTime, subCode);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/NPTHoursByRegionSubCode", produces = "application/json")
    @ResponseBody
    public String NPTHoursByRegionSubCode_GET(@RequestParam("starttime") String startTime,
                                              @RequestParam("endtime") String endTime) {
        //TODO: check nothing broke, from this change rHandler to service
        //return nptService.getNPTHoursByRegionSubCode(startTime, endTime);
        return requestHandler.NPTHoursByRegionSubCode(startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/NPTHoursByRegionSubCode", produces = "application/json")
    @ResponseBody
    public String NPTHoursByRegionSubCode_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String starttime = jsonOb.getString("starttime");
            String endtime = jsonOb.getString("endtime");
            response = requestHandler.NPTHoursByRegionSubCode(starttime, endtime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }

        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/NPTByRegionSubCodeYQM", produces = "application/json")
    @ResponseBody
    public Collection NPTByRegionSubCodeYQM(@RequestParam("starttime") String startTime,
                                            @RequestParam("endtime") String endTime,
                                            @RequestParam(defaultValue = "hours") Optional<String> type,
                                            @RequestParam(value = "subcode", required = false) Optional<String> subCode) throws ParseException {
        return nptService.getNPTByRegionSubCodeYQM(type, startTime, endTime, subCode);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/NPTHoursByRegionSubCodeYQM", produces = "application/json")
    @ResponseBody
    public String NPTHoursByRegionSubCodeYQM_GET(HttpServletRequest request, @RequestParam("starttime") String startTime, @RequestParam("endtime") String endTime) throws ParseException {
        return requestHandler.NPTHoursByRegionSubCodeYQM(startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/NPTHoursByRegionSubCodeYQM", produces = "application/json")
    @ResponseBody
    public String NPTHoursByRegionSubCodeYQM_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String starttime = jsonOb.getString("starttime");
            String endtime = jsonOb.getString("endtime");
            response = requestHandler.NPTHoursByRegionSubCodeYQM(starttime, endtime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }

        return response;
    }

    @CrossOrigin
    @GetMapping(value = "/NPTByRYQM", produces = "application/json")
    @ResponseBody
    public Collection NPTByRYQM(@RequestParam String starttime,
                                @RequestParam String endtime,
                                @RequestParam(defaultValue = "hours") Optional<String> type,
                                @RequestParam(value = "subcode", required = false) Optional<String> subCode) {
        return nptService.NPTByRYQM(type, starttime, endtime, subCode);
    }


    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/NPTHoursByRYQM", produces = "application/json")
    @ResponseBody
    public Collection NPTHoursByRYQM_GET(@RequestParam String starttime,
                                         @RequestParam String endtime) {
        return nptService.NPTHoursByRYQM(starttime, endtime);
    }


    /**
     * created from /NPTHoursByRYQM to support region as url parameter
     * //TODO: merge this once cerebra supports optional parameters
     *
     * @param starttime
     * @param endtime
     * @param region
     * @return
     */
    @CrossOrigin
    @GetMapping(value = "/NPTHoursByRYQMReg", produces = "application/json")
    @ResponseBody
    public Collection NPTHoursByRYQMperRegion(@RequestParam String starttime,
                                              @RequestParam String endtime,
                                              @RequestParam(required = false) String region) {
        if (null != region) {
            log.info("filter by region: {}", region);
        }
        return nptService.NPTHoursByRYQM(region, starttime, endtime);

    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/NPTHoursByRYQM", produces = "application/json")
    @ResponseBody
    public String NPTHoursByRYQM_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String starttime = jsonOb.getString("starttime");
            String endtime = jsonOb.getString("endtime");
            response = requestHandler.NPTHoursByRYQM(starttime, endtime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }

        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/NPTHoursBySCRLYM", produces = "application/json")
    @ResponseBody
    public String NPTHoursBySCRLYM_GET(@RequestParam("riglist") String riglist,
                                       @RequestParam("starttime") String startTime,
                                       @RequestParam("endtime") String endTime) {
        return requestHandler.NPTHoursBySCRLYM(riglist, startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/NPTHoursBySCRLYM", produces = "application/json")
    @ResponseBody
    public String NPTHoursBySCRLYM_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String riglist = jsonOb.getString("riglist");
            String starttime = jsonOb.getString("starttime");
            String endtime = jsonOb.getString("endtime");
            response = requestHandler.NPTHoursBySCRLYM(riglist, starttime, endtime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }

        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/NPTHoursBySCRL", produces = "application/json")
    @ResponseBody
    public Object NPTHoursBySCRL_GET(HttpServletRequest request,
                                     @RequestParam("riglist") String riglist,
                                     @RequestParam("starttime") String starttime,
                                     @RequestParam("endtime") String endtime) {
        return requestHandler.NPTHoursBySCRL(riglist, starttime, endtime);
    }

    @CrossOrigin
    @GetMapping(value = "/NPTBySCRL", produces = "application/json")
    @ResponseBody
    public Object NPTBySCRL(HttpServletRequest request,
                            @RequestParam("riglist") String riglist,
                            @RequestParam("starttime") String starttime,
                            @RequestParam("endtime") String endtime,
                            @RequestParam(defaultValue = "hours") Optional<String> type) {
        return nptService.getNPT(type, riglist, starttime, endtime);

    }

    @CrossOrigin
    @GetMapping(value = "/NPTBySCRLYM", produces = "application/json")
    @ResponseBody
    public Object NPTBySCRLYM(HttpServletRequest request,
                              @RequestParam("riglist") String riglist,
                              @RequestParam("starttime") String starttime,
                              @RequestParam("endtime") String endtime,
                              @RequestParam(defaultValue = "hours") Optional<String> type) {
        return nptService.getNPTBySCRLYM(type, riglist, starttime, endtime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/NPTHoursBySCRLOverall", produces = "application/json")
    @ResponseBody
    public Object NPTHoursBySCRL(HttpServletRequest request,
                                 @RequestParam("riglist") String riglist,
                                 @RequestParam("starttime") String starttime,
                                 @RequestParam("endtime") String endtime
    ) {


        return nptService.getNPTHoursBySCRL(riglist, starttime, endtime);
        //return requestHandler.NPTBySCRL(riglist, starttime, endtime);
    }


    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/NPTHoursBySCRL", produces = "application/json")
    @ResponseBody
    public String NPTHoursBySCRL_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String riglist = jsonOb.getString("riglist");
            String starttime = jsonOb.getString("starttime");
            String endtime = jsonOb.getString("endtime");
            response = requestHandler.NPTHoursBySCRL(riglist, starttime, endtime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }

        return response;
    }

    @CrossOrigin
    @GetMapping(value = "/NPTByRig", produces = "application/json")
    @ResponseBody
    public Object NPTByRig(@RequestParam("region") String region,
                           @RequestParam("starttime") String starttime,
                           @RequestParam("endtime") String endtime,
                           @RequestParam(defaultValue = "hours") Optional<String> type,
                           @RequestParam(defaultValue = "All Rig Types") Optional<String> rigtype,
                           @RequestParam(value = "subcode", required = false) Optional<String> subcode) {

        return nptService.NPTByRig(type, region, starttime, endtime, rigtype, subcode);
    }

    @CrossOrigin
    @GetMapping(value = "/NPTHoursByRig", produces = "application/json")
    @ResponseBody
    public Object NPTHoursByRig(@RequestParam("region") String region,
                                @RequestParam("starttime") String starttime,
                                @RequestParam("endtime") String endtime) {

        return requestHandler.NPTRegionHoursByRig(region, starttime, endtime);
    }

    @CrossOrigin
    @GetMapping(value = "/NPTByRigType", produces = "application/json")
    @ResponseBody
    public Object NPTByRigType(@RequestParam(defaultValue = " ") Optional <String> region,
                               @RequestParam("starttime") String starttime,
                               @RequestParam("endtime") String endtime,
                               @RequestParam(defaultValue = "hours") Optional<String> type,
                               @RequestParam(value = "subcode", required = false) Optional<String> subcode) {

        return nptService.getNPTByRigType(type, region, starttime, endtime, subcode);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/NPTHoursByRigType", produces = "application/json")
    @ResponseBody
    public Object NPTHoursByRigType(HttpServletRequest request,
                                    @RequestParam("region") String region,
                                    @RequestParam("starttime") String starttime,
                                    @RequestParam("endtime") String endtime) {

        return nptService.getNPTHoursByRigType(region, starttime, endtime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/NPTSCHoursFromRL", produces = "application/json")
    @ResponseBody
    public String NPTSCHoursFromRL_GET(@RequestParam("riglist") String riglist,
                                       @RequestParam("starttime") String startTime,
                                       @RequestParam("endtime") String endTime) {
        return requestHandler.NPTSCHoursFromRL(riglist, startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/NPTSCHoursFromRL", produces = "application/json")
    @ResponseBody
    public String NPTSCHoursFromRL_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String riglist = jsonOb.getString("riglist");
            String starttime = jsonOb.getString("starttime");
            String endtime = jsonOb.getString("endtime");
            response = requestHandler.NPTSCHoursFromRL(riglist, starttime, endtime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }

        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/NPTPctFromRL", produces = "application/json")
    @ResponseBody
    public String NPTPctFromRL_GET(@RequestParam("riglist") String riglist,
                                   @RequestParam("starttime") String startTime,
                                   @RequestParam("endtime") String endTime) {
        return requestHandler.NPTPctFromRL(riglist, startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/NPTPctFromRL", produces = "application/json")
    @ResponseBody
    public String NPTPctFromRL_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String riglist = jsonOb.getString("riglist");
            String starttime = jsonOb.getString("starttime");
            String endtime = jsonOb.getString("endtime");
            response = requestHandler.NPTPctFromRL(riglist, starttime, endtime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }

        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/NPTPctByYMRL", produces = "application/json")
    @ResponseBody
    public String NPTPctByYMRL_GET(@RequestParam("riglist") String riglist,
                                   @RequestParam("starttime") String startTime,
                                   @RequestParam("endtime")
                                           String endTime) {
        return requestHandler.NPTPctByYMRL(riglist, startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/NPTPctByYMRL", produces = "application/json")
    @ResponseBody
    public String NPTPctYMRL_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String riglist = jsonOb.getString("riglist");
            String starttime = jsonOb.getString("starttime");
            String endtime = jsonOb.getString("endtime");
            response = requestHandler.NPTPctByYMRL(riglist, starttime, endtime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }

        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/NPTRegionRigMonthAverage", produces = "application/json")
    @ResponseBody
    public String NPTRegionRigMonthAverage_GET(@RequestParam("region") String region,
                                               @RequestParam("starttime") String startTime,
                                               @RequestParam("endtime") String endTime) {
        //TODO: return hours not percent.
        return requestHandler.NPTRegionRigMonthAverageSort(region, startTime, endTime);
        //return nptService.getNPTRegionRigMonthAverageSort(region, startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/NPTRegionRigMonthAverage", produces = "application/json")
    @ResponseBody
    public String NPTRegionRigMonthAverage_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String region = jsonOb.getString("region");
            String starttime = jsonOb.getString("starttime");
            String endtime = jsonOb.getString("endtime");
            response = requestHandler.NPTRegionRigMonthAverageSort(region, starttime, endtime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }

        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/NPTRegionHoursBySubCode", produces = "application/json")
    @ResponseBody
    public String NPTRegionHoursBySubCode_GET(@RequestParam("region") String region,
                                              @RequestParam("starttime") String startTime,
                                              @RequestParam("endtime") String endTime,
                                              @RequestParam(defaultValue = "All Rig Types") Optional<String> rigtype) {
        return nptService.NPTRegionHoursBySubCode(region, startTime, endTime, rigtype);
    }

    @CrossOrigin
    @GetMapping(value = "/NPTByYQM", produces = "application/json")
    @ResponseBody
    public Collection NPTByYQM(@RequestParam("region") String region,
                               @RequestParam("starttime") String startTime,
                               @RequestParam("endtime") String endTime,
                               @RequestParam(defaultValue = "hours") Optional<String> type,
                               @RequestParam(defaultValue = "All Rig Types") Optional<String> rigtype,
                               @RequestParam(value = "subcode", required = false) Optional<String> subCode) {
        return nptService.NPTByYQM(type, region, startTime, endTime, rigtype, subCode);
    }

    @CrossOrigin
    @GetMapping(value = "/NPTRegionHoursByYQM", produces = "application/json")
    @ResponseBody
    public String NPTRegionHoursByYQM(@RequestParam("region") String region,
                                      @RequestParam("starttime") String startTime,
                                      @RequestParam("endtime") String endTime) {
        return requestHandler.NPTRegionHoursByYQM(region, startTime, endTime);
    }

    @CrossOrigin
    @GetMapping(value = "/NPTRegionHoursByRYM", produces = "application/json")
    @ResponseBody
    public String NPTRegionHoursByRYM(@RequestParam("region") String region,
                                      @RequestParam("starttime") String startTime,
                                      @RequestParam("endtime") String endTime) {
        return requestHandler.NPTRegionHoursByRYM(region, startTime, endTime);
    }

    @CrossOrigin
    @GetMapping(value = "/NPTByRYM", produces = "application/json")
    @ResponseBody
    public Object NPTByRYM(@RequestParam("region") String region,
                           @RequestParam("starttime") String startTime,
                           @RequestParam("endtime") String endTime,
                           @RequestParam(defaultValue = "hours") Optional<String> type,
                           @RequestParam(defaultValue = "All Rig Types") Optional<String> rigtype,
                           @RequestParam(value = "subcode", required = false) Optional<String> subCode) {
        return nptService.NPTByRYM(type, region, startTime, endTime, rigtype, subCode);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/NPTRegionRigPercent", produces = "application/json")
    @ResponseBody
    public String NPTRegionRigPercent_GET(@RequestParam("region") String region,
                                          @RequestParam("starttime") String startTime,
                                          @RequestParam("endtime") String endTime) {
        return requestHandler.NPTRegionRigPercent(region, startTime, endTime);
    }

    @CrossOrigin
    @GetMapping(value = "/NPTRegionPercentByRYM", produces = "application/json")
    @ResponseBody
    public String NPTRegionPercentByRYM(@RequestParam("region") String region,
                                        @RequestParam("starttime") String startTime,
                                        @RequestParam("endtime") String endTime) {
        return requestHandler.NPTRegionPercentByRYM(region, startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/NPTRegionRigPercent", produces = "application/json")
    @ResponseBody
    public String NPTRegionRigPercent_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String region = jsonOb.getString("region");
            String starttime = jsonOb.getString("starttime");
            String endtime = jsonOb.getString("endtime");
            response = requestHandler.NPTRegionRigPercent(region, starttime, endtime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }

        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/NPTRegionRigTypeNPTPercent", produces = "application/json")
    @ResponseBody
    public String NPTRegionRigTypeNPTPercent_GET(@RequestParam("region") String region,
                                                 @RequestParam("starttime") String startTime,
                                                 @RequestParam("endtime") String endTime) {
        return requestHandler.NPTRegionRigTypeNPTPercent(region, startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/NPTRegionRigTypeNPTPercent", produces = "application/json")
    @ResponseBody
    public String NPTRegionRigTypeNPTPercent_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + " with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String region = jsonOb.getString("region");
            String starttime = jsonOb.getString("starttime");
            String endtime = jsonOb.getString("endtime");
            response = requestHandler.NPTRegionRigTypeNPTPercent(region, starttime, endtime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }

        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/NPTPercentageByRegion", produces = "application/json")
    @ResponseBody
    public String NPTPercentageByRegion_GET(@RequestParam("starttime") String startTime,
                                            @RequestParam("endtime") String endTime) {
        return requestHandler.NPTPercentageByRegion(startTime, endTime);

    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/NPTPercentageByRegion", produces = "application/json")
    @ResponseBody
    public String NPTPercentageByRegion_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            response = requestHandler.NPTPercentageByRegion(startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/NPTHoursBySubCode", produces = "application/json")
    @ResponseBody
    public String NPTHoursBySubCode_GET(@RequestParam("starttime") /* @DateTimeFormat(pattern = "yyyy/MM/dd hh:mm:ss") */ String startTime,
                                        @RequestParam("endtime") /*@DateTimeFormat(pattern = "yyyy/MM/dd hh:mm:ss") */ String endTime) {
        return requestHandler.NPTHoursBySubCode(startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/NPTHoursBySubCode", produces = "application/json")
    @ResponseBody
    public String NPTHoursBySubCode_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            response = requestHandler.NPTHoursBySubCode(startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/NPTQuarterlyWithAverage", produces = "application/json")
    @ResponseBody
    public String NPTQuarterlyWithAverage_GET(@RequestParam("starttime") String startTime,
                                              @RequestParam("endtime") String endTime) {
        return requestHandler.NPTQuarterlyWithAverage(startTime, endTime);

    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/NPTQuarterlyWithAverage", produces = "application/json")
    @ResponseBody
    public String NPTQuarterlyWithAverage_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            response = requestHandler.NPTQuarterlyWithAverage(startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/NPTPercentageByRegionMonth", produces = "application/json")
    @ResponseBody
    public String NPTPercentageByRegionMonth_GET(@RequestParam("starttime") String startTime,
                                                 @RequestParam("endtime") String endTime) {
        return requestHandler.NPTPercentageByRegionMonth(startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, value = "/NPTPercentageByRegionMonth", produces = "application/json")
    @ResponseBody
    public String NPTPercentageByRegionMonth_POST(HttpServletRequest request, @RequestBody String body) throws SQLException, ParseException, JSONException {
        String response = "";
        log.info("Request URL is : " + request.getRemoteAddr() + ":" + request.getRemotePort() + "with body : " + body);
        try {
            JSONObject jsonOb = new JSONObject(body);
            String startTime = jsonOb.getString("starttime");
            String endTime = jsonOb.getString("endtime");
            response = requestHandler.NPTPercentageByRegionMonth(startTime, endTime);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response = e.getMessage();
        }
        return response;
    }

    @CrossOrigin
    @GetMapping(value = "/npt/percent/top/rigs", produces = "application/json")
    @ResponseBody
    public Object getTopNRigsInRegion(String region) {
        return nptService.getRigs(region).toString();
    }

}
