package com.pten.ui.controller;

import com.pten.ui.service.CustomKPIService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;


@RestController
@RequestMapping(value = "/kpi")
public class CustomKPIController {

    @Autowired
    private CustomKPIService customKPIService;

    private static final Logger log = LoggerFactory.getLogger(CustomKPIController.class);

    @CrossOrigin
    @GetMapping(value = "/metrics/monthly", produces = "application/json")
    @ResponseBody
    public Collection getMetricsMonthly(@RequestParam String operator,
                                        @RequestParam("endtime") @DateTimeFormat(pattern = "yyyy/MM/dd") Date endTime) {
        return customKPIService.getMetricsMonthly(operator, endTime);
    }

    @CrossOrigin
    @GetMapping(value = "/metrics/weekly", produces = "application/json")
    @ResponseBody
    public Collection getMetricsWeekly(@RequestParam String operator,
                                       @RequestParam("endtime") @DateTimeFormat(pattern = "yyyy/MM/dd") Date selectedDate){
        return customKPIService.getMetricsWeekly(operator, selectedDate);
    }

    @CrossOrigin
    @GetMapping(value = "/operator", produces = "application/json")
    @ResponseBody
    public Collection getOperators(@RequestParam Optional<String> operator,
                                   @RequestParam("starttime") @DateTimeFormat(pattern = "yyyy/MM/dd") Optional<Date> startTime) {
        if (!operator.isPresent() && !startTime.isPresent()){
            return customKPIService.getOperator();
        }
        return customKPIService.getOperators(operator.get(), startTime.get());
    }

    //TODO: rename. unable to categorize this data
    @CrossOrigin
    @GetMapping(value = "/header", produces = "application/json")
    @ResponseBody
    public Collection getHeader(@RequestParam String operator,
                                @RequestParam("starttime") @DateTimeFormat(pattern = "yyyy/MM/dd") Date startTime) {
        return customKPIService.getHeader(operator, startTime);
    }

    @CrossOrigin
    @GetMapping(value = "/casing", produces = "application/json")
    @ResponseBody
    public Collection getCasing(@RequestParam String operator,
                                @RequestParam("selecteddate") @DateTimeFormat(pattern = "yyyy/MM/dd") Date selectedDate) {
        return customKPIService.getCasing(operator, selectedDate);
    }

    @CrossOrigin
    @GetMapping(value = "/tripping", produces = "application/json")
    @ResponseBody
    public Collection getTripping(@RequestParam String operator,
                                  @RequestParam("selecteddate") @DateTimeFormat(pattern = "yyyy/MM/dd") Date selectedDate){
        return customKPIService.getTripping(operator, selectedDate);
    }

    @CrossOrigin
    @GetMapping(value = "/drilling", produces = "application/json")
    @ResponseBody
    public Collection getDrilling(@RequestParam String operator,
                                  @RequestParam("selecteddate") @DateTimeFormat(pattern = "yyyy/MM/dd") Date selectedDate) {
        return customKPIService.getDrilling(operator, selectedDate);
    }

    @CrossOrigin
    @GetMapping(value = "/glossary", produces = "application/json")
    @ResponseBody
    public Collection getGlossary() {
        return customKPIService.getGlossary();
    }

}
