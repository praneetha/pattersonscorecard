package com.pten.ui.controller;

import com.pten.ui.repo.OptionsRepo;
import com.pten.ui.service.OptionsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Optional;

@RestController
@RequestMapping(value = {"/assetReport/options"})
public class OptionsController {
    private static final Logger log = LoggerFactory.getLogger(OptionsController.class);

    @Autowired
    private OptionsService cmrService;

    @Autowired
    OptionsRepo optionsDataLayer;

    @CrossOrigin
    @GetMapping(value = "/cmr", produces = "application/json")
    @ResponseBody
    public Object cmrOperatorList(@RequestParam @DateTimeFormat(pattern = "y-M-d") Optional<Date> from,
                                  @RequestParam @DateTimeFormat(pattern = "y-M-d") Optional<Date> to) {
        return optionsDataLayer.findAllByReportingFrom(from, to).toString();
    }
//
//    @CrossOrigin
//    @RequestMapping(method = RequestMethod.GET, value = "/cmr/operators", produces = "application/json")
//    @ResponseBody
//    public Collection cmrOperatorsByReportDate() {
//        return cmrService.RigCMROperators();
//    }
//
//    @CrossOrigin
//    @RequestMapping(method = RequestMethod.GET, value = "/cmr/regions", produces = "application/json")
//    @ResponseBody
//    public Collection cmrRegionList() {
//        return cmrService.getAllRegions();
//    }


}
