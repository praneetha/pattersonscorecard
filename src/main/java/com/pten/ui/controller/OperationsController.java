package com.pten.ui.controller;

import com.pten.ui.repo.OperationsRepo;
import com.pten.ui.service.OperationsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping(value = {"/assetReport/operations"})
public class OperationsController {
    private static final Logger log = LoggerFactory.getLogger(OperationsController.class);

    @Autowired
    OperationsRepo repo;

    @Autowired
    OperationsService operationsService;

    @CrossOrigin
    @GetMapping(value = "/operators", produces = "application/json")
    @ResponseBody
    public Object getOperators(@RequestParam("starttime")Date startTime, @RequestParam("endtime") Date endTime) {
        return operationsService.getOperators(startTime, endTime).toString();
    }

    @CrossOrigin
    @GetMapping(value = "/areas", produces = "application/json")
    @ResponseBody
    public Object getAreas(@RequestParam("starttime") Date startTime, @RequestParam("endtime") Date endTime) {
        return operationsService.getAreas(startTime, endTime).toString();
    }

    @CrossOrigin
    @GetMapping(value = "/rigs", produces = "application/json")
    @ResponseBody
    public Object getRigs(@RequestParam("starttime") Date startTime, @RequestParam("endtime") Date endTime) {
        return operationsService.getRigs(startTime, endTime).toString();
    }

    @CrossOrigin
    @GetMapping(value = "/rigtypes", produces = "application/json")
    @ResponseBody
    public Object getRigTypes(@RequestParam("starttime") Date startTime, @RequestParam("endtime") Date endTime) {
        return operationsService.getRigTypes(startTime, endTime).toString();
    }

    @CrossOrigin
    @GetMapping(value = "/wells", produces = "application/json")
    @ResponseBody
    public Object getWells(@RequestParam("starttime") Date startTime, @RequestParam("endtime") Date endTime) {
        return operationsService.getWells(startTime, endTime).toString();
    }
}
