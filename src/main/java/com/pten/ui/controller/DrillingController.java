package com.pten.ui.controller;

import com.pten.ui.data.tripping.Request;
import com.pten.ui.service.DrillingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Collection;

@Controller
@RequestMapping(value = "/assetReport/drilling")
public class DrillingController {

    @Autowired
    private DrillingService service;

    private static final Logger log = LoggerFactory.getLogger(DrillingController.class);

    @CrossOrigin
    @PostMapping(value = "/connection/time", produces = "application/json")
    @ResponseBody
    public Collection getConnectionTimes(Principal user, @RequestBody Request r) {

        return service.getConnectionTime(r.getWelllist(), r.getStarttime(), r.getEndtime(), r.getEntitytype().toUpperCase(), r.getSection().toUpperCase(), r.getOrder().toUpperCase(), r.getReturncount(), Integer.parseInt(user.getName()));
    }

    @PostMapping(value = "connection/distribution", produces = "application/json")
    @ResponseBody
    public Collection getConnectionDistribution(Principal user, @RequestBody Request r) {
        return service.getConnectionDistribution(r.getWelllist(), r.getStarttime(), r.getEndtime(), r.getDistribution_type().toUpperCase(), r.getSection(), Integer.parseInt(user.getName()));
    }

    @PostMapping(value = "connection/time/s2w", produces = "application/json")
    @ResponseBody
    public Collection getConnectionTime(Principal user, @RequestBody Request r) {
        return service.getS2WConnectionTime(r.getWelllist(), r.getStarttime(), r.getEndtime(), r.getEntitytype().toUpperCase(), r.getSection().toUpperCase(), r.getOrder().toUpperCase(), r.getReturncount(), Integer.parseInt(user.getName()));
    }

    @PostMapping(value = "connection/breakdown/s2s", produces = "application/json")
    @ResponseBody
    public Collection getConnectionBreakdown(Principal user, @RequestBody Request r) {
        return service.getS2SConnectionBrakedown(r.getWelllist(), r.getStarttime(), r.getEndtime(), r.getEntitytype().toUpperCase(), r.getSection().toUpperCase(), r.getOrder().toUpperCase(), r.getReturncount(), Integer.parseInt(user.getName()));
    }


}
