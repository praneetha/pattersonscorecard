package com.pten.ui.controller;

import com.flutura.ImpalaRestApiController.CerebraAPIDataLayer;
import com.pten.ui.service.CMRService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;


@RestController
@RequestMapping(value = "/assetReport")
public class CMRApiController {

    @Autowired
    private CMRService cmrService;

    @Autowired
    private CerebraAPIDataLayer cerebraAPIDataLayer;

    private static final Logger log = LoggerFactory.getLogger(CMRApiController.class);

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cmrOperators", produces = "application/json")
    @ResponseBody
    public Collection cmrOperatorList() {
        return cmrService.RigCMROperators();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cmr/operators", produces = "application/json")
    @ResponseBody
    public Collection cmrOperatorsByReportDate() {
        return cmrService.RigCMROperators();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cmr/wells", produces = "application/json")
    @ResponseBody
    public Object cmrWellsByRigAndProject(@RequestParam String rig,
                                          @RequestParam String job_no) {
        return cmrService.getJobWells(rig, job_no).toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cmr/regions", produces = "application/json")
    @ResponseBody
    public Collection cmrRegionList() {
        return cmrService.getAllRegions();
    }

    @CrossOrigin
    @GetMapping(value = "/cmr/countyProvinceList", produces = "application/json")
    @ResponseBody
    public Object ropDethData(@RequestParam String region,
                              @RequestParam Date starttime,
                              @RequestParam Date endtime) {
        return cerebraAPIDataLayer.cmrCountyList(region, starttime, endtime).toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cmrOperatorsRegions", produces = "application/json")
    @ResponseBody
    public Collection cmrOperatorRegionList(@RequestParam("operator") String operator) {
        return cmrService.RigCMROperatorRegions(operator);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cmrOperatorsRegionRigTypes", produces = "application/json")
    @ResponseBody
    public Collection cmrOperatorRegionRigTypeList(@RequestParam("operator") String operator,
                                                   @RequestParam("region") String region) {
        return cmrService.RigCMROperatorRegionRigTypes(operator, region);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cmrRigListbyOperatorRegionRigType", produces = "application/json")
    @ResponseBody
    public Collection cmrRigListByOperatorRegionRigType(@RequestParam("operator") String operator,
                                                        @RequestParam("region") String region,
                                                        @RequestParam("rigtype") String rigType) {
        return cmrService.RigCMRRiglistbyOperatorRegionRigType(operator, region, rigType);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cmr/rigListByRegion", produces = "application/json")
    @ResponseBody
    public Collection cmrRigListByRegion(@RequestParam("regions") String regions) {
        return cmrService.RigCMROperatorRegionRigTypes("All Operators", regions);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cmr/rigTypeListByRegionTime", produces = "application/json")
    @ResponseBody
    public Collection cmrRigListByRegionTime(@RequestParam("regions") String regions,
                                             @RequestParam("starttime") String startTime,
                                             @RequestParam("endtime") String endTime) {
        return cmrService.RigCMROperatorRegionRigTypes("All Operators", regions, startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cmr/rigTypeByTime", produces = "application/json")
    @ResponseBody
    public Collection cmrRigListByRegionTime(@RequestParam("starttime") String startTime,
                                             @RequestParam("endtime") String endTime) {
        return cmrService.RigCMROperatorRegionRigTypes("All Operators", "All Regions", startTime, endTime);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cmr/rigListByTypeTime", produces = "application/json")
    @ResponseBody
    public Collection cmrRigListByTimeType(@RequestParam("starttime") String startTime,
                                           @RequestParam("endtime") String endTime,
                                           @RequestParam("rigtype") String rigType) {
        return cmrService.RigCMRRigListByTimeType(startTime, endTime, rigType);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cmr/rigListByTypeTimeSelection", produces = "application/json")
    @ResponseBody
    public Collection cmrRigListByTimeTypeSelection(@RequestParam("starttime") String startTime,
                                                    @RequestParam("endtime") String endTime,
                                                    @RequestParam("rigtype") String rigType) {
        return cmrService.RigCMRRigListByTimeType(startTime, endTime, rigType, Optional.of(6));
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cmr/live", produces = "application/json")
    @ResponseBody
    public Object cmrRigListByTimeTypeSelection() {
        return "[[\"2018-10-28T02:08:01.993Z\",8.33279513056775],[\"2018-10-28T02:08:02.993Z\",6.223334955596496],[\"2018-10-28T02:08:03.993Z\",8.971799115620556],[\"2018-10-28T02:08:04.993Z\",8.30542836785636],[\"2018-10-28T02:08:05.993Z\",7.5398176184781285],[\"2018-10-28T02:08:06.993Z\",3.042087105085003],[\"2018-10-28T02:08:07.993Z\",8.705481009562547],[\"2018-10-28T02:08:08.993Z\",9.329590637032066],[\"2018-10-28T02:08:09.993Z\",3.4815788777946977],[\"2018-10-28T02:08:10.993Z\",5.5347524987073315]]";
    }


    @CrossOrigin
    @GetMapping(value = "/cmr/daily/depth/wellD", produces = "application/json")
    @ResponseBody
    public Object getDepth() {

        // 2 series
        //return "[[1,5948,948], [2,8105,105], [3,11248,1248], [4,8989,989], [5,11816,1181], [6,18274,8274], [7,18111,1811]]";

        //2 series no y
        return "[[1,5948,948], [2,8105,105], [3,11248,1248], [4,8989,989], [5,11816,1181], [6,18274,8274], [7,18111,1811]]";
        //1 serie
        //return "[[1,5948], [2,8105], [3,11248], [4,8989], [5,11816], [6,18274], [7,18111]]";

        //return "[null,7275.0,8810.0,10893.0,13682.0,16189.0,16189.0,16189.0","16189.0","16189.0"]";
    }

    @CrossOrigin
    @GetMapping(value = "/cmr/daily/depth", produces = "application/json")
    @ResponseBody
    public Object getDailyDepth(@RequestParam Collection<Long> wells) {
        return cerebraAPIDataLayer.getDailyDepth(wells).toString();
    }

    @CrossOrigin
    @GetMapping(value = "/cmr/depth", produces = "application/json")
    @ResponseBody
    public Object getDepth(@RequestParam Collection<Long> wells) {
        return cmrService.getDepth(wells).toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/openPdfFile", produces = "application/pdf")
    @ResponseBody
    public ResponseEntity OpenPdfFile(@RequestParam("pdf_type") String pdf_type,
                                      @RequestParam("rig_name") String rig_name,
                                      @RequestParam("date_input") String date_input) throws FileNotFoundException {
        return cmrService.OpenFile(pdf_type, rig_name, date_input);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cmr/allWellActivity", produces = "application/json")
    @ResponseBody
    public Collection allWellActivity() {
        return cmrService.getWellActivtyAllWells();
    }

    @CrossOrigin
    @GetMapping(value = "/cmr/ropActivity", produces = "application/json")
    @ResponseBody
    public Object ropActivity(@RequestParam(name = "welllist") Collection<Long> wells) {
        return cerebraAPIDataLayer.getROPActivityAllRigsWells(wells).toString();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cmr/allWellActivityROP", produces = "application/json")
    @ResponseBody
    public Object allWellActivityROP() {
//        Object ret = cmrService.getWellActivtyAllWellsROP();
//        log.info("{}", ret);
        return cmrService.getWellActivtyAllWellsROP();
//        return ret;
    }

    @CrossOrigin
    @GetMapping(value = "/cmr/ropDepthData", produces = "application/json")
    @ResponseBody
    public Object ropDethData(@RequestParam(name = "welllist") Collection<Long> wells) {
        return cmrService.getRopDepthData(wells);
    }

    @CrossOrigin
    @GetMapping(value = "/cmr/trippingData", produces = "application/json")
    @ResponseBody
    public Object trippingData(@RequestParam(name = "welllist") Collection<Long> wells) {
        return cmrService.getTrippingData(wells);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/cmr/ropDepthTop", produces = "application/json")
    @ResponseBody
    public Collection ropDethDataTop() {
        return cmrService.getRopDepthDataTop();
    }

    /**
     * last completed wells per rig_name
     *
     * @param rig       rig_name url parameter. rig the wells belong to
     * @param lastWells either all | number. defaults to limit 0 (all)
     * @return
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/lastCompletedWells/{lastWells}", produces = "application/json")
    @ResponseBody
    public Object getWells(@RequestParam("rig_name") String rig,
                           @PathVariable("lastWells") String lastWells) {
        try {
            return cmrService.getWells(rig, Integer.parseInt(lastWells));
        } catch (Exception e) {
            if (!lastWells.equalsIgnoreCase("all")) {
                return ResponseEntity.ok(ResponseEntity.notFound()); //ResponseEntity.badRequest()notFound();
            }
        }
        //return all
        return cmrService.getWells(rig, 0);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/lastCompletedWells", produces = "application/json")
    @ResponseBody
    public Collection getAllLastWells(@RequestParam("rig_name") String rig,
                                      @RequestParam(defaultValue = "0") int limit) {
        return cmrService.getWells(rig, limit);
    }

    @CrossOrigin
    @GetMapping(value = "/completedWellsList", produces = "application/json")
    public Object getAllLastWells(@RequestParam("region") String region,
                                  @RequestParam("starttime") Date startTime) {
        return cmrService.getCompletedWellsList(region, startTime).toString();
    }

    @CrossOrigin
    @GetMapping(value = "/completedWells", produces = "application/json")
    public Object completedWells(@RequestParam("region") String region,
                                 @RequestParam("starttime") Date startTime,
                                 @RequestParam("alias") boolean alias) {
        return cmrService.getCompletedWells(region, startTime, alias).toString();
    }

    @CrossOrigin
    @GetMapping(value = "/cmr/spudToTD/top/wells", produces = "application/json")
    @ResponseBody
    public Object getTopWellsBySpudToTDInRegion(@RequestParam(required = false) String region,
                                                @RequestParam(required = false) String county,
                                                @RequestParam(required = false) String stateProvince,
                                                @RequestParam(required = false) Date starttime,
                                                @RequestParam(required = false) Date endtime,
                                                @RequestParam(required = false) boolean alias) {

        if (null != region) {
            return cmrService.getSpudToTDWells(region).toString();
        } else if (null != county && null != stateProvince) {
            return cmrService.getSpudToTDWells(starttime, endtime, county, stateProvince, alias).toString();
        }
        return ResponseEntity.badRequest();
    }

    @CrossOrigin
    @GetMapping(value = "/cmr/footagePerDay/wells", produces = "application/json")
    @ResponseBody
    public Object getWellsByFootage(@RequestParam(required = false) String county,
                                    @RequestParam(required = false) String stateProvince,
                                    @RequestParam(required = false) Date starttime,
                                    @RequestParam(required = false) Date endtime,
                                    @RequestParam(required = false) boolean alias) {
        return cmrService.getFootagePerDayWells(starttime, endtime, county, stateProvince, alias).toString();
    }

    @CrossOrigin
    @GetMapping(value = "/cmr/footagePerDay/top/wells", produces = "application/json")
    @ResponseBody
    public Object getTopWellsByFootage(@RequestParam(required = false) String region,
                                       @RequestParam(required = false) String county,
                                       @RequestParam(required = false) String stateProvince,
                                       @RequestParam(required = false) Date starttime,
                                       @RequestParam(required = false) Date endtime,
                                       @RequestParam(required = false) boolean alias) {

        if (null != region) {
            return cmrService.getTopFootagePerDayWells(region).toString();
        } else if (null != county && null != stateProvince) {
            return cmrService.getTopFootagePerDayWells(starttime, endtime, county, stateProvince, alias).toString();
        }
        return ResponseEntity.badRequest();

    }

    @CrossOrigin
    @GetMapping(value = "/cmr/footagePerDay/spudToTD/legend/wells", produces = "application/json")
    @ResponseBody
    public Object getTopWellsByFootageAndSpudToTDLegend(@RequestParam(required = false) String region) {

        return cmrService.getTopWellsByFootageAndSpudToTDLegend(region).toString();
    }

    @CrossOrigin
    @GetMapping(value = "/cmr/projects", produces = "application/json")
    @ResponseBody
    public Object getLastProjects(@RequestParam String rig_name,
                                  @RequestParam(required = false, defaultValue = "6") int limit) {

        return cmrService.getLastProjects(rig_name, limit).toString();

    }

    @CrossOrigin
    @GetMapping(value = "/cmr/casing", produces = "application/json")
    @ResponseBody
    public Object getCasingStatus(@RequestParam Collection<Long> welllist) {

        return cmrService.getCasingStatus(welllist).toString();

    }

}
