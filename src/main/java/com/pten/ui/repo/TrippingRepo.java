package com.pten.ui.repo;

import com.pten.Utilities.DateUtils.DateTimeUtils;
import com.pten.Utilities.DbUtils.ImpalaDbUtilLib;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Date;

@Component
public class TrippingRepo {

    private static final Logger log = LoggerFactory.getLogger(TrippingRepo.class);

    @Autowired
    ImpalaDbUtilLib impalaDbUtilLib;


    public Collection getReasons(Collection<Long> wells, Date startDate, Date endDate, int user) {

        long starttime = DateTimeUtils.atStartOfDay(DateTimeUtils.toLocalDate(startDate));
        long endtime = DateTimeUtils.dateToLastSecondOfDay(DateTimeUtils.toLocalDate(endDate));

        String query = "SELECT ws.trip_reason \n" +
                "   , SUM(ws.total_sec) AS total_time \n" +
                "   , SUM(ws.stand_length) AS total_distance \n" +
                "FROM ptendrilling.vw_wellstands_tripping ws \n" +
                "   INNER JOIN user_Security.user_mapping_details sec ON sec.well_num = ws.well_num \n" +
                "       AND sec.well_num IN (:wells) \n" +
                "       AND sec.userid = :user \n" +
                "WHERE UPPER(ws.stand_type) = 'TRIPPING' \n" +
                "   AND ws.stand_start_time BETWEEN :start_date AND :end_date \n" +
                "GROUP BY ws.trip_reason;";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells)
                .addValue("user", user)
                .addValue("start_date", starttime)
                .addValue("end_date", endtime);

        return impalaDbUtilLib.executeNamedQuery(query, namedParameters);
    }

    public Collection getSpeeds(Collection<Long> wells, Date startDate, Date endDate, String entityName, String direction, String holestatus, int userId) {

        long starttime = DateTimeUtils.atStartOfDay(DateTimeUtils.toLocalDate(startDate));
        long endtime = DateTimeUtils.dateToLastSecondOfDay(DateTimeUtils.toLocalDate(endDate));

        StringBuilder q = new StringBuilder("SELECT\n");

        q.append(getSpeedSelect(entityName));

        q.append("\n,SUM(CASE WHEN ws.in_out = 'IN' THEN ws.stand_length ELSE NULL END) / SUM(CASE WHEN ws.in_out = 'IN' THEN ws.total_sec ELSE NULL END) * 3600.0 AS tripping_speed_in \n" +
                ",SUM(CASE WHEN ws.in_out = 'OUT' THEN ws.stand_length ELSE NULL END) / SUM(CASE WHEN ws.in_out = 'OUT' THEN ws.total_sec ELSE NULL END) * 3600.0 AS tripping_speed_out \n" +
                "FROM ptendrilling.vw_wellstands_tripping ws \n");
        if (entityName.equalsIgnoreCase("RIG") || entityName.equalsIgnoreCase("WELL")) {
            q.append("INNER JOIN ptendrilling.ptendrilling_welldata wd ON ws.well_num = wd.well_num\n");
        }
        q.append("INNER JOIN user_Security.user_mapping_details sec ON ws.well_num = sec.well_num AND sec.userid = :user AND sec.well_num IN (:wells) \n" +
                "WHERE ws.stand_start_time BETWEEN :start_date AND :end_date \n");

        if (!direction.equalsIgnoreCase("ALL")) {
            q.append("   AND UPPER(ws.in_out) = :direction\n"); // + ((inOut == "IN"), "'IN'", "'OUT'")
        }

        if (!holestatus.equalsIgnoreCase("ALL")) {
            q.append("   AND UPPER(ws.hole_status) = :holestatus\n"); // + ((casedOpen == "CASED") , "'CASED'", "'OPEN'") + " \n", "") +
        }

        q.append("GROUP BY ");

        q.append(getSpeedGroupBy(entityName));

        q.append(";");

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells)
                .addValue("start_date", starttime)
                .addValue("end_date", endtime)
                .addValue("direction", direction)
                .addValue("holestatus", holestatus)
                .addValue("user", userId);

        return impalaDbUtilLib.executeNamedQuery(q.toString(), namedParameters);
    }

    private String getSpeedSelect(String entityName) {
        switch (entityName.toUpperCase()) {
            case "RIG":
                return "STRRIGHT(wd.rig_name,3) as rig_name";
            case "WELL":
                return "CONCAT(UPPER(STRRIGHT(wd.rig_name, 3)), ' – ', UPPER(wd.well_name)) as well_name";
            default:
                return "CASE WHEN (NOT (ws.crew IS NULL)) THEN ws.crew ELSE 'UNKNOWN' END AS crew";
        }
    }

    private String getSpeedGroupBy(String entityName) {
        switch (entityName.toUpperCase()) {
            case "RIG":
                return "wd.rig_name";
            case "WELL":
                return "well_name";
            default:
                return "CASE WHEN (NOT (ws.crew IS NULL)) THEN ws.crew ELSE 'UNKNOWN' END";
        }
    }

    private String getConnectionSelect(String entityName) {
        switch (entityName.toUpperCase()) {
            case "RIG":
                return "UPPER(STRRIGHT(wd.rig_name, 3)) AS entity_name";
            case "WELL":
                return "CONCAT(UPPER(STRRIGHT(wd.rig_name, 3)), ' – ', UPPER(wd.well_name)) AS entity_name";
            case "CREW":
                return "UPPER(wd.well_name) AS entity_name ";
            default:
                return "date_part('year', to_timestamp(ws.stand_start_time)) AS entity_year\n" +
                        ",date_part('month', to_timestamp(ws.stand_start_time)) AS entity_month";
        }
    }

    private String getConnectionFrom(String entityName) {
        switch (entityName.toUpperCase()) {
            case "TREND":
                return "";
            case "WELL":
            case "RIG":
            default:
                return "INNER JOIN ptendrilling.ptendrilling_welldata wd ON ws.well_num = wd.well_num";
        }
    }

    private String getConnectionGroupBy(String entityName) {
        switch (entityName.toUpperCase()) {
            case "RIG":
                return "UPPER(STRRIGHT(wd.rig_name, 3))";
            case "WELL":
            case "CREW":
                return "entity_name";
            default:
                return "entity_year\n" +
                        ",entity_month";
        }
    }


    private String getConnectionTimeSelect(String entityName) {
        switch (entityName.toUpperCase()) {
            case "RIG":
                return "UPPER(STRRIGHT(wd.rig_name, 3)) AS entity_name";
            case "WELL":
                return "CONCAT(UPPER(STRRIGHT(wd.rig_name, 3)), ' – ', UPPER(wd.well_name)) AS entity_name";
            case "TIME":
                return "CONCAT(CAST(date_part('year', to_timestamp(ws.stand_start_time)) AS STRING), '/', CAST(date_part('month', to_timestamp(ws.stand_start_time)) AS STRING)) AS entity_name";
            default:
                return "UPPER(ws.crew) AS entity_name";
        }
    }

    private String getConnectionTimeFrom(String entityName) {
        switch (entityName.toUpperCase()) {
            case "RIG":
            case "WELL":
                return " INNER JOIN ptendrilling.ptendrilling_welldata wd ON ws.well_num = wd.well_num\n";
            default:
                return "";
        }
    }

    private String getConnectionBreakdownSelect(String direction) {
        switch (direction) {
            case "IN":
                return "AVG(ws.sec_tih_moving) AS sec_tih_moving\n" +
                        "   ,AVG(ws.sec_tih_stab) AS sec_tih_stab\n" +
                        "   ,AVG(ws.sec_tih_makeup) AS sec_tih_makeup\n";
            default:
                return "AVG(ws.sec_tooh_breakout) AS sec_tooh_breakout\n" +
                        "        ,AVG(ws.sec_tooh_racking) AS sec_tooh_racking\n" +
                        "        ,AVG(ws.sec_tooh_moving) AS sec_tooh_moving\n" +
                        "        ,AVG(ws.sec_tooh_slips) AS sec_tooh_slips\n";
        }
    }

    private String getConnectionBreakdownEntityName(String entityName) {
        switch (entityName.toUpperCase()) {
            case "RIG":
                return "UPPER(STRRIGHT(wd.rig_name, 3)) AS entity_name";
            case "WELL":
                return "CONCAT(UPPER(STRRIGHT(wd.rig_name, 3)), ' – ', UPPER(wd.well_name)) AS entity_name";
            case "TIME":
                return "CONCAT(CAST(date_part('year', to_timestamp(ws.stand_start_time)) AS STRING), '/', CAST(date_part('month', to_timestamp(ws.stand_start_time)) AS STRING)) AS entity_name";
            default:
                return "UPPER(ws.crew) AS entity_name";
        }
    }

    private String getConnectionBreakdownFrom(String entityName) {
        switch (entityName.toUpperCase()) {
            case "RIG":
            case "WELL":
                return " INNER JOIN ptendrilling.ptendrilling_welldata wd ON ws.well_num = wd.well_num\n";
            default:
                return "";
        }
    }

    public Collection getRigSpeedTrend(Collection<Long> wells, Date startDate, Date endDate, String direction, String holestatus, String order, int userId) {
        long starttime = DateTimeUtils.atStartOfDay(DateTimeUtils.toLocalDate(startDate));
        long endtime = DateTimeUtils.dateToLastSecondOfDay(DateTimeUtils.toLocalDate(endDate));

        int limit = 10;

        StringBuilder q = new StringBuilder("WITH LastCMR AS (\n" +
                "SELECT a.well_num \n" +
                "              , a.rig_name \n" +
                "              , a.contractor_business_unit \n" +
                "  FROM pten_cmr.cmr_data_usalt7 a \n" +
                " WHERE a.well_num IN (:wells) \n" +
                "   AND a.report_date BETWEEN :start_date - 86400 AND :end_date + 86400 \n" +
                "   AND NOT EXISTS (\n" +
                "        SELECT NULL\n" +
                "          FROM pten_cmr.cmr_data_usalt7 b\n" +
                "         WHERE a.well_num = b.well_num\n" +
                "           AND a.report_date < b.report_date\n" +
                "   ) \n" +
                ") \n" +
                ", RigsRanked AS ( \n" +
                " SELECT STRRIGHT(wd.rig_name, 3) AS rig_name \n" +
                "   ,SUM(ws.stand_length) / SUM(ws.total_sec) AS tripping_speed" +
                " FROM ptendrilling.vw_wellstands_tripping ws \n" +
                "     INNER JOIN ptendrilling.ptendrilling_welldata wd ON ws.well_num = wd.well_num \n" +
                " WHERE wd.well_num IN (:wells) \n" +
                "   AND ws.stand_start_time BETWEEN :start_date AND :end_date \n");

        if (!direction.equalsIgnoreCase("ALL")) {
            q.append("   AND UPPER(ws.in_out) = :direction\n"); // + ((inOut == "IN"), "'IN'", "'OUT'")
        }

        if (!holestatus.equalsIgnoreCase("ALL")) {
            q.append("   AND UPPER(ws.hole_status) = :holestatus\n"); // + ((casedOpen == "CASED") , "'CASED'", "'OPEN'") + " \n", "") +
        }
        q.append("   AND UPPER(ws.stand_type) = 'TRIPPING' \n" +
                " GROUP BY wd.rig_name \n" +
                " ORDER BY tripping_speed");
        if (order.equalsIgnoreCase("BOTTOM")) {
            q.append(" ASC\n");
        }else {
            q.append(" DESC\n");
        }

        q.append("LIMIT :returnCount )\n" +
                " SELECT \n" +
                "rr.rig_name AS entity_name,\n" +
                "  date_part('year', to_timestamp(ws.stand_start_time)) AS observation_year,\n" +
                "   date_part('month', to_timestamp(ws.stand_start_time)) AS observation_month,\n" +
                "   date_part('day' , to_timestamp(ws.stand_start_time)) AS observation_day,\n" +
                "   SUM(ws.stand_length) / SUM(ws.total_sec) * 3600.0 AS tripping_speed \n" +
                " FROM ptendrilling.vw_wellstands_tripping ws \n" +
                "              INNER JOIN LastCMR cmr ON ws.well_num = cmr.well_num \n" +
                "              INNER JOIN user_Security.user_mapping_details sec ON sec.well_num = cmr.well_num AND sec.well_num IN (:wells) AND sec.userid = :user \n" +
                " INNER JOIN RigsRanked rr ON cmr.rig_name = rr.rig_name \n" +
                " WHERE ws.stand_start_time BETWEEN :start_date AND :end_date \n");

        if (!direction.equalsIgnoreCase("ALL")) { //if (!inOut.equalsIgnoreCase("ALL")) {
            q.append("AND UPPER(ws.in_out) = :direction"); // " + ((inOut == "IN"), "'IN'", "'OUT'") + " \n", "") +
        }
        if (!holestatus.equalsIgnoreCase("ALL")) { //if (!casedOpen.equalsIgnoreCase("ALL")) {
            q.append("   AND UPPER(ws.hole_status) = :holestatus\n"); // + ((casedOpen == "CASED"), "'CASED'", "'OPEN'") + " \n", "") +
        }

        q.append("   AND UPPER(ws.stand_type) = 'TRIPPING' \n" +
                " GROUP BY  observation_year,\n" +
                "        observation_month,\n" +
                "        observation_day,\n" +
                "        rr.rig_name \n" +
                " ORDER BY 1,2,3,4;");

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells)
                .addValue("start_date", starttime)
                .addValue("end_date", endtime)
                .addValue("direction", direction)
                .addValue("user", userId)
                .addValue("holestatus", holestatus)
                .addValue("returnCount", limit);

        return impalaDbUtilLib.executeNamedQuery(q.toString(), namedParameters);
    }

    public Collection getRigTypeSpeedTrend(Collection<Long> wells, Date startDate, Date endDate, String direction, String holestatus, String order, int userId) {

        long starttime = DateTimeUtils.atStartOfDay(DateTimeUtils.toLocalDate(startDate));
        long endtime = DateTimeUtils.dateToLastSecondOfDay(DateTimeUtils.toLocalDate(endDate));

        int limit = 10;

        StringBuilder q = new StringBuilder("WITH LastCMR AS (\n" +
                "SELECT a.well_num \n" +
                "              , a.rig_name \n" +
                "              , a.contractor_business_unit \n" +
                "  FROM pten_cmr.cmr_data_usalt7 a \n" +
                " WHERE a.well_num IN (:wells) \n" +
                "   AND a.report_date BETWEEN :start_date - 86400 AND :end_date + 86400 \n" +
                "   AND NOT EXISTS (\n" +
                "        SELECT NULL\n" +
                "          FROM pten_cmr.cmr_data_usalt7 b\n" +
                "         WHERE a.well_num = b.well_num\n" +
                "           AND a.report_date < b.report_date\n" +
                "   ) \n" +
                ") \n");


        q.append(" SELECT \n");

        q.append("rmt.rig_master_type_name AS entity_name ,");

        q.append("  date_part('year', to_timestamp(ws.stand_start_time)) AS observation_year,\n" +
                "   date_part('month', to_timestamp(ws.stand_start_time)) AS observation_month,\n" +
                "   date_part('day' , to_timestamp(ws.stand_start_time)) AS observation_day," +
                "   SUM(ws.stand_length) / SUM(ws.total_sec) * 3600.0 AS tripping_speed \n" +
                " FROM ptendrilling.vw_wellstands_tripping ws \n" +
                "              INNER JOIN LastCMR cmr ON ws.well_num = cmr.well_num \n" +
                "              INNER JOIN user_Security.user_mapping_details sec ON sec.well_num = cmr.well_num AND sec.well_num IN (:wells) AND sec.userid = :user \n");
        q.append(" INNER JOIN pten_cmr.rig r ON cmr.rig_name = r.display_text \n" +
                " INNER JOIN pten_cmr.rigtype rt ON r.rig_type_id = rt.rig_type_id \n" +
                " INNER JOIN pten_cmr.rigmastertype rmt ON rmt.rig_master_type_id = rt.rig_master_type_id\n");

        q.append(" WHERE ws.stand_start_time BETWEEN :start_date AND :end_date \n");

        if (!direction.equalsIgnoreCase("ALL")) { //if (!inOut.equalsIgnoreCase("ALL")) {
            q.append("AND UPPER(ws.in_out) = :direction\n"); // " + ((inOut == "IN"), "'IN'", "'OUT'") + " \n", "") +
        }
        if (!holestatus.equalsIgnoreCase("ALL")) { //if (!casedOpen.equalsIgnoreCase("ALL")) {
            q.append("   AND UPPER(ws.hole_status) = :holestatus\n"); // + ((casedOpen == "CASED"), "'CASED'", "'OPEN'") + " \n", "") +
        }

        q.append("   AND UPPER(ws.stand_type) = 'TRIPPING' \n" +
                " GROUP BY  observation_year,\n" +
                "        observation_month,\n" +
                "        observation_day,\n");


        q.append(" rmt.rig_master_type_name\n");

        q.append(" ORDER BY 1,2,3,4;");

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells)
                .addValue("start_date", starttime)
                .addValue("end_date", endtime)
                .addValue("direction", direction)
                .addValue("user", userId)
                .addValue("holestatus", holestatus)
                .addValue("returnCount", limit);

        return impalaDbUtilLib.executeNamedQuery(q.toString(), namedParameters);
    }

    public Collection getRegionSpeedTrend(Collection<Long> wells, Date startDate, Date endDate, String direction, String holestatus, String order, int userId) {
        long starttime = DateTimeUtils.atStartOfDay(DateTimeUtils.toLocalDate(startDate));
        long endtime = DateTimeUtils.dateToLastSecondOfDay(DateTimeUtils.toLocalDate(endDate));

        int limit = 10;

        StringBuilder q = new StringBuilder("WITH LastCMR AS (\n" +
                "SELECT a.well_num \n" +
                "              , a.rig_name \n" +
                "              , a.contractor_business_unit \n" +
                "  FROM pten_cmr.cmr_data_usalt7 a \n" +
                " WHERE a.well_num IN (:wells) \n" +
                "   AND a.report_date BETWEEN :start_date - 86400 AND :end_date + 86400 \n" +
                "   AND NOT EXISTS (\n" +
                "        SELECT NULL\n" +
                "          FROM pten_cmr.cmr_data_usalt7 b\n" +
                "         WHERE a.well_num = b.well_num\n" +
                "           AND a.report_date < b.report_date\n" +
                "   ) \n" +
                ") \n" +

                " SELECT \n" +

                " rm.region_name AS entity_name , \n" +

                "  date_part('year', to_timestamp(ws.stand_start_time)) AS observation_year,\n" +
                "   date_part('month', to_timestamp(ws.stand_start_time)) AS observation_month,\n" +
                "   date_part('day' , to_timestamp(ws.stand_start_time)) AS observation_day,\n" +
                "   SUM(ws.stand_length) / SUM(ws.total_sec) * 3600.0 AS tripping_speed \n" +
                " FROM ptendrilling.vw_wellstands_tripping ws \n" +
                "              INNER JOIN LastCMR cmr ON ws.well_num = cmr.well_num \n" +
                "              INNER JOIN user_Security.user_mapping_details sec ON sec.well_num = cmr.well_num AND sec.well_num IN (:wells) AND sec.userid = :user \n" +
                " INNER JOIN pten_cmr.regionmap rm ON UPPER(cmr.contractor_business_unit) = UPPER(rm.contractor_business_unit) \n" +
                " WHERE ws.stand_start_time BETWEEN :start_date AND :end_date \n");

        if (!direction.equalsIgnoreCase("ALL")) { //if (!inOut.equalsIgnoreCase("ALL")) {
            q.append("AND UPPER(ws.in_out) = :direction\n"); // " + ((inOut == "IN"), "'IN'", "'OUT'") + " \n", "") +
        }
        if (!holestatus.equalsIgnoreCase("ALL")) { //if (!casedOpen.equalsIgnoreCase("ALL")) {
            q.append("   AND UPPER(ws.hole_status) = :holestatus\n"); // + ((casedOpen == "CASED"), "'CASED'", "'OPEN'") + " \n", "") +
        }

        q.append("   AND UPPER(ws.stand_type) = 'TRIPPING' \n" +
                " GROUP BY  observation_year,\n" +
                "        observation_month,\n" +
                "        observation_day,\n" +
                " rm.region_name \n" +
                " ORDER BY 1,2,3,4;");

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells)
                .addValue("start_date", starttime)
                .addValue("end_date", endtime)
                .addValue("direction", direction)
                .addValue("user", userId)
                .addValue("holestatus", holestatus)
                .addValue("returnCount", limit);

        return impalaDbUtilLib.executeNamedQuery(q.toString(), namedParameters);
    }

    public Collection getConnections(Collection<Long> wells, Date startDate, Date endDate, String entityName, int userId) {

        long starttime = DateTimeUtils.atStartOfDay(DateTimeUtils.toLocalDate(startDate));
        long endtime = DateTimeUtils.dateToLastSecondOfDay(DateTimeUtils.toLocalDate(endDate));

        StringBuilder q = new StringBuilder("SELECT ws.in_out, \n");

        q.append(getConnectionSelect(entityName));

        q.append("\n,AVG(ws.sec_s2s) AS sec_s2s\n");

        q.append("FROM ptendrilling.vw_wellstands_tripping ws\n");

        q.append(getConnectionFrom(entityName));

        q.append("\nWHERE ws.stand_start_time BETWEEN :start_date AND :end_date \n" +
                "        AND ws.well_num IN (:wells) \n" +
                "        AND UPPER(ws.stand_type) = 'TRIPPING' \n" +
                "        AND ws.sec_s2s BETWEEN 30 AND 200 \n" +
                "        AND ws.starting_bit_depth > 500.0 \n" +
                "        AND ws.ending_bit_depth > 500.0 \n" +
                "GROUP BY ws.in_out, ");

        q.append(getConnectionGroupBy(entityName));

        q.append(";");

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells)
                .addValue("start_date", starttime)
                .addValue("end_date", endtime)
                .addValue("user", userId);

        return impalaDbUtilLib.executeNamedQuery(q.toString(), namedParameters);
    }

    public Collection getConnectionDistribution(Collection<Long> wells, Date startDate, Date endDate, String direction, String holeStatus,  int userId) {

        long starttime = DateTimeUtils.atStartOfDay(DateTimeUtils.toLocalDate(startDate));
        long endtime = DateTimeUtils.dateToLastSecondOfDay(DateTimeUtils.toLocalDate(endDate));

        double bucketSize = 2.0;
        int target = 96;
        int max = 200;
        int min = 30;

        StringBuilder q = new StringBuilder("WITH TopQuery AS ( \n" +
                "SELECT :bucketSize AS bucket_size \n" +
                "     , :max AS range_end \n" +
                "     , :min AS range_start \n" +
                "     , AVG(ws.sec_s2s) OVER() AS mean \n" +
                "     , CEIL(ws.sec_s2s / 2.0) AS bucket \n" +
                "FROM ptendrilling.vw_wellstands_tripping ws \n" +
                "WHERE ws.well_num IN (:wells)\n" +
                "AND ws.stand_start_time BETWEEN :start_date AND :end_date\n");

        if (!"ALL".equalsIgnoreCase(holeStatus)) {
            q.append("AND UPPER(ws.hole_status) = :casedOpen\n");
        }
        if (!"ALL".equalsIgnoreCase(direction)) {
            q.append("AND UPPER(ws.in_out) = :inOut\n");
        }

        q.append(")\n");

        q.append("SELECT bucket \n" +
                "     , COUNT(*) AS value \n" +
                "     , :bucketSize * bucket AS category \n" +
                "     , max(mean) as mean \n" +
                "     , :target as target\n" +
                "FROM TopQuery \n" +
                "GROUP by bucket \n" +
                "ORDER by bucket");
        q.append(";");

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells)
                .addValue("start_date", starttime)
                .addValue("end_date", endtime)
                .addValue("casedOpen", holeStatus)
                .addValue("inOut", direction)
                .addValue("bucketSize", bucketSize)
                .addValue("target", target)
                .addValue("min", min)
                .addValue("max", max)
                .addValue("user", userId);

        return impalaDbUtilLib.executeNamedQuery(q.toString(), namedParameters);
    }

    public Collection getConnectionTime(Collection<Long> wells, Date startDate, Date endDate, String entityType, int returnCount, int userId) {

        long starttime = DateTimeUtils.atStartOfDay(DateTimeUtils.toLocalDate(startDate));
        long endtime = DateTimeUtils.dateToLastSecondOfDay(DateTimeUtils.toLocalDate(endDate));

        StringBuilder q = new StringBuilder("SELECT " +
                "  AVG(CASE UPPER(ws.in_out) WHEN 'IN' THEN ws.sec_s2s ELSE NULL END) AS in_time\n" +
                ", AVG(CASE UPPER(ws.in_out) WHEN 'OUT' THEN ws.sec_s2s ELSE NULL END) AS out_time\n" +
                ", AVG(ws.sec_s2s) AS total_time\n" +
                ", SUM(CASE UPPER(ws.in_out) WHEN 'IN' THEN 1 ELSE 0 END) AS in_count\n" +
                ", SUM(CASE UPPER(ws.in_out) WHEN 'OUT' THEN 1 ELSE 0 END) AS out_count\n,");

        q.append(getConnectionTimeSelect(entityType));

        q.append("\nFROM ptendrilling.vw_wellstands_tripping ws\n");

        q.append(getConnectionTimeFrom(entityType));

        q.append("WHERE UPPER(ws.stand_type) = 'TRIPPING'\n" +
                "   AND ws.sec_s2s BETWEEN 30 AND 200\n" +
                "   AND ws.well_num IN (:wells)\n" +
                "   AND ws.stand_start_time BETWEEN :start_date AND :end_date\n" +
                "   AND ws.starting_bit_depth > 500.0\n" +
                "   AND ws.ending_bit_depth > 500.0\n" +
                "GROUP BY entity_name\n" +
                "ORDER BY AVG(ws.sec_s2s) LIMIT :returnCount");

        q.append(";");

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells)
                .addValue("start_date", starttime)
                .addValue("end_date", endtime)
                .addValue("returnCount", returnCount)
                .addValue("user", userId);

        return impalaDbUtilLib.executeNamedQuery(q.toString(), namedParameters);
    }

    public Collection getConnectionBreakdown(Collection<Long> wells, Date startDate, Date endDate, String entityType, String direction, int userId) {

        long starttime = DateTimeUtils.atStartOfDay(DateTimeUtils.toLocalDate(startDate));
        long endtime = DateTimeUtils.dateToLastSecondOfDay(DateTimeUtils.toLocalDate(endDate));

        StringBuilder q = new StringBuilder("SELECT\n");

        q.append(getConnectionBreakdownSelect(direction));

        q.append(",").append(getConnectionBreakdownEntityName(entityType));

        q.append("\nFROM ptendrilling.vw_wellstands_tripping ws\n");

        q.append(getConnectionBreakdownFrom(entityType));

        q.append("WHERE UPPER(ws.stand_type) = 'TRIPPING'  \n" +
                "   AND ws.sec_s2s BETWEEN 30 AND 200 \n" +
                "   AND ws.well_num IN (:wells)\n" +
                "   AND ws.stand_start_time BETWEEN :start_date AND :end_date \n" +
                "   AND ws.starting_bit_depth > 500.0 \n" +
                "   AND ws.ending_bit_depth > 500.0 \n" +
                "   AND UPPER(ws.in_out) = :direction \n" +
                " GROUP BY entity_name");

        q.append(";");

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells)
                .addValue("start_date", starttime)
                .addValue("end_date", endtime)
                .addValue("direction", direction)
                .addValue("user", userId);

        return impalaDbUtilLib.executeNamedQuery(q.toString(), namedParameters);
    }

}
