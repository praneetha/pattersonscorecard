package com.pten.ui.repo;

import com.pten.Utilities.DateUtils.DateTimeUtils;
import com.pten.Utilities.DbUtils.ImpalaDbUtilLib;
import com.pten.Utilities.UtilityLibrary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;

/**
 * B.Seiler - This is wisdom - we shall retain.
 * this class should directly call data from dataLayer JSONArray responses.
 * The .Repo layer will serve as optional design in case other kind of mapping
 * is used for the results sets from DB.
 * Keeping methods simple in case this class becomes an interface
 */

@Component
public class RMCTRepo {
    private static final Logger log = LoggerFactory.getLogger(RMCTRepo.class);

    private final int TOP_10 = 10;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    UtilityLibrary myUtils;

    @Autowired
    ImpalaDbUtilLib impalaDbUtil; 
    
    public int getUser_id() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().
                getAuthentication().getPrincipal();
        return Integer.parseInt(userDetails.getUsername());
    }

    public Collection getRMCTByRegionRigTypeTimeRangeAggMonth(Collection<String> regionlist, Collection<Integer> rigtypelist, String starttime, String endtime) throws ParseException {

        // Convert dates to epoch and move to appropriate month boundaries
        final long startEpoch = DateTimeUtils.dateToFirstDayOfMonth(starttime);
        final long endEpoch = DateTimeUtils.dateToLastDayOfMonth(endtime);

        boolean regions = regionlist.stream().anyMatch(f -> "All Regions".equalsIgnoreCase(f));
        boolean rigtype = rigtypelist.stream().anyMatch(f -> f == -99);

        String query = "WITH NotOnes AS (\n" +
                "        SELECT a.rig_name\n" +
                "             , a.well_num\n" +
                "             , a.report_date\n" +
                "          FROM pten_cmr.vw_cmr_data_usalt7_release a inner join user_Security.user_mapping_Details u on a.well_num=u.well_num and u.userid="+getUser_id()+" \n" +
                "         WHERE EXISTS (\n" +
                "            SELECT NULL\n" +
                "              FROM pten_cmr.cmr_time_code_data_usalt7 b\n" +
                "             WHERE a.cmr_data_id = b.cmr_data_id\n" +
                "               AND COALESCE(b.code, '1') <> '1')\n" +
                "        ), ActivationMoves AS (\n" +
                "        SELECT c.rig_name\n" +
                "            , c.well_num\n" +
                "          FROM pten_cmr.vw_cmr_data_usalt7_release c inner join user_Security.user_mapping_Details u on c.well_num=u.well_num and u.userid="+getUser_id()+" \n" +
                "         WHERE NOT EXISTS (\n" +
                "            SELECT NULL\n" +
                "              FROM NotOnes d\n" +
                "             WHERE c.rig_name = d.rig_name\n" +
                "               AND d.report_date BETWEEN c.report_date - 2592001 AND c.report_date - 1)\n" +
                "        ), TopQuery AS (\n" +
                "        SELECT YEAR(to_timestamp(e.daywork_start_time)) AS move_year\n" +
                "            , MONTH(to_timestamp(e.daywork_start_time)) AS move_month\n" +
                "            , COUNT(*) OVER(PARTITION BY YEAR(to_timestamp(e.daywork_start_time)), MONTH(to_timestamp(e.daywork_start_time))) AS move_count\n" +
                "            , AVG((e.rig_move_time) / 24.0) OVER(PARTITION BY YEAR(to_timestamp(e.daywork_start_time)), MONTH(to_timestamp(e.daywork_start_time))) AS move_avg_days\n" +
                "            , COUNT(*) OVER(PARTITION BY YEAR(to_timestamp(e.daywork_start_time))) AS year_move_count\n" +
                "            , AVG((e.rig_move_time) / 24.0) OVER(PARTITION BY YEAR(to_timestamp(e.daywork_start_time))) AS year_move_avg_days\n" +
                "            , COUNT(*) OVER () AS overall_move_count\n" +
                "            , AVG((e.rig_move_time) / 24.0) OVER() AS overall_move_avg_days\n" +
                "          FROM pten_cmr.vw_cmr_data_usalt7_release e inner join user_Security.user_mapping_Details u on e.well_num=u.well_num and u.userid="+getUser_id()+" \n" +
                "            INNER JOIN pten_cmr.regionmap rm ON UPPER(e.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "            INNER JOIN pten_cmr.rig r ON r.display_text = e.rig_name\n" +
                "            INNER JOIN pten_cmr.rigtype rt ON r.rig_type_id = rt.rig_type_id\n" +
                "            INNER JOIN pten_cmr.rigmastertype rmt ON rmt.rig_master_type_id = rt.rig_master_type_id\n" +
                "         WHERE COALESCE(e.rig_move_time, 0.0) > 0.0\n" +
                "           AND COALESCE(e.trucks_move_distance, 0.0) < 100.0\n" +
                "           AND e.daywork_start_time BETWEEN :start AND :end\n"+
                "           "+ (!regions? "AND UPPER(rm.region_name) IN (:regions)\n": "") +
                "           "+ (!rigtype? "AND rt.rig_master_type_id IN (:rigTypes)\n": "") +
                "           AND NOT EXISTS (\n" +
                "            SELECT NULL\n" +
                "              FROM ActivationMoves f\n" +
                "             WHERE e.well_num = f.well_num)\n" +
                "           AND NOT EXISTS (\n" +
                "            SELECT NULL\n" +
                "              FROM pten_cmr.vw_cmr_data_usalt7_release g\n" +
                "             WHERE e.well_num = g.well_num\n" +
                "               AND g.report_date > e.report_date\n" +
                "               AND COALESCE(g.rig_move_time, 0.0) > 0.0)\n" +
                "        )\n" +
                "        SELECT DISTINCT move_year, move_month, move_count, move_avg_days, year_move_count, year_move_avg_days, overall_move_count, overall_move_avg_days\n" +
                "          FROM TopQuery\n" +
                "         ORDER BY move_year, move_month;";

        //log.info(query);

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("regions", regionlist)
                .addValue("rigTypes", rigtypelist)
                .addValue("start", startEpoch)
                .addValue("end", endEpoch);

        return impalaDbUtil.executeNamedQuery(query, namedParameters);


    }

    public Collection getRMCTByRegionRigTypeTimeRangeAggRigType(Collection<String> regionlist, Collection<Integer> rigtypelist, String starttime, String endtime) throws ParseException {
        // Convert dates to epoch and move to appropriate month boundaries
        final long startEpoch = DateTimeUtils.dateToFirstDayOfMonth(starttime);
        final long endEpoch = DateTimeUtils.dateToLastDayOfMonth(endtime);
        boolean regions = regionlist.stream().anyMatch(f -> "All Regions".equalsIgnoreCase(f));
        boolean rigtype = rigtypelist.stream().anyMatch(f -> f == -99);

        String query = "WITH NotOnes AS (\n" +
                "  SELECT a.rig_name\n" +
                "      , a.well_num\n" +
                "      , a.report_date\n" +
                "    FROM pten_cmr.vw_cmr_data_usalt7_release a inner join user_Security.user_mapping_Details u on a.well_num=u.well_num and u.userid="+getUser_id()+" \n" +
                "   WHERE EXISTS (\n" +
                "      SELECT NULL\n" +
                "        FROM pten_cmr.cmr_time_code_data_usalt7 b\n" +
                "       WHERE a.cmr_data_id = b.cmr_data_id\n" +
                "         AND COALESCE(b.code, '1') <> '1')\n" +
                "  ), ActivationMoves AS (\n" +
                "  SELECT c.rig_name\n" +
                "      , c.well_num\n" +
                "    FROM pten_cmr.vw_cmr_data_usalt7_release c inner join user_Security.user_mapping_Details u on c.well_num=u.well_num and u.userid="+getUser_id()+" \n" +
                "   WHERE NOT EXISTS (\n" +
                "      SELECT NULL\n" +
                "        FROM NotOnes d\n" +
                "       WHERE c.rig_name = d.rig_name\n" +
                "         AND d.report_date BETWEEN c.report_date - 2592001 AND c.report_date - 1)\n" +
                "  ), TopQuery AS (\n" +
                "  SELECT rmt.rig_master_type_name AS rig_type\n" +
                "      , COUNT(*) OVER(PARTITION BY rt.display_text) AS move_count\n" +
                "      , AVG((e.rig_move_time) / 24.0) OVER(PARTITION BY rt.display_text) AS move_avg_days\n" +
                "      , COUNT(*) OVER () AS overall_move_count\n" +
                "      , AVG((e.rig_move_time) / 24.0) OVER() AS overall_move_avg_days\n" +
                "      , rt.display_color AS rig_color\n" +
                "    FROM pten_cmr.vw_cmr_data_usalt7_release e inner join user_Security.user_mapping_Details u on e.well_num=u.well_num and u.userid="+getUser_id()+" \n" +
                "      INNER JOIN pten_cmr.regionmap rm ON UPPER(e.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "      INNER JOIN pten_cmr.rig r ON r.display_text = e.rig_name\n" +
                "      INNER JOIN pten_cmr.rigtype rt ON r.rig_type_id = rt.rig_type_id\n" +
                "      INNER JOIN pten_cmr.rigmastertype rmt ON rmt.rig_master_type_id = rt.rig_master_type_id\n" +
                "   WHERE COALESCE(e.rig_move_time, 0.0) > 0.0\n" +
                "     AND COALESCE(e.trucks_move_distance, 0.0) < 100.0\n" +
                "     AND e.previous_well_release_time BETWEEN :start AND :end\n" +
                "     "+ (!regions? "AND UPPER(rm.region_name) IN (:regions)\n": "") +
                "     "+ (!rigtype? "AND rt.rig_master_type_id IN (:rigTypes)\n": "") +
                "     AND NOT EXISTS (\n" +
                "      SELECT NULL\n" +
                "        FROM ActivationMoves f\n" +
                "       WHERE e.well_num = f.well_num)\n" +
                "     AND NOT EXISTS (\n" +
                "      SELECT NULL\n" +
                "        FROM pten_cmr.vw_cmr_data_usalt7_release g inner join user_Security.user_mapping_Details u on g.well_num=u.well_num and u.userid="+getUser_id()+" \n" +
                "       WHERE e.well_num = g.well_num\n" +
                "         AND g.report_date > e.report_date\n" +
                "         AND COALESCE(g.rig_move_time, 0.0) > 0.0)\n" +
                "  )\n" +
                "  SELECT DISTINCT rig_type, move_count, move_avg_days, overall_move_count, overall_move_avg_days, rig_color\n" +
                "    FROM TopQuery\n" +
                "  ORDER BY move_avg_days DESC";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("regions", regionlist)
                .addValue("rigTypes", rigtypelist)
                .addValue("start", startEpoch)
                .addValue("end", endEpoch);

        return impalaDbUtil.executeNamedQuery(query, namedParameters);
    }

    public Collection getRMCTReactivatedRegionRigTypeTimeRange(Collection<String> regionlist, Collection<Integer> rigtypelist, String starttime, String endtime) throws ParseException {
        // Convert dates to epoch and move to appropriate month boundaries
        final long startEpoch = DateTimeUtils.dateToFirstDayOfMonth(starttime);
        final long endEpoch = DateTimeUtils.dateToLastDayOfMonth(endtime);

        boolean regions = regionlist.stream().anyMatch(f -> "All Regions".equalsIgnoreCase(f));
        boolean rigtype = rigtypelist.stream().anyMatch(f -> f == -99);

        String query = "WITH NotOnes AS (\n" +
                "  SELECT a.rig_name\n" +
                "      , a.well_num\n" +
                "      , a.report_date\n" +
                "    FROM pten_cmr.vw_cmr_data_usalt7_release a inner join user_Security.user_mapping_Details u on a.well_num=u.well_num and u.userid="+getUser_id()+" \n" +
                "  WHERE EXISTS (\n" +
                "      SELECT NULL\n" +
                "        FROM pten_cmr.cmr_time_code_data_usalt7 b\n" +
                "       WHERE a.cmr_data_id = b.cmr_data_id\n" +
                "         AND COALESCE(b.code, '1') <> '1')\n" +
                "  ), ActivationMoves AS (\n" +
                "  SELECT c.rig_name\n" +
                "      , c.well_num\n" +
                "    FROM pten_cmr.vw_cmr_data_usalt7_release c inner join user_Security.user_mapping_Details u on c.well_num=u.well_num and u.userid="+getUser_id()+" \n" +
                "  WHERE NOT EXISTS (\n" +
                "      SELECT NULL\n" +
                "        FROM NotOnes d\n" +
                "       WHERE c.rig_name = d.rig_name\n" +
                "         AND d.report_date BETWEEN c.report_date - 2592001 AND c.report_date - 1)\n" +
                "  )\n" +
                "  SELECT DISTINCT e.rig_name\n" +
                "      , (e.rig_move_time) / 24.0 AS rig_move_days\n" +
                "      , e.daywork_start_time AS rig_move_date\n" +
                "      , e.trucks_move_distance AS rig_move_miles\n" +
                "  FROM pten_cmr.vw_cmr_data_usalt7_release e inner join user_Security.user_mapping_Details u on e.well_num=u.well_num and u.userid="+getUser_id()+" \n" +
                "      INNER JOIN ActivationMoves f ON e.well_num = f.well_num\n" +
                "      INNER JOIN pten_cmr.regionmap rm ON UPPER(rm.contractor_business_unit) = UPPER(e.contractor_business_unit)\n" +
                "      INNER JOIN pten_cmr.rig r ON r.display_text = e.rig_name\n" +
                "      INNER JOIN pten_cmr.rigtype rt ON r.rig_type_id = rt.rig_type_id\n" +
                "      INNER JOIN pten_cmr.rigmastertype rmt ON rmt.rig_master_type_id = rt.rig_master_type_id\n" +
                "  WHERE " +
                "    "+ (!regions? "UPPER(rm.region_name) IN (:regions)\nAND ": "") +
                "    "+ (!rigtype? "rt.rig_master_type_id IN (:rigTypes)\nAND ": "") +
                "    e.previous_well_release_time BETWEEN :start AND :end\n" +
                "    AND COALESCE(e.rig_move_time, 0.0) > 0.0\n" +
                "    AND NOT EXISTS (\n" +
                "      SELECT NULL\n" +
                "      FROM pten_cmr.vw_cmr_data_usalt7_release g inner join user_Security.user_mapping_Details u on g.well_num=u.well_num and u.userid="+getUser_id()+" \n" +
                "      WHERE e.well_num = g.well_num\n" +
                "        AND COALESCE(g.rig_move_time, 0.0) > 0.0\n" +
                "        AND g.report_date > e.report_date)\n" +
                "  ORDER BY e.daywork_start_time DESC, ((e.rig_move_time) / 24.0) ASC  LIMIT 10";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("regions", regionlist)
                .addValue("rigTypes", rigtypelist)
                .addValue("start", startEpoch)
                .addValue("end", endEpoch);

        return impalaDbUtil.executeNamedQuery(query, namedParameters);
    }

    public Collection getRMCTTopNRigsRegionRigTypeTimeRange(Collection<String> regionlist, Collection<Integer> rigtypelist, String starttime, String endtime) throws ParseException {
        // Convert dates to epoch and move to appropriate month boundaries
        final long startEpoch = DateTimeUtils.dateToFirstDayOfMonth(starttime);
        final long endEpoch = DateTimeUtils.dateToLastDayOfMonth(endtime);

        boolean regions = regionlist.stream().anyMatch(f -> "All Regions".equalsIgnoreCase(f));
        boolean rigtype = rigtypelist.stream().anyMatch(f -> f == -99);

        String query = "WITH NotOnes AS (\n" +
                "        SELECT a.rig_name\n" +
                "            , a.well_num\n" +
                "            , a.report_date\n" +
                "          FROM pten_cmr.vw_cmr_data_usalt7_release a inner join user_security.user_mapping_Details u on a.well_num=u.well_num and u.userid="+getUser_id()+" \n" +
                "         WHERE EXISTS (\n" +
                "            SELECT NULL\n" +
                "              FROM pten_cmr.cmr_time_code_data_usalt7 b\n" +
                "             WHERE a.cmr_data_id = b.cmr_data_id\n" +
                "               AND COALESCE(b.code, '1') <> '1')\n" +
                "        ), ActivationMoves AS (\n" +
                "        SELECT c.rig_name\n" +
                "            , c.well_num\n" +
                "          FROM pten_cmr.vw_cmr_data_usalt7_release c inner join user_security.user_mapping_Details u on c.well_num=u.well_num and u.userid="+getUser_id()+" \n" +
                "         WHERE NOT EXISTS (\n" +
                "            SELECT NULL\n" +
                "              FROM NotOnes d\n" +
                "             WHERE c.rig_name = d.rig_name\n" +
                "               AND d.report_date BETWEEN c.report_date - 2592001 AND c.report_date - 1)\n" +
                "        )\n" +
                "        SELECT e.rig_name\n" +
                "            , COUNT(*) AS move_count\n" +
                "            , AVG((e.rig_move_time) / 24.0) AS move_avg_days\n" +
                "            , rt.display_text AS rig_type" +
                "            , rt.display_color AS rig_color" +
                "          FROM pten_cmr.vw_cmr_data_usalt7_release e inner join user_Security.user_mapping_details u on e.well_num=u.well_num \n" +
                "            INNER JOIN pten_cmr.regionmap rm ON UPPER(e.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "            INNER JOIN pten_cmr.rig r ON r.display_text = e.rig_name\n" +
                "            INNER JOIN pten_cmr.rigtype rt ON r.rig_type_id = rt.rig_type_id\n" +
                "            INNER JOIN pten_cmr.rigmastertype rmt ON rmt.rig_master_type_id = rt.rig_master_type_id\n" +
                "         WHERE u.userid="+getUser_id()+" and COALESCE(e.rig_move_time, 0.0) > 0.0\n" +
                "           AND COALESCE(e.trucks_move_distance, 0.0) < 100.0\n" +
                "           AND e.previous_well_release_time BETWEEN :start AND :end\n" +
                "           "+ (!regions? "AND UPPER(rm.region_name) IN (:regions)\n": "") +
                "           "+ (!rigtype? "AND rt.rig_master_type_id IN (:rigTypes)\n": "") +
                "           AND NOT EXISTS (\n" +
                "            SELECT NULL\n" +
                "              FROM ActivationMoves f\n" +
                "             WHERE e.well_num = f.well_num)\n" +
                "           AND NOT EXISTS (\n" +
                "            SELECT NULL\n" +
                "              FROM pten_cmr.vw_cmr_data_usalt7_release g inner join user_Security.user_mapping_Details u on g.well_num=u.well_num and u.userid="+getUser_id()+" \n" +
                "             WHERE e.well_num = g.well_num\n" +
                "               AND g.report_date > e.report_date\n" +
                "               AND COALESCE(g.rig_move_time, 0.0) > 0.0)\n" +
                "         GROUP BY e.rig_name, rt.display_text, rt.display_color\n" +
                "         ORDER BY AVG((e.rig_move_time)) limit 10";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("regions", regionlist)
                .addValue("rigTypes", rigtypelist)
                .addValue("start", startEpoch)
                .addValue("end", endEpoch);

        return impalaDbUtil.executeNamedQuery(query, namedParameters);

    }

    public Collection getTopRMCTWells(String region) {

        LocalDate now = LocalDate.now();

String query = "WITH lastNMonthsWellsInArea as (\n" +
                "  SELECT distinct a.well_num\n" +
                "  FROM pten_cmr.vw_cmr_data_usalt7_release a\n" +
                "  INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "  where UPPER(rm.region_name) in (:region)\n" +
                "    and not exists (select null from pten_cmr.vw_cmr_data_usalt7_release z where a.well_num = z.well_num and a.report_date < z.report_date)\n" +
                "    and a.report_date >= :startDate\n" +
                "    and release_time is not null\n" +
                ")\n" +
                "SELECT a.well_num\n" +
                "  ,a.rig_name\n" +
                "  ,rm.region_name\n" +
                "  ,rt.display_text as rig_type\n" +
                "  ,trunc(to_timestamp(a.release_time), 'DD') AS release_time\n" +
                "  ,cast((a.rig_move_time / 24.0) as DECIMAL(10,2))  AS rig_move_cycle_time\n" +
                "  ,rtcc.color_code\n" +
		"  ,concat('<div>',a.county,', ',a.state_province,'<br>',coalesce(cast(cast(a.trucks_move_distance as DECIMAL(10,2)) as string),' '),' miles</div>') as county_state_miles\n" +
                "FROM user_Security.user_mapping_details u \n" +
                "  INNER JOIN pten_cmr.vw_cmr_data_usalt7_release a on a.well_num = u.well_num AND u.userid = :user\n" +
                "  INNER JOIN lastNMonthsWellsInArea w ON a.well_num = w.well_num\n" +
                "  INNER JOIN pten_cmr.rig r  ON r.display_text = a.rig_name\n" +
                "  INNER JOIN pten_cmr.rigtype rt  ON r.rig_type_id = rt.rig_type_id\n" +
                "  INNER JOIN pten_cmr.rigmastertype rmt  ON rmt.rig_master_type_id = rt.rig_master_type_id\n" +
                "  INNER JOIN pten_cmr.regionmap rm  ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "  INNER JOIN pten_cmr.rig_type_color_code rtcc ON UPPER(rt.display_text) = UPPER(rtcc.rig_type)\n" +
                "WHERE UPPER(rm.region_name) in (:region)\n" +
                "  AND a.trucks_move_distance > 0 \n" +
                "  and a.previous_well_release_time >= :startDate\n" +
                "  and a.rig_move_time is not null\n" +
                "GROUP BY a.well_num\n" +
                "        ,a.rig_name\n" +
                "        ,rt.display_text\n" +
                "        ,rm.region_name\n" +
                "        , release_time\n" +
                "        , a.county\n" +
                "        , a.state_province\n" +
                "        , a.trucks_move_distance\n" +
                "        , a.rig_move_time\n" +
                "        , rtcc.color_code\n" +
                "ORDER BY  a.rig_move_time asc\n" +
                "LIMIT :limit";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("region", region.toUpperCase())
                .addValue("limit", TOP_10)
                .addValue("startDate", DateTimeUtils.dateToFirstSecondOfDay(now.minusMonths(6)))
                .addValue("user", getUser_id());

        return impalaDbUtil.executeNamedQuery(query, namedParameters);

    }

    public Collection getTopRMCTWellsLegend(String region) {

        LocalDate now = LocalDate.now();

	String query = "WITH lastNMonthsWellsInArea as (\n" +
                "  SELECT distinct a.well_num\n" +
                "  FROM pten_cmr.vw_cmr_data_usalt7_release a\n" +
                "  INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "  where UPPER(rm.region_name) in (:region)\n" +
                "    and not exists (select null from pten_cmr.vw_cmr_data_usalt7_release z where a.well_num = z.well_num and a.report_date < z.report_date)\n" +
                "    and a.report_date >= :startDate\n" +
                "    and release_time is not null\n" +
                "), FinalList as(" +
                "SELECT a.well_num\n" +
                "  ,a.rig_name\n" +
                "  ,rm.region_name\n" +
                "  ,rt.display_text as rig_type\n" +
                "  ,trunc(to_timestamp(a.release_time), 'DD') AS release_time\n" +
                "  ,a.county\n" +
                "  ,a.state_province\n" +
                "  ,a.trucks_move_distance\n" +
                "  ,a.rig_move_time / 24.0 AS rig_move_cycle_time\n" +
                "  ,rtcc.color_code\n" +
                "FROM user_Security.user_mapping_details u \n" +
                "  INNER JOIN pten_cmr.vw_cmr_data_usalt7_release a on a.well_num = u.well_num AND u.userid = :user\n" +
                "  INNER JOIN lastNMonthsWellsInArea w ON a.well_num = w.well_num\n" +
                "  INNER JOIN pten_cmr.rig r  ON r.display_text = a.rig_name\n" +
                "  INNER JOIN pten_cmr.rigtype rt  ON r.rig_type_id = rt.rig_type_id\n" +
                "  INNER JOIN pten_cmr.rigmastertype rmt  ON rmt.rig_master_type_id = rt.rig_master_type_id\n" +
                "  INNER JOIN pten_cmr.regionmap rm  ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "  INNER JOIN pten_cmr.rig_type_color_code rtcc ON UPPER(rt.display_text) = UPPER(rtcc.rig_type)\n" +
                "WHERE UPPER(rm.region_name) in (:region)\n" +
                "  AND a.trucks_move_distance > 0 \n" +
                "  and a.previous_well_release_time >= :startDate\n" +
                "  and a.rig_move_time is not null\n" +
                "GROUP BY a.well_num\n" +
                "        ,a.rig_name\n" +
                "        ,rt.display_text\n" +
                "        ,rm.region_name\n" +
                "        , release_time\n" +
                "        , a.county\n" +
                "        , a.state_province\n" +
                "        , a.trucks_move_distance\n" +
                "        , a.rig_move_time\n" +
                "        , rtcc.color_code\n" +
                "ORDER BY  a.rig_move_time asc\n" +
                "LIMIT :limit )\n" +
                "SELECT rig_type\n" +
                "      ,color_code\n" +
                "FROM FinalList\n" +
                "GROUP BY rig_type\n" +
                "        ,color_code\n";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("region", region.toUpperCase())
                .addValue("limit", TOP_10)
                .addValue("startDate", DateTimeUtils.dateToFirstSecondOfDay(now.minusMonths(6)))
                .addValue("user", getUser_id());

        return impalaDbUtil.executeNamedQuery(query, namedParameters);

    }

    public Collection getTopTotalCTActiveWells(String region) {

        LocalDate now = LocalDate.now();

        String query = "WITH lastNMonthsWellsInArea as (\n" +
                "  SELECT distinct a.well_num\n" +
                "  , ((a.release_time - COALESCE(a.previous_well_release_time, a.daywork_start_time)) / 86400.0) - a.paused_days AS total_cycle_time_days\n" +
                "  FROM pten_cmr.vw_cmr_data_usalt7_release a\n" +
                "  INNER JOIN user_Security.user_mapping_details u on  a.well_num = u.well_num AND u.userid = :user\n" +
                "  INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "  where UPPER(rm.region_name) in (:region)\n" +
                "    and not exists (select null from pten_cmr.vw_cmr_data_usalt7_release z where a.well_num = z.well_num and a.report_date < z.report_date)\n" +
                "    and a.release_time >= :startDate\n" +
                "\tand a.well_status = 'Active'\n" +
                ") , TopTenWells as (\n" +
                "SELECT a.well_num\n" +
                "FROM lastNMonthsWellsInArea a \n" +
                "where a.total_cycle_time_days >= 0\n" +
                "ORDER BY  a.total_cycle_time_days asc\n" +
                "LIMIT :limit)\n" +
                "SELECT ttw.well_num\n" +
                "     , a.well_name\n" +
                "     , ROW_NUMBER() OVER(PARTITION BY a.well_num ORDER BY a.report_date) AS day_num\n" +
                "     , COALESCE(LAG(a.current_depth, 1) OVER(PARTITION BY a.well_num ORDER BY a.report_date), 0.0) AS day_depth\n" +
                "FROM TopTenWells ttw\n"+
                "INNER JOIN pten_cmr.cmr_data_usalt7 a on ttw.well_num=a.well_num\n";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("region", region.toUpperCase())
                .addValue("limit", TOP_10)
                .addValue("startDate", DateTimeUtils.dateToFirstSecondOfDay(now.minusMonths(6)))
                .addValue("user", getUser_id());

        return impalaDbUtil.executeNamedQuery(query, namedParameters);
    }

    public Collection getRigAverageMoveDays(Collection regions, Collection rigTypes, Date startDate, Date endDate, String rmctType) {

        long startTime = DateTimeUtils.dateToEpoch(startDate);
        long endTime = DateTimeUtils.dateToEpoch(endDate);

       /* StringBuilder q = new StringBuilder("WITH moves as (\n" +
                "SELECT a.trucks_company\n" +
                "  ,a.rig_move_time / 24.0 as rig_move_cycle_time  \n" +
                "  ,a.daywork_days\n" +
                "  ,a.trucks_release_time\n" +
                "  ,a.trucks_show_up_time\n" +
                "  ,a.previous_well_release_time\n" +
                "  ,count(*) over (Partition by a.trucks_company) as moves\n" +
                "FROM user_Security.user_mapping_details u\n" +
                "  INNER JOIN pten_cmr.cmr_data_usalt7 a on a.well_num = u.well_num AND u.userid = :user\n" +
                "  INNER JOIN pten_cmr.cmr_to_trucking_company_mapping tcm on a.trucks_company = tcm.trucks_company\n" +
                "  INNER JOIN pten_cmr.Rig r ON r.display_text = a.rig_name\n" +
                "  INNER JOIN pten_cmr.RigType rt ON r.rig_type_id = rt.rig_type_id\n" +
                "  INNER JOIN pten_cmr.RegionMap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "where a.rig_move_time > 0\n" +
                "  and a.release_time between :startDate and :endDate\n");

        if (null != regions && !regions.isEmpty()) {
            q.append("  and rm.region_name in (:region)\n");
        }

        if (null != rigTypes && !rigTypes.isEmpty()) {
            q.append("  and rt.display_text in(:rigType)\n");
        }
        q.append(")select a.trucks_company\n");

        if (null != rmctType && rmctType.equalsIgnoreCase("trucks")) {
            q.append("  ,avg((a.trucks_release_time - a.trucks_show_up_time) / 86400) over (Partition by a.trucks_company) as  avg_rmct\n");
        } else {
            q.append("  ,avg(a.rig_move_cycle_time) over (Partition by a.trucks_company) as avg_rmct\n");
        }
        q.append(",a.moves\n" +
                "from moves a\n" +
                "group by a.trucks_company\n" +
                "  ,a.rig_move_cycle_time\n" +
                "  ,a.moves\n" +
                "  ,a.trucks_release_time\n" +
                "  ,a.trucks_show_up_time\n" +
                "  ,a.daywork_days\n" +
                "  ,a.previous_well_release_time\n" +
                "order by a.rig_move_cycle_time\n" +
                "  ,a.trucks_company");
*/
	StringBuilder q = new StringBuilder("SELECT a.trucks_company\n" +
                "   ,count(*) as moves\n" +
                "   ,tccc.color_code\n");
        if (null != rmctType && rmctType.equalsIgnoreCase("trucks")) {
            q.append("  ,cast(avg((a.trucks_release_time - a.trucks_show_up_time) / 86400) as DECIMAL(10,2)) as  avg_rmct\n");
        } else {
            q.append("  ,cast(avg(a.rig_move_time/24) as DECIMAL(10,2)) as avg_rmct\n");
        }
        q.append("FROM user_Security.user_mapping_details u\n" +
                "INNER JOIN pten_cmr.cmr_data_usalt7 a on a.well_num = u.well_num and u.userid=:user\n" +
                "INNER JOIN pten_cmr.cmr_to_trucking_company_mapping tcm on a.trucks_company = tcm.trucks_company\n" +
                "INNER JOIN pten_cmr.Rig r ON r.display_text = a.rig_name\n" +
                "INNER JOIN pten_cmr.RigType rt ON r.rig_type_id = rt.rig_type_id\n" +
                "INNER JOIN pten_cmr.RegionMap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "INNER JOIN pten_cmr.trucking_company_color_code tccc on UPPER(a.trucks_company) = UPPER(tccc.trucking_company)\n" +
                "where a.rig_move_time > 0\n");
        if (null != regions && !regions.isEmpty()) {
            q.append("  and rm.region_name in (:region)\n");
        }

        if (null != rigTypes && !rigTypes.isEmpty()) {
            q.append("  and rt.display_text in (:rigType)\n");
        }
	q.append("and a.release_time between :startDate and :endDate\n" +
                "GROUP BY a.trucks_company\n" +
                "         ,tccc.color_code\n" +
                "order by avg_rmct");

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("region", regions)
                .addValue("rigType", rigTypes)
                .addValue("startDate", startTime)
                .addValue("endDate", endTime)
                .addValue("user", getUser_id());

        return impalaDbUtil.executeNamedQuery(q.toString(), namedParameters);
    }

    public Collection getRigTotalMoves(Collection regions, Collection rigTypes, Date startDate, Date endDate) {

        long startTime = DateTimeUtils.dateToEpoch(startDate);
        long endTime = DateTimeUtils.dateToEpoch(endDate);

        StringBuilder q = new StringBuilder("WITH moves as (\n" +
                "SELECT a.trucks_company\n" +
                "  ,count(*) over (Partition by a.trucks_company) as moves\n" +
		"  ,tccc.color_code\n" +
                "FROM user_Security.user_mapping_details u\n" +
                "  INNER JOIN pten_cmr.cmr_data_usalt7 a on a.well_num = u.well_num AND u.userid = :user\n" +
                "  INNER JOIN pten_cmr.cmr_to_trucking_company_mapping tcm on a.trucks_company = tcm.trucks_company\n" +
                "  INNER JOIN pten_cmr.Rig r ON r.display_text = a.rig_name\n" +
                "  INNER JOIN pten_cmr.RigType rt ON r.rig_type_id = rt.rig_type_id\n" +
                "  INNER JOIN pten_cmr.RegionMap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
		"  INNER JOIN pten_cmr.trucking_company_color_code tccc on UPPER(a.trucks_company) = UPPER(tccc.trucking_company)\n" +
                "where a.rig_move_time > 0\n" +
                "  and a.daywork_start_time between :startDate and :endDate\n");

        if (null != regions && !regions.isEmpty()) {
            q.append("  and rm.region_name in (:region)\n");
        }
        if (null != rigTypes && !rigTypes.isEmpty()) {
            q.append("  and rt.display_text in(:rigType)\n");
        }
        q.append(")select a.trucks_company\n" +
                "  ,a.moves\n" +
		"  ,a.color_code\n" +
                "from moves a\n" +
                "group by a.trucks_company\n" +
                ",a.moves\n" +
		",a.color_code\n" +
                "order by a.trucks_company");

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("region", regions)
                .addValue("rigType", rigTypes)
                .addValue("limit", TOP_10)
                .addValue("startDate", startTime)
                .addValue("endDate", endTime)
                .addValue("user", getUser_id());

        return impalaDbUtil.executeNamedQuery(q.toString(), namedParameters);
    }

    public Collection getRigMoves(Collection regions, Collection rigTypes, Date startDate, Date endDate, String rmctType) {

        long startTime = DateTimeUtils.dateToEpoch(startDate);
        long endTime = DateTimeUtils.dateToEpoch(endDate);

        StringBuilder q = new StringBuilder("WITH moves as (\n" +
                "SELECT a.rig_name\n" +
                "  ,a.trucks_company\n" +
                "  ,a.rig_move_time / 24.0 as rig_move_cycle_time\n" +
		"  ,CONCAT('<div>',cast(cast(a.trucks_move_distance as DECIMAL(10,1)) as string),' mi.</div>') as trucks_move_distance\n" +
                "  ,rm.region_name\n" +
                "  ,a.trucks_release_time\n" +
                "  ,a.trucks_show_up_time\n" +
		"  ,tccc.color_code\n" +
                "  FROM user_Security.user_mapping_details u\n" +
                "  INNER JOIN pten_cmr.cmr_data_usalt7 a on a.well_num = u.well_num AND u.userid = :user\n" +
                "  INNER JOIN pten_cmr.cmr_to_trucking_company_mapping tcm on a.trucks_company = tcm.trucks_company\n" +
                "  INNER JOIN pten_cmr.Rig r ON r.display_text = a.rig_name\n" +
                "  INNER JOIN pten_cmr.RigType rt ON r.rig_type_id = rt.rig_type_id\n" +
                "  INNER JOIN pten_cmr.RegionMap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
		"  INNER JOIN pten_cmr.trucking_company_color_code tccc on UPPER(a.trucks_company) = UPPER(tccc.trucking_company)\n" +
                "  --INNER JOIN pten_cmr.trucking_companies tc on tc.trucking_company_id = tcm.trucking_company_id\n" +
                "where a.rig_move_time > 0\n" +
                "  and a.daywork_start_time between :startDate and :endDate\n");
        if (null != regions && !regions.isEmpty()) {
            q.append("  and rm.region_name in (:region)\n");
        }
        if (null != rigTypes && !rigTypes.isEmpty()) {
            q.append("  and rt.display_text in(:rigType)\n");
        }
        q.append(")select a.rig_name\n" +
                "  ,a.trucks_company\n" +
		"  ,a.color_code\n" +
                "  ,a.trucks_move_distance\n" +
		"  ,cast(a.rig_move_cycle_time as DECIMAL(10,2)) as rig_move_cycle_time\n");

        if (null != rmctType && rmctType.equalsIgnoreCase("trucks")) {
            q.append("  ,cast(avg((a.trucks_release_time - a.trucks_show_up_time) / 86400) over (Partition by a.rig_name) as DECIMAL(10,2)) as  avg_rmct\n");
            q.append("  ,cast(avg((a.trucks_release_time - a.trucks_show_up_time) / 86400) over(Partition by a.trucks_company) as DECIMAL(10,2)) as avg_company_rmct\n");
        } else {
            q.append("  ,cast(avg(a.rig_move_cycle_time) over (Partition by a.rig_name) as DECIMAL(10,2)) as avg_rmct\n");
            q.append("  ,cast(avg(a.rig_move_cycle_time) over (Partition by a.trucks_company) as DECIMAL(10,2)) as avg_company_rmct\n");
        }

        q.append("from moves a\n" +
                "group by a.rig_name\n" +
                "  ,a.trucks_company\n" +
                "  ,a.rig_move_cycle_time\n" +
                "  ,a.trucks_move_distance\n" +
                "  ,a.rig_move_cycle_time\n" +
                "  ,a.trucks_release_time\n" +
                "  ,a.trucks_show_up_time\n" +
		"  ,a.color_code\n" +
                "order by a.rig_name, a.trucks_company");

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("region", regions)
                .addValue("rigType", rigTypes)
                .addValue("limit", TOP_10)
                .addValue("startDate", startTime)
                .addValue("endDate", endTime)
                .addValue("user", getUser_id());

        return impalaDbUtil.executeNamedQuery(q.toString(), namedParameters);
    }
}
