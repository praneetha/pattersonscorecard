package com.pten.ui.repo;

import com.pten.Utilities.DbUtils.ImpalaDbUtilLib;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Date;

@Component
public class DrillingRepo {

    private static final Logger log = LoggerFactory.getLogger(DrillingRepo.class);

    @Autowired
    ImpalaDbUtilLib impalaDbUtilLib;

    public Collection getConnectionTime(Collection<Long> wells, Date startDate, Date endDate, String entityType, String section, String order, int returnCount, int userId) {

        StringBuilder q = new StringBuilder("SELECT\n");

        q.append(getConnectionTimeSelect(entityType));

        q.append("\n   , AVG(sec_w2s) AS sec_w2s\n" +
                "   , AVG(sec_s2s) AS sec_s2s\n" +
                "   , AVG(sec_s2w) AS sec_s2w\n" +
                "FROM ptendrilling.vw_wellstands_drilling ws \n");

        if (entityType.equalsIgnoreCase("RIG") || entityType.equalsIgnoreCase("WELL")) {
            q.append("INNER JOIN ptendrilling.ptendrilling_welldata wd ON ws.well_num = wd.well_num\n");
        }

        q.append("WHERE ws.well_num IN (:wells)\n" +
                "   AND ws.stand_start_timepoint BETWEEN :start_date AND :end_date \n");

        if (!section.equalsIgnoreCase("ALL")) {
            q.append("AND CASE ws.hole_progress_section  when 'SURFACE' then 'VERTICAL' ELSE ws.hole_progress_section END = :section");
        }

        q.append("\nGROUP BY ");

        q.append(getConnectionTimeGroupBy(entityType));

        q.append("\nORDER BY ");

        q.append("entity_name\n");

        if (!entityType.equalsIgnoreCase("TIME")) {
            q.append(", AVG(ws.sec_w2s + ws.sec_s2s + ws.sec_s2w)\n");
        }

        if (order.equalsIgnoreCase("BOTTOM")) {
            q.append(", DESC\n");
        }

        if (!entityType.equalsIgnoreCase("TIME")) {
            q.append(" LIMIT :return_count");
        }

        q.append(";");

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells)
                .addValue("start_date", startDate)
                .addValue("end_date", endDate)
                .addValue("section", section)
                .addValue("return_count", returnCount)
                .addValue("user", userId);

        return impalaDbUtilLib.executeNamedQuery(q.toString(), namedParameters);
    }

    private String getConnectionTimeSelect(String entityName) {
        switch (entityName.toUpperCase()) {
            case "TIME":
                return "CONCAT( CAST(YEAR(ws.stand_start_timepoint) AS STRING), '/' , CAST( MONTH(ws.stand_start_timepoint) AS STRING)) as entity_name";
            case "RIG":
                return "STRRIGHT(wd.rig_name, 3) AS entity_name";
            case "WELL":
                return "UPPER(wd.well_name) AS entity_name";
            default:
                return "UPPER(ws.crew) AS entity_name";
        }
    }

    private String getConnectionTimeGroupBy(String entityName) {
        switch (entityName.toUpperCase()) {
            case "TIME":
                return "YEAR(ws.stand_start_timepoint)\n" +
                        ",MONTH(ws.stand_start_timepoint)";
            case "RIG":
                return "wd.rig_name";
            case "WELL":
                return "wd.well_name";
            default:
                return "ws.crew";
        }
    }

    private String getS2WConnectionTimeGroupBy(String entityName) {
        switch (entityName.toUpperCase()) {
            case "TIME":
                return "YEAR(ws.stand_start_timepoint)\n" +
                        ",MONTH(ws.stand_start_timepoint)";
            case "RIG":
                return "wd.rig_name";
            case "WELL":
                return "wd.well_name";
            default:
                return "ws.crew";
        }
    }

    public Collection getTimesTrend(Collection<Long> wells, Date startDate, Date endDate, String section, String holestatus, String order, int userId) {

        StringBuilder q = new StringBuilder("SELECT\n" +
                "YEAR(ws.stand_start_timepoint) as observation_year\n" +
                ",MONTH(ws.stand_start_timepoint) AS observation_month\n" +
                ",AVG(sec_w2s) AS sec_w2s\n" +
                ", AVG(sec_s2s) AS sec_s2s\n" +
                ", AVG(sec_s2w) AS sec_s2w\n" +
                "FROM ptendrilling.vw_wellstands_drilling ws\n" +
                "WHERE ws.well_num IN (:wells)\n" +
                "AND ws.stand_start_timepoint BETWEEN :start_date AND :end_date\n");

        if (!section.equalsIgnoreCase("ALL")) {
            q.append("AND CASE ws.hole_progress_section  when 'SURFACE' then 'VERTICAL' ELSE ws.hole_progress_section END = :section");
        }

        q.append("\nGROUP BY observation_year,\n" +
                "observation_month\n" +
                "ORDER BY observation_year\n" +
                ",observation_month\n");
        if (order.equalsIgnoreCase("BOTTOM")) {
            q.append("DESC\n");
        }

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells)
                .addValue("start_date", startDate)
                .addValue("end_date", endDate)
                .addValue("direction", section)
                .addValue("user", userId)
                .addValue("holestatus", holestatus);

        return impalaDbUtilLib.executeNamedQuery(q.toString(), namedParameters);
    }

    public Collection getS2WConnectionTime(Collection<Long> wells, Date startDate, Date endDate, String entityType, String section, String order, int returnCount, int userId) {

        StringBuilder q = new StringBuilder("SELECT\n");

        q.append(getConnectionTimeSelect(entityType));

        q.append("\n    , AVG(CASE WHEN ws.s2w_pump_recycles > 0 THEN ws.sec_s2w ELSE NULL END) AS s2w_recycles\n" +
                "        , AVG(CASE WHEN ws.s2w_pump_recycles <= 0 THEN ws.sec_s2w ELSE NULL END) AS s2w_no_recycles\n" +
                "        , SUM(CASE WHEN ws.s2w_pump_recycles > 0 THEN 1 ELSE 0 END) / COUNT(*) * 100.0 AS s2w_recylces_pct\n" +
                "  FROM ptendrilling.vw_wellstands_drilling ws\n");

        if (entityType.equalsIgnoreCase("RIG") || entityType.equalsIgnoreCase("WELL")) {
            q.append("INNER JOIN ptendrilling.ptendrilling_welldata wd ON ws.well_num = wd.well_num\n");
        }

        q.append("WHERE ws.well_num IN (:wells)\n" +
                "   AND ws.stand_start_timepoint BETWEEN :start_date AND :end_date\n");

        if (!section.equalsIgnoreCase("ALL")) {
            q.append("AND CASE ws.hole_progress_section  when 'SURFACE' then 'VERTICAL' ELSE ws.hole_progress_section END = :section");
        }

        q.append("\nGROUP BY ");

        q.append(getS2WConnectionTimeGroupBy(entityType));

        q.append("\nORDER BY ");

        if (entityType.equalsIgnoreCase("TIME")) {
            q.append("YEAR(ws.stand_start_timepoint), MONTH(ws.stand_start_timepoint)");
        } else {
            q.append("SUM(CASE WHEN ws.s2w_pump_recycles > 0 THEN 1 ELSE 0 END)");
        }

        if (order.equalsIgnoreCase("BOTTOM")) {
            q.append("\nDESC");
        }

        if (!entityType.equalsIgnoreCase("TIME")) {
            q.append("\nLIMIT :return_count");
        }

        q.append(";");

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells)
                .addValue("start_date", startDate)
                .addValue("end_date", endDate)
                .addValue("section", section.toUpperCase())
                .addValue("order", order)
                .addValue("return_count", returnCount)
                .addValue("user", userId);

        return impalaDbUtilLib.executeNamedQuery(q.toString(), namedParameters);
    }

    public Collection getConnectionDistribution(Collection<Long> wells, Date startDate, Date endDate, String distribution_type, String section, int userId) {

        double bucketSize = 0;
        double rangeEnd = 0;
        double rangeStart = 0;
        Double target = null;
        String type = "";

        switch (distribution_type) {
            case "S2W":
                bucketSize = 15.0;
                rangeEnd = 1800.0;
                rangeStart = 10.0;
                type = "ws.sec_s2w";
                break;
            case "W2S":
                bucketSize = 10.0;
                rangeEnd = 1200.0;
                rangeStart = 0.0;
                type = "ws.sec_w2s";
                break;

            case "S2S":
                bucketSize = 5.0;
                rangeEnd = 660.0;
                rangeStart = 45.0;
                target = 210.0;
                type = "ws.sec_s2s";
                break;
        }

        StringBuilder q = new StringBuilder("WITH TopQuery AS ( \n" +
                "SELECT :bucketSize AS bucket_size\n" +
                "   , :rangeEnd AS range_end\n" +
                "   , :rangeStart AS range_start\n" +
                "   , AVG( ").append(type).append(") OVER() AS mean \n" +
                "  , CEIL( ").append(type).append("/ :bucketSize ) AS bucket \n" +
                "FROM ptendrilling.vw_wellstands_drilling ws \n" +
                "WHERE ws.well_num IN (:wells) \n" +
                "   AND ws.stand_start_timepoint BETWEEN :start_date AND :end_date\n");

        if (!section.equalsIgnoreCase("ALL")) {
            q.append("AND CASE ws.hole_progress_section  when 'SURFACE' then 'VERTICAL' ELSE ws.hole_progress_section END = :section");
        }
                q.append(")\n" +
                "SELECT bucket \n" +
                "   , COUNT(*) AS value \n" +
                "   , bucket * :bucketSize AS category \n" +
                "   , max(mean) as mean \n" +
                "   , :target as target\n" +
                "FROM TopQuery \n" +
                "GROUP by bucket \n" +
                "ORDER by bucket;");

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells)
                .addValue("rangeEnd", rangeEnd)
                .addValue("rangeStart", rangeStart)
                .addValue("start_date", startDate)
                .addValue("end_date", endDate)
                .addValue("bucketSize", bucketSize)
                .addValue("distribution_type", distribution_type)
                .addValue("section", section.toUpperCase())
                .addValue("type", type)
                .addValue("target", target)

                .addValue("user", userId);

        return impalaDbUtilLib.executeNamedQuery(q.toString(), namedParameters);
    }

    public Collection getS2SConnectionBreakdown(Collection<Long> wells, Date startDate, Date endDate, String entityType, String section, String order, int returnCount, int userId) {

        StringBuilder q = new StringBuilder("SELECT\n");

        q.append(getS2SConnectionBreakdownSelect(entityType));

        q.append(", AVG(ws.sec_drilling_breakout) AS sec_drilling_breakout\n" +
                "        , AVG(ws.sec_drilling_latch) AS sec_drilling_latch\n" +
                "        , AVG(ws.sec_drilling_moving) AS sec_drilling_moving\n" +
                "        , AVG(ws.sec_drilling_stab) AS sec_drilling_stab\n" +
                "        , AVG(ws.sec_drilling_makeup) AS sec_drilling_makeup\n" +
                "        , AVG(ws.sec_drilling_slips) AS sec_drilling_slips\n" +
                "FROM ptendrilling.vw_wellstands_drilling ws\n");

        if (entityType.equalsIgnoreCase("RIG") || entityType.equalsIgnoreCase("WELL")) {
            q.append("INNER JOIN ptendrilling.ptendrilling_welldata wd ON ws.well_num = wd.well_num\n");
        }

        q.append("WHERE ws.well_num IN (:wells)\n" +
                "        AND ws.stand_start_timepoint BETWEEN :start_date AND :end_date\n");

        //q.append("AND CASE UPPER(ws.hole_progress_section) WHEN 'SURFACE' THEN 'VERTICAL' ELSE ws.hole_progress_section END = :section\n");

        if (!section.equalsIgnoreCase("ALL")) {
            q.append("AND CASE ws.hole_progress_section  when 'SURFACE' then 'VERTICAL' ELSE ws.hole_progress_section END = :section");
        }

        q.append("\nGROUP BY " + getConnectionTimeGroupBy(entityType));
        q.append("\nORDER BY\n");

        if (entityType.equalsIgnoreCase("TIME")) {
            q.append("YEAR(ws.stand_start_timepoint), MONTH(ws.stand_start_timepoint)");
        } else {
            q.append("AVG(ws.sec_drilling_breakout + ws.sec_drilling_latch + ws.sec_drilling_moving + ws.sec_drilling_stab + ws.sec_drilling_makeup + ws.sec_drilling_slips)");
        }

        if (order.equalsIgnoreCase("BOTTOM")) {
            q.append("\nDESC");
        }

        if (!entityType.equalsIgnoreCase("TIME")) {
            q.append("\nLIMIT :return_count");
        }

        q.append(";");

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells)
                .addValue("start_date", startDate)
                .addValue("end_date", endDate)
                .addValue("section", section)
                .addValue("return_count", returnCount)
                .addValue("user", userId);

        return impalaDbUtilLib.executeNamedQuery(q.toString(), namedParameters);
    }

    private String getS2SConnectionBreakdownSelect(String direction) {
        switch (direction) {
            case "TIME":
                return "CONCAT( CAST(YEAR(ws.stand_start_timepoint) AS STRING) , '/' , CAST(MONTH(ws.stand_start_timepoint) AS STRING) ) AS entity_name";
            case "RIG":
                return "STRRIGHT(wd.rig_name, 3) AS entity_name";
            case "WELL":
                return "UPPER(wd.well_name) AS entity_name";
            default:
                return "UPPER(ws.crew) AS entity_name";
        }
    }

}
