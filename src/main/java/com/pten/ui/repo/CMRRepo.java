package com.pten.ui.repo;

import com.flutura.ImpalaRestApiController.CerebraAPIDataLayer;
import com.pten.Utilities.DateUtils.DateTimeUtils;
import com.pten.Utilities.DbUtils.ImpalaDbUtilLib;
import com.pten.Utilities.UtilityLibrary;
import com.pten.ui.data.CMRDetails;
import com.pten.ui.data.RigCMRHoursByMonthObj;
import com.pten.ui.data.Well;
import com.pten.ui.util.Mapper;
import com.pten.ui.view.*;
import com.pten.ui.view.cmr.WellDailyDepth;
import com.pten.ui.view.cmr.WellDailyDepthsFlatMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Component
public class CMRRepo {
    private static final Logger log = LoggerFactory.getLogger(CMRRepo.class);

    private static final Object TOP_10 = 10;

    @Autowired
    CerebraAPIDataLayer cerebraAPIDataLayer;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    Mapper mapper;

    @Autowired
    ImpalaDbUtilLib impalaDbUtilLib;

    //TODO: remove
    @Autowired
    UtilityLibrary myUtils;


    public int getUser_id() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().
                getAuthentication().getPrincipal();
        return Integer.parseInt(userDetails.getUsername());
    }

    public Collection getRigCMRPercentByMonth(String rigname, String starttime, String endtime, String subCodeText) {

        Collection model = cerebraAPIDataLayer.RigCMRPercentByMonth(rigname, starttime, endtime, subCodeText);

        return mapper.unmarshall(model, new RigCMRPercentByMonth());

    }


    public Collection getRigCMRHoursByMonth(String rigname, String starttime, String endtime, String subCodeText) {

        Collection model = cerebraAPIDataLayer.RigCMRHoursByMonth(rigname, starttime, endtime, subCodeText);

        return mapper.unmarshall(model, new RigCMRHoursByMonthObj());

    }

    //all subcodetexts
    public Collection getRigCMRPercentByMonth(String rigName, List<Long> wells, String startTime, String endTime) {

        Collection model = cerebraAPIDataLayer.RigCMRPercentByMonth(rigName, wells, startTime, endTime);

        return mapper.unmarshall(model, RigCMRPercentByMonth.class);


    }

    public Collection getCMRDetails(String rigName, String starttime, String endtime) {

        return Arrays.asList((CMRDetails[]) mapper.unmarshall(cerebraAPIDataLayer.RigCMRDetailsBySubSubCode(rigName, starttime, endtime).toString(), new CMRDetails[]{}));
    }

    public Collection getCMRDetails(String rigName, String startTime, String endTime, String subCode) {
        return (Collection) getCMRDetails(rigName, startTime, endTime)
                .stream()
                .filter(f -> ((CMRDetails) f).getSub_code_text().equals(subCode))
                .collect(Collectors.toList());
    }

    /**
     * Takes a comma separated list of wells (12) and returns the report specific data
     *
     * @param well_num comma separated list of wells (12)
     * @return
     */
    public Collection getLastTwelveGrid(String well_num, Boolean alias) {
        JSONArray myJSONArray = null;
        String query = "WITH LatestCMR AS (\n" +
                "SELECT a.well_num\n" +
                "                , a.cmr_data_id\n" +
                "                , a.well_name\n" +
                "                , CONCAT('Well ', CAST(ROW_NUMBER() OVER(ORDER BY a.release_time DESC) AS STRING)) AS well_alias\n" +
                "                , trunc(to_timestamp(a.release_time), 'DD') AS release_time\n" +
                "                , a.county\n" +
                "                , a.state_province\n" +
                "                , a.preset\n" +
                "                , CASE a.batch_drilling WHEN 1 THEN 'Yes' WHEN 0 THEN 'No' ELSE 'No' END AS batch_drilling\n" +
                "                , a.trucks_move_distance\n" +
                "                , a.surface_depth\n" +
                "                , a.total_depth\n" +
                "                , a.rig_move_time / 24.0 AS rig_move_cycle_time\n" +
                "                , ((a.surface_end_time - a.spud_time) + (a.vertical_end_time - a.drill_out_from_under_surface_time) + (a.curve_end_time - a.curve_start_time) + (a.lateral_end_time - a.lateral_start_time))/ 86400.0 AS calculated_spud_to_td_days\n" +
                "                , ((COALESCE(a.lateral_end_time, a.curve_end_time, a.vertical_end_time) - COALESCE(a.spud_time, a.drill_out_from_under_surface_time, a.curve_start_time, a.lateral_start_time)) / 86400.0) - a.paused_days AS spud_to_td_days\n" +
                "                , (a.release_time - COALESCE(a.lateral_end_time, a.curve_end_time, a.vertical_end_time)) / 86400.0 AS td_to_release_days\n" +
                "                , ((a.release_time - COALESCE(a.previous_well_release_time, a.daywork_start_time)) / 86400.0) - a.paused_days AS total_cycle_time_days\n" +
                "  FROM pten_cmr.vw_cmr_data_usalt7_release a\n" +
                "WHERE a.well_num IN (" + well_num + ")\n" +
                "   AND a.report_date = (\n" +
                "                SELECT MAX(b.report_date)\n" +
                "                  FROM pten_cmr.cmr_data_usalt7 b\n" +
                "                 WHERE a.well_num = b.well_num)\n" +
                ")\n" +
                "SELECT lcmr.well_num\n" +
                "                , lcmr.cmr_data_id\n" +
                "                , lcmr.well_name\n" +
                "                , lcmr.well_alias\n" +
                "                , lcmr.release_time\n" +
                "                , lcmr.county\n" +
                "                , lcmr.state_province\n" +
                "                , lcmr.preset\n" +
                "                , lcmr.batch_drilling\n" +
                "                , lcmr.trucks_move_distance\n" +
                "                , lcmr.surface_depth\n" +
                "                , lcmr.total_depth\n" +
                "                , lcmr.rig_move_cycle_time\n" +
                "                , SUM(wt.walk_time) AS total_walk_time            \n" +
                "                , lcmr.calculated_spud_to_td_days\n" +
                "                , lcmr.spud_to_td_days\n" +
                "                , lcmr.td_to_release_days           \n" +
                "                , lcmr.total_cycle_time_days\n" +
                "                , lcmr.total_depth / lcmr.spud_to_td_days AS avg_feet_per_day\n" +
                "  FROM LatestCMR lcmr\n" +
                "                LEFT OUTER JOIN pten_cmr.cmr_walk_times_usalt7 wt ON lcmr.cmr_data_id = wt.cmr_data_id\n" +
                "GROUP BY lcmr.well_num\n" +
                "                  , lcmr.cmr_data_id\n" +
                "                  , lcmr.well_name\n" +
                "                  , lcmr.well_alias\n" +
                "                  , lcmr.release_time\n" +
                "                  , lcmr.county\n" +
                "                  , lcmr.state_province\n" +
                "                  , lcmr.preset\n" +
                "                  , lcmr.batch_drilling\n" +
                "                  , lcmr.trucks_move_distance\n" +
                "                  , lcmr.surface_depth\n" +
                "                  , lcmr.total_depth\n" +
                "                  , lcmr.rig_move_cycle_time        \n" +
                "                  , lcmr.calculated_spud_to_td_days\n" +
                "                  , lcmr.spud_to_td_days                \n" +
                "                  , lcmr.td_to_release_days\n" +
                "                  , lcmr.total_cycle_time_days\n" +
                "ORDER BY lcmr.release_time desc\n";

//        log.debug(query);
        Collection<PerformanceMetrics> result = jdbcTemplate.query(
                query,
                (rs, rowNum) -> new PerformanceMetrics(alias,
                        rs.getInt("well_num"),
                        rs.getInt("cmr_data_id"),
                        rs.getString("well_name"),
                        rs.getString("well_alias"),
                        rs.getDate("release_time"),
                        rs.getString("county"),
                        rs.getString("state_province"),
                        rs.getBoolean("preset"),
                        rs.getBoolean("batch_drilling"),
                        rs.getDouble("trucks_move_distance"),
                        rs.getDouble("surface_depth"),
                        rs.getDouble("total_depth"),
                        rs.getDouble("rig_move_cycle_time"),
                        rs.getDouble("total_walk_time"),
                        rs.getDouble("calculated_spud_to_td_days"),
                        rs.getDouble("spud_to_td_days"),
                        rs.getDouble("td_to_release_days"),
                        rs.getDouble("total_cycle_time_days"),
                        rs.getDouble("avg_feet_per_day")
                )
        ).stream().collect(toList());

        return result;
    }


    /**
     * hardcoded mapping f
     *
     * @param rig
     * @param limit
     * @return
     */
    public Collection getLastCompletedWellsHardcoded(String rig, int limit) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(
                "with LastCompletedWells as (\n" +
                        "SELECT well_num\n" +
                        "\t,rig_name\n" +
                        "from pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                        "where u.userid=" + getUser_id() + " and  release_time is not null\n" +
                        "order by release_time desc\n" +
                        ")\n" +
                        "select distinct well_num\n" +
                        "from LastCompletedWells\n" +
                        "where rig_name =:rig");
        if (limit > 0) {
            stringBuilder.append(" limit :limit");
        }
        stringBuilder.append(";");

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("rig", rig)
                .addValue("limit", limit);

        Collection<Well> wells = namedParameterJdbcTemplate.query(
                stringBuilder.toString(),
                namedParameters,
                (rs, rowNum) -> new Well(
                        //rs.getString("well_num")
                )).stream().collect(toList());
        return wells;
    }

    /**
     * return last completed wells
     *
     * @param limit number of wells to return
     * @return
     */
    public Collection getLastCompletedWells(String rig, int limit) {

        Collection model = cerebraAPIDataLayer.getLastCompletedWells(rig, limit);

        return mapper.unmarshall(model, new Well());

    }

    public Collection getCompletedWellsList(String region, Date startDate) {

        long startTime = DateTimeUtils.dateToFirstSecondOfDay(DateTimeUtils.dateToFirstDayOfMonth(startDate));
        long endTime = DateTimeUtils.dateToLastSecondOfDay(DateTimeUtils.dateToLastDayOfMonth(startDate));

        String query = "WITH completedWells as (\n" +
                "SELECT distinct a.well_num\n" +
                " ,a.well_name\n" +
                " ,a.rig_name\n" +
                " ,a.release_time\n" +
                "from pten_cmr.vw_cmr_data_usalt7_release a \n" +
                "inner join user_Security.user_mapping_details u on  a.well_num = u.well_num AND u.userid = :user\n" +
                "INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "where UPPER(rm.region_name) in (:region)\n" +
                "and release_time between :startDate and :endDate\n" +
                "and not exists (select null from pten_cmr.vw_cmr_data_usalt7_release z where a.well_num = z.well_num and a.report_date < z.report_date)\n" +
                ")\n" +
                "select well_num\n" +
                ", to_timestamp(release_time) as release_time\n" +
                ",well_name\n" +
                "from completedWells\n" +
                "order by release_time desc";

        log.debug("{}", query);

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("region", region.toUpperCase())
                .addValue("startDate", startTime)
                .addValue("endDate", endTime)
                .addValue("user", getUser_id());

        return impalaDbUtilLib.executeNamedQuery(query, namedParameters);
    }

    public Collection getCompletedWells(String region, Date startTime, boolean alias) {

        Collection completedWellsList = getCompletedWellsList(region, startTime);

        Collection<Long> wells = new ArrayList<>();

        for (Object o : completedWellsList) {
            JSONObject jsonObject = (JSONObject) o;
            try {
                wells.add(jsonObject.getLong("well_num"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        int wellAlias = alias ? 1 : 0;

        String query = "WITH LatestCMR AS (\n" +
                "SELECT a.well_num\n" +
                "                , a.cmr_data_id\n" +
                "                , a.well_name\n" +
                "                , a.rig_name\n" +
                "                , rm.region_name\n" +
                "                , rt.display_text as rig_type\n" +
                "                , CASE WHEN :alias = 1 then cast(a.well_num as string) else UPPER(a.well_name) end AS well\n" +
                "                , CONCAT('Well ', CAST(ROW_NUMBER() OVER(ORDER BY a.release_time DESC) AS STRING)) AS well_alias\n" +
                "                , from_unixtime(a.release_time,'MM/dd/yyyy') AS release_time\n" +
                "                , a.county\n" +
                "                , a.state_province\n" +
                "                , a.preset\n" +
                "                , CASE a.batch_drilling WHEN 1 THEN 'Yes' WHEN 0 THEN 'No' ELSE 'No' END AS batch_drilling\n" +
                "                , a.trucks_move_distance\n" +
                "                , a.surface_depth\n" +
                "                , a.total_depth\n" +
                "                , a.rig_move_time / 24.0 AS rig_move_cycle_time\n" +
                "                , ((COALESCE(a.lateral_end_time, a.curve_end_time, a.vertical_end_time) - COALESCE(a.spud_time, a.drill_out_from_under_surface_time, a.curve_start_time, a.lateral_start_time)) / 86400.0) - a.paused_days AS spud_to_td_days\n" +
                "                , (a.release_time - COALESCE(a.lateral_end_time, a.curve_end_time, a.vertical_end_time)) / 86400.0 AS td_to_release_days\n" +
                "                , ((a.release_time - COALESCE(a.spud_time, a.drill_out_from_under_surface_time, a.curve_start_time, a.lateral_start_time)) / 86400.0 ) - a.paused_days AS spud_to_release_days\n" +
                "                , ((a.release_time - COALESCE(a.previous_well_release_time, a.daywork_start_time)) / 86400.0) - a.paused_days AS total_cycle_time_days\n" +
                "FROM pten_cmr.vw_cmr_data_usalt7_release a\n" +
                "  inner join user_Security.user_mapping_details u on  a.well_num=u.well_num AND u.userid = :user\n" +
                "  INNER JOIN pten_cmr.rig r  ON r.display_text = a.rig_name\n" +
                "  INNER JOIN pten_cmr.rigtype rt  ON r.rig_type_id = rt.rig_type_id\n" +
                "  INNER JOIN pten_cmr.rigmastertype rmt  ON rmt.rig_master_type_id = rt.rig_master_type_id\n" +
                "  join pten_cmr.regionmap rm  on UPPER(rm.contractor_business_unit) = UPPER(a.contractor_business_unit)\n" +
                "WHERE a.well_num IN (:wells)\n" +
                "     AND NOT EXISTS (\n" +
                "                SELECT NULL\n" +
                "                  FROM pten_cmr.cmr_data_usalt7 z\n" +
                "                WHERE a.well_num = z.well_num\n" +
                "                   AND a.report_date < z.report_date)\n" +
                "), wellsDetails as (\n" +
                "SELECT lcmr.well_num\n" +
                "                , lcmr.cmr_data_id\n" +
                "                , UPPER(lcmr.well_name) AS well_name\n" +
                "                , UPPER(lcmr.rig_name) AS rig_name\n" +
                "                , UPPER(lcmr.region_name) AS region_name\n" +
                "                , lcmr.well\n" +
                "                , UPPER(lcmr.well_alias) AS well_alias\n" +
                "                , lcmr.rig_type\n" +
                "                , lcmr.release_time\n" +
                "                , UPPER(concat (lcmr.county , ', ' , lcmr.state_province)) as county_state\n" +
                "                , lcmr.trucks_move_distance\n" +
                "                , lcmr.surface_depth\n" +
                "                , lcmr.total_depth\n" +
                "                , cast(lcmr.rig_move_cycle_time as DECIMAL(10,2)) as rig_move_cycle_time\n" +
                "                , AVG(wt.walk_time) AS total_walk_time            \n" +
                "                , cast(lcmr.spud_to_td_days as DECIMAL(10,2)) as spud_to_td_days\n" +
                "                , cast(lcmr.td_to_release_days as DECIMAL(10,2)) as td_to_release_days\n" +
                "                , cast(lcmr.spud_to_release_days as DECIMAL(10,2)) as spud_to_release_days\n" +
                "                , cast(lcmr.total_cycle_time_days as DECIMAL(10,2)) as total_cycle_time_days\n" +
                "                , cast(lcmr.total_depth / lcmr.spud_to_td_days as DECIMAL(10,0)) AS avg_feet_per_day\n" +
                "FROM LatestCMR lcmr\n" +
                " LEFT OUTER JOIN pten_cmr.cmr_walk_times_usalt7 wt ON lcmr.cmr_data_id = wt.cmr_data_id\n" +
                "GROUP BY lcmr.well_num\n" +
                "                  , lcmr.cmr_data_id\n" +
                "                  , lcmr.well_name\n" +
                "                  , lcmr.rig_name\n" +
                "                  , lcmr.rig_type\n" +
                "                  , lcmr.region_name\n" +
                "                  , lcmr.well\n" +
                "                  , lcmr.well_alias\n" +
                "                  , lcmr.release_time\n" +
                "                  , UPPER(concat (lcmr.county , ', ' , lcmr.state_province))\n" +
                "                  , lcmr.trucks_move_distance\n" +
                "                  , lcmr.surface_depth\n" +
                "                  , lcmr.total_depth\n" +
                "                  , lcmr.rig_move_cycle_time        \n" +
                "                  , lcmr.spud_to_td_days                \n" +
                "                  , lcmr.spud_to_release_days\n" +
                "                  , lcmr.td_to_release_days\n" +
                "                  , lcmr.total_cycle_time_days\n" +
//                "                  , wt.walk_time                  \n" +
                "ORDER BY lcmr.release_time desc\n" +
                "\n" +
                ") \n" +
                "Select * from wellsDetails";
//                "select\n" +
//                "  w.county_state as county_state\n" +
//                "  ,avg(nullif(w.surface_depth, 0)) over(Partition by w.county_state) as surface_depth_avg\n" +
//                "  ,avg(nullif(w.total_depth, 0)) over(Partition by w.county_state) as total_depth_avg\n" +
//                "  ,avg(nullif(w.trucks_move_distance, 0)) over(Partition by w.county_state) as trucks_move_distance_avg\n" +
//                "  ,avg(nullif(w.rig_move_cycle_time, 0)) over(Partition by w.county_state) as rig_move_cycle_time_avg\n" +
//                "  ,avg(nullif(w.total_walk_time, 0)) over(Partition by w.county_state) as total_walk_time_avg\n" +
//                "  ,avg(nullif(w.spud_to_td_days, 0)) over(Partition by w.county_state) as spud_to_td_days_avg\n" +
//                "  ,avg(nullif(w.td_to_release_days, 0)) over(Partition by w.county_state) as td_to_release_days_avg\n" +
//                "  ,avg(nullif(w.spud_to_release_days, 0)) over(Partition by w.county_state) as spud_to_release_days_avg\n" +
//                "  ,avg(nullif(w.total_cycle_time_days, 0)) over(Partition by w.county_state) as total_cycle_time_days_avg\n" +
//                "  ,avg(nullif(w.avg_feet_per_day, 0)) over(Partition by w.county_state) as avg_feet_per_day_avg\n" +
//                "  , *\n" +
//                "from wellsDetails w\n" +
//                "UNION \n" +
//                "select\n" +
//                "  'Averages' as county_state\n" +
//                "  ,avg(w.surface_depth)over(Partition by w.region_name) as surface_depth_avg\n" +
//                "  ,avg(w.total_depth)over(Partition by region_name) as total_depth_avg\n" +
//                "  ,avg(w.trucks_move_distance)over(Partition by w.region_name) as trucks_move_distance_avg\n" +
//                "  ,avg(w.rig_move_cycle_time)over(Partition by w.region_name) as rig_move_cycle_time_avg\n" +
//                "  ,avg(w.total_walk_time)over(Partition by w.region_name) as total_walk_time_avg\n" +
//                "  ,avg(w.spud_to_td_days)over(Partition by w.region_name) as spud_to_td_days_avg\n" +
//                "  ,avg(w.td_to_release_days)over(Partition by w.region_name) as td_to_release_days_avg\n" +
//                "  ,avg(w.spud_to_release_days)over(Partition by w.region_name) as spud_to_release_days_avg\n" +
//                "  ,avg(w.total_cycle_time_days)over(Partition by w.region_name) as total_cycle_time_days_avg\n" +
//                "  ,avg(w.avg_feet_per_day)over(Partition by w.region_name) as avg_feet_per_day_avg\n" +
//                "  , *\n" +
//                "from wellsDetails w\n" +
//                "limit 1";

        log.debug("{}", query);

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells)
                .addValue("user", getUser_id())
                .addValue("alias", wellAlias);

        return impalaDbUtilLib.executeNamedQuery(query, namedParameters);
    }
    public Collection getJobWells(String rig, String job_no) {
        String query = "select distinct " +
                " a.well_num" +
                ",a.well_name\n" +
                "from pten_cmr.cmr_data_usalt7 a\n" +
                "  inner join user_Security.user_mapping_details u on  a.well_num=u.well_num AND u.userid =:user\n" +
                "where a.rig_name in(:rig) and a.job_no in(:job)";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("rig", rig)
                .addValue("job", job_no)
                .addValue("user", getUser_id());

        return impalaDbUtilLib.executeNamedQuery(query.toString(), namedParameters);
    }

    public Collection getWells(Collection<Long> wells) {
        Collection model = cerebraAPIDataLayer.getWells(wells);
        return mapper.unmarshall(model, Well.class);
    }


    public Collection getCMROperators() {
        String query = "SELECT DISTINCT REGEXP_REPLACE(OPERATOR_NAME,',','') AS OPERATOR_NAME \n" +
                "FROM pten_cmr.cmr_data_usalt7 WHERE operator_name<>'' ORDER BY OPERATOR_NAME ASC";

        Collection<Operator> result = jdbcTemplate.query(
                query,
                (rs, rowNum) -> new Operator(
                        rs.getString("OPERATOR_NAME")
                )
        ).stream().collect(toList());

        return result;
    }

    public Collection getCMRRegions() {
        String query = "SELECT DISTINCT region_name FROM pten_cmr.regionmap;";
        Collection<Region> result = jdbcTemplate.query(
                query,
                (rs, rowNum) -> new Region(
                        rs.getString("region_name")
                )
        ).stream().collect(toList());

        return result;
    }


    public long getLastReportDate(String rigNum) {
        String sql = "select top 1 report_date from pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num where u.userid=" + getUser_id() + " and  rig_name='" + rigNum + "' order by report_date desc";
        long report_date = Long.parseLong(jdbcTemplate.queryForObject(
                sql, String.class));

        return report_date;
    }


    public Collection getCMROperatorRegions(String operator) {

        String query = "select distinct region_name from pten_cmr.regionmap r join pten_Cmr.cmr_data_usalt7 d on UPPER(r.contractor_business_unit) = UPPER(d.contractor_business_unit)  inner join user_Security.user_mapping_details u on  d.well_num=u.well_num\n" +
                ((operator.equalsIgnoreCase("'All Operators'")) ? "\n" : "WHERE u.userid=" + getUser_id() + " and  REGEXP_REPLACE(OPERATOR_NAME,',','') in (" + operator + ")\n") +
                "ORDER BY region_name";

        Collection<Region> result = jdbcTemplate.query(
                query,
                (rs, rowNum) -> new Region(
                        rs.getString("region_name")
                )
        ).stream().collect(toList());

        return result;
    }

    public Collection getCMROperatorRegionRigTypes(String operator, String region) {

        Collection regionList = Arrays.asList(operator.split(","));
        String regionListCS = regionList.stream().collect(Collectors.joining("','", "'", "'")).toString();

        String query = "WITH TopQuery AS (\n" +
                "SELECT rt.rig_master_type_id, " +
                " rmt.rig_master_type_name as display_text\n" +
                "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num and u.userid=" + getUser_id() + "\n" +
                "  INNER JOIN pten_cmr.RegionMap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit) \n" +
                "  INNER JOIN pten_cmr.Rig r ON r.display_text = a.rig_name \n" +
                "  INNER JOIN pten_cmr.RigType rt ON r.rig_type_id = rt.rig_type_id \n" +
                "  INNER JOIN pten_cmr.rigmastertype rmt ON rmt.rig_master_type_id = rt.rig_master_type_id\n" +
                ((operator.equalsIgnoreCase("'All Operators'") && region.equalsIgnoreCase("'All Regions'")) ? "\n" :
                        "WHERE \n" +
                                ((region.equalsIgnoreCase("'All Regions'")) ? "\n" : "rm.region_name in (" + region + ") \n" + ((operator.equalsIgnoreCase("'All Operators'") ? "" : " AND "))) +
                                ((operator.equalsIgnoreCase("'All Operators'")) ? "\n" : "REGEXP_REPLACE(OPERATOR_NAME,',','') in (" + operator + ")\n")) +
                ") \n" +
                "SELECT DISTINCT * FROM TopQuery;";

        Collection<RigType> result = jdbcTemplate.query(
                query,
                (rs, rowNum) -> new RigType(
                        rs.getInt("rig_master_type_id"),
                        rs.getString("display_text")
                )
        ).stream().collect(toList());

        return result;
    }

    public Collection getCMROperatorRegionRigTypes(List<String> operator, List<String> region, String starttime, String endtime) throws ParseException {

        long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
        long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);


        String query = "WITH TopQuery AS (\n" +
                "SELECT rmt.rig_master_type_id AS id" +
                " ,rmt.rig_master_type_name AS type_name\n" +
                "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                " INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                " INNER JOIN pten_cmr.RegionMap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit) \n" +
                " INNER JOIN pten_cmr.Rig r ON r.display_text = a.rig_name \n" +
                " INNER JOIN pten_cmr.RigType rt ON r.rig_type_id = rt.rig_type_id \n" +
                " INNER JOIN pten_cmr.rigmastertype rmt ON rmt.rig_master_type_id = rt.rig_master_type_id\n" +
                "WHERE u.userid=" + getUser_id() + " and (b.`from` BETWEEN :start AND :end OR b.`to` BETWEEN :start AND :end)\n" +
                ((operator == null && region == null) ? "\n" :
                        "AND " +
                                ((region == null) ? "\n" : "rm.region_name in (:region) \n" + ((operator == null ? "" : " AND "))) +
                                ((operator == null) ? "\n" : "REGEXP_REPLACE(OPERATOR_NAME,',','') in (:operator)\n")) +
                ") \n" +
                "SELECT DISTINCT id, type_name FROM TopQuery\n" +
                "ORDER BY type_name;";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("start", startTime)
                .addValue("end", endTime)
                .addValue("region", region)
                .addValue("operator", operator);

        return impalaDbUtilLib.executeNamedQuery(query, namedParameters);

    }


    //(b.`from` BETWEEN 1517443200 AND 1535759999 OR b.`to` BETWEEN 1517443200 AND 1535759999)

    public Collection getCMROperatorRegionRigTypeRigList(String operator, String region, String rigType) {

        String query = "WITH TopQuery AS (\n" +
                "SELECT a.rig_name\n" +
                "FROM pten_cmr.cmr_data_usalt7 a INNER JOIN user_security.user_mapping_Details u ON a.well_num=u.well_num  \n" +
                "\tINNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                "\tLEFT JOIN pten_cmr.RegionMap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit) \n" +
                "\tINNER JOIN pten_cmr.Rig r ON r.display_text = a.rig_name \n" +
                "\tLEFT JOIN pten_cmr.RigType rt ON r.rig_type_id = rt.rig_type_id \n" +
                ((operator.equalsIgnoreCase("'All Operators'") && region.equalsIgnoreCase("'All Regions'") && rigType.equalsIgnoreCase("-99")) ? "\n" :
                        "WHERE u.userid=" + getUser_id() + " and  \n" +
                                ((region.equalsIgnoreCase("'All Regions'")) ? "\n" : "rm.region_name in (" + region + ") \n" +
                                        (operator.equalsIgnoreCase("'All Operators'") ? "" : " AND ")) +
                                ((operator.equalsIgnoreCase("'All Operators'")) ? "\n" : "REGEXP_REPLACE(OPERATOR_NAME,',','') in (" + operator + ")\n" +
                                        ((rigType.equalsIgnoreCase("-99") ? "" : " AND "))) +
                                ((rigType.equalsIgnoreCase("-99") ? "" : " rt.rig_type_id in (" + rigType + ") "))) +
                ") \n" +
                "SELECT DISTINCT * FROM TopQuery ORDER BY rig_name;";

        Collection<Rig> result = jdbcTemplate.query(
                query,
                (rs, rowNum) -> new Rig(
                        rs.getString("rig_name")
                )
        ).stream().collect(toList());

        return result;
    }


    public Collection getCMRORigTypeTimeRigList(String starttime, String endtime, int rigType, Optional<Integer> selection) throws ParseException {

        long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
        long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

        String query = "WITH TopQuery AS (\n" +
                "SELECT a.rig_name\n" +
                "FROM pten_cmr.cmr_data_usalt7 a INNER JOIN user_security.user_mapping_Details u ON a.well_num=u.well_num  \n" +
                "  INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                "  LEFT JOIN pten_cmr.RegionMap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit) \n" +
                "  INNER JOIN pten_cmr.Rig r ON r.display_text = a.rig_name \n" +
                "  LEFT JOIN pten_cmr.RigType rt ON r.rig_type_id = rt.rig_type_id \n" +
                "  INNER JOIN pten_cmr.rigmastertype rmt ON rmt.rig_master_type_id = rt.rig_master_type_id\n" +
                "  WHERE u.userid=" + getUser_id() + " and  (b.`from` BETWEEN :start AND :end OR b.`to` BETWEEN :start AND :end)\n" +
                ((rigType == -99 ? "" : "AND rt.rig_master_type_id in (:rigtype) \n")) +
                ") \n" +
                "SELECT DISTINCT rig_name AS name\n " +
                "FROM TopQuery ORDER BY rig_name" +
                ((selection.isPresent()) ? "\nLimit :selection;" : ";");

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("start", startTime)
                .addValue("end", endTime)
                .addValue("rigtype", rigType)
                .addValue("selection", (selection.isPresent()) ? selection.get() : null);


        return impalaDbUtilLib.executeNamedQuery(query, namedParameters);

    }

    public Collection getWellDailyDepth(Collection<Long> wells) {

        Collection model = cerebraAPIDataLayer.getWellsCmrDailyDepth(wells);

        Collection<WellDailyDepth> dailyDepths = mapper.unmarshall(model, WellDailyDepth.class);

        /**
         * flattening well daily depths per day.
         * flutura plotting line limitation.
         */
        Set<Integer> days = dailyDepths.stream()
                .map(day -> day.getDay_num())
                .collect(Collectors.toSet());

        Collection<WellDailyDepthsFlatMap> wellDailyDepthFlats = new ArrayList<>();
        WellDailyDepthsFlatMap wellDailyDepthFlat;
        for (int day : days) {
            wellDailyDepthFlat = new WellDailyDepthsFlatMap();
            wellDailyDepthFlat.setDay_num(day);

            for (WellDailyDepth wellDailyDepth : dailyDepths) {

                if (day == wellDailyDepth.getDay_num()) {
                    wellDailyDepthFlat.add(
                            wellDailyDepth.getWell().getWell_alias()
                                    .toLowerCase()
                                    .replaceAll(" ", "_") + "_day_depth"
                            , wellDailyDepth.getDay_depth()
                    );
                }
            }
            wellDailyDepthFlats.add(wellDailyDepthFlat);
        }

        return wellDailyDepthFlats;
    }

//    public getRigTypeByRegionList(String[] regions){
//
//    }


    /**
     * Returns all drilling, tripping, casing, npt, bop, move, other and total hours, for each well.
     * Takes no parameters.
     *
     * @return
     * @throws ParseException
     */
    public Collection getTimePerActivityAllRigsWells() throws ParseException {

        String query = "WITH LastCMR AS (\n" +
                "SELECT a.well_num\n" +
                "     , a.well_name\n" +
                "     , a.rig_name\n" +
                "     , a.release_time\n" +
                "     , a.estimated_release\n" +
                "     , a.operator_name\n" +
                "     , a.contractor_business_unit\n" +
                "  FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_Details u on a.well_num=u.well_num and u.userid=" + getUser_id() + " \n" +
                " WHERE NOT EXISTS (\n" +
                "    SELECT NULL\n" +
                "      FROM pten_cmr.cmr_data_usalt7 b inner join user_Security.user_mapping_Details u on b.well_num=u.well_num and u.userid=" + getUser_id() + " \n" +
                "     WHERE a.well_num = b.well_num\n" +
                "       AND a.report_date < b.report_date)\n" +
                "), WellsWithTotals AS (\n" +
                "SELECT a.well_num\n" +
                "     , SUM(CASE WHEN b.code = '2' THEN b.hours ELSE 0 END) AS drilling_hours\n" +
                "     , SUM(CASE WHEN b.code = '6' THEN b.hours ELSE 0 END) AS tripping_hours\n" +
                "     , SUM(CASE WHEN b.code = '12' THEN b.hours ELSE 0 END) AS casing_hours\n" +
                "     , SUM(CASE WHEN b.code IN ('14','15') THEN b.hours ELSE 0 END) AS bop_hours\n" +
                "     , SUM(CASE WHEN b.code = '8' AND UPPER(b.sub_code) NOT LIKE '%Z%' THEN b.hours ELSE 0 END) AS npt_hours\n" +
                "     , SUM(CASE WHEN b.code = '1' AND UPPER(b.sub_code) NOT LIKE '%Z%' THEN b.hours ELSE 0 END) AS move_hours\n" +
                "     , SUM(CASE WHEN b.code NOT IN ('1','2','6','8','12','14','15') THEN b.hours ELSE 0 END) AS other_hours\n" +
                "     , SUM(b.hours) AS total_hours\n" +
                "  FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_Details u on a.well_num=u.well_num and u.userid=" + getUser_id() + " \n" +
                "    INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id\n" +
                " GROUP BY a.well_num\n" +
                ")\n" +
                "SELECT cmr.well_num\n" +
                "     , cmr.well_name\n" +
                "     , cmr.rig_name\n" +
                "     , rt.rig_type_id\n" +
                "     , rt.display_text AS rig_type_text\n" +
                "     , CASE WHEN cmr.release_time IS NOT NULL THEN 1 ELSE 0 END AS completed_well\n" +
                "     , to_timestamp(COALESCE(cmr.release_time, cmr.estimated_release)) AS release_time\n" +
                "     , YEAR(to_timestamp(COALESCE(cmr.release_time, cmr.estimated_release))) AS year\n" +
                "     , MONTH(to_timestamp(COALESCE(cmr.release_time, cmr.estimated_release))) AS month\n" +
                "     , DAY(to_timestamp(COALESCE(cmr.release_time, cmr.estimated_release))) AS day\n" +
                "     , rm.region_name\n" +
                "     , cmr.operator_name\n" +
                "     , wwt.drilling_hours\n" +
                "     , wwt.tripping_hours\n" +
                "     , wwt.casing_hours\n" +
                "     , wwt.bop_hours\n" +
                "     , wwt.npt_hours\n" +
                "     , wwt.move_hours\n" +
                "     , wwt.other_hours\n" +
                "     , wwt.total_hours\n" +
                "  FROM LastCMR cmr\n" +
                "    INNER JOIN pten_cmr.regionmap rm ON UPPER(cmr.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "    INNER JOIN pten_cmr.rig r ON UPPER(cmr.rig_name) = UPPER(r.display_text)\n" +
                "    INNER JOIN pten_cmr.rigtype rt ON r.rig_type_id = rt.rig_type_id\n" +
                "    INNER JOIN WellsWithTotals wwt ON cmr.well_num = wwt.well_num\n" +
                "    ORDER by rig_name, release_time, region_name";

        return impalaDbUtilLib.executeNamedQuery(query, null);

    }

    public Collection getActivityAllRigsWellsROP() {
        String query = "WITH LatestCMR AS (\n" +
                "                SELECT         a.well_num\n" +
                "                             , a.well_name\n" +
                "                             , a.rig_name\n" +
                "                             , a.release_time\n" +
                "                             , a.estimated_release\n" +
                "                             , a.operator_name\n" +
                "                             , a.contractor_business_unit\n" +
                "                             , a.cmr_data_id\n" +
                "                FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_Details u on a.well_num=u.well_num and u.userid=" + getUser_id() + " \n" +
                "                WHERE NOT EXISTS (\n" +
                "                               SELECT NULL\n" +
                "                                 FROM pten_cmr.cmr_data_usalt7 z inner join user_Security.user_mapping_Details u on z.well_num=u.well_num and u.userid=" + getUser_id() + " \n" +
                "                                WHERE a.well_num = z.well_num\n" +
                "                                  AND a.report_date < z.report_date)\n" +
                "                ), OperationHours AS (\n" +
                "                SELECT j.well_num\n" +
                "                                , SUM(CASE WHEN k.code = '2' THEN k.hours ELSE 0 END) AS drilling_hours\n" +
                "                                , SUM(CASE WHEN k.code = '6' THEN k.hours ELSE 0 END) AS tripping_hours\n" +
                "                                , SUM(CASE WHEN k.code = '12' THEN k.hours ELSE 0 END) AS casing_hours\n" +
                "                                , SUM(CASE WHEN k.code IN ('14','15') THEN k.hours ELSE 0 END) AS bop_hours\n" +
                "                                , SUM(CASE WHEN k.code = '8' THEN k.hours ELSE 0 END) AS npt_hours\n" +
                "                                , SUM(CASE WHEN k.code = '1' THEN k.hours ELSE 0 END) AS move_hours\n" +
                "                                , SUM(CASE WHEN k.code NOT IN ('1','2','6','8','12','13') THEN k.hours ELSE 0 END) AS other_hours\n" +
                "                                , SUM(k.hours) AS total_hours\n" +
                "                  FROM pten_cmr.cmr_data_usalt7 j inner join user_Security.user_mapping_Details u on j.well_num=u.well_num and u.userid=" + getUser_id() + " \n" +
                "                                INNER JOIN pten_cmr.cmr_time_code_data_usalt7 k ON j.cmr_data_id = k.cmr_data_id\n" +
                "                GROUP BY j.well_num\n" +
                "                ), DepthData AS (select DISTINCT well_num  FROM ptendrilling.ptendrilling_depthdata \n" +
                "                         )," +
                "                StandAggregates AS (\n" +
                "                SELECT wss.well_num\n" +
                "                                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'SURFACE' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.rotating_footage ELSE 0 END) AS surface_rotary_footage\n" +
                "                                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'SURFACE' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sec_rotating ELSE 0 END) AS surface_rotary_seconds\n" +
                "                                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'SURFACE' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sliding_footage ELSE 0 END) AS surface_slide_footage\n" +
                "                                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'SURFACE' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sec_sliding ELSE 0 END) AS surface_slide_seconds\n" +
                "                                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'VERTICAL' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.rotating_footage ELSE 0 END) AS vertical_rotary_footage\n" +
                "                                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'VERTICAL' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sec_rotating ELSE 0 END) AS vertical_rotary_seconds\n" +
                "                                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'VERTICAL' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sliding_footage ELSE 0 END) AS vertical_slide_footage\n" +
                "                                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'VERTICAL' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sec_sliding ELSE 0 END) AS vertical_slide_seconds\n" +
                "                                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'CURVE' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.rotating_footage ELSE 0 END) AS curve_rotary_footage\n" +
                "                                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'CURVE' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sec_rotating ELSE 0 END) AS curve_rotary_seconds\n" +
                "                                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'CURVE' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sliding_footage ELSE 0 END) AS curve_slide_footage\n" +
                "                                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'CURVE' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sec_sliding ELSE 0 END) AS curve_slide_seconds\n" +
                "                                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'LATERAL' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.rotating_footage ELSE 0 END) AS lateral_rotary_footage\n" +
                "                                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'LATERAL' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sec_rotating ELSE 0 END) AS lateral_rotary_seconds\n" +
                "                                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'LATERAL' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sliding_footage ELSE 0 END) AS lateral_slide_footage\n" +
                "                                , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'LATERAL' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sec_sliding ELSE 0 END) AS lateral_slide_seconds\n" +
                "                  FROM ptendrilling_new.ptendrilling_WellStandSeries wss\n" +
                "                GROUP BY wss.well_num\n" +
                "                )\n" +
                "                SELECT a.well_num\n" +
                "                                , a.well_name\n" +
                "                                , a.rig_name\n" +
                "                                , rt.rig_type_id\n" +
                "                                , rmt.rig_master_type_name AS rig_type_text\n" +
                "                                , CASE WHEN a.release_time IS NOT NULL AND a.total_depth > 500.0 AND a.daywork_days > 2.0 THEN 1 ELSE 0 END AS completed_well\n" +
                "                                , to_timestamp(COALESCE(a.release_time, a.estimated_release)) AS release_time\n" +
                "                                , YEAR(to_timestamp(COALESCE(a.release_time, a.estimated_release))) AS year\n" +
                "                                , MONTH(to_timestamp(COALESCE(a.release_time, a.estimated_release))) AS month\n" +
                "                                , DAY(to_timestamp(COALESCE(a.release_time, a.estimated_release))) AS day\n" +
                "                                , rm.region_name           \n" +
                "                                , a.operator_name\n" +
                "                                , a.daywork_days AS days_on_well\n" +
                "                                , COALESCE(oh.drilling_hours, 0) AS drilling_hours\n" +
                "                                , COALESCE(oh.tripping_hours, 0) AS tripping_hours\n" +
                "                                , COALESCE(oh.casing_hours, 0) AS casing_hours\n" +
                "                                , COALESCE(oh.bop_hours, 0) AS bop_hours\n" +
                "                                , COALESCE(oh.npt_hours, 0) AS npt_hours\n" +
                "                                , COALESCE(oh.move_hours, 0) AS move_hours\n" +
                "                                , COALESCE(oh.other_hours, 0) AS other_hours\n" +
                "                                , COALESCE(oh.total_hours, 0) AS total_hours\n" +
                "                , (a.total_depth - CASE UPPER(a.preset) WHEN 'SURFACE' THEN a.surface_depth WHEN 'VERTICAL' THEN a.vertical_depth ELSE 0.0 END)  / nullifzero(a.daywork_days)  AS avg_feet_per_day\n" +
                "                                , sa.surface_rotary_footage\n" +
                "                                , sa.surface_rotary_seconds\n" +
                "                                , sa.surface_slide_footage\n" +
                "                                , sa.surface_slide_seconds\n" +
                "                                , sa.vertical_rotary_footage\n" +
                "                                , sa.vertical_rotary_seconds\n" +
                "                                , sa.vertical_slide_footage\n" +
                "                                , sa.vertical_slide_seconds\n" +
                "                                , sa.curve_rotary_footage\n" +
                "                                , sa.curve_rotary_seconds\n" +
                "                                , sa.curve_slide_footage\n" +
                "                                , sa.curve_slide_seconds\n" +
                "                                , sa.lateral_rotary_footage\n" +
                "                                , sa.lateral_rotary_seconds\n" +
                "                                , sa.lateral_slide_footage\n" +
                "                                , sa.lateral_slide_seconds\n" +
                "                                , daz.well_num as depth_data\n" +
                "          FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_Details u on a.well_num=u.well_num and u.userid=" + getUser_id() + " \n" +
                "                        INNER JOIN LatestCMR b ON a.cmr_data_id = b.cmr_data_id\n" +
                "                        INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "                        INNER JOIN pten_cmr.rig r ON r.display_text = a.rig_name\n" +
                "                        INNER JOIN pten_cmr.rigtype rt ON r.rig_type_id = rt.rig_type_id\n" +
                "                        INNER JOIN pten_cmr.rigmastertype rmt ON rmt.rig_master_type_id = rt.rig_master_type_id\n" +
                "                        LEFT OUTER JOIN StandAggregates sa ON sa.well_num = a.well_num\n" +
                "                        INNER JOIN OperationHours oh ON a.well_num = oh.well_num\n" +
                "                        LEFT OUTER JOIN DepthData daz ON daz.well_num = a.well_num\n"+
                "                        ORDER BY a.well_num";

        return impalaDbUtilLib.executeNamedQuery(query, null);
    }

    public Collection getRopDepthDataTop(){

        String query = "WITH LatestCMR AS ( " +
                "   SELECT a.well_num " +
                "           , a.well_name " +
                "           , a.rig_name  " +
                "           , a.release_time  " +
                "           , a.estimated_release " +
                "           , a.operator_name  " +
                "           , a.contractor_business_unit  " +
                "           , a.cmr_data_id  " +
                "   FROM pten_cmr.cmr_data_usalt7 a " +
                "   WHERE NOT EXISTS (  " +
                "       SELECT NULL  " +
                "       FROM pten_cmr.cmr_data_usalt7 z  " +
                "       WHERE a.well_num = z.well_num  " +
                "           AND a.report_date < z.report_date)  " +
                "), OperationHours AS (  " +
                "   SELECT j.well_num  " +
                "       , SUM(CASE WHEN k.code = '2' THEN k.hours ELSE 0 END) AS drilling_hours  " +
                "       , SUM(CASE WHEN k.code = '6' THEN k.hours ELSE 0 END) AS tripping_hours  " +
                "       , SUM(CASE WHEN k.code = '12' THEN k.hours ELSE 0 END) AS casing_hours  " +
                "       , SUM(CASE WHEN k.code IN ('14','15') THEN k.hours ELSE 0 END) AS bop_hours " +
                "       , SUM(CASE WHEN k.code = '8' THEN k.hours ELSE 0 END) AS npt_hours  " +
                "       , SUM(CASE WHEN k.code = '1' THEN k.hours ELSE 0 END) AS move_hours  " +
                "       , SUM(CASE WHEN k.code NOT IN ('1','2','6','8','12','13') THEN k.hours ELSE 0 END) AS other_hours  " +
                "       , SUM(k.hours) AS total_hours  " +
                "   FROM pten_cmr.cmr_data_usalt7 j  " +
                "   INNER JOIN pten_cmr.cmr_time_code_data_usalt7 k ON j.cmr_data_id = k.cmr_data_id  " +
                "   GROUP BY j.well_num  " +
                "), StandAggregates AS (  " +
                "   SELECT wss.well_num  " +
                "       , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'SURFACE' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.rotating_footage ELSE 0 END) AS surface_rotary_footage  " +
                "       , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'SURFACE' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sec_rotating ELSE 0 END) AS surface_rotary_seconds  " +
                "       , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'SURFACE' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sliding_footage ELSE 0 END) AS surface_slide_footage  " +
                "       , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'SURFACE' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sec_sliding ELSE 0 END) AS surface_slide_seconds  " +
                "       , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'VERTICAL' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.rotating_footage ELSE 0 END) AS vertical_rotary_footage  " +
                "       , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'VERTICAL' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sec_rotating ELSE 0 END) AS vertical_rotary_seconds  " +
                "       , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'VERTICAL' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sliding_footage ELSE 0 END) AS vertical_slide_footage  " +
                "       , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'VERTICAL' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sec_sliding ELSE 0 END) AS vertical_slide_seconds  " +
                "       , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'CURVE' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.rotating_footage ELSE 0 END) AS curve_rotary_footage  " +
                "       , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'CURVE' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sec_rotating ELSE 0 END) AS curve_rotary_seconds  " +
                "       , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'CURVE' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sliding_footage ELSE 0 END) AS curve_slide_footage  " +
                "       , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'CURVE' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sec_sliding ELSE 0 END) AS curve_slide_seconds  " +
                "       , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'LATERAL' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.rotating_footage ELSE 0 END) AS lateral_rotary_footage  " +
                "       , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'LATERAL' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sec_rotating ELSE 0 END) AS lateral_rotary_seconds  " +
                "       , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'LATERAL' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sliding_footage ELSE 0 END) AS lateral_slide_footage  " +
                "       , SUM(CASE WHEN UPPER(wss.bit_in_hole_section) = 'LATERAL' AND UPPER(wss.stand_type) = 'DRILLING' THEN wss.sec_sliding ELSE 0 END) AS lateral_slide_seconds  " +
                "   FROM ptendrilling_new.ptendrilling_WellStandSeries wss  " +
                "   GROUP BY wss.well_num  " +
                "),Final AS ( " +
                "      SELECT distinct a.well_num, a.release_time" +
                "      FROM pten_cmr.cmr_data_usalt7 a" +
                "              INNER JOIN LatestCMR b ON a.cmr_data_id = b.cmr_data_id  " +
                "              INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)  " +
                "              INNER JOIN pten_cmr.rig r ON r.display_text = a.rig_name  " +
                "              INNER JOIN pten_cmr.rigtype rt ON r.rig_type_id = rt.rig_type_id  " +
                "              LEFT OUTER JOIN StandAggregates sa ON sa.well_num = a.well_num  " +
                "              INNER JOIN OperationHours oh ON a.well_num = oh.well_num " +
                "              INNER JOIN ptendrilling.ptendrilling_depthdata dd ON a.well_num = dd.well_num  " +
                "       order by a.release_time desc " +
                "       limit 4" +
                ") " +
                "select a.well_num " +
                "from Final a " +
                "inner join user_Security.user_mapping_details u on  a.well_num = u.well_num AND u.userid = " + getUser_id() + ";";

        Collection<String> result = jdbcTemplate.query(query, (rs, rowNum) -> rs.getString("well_num")).stream().collect(toList());

        return result;

    }

    /**
     * Returns all drilling, tripping, casing, npt, bop, move, other and total hours, for each well.
     * Takes no parameters.
     * @return
     * @throws ParseException
     */
    public Collection getROPDepthData(Collection<Long> wells)  throws ParseException {

        String query = "SELECT if((is_inf(start_footage) || is_nan(start_footage)), null, start_footage) AS start_footage\n" +
                "        , if((is_inf(weight_on_bit) || is_nan(weight_on_bit)), null, weight_on_bit) AS weight_on_bit\n" +
                "        , if((is_inf(rop) || is_nan(rop)), null, rop) AS rop\n" +
                "        , if((is_inf(rpm) || is_nan(rpm)), null, rpm) AS rpm\n" +
                "        , if((is_inf(pump_total_strokes_rate) || is_nan(pump_total_strokes_rate)), null, pump_total_strokes_rate) AS pump_total_strokes_rate\n" +
                "        , if((is_inf(standpipe_pressure) || is_nan(standpipe_pressure)), null, standpipe_pressure) AS standpipe_pressure\n" +
                "        , if((is_inf(differential_pressure) || is_nan(differential_pressure)), null, differential_pressure) AS differential_pressure\n" +
                "        , if((is_inf(convertible_torque) || is_nan(convertible_torque)), null, convertible_torque) AS convertible_torque\n" +
                "        , if((is_inf(flow_paddle) || is_nan(flow_paddle)), null, flow_paddle) AS flow_paddle\n" +
                "        , if((is_inf(flow_in) || is_nan(flow_in)), null, flow_in) AS flow_in\n" +
                "        , sld_rot\n" +
                "        , well_num\n" +
                "        , well_name\n" +
                "        , rig_name\n" +
                "FROM ptendrilling.ptendrilling_depthdata\n" +
                "Where well_num in(:wells) AND start_footage < 30000;";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells);
        return impalaDbUtilLib.executeNamedQuery(query, namedParameters);

    }

    public Collection getTrippingStandsData(Collection<Long> wells)  throws ParseException {

        String query = "SELECT rm.division\n" +
                "  , rm.region_name\n" +
                "  , r.display_text AS rig\n" +
                "  , rt.display_text AS rig_type\n" +
                "  , a.well_name\n" +
                "  , ws.stand_start_time\n" +
                "  , ws.crew AS driller\n" +
                "  , ws.stand_type\n" +
                "  , ws.in_out AS stand_direction\n" +
                "  , ws.hole_status\n" +
                "  , ws.sec_s2s\n" +
                "  , ws.sec_tih_moving\n" +
                "  , ws.sec_tih_stab\n" +
                "  , ws.sec_tih_makeup\n" +
                "  , ws.sec_tooh_breakout\n" +
                "  , ws.sec_tooh_racking\n" +
                "  , ws.sec_tooh_moving\n" +
                "  , ws.sec_tooh_slips\n" +
                "  , ws.trip_reason\n" +
                "  , total_sec\n" +
                "  , stand_length\n" +
                "  , (stand_length*60*60)/total_sec AS feet_hr\n" +
                " FROM ptendrilling.vw_wellstands_tripping ws\n" +
                "   INNER JOIN pten_cmr.cmr_data_usalt7 a ON ws.well_num = a.well_num\n" +
                "       AND ws.stand_start_time BETWEEN a.report_date - 64800 AND a.report_date + 21600\n" +
                "   inner join user_Security.user_mapping_details u on a.well_num = u.well_num AND u.userid = " + getUser_id() + " \n" +
                "   INNER JOIN pten_cmr.rig r ON a.rig_name = r.display_text\n" +
                "   INNER JOIN pten_cmr.rigtype rt ON r.rig_type_id = rt.rig_type_id\n" +
                "   INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                " WHERE ws.well_num IN (:wells);";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells);

        return impalaDbUtilLib.executeNamedQuery(query, namedParameters);

    }

    public Collection getFootagePerDayWells(Date startDate, Date endDate, String county, String state, boolean alias) {

        long starttime = DateTimeUtils.atStartOfDay(DateTimeUtils.toLocalDate(startDate));
        long endtime = DateTimeUtils.dateToLastSecondOfDay(DateTimeUtils.toLocalDate(endDate));

		/*StringBuilder q = new StringBuilder("WITH wellsInRange as (\n" +
                "  SELECT  a.well_num\n" +
                "   , a.well_name\n" +
                "   , a.rig_name\n" +
                "   , a.county\n" +
                "   , a.state_province\n" +
                "   , a.target_formation\n" +
                "   , a.release_time\n" +
                "   , a.current_depth \n" +
                "   , a.preset \n" +
                "   , a.surface_depth\n" +
                "   , a.vertical_depth\n" +
                "   , a.total_depth\n" +
                "   , a.daywork_days\n" +
                "   , avg(a.surface_depth) over (Partition by a.county, a.state_province) as avg_surface_depth\n" +
                "   , a.lateral_end_time" +
                "   , a.curve_end_time" +
                "   , a.vertical_end_time" +
                "   , a.surface_end_time");

        if (null != spudTo && spudTo.equalsIgnoreCase("REL")) {
            q.append(", ((COALESCE(a.lateral_end_time, a.curve_end_time, a.vertical_end_time) - COALESCE(a.spud_time, a.drill_out_from_under_surface_time, a.curve_start_time, a.lateral_start_time)) / 86400.0) - a.paused_days AS spud_to\n");
        } else {
            q.append(", ((a.release_time - COALESCE(a.spud_time, a.drill_out_from_under_surface_time, a.curve_start_time, a.lateral_start_time)) / 86400.0 ) - a.paused_days AS spud_to\n");
        }
        q.append("  FROM user_Security.user_mapping_details u\n" +
                "  INNER JOIN pten_cmr.vw_cmr_data_usalt7_release a on  a.well_num = u.well_num AND u.userid = :user\n" +
                "  INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "  where not exists (select null from pten_cmr.vw_cmr_data_usalt7_release z where a.well_num = z.well_num and a.report_date < z.report_date)\n" +
                "    and a.release_time between :startDate and :endDate\n" +
                "    and UPPER( concat(a.county, ',', a.state_province) ) = UPPER(concat(:county, ',', :state))\n" +
                ")\n" +
                "SELECT ");
        if (alias) {
            q.append("a.well_num as well_name\n");
        } else {
            q.append("a.well_name\n");
        }
        q.append("   , a.rig_name\n" +
				//"   , CONCAT(CONCAT(a.county,','),a.state_province) as county_state" +
				"   , CONCAT(a.county,', ' ,a.state_province) as county_state" +
                "   , a.target_formation\n" +
                "   , a.current_depth - CASE UPPER(a.preset) WHEN 'SURFACE' THEN a.surface_depth WHEN 'VERTICAL' THEN a.vertical_depth ELSE 0 END as footage_made\n" +
                "   , from_timestamp(to_timestamp(COALESCE(a.lateral_end_time, curve_end_time, vertical_end_time, surface_end_time)),'MM/dd/yy') as td_time\n" +
                "   , a.preset\n" +
                "   , CASE UPPER(a.preset) WHEN 'VERTICAL' THEN vertical_depth WHEN 'SURFACE' THEN surface_depth ELSE 0.0 end  as preset_depth\n" +
                "   , cast(a.avg_surface_depth as INT) as avg_surface_depth\n" +
                "   , cast(a.total_depth as INT) as total_depth\n" +
                "   , cast((a.current_depth - CASE UPPER(a.preset) WHEN 'SURFACE' THEN a.surface_depth WHEN 'VERTICAL' THEN a.vertical_depth ELSE 0 END )/ case when a.daywork_days < 1 THEN 1 ELSE a.daywork_days END as INT) as avg_feet_per_day   \n" +
                "   , cast(a.spud_to as DECIMAL(10,2)) as spud_to\n" +
                "  FROM wellsInRange a  \n" +
                "  where a.total_depth is not null\n" +
                "order by a.total_depth desc, CONCAT(CONCAT(a.county,','),a.state_province), a.well_name, a.well_num"
        );*/
		StringBuilder q = new StringBuilder("WITH wellsInRange as (\n" +
                "  SELECT  a.well_num\n" +
                "   , a.well_name\n" +
                "   , a.rig_name\n" +
                "   , a.county\n" +
                "   , a.state_province\n" +
                "   , a.target_formation\n" +
                "   , a.release_time\n" +
                "   , a.current_depth \n" +
                "   , a.preset \n" +
                "   , a.surface_depth\n" +
                "   , a.vertical_depth\n" +
                "   , a.total_depth\n" +
                "   , a.daywork_days\n" +
                "   , avg(a.surface_depth) over (Partition by a.well_num, a.state_province) as avg_surface_depth\n" +
                "   , a.lateral_end_time" +
                "   , a.curve_end_time" +
                "   , a.vertical_end_time" +
                "   , a.surface_end_time");

        //if (null != spudTo && spudTo.equalsIgnoreCase("REL")) {
            q.append(", ((COALESCE(a.lateral_end_time, a.curve_end_time, a.vertical_end_time) - COALESCE(a.spud_time, a.drill_out_from_under_surface_time, a.curve_start_time, a.lateral_start_time)) / 86400.0) - a.paused_days AS spud_to_td\n");
        //} else {
            q.append(", ((a.release_time - COALESCE(a.spud_time, a.drill_out_from_under_surface_time, a.curve_start_time, a.lateral_start_time)) / 86400.0 ) - a.paused_days AS spud_to_rel\n");
        //}
        q.append("  FROM user_Security.user_mapping_details u\n" +
                "  INNER JOIN pten_cmr.vw_cmr_data_usalt7_release a on  a.well_num = u.well_num AND u.userid = :user\n" +
                "  INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "  where not exists (select null from pten_cmr.vw_cmr_data_usalt7_release z where a.well_num = z.well_num and a.report_date < z.report_date)\n" +
                "    and a.release_time between :startDate and :endDate\n" +
                "    and UPPER( concat(a.county, ',', a.state_province) ) = UPPER(concat(:county, ',', :state))\n" +
                ")\n" +
                "SELECT ");
        if (alias) {
            q.append("a.well_num as well_name\n");
        } else {
            q.append("a.well_name\n");
        }
        q.append("   , a.rig_name\n" +
				//"   , CONCAT(CONCAT(a.county,','),a.state_province) as county_state" +
				"   , CONCAT(a.county,', ' ,a.state_province) as county_state" +
                "   , a.target_formation\n" +
                "   , a.current_depth - CASE UPPER(a.preset) WHEN 'SURFACE' THEN a.surface_depth WHEN 'VERTICAL' THEN a.vertical_depth ELSE 0 END as footage_made\n" +
                "   , from_timestamp(to_timestamp(COALESCE(a.lateral_end_time, curve_end_time, vertical_end_time, surface_end_time)),'MM/dd/yy') as td_time\n" +
                "   , a.preset\n" +
                "   , CASE UPPER(a.preset) WHEN 'VERTICAL' THEN vertical_depth WHEN 'SURFACE' THEN surface_depth ELSE 0.0 end  as preset_depth\n" +
                "   , cast(a.avg_surface_depth as INT) as avg_surface_depth\n" +
                "   , cast(a.total_depth as INT) as total_depth\n" +
                "   , cast((a.current_depth - CASE UPPER(a.preset) WHEN 'SURFACE' THEN a.surface_depth WHEN 'VERTICAL' THEN a.vertical_depth ELSE 0 END )/ case when a.daywork_days < 1 THEN 1 ELSE a.daywork_days END as INT) as avg_feet_per_day   \n" +
                "   , cast(a.spud_to_rel as DECIMAL(10,2)) as spud_to_rel\n" +
				"   , cast(a.spud_to_td as DECIMAL(10,2)) as spud_to_td\n" +
                "  FROM wellsInRange a  \n" +
                "  where a.total_depth is not null\n" +
                "order by a.total_depth desc, CONCAT(CONCAT(a.county,','),a.state_province), a.well_name, a.well_num"
        );
		

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("county", county)
                .addValue("state", state)
                .addValue("limit", TOP_10)
                .addValue("startDate", starttime)
                .addValue("endDate", endtime)
                .addValue("user", getUser_id());

        return impalaDbUtilLib.executeNamedQuery(q.toString(), namedParameters);
    }

    public Collection getTopFootagePerDayWells(String region) {

        LocalDate now = LocalDate.now();

        /*String query = "WITH lastNMonthsWellsInArea as (\n" +
                "  SELECT  a.well_num\n" +
                "   , a.well_name\n" +
                "   , a.rig_name\n" +
                "   , a.county\n" +
                "   , a.state_province\n" +
                "   , a.current_depth \n" +
                "   , a.preset \n" +
                "   , a.surface_depth\n" +
                "   ,a.vertical_depth\n" +
                "   ,a.daywork_days \n" +
                "  FROM user_Security.user_mapping_details u\n" +
                "  INNER JOIN pten_cmr.vw_cmr_data_usalt7_release a on  a.well_num = u.well_num AND u.userid = :user\n" +
                "  INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "  where UPPER( rm.region_name) in (:region)\n" +
                "    and not exists (select null from pten_cmr.vw_cmr_data_usalt7_release z where a.well_num = z.well_num and a.report_date < z.report_date)\n" +
                "    and a.release_time >= :startDate\n" +
                "), footageRankedWells AS (\n" +
                "SELECT a.well_name\n" +
                "   , a.rig_name\n" +
                "   , a.county\n" +
                "   , a.state_province\n" +
                "   , (a.current_depth - CASE a.preset WHEN 'Surface' THEN a.surface_depth WHEN 'Vertical' THEN a.vertical_depth ELSE 0 END )/ case when a.daywork_days < 1 THEN 1 ELSE a.daywork_days END as avg_feet_per_day\n" +
                "  FROM lastNMonthsWellsInArea a  \n" +
                ")\n" +
                "select a.well_name\n" +
                "  , a.rig_name\n" +
                "  , a.county\n" +
                "  , a.state_province\n" +
                "  , a.avg_feet_per_day \n" +
                "from footageRankedWells a\n" +
                "where avg_feet_per_day is not null\n" +
                "order by avg_feet_per_day desc\n" +
                "limit :limit";*/
			
	String query = "WITH lastNMonthsWellsInArea as (\n" +
                "  SELECT  a.well_num\n" +
                "   , a.well_name\n" +
                "   , a.rig_name\n" +
                "   , a.current_depth \n" +
                "   , a.preset \n" +
                "   , a.surface_depth\n" +
                "   ,a.vertical_depth\n" +
                "   ,a.daywork_days \n" +
                "   ,tfcc.color_code\n" +
                "   ,a.target_formation\n" +
                "   ,concat('<div>',a.county,', ',a.state_province,'<br>',coalesce(cast(cast(a.total_depth as DECIMAL(10,0)) as string),' '),' ft.</div>') as county_state_depth\n" +
                "  FROM user_Security.user_mapping_details u\n" +
                "  INNER JOIN pten_cmr.vw_cmr_data_usalt7_release a on  a.well_num = u.well_num AND u.userid = :user\n" +
                "  INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "  INNER JOIN pten_cmr.target_formation_color_code tfcc on UPPER(tfcc.target_formation) = UPPER(a.target_formation)\n" +
                "  where UPPER( rm.region_name) in (:region)\n" +
                "    and not exists (select null from pten_cmr.vw_cmr_data_usalt7_release z where a.well_num = z.well_num and a.report_date < z.report_date)\n" +
                "    and a.release_time >= :startDate\n" +
                "), footageRankedWells AS (\n" +
                "SELECT a.well_name\n" +
                "   , a.rig_name\n" +
                "   , a.color_code\n" +
                "   , a.target_formation\n" +
                "   , a.county_state_depth\n" +
                "   , (a.current_depth - CASE a.preset WHEN 'Surface' THEN a.surface_depth WHEN 'Vertical' THEN a.vertical_depth ELSE 0 END )/ case when a.daywork_days < 1 THEN 1 ELSE a.daywork_days END as avg_feet_per_day\n" +
                "  FROM lastNMonthsWellsInArea a  \n" +
                ")\n" +
                "select a.well_name\n" +
                "  , a.rig_name\n" +
                "   , a.color_code\n" +
                "   , a.target_formation\n" +
                "   , a.county_state_depth\n" +
                "  , cast(a.avg_feet_per_day as INT) as avg_feet_per_day\n" +
                "from footageRankedWells a\n" +
                "where avg_feet_per_day is not null\n" +
                "order by avg_feet_per_day desc\n" +
                "limit :limit";			
				//log.info(query.toString());
        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("region", region.toUpperCase())
                .addValue("limit", TOP_10)
                .addValue("startDate", DateTimeUtils.dateToFirstSecondOfDay(now.minusMonths(6)))
                .addValue("user", getUser_id());
        return impalaDbUtilLib.executeNamedQuery(query, namedParameters);
    }

    public Collection getTopFootagePerDayWells(Date startDate, Date endDate, String county, String stateProvince, boolean alias) {

        long starttime = DateTimeUtils.atStartOfDay(DateTimeUtils.toLocalDate(startDate));
        long endtime = DateTimeUtils.dateToLastSecondOfDay(DateTimeUtils.toLocalDate(endDate));

	StringBuilder q = new StringBuilder("WITH lastNMonthsWellsInArea as (\n" +
                "  SELECT  a.well_num\n" +
                "   , a.well_name\n" +
                "   , a.rig_name\n" +
                "   , a.current_depth \n" +
                "   , a.preset \n" +
                "   , a.surface_depth\n" +
                "   ,a.vertical_depth\n" +
                "   ,a.daywork_days \n" +
                "   ,tfcc.color_code\n" +
                "   ,a.target_formation\n" +
		"   ,concat('<div>',a.county,', ',a.state_province,'<br>',coalesce(cast(cast(a.total_depth as DECIMAL(10,0)) as string),' '),' ft.</div>') as county_state_depth\n" +
                "  FROM user_Security.user_mapping_details u\n" +
                "  INNER JOIN pten_cmr.vw_cmr_data_usalt7_release a on  a.well_num = u.well_num AND u.userid = :user\n" +
                "  INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "  INNER JOIN pten_cmr.target_formation_color_code tfcc on UPPER(tfcc.target_formation) = UPPER(a.target_formation)\n" +
                "  where UPPER( concat(a.county, ',', a.state_province) ) = UPPER(concat(:county, ',', :state))\n" +
                "    and not exists (select null from pten_cmr.vw_cmr_data_usalt7_release z where a.well_num = z.well_num and a.report_date < z.report_date)\n" +
                "    and a.release_time >= :startDate\n" +
                "), footageRankedWells AS (\n" +
                "SELECT ");
        if (alias){
            q.append("a.well_num as well_name\n");
        }else {
            q.append("a.well_name\n");
        }
        q.append(
                "   , a.rig_name\n" +
                        "   , a.color_code\n" +
                        "   , a.target_formation\n" +
                        "   , a.county_state_depth\n" +
                        "   , (a.current_depth - CASE a.preset WHEN 'Surface' THEN a.surface_depth WHEN 'Vertical' THEN a.vertical_depth ELSE 0 END )/ case when a.daywork_days < 1 THEN 1 ELSE a.daywork_days END as avg_feet_per_day\n" +
                        "  FROM lastNMonthsWellsInArea a  \n" +
                        ")\n" +
                        "select a.well_name\n" +
                        "  , a.rig_name\n" +
                        "   , a.color_code\n" +
                        "   , a.target_formation\n" +
                        "  , cast(a.avg_feet_per_day as INT) as avg_feet_per_day\n" +
                        "   , a.county_state_depth\n" +
                        "from footageRankedWells a\n" +
                        "where avg_feet_per_day is not null\n" +
                        "order by avg_feet_per_day desc\n" +
                        "limit :limit");						
						log.info(q.toString());

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("county", county)
                .addValue("state", stateProvince)
                .addValue("limit", TOP_10)
                .addValue("startDate", starttime)
                .addValue("endDate", endtime)
                .addValue("user", getUser_id());

        return impalaDbUtilLib.executeNamedQuery(q.toString(), namedParameters);
    }

    public Collection getTopWellsByFootageAndSpudToTDLegend(String region) {

        Collection footageWells = getTopFootagePerDayWells(region);
        Collection spudWells = getSpudToTDWells(region);
        HashMap<String, String> colors = new HashMap<>();
        for (Object o : footageWells) {
            JSONObject jsonObject = (JSONObject) o;
            try {
                colors.put(jsonObject.getString("target_formation").toUpperCase(), jsonObject.getString("color_code"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        for (Object o : spudWells) {
            JSONObject jsonObject = (JSONObject) o;
            try {
                colors.put(jsonObject.getString("target_formation").toUpperCase(), jsonObject.getString("color_code"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        List<JSONObject> coloursList = new ArrayList<>();
        for (Map.Entry<String, String> stringStringEntry : colors.entrySet()) {
            try {
                coloursList.add(new JSONObject().put("target_formation", myUtils.capitalize(stringStringEntry.getKey()))
                        .put("color_code", stringStringEntry.getValue()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return coloursList;
    }

    public Collection getSpudToTDWells(String region) {

        LocalDate now = LocalDate.now();
		/*String query = "WITH lastNMonthsWellsInArea as (\n" +
                "  SELECT a.well_num\n" +
                "  ,((COALESCE(a.lateral_end_time, a.curve_end_time, a.vertical_end_time) - COALESCE(a.spud_time, a.drill_out_from_under_surface_time, a.curve_start_time, a.lateral_start_time)) / 86400.0) - a.paused_days AS spud_to_td_days\n" +
                "  FROM pten_cmr.vw_cmr_data_usalt7_release a\n" +
                "  INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "  where UPPER(rm.region_name) in (:region)\n" +
                "    and not exists (select null from pten_cmr.vw_cmr_data_usalt7_release z where a.well_num = z.well_num and a.report_date < z.report_date)\n" +
                "    and a.report_date >= :startDate\n" +
                "    and release_time is not null\n" +
                "    and ((COALESCE(a.lateral_end_time, a.curve_end_time, a.vertical_end_time) - COALESCE(a.spud_time, a.drill_out_from_under_surface_time, a.curve_start_time, a.lateral_start_time)) / 86400.0) - a.paused_days > 0\n" +
                ")\n" +
                "SELECT a.well_num\n" +
                "  ,a.rig_name\n" +
                "  ,rm.region_name\n" +
                "  ,a.county\n" +
                "  ,a.state_province\n" +
                "  , a.total_depth\n" +
                "  , w.spud_to_td_days \n" +
                "  ,a.target_formation\n" +
                "FROM user_Security.user_mapping_details u\n" +
                "  INNER JOIN pten_cmr.vw_cmr_data_usalt7_release a on a.well_num = u.well_num AND u.userid = :user\n" +
                "  INNER JOIN lastNMonthsWellsInArea w ON a.well_num = w.well_num\n" +
                "  INNER JOIN pten_cmr.regionmap rm  ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "WHERE UPPER(rm.region_name) in (:region)\n" +
                "  and a.previous_well_release_time >= :startDate\n" +
                "  and w.spud_to_td_days > 0\n" +
                "  and a.total_depth > 0\n" +
                "GROUP BY a.well_num\n" +
                "        ,a.rig_name\n" +
                "        ,rm.region_name\n" +
                "        , a.county\n" +
                "        , a.state_province\n" +
                "        , a.total_depth\n" +
                "        ,a.lateral_end_time\n" +
                "        ,a.curve_end_time\n" +
                "        ,a.vertical_end_time\n" +
                "        ,a.spud_time\n" +
                "        ,a.drill_out_from_under_surface_time\n" +
                "        ,a.curve_start_time\n" +
                "        ,a.lateral_start_time\n" +
                "        ,a.paused_days\n" +
                "        ,w.spud_to_td_days\n" +
                "        ,a.target_formation\n" +
                "ORDER BY  w.spud_to_td_days asc\n" +
                "LIMIT :limit"; */
				
	String query = "WITH lastNMonthsWellsInArea as (\n" +
                "  SELECT a.well_num\n" +
                "  ,((COALESCE(a.lateral_end_time, a.curve_end_time, a.vertical_end_time) - COALESCE(a.spud_time, a.drill_out_from_under_surface_time, a.curve_start_time, a.lateral_start_time)) / 86400.0) - a.paused_days AS spud_to_td_days\n" +
                "  FROM pten_cmr.vw_cmr_data_usalt7_release a\n" +
                "  INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "  where UPPER(rm.region_name) in (:region)\n" +
                "    and not exists (select null from pten_cmr.vw_cmr_data_usalt7_release z where a.well_num = z.well_num and a.report_date < z.report_date)\n" +
                "    and a.report_date >= :startDate\n" +
                "    and release_time is not null\n" +
                "    and ((COALESCE(a.lateral_end_time, a.curve_end_time, a.vertical_end_time) - COALESCE(a.spud_time, a.drill_out_from_under_surface_time, a.curve_start_time, a.lateral_start_time)) / 86400.0) - a.paused_days > 0\n" +
                ")\n" +
                "SELECT a.well_num\n" +
                "  ,a.rig_name\n" +
                "  ,rm.region_name\n" +
                "  , cast(w.spud_to_td_days as DECIMAL(10,2)) as spud_to_td_days\n" +
                "  ,a.target_formation\n" +
                "  ,tfcc.color_code\n" +
                "  ,concat('<div>',a.county,', ',a.state_province,'<br>',coalesce(cast(cast(a.total_depth as DECIMAL(10,0)) as string),' '),' ft.</div>') as county_state_depth\n" +
                "FROM user_Security.user_mapping_details u\n" +
                "  INNER JOIN pten_cmr.vw_cmr_data_usalt7_release a on a.well_num = u.well_num AND u.userid = :user\n" +
                "  INNER JOIN lastNMonthsWellsInArea w ON a.well_num = w.well_num\n" +
                "  INNER JOIN pten_cmr.regionmap rm  ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "  INNER JOIN pten_cmr.target_formation_color_code tfcc on UPPER(tfcc.target_formation) = UPPER(a.target_formation)\n" +
                "WHERE UPPER(rm.region_name) in (:region)\n" +
                "  and a.previous_well_release_time >= :startDate\n" +
                "  and w.spud_to_td_days > 0\n" +
                "  and a.total_depth > 0\n" +
                "GROUP BY a.well_num\n" +
                "        ,a.rig_name\n" +
                "        ,rm.region_name\n" +
                "        , a.county\n" +
                "        , a.state_province\n" +
                "        , a.total_depth\n" +
                "        ,a.lateral_end_time\n" +
                "        ,a.curve_end_time\n" +
                "        ,a.vertical_end_time\n" +
                "        ,a.spud_time\n" +
                "        ,a.drill_out_from_under_surface_time\n" +
                "        ,a.curve_start_time\n" +
                "        ,a.lateral_start_time\n" +
                "        ,a.paused_days\n" +
                "        ,w.spud_to_td_days\n" +
                "        ,a.target_formation\n" +
                "        ,tfcc.color_code\n" +
                "ORDER BY  w.spud_to_td_days asc\n" +
                "LIMIT :limit";
				//log.info(query.toString());

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("region", region.toUpperCase())
                .addValue("limit", TOP_10)
                .addValue("startDate", DateTimeUtils.dateToFirstSecondOfDay(now.minusMonths(6)))
                .addValue("user", getUser_id());

        return impalaDbUtilLib.executeNamedQuery(query, namedParameters);

    }

    public Collection getSpudToTDWells(Date startDate, Date endDate, String county, String state, boolean alias) {

        long starttime = DateTimeUtils.atStartOfDay(DateTimeUtils.toLocalDate(startDate));
        long endtime = DateTimeUtils.dateToLastSecondOfDay(DateTimeUtils.toLocalDate(endDate));

		/*StringBuilder q = new StringBuilder("WITH lastNMonthsWellsInArea as (\n" +
                "  SELECT a.well_num\n" +
                "  ,((COALESCE(a.lateral_end_time, a.curve_end_time, a.vertical_end_time) - COALESCE(a.spud_time, a.drill_out_from_under_surface_time, a.curve_start_time, a.lateral_start_time)) / 86400.0) - a.paused_days AS spud_to_td_days\n" +
                "  FROM pten_cmr.vw_cmr_data_usalt7_release a\n" +
                "  INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "  where not exists (select null from pten_cmr.vw_cmr_data_usalt7_release z where a.well_num = z.well_num and a.report_date < z.report_date)\n" +
                "    and a.release_time between :startDate and :endDate\n" +
                "    and UPPER( concat(a.county, ',', a.state_province) ) = UPPER(concat(:county, ',', :state))\n" +
                "    and release_time is not null\n" +
                "    and ((COALESCE(a.lateral_end_time, a.curve_end_time, a.vertical_end_time) - COALESCE(a.spud_time, a.drill_out_from_under_surface_time, a.curve_start_time, a.lateral_start_time)) / 86400.0) - a.paused_days > 0\n" +
                ")\n" +
                "SELECT ");
        if (alias) {
            q.append("a.well_num as well_name");
        } else {
            q.append("a.well_name");
        }

        q.append("  ,a.rig_name\n" +
                "  ,rm.region_name\n" +
                "  ,a.county\n" +
                "  ,a.state_province\n" +
                "  , w.spud_to_td_days \n" +
                "  ,a.target_formation\n" +
                "FROM user_Security.user_mapping_details u\n" +
                "  INNER JOIN pten_cmr.vw_cmr_data_usalt7_release a on a.well_num = u.well_num AND u.userid = :user\n" +
                "  INNER JOIN lastNMonthsWellsInArea w ON a.well_num = w.well_num\n" +
                "  INNER JOIN pten_cmr.regionmap rm  ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "WHERE a.previous_well_release_time between :startDate and :endDate\n" +
                "  and w.spud_to_td_days > 0\n" +
                "  and a.total_depth > 0\n" +
                "GROUP BY a.well_num\n" +
                "        ,a.well_name\n" +
                "        ,a.rig_name\n" +
                "        ,rm.region_name\n" +
                "        , a.county\n" +
                "        , a.state_province\n" +
                "        ,a.lateral_end_time\n" +
                "        ,a.curve_end_time\n" +
                "        ,a.vertical_end_time\n" +
                "        ,a.spud_time\n" +
                "        ,a.drill_out_from_under_surface_time\n" +
                "        ,a.curve_start_time\n" +
                "        ,a.lateral_start_time\n" +
                "        ,a.paused_days\n" +
                "        ,w.spud_to_td_days\n" +
                "        ,a.target_formation\n" +
                "ORDER BY  w.spud_to_td_days asc\n" +
                "LIMIT :limit"); */
				
        StringBuilder q = new StringBuilder("WITH lastNMonthsWellsInArea as (\n" +
                "  SELECT a.well_num\n" +
                "  ,((COALESCE(a.lateral_end_time, a.curve_end_time, a.vertical_end_time) - COALESCE(a.spud_time, a.drill_out_from_under_surface_time, a.curve_start_time, a.lateral_start_time)) / 86400.0) - a.paused_days AS spud_to_td_days\n" +
                "  FROM pten_cmr.vw_cmr_data_usalt7_release a\n" +
                "  INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "  where not exists (select null from pten_cmr.vw_cmr_data_usalt7_release z where a.well_num = z.well_num and a.report_date < z.report_date)\n" +
                "    and a.release_time between :startDate and :endDate\n" +
                "    and UPPER( concat(a.county, ',', a.state_province) ) = UPPER(concat(:county, ',', :state))\n" +
                "    and release_time is not null\n" +
                "    and ((COALESCE(a.lateral_end_time, a.curve_end_time, a.vertical_end_time) - COALESCE(a.spud_time, a.drill_out_from_under_surface_time, a.curve_start_time, a.lateral_start_time)) / 86400.0) - a.paused_days > 0\n" +
                ")\n" +
                "SELECT ");
        if (alias) {
            q.append("a.well_num as well_name");
        } else {
            q.append("a.well_name");
        }

        q.append("  ,a.rig_name\n" +
                "  ,rm.region_name\n" +
                "  ,a.county\n" +
                "  ,a.state_province\n" +
                "  , cast(w.spud_to_td_days as DECIMAL(10,2)) as spud_to_td_days\n" +
                "  ,a.target_formation\n" +
                "FROM user_Security.user_mapping_details u\n" +
                "  INNER JOIN pten_cmr.vw_cmr_data_usalt7_release a on a.well_num = u.well_num AND u.userid = :user\n" +
                "  INNER JOIN lastNMonthsWellsInArea w ON a.well_num = w.well_num\n" +
                "  INNER JOIN pten_cmr.regionmap rm  ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "WHERE a.previous_well_release_time between :startDate and :endDate\n" +
                "  and w.spud_to_td_days > 0\n" +
                "  and a.total_depth > 0\n" +
                "GROUP BY a.well_num\n" +
                "        ,a.well_name\n" +
                "        ,a.rig_name\n" +
                "        ,rm.region_name\n" +
                "        , a.county\n" +
                "        , a.state_province\n" +
                "        ,a.lateral_end_time\n" +
                "        ,a.curve_end_time\n" +
                "        ,a.vertical_end_time\n" +
                "        ,a.spud_time\n" +
                "        ,a.drill_out_from_under_surface_time\n" +
                "        ,a.curve_start_time\n" +
                "        ,a.lateral_start_time\n" +
                "        ,a.paused_days\n" +
                "        ,w.spud_to_td_days\n" +
                "        ,a.target_formation\n" +
                "ORDER BY  w.spud_to_td_days asc\n" +
                "LIMIT :limit");
				
				//log.info(q.toString());

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("county", county)
                .addValue("state", state)
                .addValue("limit", TOP_10)
                .addValue("startDate", starttime)
                .addValue("endDate", endtime)
                .addValue("user", getUser_id());

        return impalaDbUtilLib.executeNamedQuery(q.toString(), namedParameters);
    }

    public Collection getLastProjects(String rig_name, int limit) {

        String query = "\n" +
                "WITH projects as (\n" +
                "SELECT distinct a.job_no  \n" +
                "FROM user_Security.user_mapping_details u\n" +
                "  INNER JOIN pten_cmr.cmr_data_usalt7 a on a.well_num = u.well_num AND u.userid = :user\n" +
                "where a.rig_name in (:rig)\n" +
                "  and release_time is not null\n" +
                "order by a.job_no desc\n" +
                "limit :limit\n" +
                "), cmr as (\n" +
                "SELECT a.rig_name\n" +
                "  ,a.job_no\n" +
                "  ,a.operator_name\n" +
                "  ,a.well_name\n" +
                "  ,a.well_num\n" +
                "  ,a.county\n" +
                "  ,a.state_province\n" +
                "  ,split_part(a.wells_on_pad,'/',1) as order_on_pad\n" +
                "  ,split_part(a.wells_on_pad,'/',2) as wells_on_pad\n" +
                "  ,a.batch_drilling\n" +
                "  ,a.preset\n" +
                "  ,a.spud_time\n" +
                "  ,COALESCE(a.lateral_end_time, curve_end_time, vertical_end_time, surface_end_time) as td_time\n" +
                "  ,a.release_time\n" +
                "  ,a.estimated_release\n" +
                "  ,a.rig_move_time\n" +
                "  ,((COALESCE(a.lateral_end_time, a.curve_end_time, a.vertical_end_time) - COALESCE(a.spud_time, a.drill_out_from_under_surface_time, a.curve_start_time, a.lateral_start_time)) / 86400.0) - a.paused_days AS spud_to_td_days\n" +
                "  ,((a.release_time - COALESCE(a.previous_well_release_time, a.daywork_start_time)) / 86400.0) - a.paused_days AS total_cycle_time_days\n" +
                "FROM user_Security.user_mapping_details u\n" +
                "  INNER JOIN pten_cmr.cmr_data_usalt7 a on a.well_num = u.well_num AND u.userid = :user\n" +
                "  inner join projects p on p.job_no = a.job_no\n" +
                "where a.rig_name in (:rig)\n" +
                "  and release_time is not null\n" +
                "group by\n" +
                "   a.rig_name\n" +
                "  ,a.job_no\n" +
                "  ,a.operator_name\n" +
                "  ,a.well_name\n" +
                "  ,a.well_num\n" +
                "  ,a.county\n" +
                "  ,a.state_province\n" +
                "  ,a.wells_on_pad\n" +
                "  ,a.batch_drilling\n" +
                "  ,a.preset\n" +
                "  ,a.spud_time\n" +
                "  ,a.lateral_end_time\n" +
                "  ,a.curve_end_time\n" +
                "  ,a.vertical_end_time\n" +
                "  ,a.surface_end_time\n" +
                "  ,a.release_time\n" +
                "  ,a.estimated_release\n" +
                "  ,a.rig_move_time\n" +
                "  ,a.drill_out_from_under_surface_time\n" +
                "  ,a.curve_start_time\n" +
                "  ,a.lateral_start_time\n" +
                "  ,a.paused_days\n" +
                "  ,a.previous_well_release_time\n" +
                "  ,a.daywork_start_time\n" +
                "  ,a.paused_days\n" +
                ")select \n" +
                "  a.job_no as project_number\n" +
                "  ,a.operator_name\n" +
                "  ,a.well_name\n" +
                "  ,a.well_num\n" +
                "  ,a.county\n" +
                "  ,a.state_province\n" +
                "  ,a.order_on_pad\n" +
                "  ,a.wells_on_pad\n" +
                "  ,CASE a.batch_drilling WHEN 1 THEN 'Yes' WHEN 0 THEN 'No' ELSE 'No' END AS batch_drilling\n" +
                "  ,a.preset\n" +
                "  ,from_timestamp(to_timestamp(a.spud_time), 'MM/dd/yy') as spud_time\n" +
                "  ,from_timestamp(to_timestamp(a.td_time),'MM/dd/yy') as td_time\n" +
                "  ,from_timestamp(to_timestamp(a.release_time),'MM/dd/yy') as release_time\n" +
                "  ,max(from_timestamp(to_timestamp(a.estimated_release),'MM/dd/yy')) over (Partition by a.well_num) AS estimated_release\n" +
                "  ,avg(a.rig_move_time) over (Partition by a.well_num) / 24.0 as rmct\n" +
                "  ,a.spud_to_td_days\n" +
                "  ,a.total_cycle_time_days\n" +
                "from cmr a\n" +
                "group by a.job_no\n" +
                "  ,a.operator_name\n" +
                "  ,a.well_num\n" +
                "  ,a.well_name\n" +
                "  ,a.county\n" +
                "  ,a.state_province\n" +
                "  ,a.order_on_pad\n" +
                "  ,a.wells_on_pad\n" +
                "  ,a.batch_drilling\n" +
                "  ,a.preset\n" +
                "  ,a.spud_time\n" +
                "  ,a.td_time\n" +
                "  ,a.release_time\n" +
                "  ,a.estimated_release\n" +
                "  ,a.rig_move_time\n" +
                "  ,a.spud_to_td_days\n" +
                "  ,a.total_cycle_time_days\n" +
                "order by a.job_no";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("rig", rig_name)
                .addValue("limit", limit)
                .addValue("user", getUser_id());

        return impalaDbUtilLib.executeNamedQuery(query, namedParameters);

    }


    public Collection getCasingStatus(Collection<Long> wells) {


        String query = "SELECT \n" +
                "    a.well_num\n" +
                "  , a.well_name\n" +
                "  , b.name AS casing_section_name\n" +
                "  , b.size AS casing_size\n" +
                "  , set_depth \n" +
                "  , to_timestamp(b.start_time) as start_time\n" +
                "  , to_timestamp(b.end_time) as end_time\n" +
                "  , (b.end_time - b.start_time ) / 3600 as hours_to_run\n" +
                "  , split_part(a.wells_on_pad,'/',1) as order_on_pad\n" +
                "  , split_part(a.wells_on_pad,'/',2) as wells_on_pad\n" +
                "  , a.projected_total_depth\n" +
                "FROM pten_cmr.cmr_data_usalt7 a \n" +
                "  INNER JOIN user_Security.user_mapping_details u on  a.well_num=u.well_num AND u.userid =:user\n" +
                "  INNER JOIN pten_cmr.cmr_casings_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                "WHERE \n" +
                "  NOT EXISTS (\n" +
                "    SELECT NULL FROM pten_cmr.cmr_data_usalt7 z \n" +
                "    WHERE a.well_num = z.well_num \n" +
                "    AND a.report_date < z.report_date\n" +
                "  )\n" +
                "and a.well_num in (:wells)";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells)
                .addValue("user", getUser_id());

        return impalaDbUtilLib.executeNamedQuery(query, namedParameters);
    }

    public Collection getDepth(Collection<Long> wells) {

        String query = "--dailydepth\n" +
                "WITH WellAliases AS (\n" +
                "SELECT a.well_num\n" +
                "  , a.well_name\n" +
                "  , CONCAT('Well ', CAST(ROW_NUMBER() OVER(ORDER BY a.release_time DESC) AS STRING)) AS well_alias\n" +
                "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_Details u on a.well_num=u.well_num and u.userid=:user\n" +
                "WHERE a.well_num IN (:wells)\n" +
                "   AND NOT EXISTS (\n" +
                "       SELECT NULL\n" +
                "       FROM pten_cmr.cmr_data_usalt7 z inner join user_Security.user_mapping_Details u on z.well_num=u.well_num and u.userid=2910 \n" +
                "       WHERE a.well_num = z.well_num\n" +
                "        AND a.report_date < z.report_date\n" +
                "   )\n" +
                ")SELECT a.well_num\n" +
                "  , a.well_name\n" +
                "  , wa.well_alias                \n" +
                "  , ROW_NUMBER() OVER(PARTITION BY a.well_num ORDER BY a.report_date) AS day_num\n" +
                "  , from_timestamp(to_timestamp(a.report_date), 'MM/dd/yy') as report_date\n" +
                "  , from_timestamp(to_timestamp(a.estimated_release), 'MM/dd/yy') as estimated_release\n" +
                "  , COALESCE(LAG(a.current_depth, 1) OVER(PARTITION BY a.well_num ORDER BY a.report_date), 0.0) AS day_depth\n" +
                "  , from_timestamp(to_timestamp(max(a.report_date) over ()), 'MM/dd/yy') as maximum\n" +
                "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_Details u on a.well_num=u.well_num and u.userid=:user\n" +
                "  INNER JOIN WellAliases wa ON a.well_num = wa.well_num\n" +
                "WHERE a.current_depth > 0.0\n";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("wells", wells)
                .addValue("user", getUser_id());

        return impalaDbUtilLib.executeNamedQuery(query, namedParameters);
    }
}
