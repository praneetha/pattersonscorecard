package com.pten.ui.repo;

import com.flutura.ImpalaRestApiController.RequestHandler;
import com.pten.Utilities.UtilityLibrary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.time.*;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import static java.time.temporal.TemporalAdjusters.firstDayOfYear;

@Component
public class OptionsRepo {
    private static final Logger log = LoggerFactory.getLogger(RequestHandler.class);

    @Autowired
    UtilityLibrary myUtils;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;


    public Collection findAllByReportingFrom(Optional<Date> from, Optional<Date> to) {
        Collection myJSONArray = null;

        Instant startTimeInstant, endTimeInstant;

        LocalDateTime localDateFrom, localDateTo;

        LocalDate thisDate = LocalDate.now();

        if (from.isPresent()){
            startTimeInstant = from.get().toInstant().atZone(ZoneId.systemDefault())
                    .toLocalDate().atStartOfDay()
                    .toInstant(ZoneOffset.UTC);

        } else {
            //Default logic from jan first that morning, up to tonight

            localDateFrom = thisDate.now().with(firstDayOfYear()).atStartOfDay(); // 1983-01-01 00:00...
            startTimeInstant = localDateFrom.toInstant(ZoneOffset.UTC);

        }

        if (to.isPresent()){
            endTimeInstant = to.get().toInstant().atZone(ZoneId.systemDefault())
                    .toLocalDate().atStartOfDay()
                    .toInstant(ZoneOffset.UTC);
        }else {
            endTimeInstant = thisDate.now().atTime(LocalTime.MAX).toInstant(ZoneOffset.UTC);
        }


        try {
            //TODO: handle optional.isPresent()
            long startTime = startTimeInstant.getEpochSecond();
            long endTime = endTimeInstant.getEpochSecond();

            String where;

            where = to.isPresent() ?
                    "WHERE a.report_date between " + startTime + " and " + endTime + "\n"
                    : "WHERE a.report_date >= " + startTime + "\n";

            String query = " SELECT a.operator_name\n" +
                    "                , rm.region_name\n" +
                    "                , MIN(a.report_date) AS min_date\n" +
                    "                , MAX(a.report_date) AS max_date\n" +
                    "                , a.rig_name\n" +
                    "                , a.well_name\n" +
                    "                , rt.display_text AS rig_type\n" +
                    "                , CASE WHEN MAX(a.release_time) IS NOT NULL THEN 0 ELSE 1 END AS active_well\n" +
                    "                , a.well_num\n" +
                    "  FROM pten_cmr.cmr_data_usalt7 a\n" +
                    "                INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                    "                INNER JOIN pten_cmr.rig r ON a.rig_name = r.display_text\n" +
                    "                INNER JOIN pten_cmr.rigtype rt ON r.rig_type_id = rt.rig_type_id\n" +
                    where +
                    "   AND NOT EXISTS (\n" +
                    "                SELECT NULL\n" +
                    "                  FROM pten_cmr.cmr_data_usalt7 b\n" +
                    "                 WHERE a.well_num = b.well_num\n" +
                    "                   AND a.report_date < b.report_date)\n" +
                    "GROUP BY a.operator_name, rm.region_name, a.rig_name, a.well_name, rt.display_text, a.well_num";

            log.info("findAllByReportingFrom: {}", query);

            myJSONArray = myUtils.runSQLGetJSONArray(query);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return myJSONArray;
    }


}
