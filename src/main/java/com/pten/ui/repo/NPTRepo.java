package com.pten.ui.repo;

import com.flutura.ImpalaRestApiController.CerebraAPIDataLayer;
import com.pten.Utilities.DateUtils.DateTimeUtils;
import com.pten.Utilities.DbUtils.ImpalaDbUtilLib;
import com.pten.Utilities.UtilityLibrary;
import com.pten.ui.data.NPT;
import com.pten.ui.data.SubCode;
import com.pten.ui.util.Mapper;
import com.pten.ui.view.npt.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.Collection;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

/**
 * this class should directly call data from dataLayer JSONArray responses.
 * The .Repo layer will serve as optional design in case other kind of mapping
 * is used for the results sets from DB.
 * Keeping methods simple in case this class becomes an interface
 */

@Component
public class NPTRepo {
    private static final Logger log = LoggerFactory.getLogger(NPTRepo.class);

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    CerebraAPIDataLayer cerebraAPIDataLayer;

    @Autowired
    Mapper mapper;

    //TODO: remove
    @Autowired
    UtilityLibrary myUtils;

    @Autowired
    ImpalaDbUtilLib impalaDbUtilLib;

    public int getUser_id() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().
                getAuthentication().getPrincipal();
        return Integer.parseInt(userDetails.getUsername());
    }


    public Collection getNPTPctByRegionSubCodeYQM(String starttime, String endtime, String subCodeText) throws ParseException {

        long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
        long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

        //todo: move queries to C...DataLayer
        String query = "--NPTPctByRegionSubCodeYQM\n" +
                "WITH TopQuery as (\n" +
                "SELECT \n" +
                "YEAR(to_timestamp(b.`from`)) AS npt_year\n" +
                ", MONTH(to_timestamp(b.`from`)) as npt_month\n" +
                ",CASE MONTH(TRUNC(to_timestamp(b.`from`), 'Q')) WHEN 1 THEN 1 WHEN 4 THEN 2 WHEN 7 THEN 3 WHEN 10 THEN 4 END \n" +
                "\tas npt_qtr\n" +
                ",SUM(CASE WHEN b.code = '8' AND UPPER(b.sub_code) NOT LIKE '%Z%' "+((subCodeText != null) ? " AND c.display_text = :subCode\n" : "")+" THEN b.hours ELSE 0 END) \n" +
                "\tOVER(PARTITION BY YEAR(to_timestamp(b.`from`)), MONTH(to_timestamp(b.`from`))) / SUM(CASE WHEN UPPER(b.sub_code) NOT LIKE '%Z%' THEN b.hours ELSE 0 END) \n" +
                "\t\tOVER(PARTITION BY YEAR(to_timestamp(b.`from`)), MONTH(to_timestamp(b.`from`))) \n" +
                "\t\t\tAS npt_month_pct\n" +
                ",SUM(CASE WHEN b.code = '8' AND UPPER(b.sub_code) NOT LIKE '%Z%' "+((subCodeText != null) ? " AND c.display_text = :subCode\n" : "")+" THEN b.hours ELSE 0 END) \n" +
                "\tOVER(PARTITION BY YEAR(to_timestamp(b.`from`))) / SUM(CASE WHEN UPPER(b.sub_code) NOT LIKE '%Z%' THEN b.hours ELSE 0 END) \n" +
                "\t\tOVER(PARTITION BY YEAR(to_timestamp(b.`from`))) \n" +
                "\t\t\tAS npt_year_pct\n" +
                ",SUM(CASE WHEN b.code = '8' AND UPPER(b.sub_code) NOT LIKE '%Z%'"+((subCodeText != null) ? " AND c.display_text = :subCode\n" : "")+" THEN b.hours ELSE 0 END) \n" +
                "\tOVER(PARTITION BY YEAR(to_timestamp(b.`from`)), CASE MONTH(TRUNC(to_timestamp(b.`from`), 'Q')) WHEN 1 THEN 1 WHEN 4 THEN 2 WHEN 7 THEN 3 WHEN 10 THEN 4 END) / SUM(CASE WHEN UPPER(b.sub_code) NOT LIKE '%Z%' THEN b.hours ELSE 0 END) \n" +
                "\t\tOVER(PARTITION BY YEAR(to_timestamp(b.`from`)), CASE MONTH(TRUNC(to_timestamp(b.`from`), 'Q')) WHEN 1 THEN 1 WHEN 4 THEN 2 WHEN 7 THEN 3 WHEN 10 THEN 4 END) \n" +
                "\t\t\tAS npt_qtr_pct\n" +
                ",SUM(CASE WHEN b.code = '8' AND UPPER(b.sub_code) NOT LIKE '%Z%' "+((subCodeText != null) ? " AND c.display_text = :subCode\n" : "")+" THEN b.hours ELSE 0 END) \n" +
                "\tOVER() / SUM(CASE WHEN UPPER(b.sub_code) NOT LIKE '%Z%' THEN b.hours ELSE 0 END) \n" +
                "\t\tOVER() \n" +
                "\t\t\tAS npt_overall_pct\n" +
                "from pten_cmr.cmr_time_code_data_usalt7 b\n" +
                "\tINNER JOIN pten_cmr.cmr_data_usalt7 a ON b.cmr_data_id = a.cmr_data_id inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                "LEFT OUTER JOIN pten_cmr.CMRDecode c ON b.code = c.cmr_code AND split_part(b.sub_code, '.', 2) = c.cmr_sub_code AND c.cmr_sub_sub_code IS NULL\n" +
                "WHERE u.userid=" + getUser_id() + " and  b.`from` BETWEEN :start AND :end\n" +
                ")\n" +
                "SELECT DISTINCT npt_year\n" +
                "\t,npt_month\n" +
                "\t,npt_qtr\n" +
                "\t,round(cast(npt_month_pct AS decimal(17,15))*100,2) AS value\n" +
                "\t,npt_qtr_pct\n" +
                "\t,npt_year_pct\n" +
                "\t,round(cast(npt_overall_pct AS decimal(17,15))*100,2) AS overall_npt_percentage \n" +
                "\t,CASE npt_month WHEN 1 THEN 'January' WHEN 2 THEN 'February' WHEN 3 THEN 'March' WHEN 4 THEN 'April' WHEN 5 THEN 'May' WHEN 6 THEN 'June' WHEN 7 THEN 'July' WHEN 8 THEN 'August' WHEN 9 THEN 'September' WHEN 10 THEN 'October' WHEN 11 THEN 'November' WHEN 12 THEN 'December' END AS month_long_text\n" +
                "\t,CASE npt_month WHEN 1 THEN 'Jan' WHEN 2 THEN 'Feb' WHEN 3 THEN 'Mar' WHEN 4 THEN 'Apr' WHEN 5 THEN 'May' WHEN 6 THEN 'Jun' WHEN 7 THEN 'Jul' WHEN 8 THEN 'Aug' WHEN 9 THEN 'Sep' WHEN 10 THEN 'Oct' WHEN 11 THEN 'Nov' WHEN 12 THEN 'Dec' END AS month_short_text\n" +
                "    --,CASE npt_qtr WHEN 1 THEN 'Q1' WHEN 2 THEN 'Q2' WHEN 3 THEN 'Q3' WHEN 4 THEN 'Q4' END as npt_qtr_text\n" +
                "FROM TopQuery\n" +
                "ORDER BY npt_year, npt_qtr, npt_month;";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("start", startTime)
                .addValue("end", endTime)
                .addValue("subCode", subCodeText);

        return impalaDbUtilLib.executeNamedQuery(query, namedParameters);
    }

    public Collection getNPTHoursByRegionSubCodeYQM(String starttime, String endtime, String subCodeText) throws ParseException {
        long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
        long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);
        subCodeText = ((subCodeText != null)?subCodeText.trim().toUpperCase():null);

        String query = "WITH TopLevel AS(\n" +
                "SELECT \n" +
                "\tcd.display_text\n" +
                "\t, cd.display_color\n" +
                "\t, YEAR(to_timestamp(CASE WHEN b.`from` < :start THEN :start ELSE b.`from` END)) AS npt_year\n" +
                "\t, FLOOR((MONTH(to_timestamp(CASE WHEN b.`from` < :start THEN :start ELSE b.`from` END)) - 1) / 3) + 1 AS npt_qtr\n" +
                "\t, MONTH(to_timestamp(CASE WHEN b.`from` < :start THEN :start ELSE b.`from` END)) AS npt_month\n" +
                "\t, SUM(b.hours) AS 'value' \n" +
                "\t, cd.cmr_sub_code AS 'sub_code' \n" +
                "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                "\tINNER JOIN pten_cmr.cmr_time_code_data_usalt7 b \n" +
                "\t\tON a.cmr_data_id = b.cmr_data_id \n" +
                "\t\t\tINNER JOIN pten_cmr.CMRDecode cd \n" +
                "\t\t\tON b.code = cd.cmr_code \n" +
                "\t\t\tAND split_part(b.sub_code, '.', 2) = cd.cmr_sub_code \n" +
                "\t\t\tAND cd.cmr_sub_sub_code IS NULL \n" +
                "WHERE u.userid=" + getUser_id() + " and  cd.cmr_code = '8' \n" +
                "\tAND b.sub_code NOT LIKE '%z%' \n" +
                "\tAND (b.`from` BETWEEN :start AND :end OR b.`to` BETWEEN :start AND :end)\n" +
                ((subCodeText != null) ? " AND UPPER(cd.display_text) = :subCode \n" : "") +
                "group by cd.display_text \n" +
                "\t,cd.display_color\n" +
                "\t,npt_year\n" +
                "\t,npt_qtr\n" +
                "\t,npt_month\n" +
                "\t,sub_code\n" +
                ")\n" +
                "SELECT distinct display_text \n" +
                "\t,display_color\n" +
                "\t,npt_year\n" +
                "\t,npt_qtr\n" +
                "\t,npt_month\n" +
                "\t,CASE npt_month WHEN 1 THEN 'January' WHEN 2 THEN 'February' WHEN 3 THEN 'March' WHEN 4 THEN 'April' WHEN 5 THEN 'May' WHEN 6 THEN 'June' WHEN 7 THEN 'July' WHEN 8 THEN 'August' WHEN 9 THEN 'September' WHEN 10 THEN 'October' WHEN 11 THEN 'November' WHEN 12 THEN 'December' END AS month_long_text" +
                "\t,CASE npt_month WHEN 1 THEN 'Jan' WHEN 2 THEN 'Feb' WHEN 3 THEN 'Mar' WHEN 4 THEN 'Apr' WHEN 5 THEN 'May' WHEN 6 THEN 'Jun' WHEN 7 THEN 'Jul' WHEN 8 THEN 'Aug' WHEN 9 THEN 'Sep' WHEN 10 THEN 'Oct' WHEN 11 THEN 'Nov' WHEN 12 THEN 'Dec' END AS month_short_text\n" +
                "\t,value" +
                "\t,sub_code" +
                " FROM TopLevel ORDER BY npt_year, npt_month; ";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("start", startTime)
                .addValue("end", endTime)
                .addValue("subCode", subCodeText);

        return impalaDbUtilLib.executeNamedQuery(query, namedParameters);
    }


    public Collection getNPTHoursByRegionSubCode(String starttime, String endtime, String subCodeText) throws ParseException {
        long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
        long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);
        subCodeText = ((subCodeText != null)?subCodeText.trim().toUpperCase():null);

        String query = "SELECT rm.region_name\n" +
                ", cd.display_text\n" +
                ", cd.display_color\n" +
                ", cd.cmr_sub_code AS 'sub_code'\n" +
                ", SUM(b.hours) AS 'value' \n" +
                "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                "\tINNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                "\tINNER JOIN pten_cmr.RegionMap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit) \n" +
                "\tINNER JOIN pten_cmr.CMRDecode cd ON b.code = cd.cmr_code \n" +
                "\t\tAND cd.cmr_sub_sub_code IS NULL \n" +
                "\t\tAND split_part(b.sub_code, '.', 2) = cd.cmr_sub_code \n" +
                "WHERE u.userid= " + getUser_id() + " and  (b.`to` BETWEEN :start AND :end OR b.`from` BETWEEN :start AND :end) \n" +
                "AND UPPER(b.sub_code) NOT LIKE '%Z%' AND b.code = '8'" +
                ((subCodeText != null) ? " AND UPPER(cd.display_text) = :subCode\n" : "") +
                "GROUP BY rm.region_name, cd.display_text, cd.display_color, cd.cmr_sub_code;";

            SqlParameterSource namedParameters = new MapSqlParameterSource()
                    .addValue("start", startTime)
                    .addValue("end", endTime)
                    .addValue("subCode", subCodeText);

            return impalaDbUtilLib.executeNamedQuery(query, namedParameters);

    }

    public Collection NPTPercentageByRegion(String starttime, String endtime, String subCodeText) throws ParseException {
        long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
        long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);
        subCodeText = ((subCodeText!=null)?subCodeText.trim().toUpperCase():null);

        String query = "--percent NptByRegionSubcode\n" +
                "WITH TopQuery AS ( \n" +
                "SELECT rm.region_name \n" +
                ", SUM(CASE WHEN b.code = '8' AND UPPER(b.sub_code) NOT LIKE '%Z%' "+((subCodeText != null) ? " AND UPPER(c.display_text) = :subCode\n" : "")+" THEN b.hours ELSE 0 END) OVER(PARTITION BY rm.region_name) / SUM(b.hours) OVER(PARTITION BY rm.region_name) AS 'value'\n" +
                ", SUM(CASE WHEN b.code = '8' AND UPPER(b.sub_code) NOT LIKE '%Z%' "+((subCodeText != null) ? " AND UPPER(c.display_text) = :subCode\n" : "")+" THEN b.hours ELSE 0 END) OVER() / SUM(b.hours) OVER() AS npt_overall_percentage \n" +
                "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                "\tINNER JOIN pten_cmr.cmr_time_code_data_usalt7 b \n" +
                "\tON a.cmr_data_id = b.cmr_data_id \n" +
                "\t\tINNER JOIN pten_cmr.RegionMap rm \n" +
                "\t\tON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)  \n" +
                "   LEFT OUTER JOIN pten_cmr.CMRDecode c ON b.code = c.cmr_code AND split_part(b.sub_code, '.', 2) = c.cmr_sub_code AND c.cmr_sub_sub_code IS NULL\n" +
                "WHERE u.userid= " + getUser_id() + " and a.report_date BETWEEN :start AND :end" +
                ") SELECT region_name\n" +
                ", round(cast(value AS decimal(17,15))*100,2) AS value\n" +
                ", round(cast(npt_overall_percentage AS decimal(17,15))*100,2) AS overall_npt_pct   \n" +
                "FROM TopQuery  \n" +
                "GROUP BY region_name , value, npt_overall_percentage;";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("start", startTime)
                .addValue("end", endTime)
                .addValue("subCode", subCodeText);

        return impalaDbUtilLib.executeNamedQuery(query, namedParameters);

    }

    public Collection<NPTHrPct> getNPTHoursBySCRL(String starttime, String endtime, String[] riglist) throws ParseException {

        long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
        long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);
        String rigList = myUtils.arrayToInClause(riglist);

        String query = "SELECT a.rig_name \n" +
                ", c.display_text\t , b.code \n" +
                "\t , split_part(b.sub_code, '.', 2) AS sub_code \n" +
                "\t , SUM(CASE WHEN b.code = '8' THEN b.hours ELSE 0 END) AS 'value' \n" +
                "    , c.display_color \n" +
                "   FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                "\tLEFT OUTER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                "\t\tAND b.code = '8' \n" +
                "    LEFT OUTER JOIN pten_cmr.CMRDecode c \n" +
                "    \tON b.code = c.cmr_code \n" +
                "    \tAND split_part(b.sub_code, '.', 2) = c.cmr_sub_code \n" +
                "    \tAND c.cmr_sub_sub_code IS NULL \n" +
                " WHERE u.userid= " + getUser_id() + " and  a.rig_name IN ( " + rigList + ") \n" +
                " \tAND (b.`from` BETWEEN  " + startTime + " AND  " + endTime + " OR b.`to` BETWEEN  " + startTime + " AND  " + endTime + ") \n" +
                " \tAND b.code = '8' \n" +
                " \tAND b.sub_code NOT LIKE '%Z%' \n" +
                " GROUP BY rig_name \n" +
                "\t\t, display_text \n" +
                "\t\t, b.code \n" +
                "\t\t, sub_code \n" +
                "        , display_color\n" +
                " ORDER BY a.rig_name, sub_code;";

        //log.info("{}",query);

        Collection<NPTHrPct> result = jdbcTemplate.query(
                query,
                (rs, rowNum) -> new NPTHrPct(
                        rs.getString("rig_name"), //constant code
                        rs.getString("display_text"),
                        rs.getString("code"),
                        rs.getString("sub_code"),
                        rs.getDouble("value"),
                        rs.getString("display_color")
                )
        ).stream().collect(toList());

        return result;

    }

    public Collection<NPTHrPct> getNPTPctBySCRL(String starttime, String endtime, String[] riglist) throws ParseException {
        long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
        long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);
        String rigList = myUtils.arrayToInClause(riglist);

        String query = "--NPTPctFromRL\n" +
                "WITH TopQuery AS (\n" +
                "\tSELECT a.rig_name\n" +
                "\t\t,SUM(CASE WHEN b.code = '8' THEN b.hours ELSE 0 END) OVER(PARTITION BY a.rig_name) / SUM(b.hours) OVER(PARTITION BY a.rig_name) AS 'value'\n" +
                //"\t\t,SUM(CASE WHEN b.code = '8' THEN b.hours ELSE 0 END) OVER() / SUM(b.hours) OVER() AS overall_npt_pct \n" +
                "\tFROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                "\t\tINNER JOIN pten_cmr.cmr_time_code_data_usalt7 b \n" +
                "\t\t\tON a.cmr_data_id = b.cmr_data_id \n" +
                "\tWHERE u.userid=" + getUser_id() + " and  a.rig_name IN (" + rigList + " ) \n" +
                "\t\tAND (b.`from` BETWEEN  " + startTime + " AND " + endTime + " OR b.`to` BETWEEN " + startTime + " AND " + endTime + " )\n" +
                ")SELECT DISTINCT " +
                " tq.rig_name " +
                ", tq.value " +
                " FROM TopQuery tq\n" +
                " ORDER by tq.value DESC;";

        Collection<NPTHrPct> result = jdbcTemplate.query(
                query,
                (rs, rowNum) -> new NPTHrPct(
                        rs.getString("rig_name"),
                        rs.getDouble("value")
                )
        ).stream().collect(toList());

        return result;
    }

    public Collection<NPTBySCRLYM> getNPTPctBySCRLYM(String starttime, String endtime, String[] riglist) throws ParseException {
        long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
        long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);
        String rigList = myUtils.arrayToInClause(riglist);

        String query = "--NPTPctBySCRYLM\n" +
                "WITH TopLevel AS ( \n" +
                "SELECT a.rig_name \n" +
                "\t , YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END)) AS npt_year \n" +
                "\t , MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END)) AS npt_month \n" +
                "\t , SUM(CASE WHEN b.code = '8' AND UPPER(b.sub_code) NOT LIKE '%Z%' THEN b.hours ELSE 0 END) OVER(PARTITION BY a.rig_name, YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END)), MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END))) / SUM(b.hours) OVER(PARTITION BY a.rig_name, YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END)), MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END))) AS 'value' \n" +
                "\t , SUM(CASE WHEN b.code = '8' AND UPPER(b.sub_code) NOT LIKE '%Z%'  THEN b.hours ELSE 0 END) OVER(PARTITION BY a.rig_name, YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END))) / SUM(b.hours) OVER(PARTITION BY a.rig_name, YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END))) AS annual_npt_percentage \n" +
                "\t ,SUM(CASE WHEN b.code = '8' AND UPPER(b.sub_code) NOT LIKE '%Z%'  THEN b.hours ELSE 0 END) OVER(PARTITION BY a.rig_name) / SUM(b.hours) OVER(PARTITION BY a.rig_name) AS overall_npt_percentage \n" +
                "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                "\tLEFT OUTER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                "WHERE u.userid=" + getUser_id() + " and  a.rig_name IN (" + rigList + ") AND (b.`from` BETWEEN " + startTime + " AND " + endTime + " OR b.`to` BETWEEN " + startTime + " AND " + endTime + ")) \n" +
                "SELECT DISTINCT rig_name, npt_year, npt_month, CASE npt_month WHEN 1 THEN 'January' WHEN 2 THEN 'February' WHEN 3 THEN 'March' WHEN 4 THEN 'April' WHEN 5 THEN 'May' WHEN 6 THEN 'June' WHEN 7 THEN 'July' WHEN 8 THEN 'August' WHEN 9 THEN 'September' WHEN 10 THEN 'October' WHEN 11 THEN 'November' WHEN 12 THEN 'December' END AS month_long_text, CASE npt_month WHEN 1 THEN 'Jan' WHEN 2 THEN 'Feb' WHEN 3 THEN 'Mar' WHEN 4 THEN 'Apr' WHEN 5 THEN 'May' WHEN 6 THEN 'Jun' WHEN 7 THEN 'Jul' WHEN 8 THEN 'Aug' WHEN 9 THEN 'Sep' WHEN 10 THEN 'Oct' WHEN 11 THEN 'Nov' WHEN 12 THEN 'Dec' END AS month_short_text, value , annual_npt_percentage, overall_npt_percentage FROM TopLevel ORDER BY npt_year, npt_month;";

        //log.info("{}",query);

        Collection<NPTBySCRLYM> result = jdbcTemplate.query(
                query,
                (rs, rowNum) -> new NPTBySCRLYM(
                        rs.getString("rig_name"),
                        rs.getInt("npt_year"),
                        rs.getInt("npt_month"),
                        rs.getString("month_long_text"),
                        rs.getString("month_short_text"),
                        rs.getDouble("value"),
                        rs.getDouble("annual_npt_percentage"),
                        rs.getDouble("overall_npt_percentage")
                )
        ).stream().collect(toList());

        return result;
    }


    public Collection<NPTBySCRLYM> getNPTHoursBySCRLYM(String starttime, String endtime, String[] riglist) throws ParseException {

        //myDataLayer.NPTHoursBySCRLYM(starttime, endtime, riglist.split(",")).toString();

        long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
        long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);
        String rigList = myUtils.arrayToInClause(riglist);

        String query = "--NPTHoursBySCRLYM\n" +
                "WITH TopLevel AS (" +
                " SELECT a.rig_name \n" +
                //",to_timestamp(CASE WHEN b.`from` < 1514764800 THEN b.`to` ELSE b.`from` END) AS start_time" +
                "	 , YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END)) AS npt_year \n" +
                "	 , MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END)) AS npt_month \n" +
                "	 , b.code \n" +
                "	 , split_part(b.sub_code, '.', 2) AS sub_code \n" +
                "	 , SUM(CASE WHEN b.code = '8' THEN b.hours ELSE 0 END) AS 'value' \n" +
                "    , c.display_color \n " +
                "  FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                "	LEFT OUTER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id AND b.code = '8' \n" +
                "    LEFT OUTER JOIN pten_cmr.CMRDecode c ON b.code = c.cmr_code AND split_part(b.sub_code, '.', 2) = c.cmr_sub_code AND c.cmr_sub_sub_code IS NULL \n" +
                " WHERE u.userid=" + getUser_id() + " and  a.rig_name IN (" + rigList + ") AND (b.`from` BETWEEN " + startTime + " AND " + endTime + " OR b.`to` BETWEEN " + startTime + " AND " + endTime + ") AND b.code = '8' AND b.sub_code NOT LIKE '%Z%' \n" +
                "   group by rig_name " +
                //",start_time " +
                ",npt_year, npt_month, code, sub_code, display_color\n" +
                " )" +
                "  SELECT DISTINCT rig_name\n" +
                //",start_time" +
                "   ,npt_year\n" +
                "   ,npt_month\n" +
                "   ,CASE npt_month WHEN 1 THEN 'January' WHEN 2 THEN 'February' WHEN 3 THEN 'March' WHEN 4 THEN 'April' WHEN 5 THEN 'May' WHEN 6 THEN 'June' WHEN 7 THEN 'July' WHEN 8 THEN 'August' WHEN 9 THEN 'September' WHEN 10 THEN 'October' WHEN 11 THEN 'November' WHEN 12 THEN 'December' END AS month_long_text, CASE npt_month WHEN 1 THEN 'Jan' WHEN 2 THEN 'Feb' WHEN 3 THEN 'Mar' WHEN 4 THEN 'Apr' WHEN 5 THEN 'May' WHEN 6 THEN 'Jun' WHEN 7 THEN 'Jul' WHEN 8 THEN 'Aug' WHEN 9 THEN 'Sep' WHEN 10 THEN 'Oct' WHEN 11 THEN 'Nov' WHEN 12 THEN 'Dec' END AS month_short_text\n" +
                "   ,code\n" +
                "   ,sub_code\n" +
                "   ,value\n" +
                "   ,display_color \n" +
                "FROM TopLevel ORDER BY rig_name, npt_year, npt_month, sub_code;";

        //log.info("{}",query);

        Collection<NPTBySCRLYM> result = jdbcTemplate.query(
                query,
                (rs, rowNum) -> new NPTBySCRLYM(
                        rs.getString("rig_name"),
                        rs.getInt("npt_year"),
                        rs.getInt("npt_month"),
                        rs.getString("month_long_text"),
                        rs.getString("month_short_text"),
                        rs.getString("code"),
                        rs.getString("sub_code"),
                        rs.getDouble("value"),
                        rs.getString("display_color")
//                        rs.getTimestamp("start_time")
                )
        ).stream().collect(toList());

        return result;
    }

    public Collection getNPTPercentByRYQM(String starttime, String endtime, String subCodeText, int rigTypeId) throws ParseException {
        long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
        long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

        String query = "--NPTPercentByRYQM\n" +
                "        WITH TopQuery AS (\n" +
                "                SELECT rm.region_name\n" +
                "                , rt.rig_master_type_id\n" +
                "                , YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN " + startTime + " ELSE b.`from` END)) AS npt_year\n" +
                "                , MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN " + startTime + " ELSE b.`from` END)) AS npt_month\n" +
                "                , ((cast(from_timestamp(TRUNC(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN " + startTime + " ELSE b.`from` END), 'Q'), 'MM') AS int)-1)/3)+1 AS npt_qtr\n" +
                "                , SUM(CASE WHEN b.code = '8' AND UPPER(b.sub_code) NOT LIKE '%Z%' "+((subCodeText != null) ? " AND c.display_text = '" + subCodeText + "' \n" : "")+ " THEN b.hours ELSE 0 END) OVER(PARTITION BY rm.region_name, " + ((rigTypeId == -99)?"": "rt.rig_type_id, ") + " YEAR(to_timestamp(CASE WHEN b.`from` < " + endTime + " THEN " + endTime + " ELSE b.`from` END)), MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN " + startTime + " ELSE b.`from` END))) / SUM(b.hours) OVER(PARTITION BY rm.region_name, YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN " + startTime + " ELSE b.`from` END)), MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN " + startTime + " ELSE b.`from` END))) AS npt_month_pct\n" +
                " FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num and u.userid=" + getUser_id() + "\n" +
                "        INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id\n" +
                "        INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "        INNER JOIN pten_cmr.rig r ON a.rig_name = r.display_text\n" +
                "        INNER JOIN pten_cmr.rigtype rt ON r.rig_type_id = rt.rig_type_id\n" +
                "        INNER JOIN pten_cmr.rigmastertype rmt ON rmt.rig_master_type_id = rt.rig_master_type_id\n" +
                "        LEFT OUTER JOIN pten_cmr.CMRDecode c ON b.code = c.cmr_code AND split_part(b.sub_code, '.', 2) = c.cmr_sub_code AND c.cmr_sub_sub_code IS NULL\n" +
                "        WHERE b.`from` BETWEEN " + startTime + " AND " + endTime + " OR b.`to` BETWEEN " + startTime + " AND " + endTime + "\n" +
                ")\n" +
                "        select distinct region_name\n" +
                "                ,npt_year\n" +
                "                ,npt_month\n" +
                "                ,npt_qtr\n" +
                "                ,npt_month_pct\n" +
                "                ,CASE npt_month WHEN 1 THEN 'January' WHEN 2 THEN 'February' WHEN 3 THEN 'March' WHEN 4 THEN 'April' WHEN 5 THEN 'May' WHEN 6 THEN 'June' WHEN 7 THEN 'July' WHEN 8 THEN 'August' WHEN 9 THEN 'September' WHEN 10 THEN 'October' WHEN 11 THEN 'November' WHEN 12 THEN 'December' END AS month_long_text\n" +
                "                ,CASE npt_month WHEN 1 THEN 'Jan' WHEN 2 THEN 'Feb' WHEN 3 THEN 'Mar' WHEN 4 THEN 'Apr' WHEN 5 THEN 'May' WHEN 6 THEN 'Jun' WHEN 7 THEN 'Jul' WHEN 8 THEN 'Aug' WHEN 9 THEN 'Sep' WHEN 10 THEN 'Oct' WHEN 11 THEN 'Nov' WHEN 12 THEN 'Dec' END as month_short_text\n" +
                "        FROM TopQuery\n" +
                ((rigTypeId == -99)?"": "           WHERE rig_master_type_id = " + rigTypeId) + "\n" +
                "        ORDER BY region_name, npt_year, npt_month;";

        Collection<NPTByRYQM> result = jdbcTemplate.query(
                query,
                (rs, rowNum) -> new NPTByRYQM(
                        rs.getString("region_name"),
                        rs.getInt("npt_year"),
                        rs.getInt("npt_qtr"),
                        rs.getInt("npt_month"),
                        rs.getDouble("npt_month_pct"),
                        rs.getString("month_long_text"),
                        rs.getString("month_short_text")
                )
        ).stream().collect(toList());

        return result;

    }

    public Collection getNPTHoursByRYQM(String starttime, String endtime, String subCodeText) throws ParseException {

        long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
        long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

        String query = "--NPTHoursByRYQM\n" +
                "WITH TopQuery AS (\n" +
                "SELECT rm.region_name\n" +
                ", YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN " + startTime + " ELSE b.`from` END)) AS npt_year\n" +
                ", MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN " + startTime + " ELSE b.`from` END)) AS npt_month\n" +
                ", FLOOR((MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN " + startTime + " ELSE b.`from` END)) - 1) / 3) + 1 AS npt_qtr\n" +
                ", SUM(b.hours) AS npt_hours \n" +
                "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                "INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                "INNER JOIN pten_cmr.RegionMap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit) \n" +
                "LEFT OUTER JOIN pten_cmr.cmrdecode cmrd ON b.code = cmrd.cmr_code AND split_part(b.sub_code, '.', 2) = cmrd.cmr_sub_code AND cmrd.cmr_sub_sub_code IS NULL\n" +
                "WHERE u.userid=" + getUser_id() + " and  b.code = '8' \n" +
                "AND UPPER(b.sub_code) NOT LIKE '%Z%' AND (b.`from` BETWEEN " + startTime + " AND " + endTime + " OR b.`to` BETWEEN " + startTime + " AND " + endTime + " ) \n" +
                ((subCodeText != null) ? " AND cmrd.display_text = '" + subCodeText + "' \n" : "") +
                "GROUP BY rm.region_name\n" +
                ",npt_year\n" +
                ",npt_month\n" +
                ",npt_qtr\n" +
                ")select region_name\n" +
                ",npt_year\n" +
                ",npt_month\n" +
                ",npt_hours\n" +
                ",npt_qtr\n " +
                ",CASE npt_month WHEN 1 THEN 'January' WHEN 2 THEN 'February' WHEN 3 THEN 'March' WHEN 4 THEN 'April' WHEN 5 THEN 'May' WHEN 6 THEN 'June' WHEN 7 THEN 'July' WHEN 8 THEN 'August' WHEN 9 THEN 'September' WHEN 10 THEN 'October' WHEN 11 THEN 'November' WHEN 12 THEN 'December' END AS month_long_text" +
                ",CASE npt_month WHEN 1 THEN 'Jan' WHEN 2 THEN 'Feb' WHEN 3 THEN 'Mar' WHEN 4 THEN 'Apr' WHEN 5 THEN 'May' WHEN 6 THEN 'Jun' WHEN 7 THEN 'Jul' WHEN 8 THEN 'Aug' WHEN 9 THEN 'Sep' WHEN 10 THEN 'Oct' WHEN 11 THEN 'Nov' WHEN 12 THEN 'Dec' END as month_short_text\n" +
                "FROM TopQuery\n" +
                "ORDER BY region_name, npt_year, npt_month;";

        Collection<NPTByRYQM> result = jdbcTemplate.query(
                query,
                (rs, rowNum) -> new NPTByRYQM(
                        rs.getString("region_name"),
                        rs.getInt("npt_year"),
                        rs.getInt("npt_qtr"),
                        rs.getInt("npt_month"),
                        rs.getString("month_long_text"),
                        rs.getString("month_short_text"),
                        rs.getDouble("npt_hours"),
                        "Other"
                )
        ).stream().collect(toList());

        return result;

    }

    public Collection getNPTHoursByYQM(String starttime, String endtime, int rigTypeId, String subCodeText) throws ParseException {

        long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
        long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);
        subCodeText = ((subCodeText!=null)?subCodeText.trim().toUpperCase(): null);

        String query = "--NPTHoursByYQM\n" +
                "WITH TopQuery AS (\n" +
                "SELECT rm.region_name\n" +
                ", YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN " + startTime + " ELSE b.`from` END)) AS npt_year\n" +
                ", MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN " + startTime + " ELSE b.`from` END)) AS npt_month\n" +
                ", FLOOR((MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN " + startTime + " ELSE b.`from` END)) - 1) / 3) + 1 AS npt_qtr\n" +
                ", SUM(b.hours) AS npt_hours \n" +
                ", coalesce(cmrd.display_color, '#bab0ac') AS display_color\n" +
                ", split_part(b.sub_code, '.', 2) AS sub_code\n" +
                ", coalesce(cmrd.display_text, 'Other') AS display_text\n" +
                "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                "INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                "INNER JOIN pten_cmr.RegionMap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit) \n" +
                "INNER JOIN pten_cmr.rig r ON a.rig_name = r.display_text " +
                "INNER JOIN pten_cmr.rigtype rt ON r.rig_type_id = rt.rig_type_id " +
                "LEFT OUTER JOIN pten_cmr.cmrdecode cmrd ON b.code = cmrd.cmr_code AND split_part(b.sub_code, '.', 2) = cmrd.cmr_sub_code AND cmrd.cmr_sub_sub_code IS NULL\n" +
                "WHERE u.userid=" + getUser_id() + " and  b.code = '8' AND UPPER(b.sub_code) NOT LIKE '%Z%' AND (b.`from` BETWEEN " + startTime + " AND " + endTime + " OR b.`to` BETWEEN " + startTime + " AND " + endTime + ") \n" +
                ((rigTypeId == -99)?"": "AND rt.rig_type_id = " + rigTypeId) + "\n" +
                ((subCodeText != null) ? " AND UPPER(cmrd.display_text) = '" + subCodeText + "' \n" : "") +
                "GROUP BY rm.region_name\n" +
                ",npt_year\n" +
                ",npt_month\n" +
                ",npt_qtr\n" +
                ",sub_code\n" +
                ",display_color\n" +
                ",display_text" +
                ")select region_name\n" +
                ",sub_code\n" +
                ",display_color\n" +
                ",display_text\n" +
                ",npt_year\n" +
                ",npt_month\n" +
                ",npt_hours\n" +
                ",npt_qtr\n " +
                ",CASE npt_month WHEN 1 THEN 'January' WHEN 2 THEN 'February' WHEN 3 THEN 'March' WHEN 4 THEN 'April' WHEN 5 THEN 'May' WHEN 6 THEN 'June' WHEN 7 THEN 'July' WHEN 8 THEN 'August' WHEN 9 THEN 'September' WHEN 10 THEN 'October' WHEN 11 THEN 'November' WHEN 12 THEN 'December' END AS month_long_text" +
                ",CASE npt_month WHEN 1 THEN 'Jan' WHEN 2 THEN 'Feb' WHEN 3 THEN 'Mar' WHEN 4 THEN 'Apr' WHEN 5 THEN 'May' WHEN 6 THEN 'Jun' WHEN 7 THEN 'Jul' WHEN 8 THEN 'Aug' WHEN 9 THEN 'Sep' WHEN 10 THEN 'Oct' WHEN 11 THEN 'Nov' WHEN 12 THEN 'Dec' END as month_short_text\n" +
                "FROM TopQuery\n" +
                "ORDER BY region_name, npt_year, npt_month;";

        Collection<NPTByRYQM> result = jdbcTemplate.query(
                query,
                (rs, rowNum) -> new NPTByRYQM(
                        rs.getString("region_name"),
                        rs.getString("sub_code"),
                        rs.getString("display_color"),
                        rs.getString("display_text"),
                        rs.getInt("npt_year"),
                        rs.getInt("npt_qtr"),
                        rs.getInt("npt_month"),
                        rs.getDouble("npt_hours"),
                        rs.getString("month_long_text"),
                        rs.getString("month_short_text")
                )
        ).stream().collect(toList());

        return result;


    }

    public Collection getNPTHoursByRYQMperRegion(String region, String starttime, String endtime) {

        Collection<NPTByRYQM> hoursByRyqm = mapper.unmarshall(cerebraAPIDataLayer.NPTHoursByRYQM(starttime, endtime), new NPTByRYQM());

        return hoursByRyqm.stream()
                .filter(f -> f.getRegion_name().equals(region))
                .collect(Collectors.toList());
    }


    public Collection getNPTHoursOverall(String[] riglist, String starttime, String endtime) throws ParseException {

        long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
        long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);
        String rigList = myUtils.arrayToInClause(riglist);

        String query = "SELECT c.display_text" +
                ",b.code" +
                ",SUM(b.hours) AS npt_sub_code_hours" +
                ",c.display_color" +
                ",split_part(b.sub_code, '.', 2) AS sub_code " +
                " FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num " +
                " INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id " +
                " INNER JOIN pten_cmr.CMRDecode c ON c.cmr_sub_sub_code IS NULL " +
                " AND b.code = c.cmr_code " +
                " AND c.cmr_sub_code = split_part(b.sub_code, '.', 2)" +
                " WHERE u.userid=" + getUser_id() + " and  a.rig_name IN (" + rigList + " ) AND b.code = '8'" +
                " AND a.report_date BETWEEN " + startTime + " AND " + endTime +
                " GROUP BY c.display_text, sub_code, c.display_color, b.code" +
                " ORDER BY npt_sub_code_hours DESC;";

        Collection<NPTBySCRL> result = jdbcTemplate.query(
                query,
                (rs, rowNum) -> new NPTBySCRL(
                        rs.getString("code"), //constant code
                        rs.getString("sub_code"),
                        rs.getDouble("npt_sub_code_hours"),
                        rs.getString("display_color"),
                        rs.getString("display_text")
                )
        ).stream().collect(toList());

        //result.forEach(t -> log.info("{}", t.toString()));

        return result;
    }

    public Collection getNPTHoursByRig(String region, String starttime, String endtime, int rigTypeId, String subCodeText) throws ParseException {
        long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
        long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);
        region = region.trim().toUpperCase();
        subCodeText = ((subCodeText != null)?subCodeText.trim().toUpperCase():null);

        String query = "WITH RigsInRegion AS (\n" +
                "SELECT DISTINCT z.rig_name\n" +
                "  FROM  pten_cmr.cmr_data_usalt7 z inner join user_Security.user_mapping_details u on  z.well_num=u.well_num \n" +
                "                INNER JOIN pten_cmr.regionmap rmz ON UPPER(rmz.contractor_business_unit) = UPPER(z.contractor_business_unit)\n" +
                "                INNER JOIN pten_cmr.cmr_time_code_data_usalt7 y ON z.cmr_data_id = y.cmr_data_id\n" +
                "                INNER JOIN pten_cmr.rig r ON z.rig_name = r.display_text\n" +
                "                INNER JOIN pten_cmr.rigtype rt ON r.rig_type_id = rt.rig_type_id\n" +
                "WHERE u.userid=" + getUser_id() + " and  (y.`from` BETWEEN :start AND :end OR y.`to` BETWEEN :start AND :end) " +
                "   AND UPPER(rmz.region_name) = :region" +
                ((rigTypeId == -99)?"": "\nAND rt.rig_type_id = :rigTypeId\n") +
                "), RigNPT AS (\n" +
                "SELECT a.rig_name\n" +
                "                , cmrd.display_color\n" +
                "                , cmrd.display_text\n" +
                "                , SUM(b.hours) OVER(PARTITION BY a.rig_name) AS overall_npt_hours\n" +
                "                , SUM(b.hours) OVER(PARTITION BY a.rig_name, cmrd.display_color) AS sub_code_npt_hours\n" +
                "                ,SUM(b.hours) OVER(PARTITION BY a.rig_name) /SUM(b.hours) OVER()  as npt_percent \n" +
                "                ,split_part(b.sub_code, \".\", 2) as sub_code\n" +
                "  FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num  \n" +
                "                INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id\n" +
                "                INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "                INNER JOIN pten_cmr.cmrdecode cmrd ON b.code = cmrd.cmr_code AND split_part(b.sub_code, '.', 2) = cmrd.cmr_sub_code AND cmrd.cmr_sub_sub_code IS NULL\n" +
                "                INNER JOIN pten_cmr.rig r ON a.rig_name = r.display_text\n" +
                "                INNER JOIN pten_cmr.rigtype rt ON r.rig_type_id = rt.rig_type_id\n" +
                "                INNER JOIN pten_cmr.rigmastertype rmt ON rmt.rig_master_type_id = rt.rig_master_type_id\n" +
                "WHERE u.userid=" + getUser_id() + " and  UPPER(rm.region_name) = :region" +
                "   AND b.code = '8'\n" +
                "   AND b.sub_code NOT LIKE '%z%'\n" +
                "   AND (b.`from` BETWEEN :start AND :end OR b.`to` BETWEEN :start AND :end) " +
                ((subCodeText != null) ? "\nAND UPPER(cmrd.display_text) = :subCode\n" : "") +
                ((rigTypeId == -99)?"": "\nAND rt.rig_type_id = :rigTypeId\n") +
                ")\n" +
                "SELECT DISTINCT rir.rig_name\n" +
                "                , rnpt.display_color\n" +
                "                , rnpt.display_text\n" +
                "                , rnpt.sub_code\n" +
                "                , npt_percent\n" +
                "                , COALESCE(rnpt.overall_npt_hours, 0.0) AS overall_npt_hours\n" +
                "                , COALESCE(rnpt.sub_code_npt_hours, 0.0) AS value\n" +
                "  FROM RigsInRegion rir\n" +
                "                LEFT OUTER JOIN RigNPT rnpt ON rir.rig_name = rnpt.rig_name\n" +
                "ORDER BY COALESCE(overall_npt_hours, 0.0) DESC;";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("region", region)
                .addValue("start", startTime)
                .addValue("end", endTime)
                .addValue("rigTypeId", rigTypeId)
                .addValue("subCode", subCodeText);

        return impalaDbUtilLib.executeNamedQuery(query, namedParameters);

    }

    public Collection getNPTPercentByRig(String region, String starttime, String endtime, int rigTypeId, String subCodeText ) throws ParseException {
        long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
        long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);
        region = region.trim().toUpperCase();
        subCodeText = ((subCodeText != null)?subCodeText.trim().toUpperCase():null);

        String query = "--NPTRegionRigPercent\n" +
                "SELECT a.rig_name\n" +
                ", round(cast(SUM(CASE WHEN b.code = '8' AND UPPER(b.sub_code) NOT LIKE '%Z%' "+((subCodeText != null) ? " AND UPPER(c.display_text) = :subCode\n" : "")+" THEN b.hours ELSE 0 END) / SUM(b.hours) AS decimal(17,15))*100,2) AS value \n" +
                "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num  \n" +
                "INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                "INNER JOIN pten_cmr.RegionMap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit) \n" +
                "INNER JOIN pten_cmr.rig r ON a.rig_name = r.display_text \n" +
                "INNER JOIN pten_cmr.rigtype rt ON r.rig_type_id = rt.rig_type_id \n" +
                "INNER JOIN pten_cmr.rigmastertype rmt ON rmt.rig_master_type_id = rt.rig_master_type_id \n" +
                "LEFT OUTER JOIN pten_cmr.CMRDecode c ON b.code = c.cmr_code AND split_part(b.sub_code, '.', 2) = c.cmr_sub_code AND c.cmr_sub_sub_code IS NULL \n" +
                "WHERE u.userid= " + getUser_id() + " and  UPPER(rm.region_name) in (:region) \n" +
                ((rigTypeId == -99)?"": "\nAND rt.rig_master_type_id = :rigTypeId \n") +
                "AND (b.`from` BETWEEN :start AND :end OR b.`to` BETWEEN :start AND :end)" +
                "GROUP BY a.rig_name ORDER BY value DESC;";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("region", region)
                .addValue("start", startTime)
                .addValue("end", endTime)
                .addValue("rigTypeId", rigTypeId)
                .addValue("subCode", subCodeText);

        return impalaDbUtilLib.executeNamedQuery(query, namedParameters);

    }

    public Collection getNPTHoursByRigType(String region, String starttime, String endtime, String subCodeText) throws ParseException {

        long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
        long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);
        region = (region != null) ? region.toUpperCase(): null;
        subCodeText = (subCodeText != null) ? subCodeText.toUpperCase(): null;

        String query = "--regional hours per rig type (region, from, to ) \n " +
                "WITH TopQuery AS ( " +
                "SELECT rmt.rig_master_type_name AS rig_name" +
                ", cmrd.display_color " +
                ", cmrd.display_text " +
                ", SUM(b.hours) OVER(PARTITION BY rt.display_text) AS npt_hours " +
                ", SUM(b.hours) OVER(PARTITION BY rt.display_text, cmrd.cmr_sub_code) AS npt_sub_code_hours " +
                ",split_part(b.sub_code, '.', 2) AS sub_code " +
                "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num " +
                "   INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id " +
                "   INNER JOIN pten_cmr.rig r ON a.rig_name = r.display_text " +
                "   INNER JOIN pten_cmr.rigtype rt ON r.rig_type_id = rt.rig_type_id " +
                "   INNER JOIN pten_cmr.rigmastertype rmt ON rmt.rig_master_type_id = rt.rig_master_type_id\n" +
                "   INNER JOIN pten_cmr.cmrdecode cmrd ON b.code = cmrd.cmr_code AND split_part(b.sub_code, '.', 2) = cmrd.cmr_sub_code AND cmrd.cmr_sub_sub_code IS NULL " +
                "   INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit) " +
                "WHERE u.userid=" + getUser_id() + " and  " + ((region!= null)? "UPPER(rm.region_name) in (:region)\n AND " : "") +
                ((subCodeText != null) ? "UPPER(cmrd.display_text) = :subCode\n AND " : "") +
                "b.code = '8' " +
                "AND b.sub_code NOT LIKE '%z%' " +
                "AND (b.`from` BETWEEN :start AND :end OR b.`to` BETWEEN :start AND :end) " +
                ") " +
                "SELECT DISTINCT rig_name, 8 as code, sub_code, display_text, display_color, npt_hours, npt_sub_code_hours AS value, npt_hours AS overall_npt_pct\n" +
                "FROM TopQuery " +
                "ORDER BY npt_hours, rig_name, sub_code, npt_hours ASC;";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("region", region)
                .addValue("start", startTime)
                .addValue("end", endTime)
                .addValue("subCode", subCodeText);

        return impalaDbUtilLib.executeNamedQuery(query, namedParameters);
    }

    public Collection getNPTPercentByRigType(String region, String starttime, String endtime, String subCodeText) throws ParseException {
        long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
        long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);
        region = (region != null) ? region.toUpperCase(): null;
        subCodeText = (subCodeText != null) ? subCodeText.toUpperCase(): null;
        String query = "--NPTRegionRigTypeNPTPercent\n" +
                "WITH TopQuery AS (\n" +
                "SELECT rmt.rig_master_type_name as rig_type\n" +
                ", rt.display_color\n" +
                ", SUM(CASE WHEN b.code = '8' AND UPPER(b.sub_code) NOT LIKE '%Z%' " + ((subCodeText != null) ? " AND UPPER(cd.display_text) = :subCode" : "") + " THEN b.hours ELSE 0 END)  OVER(PARTITION BY rt.rig_type_id) / SUM(b.hours) OVER(PARTITION BY rt.rig_type_id) AS npt_percent\n" +
                ", SUM(CASE WHEN b.code = '8' AND UPPER(b.sub_code) NOT LIKE '%Z%' THEN b.hours ELSE 0 END) OVER() / SUM(b.hours) OVER() AS overall_npt_pct \n" +
                "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num  \n" +
                " INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                " INNER JOIN pten_cmr.RegionMap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit) \n" +
                " INNER JOIN pten_cmr.Rig r ON r.display_text = a.rig_name \n" +
                " INNER JOIN pten_cmr.RigType rt ON r.rig_type_id = rt.rig_type_id \n" +
                " INNER JOIN pten_cmr.rigmastertype rmt ON rmt.rig_master_type_id = rt.rig_master_type_id\n" +
                " LEFT OUTER JOIN pten_cmr.CMRDecode cd \n" +
                "  ON b.code = cd.cmr_code \n" +
                "  AND cd.cmr_sub_sub_code IS NULL \n" +
                "  AND split_part(b.sub_code, '.', 2) = cd.cmr_sub_code \n" +
                "WHERE u.userid= " + getUser_id() + " and  "+ ((region!= null)? " UPPER(rm.region_name) = :region\n AND " : "") +
                "(b.`from` BETWEEN :start AND :end OR b.`to` BETWEEN :start AND :end)" +
                ") SELECT DISTINCT rig_type AS rig_name, round(cast(npt_percent AS decimal(17,15))*100,2) AS value, '' AS display_color, overall_npt_pct FROM TopQuery\n" +
                "ORDER By value, rig_name DESC;";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("region", region)
                .addValue("start", startTime)
                .addValue("end", endTime)
                .addValue("subCode", subCodeText);

        return impalaDbUtilLib.executeNamedQuery(query, namedParameters);
    }

    public Object getNPTHoursByRegionRigMonthAverageSort(String region, String starttime, String endtime) throws ParseException {

        long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
        long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

        String query = "WITH TopQuery AS (\n" +
                "SELECT a.rig_name \n" +
                "\t,YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END)) AS npt_year\n" +
                "\t,MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END)) AS npt_month\n" +
                "\t, FLOOR((MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN " + startTime + " ELSE b.`from` END)) - 1) / 3) + 1 AS npt_qtr\n" +
                "\t,SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%Z%' THEN b.hours ELSE 0 END) OVER(PARTITION BY rig_name, YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END))\n" +
                "\t,MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END))) / SUM(b.hours) OVER(PARTITION BY rig_name, YEAR(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END))\n" +
                "\t,MONTH(to_timestamp(CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END))) AS rig_npt_percentage\n" +
                "\t,SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%Z%' THEN b.hours ELSE 0 END) OVER(PARTITION BY rig_name) / SUM(b.hours) OVER(PARTITION BY rig_name) AS rig_sort_order \n" +
                "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num   \n" +
                "\tINNER JOIN pten_cmr.cmr_time_code_data_usalt7 b \n" +
                "\tON a.cmr_data_id = b.cmr_data_id \n" +
                "\t\tINNER JOIN pten_cmr.RegionMap rm \n" +
                "\t\tON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit) \n" +
                "WHERE u.userid=" + getUser_id() + " and  UPPER(rm.region_name) = '" + region.toUpperCase() + "' \n" +
                " AND CASE WHEN b.`from` < " + startTime + " THEN b.`to` ELSE b.`from` END BETWEEN " + startTime + " AND " + endTime + " \n" +
                ")SELECT DISTINCT npt_year\n" +
                ",npt_month\n" +
                ",npt_qtr\n" +
                ",rig_name\n" +
                ",rig_npt_percentage\n" +
                ",rig_sort_order\n" +
                ",CASE npt_month WHEN 1 THEN 'January' WHEN 2 THEN 'February' WHEN 3 THEN 'March' WHEN 4 THEN 'April' WHEN 5 THEN 'May' WHEN 6 THEN 'June' WHEN 7 THEN 'July' WHEN 8 THEN 'August' WHEN 9 THEN 'September' WHEN 10 THEN 'October' WHEN 11 THEN 'November' WHEN 12 THEN 'December' END AS month_long_text\n" +
                ",CASE npt_month WHEN 1 THEN 'Jan' WHEN 2 THEN 'Feb' WHEN 3 THEN 'Mar' WHEN 4 THEN 'Apr' WHEN 5 THEN 'May' WHEN 6 THEN 'Jun' WHEN 7 THEN 'Jul' WHEN 8 THEN 'Aug' WHEN 9 THEN 'Sep' WHEN 10 THEN 'Oct' WHEN 11 THEN 'Nov' WHEN 12 THEN 'Dec' END AS month_short_text\n" +
                "FROM TopQuery ORDER BY rig_sort_order, rig_name, npt_year, npt_month;";


        Collection<NPTByRYQM> result = jdbcTemplate.query(
                query,
                (rs, rowNum) -> new NPTByRYQM(
                        rs.getString("display_text"),
                        rs.getString("display_color"),
                        rs.getInt("npt_year"),
                        rs.getInt("npt_qtr"),
                        rs.getInt("npt_month"),
                        rs.getString("month_long_text"),
                        rs.getString("month_short_text"),
                        rs.getDouble("npt_hours")

                )
        ).stream().collect(toList());

        result.forEach(t -> log.debug("{}", t.toString()));

        return result;
    }


    public Collection getNPTPercentByRYM(String region, String starttime, String endtime, int rigTypeId, String subCodeText) throws ParseException {

        long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);
        LocalDate minusSixMonths = DateTimeUtils.toLocalDate(endtime).minusMonths(5);
        long startTime = DateTimeUtils.atStartOfDay(minusSixMonths); //minus 6 months //DateTimeUtils.dateToFirstDayOfMonth(starttime);
        endTime = endTime - (86400);
        region = region.trim().toUpperCase();
        subCodeText = ((subCodeText!=null)?subCodeText.trim().toUpperCase(): null);

        String query = "--NPTPercentByRYM\n" +
                "WITH TopQuery AS (\n" +
                "SELECT a.rig_name\n" +
                "  ,YEAR(to_timestamp(a.report_date)) AS npt_year\n" +
                "  ,MONTH(to_timestamp(a.report_date)) AS npt_month\t\n" +
                "  ,SUM(CASE WHEN b.code = '8' AND UPPER(b.sub_code) NOT LIKE '%Z%' "+((subCodeText != null) ? " AND UPPER(c.display_text) = :subCode" : "")+" THEN b.hours ELSE 0 END) / SUM(b.hours) AS value \n" +
                "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num  \n" +
                "    INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                "    INNER JOIN pten_cmr.RegionMap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "    INNER JOIN pten_cmr.Rig r ON r.display_text = a.rig_name\n"+
                "    INNER JOIN pten_cmr.RigType rt ON r.rig_type_id = rt.rig_type_id\n"+
                "    INNER JOIN pten_cmr.rigmastertype rmt ON rmt.rig_master_type_id = rt.rig_master_type_id\n" +
                "    LEFT OUTER JOIN pten_cmr.CMRDecode c ON b.code = c.cmr_code AND split_part(b.sub_code, '.', 2) = c.cmr_sub_code AND c.cmr_sub_sub_code IS NULL\n" +
                "WHERE u.userid=" + getUser_id() + " and  UPPER(rm.region_name) in (:region) \n" +
                "  AND (b.`from` BETWEEN :start AND :end OR b.`to` BETWEEN :start AND :end)\n" +
                ((rigTypeId == -99)?"": "\t AND rt.rig_master_type_id = :rigtypeid\n") +
                "group by rig_name, npt_year, npt_month\n" +
                ")SELECT rig_name\n" +
                "  ,npt_year\n" +
                "  ,npt_month\n" +
                "  ,round(cast(value AS decimal(17,15))*100,2) AS value\t\n" +
                "  ,CONCAT(CASE npt_month WHEN 1 THEN 'Jan' WHEN 2 THEN 'Feb' WHEN 3 THEN 'Mar' WHEN 4 THEN 'Apr' WHEN 5 THEN 'May' WHEN 6 THEN 'Jun' WHEN 7 THEN 'Jul' WHEN 8 THEN 'Aug' WHEN 9 THEN 'Sep' WHEN 10 THEN 'Oct' WHEN 11 THEN 'Nov' WHEN 12 THEN 'Dec' END, ' ', cast(npt_year as varchar(4))) as month_year_text\n" +
                "FROM TopQuery \n" +
                "ORDER BY npt_year, npt_month, rig_name;";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("region", region)
                .addValue("start", startTime)
                .addValue("end", endTime)
                .addValue("rigtypeid", rigTypeId)
                .addValue("subCode", subCodeText);

        return impalaDbUtilLib.executeNamedQuery(query, namedParameters);
    }

    //todo: overload to process starttime
    //todo: removed unused parameter
    public Collection getNPTHoursByRYM(String region, String starttime, String endtime, int rigTypeId, String subCodeText) throws ParseException {

        long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);
        LocalDate minusSixMonths = DateTimeUtils.toLocalDate(endtime).minusMonths(5);
        long startTime = DateTimeUtils.dateToLastSecondOfDay(minusSixMonths); //minus 6 months //DateTimeUtils.dateToFirstDayOfMonth(starttime);

        region = ((region!= null)?region.trim().toUpperCase():null);
        subCodeText = ((subCodeText!= null)?subCodeText.trim().toUpperCase():null);


        String query = "--nptregionhoursbyrym\n" +
                "WITH TopQuery AS (\n" +
                "SELECT a.rig_name\n" +
                "\t, YEAR(to_timestamp(CASE WHEN b.`from` < :start THEN :start ELSE b.`from` END)) AS npt_year\n" +
                "\t, MONTH(to_timestamp(CASE WHEN b.`from` < :start THEN :start ELSE b.`from` END)) AS npt_month\n" +
                "\t, SUM(b.hours) AS npt_hours \n" +
                "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num \n" +
                "\tINNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                "\tINNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit) \n" +
                "\tINNER JOIN pten_cmr.Rig r ON r.display_text = a.rig_name\n"+
                "\tINNER JOIN pten_cmr.RigType rt ON r.rig_type_id = rt.rig_type_id\n"+
                "\tLEFT OUTER JOIN  pten_cmr.cmrdecode cmrd ON b.code = cmrd.cmr_code AND split_part(b.sub_code, '.', 2) = cmrd.cmr_sub_code \n" +
                "WHERE u.userid=" + getUser_id() + " and  b.code = '8' AND UPPER(b.sub_code) NOT LIKE '%Z%' AND (b.`from` BETWEEN " + startTime + " AND " + endTime + " OR b.`to` BETWEEN " + startTime + " AND " + endTime + ") \n" +
                "\tAND cmrd.cmr_sub_sub_code IS NULL\n" +
                "\tAND UPPER(rm.region_name) = :region \n" +
                ((subCodeText != null) ? "\tAND UPPER(cmrd.display_text) = :subCode\n" : "") +
                ((rigTypeId == -99)?"": "\tAND rt.rig_type_id = :rigtypeid") + "\n" +
                "GROUP BY rig_name\n" +
                "\t,npt_year\n" +
                "\t,npt_month\n" +
                "ORDER BY npt_year\n" +
                "\t,npt_month\n" +
                ")SELECT rig_name\n" +
                ",npt_year\n" +
                ",npt_month\n" +
                ",npt_hours AS value\n" +
                ",CASE npt_month WHEN 1 THEN 'January' WHEN 2 THEN 'February' WHEN 3 THEN 'March' WHEN 4 THEN 'April' WHEN 5 THEN 'May' WHEN 6 THEN 'June' WHEN 7 THEN 'July' WHEN 8 THEN 'August' WHEN 9 THEN 'September' WHEN 10 THEN 'October' WHEN 11 THEN 'November' WHEN 12 THEN 'December' END AS month_long_text\n" +
                ",CASE npt_month WHEN 1 THEN 'Jan' WHEN 2 THEN 'Feb' WHEN 3 THEN 'Mar' WHEN 4 THEN 'Apr' WHEN 5 THEN 'May' WHEN 6 THEN 'Jun' WHEN 7 THEN 'Jul' WHEN 8 THEN 'Aug' WHEN 9 THEN 'Sep' WHEN 10 THEN 'Oct' WHEN 11 THEN 'Nov' WHEN 12 THEN 'Dec' END AS month_short_text\n" +
                ",CONCAT(CASE npt_month WHEN 1 THEN 'Jan' WHEN 2 THEN 'Feb' WHEN 3 THEN 'Mar' WHEN 4 THEN 'Apr' WHEN 5 THEN 'May' WHEN 6 THEN 'Jun' WHEN 7 THEN 'Jul' WHEN 8 THEN 'Aug' WHEN 9 THEN 'Sep' WHEN 10 THEN 'Oct' WHEN 11 THEN 'Nov' WHEN 12 THEN 'Dec' END, ' ', cast(npt_year as varchar(4))) as month_year_text\n" +
                "from TopQuery tq\n" +
                "order by tq.npt_year, tq.npt_month";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("region", region)
                .addValue("start", startTime)
                .addValue("end", endTime)
                .addValue("rigtypeid", rigTypeId)
                .addValue("subCode", subCodeText);

        return impalaDbUtilLib.executeNamedQuery(query, namedParameters);
    }

    public Collection getNPTPercentByTimegroups(String starttime, String endtime, String[] regions, String groupType) throws ParseException {

        long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
        long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

        String regionList = myUtils.arrayToInClause(regions);

        String query = "WITH AllChunks AS ( \n" +
                getIntervalQuery(groupType) +
                ", a.report_date\n" +
                "  FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num AND u.userid = " +  getUser_id() + " \n" +
                "WHERE a.report_date BETWEEN " + startTime + " AND " + endTime + "\n" +
                "), FunkyChunks AS (\n" +
                "SELECT ROW_NUMBER() OVER(ORDER BY chunk_start) AS chunk_index\n" +
                "       , report_date\n" +
                "       , chunk_start\n" +
                "       , COALESCE(lead(chunk_start, 1) OVER(ORDER BY chunk_start) - 1, " + endTime + ") AS chunk_end\n" +
                "  FROM AllChunks\n" +
                "GROUP BY chunk_start, report_date\n" +
                "), RegionChunkPcts AS (\n" +
                "SELECT rm.region_name\n" +
                "       , tw.report_date\n" +
                "       , tw.chunk_start\n" +
                "       , SUM(CASE WHEN b.code = '8' AND UPPER(b.sub_code) NOT LIKE '%Z%' THEN b.hours ELSE 0 END) OVER(PARTITION BY rm.region_name, tw.chunk_start) / SUM(b.hours) OVER(PARTITION BY rm.region_name, tw.chunk_start) AS chunk_pct\n" +
                "  FROM pten_cmr.cmr_time_code_data_usalt7 b\n" +
                "       INNER JOIN pten_cmr.cmr_data_usalt7 a ON a.cmr_data_id = b.cmr_data_id inner join user_Security.user_mapping_details u on  a.well_num=u.well_num AND u.userid = " + getUser_id() + " \n" +
                "       INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "       INNER JOIN FunkyChunks tw ON b.`to` BETWEEN tw.chunk_start AND tw.chunk_end\n" +
                "WHERE  UPPER(rm.region_name) IN (" + regionList.toUpperCase() + ")\n" +
                ")\n" +
                "SELECT DISTINCT region_name, to_timestamp(chunk_start) AS chunk_start, chunk_pct\n" +
                "  FROM RegionChunkPcts\n" +
                "ORDER BY chunk_start, region_name;";

//        log.info("{}",query);
        Collection<NPTByTimeGroup> result = jdbcTemplate.query(
                query,
                (rs, rowNum) -> new NPTByTimeGroup(
                        rs.getString("region_name"),
                        rs.getDate("chunk_start"),
                        rs.getDouble("chunk_pct"),
                        Double.NaN,
                        groupType
                )
        ).stream().collect(toList());

        result.forEach(t -> log.debug("{}", t.toString()));

        return result;

    }



    public Collection getNPTHoursByTimegroups(String starttime, String endtime, String[] regions, String groupType) throws ParseException {
        long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
        long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

        String regionList = myUtils.arrayToInClause(regions);

        String query = "--getNPTHoursByTimegroups\n" +
                "WITH AllChunks AS ( \n" +
                getIntervalQuery(groupType) +
                ",a.report_date\n" +
                "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num AND u.userid = " + getUser_id() + "  \n" +
                "WHERE a.report_date BETWEEN " + startTime + " AND " + endTime + " \n" +
                "), FunkyChunks AS ( \n" +
                "SELECT ROW_NUMBER() OVER(ORDER BY chunk_start) AS chunk_index \n" +
                "   , report_date\t, chunk_start \n" +
                "   , COALESCE(lead(chunk_start, 1) OVER(ORDER BY chunk_start) - 1, " + endTime + ") AS chunk_end\n" +
                "  FROM AllChunks \n" +
                " GROUP BY chunk_start, report_date\n" +
                ") \n" +
                "SELECT rm.region_name \n" +
                "\t, to_timestamp(fc.chunk_start) as chunk_start\n" +
                "\t, cd.display_color \n" +
                "\t, cd.display_text \n" +
                "\t, cd.cmr_sub_code \n" +
                "\t, SUM(b.hours) AS chunk_code_hours \n" +
                "  FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num AND u.userid = " + getUser_id() + " \n" +
                "\tINNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                "\tINNER JOIN FunkyChunks fc ON b.`to` BETWEEN fc.chunk_start AND COALESCE(fc.chunk_end, " + endTime + ")\n" +
                "\tINNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit) \n" +
                "\tINNER JOIN pten_cmr.cmrdecode cd ON b.code = cd.cmr_code AND split_part(b.sub_code, '.', 2) = cd.cmr_sub_code AND cd.cmr_sub_sub_code IS NULL \n" +
                " WHERE b.code = '8' \n" +
                "   AND UPPER(rm.region_name) IN (" + regionList.toUpperCase() + ") \n" +
                " GROUP BY rm.region_name, fc.chunk_start, cd.display_color, cd.display_text, cd.cmr_sub_code \n" +
                " ORDER BY rm.region_name, fc.chunk_start, cd.display_text;";

//        log.info("{}",query);
        Collection<NPTByTimeGroup> result = jdbcTemplate.query(
                query,
                (rs, rowNum) -> new NPTByTimeGroup(
                        rs.getString("region_name"),
                        rs.getDate("chunk_start"),
                        rs.getString("display_color"),
                        rs.getString("display_text"),
                        rs.getString("cmr_sub_code"),
                        rs.getDouble("chunk_code_hours"),
                        groupType
                )
        ).stream().collect(toList());

        result.forEach(t -> log.debug("{}", t.toString()));

        return result;
    }

        public Collection NPTRegionHoursBySubCode(String region, String starttime, String endtime, int rigTypeId) {
        Collection myJSONArray = null;
        region = region.trim().toUpperCase();

        try {

            long startTime = DateTimeUtils.dateToFirstDayOfMonth(starttime);
            long endTime = DateTimeUtils.dateToLastDayOfMonth(endtime);

            long k = System.currentTimeMillis();
            String query =
                    "--NPTRegionHoursBySubCode\n" +
                    "SELECT c.display_text\n" +
                    ", c.display_color\n" +
                    ", SUM(b.hours) AS code_hours   \n" +
                    ",c.cmr_sub_code as sub_code\n" +
                    "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_details u on  a.well_num=u.well_num  \n" +
                    "  INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                    "  INNER JOIN pten_cmr.CMRDecode c ON c.cmr_sub_sub_code IS NULL AND b.code = c.cmr_code AND c.cmr_sub_code = split_part(b.sub_code, '.', 2) \n" +
                    "  INNER JOIN pten_cmr.RegionMap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)  \n" +
                    "  INNER JOIN pten_cmr.rig r ON a.rig_name = r.display_text\n" +
                    "  INNER JOIN pten_cmr.rigtype rt ON r.rig_type_id = rt.rig_type_id\n" +
                    "  INNER JOIN pten_cmr.rigmastertype rmt ON rmt.rig_master_type_id = rt.rig_master_type_id\n" +
                    "WHERE u.userid=" + getUser_id() + " and  UPPER(rm.region_name) = :region \n" +
                    "  AND b.code = '8' \n" +
                    "  AND b.code NOT LIKE '%Z%'    \n" +
                    "  AND (b.`from` BETWEEN :start AND :end OR b.`to` BETWEEN :start AND :end) \n" +
                    ((rigTypeId == -99)?"": "\t AND rt.rig_master_type_id = :rigtypeid") + "\n" +
                    "GROUP BY display_text, c.display_color,c.cmr_sub_code\n" +
                    "ORDER BY code_hours DESC;";


            SqlParameterSource namedParameters = new MapSqlParameterSource()
                    .addValue("region", region)
                    .addValue("start", startTime)
                    .addValue("end", endTime)
                    .addValue("rigtypeid", rigTypeId);

            return impalaDbUtilLib.executeNamedQuery(query, namedParameters);

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);

        }

        return myJSONArray;
    }

    private String getChunkEnd(String groupType) {
        switch (groupType.toUpperCase()) {
            case "Q":
                // Quarterly , adding one quarter (by months)
                return ",add_months(seconds_add(lead(chunk_start, 1) OVER(order by chunk_start) , -1), 4) AS chunk_end\n";
            case "W":
                // Weeks have to start on Thursday, so we have to slightly modify the trunc output
                return ",date_add(seconds_add(lead(chunk_start, 1) OVER(order by chunk_start) , -1), 7) AS chunk_end\n";
            default:
                return ",date_add(seconds_add(lead(chunk_start, 1) OVER(order by chunk_start) , -1), interval 1 month) AS chunk_end\n";
        }

    }

    //shared time groups interval selector query

    /**
     * time group interval type. quarter, week or month.
     * default is month
     * @param groupType  q | w | m (default)
     * @return
     */
    private String getIntervalQuery(String groupType) {
        switch (groupType.toUpperCase()) {
            case "Q":
                // Quarterly and Monthly can use basic trunc function as root method
                //return "SELECT TRUNC(to_timestamp(a.report_date), 'Q') AS chunk_start \n";
                return "SELECT UNIX_TIMESTAMP(TRUNC(to_timestamp(a.report_date), 'Q')) AS chunk_start \n";
            case "W":
                // Weeks have to start on Thursday, so we have to slightly modify the trunc output
                //return "SELECT adddate(trunc(to_timestamp(a.report_date), 'D'), 3) AS chunk_start \n";
                return "SELECT UNIX_TIMESTAMP(adddate(trunc(to_timestamp(a.report_date), 'D'), 3)) AS chunk_start \n";
            default:
                //return "SELECT trunc(to_timestamp(a.report_date), 'MM') AS chunk_start \n";
                return "SELECT UNIX_TIMESTAMP(TRUNC(to_timestamp(a.report_date), 'MM')) AS chunk_start \n";

        }
    }

    public Collection getWellNptPercentage(Collection<Long> wells) {

        Collection model = cerebraAPIDataLayer.getNptPercent(wells);

        return mapper.unmarshall(model, new Percent());
    }

    public Collection getWellNptHours(Collection<Long> wells) {

        Collection model = cerebraAPIDataLayer.getNptHours(wells);

        return mapper.unmarshall(model, new NPT());

    }

    public Collection getNptHoursBySubCode(Collection<Long> wells) {

        Collection model = cerebraAPIDataLayer.getNptHoursBySubCode(wells);

        return mapper.unmarshall(model, new NPT());

    }

    public Collection getNptHoursAverage(Collection<Long> wells) {

        Collection model = cerebraAPIDataLayer.getNptHoursAverage(wells);

        return mapper.unmarshall(model, new NPT());
    }

    //TODO: candidate to be chached.
    //TODO: implement cache
    public Collection getSubcodes(){
        Collection model = cerebraAPIDataLayer.getSubcodeColors();
        return mapper.unmarshall(model, new SubCode());
    }

    public Collection getRigs(String region) {

        LocalDate now = LocalDate.now();

        int limit = 5;

        String query = "SELECT a.rig_name\n" +
                "  ,cast ( SUM(CASE WHEN b.code = '8' AND b.sub_code NOT LIKE '%Z%' THEN b.hours ELSE 0 END) / SUM(b.hours) as decimal(10,10)) AS rig_npt_percent \n" +
                "FROM user_Security.user_mapping_details u \n" +
                "inner join pten_cmr.cmr_data_usalt7 a on  a.well_num=u.well_num AND u.userid = :user\n" +
                "  INNER JOIN pten_cmr.cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id \n" +
                "  INNER JOIN pten_cmr.RegionMap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "WHERE UPPER(rm.region_name) in (:region)\n" +
                "  AND (b.`from` BETWEEN :startDate AND :endDate OR b.`to` BETWEEN :startDate AND :endDate ) \n" +
                "GROUP BY a.rig_name \n" +
                "ORDER BY rig_npt_percent\n" +
                "limit :limit";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("region", region.toUpperCase())
                .addValue("limit", limit)
                .addValue("startDate", DateTimeUtils.dateToFirstSecondOfDay(now.minusMonths(6)))
                .addValue("endDate", DateTimeUtils.dateToFirstSecondOfDay(now))
                .addValue("user", getUser_id());

        return impalaDbUtilLib.executeNamedQuery(query, namedParameters);
    }
}
