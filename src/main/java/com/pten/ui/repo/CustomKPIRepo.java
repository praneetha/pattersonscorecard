package com.pten.ui.repo;

import com.pten.Utilities.DateUtils.DateTimeUtils;
import com.pten.Utilities.DbUtils.MSSQLDbUtilLib;
import microsoft.sql.Types;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Date;

@Component
public class CustomKPIRepo {

    private static final Logger log = LoggerFactory.getLogger(CustomKPIRepo.class);

    @Autowired
    MSSQLDbUtilLib mssqlDbUtilLib;

    @Autowired
    DateTimeUtils dateTimeUtils;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Collection getMetricsMonthly(String operator, Date selectedDate){

        String query = "select * from MetricsMonthlyCacheResult";
        return mssqlDbUtilLib.executeNamedQuery(query, null);
    }

    public Collection getMetricsWeekly(String operator, Date startTime, Date endTime){

        String query = " select * from MetricsWeeklyCacheResult";
        return mssqlDbUtilLib.executeNamedQuery(query, null);
    }

    public Collection getGlossary() {
        String query = "SELECT headline_text, section_text, detail_text,header_description\n" +
                "  FROM dbo.Glossary\n" +
                "  Where report_id=3 or report_id=0\n" +
                "  ORDER BY sort_order";

        return mssqlDbUtilLib.executeNamedQuery(query, new MapSqlParameterSource());
    }

    public Collection getOperator() {

        String query = "select top 1 * from welldata";

        return mssqlDbUtilLib.executeNamedQuery(query, null);
    }

    public Collection getOperators(String operator, Date starttime) {

        String query = "select distinct operator\n" +
                "        ,datename( month, :selecteddate) as month\n" +
                "        ,datename( year, :selecteddate) as year\n" +
                "from wellData\n" +
                "where operator like :operator ;";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("operator", "%" + operator + "%")
                .addValue("selecteddate", starttime, Types.DATETIME);

        return mssqlDbUtilLib.executeNamedQuery(query, namedParameters);
    }

    public Collection getHeader(String operator, Date selectedDate){

        String query = "select * from HeaderCacheResult";
        return mssqlDbUtilLib.executeNamedQuery(query, null);
    }

    public Collection getCasing(String operator, Date selectedDate) {

        String query = "select * from CasingCacheResult";
        return mssqlDbUtilLib.executeNamedQuery(query, null);
    }

    public Collection getDrilling(String operator, Date selectedDate) {

        String query = "select distinct " +
                "   rig_name, " +
                "   avg_s2s, " +
                "   avg_w2s, " +
                "   avg_s2w " +
                "from DrillingCacheResult";
        return mssqlDbUtilLib.executeNamedQuery(query, null);
    }

    public Collection getTripping(String operator, Date selectedDate){

        String query = " select * from dbo.TrippingCacheResult";
        return mssqlDbUtilLib.executeNamedQuery(query, null);
    }
}
