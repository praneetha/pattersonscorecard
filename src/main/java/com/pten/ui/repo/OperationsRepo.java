package com.pten.ui.repo;

import com.flutura.ImpalaRestApiController.RequestHandler;
import com.pten.Utilities.DateUtils.DateTimeUtils;
import com.pten.Utilities.DbUtils.ImpalaDbUtilLib;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Date;

@Component
public class OperationsRepo {
    private static final Logger log = LoggerFactory.getLogger(RequestHandler.class);

    @Autowired
    ImpalaDbUtilLib impalaDbUtil;

    public int getUser_id() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().
                getAuthentication().getPrincipal();
        return Integer.parseInt(userDetails.getUsername());
    }

    public Collection getOperators(Date from, Date to) {
        String query = "SELECT  distinct a.operator_name\n" +
                "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_Details u on a.well_num=u.well_num and u.userid = :user\n" +
                "WHERE NOT EXISTS (\n" +
                "  SELECT NULL\n" +
                "  FROM pten_cmr.cmr_data_usalt7 z inner join user_Security.user_mapping_Details u on z.well_num=u.well_num and u.userid = :user\n" +
                "  WHERE a.well_num = z.well_num\n" +
                "     AND a.report_date < z.report_date\n" +
                ") and a.release_time between :startDate and :endDate\n" +
                "order by operator_name";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("startDate", DateTimeUtils.dateToFirstSecondOfDay(DateTimeUtils.toLocalDate(from)))
                .addValue("endDate", DateTimeUtils.dateToLastSecondOfDay(DateTimeUtils.toLocalDate(to)))
                .addValue("user", getUser_id());

        return impalaDbUtil.executeNamedQuery(query, namedParameters);

    }

    public Collection getAreas(Date from, Date to) {
        String query = "SELECT  distinct \n" +
                "rm.region_name     \n" +
                "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_Details u on a.well_num=u.well_num and u.userid = :user\n" +
                "INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "WHERE NOT EXISTS (\n" +
                "  SELECT NULL\n" +
                "  FROM pten_cmr.cmr_data_usalt7 z inner join user_Security.user_mapping_Details u on z.well_num=u.well_num and u.userid = :user\n" +
                "  WHERE a.well_num = z.well_num\n" +
                "     AND a.report_date < z.report_date\n" +
                ") and a.release_time between :startDate and :endDate\n" +
                "order by region_name";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("startDate", DateTimeUtils.dateToFirstSecondOfDay(DateTimeUtils.toLocalDate(from)))
                .addValue("endDate", DateTimeUtils.dateToLastSecondOfDay(DateTimeUtils.toLocalDate(to)))
                .addValue("user", getUser_id());

        return impalaDbUtil.executeNamedQuery(query, namedParameters);

    }

    public Collection getRigs(Date from, Date to) {
        String query = "SELECT  distinct \n" +
                "a.rig_name\n" +
                "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_Details u on a.well_num=u.well_num and u.userid = :user\n" +
                "INNER JOIN pten_cmr.regionmap rm ON UPPER(a.contractor_business_unit) = UPPER(rm.contractor_business_unit)\n" +
                "WHERE NOT EXISTS (\n" +
                "  SELECT NULL\n" +
                "  FROM pten_cmr.cmr_data_usalt7 z inner join user_Security.user_mapping_Details u on z.well_num=u.well_num and u.userid = :user\n" +
                "  WHERE a.well_num = z.well_num\n" +
                "     AND a.report_date < z.report_date\n" +
                ") and a.release_time between :startDate and :endDate\n" +
                "order by rig_name";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("startDate", DateTimeUtils.dateToFirstSecondOfDay(DateTimeUtils.toLocalDate(from)))
                .addValue("endDate", DateTimeUtils.dateToLastSecondOfDay(DateTimeUtils.toLocalDate(to)))
                .addValue("user", getUser_id());

        return impalaDbUtil.executeNamedQuery(query, namedParameters);

    }

    public Collection getRigTypes(Date from, Date to) {
        String query = "SELECT  distinct \n" +
                "rmt.rig_master_type_name as rig_type_text\n" +
                "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_Details u on a.well_num=u.well_num and u.userid = :user\n" +
                "INNER JOIN pten_cmr.rig r ON r.display_text = a.rig_name\n" +
                "INNER JOIN pten_cmr.rigtype rt ON r.rig_type_id = rt.rig_type_id\n" +
                "INNER JOIN pten_cmr.rigmastertype rmt ON rmt.rig_master_type_id = rt.rig_master_type_id\n" +
                "WHERE NOT EXISTS (\n" +
                "  SELECT NULL\n" +
                "  FROM pten_cmr.cmr_data_usalt7 z inner join user_Security.user_mapping_Details u on z.well_num=u.well_num and u.userid = :user\n" +
                "  WHERE a.well_num = z.well_num\n" +
                "     AND a.report_date < z.report_date\n" +
                ") and a.release_time between :startDate and :endDate\n" +
                "order by rig_master_type_name\n";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("startDate", DateTimeUtils.dateToFirstSecondOfDay(DateTimeUtils.toLocalDate(from)))
                .addValue("endDate", DateTimeUtils.dateToLastSecondOfDay(DateTimeUtils.toLocalDate(to)))
                .addValue("user", getUser_id());

        return impalaDbUtil.executeNamedQuery(query, namedParameters);

    }

    public Collection getWells(Date from, Date to) {
        String query = "SELECT  distinct \n" +
                "a.well_num\n" +
                ",a.well_name\n" +
                "FROM pten_cmr.cmr_data_usalt7 a inner join user_Security.user_mapping_Details u on a.well_num=u.well_num and u.userid = :user\n" +
                "WHERE NOT EXISTS (\n" +
                "  SELECT NULL\n" +
                "  FROM pten_cmr.cmr_data_usalt7 z inner join user_Security.user_mapping_Details u on z.well_num=u.well_num and u.userid = :user\n" +
                "  WHERE a.well_num = z.well_num\n" +
                "     AND a.report_date < z.report_date\n" +
                ") and a.release_time between :startDate and :endDate\n" +
                "order by well_name\n";

        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("startDate", DateTimeUtils.dateToFirstSecondOfDay(DateTimeUtils.toLocalDate(from)))
                .addValue("endDate", DateTimeUtils.dateToLastSecondOfDay(DateTimeUtils.toLocalDate(to)))
                .addValue("user", getUser_id());

        return impalaDbUtil.executeNamedQuery(query, namedParameters);

    }

}
