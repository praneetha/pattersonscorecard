package com.pten.rs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter //(urlPatterns = {"/"})
public class LoggerFilter implements Filter {

    private static final Logger log = LoggerFactory.getLogger(LoggerFilter.class);


    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;

        HttpServletResponse httpServletResponse = ((HttpServletResponse)servletResponse);

        String queryString = httpServletRequest.getQueryString();

        log.info("{} {} {}{}",
                httpServletRequest.getMethod(),
                httpServletResponse.getStatus(),
                httpServletRequest.getRequestURI(),
                null!=queryString?"?" + queryString:""
        );

        filterChain.doFilter(servletRequest, servletResponse);

    }

    @Override
    public void destroy() {

    }
}
