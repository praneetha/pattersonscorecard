package com.pten.Utilities.DbUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class PtenInternalDbConfig {

    private static final Logger log = LoggerFactory.getLogger(PtenInternalDbUtilLib.class);

    @Bean(name = "ptenInternalDataSource")
    @ConfigurationProperties(prefix = "ptenInternal.datasource")
    public DataSource dataSource(){
        return DataSourceBuilder.create().build();
    }

}
