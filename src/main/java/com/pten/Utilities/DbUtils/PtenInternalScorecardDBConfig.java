package com.pten.Utilities.DbUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration

public class PtenInternalScorecardDBConfig {

    private static final Logger log = LoggerFactory.getLogger(PtenInternalScorecardDBConfig.class);

    @Bean(name = "ptenInternalDataSourceScorecard")
    @ConfigurationProperties(prefix = "ptenInternal.datasource.scorecard")
    public DataSource dataSource(){
        return DataSourceBuilder.create().build();
    }
}
