package com.pten.Utilities.DbUtils;

import com.flutura.ImpalaRestApiController.JSONArrayExtractor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.Collection;

@Repository
@Component
public class PtenInternalDbUtilLib {

    private static final Logger log = LoggerFactory.getLogger(PtenInternalDbUtilLib.class);

    @Qualifier("ptenInternalDataSource")
    private NamedParameterJdbcTemplate pgNamedParameterJdbcTemplate;

    @Autowired
    private JSONArrayExtractor jsonArrayExtractor;

    final String pattern = "yyyy/MM";

    @Autowired
    public void setDataSource(@Qualifier("ptenInternalDataSource") DataSource dataSource) {
        this.pgNamedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public Collection executeNamedQuery(String query, SqlParameterSource namedParameters) {
        return pgNamedParameterJdbcTemplate.query(
                query,
                namedParameters,
                new JSONArrayExtractor()
        );
    }

}
