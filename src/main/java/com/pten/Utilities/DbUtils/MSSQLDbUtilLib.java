package com.pten.Utilities.DbUtils;

import com.flutura.ImpalaRestApiController.JSONArrayExtractor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.Collection;

@Repository
@Component
public class MSSQLDbUtilLib {
    private static final Logger log = LoggerFactory.getLogger(PtenInternalDbUtilLib.class);

    @Qualifier("MSSQLDataSource")
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setDataSource(@Qualifier("MSSQLDataSource") DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public Collection executeNamedQuery(String query, SqlParameterSource namedParameters) {
        return namedParameterJdbcTemplate.query(
                query,
                namedParameters,
                new JSONArrayExtractor()
        );
    }

}
