package com.pten.Utilities.DateUtils;

import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.TimeZone;

@Component
public final class DateTimeUtils {

    private DateTimeUtils(){}

    final static String pattern = "yyyy/MM";

    public static boolean isNumeric(String input)
    {
        try
        {
            // checking valid integer using parseInt() method
            Integer.parseInt(input);
            return true;
        }
        catch (NumberFormatException e)
        {
            return false;
        }
    }

    public static boolean isDate(String input)
    {
        try
        {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            dtf.parse(input);
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public static String convertUnixDate(long unixSeconds)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date dateTime = new Date((long)unixSeconds*1000);
        String report_date = LocalDate.parse(sdf.format(dateTime).toString()).minusDays(-1).toString();
        return report_date;
    }

    public static LocalDate toLocalDate(String date) throws ParseException {
        Date d = new SimpleDateFormat(pattern).parse(date);

        Instant fromInstant = d.toInstant();

        return fromInstant.atZone(ZoneId.systemDefault()).toLocalDate();
    }
    public static LocalDate toLocalDate(Date d){
        Instant fromInstant = d.toInstant();

        return fromInstant.atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static long dateToEpoch(String date, String pattern) throws ParseException {
        return new SimpleDateFormat(pattern).parse(date).getTime() / 1000;
    }

    public static long dateToEpoch(String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");

        //sdf.setTimeZone(TimeZone.getTimeZone("America/Chicago"));
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.parse(date).getTime() / 1000;
    }

    public static long dateToEpoch(Date date){
        return toLocalDate(date).atStartOfDay().toInstant(ZoneOffset.UTC).getEpochSecond();
    }

    public static long dateToFirstDayOfMonth(String date) throws ParseException {
        YearMonth month = YearMonth.from(toLocalDate(date));

        LocalDate firstDay = month.atDay(1);

        return atStartOfDay(firstDay);
    }

    public static long atStartOfDay(LocalDate date) {
        return date.atStartOfDay().toInstant(ZoneOffset.UTC).getEpochSecond();
    }

    public static long dateToLastDayOfMonth(String date) throws ParseException {
        YearMonth month = YearMonth.from(toLocalDate(date));

        LocalDate lastDay = month.atEndOfMonth();

        return lastDay.atTime(LocalTime.MAX).toInstant(ZoneOffset.UTC).getEpochSecond();
    }

    public static LocalDate dateToFirstDayOfMonth(Date date){
        YearMonth month = YearMonth.from(toLocalDate(date));

        LocalDate lastDay = month.atDay(1);

        return lastDay.atTime(LocalTime.MIN).toLocalDate();
    }

    public static LocalDate dateToLastDayOfMonth(Date date){
        YearMonth month = YearMonth.from(toLocalDate(date));

        LocalDate lastDay = month.atEndOfMonth();

        return lastDay.atTime(LocalTime.MAX).toLocalDate();
    }

    public static long dateToLastSecondOfDay(String date) throws ParseException {
        return dateToLastSecondOfDay(toLocalDate(date));
    }

    public static long dateToLastSecondOfDay(LocalDate date){
        return date.atTime(LocalTime.MAX).toInstant(ZoneOffset.UTC).getEpochSecond();
    }

    public static long dateToFirstSecondOfDay(LocalDate date){
        return date.atTime(LocalTime.MIN).toInstant(ZoneOffset.UTC).getEpochSecond();
    }
	
	 public static long toStartDay(String date) throws ParseException {
        Date d = new SimpleDateFormat("yyyy/MM/dd").parse(date);

        Instant fromInstant = d.toInstant();

        LocalDate localDate = fromInstant.atZone(ZoneId.systemDefault()).toLocalDate();
        return localDate.atStartOfDay().toInstant(ZoneOffset.UTC).getEpochSecond();
    }
	
}
