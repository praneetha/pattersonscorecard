package com.pten.Utilities;

import com.flutura.ImpalaRestApiController.JSONArrayExtractor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

@Repository
@Component
public class UtilityLibrary {
    private static final Logger log = LoggerFactory.getLogger(UtilityLibrary.class);


    @Autowired
    JSONArrayExtractor jsonArrayExtractor;


    public Connection connect() {
        Connection con = null;
        try {
            // Register driver and create driver instance
            Class.forName("com.cloudera.impala.jdbc41.Driver");
            con = DriverManager.getConnection("jdbc:impala://PPLUS-PROD-DATA01:21051/default", "", "");
        } catch (ClassNotFoundException | SQLException ex) {
            log.error(ex.getMessage(), ex);
        }
        return con;
    }


    public Collection runSQLGetJSONArray(String query) {
        Collection myJSONResults = new ArrayList();

        try (Connection con = connect(); Statement stmt = con.createStatement()) {

            stmt.setFetchSize(6000);
            ResultSet myResultSet = stmt.executeQuery(query);

            return jsonArrayExtractor.extractData(myResultSet);

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            myJSONResults.add(ex.getMessage());
        }
        return myJSONResults;
    }

    public Collection executePreparedStatement(Connection con, String query, Object... params) {
        try (PreparedStatement ps = con.prepareStatement(query)) {

            int index = 0;

            for (Object optional : params) {
                if (optional instanceof List<?>) {
                    for (Object param : (List) optional) {
                        if (param instanceof List<?>) {
                            for (Object inListParam : (List) param) {
                                ps.setObject(++index, inListParam);
                            }
                        } else {
                            ps.setObject(++index, param);
                        }
                    }
                } else {
                    ps.setObject(++index, optional);
                }
            }

            ResultSet myResultSet = ps.executeQuery();
            return jsonArrayExtractor.extractData(myResultSet);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return null;
    }


    public Collection executePreparedStatement(PreparedStatement preparedStatement) {
        try(Connection con = connect();  Statement stmt = con.createStatement()) {

            stmt.setFetchSize(6000);
            ResultSet myResultSet = preparedStatement.executeQuery();
            return jsonArrayExtractor.extractData(myResultSet);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return null;
    }


    public String arrayToInClause(String[] myArray) {
        // Array to IN list
        String myReturn = "";
        for (String i : myArray) {
            myReturn += "'" + i + "',";
        }

        if (myReturn.length() > 0) {
            myReturn = myReturn.substring(0, myReturn.length() - 1);
        }

        return myReturn;
    }

    public String collectionToInClause(Collection items) {
        String clause = "";
        for (Object item : items) {
            if (item instanceof Long) {
                clause += item + ",";
            }
        }
        if (clause.length() > 0) {
            return clause.substring(0, clause.length() - 1);
        }
        return clause;
    }

    public String inClauseParams(int size) {
        String clause = "";
        for (int i = 1; i <= size; i++) {
            clause += "?";
            if (i != size) {
                clause += ",";
            }
        }
        return clause;
    }
public String capitalize(String text){
        String c = (text != null)? text.trim() : "";
        String[] words = c.split(" ");
        StringBuilder result = new StringBuilder();
        for(String w : words){
            result.append(w.length() > 1 ? w.substring(0, 1).toUpperCase(Locale.US) + w.substring(1, w.length()).toLowerCase(Locale.US) : w).append(" ");
        }
        return result.toString().trim();
    }
}
